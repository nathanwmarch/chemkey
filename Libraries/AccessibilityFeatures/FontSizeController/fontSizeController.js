function createFontSizeController() {
    var controllerDiv = document.createElement("div");
    controllerDiv.setAttribute("id", "font-size-controller");
    document.body.appendChild(controllerDiv);

    var increaseFontDiv = document.createElement("div");
    var fontDivTextNode = document.createTextNode("A");
    increaseFontDiv.appendChild(fontDivTextNode);

    var decreaseFontDiv = document.createElement("div");
    var fontDivTextNode = document.createTextNode("A");
    decreaseFontDiv.appendChild(fontDivTextNode);

    increaseFontDiv.setAttribute("id", "increase-font-size");
    increaseFontDiv.onclick = increaseFontSize;

    decreaseFontDiv.setAttribute("id", "decrease-font-size");
    decreaseFontDiv.onclick = decreaseFontSize;

    controllerDiv.appendChild(decreaseFontDiv);
    controllerDiv.appendChild(increaseFontDiv);
}

function increaseFontSize() {
    var root = document.querySelector(":root");
    var fontSizeValue = window.getComputedStyle(root, null).getPropertyValue("font-size");
    var fontSize = parseFloat(fontSizeValue);
    if (fontSize >= 24) {
        root.style.fontSize = 24 + "px";
    } else {
        root.style.fontSize = (fontSize + 1) + "px";
    }
   
}

function decreaseFontSize() {
    var root = document.querySelector(":root");
    var fontSizeValue = window.getComputedStyle(root, null).getPropertyValue("font-size");
    var fontSize = parseFloat(fontSizeValue);
    if (fontSize <= 12) {
        root.style.fontSize = 12 + "px";
    } else {
        root.style.fontSize = (fontSize - 1) + "px";
    }
}

window.onload = function() {
    createFontSizeController();
}