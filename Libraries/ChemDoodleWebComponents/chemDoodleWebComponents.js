
window.addEventListener("load", generateChemicalStructures);

function generateChemicalStructures() {
    configureCDWCGlobalVariables();
    generateChemicalStructureContainerElements();
}

function configureCDWCGlobalVariables() {
    ChemDoodle.default_backgroundColor = "rgba(0,0,0,0)";
    //ChemDoodle.default_atoms_displayTerminalCarbonLabels_2D = true;
    ChemDoodle.default_atoms_font_size_2D = 35;
    //ChemDoodle.default_atoms_useJMOLColors = false;
    ChemDoodle.default_atoms_displayAllCarbonLabels_2D = true;
    ChemDoodle.default_atoms_implicitHydrogens_2D = true;
}

function generateChemicalStructureContainerElements() {
    var chemicalStructureContainers = document.getElementsByClassName("chemical-structure");
    processChemicalStructuresAtAndBeforeIndex(chemicalStructureContainers, chemicalStructureContainers.length - 1);
}

function processChemicalStructuresAtAndBeforeIndex(chemicalStructureContainers, index) {
    var chemicalStructureContainer = chemicalStructureContainers[index];

    if (chemicalStructureContainer) {
        var name = chemicalStructureContainer.getAttribute("name");
        console.log("Name:", name);
        var canvas = document.createElement("canvas");
        var canvasId = "cdwc-chemical-2d-" + canvasIDs.next();
        console.log("canvasId:", canvasId);
        canvas.setAttribute("id",canvasId);
        chemicalStructureContainer.appendChild(canvas);

        console.log("Loading mol file");

        var fileUrl = "https://shout.education/ChemKey/Sources/ChemicalStructures/" + name + ".mol";

        ChemDoodle.io.file.content(fileUrl, function(fileContents) {
            console.log("Generating canvas");
            var myCanvas = new ChemDoodle.ViewerCanvas(canvasId,200,200);
            var molecule = ChemDoodle.readMOL(fileContents);
            molecule.scaleToAverageBondLength(80);
            var moleculeDimensions = molecule.getDimension();
            var moleculeWidth = moleculeDimensions.x;
            var moleculeHeight = moleculeDimensions.y;
            myCanvas.loadMolecule(molecule);
            myCanvas.repaint();
            myCanvas.resize(200,200);
            processChemicalStructuresAtAndBeforeIndex(chemicalStructureContainers, index - 1);
        })
    } 
}

var canvasIDs = {
    counter: 0,
    next: function() {return canvasIDs.counter++},
}

