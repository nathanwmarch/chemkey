function constructSequence(sequence) {
    var sequenceContainer = document.createElement("div");
    sequenceContainer.setAttribute("class", "sequence-container");
    sequence.parentElement.insertBefore(sequenceContainer, sequence)

    sequenceContainer.appendChild(sequence);

    var sequenceItems = sequence.children;
    //console.log(sequenceItems);

    for (var i = sequenceItems.length - 1; i >= 0; i--) {
        var sequenceItem = sequenceItems[i];
        var sequenceItemClass = sequenceItem.getAttribute("class");
        sequenceItem.setAttribute("onclick", "nextSequenceItem(this)");
        //console.log(sequenceItem.offsetHeight);
        if (i == 0) {
            sequenceItem.className += " sequence-item active"
        } else {
            sequenceItem.setAttribute("class", "sequence-item")
        }
    }

    var previousButton = document.createElement("div");
    previousButton.setAttribute("class", "previous-button");
    previousButton.setAttribute("onclick", "previousSequenceItem(this)");
    sequenceContainer.appendChild(previousButton);
}

function constructSequences() {
    var sequences = document.getElementsByClassName("sequence");
    //console.log(sequences);

    for (var i = sequences.length - 1; i >= 0; i--) {
        var sequenceName = "sequence" + i;
        var sequence = sequences[i];
        window[sequenceName] = sequence;
        constructSequence(sequence);
    }
}

function nextSequenceItem(control) {
    var sequence = control.parentElement
    var activeSequenceItem = sequence.getElementsByClassName("active")[0];
    var nextSequenceItem;
    //console.log(activeSequenceItem);

    if (activeSequenceItem.nextElementSibling) {
        nextSequenceItem = activeSequenceItem.nextElementSibling;
    } else {
        nextSequenceItem = sequence.firstElementChild;
    }
    //console.log(nextSequenceItem);

    nextSequenceItem.className += " active";
    activeSequenceItem.className = activeSequenceItem.className.replace(" active", "");
}

function previousSequenceItem(control) {
    var sequence = control.parentElement.getElementsByClassName("sequence")[0];
    var activeSequenceItem = sequence.getElementsByClassName("active")[0];
    var previousSequenceItem;
    //console.log(activeSequenceItem);

    if (activeSequenceItem.previousElementSibling) {
        previousSequenceItem = activeSequenceItem.previousElementSibling;
    } else {
        previousSequenceItem = sequence.lastElementChild;
    }
    //console.log(previousSequenceItem);

    previousSequenceItem.className += " active";
    activeSequenceItem.className = activeSequenceItem.className.replace(" active", "");
}

window.addEventListener("load", constructSequences);