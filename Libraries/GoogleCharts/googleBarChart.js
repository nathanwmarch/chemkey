google.charts.load("current", {"packages": ["corechart"]});

function convertGoogleTableToGoogleBarChart(googleTable) {
    var table = googleTable.getElementsByTagName("table")[0];
    
    var tableObject = convertTableToObject(table);
    var googleDataTable = convertTableObjectToGoogleDataTable(tableObject);
    var googleDataView = new google.visualization.DataView(googleDataTable);

    var chart = new google.visualization.BarChart(googleTable);

    chart.draw(googleDataView,{});
}