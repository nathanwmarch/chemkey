// <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

google.charts.load("current", {"packages": ["table"]});
google.charts.setOnLoadCallback(convertTablesToGoogleTables);

function convertTablesToGoogleTables() {
    var convertableTables = document.getElementsByClassName("convertable-table");

    for (var i = convertableTables.length - 1; i >= 0; i--) {
        convertToGoogleTable(convertableTables[i]);
    }
}

function coercedToNumber(text) {
    //console.log("Coerced " + text + " to", +text);
    return +text;
}

function convertToGoogleTable(table) {
    var div = document.createElement("div");
    var tableClass = table.className;
    div.setAttribute("class",tableClass.replace("convertable-table", "google-table"));
    table.parentElement.insertBefore(div, table);

    var tableObject = convertTableToObject(table);
    var googleDataTable = convertTableObjectToGoogleDataTable(tableObject);

    var googleTable = new google.visualization.Table(div);

    googleTable.draw(googleDataTable, {});
    table.parentElement.removeChild(table);
    //table.setAttribute("style", "display: none;");
}

function convertTableObjectToGoogleDataTable(tableObject) {
    var data = new google.visualization.DataTable();

    var numberOfSeries = tableObject.series.length;

    //Add columns using headers and seriesTypes
    for (var i = 0; i < numberOfSeries; i++) {
        var columnName = tableObject.headers[i];
        var columnType = tableObject.seriesTypes[i];
        data.addColumn(columnType, columnName);
    }

    var rowData = convertSeriesToRowData(tableObject.series);

    data.addRows(rowData);

    //console.log(data);

    return data
}

function convertSeriesToRowData(series) {
    //Prepare empty array of empty arrays for data

    //console.log(series);
    var rowData = [];

    for (var i = 0; i < series.length; i++) {
        //console.log("Checking series", i);
        for (var j = 0; j < series[i].length; j++) {
            //console.log("Checking item", j);
            if (rowData[j]) {
                rowData[j].push(series[i][j]);
            } else {
                rowData[j] = [];
                rowData[j].push(series[i][j]);
            }   
        }
    }

    //console.log(rowData);
    return rowData;
}

function convertTableToObject(table) {
    var tableObject = {};

    var tableHeadRow = table.tHead.rows[0];
    var tableHeadersArray = Array.from(tableHeadRow.getElementsByTagName("TH"));
    var tableHeadersInnerHTML = tableHeadersArray.map(function (th) { return formattedHeading(th.innerHTML); });

    tableObject["headers"] = tableHeadersInnerHTML;

    var tableBodyRows = table.tBodies[0].rows;

    var numberOfSeries = tableBodyRows[0].getElementsByTagName("TD").length;

    //Create empty array of arrays to contain table data
    var series = [];
    for (var i = 0; i < numberOfSeries; i++) {
        series.push([]);
    }

    //Populate series array
    for (var i = 0; i < tableBodyRows.length; i++) {
        var bodyRowCells = tableBodyRows[i].getElementsByTagName("TD");

        for (var j = 0; j < bodyRowCells.length; j++) {
            series[j].push(formattedCellValue(bodyRowCells[j].innerHTML));
        }
    }

    //Add series to tableObject
    tableObject["series"] = series;

    //Create empty array for series types
    var seriesTypes = []

    //Populate series types array
    series.forEach(function(array) {
        seriesTypes.push(typeOf(array));
    })

    //Add series types to tableObject
    tableObject["seriesTypes"] = seriesTypes;

    for (var i = 0; i < tableObject.series.length; i++) {
        if (tableObject.seriesTypes[i] == "number") {
            var coercedSeries = [];
            tableObject.series[i].forEach(function(item) {coercedSeries.push(coercedToNumber(item))});
            tableObject.series[i] = coercedSeries;
        }
    }

    //console.log(tableObject);
    return tableObject;
}






function typeOf(array) {
    if (array.every(isNaN)) {
        return "string";
    } else {
        return "number"
    }
}

function formattedHeading(innerHTML) {
    var newString = innerHTML.replace("<sup>-1</sup>","⁻¹").replace(/1<sup>st<\/sup>/g,"first").replace(/<br>/g, " ");
    return newString
}

function formattedCellValue(innerHTML) {
    var newString = innerHTML.replace(/<sub>2<\/sub>/g, "₂").replace(/<sub>3<\/sub>/g, "₃").replace(/<sub>4<\/sub>/g, "₄").replace(/<sub>5<\/sub>/g, "₅").replace(/<sub>6<\/sub>/g, "₆").replace(/<sub>7<\/sub>/g, "₇").replace(/<sub>8<\/sub>/g, "₈").replace(/<sub>9<\/sub>/g, "₉").replace(/<sub>10<\/sub>/g, "₁₀").replace(/<sub>11<\/sub>/g, "₁₁").replace(/<sub>12<\/sub>/g, "₁₂")
    return newString
}