window.addEventListener("load", generateQuickNavigation);

function generateQuickNavigation() {
    var content = document.getElementsByClassName("content")[0];
    var children = content.children;
    var quickNavigation = document.createElement("div");
    quickNavigation.setAttribute("id", "quick-navigation");
    quickNavigation.setAttribute("onclick", "clickQuickNavigation()");
    var scrollDiv = document.createElement("div");
    scrollDiv.setAttribute("id", "quick-navigation-scroll");
    quickNavigation.appendChild(scrollDiv);

    var h1Counter = 0;
    var h2Counter = 0;
    var h3Counter = 0;
    var h4Counter = 0;
 
    for (var i = 0; i < children.length; i++) {
        var child = children[i];
        var childTagName = child.tagName;

        switch (childTagName) {
            case "H1":
                var h1link = document.createElement("a");
                child.setAttribute("id", "h1-" + h1Counter);
                h1link.setAttribute("class", "quick-navigation-h1");
                h1link.setAttribute("href", "#h1-" + h1Counter);
                //h1link.setAttribute("onclick", "closeQuickNavigation()");
                h1Counter++;
                h1link.innerHTML = child.innerHTML;
                scrollDiv.appendChild(h1link);
                break;
            case "H2":
                var h2link = document.createElement("a");
                child.setAttribute("id", "h2-" + h2Counter);
                h2link.setAttribute("class", "quick-navigation-h2");
                h2link.setAttribute("href", "#h2-" + h2Counter);
                //h2link.setAttribute("onclick", "closeQuickNavigation()");
                h2Counter++;
                h2link.innerHTML = child.innerHTML;
                scrollDiv.appendChild(h2link);
                break;
            case "H3":
                var h3link = document.createElement("a");
                child.setAttribute("id", "h3-" + h3Counter);
                h3link.setAttribute("class", "quick-navigation-h3");
                h3link.setAttribute("href", "#h3-" + h3Counter);
                //h3link.setAttribute("onclick", "closeQuickNavigation()");
                h3Counter++;
                h3link.innerHTML = child.innerHTML;
                scrollDiv.appendChild(h3link);
                break;
            case "H4":
                var h4link = document.createElement("a");
                child.setAttribute("id", "h4-" + h4Counter);
                h4link.setAttribute("class", "quick-navigation-h4");
                h4link.setAttribute("href", "#h4-" + h4Counter);
                //h4link.setAttribute("onclick", "closeQuickNavigation()");
                h4Counter++;
                h4link.innerHTML = child.innerHTML;
                scrollDiv.appendChild(h4link);
                break;
        }
        
    }

    document.body.appendChild(quickNavigation);
}

function clickQuickNavigation() {
    var quickNavigation = quickNavigationElement();
    var classOfQuickNavigation = quickNavigation.className;
    if (classOfQuickNavigation != "open") {
        quickNavigation.setAttribute("class", "open");
    } else {
        quickNavigation.setAttribute("class", "");
    }
}

function quickNavigationElement() {
    var quickNavigation = document.getElementById("quick-navigation");
    return quickNavigation;
}