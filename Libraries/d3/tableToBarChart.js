function formatted(data) {
    var formattedValue = d3.format(".1f")(data);
    console.log("Input data", data, "formatted", formattedValue);
    return formattedValue
}

function coerceToNumber(text) {
    console.log("Coerced " + text + " to", +text);
    return +text;
}

function convertToBarChart(table) {
    var tableObject = {}
    //console.log(table);
    var tableHeadRow = table.tHead.rows[0];
    //console.log(tableHeadRow);
    var tableHeadersArray = Array.from(tableHeadRow.getElementsByTagName("TH"));
    //console.log(tableHeadersArray);
    var tableHeadersInnerHTML = tableHeadersArray.map(function (th) { return th.innerHTML; });
    console.log("Headers:", tableHeadersInnerHTML);

    //Add innerHTML of each header to tableObject under key "headers", in an array
    tableObject["headers"] = tableHeadersInnerHTML;
    //console.log(tableObject);

    var tableBodyRows = table.tBodies[0].rows;
    //console.log(tableBodyRows);

    var numberOfSeries = tableBodyRows[0].getElementsByTagName("TD").length;
    //console.log("Number of series:", numberOfSeries);

    var series = [];

    //Create variables to store values for each series in each row
    for (var i = 0; i < numberOfSeries; i++) {
        var seriesName = "series" + i
        window[seriesName] = [];
        //console.log(window[seriesName]);
        series.push(window[seriesName]);
    }

    for (var i = 0; i < tableBodyRows.length; i++) {
        var bodyRowCells = tableBodyRows[i].getElementsByTagName("TD");
        for (var j = 0; j < bodyRowCells.length; j++) {
            if (j == 1) {
                series[j].push(coerceToNumber(bodyRowCells[j].innerHTML));
            } else if (j == 0) {
                var string = bodyRowCells[j].innerHTML;
                console.log("Pre-string:", string);
                string = string.replace("<sub>2</sub>", "₂")
                string = string.replace("<sub>3</sub>", "₃")
                string = string.replace("<sub>4</sub>", "₄")
                string = string.replace("<sub>5</sub>", "₅")
                string = string.replace("<sub>6</sub>", "₆")
                string = string.replace("<sub>7</sub>", "₇")
                string = string.replace("<sub>8</sub>", "₈")
                string = string.replace("<sub>9</sub>", "₉")
                string = string.replace("<sub>10</sub>", "₁₀")
                string = string.replace("<sub>11</sub>", "₁₁")
                string = string.replace("<sub>12</sub>", "₁₂")
                console.log("String:", string);
                series[j].push(string);
            }
        }
    }
    console.log("Series:", series);

    tableObject["series"] = series;

    //Generate containers
    var barChartDiv = document.createElement("div");
    barChartDiv.setAttribute("class", "converted-bar-chart-container svg-container");
    var barChartSVG = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    barChartSVG.setAttribute("class", "converted-bar-chart large-responsive-svg");
    barChartDiv.append(barChartSVG);
    barChartDiv.setAttribute("onclick", "restoreTable(this)");
    table.parentElement.insertBefore(barChartDiv, table);

    console.log("svg width:", barChartSVG.scrollWidth);
    //Sizes and X/Y valies
    //Height to match size of original table, width to match svg-container width unless smaller than 450px, in which case width defaults to 360px
    var SVGwidth = barChartSVG.scrollWidth;
    var width;
    if (SVGwidth < 450) {
        width = 450;
    } else {
        width = SVGwidth;
    }

    var height = table.offsetHeight;
    //Widths and heights dedicated to labels and titles
    var yAxisTitleWidth = 23;
    var yAxisLabelsWidth = 60;
    var xAxisTitleHeight = 23;
    var xAxisLabelsHeight = 0;
    //Right padding added to allow for x axis label overflow
    var rightPadding = 0.1 * width;
    //Plot area sizes and x/y values
    var plotHeight = height - xAxisTitleHeight - xAxisLabelsHeight;
    var plotWidth = width - yAxisTitleWidth - yAxisLabelsWidth - rightPadding;
    var plotMinX = yAxisTitleWidth + yAxisLabelsWidth;
    var plotMaxX = width - rightPadding;
    var plotMinY = 0;
    var plotMaxY = plotHeight;
    //Bar height
    var barHeight = plotHeight / series1.length;

    //Scale function
    var minValue = d3.min(series1);
    var maxValue = d3.max(series1);
    var scale;
    var originX;

    if (minValue >= 0) {
        scale = d3.scale.linear()
            .domain([0, maxValue])
            .range([plotMinX, plotMaxX]);
        originX = plotMinX;
    } else if (maxValue < 0) {
        scale = d3.scale.linear()
            .domain([minValue, 0])
            .range([plotMinX, plotMaxX]);
        originX = plotMaxX;
    } else {
        scale = d3.scale.linear()
            .domain([minValue, maxValue])
            .range([plotMinX, plotMaxX]);
        originX = scale(0);
    }

    //Chart
    var chart = d3.select(".converted-bar-chart")
        .attr("height", height)
        .attr("width", width);

    console.log(chart);

    //Plot
    var plot = chart.append("g")
        .attr("class", "plot")
        .attr("transform", "translate(" + plotMinX + ",0)");

    plot.append("rect")
        .attr("class", "plot-frame")
        .style("width", plotWidth)
        .style("height", plotHeight)
        .attr("fill", "transparent");

    //Bars
    var bars = plot.selectAll("g")
        .data(series1)
        .enter()
        .append("g")
        .attr("class", "bar-container")
        .attr("transform", function (d, i) {
            var y = formatted(i * barHeight);
            var x;

            if (originX > plotMinX) {
                if (d >= 0) {
                    x = formatted(originX - plotMinX);
                } else {
                    x = formatted(scale(d) - plotMinX);
                }

            } else {
                x = 0;
            }

            return "translate(" + x + "," + y + ")";
        });
    console.log(bars);

    //Draw bars
    bars.append("rect")
        .attr("class", function (d) { return d >= 0 ? "bar bar-positive" : "bar bar-negative"; })
        .style("width", function (d) { return d >= 0 ? formatted(scale(d) - originX) + "px" : formatted(originX - scale(d)) + "px"; })
        .style("height", formatted(barHeight - 1))

    //Bar labels
    bars.append("text")
        .attr("class", function (d) { return d >= 0 ? "bar-value bar-value-positive" : "bar-value bar-value-negative"; })
        .attr("x", function (d) { return d >= 0 ? formatted(scale(d) - originX - 3) + "px" : "3px" })
        .attr("y", formatted(barHeight / 2) + "px")
        .attr("dy", ".35em")
        .text(function (d) { return d; });

    //Y-axis labels
    var yAxisLabels = chart.append("g")
        .attr("class", "y-axis-labels")
        .attr("transform", "translate(" + plotMinX + ",0)");

    yAxisLabels.selectAll("g")
        .data(series0)
        .enter()
        .append("text")
        .attr("class", "y-axis-label")
        .attr("font-size", "0.8rem")
        .attr("transform", function (d, i) { return "translate(-3," + formatted((i + 0.5) * barHeight) + ")"; })
        .attr("dy", ".35em")
        .text(function (d) { return d });

    //Y-axis title
    var yAxisTitle = chart.append("g")
        .attr("transform", "rotate(-90 0 0) translate(" + -plotHeight + ",0)");

    var yAxisTitleString = tableObject["headers"][0];
    yAxisTitleString = yAxisTitleString.replace("<sub>3</sub>", "₃");
    console.log("yAxisTitleString", yAxisTitleString)

    yAxisTitle.append("text")
        .attr("class", "y-axis-title")
        .attr("x", formatted(plotHeight / 2))
        .attr("y", formatted(yAxisTitleWidth / 2))
        .attr("dy", "0.35em")
        .text(yAxisTitleString);

    //X-axis title
    var xAxisTitle = chart.append("g")
        .attr("transform", "translate(" + formatted(plotMinX) + "," + formatted(height - xAxisTitleHeight) + ")");

    var xAxisTitleString = tableObject["headers"][1];
    xAxisTitleString = xAxisTitleString.replace("<sub>3</sub>", "₃");
    xAxisTitleString = xAxisTitleString.replace("<sup>-1</sup>", "⁻¹");
    xAxisTitleString = xAxisTitleString.replace("<br>", " ")
    console.log("xAxisTitleString", xAxisTitleString)

    xAxisTitle.append("text")
        .attr("class", "y-axis-title")
        .attr("x", formatted(plotWidth / 2))
        .attr("y", formatted(xAxisTitleHeight / 2))
        .attr("dy", "0.35em")
        .text(xAxisTitleString);


    //Make SVG responsive
    barChartSVG.setAttribute("viewBox", "0 0 " + width + " " + height);
    barChartSVG.removeAttribute("height");
    barChartSVG.removeAttribute("width");

    //Hide table
    table.setAttribute("style", "display: none");
}

function restoreTable(barChartDiv) {
    var sourceTable = barChartDiv.nextSibling;

    sourceTable.style.display = null;
    sourceTable.setAttribute("onclick", "restoreBarChart(this)");
    barChartDiv.style.display = "none";
}

function restoreBarChart(table) {
    var barChartDiv = table.previousSibling;

    barChartDiv.style.display = null;
    table.style.display = "none";
}