function generateFormulas() {
    var inlineFormulaElements = document.getElementsByClassName("inline-formula");

    for (var i = inlineFormulaElements.length - 1; i >= 0; i--) {
        var inlineFormulaElement = inlineFormulaElements[i];
        convertInlineFormula(inlineFormulaElement);
    }

    var blockFormulaElements = document.getElementsByClassName("block-formula");

    for (var i = blockFormulaElements.length - 1; i >= 0; i--) {
        var blockFormulaElement = blockFormulaElements[i];
        convertBlockFormula(blockFormulaElement);
    }
}

function convertInlineFormula(inlineFormulaElement) {
    var TeXString = inlineFormulaElement.innerHTML.replace(/&amp;/g, '&').replace(/&lt;/g,'<').replace(/&gt;/g,'>');
    katex.render(TeXString, inlineFormulaElement, {
        displayMode: false,
        colorIsTextColor: true
    });
}

function convertBlockFormula(blockFormulaElement) {
    var TeXString = blockFormulaElement.innerHTML.replace(/&amp;/g, '&').replace(/&lt;/g,'<').replace(/&gt;/g,'>');
    console.log(TeXString);
    katex.render(TeXString, blockFormulaElement, {
        displayMode: true,
        colorIsTextColor: true
    });
}

window.addEventListener("load", generateFormulas);