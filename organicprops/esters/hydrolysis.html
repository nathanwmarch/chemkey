<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Hydrolysis of Esters | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Splitting esters into acids (or their salts) and alcohols using water, dilute acid or dilute alkali, including splitting of large esters to make soap.">
<meta name="keywords" content="ester, esters, hydrolysis, hydrolyse, hydrolysing, soap, glycerol">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Hydrolysis of Esters</h1>

<div class="text-block">
<p>This page describes ways of hydrolysing esters – splitting them into carboxylic acids (or their salts) and alcohols by the action of water, dilute acid or dilute alkali. It starts by looking at the hydrolysis of simple esters like ethyl ethanoate, and goes on to look at hydrolysing bigger, more complicated ones to make soap.</p>
</div>

<h2>Hydrolysing Simple Esters</h2>

<h3>What is Hydrolysis?</h3>

<div class="text-block">
<p>Technically, hydrolysis is a reaction with water. That is exactly what happens when esters are hydrolysed by water or by dilute acids such as dilute hydrochloric acid.</p>
<p>The alkaline hydrolysis of esters actually involves reaction with hydroxide ions, but the overall result is so similar that it is lumped together with the other two.</p>
</div>

<h3>Hydrolysis Using Water or Dilute Acid</h3>

<div class="text-block">
<p>The reaction with pure water is so slow that it is never used. The reaction is catalysed by dilute acid, and so the ester is heated under reflux with a dilute acid like dilute hydrochloric acid or dilute sulfuric acid.</p>
<p>Here are two simple examples of hydrolysis using an acid catalyst.</p>
<p>First, hydrolysing ethyl ethanoate:</p>
</div>

<div class="block-formula">
\underset{\color{#467abf}{\text{ethyl ethanoate}}}{\text{CH}_3\text{COOCH}_2\text{CH}_3} +~\text{H}_2\text{O} \xrightleftharpoons{\text{H}^+_{(aq)}} \underset{\color{#467abf}{\text{ethanoic acid}}}{\text{CH}_3\text{COOH}} + \underset{\color{#467abf}{\text{ethanol}}}{\text{CH}_3\text{CH}_2\text{OH}}
</div>

<div class="text-block">
<p>and then hydrolysing methyl propanoate:</p>
</div>

<div class="block-formula">
\underset{\color{#467abf}{\text{methyl propanoate}}}{\text{CH}_3\text{CH}_2\text{COOCH}_3} +~\text{H}_2\text{O} \xrightleftharpoons{\text{H}^+_{(aq)}} \underset{\color{#467abf}{\text{propanoic acid}}}{\text{CH}_3\text{CH}_2\text{COOH}} + \underset{\color{#467abf}{\text{methanol}}}{\text{CH}_3\text{OH}}
</div>

<div class="text-block">
<p>Notice that the reactions are reversible. To make the hydrolysis as complete as possible, you would have to use an excess of water. The water comes from the dilute acid, and so you would mix the ester with an excess of dilute acid.</p>
</div>

<div class="note">
<p>Note: These reactions are exactly the reverse of those used to make an ester from a carboxylic acid and an alcohol. The only difference in that case is that you use a concentrated acid as the catalyst. To get as much ester as possible, you wouldn't add any water otherwise you would favour the hydrolysis reaction.</p>
<p>The <a href="../../physical/catalysis/hydrolyse.html">mechanism for the acid hydrolysis of esters</a> is covered in the catalysis section of this site. It is not required for any UK A-level (or equivalent) chemistry syllabus.</p>
</div>

<h3>Hydrolysis Using Dilute Alkali</h3>

<div class="text-block">
<p>This is the usual way of hydrolysing esters. The ester is heated under reflux with a dilute alkali like sodium hydroxide solution.</p>
<p>There are two big advantages of doing this rather than using a dilute acid. The reactions are one-way rather than reversible, and the products are easier to separate.</p>
<p>Taking the same esters as above, but using sodium hydroxide solution rather than a dilute acid:</p>
<p>First, hydrolysing ethyl ethanoate using sodium hydroxide solution:</p>
</div>

<div class="block-formula">
\underset{\color{#467abf}{\text{ethyl ethanoate}}}{\text{CH}_3\text{COOCH}_2\text{CH}_3} +~\text{NaOH} \xrightleftharpoons{} \underset{\color{#467abf}{\text{sodium ethanoate}}}{\text{CH}_3\text{COONa}} + \underset{\color{#467abf}{\text{ethanol}}}{\text{CH}_3\text{CH}_2\text{OH}}
</div>

<div class="text-block">
<p> and then hydrolysing methyl propanoate in the same way:</p>
</div>

<div class="block-formula">
\underset{\color{#467abf}{\text{methyl propanoate}}}{\text{CH}_3\text{CH}_2\text{COOCH}_3} +~\text{NaOH} \xrightleftharpoons{} \underset{\color{#467abf}{\text{sodium propanoate}}}{\text{CH}_3\text{CH}_2\text{COONa}} + \underset{\color{#467abf}{\text{methanol}}}{\text{CH}_3\text{OH}}
</div>

<div class="text-block">
<p>Notice that you get the sodium salt formed rather than the carboxylic acid itself.</p>
<p>This mixture is relatively easy to separate. Provided you use an excess of sodium hydroxide solution, there won't be any ester left – so you don't have to worry about that.</p>
<p>The alcohol formed can be distilled off. That's easy!</p>
<p>If you want the acid rather than its salt, all you have to do is to add an excess of a strong acid like dilute hydrochloric acid or dilute sulfuric acid to the solution left after the first distillation.</p>
<p>If you do this, the mixture is flooded with hydrogen ions. These are picked up by the ethanoate ions (or propanoate ions or whatever) present in the salts to make ethanoic acid (or propanoic acid, etc). Because these are weak acids, once they combine with the hydrogen ions, they tend to stay combined.</p>
<p>The carboxylic acid can now be distilled off.</p>
</div>

<h2>Hydrolysing Complicated Esters to Make Soap</h2>

<div class="text-block">
<p>This next bit deals with the alkaline hydrolysis (using sodium hydroxide solution) of the big esters found in animal and vegetable fats and oils.</p>
</div>

<div class="note">
<p>Important: If you haven't already read that page, you should read the <a href="background.html">introduction to esters</a> so that you understand the nature of the fats and oils that are coming up next.</p>
</div>

<div class="text-block">
<p>If the large esters present in animal or vegetable fats and oils are heated with concentrated sodium hydroxide solution exactly the same reaction happens as with the simple esters.</p>
<p>A salt of a carboxylic acid is formed – in this case, the sodium salt of a big acid such as octadecanoic acid (stearic acid). These salts are the important ingredients of soap – the ones that do the cleaning.</p>
<p>An alcohol is also produced – in this case, the more complicated alcohol, propane-1,2,3-triol (glycerol).</p>
</div>

<div class="image-container"><img src="makesoap.gif"></div>

<div class="text-block">
<p>Because of its relationship with soap making, the alkaline hydrolysis of esters is sometimes known as saponification.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-estershydrolysis.pdf" target="_blank">Questions on the hydrolysis of esters</a>
<a href="../questions/a-estershydrolysis.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../estermenu.html#top">To the esters menu</a>
<a href="../../orgpropsmenu.html#top">To the menu of other organic compounds</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>