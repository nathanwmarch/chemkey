<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Phenylamine (Aniline) as a Primary Amine | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A summary of the reactions of phenylamine (aniline) in which it acts as a fairly straightforward primary amine">
<meta name="keywords" content="phenylamine, aminobenzene, aniline, amine, amines, base, basic, properties, acylation, ethanoylation, acid, acyl, chloride, anhydride">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Phenylamine (Aniline) as a Primary Amine</h1>

<div class="text-block">
<p>This page looks at reactions of phenylamine (also known as aniline or aminobenzene) where it behaves as a fairly straightforward primary amine. It explains why phenylamine is a weaker base than other primary amines, and summarises its reactions with acyl chlorides (acid chlorides), acid anhydrides and halogenoalkanes (haloalkanes or alkyl halides).</p>
<p>Before you read each section on this page, you should follow the link to the corresponding page about aliphatic amines (those not based on benzene rings). In most cases, the reactions are the same, and this page only really looks in detail at the differences in the phenylamine case.</p>
</div>

<h2>Phenylamine as a Base</h2>


<div class="note">
<p>Important: First read the page about the <a href="../amines/base.html#top">basic properties of amines</a> to give you all the detailed explanations about amines in general.</p>
</div>

<div class="text-block">
<p>Amines are bases because the lone pair of electrons on the nitrogen atom can accept a hydrogen ion – in other words, for exactly the same reason that ammonia is a base.</p>
<p>With phenylamine, the only difference is that it is a much weaker base than ammonia or an amine like ethylamine – for reasons that we will explore later.</p>
</div>

<h3>The Reaction of Phenylamine With Acids</h3>

<div class="text-block">
<p>Phenylamine reacts with acids like hydrochloric acid in exactly the same way as any other amine. Despite the fact that the phenylamine is only a very weak base, with a strong acid like hydrochloric acid the reaction is completely straightforward.</p>
<p>Phenylamine is only very slightly soluble in water, but dissolves freely in dilute hydrochloric acid. A solution of a salt is formed – phenylammonium chloride.</p>
<p>If you just want to show the formation of the salt, you could write:</p>
</div>

<div class="block-formula">
\text{C}_6\text{H}_5\text{NH}_2 + \text{HCl} \longrightarrow \text{C}_6\text{H}_5{\text{NH}_3}^+\text{Cl}^-
</div>

<div class="text-block">
<p> or if you want to emphasise the fact that the phenylamine is acting as a base, you could most simply use:</p>
</div>

<div class="block-formula">
\text{C}_6\text{H}_5\text{NH}_{2(l)} + \text{H}^+_{(aq)} \longrightarrow \text{C}_6\text{H}_5{\text{NH}_3}^+_{(aq)}
</div>

<div class="note">
<p>Note: I have deliberately written these equations without stressing the benzene ring to emphasise the similarity between the phenylamine reaction and that of other amines.</p>
</div>

<h4>Getting the phenylamine back from its salt</h4>

<div class="text-block">
<p>To get the phenylamine back from the phenylammonium ion present in the salt, all you have to do is to take the hydrogen ion away again. You can do that by adding any stronger base.</p>
<p>Normally, you would choose sodium hydroxide solution.</p>
</div>

<div class="block-formula">
\text{C}_6\text{H}_5{\text{NH}_3}^+_{(aq)} + {}^-\text{OH}_{(aq)} \longrightarrow \text{C}_6\text{H}_5\text{NH}_{2(l)} + \text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>The phenylamine is formed first as an off-white emulsion – tiny droplets of phenylamine scattered throughout the water. This then settles out to give an oily bottom layer of phenylamine under the aqueous layer.</p>
</div>

<h3>The Reaction of Phenylamine With Water</h3>

<div class="text-block">
<p>This is where it is possible to tell that phenylamine is a much weaker base than ammonia and the aliphatic amines like methylamine and ethylamine.</p>
<p>Phenylamine reacts reversibly with water to give phenylammonium ions and hydroxide ions.</p>
</div>

<div class="block-formula">
\text{C}_6\text{H}_5\text{NH}_{2(aq)} + \text{H}_2\text{O}_{(l)} \xrightleftharpoons{} \text{C}_6\text{H}_5{\text{NH}_3}^+_{(aq)} + {}^-\text{OH}_{(aq)}
</div>

<div class="text-block">
<p>The position of equilibrium lies well to the left of the corresponding ammonia or aliphatic amine equilibria – which means that not many hydroxide ions are formed in the solution.</p>
<p>The effect of this is that the pH of a solution of phenylamine will be quite a bit lower than a solution of ammonia or one of the aliphatic amines of the same concentration. For example, a 0.1 M phenylamine solution has a pH of about 9 compared to a pH of about 11 for 0.1 M ammonia solution.</p>
</div>

<h4>Why is Phenylamine Such a Weak Base?</h4>

<div class="text-block">
<p>Amines are bases because they pick up hydrogen ions on the lone pair on the nitrogen atom. In phenylamine, the attractiveness of the lone pair is lessened because of the way it interacts with the ring electrons.</p>
<p>The lone pair on the nitrogen touches the delocalised ring electrons</p>
</div>

<div class="image-container"><img src="phaminedeloc1.gif"></div>

<div class="text-block">
<p> and becomes delocalised with them:</p>
</div>

<div class="image-container"><img src="phaminedeloc2.gif"></div>

<div class="note">
<p>Note: If you don't understand these diagrams, you will need to read about <a href="../../basicorg/bonding/benzene2.html#top">bonding in benzene</a> in order to make sense of them.</p>
</div>
 
<div class="text-block">
<p>That means that the lone pair is no longer fully available to combine with hydrogen ions. The nitrogen is still the most electronegative atom in the molecule, and so the delocalised electrons will be attracted towards it, but the electron density around the nitrogen is nothing like it is in, say, an ammonia molecule.</p>
<p>The other problem is that if the lone pair is used to join to a hydrogen ion, it is no longer available to contribute to the delocalisation. That means that the delocalisation would have to be disrupted if the phenylamine acts as a base. Delocalisation makes molecules more stable, and so disrupting the delocalisation costs energy and won't happen easily.</p>
<p>Taken together – the lack of intense charge around the nitrogen, and the need to break some delocalisation – means that phenylamine is a very weak base indeed.</p>
</div>

<h2>The Acylation of Phenylamine</h2>

<h3>The Reactions With Acyl Chlorides and Acid Anhydrides</h3>

<div class="text-block">
<p>These are reactions in which the phenylamine acts as a nucleophile. There is no essential difference between these reactions and the same reactions involving any other primary amine. You will find a summary of the reactions below, but all the detailed explanations are on other pages.</p>
</div>

<div class="note">
<p>Important: For detailed explanations, read the pages about the <a href="../acylchlorides/nitrogen.html#top">reactions of acyl chlorides</a> and <a href="../anhydrides/nitrogen.html#top">reactions of acid anhydrides</a> with nitrogen compounds. It might pay you to read the acid anhydride page first because this also summarises the acyl chloride reactions. It could save you having to read the acyl chloride page at all.</p>
<p>If you want the mechanism for the reaction with an acyl chloride, you will find a link from the acyl chloride page. Mechanisms for acid anhydrides aren't required by any UK A-level (or equivalent) syllabus, so aren't covered anywhere on the site.</p>
<p>Take your time over this – it isn't easy stuff! It might be a good idea to check your <a href="../../syllabuses.html#top">syllabus</a> before you get too bogged down.Follow this link if you are a studying a UK-based syllabus and haven't got a copy of it.</p>
</div>

<div class="text-block">
<p>We'll take ethanoyl chloride as a typical acyl chloride, and ethanoic anhydride as a typical acid anhydride. The important product of the reaction of phenylamine with either of these is the same.</p>
<p>Phenylamine reacts vigorously in the cold with ethanoyl chloride to give a mixture of solid products – ideally white, but usually stained brownish. A mixture of N-phenylethanamide (old name: acetanilide) and phenylammonium chloride is formed.</p>
<p>The overall equation for the reaction is:</p>
</div>

<div class="block-formula full-width">
\text{CH}_3\text{COCl} + 2\text{C}_6\text{H}_5\text{NH}_2 \longrightarrow \text{CH}_3\text{CONHC}_6\text{H}_5 + \text{C}_6\text{H}_5{\text{NH}_3}^+\text{Cl}^-
</div>

<div class="text-block">
<p>With ethanoic anhydride, heat is needed. In this case, the products are a mixture of N-phenylethanamide and phenylammonium ethanoate.</p>
</div>

<div class="block-formula full-width">
(\text{CH}_3\text{CO})_2\text{O} + 2\text{C}_6\text{H}_5\text{NH}_2 \longrightarrow \text{CH}_3\text{CONHC}_6\text{H}_5 + \text{CH}_3\text{COO}^-{}^+\text{NH}_3\text{C}_6\text{H}_5
</div>

<div class="note">
<p>Note: Don't be confused by the fact that the two salts (phenylammonium chloride and phenylammonium ethanoate) are written differently. Ethanoates are always written with the ethanoic acid bit first. That means having to reverse the way you write the phenylammonium ion to keep the charges close together in the formula.</p>
</div>

<div class="text-block">
<p>The main product molecule (the N-phenylethanamide) is often drawn looking like this:</p>
</div>

<div class="image-container"><img src="npheneth.gif"></div>

<div class="text-block">
<p>If you stop and think about it, this is obviously the same molecule as in the equation above, but it stresses the phenylamine part of it much more.</p>
<p>Looking at it this way, notice that one of the hydrogens of the -NH<sub>2</sub> group has been replaced by an acyl group – an alkyl group attached to a carbon-oxygen double bond.</p>
<p>You can say that the phenylamine has been acylated or has undergone acylation.</p>
<p>Because of the nature of this particular acyl group, it is also described as ethanoylation. The hydrogen is being replaced by an ethanoyl group, CH<sub>3</sub>CO-.</p>
</div>

<div class="note">
<p>Taking chemistry further: This is a useful reaction in the case of phenylamine. The -NH<sub>2</sub> group in phenylamine has major effects on the properties of the benzene ring. It makes it much more reactive than in benzene itself, and also makes it prone to oxidation.</p>
<p>For example, if you wanted to nitrate phenylamine, you would get several nitro groups substituted around the ring and a lot of unwanted oxidation because nitric acid is a strong oxidising agent.</p>
<p>To just substitute one nitro group and stop the oxidation, you first ethanoylate the -NH<sub>2</sub> group which reduces its effect on the ring considerably. After nitration, you can remove the ethanoyl group by simply warming with sodium hydroxide solution. The N-substituted amide is hydrolysed just like any other amide under alkaline conditions.</p>
<p>If you are interested, the alkaline <a href="../amides/hydrolysis.html#top">hydrolysis of amides</a> is covered on another page.</p>
</div>

<h2>The Reaction of Phenylamine With Halogenoalkanes</h2>

<div class="text-block">
<p>This is another reaction of phenylamine as a nucleophile, and again there is no essential difference between its reactions and those of aliphatic amines.</p>
</div>

<div class="note">
<p>Important: You should read the first half of the page about <a href="../amines/nucleophile.html#top">amines as nucleophiles</a> to give you explanations about amines in general – and possibly also the further link on that page back to reactions of halogenoalkanes for even more detail.</p>
<p>Again, take your time over this. It isn't easy, particularly if you have to go beyond the formation of the secondary amine.</p>
</div>

<div class="text-block">
<p>Taking bromoethane as a typical halogenoalkane, the reaction with phenylamine happens in the same series of complicated steps as with any other amine.</p>
<p>We'll just look at the first step.</p>
<p>On heating, the bromoethane and phenylamine react to give a mixture of a salt of a secondary amine and some free secondary amine. In this case, you would first get N-ethylphenylammonium bromide:</p>
</div>

<div class="image-container"><img src="alkyl1.gif"></div>

<div class="text-block">
<p>but this would instantly be followed by a reversible reaction in which some unreacted phenylamine would take a hydrogen ion from the salt to give some free secondary amine: N-ethylphenylamine.</p>
</div>

<div class="image-container"><img src="alkyl2.gif"></div>

<div class="text-block">
<p>The reaction wouldn't stop there. You will get further reactions to produce a tertiary amine and its salt, and eventually a quaternary ammonium compound. If you want to explore this further, refer to the last link just up the page, and trace the sequence of equations through using phenylamine rather than ethylamine.</p>
</div>

<div class="note">
<p>Note: You are so unlikely to be asked to do this at this level (UK A-level or equivalent) that I am not going to go through it. All it would do is make chemistry look scary! If you want to trace the whole reaction through, make sure that you understand what is going on at each stage by following up the leads I have given you to other pages on the site – and then work it out for yourself. If you can do it – well done! If you can't, don't worry about it – life's too short!</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-anilineamine.pdf" target="_blank">Questions on the reactions of phenylamine as a primary amine</a>
<a href="../questions/a-anilineamine.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../anilinemenu.html#top">To the phenylamine menu</a>
<a href="../../orgpropsmenu.html#top">To the menu of other organic compounds</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>