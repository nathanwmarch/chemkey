<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to Arenes (Aromatic Hydrocarbons) | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Background on the arenes, including their physical properties">
<meta name="keywords" content="arenes, arene, aromatic, hydrocarbons, hydrocarbon, structure, physical, properties, melting, boiling, solubility, benzene, methylbenzene, toluene">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to Arenes (Aromatic Hydrocarbons)</h1>

<div class="text-block">
<p>This page looks at the structures and physical properties of the simplest arenes (benzene and methylbenzene), together with a very brief introduction to their reactivity. Much of this is covered in detail elsewhere on the site – in sections on bonding and mechanisms, for example. You will find links to all of these pages.</p>
</div>

<h2>What are Arenes?</h2>

<div class="text-block">
<p>Arenes are aromatic hydrocarbons. The term "aromatic" originally referred to their pleasant smells, but now implies a particular sort of delocalised bonding (see below).</p>
<p>The arenes you are likely to meet at this level are based on benzene rings. The simplest of them is benzene itself, C<sub>6</sub>H<sub>6</sub>. The next simplest is methylbenzene (old name: toluene) which has one of the hydrogen atoms attached to the ring replaced by a methyl group – C<sub>6</sub>H<sub>5</sub>CH<sub>3</sub>.</p>
</div>

<h3>The Structure of Benzene</h3>

<div class="text-block">
<p>The structure of benzene is covered in detail in two pages in the organic bonding section of this site. It is important to understand these thoroughly to make sense of benzene and methylbenzene chemistry. Unless you have read these pages recently, you should spend some time on them now before you go any further on this page.</p>
</div>

<div class="note">
<p>Note: You will find two pages dealing with the bonding in benzene. One deals with the <a href="../../basicorg/bonding/benzene1.html#top">Kelulé structure</a>, and the other with the <a href="../../basicorg/bonding/benzene2.html#top">modern delocalised structure</a>.</p>
<p>You should read both of these pages starting with the one about the Kelulé structure. You will find a link to the second page at the bottom of the first one.</p>
<p>This is likely to take you some time because you will probably have to follow up other links as well in order to fully understand the second page. Don't try to short-cut this.</p>
</div>

<div class="text-block">
<p>What you need to understand about benzene is:</p>
</div>

<ul>
<li>Benzene, C<sub>6</sub>H<sub>6</sub>, is a planar molecule containing a ring of six carbon atoms each with a hydrogen atom attached.<br>The six carbon atoms form a perfectly regular hexagon. All the carbon-carbon bonds have exactly the same lengths – somewhere between single and double bonds.</li>
<li>There are delocalised electrons above and below the plane of the ring.</li>
</ul>

<div class="image-container"><img src="benzenedeloc.gif"></div>

<div class="text-block">
<p>This diagram shows one of the molecular orbitals containing two of the delocalised electrons, which may be found anywhere within the two "doughnuts". The other molecular orbitals are almost never drawn.</p>

<ul>
<li>The presence of the delocalised electrons makes benzene particularly stable.</li>
<li>Benzene resists addition reactions because that would involve breaking the delocalisation and losing that stability.
<div class="image-container"><img src="benzene.gif"></div></li>
<li>Benzene is represented by this symbol, where the circle represents the delocalised electrons, and each corner of the hexagon has a carbon atom with a hydrogen attached.</li>
</div>

<h3>The Structure of Methylbenzene (Toluene)</h3>

<div class="text-block">
<p>Methylbenzene just has a methyl group attached to the benzene ring – replacing one of the hydrogen atoms.</p>
</div>

<div class="image-container"><img src="toluene.gif"></div>

<div class="text-block">
<p>Attached groups are often drawn at the top of the ring, but you may occasionally find them drawn in other places with the ring rotated.</p>
</div>

<div class="note">
<p>Note: If you have to count up the hydrogens in a diagram of this kind, don't forget that there isn't a hydrogen atom at any corner of the hexagon where there is something else attached. The molecular formula of methylbenzene, for example, is C<sub>7</sub>H<sub>8</sub>. Check that you get that answer from this diagram!</p>
</div>

<h2>Physical Properties</h2>

<h3>Boiling Points</h3>

<div class="text-block">
<p>In benzene, the only attractions between neighbouring molecules are van der Waals dispersion forces. There is no permanent dipole on the molecule.</p>
<p>Benzene boils at 80°C – rather higher than other hydrocarbons of similar molecular size (pentane and hexane, for example). This is presumably due to the ease with which temporary dipoles can be set up involving the delocalised electrons.</p>
</div>

<div class="note">
<p>Note: If you aren't happy about <a href="../../atoms/bonding/vdw.html#top">van der Waals dispersion forces</a> then you should follow this link before you go on.</p>
</div>

<div class="text-block">
<p>Methylbenzene boils at 111°C. It is a bigger molecule and so the van der Waals dispersion forces will be bigger.</p>
<p>Methylbenzene also has a small permanent dipole, so there will be dipole-dipole attractions as well as dispersion forces. The dipole is due to the CH<sub>3</sub> group's tendency to "push" electrons away from itself. This also affects the reactivity of methylbenzene (see below).</p>
</div>

<h3>Melting Points</h3>

<div class="text-block">
<p>You might have expected that methylbenzene's melting point would be higher than benzene's as well, but it isn't – it is much lower! Benzene melts at 5.5°C; methylbenzene at -95°C.</p>
<p>Molecules must pack efficiently in the solid if they are to make best use of their intermolecular forces. Benzene is a tidy, symmetrical molecule and packs very efficiently. The methyl group sticking out in methylbenzene tends to disrupt the closeness of the packing. If the molecules aren't as closely packed, the intermolecular forces don't work as well and so the melting point falls.</p>
</div>

<h3>Solubility in Water</h3>

<div class="text-block">
<p>The arenes are insoluble in water.</p>
<p>Benzene is quite large compared with a water molecule. In order for benzene to dissolve it would have to break lots of existing hydrogen bonds between water molecules. You also have to break the quite strong van der Waals dispersion forces between benzene molecules. Both of these cost energy.</p>
<p>The only new forces between the benzene and the water would be van der Waals dispersion forces. These aren't as strong as hydrogen bonds (or the original dispersion forces in the benzene), and so you wouldn't get much energy released when they form.</p>
<p>It simply isn't energetically profitable for benzene to dissolve in water. It would, of course, be even worse for larger arene molecules.</p>
</div>

<h2>Reactivity</h2>

<h3>Benzene</h3>

<div class="text-block">
<p>It has already been pointed out above that benzene is resistant to addition reactions. Adding something new to the ring would need you to use some of the delocalised electrons to form bonds with whatever you are adding. That results in a major loss of stability as the delocalisation is broken.</p>
<p>Instead, benzene mainly undergoes substitution reactions – replacing one or more of the hydrogen atoms by something new. That leaves the delocalised electrons as they were.</p>
</div>

<div class="note">
<p>Note: The delocalisation is, in fact, broken during a substitution reaction, but reforms at the end. If you are interested in the <a href="../../mechanisms/elsubmenu.html#top">mechanisms for benzene's substitution reactions</a> you could follow this link.</p>
</div>

<h3>Methylbenzene</h3>

<div class="text-block">
<p>You have to consider the reactivity of something like methylbenzene in two distinct bits:</p>
</div>

<div class="image-container"><img src="toluene2.gif"></div>

<div class="text-block">
<p>For example, if you explore other pages in this section, you will find that alkyl groups attached to a benzene ring are oxidised by alkaline potassium manganate(VII) solution. This doesn't happen in the absence of the benzene ring.</p>
<p>The tendency of the CH<sub>3</sub> group to "push" electrons away from itself also has an effect on the ring, making methylbenzene react more quickly than benzene itself. You will find this explored in other pages in this section as well.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-arenesintro.pdf" target="_blank">Questions on the introduction to arenes</a>
<a href="../questions/a-arenesintro.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../arenesmenu.html#top">To the arenes menu</a>
<a href="../../orgpropsmenu.html#top">To the menu of other organic compounds</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>