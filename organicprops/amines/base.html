<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Amines as Bases | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="The reactions of amines as bases">
<meta name="keywords" content="amines, amine, base, bases, basic">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Amines as Bases</h1>

<div class="text-block">
<p>This page looks at the reactions of amines as bases. Their basic properties include the reactions with dilute acids, water and copper(II) ions.</p>
<p>It only deals with amines where the functional group is not attached directly to a benzene ring. Aromatic amines such as phenylamine (aniline) are much weaker bases than the amines discussed on this page and are dealt with separately on a page specifically about phenylamine. If you are interested in phenylamine, read this page first and then follow the link at the bottom.</p>
</div>

<h2>The Basic Properties of Amines</h2>

<div class="text-block">
<p>We are going to have to use two different definitions of the term "base" in this page.</p>
</div>

<h5>A base is:</h5>

<ul>
<li>a substance which combines with hydrogen ions.<br>This is the Bronsted-Lowry theory.</li>
<li>an electron pair donor.<br>This is the Lewis theory.</li>
</ul>

<div class="note">
<p>Note: If you aren't familiar with either of these terms, you should follow this link to a page about <a href="../../physical/acidbaseeqia/theories.html#top">theories of acids and bases</a>.</p>
</div>

<div class="text-block">
<p>The easiest way of looking at the basic properties of amines is to think of an amine as a modified ammonia molecule. In an amine, one or more of the hydrogen atoms in ammonia has been replaced by a hydrocarbon group.</p>
<p>Replacing the hydrogens still leaves the lone pair on the nitrogen unchanged – and it is the lone pair on the nitrogen that gives ammonia its basic properties. Amines will therefore behave much the same as ammonia in all cases where the lone pair is involved.</p>
</div>

<h3>The Reactions of Amines With Acids</h3>

<div class="text-block">
<p>These are most easily considered using the Bronsted-Lowry theory of acids and bases – the base is a hydrogen ion acceptor. We'll do a straight comparison between amines and the familiar ammonia reactions.</p>
</div>

<h4>A reminder about the ammonia reactions</h4>

<div class="text-block">
<p>Ammonia reacts with acids to produce ammonium ions. The ammonia molecule picks up a hydrogen ion from the acid and attaches it to the lone pair on the nitrogen.</p>
</div>

<div class="image-container"><img src="ammonium.gif"></div>

<div class="text-block">
<p>If the reaction is in solution in water (using a dilute acid), the ammonia takes a hydrogen ion (a proton) from a hydroxonium ion. (Remember that hydrogen ions present in solutions of acids in water are carried on water molecules as hydroxonium ions, H<sub>3</sub>O<sup>+</sup>.)</p>
</div>

<div class="block-formula">
\text{NH}_{3(aq)} + \text{H}_3\text{O}^+_{(aq)} \longrightarrow {\text{NH}_4}^+_{(aq)} + \text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>If the acid was hydrochloric acid, for example, you would end up with a solution containing ammonium chloride – the chloride ions, of course, coming from the hydrochloric acid.</p>
<p>You could also write this last equation as:</p>
</div>

<div class="block-formula">
\text{NH}_{3(aq)} + \text{H}^+_{(aq)} \longrightarrow {\text{NH}_4}^+_{(aq)}
</div>

<div class="image-container"><img src="nh3h3oeqn2.gif"></div>

<div class="text-block">
<p>But if you do it this way, you must include the state symbols. If you write H<sup>+</sup> on its own, it implies an unattached hydrogen ion – a proton. Such things don't exist on their own in solution in water.</p>
<p>If the reaction is happening in the gas state, the ammonia accepts a proton directly from the hydrogen chloride:</p>
</div>

<div class="block-formula">
\text{NH}_{3(aq)} + \text{HCl}_{(g)} \longrightarrow \text{NH}_4\text{Cl}_{(s)}
</div>

<div class="text-block">
<p>This time you produce clouds of white solid ammonium chloride.</p>
</div>

<h4>The corresponding reactions with amines</h4>

<div class="text-block">
<p>The nitrogen lone pair behaves exactly the same. The fact that one (or more) of the hydrogens in the ammonia has been replaced by a hydrocarbon group makes no difference.</p>
<p>For example, with ethylamine:</p>
<p>If the reaction is done in solution, the amine takes a hydrogen ion from a hydroxonium ion and forms an ethylammonium ion.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{NH}_{2(aq)} + \text{H}_3\text{O}^+_{(aq)} \longrightarrow {\text{CH}_3\text{CH}_2\text{NH}_3}^+_{(aq)} + \text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>Or:</p>
</div>

<div class="block-formula full-width">
\text{CH}_3\text{CH}_2\text{NH}_{2(aq)} + \text{H}^+_{(aq)} \longrightarrow {\text{CH}_3\text{CH}_2\text{NH}_3}^+_{(aq)}
</div>

<div class="text-block">
<p>The solution would contain ethylammonium chloride or sulfate or whatever.</p>
<p>Alternatively, the amine will react with hydrogen chloride in the gas state to produce the same sort of white smoke as ammonia did – but this time of ethylammonium chloride.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{NH}_{2(g)} + \text{HCl}_{(g)} \longrightarrow \text{CH}_3\text{CH}_2\text{NH}_3\text{Cl}_{(s)}
</div>

<div class="image-container"><img src="aminehclg.gif"></div>

<div class="text-block">
<p>These examples have involved a primary amine. It would make no real difference if you used a secondary or tertiary one. The equations would just look more complicated.</p>
<p>The product ions from diethylamine and triethylamine would be diethylammonium ions and triethylammonium ions respectively.</p>
</div>

<div class="image-container"><img src="sectertions.gif"></div>

<h3>The Reactions of Amines With Water</h3>

<div class="text-block">
<p>Again, it is easiest to use the Bronsted-Lowry theory and, again, it is useful to do a straight comparison with ammonia.</p>
</div>

<h4>A reminder about the ammonia reaction with water</h4>

<div class="text-block">
<p>Ammonia is a weak base and takes a hydrogen ion from a water molecule to produce ammonium ions and hydroxide ions.</p>
<p>However, the ammonia is only a weak base, and doesn't hang on to the hydrogen ion very successfully. The reaction is reversible, with the great majority of the ammonia at any one time present as free ammonia rather than ammonium ions.</p>
</div>

<div class="block-formula">
\text{NH}_{3(aq)} + \text{H}_2\text{O}_{(l)} \xrightleftharpoons{} {\text{NH}_4}^+_{(aq)} + {}^-\text{OH}_{(aq)}
</div>

<div class="text-block">
<p>The presence of the hydroxide ions from this reaction makes the solution alkaline.</p>
</div>

<h4>The corresponding reaction with amines</h4>

<div class="text-block">
<p>The amine still contains the nitrogen lone pair, and does exactly the same thing.</p>
<p>For example, with ethylamine, you get ethylammonium ions and hydroxide ions produced.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{NH}_{2(aq)} + \text{H}_2\text{O}_{(l)} \xrightleftharpoons{} \text{CH}_3\text{CH}_2{\text{NH}_3}^+_{(aq)} + {}^-\text{OH}_{(aq)}
</div>

<div class="text-block">
<p>There is, however, a difference in the position of equilibrium. Amines are usually stronger bases than ammonia. (There are exceptions to this, though – particularly if the amine group is attached directly to a benzene ring.)</p>
</div>

<div class="note">
<p>Note: If you want to explore some of the reasons for the <a href="../../basicorg/acidbase/bases.html#top">relative strengths of ammonia and the amines as bases</a> you could follow this link.</p>
<p>UK A-level syllabuses are only concerned with the relative strengths of ammonia and the primary amines, so that is all you will find on that page.</p>
</div>

<h3>The Reactions of Amines With Copper(II) Ions</h3>

<div class="text-block">
<p>Just like ammonia, amines react with copper(II) ions in two separate stages. In the first step, we can go on using the Bronsted-Lowry theory (that a base is a hydrogen ion acceptor). The second stage of the reaction can only be explained in terms of the Lewis theory (that a base is an electron pair donor).</p>
</div>

<h4>The reaction between ammonia and copper(II) ions</h4>

<div class="text-block">
<p>Copper(II) sulfate solution, for example, contains the blue hexaaquacopper(II) ion – [Cu(H<sub>2</sub>O)<sub>6</sub>]<sup>2+</sup>.</p>
<p>In the first stage of the reaction, the ammonia acts as a Bronsted-Lowry base. With a small amount of ammonia solution, hydrogen ions are pulled off two water molecules in the hexaaqua ion.</p>
<p>This produces a neutral complex – one carrying no charge. If you remove two positively charged hydrogen ions from a 2+ ion, then obviously there isn't going to be any charge left on the ion.</p>
<p>Because of the lack of charge, the neutral complex isn't soluble in water, and so you get a pale blue precipitate.</p>
</div>

<div class="block-formula">
[\text{Cu}(\text{H}_2\text{O})_6]^{2+} + 2\text{NH}_3 \xrightleftharpoons{} [\text{Cu}(\text{H}_2\text{O})_4(\text{OH})_2] + 2{\text{NH}_4}^+
</div>

<div class="text-block">
<p>This precipitate is often written as Cu(OH)<sub>2</sub> and called copper(II) hydroxide. The reaction is reversible because ammonia is only a weak base.</p>
<p>That precipitate dissolves if you add an excess of ammonia solution, giving a deep blue solution.</p>
<p>The ammonia replaces four of the water molecules around the copper to give tetraamminediaquacopper(II) ions. The ammonia uses its lone pair to form a co-ordinate covalent bond (dative covalent bond) with the copper. It is acting as an electron pair donor – a Lewis base.</p>
</div>

<div class="block-formula full-width">
[\text{Cu}(\text{H}_2\text{O})_6]^{2+} + 4\text{NH}_3 \xrightleftharpoons{} [\text{Cu}(\text{NH}_3)_4(\text{H}_2\text{O})_2]^{2+} + 4\text{H}_2\text{O}
</div>

<div class="note">
<p>Note: You might wonder why this second equation is given starting from the original hexaaqua ion rather than the neutral complex. Explaining why the precipitate redissolves is quite complicated. You will find a lot more explanations about the <a href="../../inorganic/complexions/aquanh3.html#top">reactions between hexaaqua ions and ammonia solution</a> in the inorganic section of this site if you are interested.</p>
<p>(Important: The inorganic section describes ammonia acting as a ligand in the second stage of the reaction. It is acting as a ligand because it has a lone pair of electrons – in other words, because it is a Lewis base.)</p>
</div>

<div class="text-block">
<p>The colour changes are:</p>
</div>

<div class="image-container"><img src="cunh3cols.gif"></div>

<h4>The corresponding reaction with amines</h4>

<div class="text-block">
<p>The small primary amines behave in exactly the same way as ammonia. There will, however, be slight differences in the shades of blue that you get during the reactions.</p>
<p>Taking methylamine as an example:</p>
<p>With a small amount of methylamine solution you will get a pale blue precipitate of the same neutral complex as with ammonia. All that is happening is that the methylamine is pulling hydrogen ions off the attached water molecules.</p>
</div>

<div class="block-formula full-width">
[\text{Cu}(\text{H}_2\text{O})_6]^{2+} + 2\text{CH}_3\text{NH}_2 \xrightleftharpoons{} [\text{Cu}(\text{H}_2\text{O})_4(\text{OH})_2] + 2\text{CH}_3{\text{NH}_3}^+
</div>

<div class="text-block">
<p>With more methylamine solution the precipitate redissolves to give a deep blue solution – just as in the ammonia case. The amine replaces four of the water molecules around the copper.</p>
</div>

<div class="block-formula full-width">
[\text{Cu}(\text{H}_2\text{O})_6]^{2+} + 4\text{CH}_3\text{NH}_2 \xrightleftharpoons{} [\text{Cu}(\text{CH}_3\text{NH}_2)_4(\text{H}_2\text{O})_2]^{2+} + 4\text{H}_2\text{O}
</div>

<div class="text-block">
<p>As the amines get bigger and more bulky, the formula of the final product may change – simply because it is impossible to fit four large amine molecules and two water molecules around the copper atom.</p>
</div> 

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-aminesbase.pdf" target="_blank">Questions on amines as bases</a>
<a href="../questions/a-aminesbase.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../aniline/amine.html#top">To the section on phenylamine</a>
<a href="../aminemenu.html#top">To the amines menu</a>
<a href="../../orgpropsmenu.html#top">To the menu of other organic compounds</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>