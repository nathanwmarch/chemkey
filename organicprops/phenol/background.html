<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to Phenol | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Background on phenol, including its physical properties">
<meta name="keywords" content="phenol, structure, physical, properties, melting, boiling, solubility">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to Phenol</h1>

<div class="text-block">
<p>This page looks at the structure and physical properties of phenol (very old name: carbolic acid). Phenol is the simplest member of a family of compounds in which an -OH group is attached directly to a benzene ring. Phenol itself is the only one of the family that you are likely to need to know about for UK A-level purposes.</p>
</div>

<h2>The Structure of Phenol</h2>

<div class="text-block">
<p>The simplest way to draw the structure of phenol is:</p>
</div>

<div class="image-container"><img src="phenol.gif"></div>

<div class="text-block">
<p>but to understand phenol properly, you need to dig a bit deeper than this.</p>
</div>

<div class="note">
<p>Warning! You need to understand about the <a href="../../basicorg/bonding/benzene2.html#top">bonding in benzene</a> in order to make sense of this next bit.</p>
</div>

<div class="text-block">
<p>There is an interaction between the delocalised electrons in the benzene ring and one of the lone pairs on the oxygen atom. This has an important effect on both the properties of the ring and of the -OH group.</p>
<p>One of the lone pairs on the oxygen overlaps with the delocalised ring electron system</p>
</div>

<div class="image-container"><img src="phenoldeloc1.gif"></div>

<div class="text-block">
<p> giving a structure rather like this:</p>
</div>

<div class="image-container"><img src="phenoldeloc2.gif"></div>

<div class="text-block">
<p>The donation of the oxygen's lone pair into the ring system increases the electron density around the ring. That makes the ring much more reactive than it is in benzene itself. That is explored in another page in this phenol section.</p>
<p>It also helps to make the -OH group's hydrogen a lot more acidic than it is in alcohols. That will also be explored elsewhere in this section.</p>
</div>

<h2>Physical Properties</h2>

<div class="text-block">
<p>Pure phenol is a white crystalline solid, smelling of disinfectant. It has to be handled with great care because it causes immediate white blistering to the skin. The crystals are often rather wet and discoloured.</p>
</div>

<h3>Melting and Boiling Points</h3>

<div class="text-block">
<p>It is useful to compare phenol's melting and boiling points with those of methylbenzene (toluene). Both molecules contain the same number of electrons and are a very similar shape. That means that the intermolecular attractions due to van der Waals dispersion forces are going to be very similar.</p>
</div>

<div class="note">
<p>Note: If you aren't happy about <a href="../../atoms/bonding/vdw.html#top">intermolecular forces</a> (including van der Waals dispersion forces and hydrogen bonds) then you really ought to follow this link before you go on. This section won't make much sense to you if you aren't familiar with the various sorts of intermolecular forces.</p>
</div>

<table class="data-table">
<tbody><tr><th></th><th>melting point<br>/ °C</th><th>boiling point<br>/ °C</th></tr>
<tr><td>C<sub>6</sub>H<sub>5</sub>OH</td><td>40 – 43</td><td>182</td></tr>
<tr><td>C<sub>6</sub>H<sub>5</sub>CH<sub>3</sub></td><td>-95.0</td><td>111</td></tr>
</tbody></table>
</center>

<div class="note">
<p>Note: The melting point of phenol is quoted variously by different sources within this 40 – 43°C range. I suspect (but don't know for sure) that the problem lies in the difficulty of getting the phenol absolutely dry.</p>
</div>

<div class="text-block">
<p>The reason for the higher values for phenol is in part due to permanent dipole-dipole attractions due to the electronegativity of the oxygen – but is mainly due to hydrogen bonding.</p>
<p>Hydrogen bonds can form between a lone pair on an oxygen on one molecule and the hydrogen on the -OH group of one of its neighbours.</p>
</div>

<h3>Solubility in Water</h3>

<div class="text-block">
<p>Phenol is moderately soluble in water – about 8 g of phenol will dissolve in 100 g of water.</p>
<p>If you try to dissolve more than this, you get two layers of liquid. The top layer is a solution of phenol in water, and the bottom one a solution of water in phenol. The solubility behaviour of phenol and water is complicated, and beyond UK A-level.</p>
<p>Phenol is somewhat soluble in water because of its ability to form hydrogen bonds with the water.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-phenolintro.pdf" target="_blank">Questions on the introduction to phenol</a>
<a href="../questions/a-phenolintro.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../phenolmenu.html#top">To the phenol menu</a>
<a href="../../orgpropsmenu.html#top">To the menu of other organic compounds</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>