<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Effect of Changing Conditions in Enzyme Catalysis | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of the effect of substrate concentration, temperature and pH on enzymes">
<meta name="keywords" content="protein, proteins, structure, structures, enzyme, enzymes, catalyst, catalysis, active site, concentration, temperature, ph, denature">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Effect of Changing Conditions in Enzyme Catalysis</h1>

<div class="text-block">
<p>This page looks at the effect of changing substrate concentration, temperature and pH on reactions involving enzymes. It follows on from a page describing in simple terms how enzymes function as catalysts. Please remember that this series of pages is written for 16 – 18 year old chemistry students. If you want something more advanced, you are looking in the wrong place.</p>
</div>

<div class="note">
<p>Note: This page assumes that you have already read the page about <a href="enzymes.html#top">how proteins function as enzymes</a>. If you have come straight to this page via a search engine, you really ought to go back and read that page first. In fact, unless you already have a good knowledge of protein structure, you may have to go back further still to a page about <a href="proteinstruct.html#top">the structure of proteins</a>.</p>
</div>

<h2>The Effect of Substrate Concentration on the Rate of Enzyme-controlled Reactions</h2>

<div class="text-block">
<p>Remember that in biology or biochemistry, the reactant in an enzyme reaction is known as the "substrate".</p>
<p>What follows is a very brief and simple look at a very complicated topic. Anything beyond this is the stuff of biochemistry degree courses!</p>
</div>

<h3>A Reminder About the Effect of Concentration on Rate in Ordinary Chemical Reactions</h3>

<div class="text-block">
<p>If you have done any work on rates of reaction (especially if you have done orders of reaction), you will have come across cases where the rate of reaction is proportional to the concentration of a reactant, or perhaps to the square of its concentration.</p>
<p>You would discover this by changing the concentration of one of the reactants, keeping everything else constant, and measuring the initial rate of the reaction. If you measure the rate after the reaction has been going for a while, the concentration of the reactant(s) will have changed and that just complicates things. That's why initial rates are so useful – you know exactly how much you have of everything.</p>
<p>If you plotted a graph of initial reaction rate against the concentration of a reactant, then there are various possibilities depending on the relationship between the concentration and the rate.</p>
</div>

<h4>If the rate is independent of the concentration</h4>

<div class="text-block">
<p>This is called a zero order reaction.</p>
</div>

<div class="image-container"><img src="order0.gif"></div>

<h4>If the rate is proportional to the concentration</h4>

<div class="text-block">
<p>This is called a first order reaction.</p>
</div>

<div class="image-container"><img src="order1.gif"></div>

<h4>If the rate is proportional to some power of the concentration greater than one</h4>

<div class="text-block">
<p>In this case, you get a curve. If the rate was proportional to the square of the concentration, that's a second order reaction.</p>
</div>

<div class="image-container"><img src="orderx.gif"></div>

<div class="text-block">
<p>With reactions controlled by enzymes, you get a completely different type of graph.</p>
</div>

<h3>Plotting Initial Rates of Enzyme-controlled Reactions Against Substrate Concentration</h3>

<div class="text-block">
<p>The graph for enzyme controlled reactions looks like this:</p>
</div>

<div class="image-container"><img src="enzymerate1.gif"></div>

<div class="text-block">
<p>Two minor things to notice before we discuss it</p>
</div>

<ul>
<li>Biochemists talk about a reaction velocity instead of a reaction rate. If you have done any physics, you will know that this is a complete misuse of the word "velocity"! But that's what you will find in biochemistry sources, so that's what we will have to use.</li>
<li>In chemistry, rates are normally measured in terms of rate of change of concentration, with units like mol dm<sup>-3</sup> s<sup>-1</sup> (moles per cubic decimetre per second). Biochemists often quote it in terms of the number of molecules of substrate which a single molecule of enzyme is processing per unit time – per second, for example. It is easier to visualise, but involves a messy calculation to get there. That's not our problem for this topic!</li>
</ul>

<div class="text-block">
<p>So why is the graph the shape it is?</p>
<p>For very, very low substrate concentrations, the graph is almost a straight line – like the second chemistry rate graph above. In other words, at very, very low concentrations, the rate is proportional to the substrate concentration.</p>
<p>But as concentration increases, increasing the concentration more has less and less effect – and eventually the rate reaches a maximum. Increasing the concentration any more makes no difference to the rate of the reaction.</p>
<p>If you know about orders of reaction, the reaction has now become zero order with respect to the substrate.</p>
<p>The reason for this is actually fairly obvious if you think about it. After a certain concentration of substrate is reached, every enzyme molecule present in the mixture is working as fast as it can. If you increase the substrate concentration any more, there aren't any enzyme molecules free to help the extra substrate molecules to react.</p>
<p>Is this unique to enzyme-controlled reactions? No! It can happen in some ordinary chemistry cases as well, usually involving a solid catalyst working with gases. At very high gas pressures (in other words, very high concentrations of gas molecules), the surface of the catalyst can be completely full of gas molecules. If you increase the amount of gas any more, there isn't any available surface for it to stick to and react.</p>
</div>

<h3>V<sub>max</sub> and K<sub>M</sub></h3>

<div class="text-block">
<p>The maximum rate for a particular enzyme reaction is known as V<sub>max</sub>. (That's V for velocity – a bit confusing for chemists where V is almost always used for volume!)</p>
<p>This is easily measured by drawing a line on the graph:</p>
</div>

<div class="image-container"><img src="enzymerate2.gif"></div>

<div class="text-block">
<p>This is sometimes reported as a "turnover number", measured as the number of molecules of substrate processed by a single enzyme molecule per second, or per minute.</p>
<p>K<sub>M</sub> is known as the Michaelis constant or the Michaelis-Menten constant (for reasons which needn't concern us), and is a useful measure of the efficiency of an enzyme.</p>
<p>K<sub>M</sub> is the concentration of the substrate in mol dm<sup>-3</sup> which produces a reaction rate of half V<sub>max</sub>. So it is found like this</p>
</div>

<div class="image-container"><img src="enzymerate3.gif"></div>

<div class="text-block">
<p>A low value of K<sub>M</sub> means that the reaction is going quickly even at low substrate concentrations. A higher value means the enzyme isn't as effective.</p>
</div>

<div class="note">
<p>Note: You might possibly wonder why you have to go to the trouble of halving the maximum rate to get this information. Couldn't you just find the concentration of the substrate which produced the maximum rate?</p>
<p>If you look at the shape of the graph, it would be impossible to get any accurate measure of the concentration which first produced a maximum rate. The curve just gradually gets closer and closer to the horizontal, and there is no clear cut-off point at which it suddenly becomes horizontal.</p>
</div>

<h2>The Effect of Temperature on the Rate of Enzyme-controlled Reactions</h2>

<h3>A Reminder About the Effect of Temperature on Rate in Ordinary Chemical Reactions</h3>

<div class="text-block">
<p>Remember that for molecules to react, they have to collide with an energy equal to or greater than the activation energy for the reaction.</p>
<p>Heating a reaction makes the molecules move faster and so collide more often. More collisions in a given time means a faster reaction.</p>
<p>But far more importantly, increasing the temperature has a very big effect on the number of collisions with enough energy to react. Quite a small temperature rise can produce a large increase in rate.</p>
<p>As a reasonable approximation for many (although not all) reactions close to room temperature, a 10°C increase in temperature will double the rate of a chemical reaction. This is only an approximation – it may take 9°C or 11°C or whatever, but it gives you an idea of what to expect.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about any of this, have a quick look at the page about the <a href="../../physical/basicrates/temperature.html#top">effect of temperature on rates of reaction</a>.</p>
</div>

<h3>Plotting Rates of Enzyme-controlled Reactions Against Temperature</h3>

<div class="text-block">
<p>For low temperatures up to about 40°C, enzyme-controlled reactions behave much as you would expect any other chemical reaction to behave. But above about 40°C (the exact temperature varies from enzyme to enzyme), there is a dramatic fall in reaction rate.</p>
<p>A typical graph of rate against temperature might look like this:</p>
</div>

<div class="image-container"><img src="enzymeratet.gif"></div>

<div class="text-block">
<p>The temperature at which the rate is fastest is called the optimum temperature for that enzyme.</p>
<p>Different enzymes have different optimum temperatures. Some enzymes, for example, in organisms known as thermophiles or extremophiles are capable of working at temperatures like 80°C or even higher.</p>
<p>The optimum temperature for a particular enzyme varies depending on how long it is exposed to the higher temperatures. Enzymes can withstand temperatures higher than their normal optimum if they are only exposed to the higher temperatures for a very short time.</p>
</div>

<h3>Explaining the Rate Against Temperature Graph</h3>

<div class="text-block">
<p>At lower temperatures, the shape of the graph is exactly what you would expect for any reaction. All that needs explaining is why the rate falls above the optimum temperature.</p>
<p>Remember that the enzyme works because its substrate fits into the active site on the protein molecule. That active site is produced because of the way the protein is folded into its tertiary structure.</p>
<p>Heating an enzyme gives the protein chains extra energy and makes them move more. If they move enough, then the bonds holding the tertiary structure in place will come under increasing strain.</p>
<p>The weaker bonds will break first – van der Waals attractions between side groups, and then hydrogen bonds. As soon as these bonds holding the tertiary structure together are broken, then the shape of the active site is likely to be lost.</p>
<p>Obviously, the longer the enzyme is held at the higher temperature, the more time there is for the enzyme structure to be broken up. Given enough time and high enough temperatures, the enzyme structure is broken permanently. The enzyme is said to be denatured. For example, boiling an egg for 5 minutes denatures the proteins in the egg. Once that has happened, you can't un-boil it by cooling it!</p>
</div>

<h2>The Effect of pH on the Rate of Enzyme-controlled Reactions</h2>

<div class="text-block">
<p>In the same way that every enzyme has an optimum temperature, so each enzyme also has an optimum pH at which it works best.</p>
<p>For example, trypsin and pepsin are both enzymes in the digestive system which break protein chains in the food into smaller bits – either into smaller peptide chains or into individual amino acids.</p>
<p>Pepsin works in the highly acidic conditions of the stomach. It has an optimum pH of about 1.5.</p>
<p>On the other hand, trypsin works in the small intestine, parts of which have a pH of around 7.5. Trypsin's optimum pH is about 8.</p>
<p>If you think about the structure of an enzyme molecule, and the sorts of bonds that it may form with its substrate, it isn't surprising that pH should matter.</p>
<p>Suppose an enzyme has an optimum pH around 7. Imagine that at a pH of around 7, a substrate attaches itself to the enzyme via two ionic bonds.In the diagram below, the groups allowing ionic bonding are caused by the transfer of a hydrogen ion from a -COOH group in the side chain of one amino acid residue to an -NH<sub>2</sub> group in the side chain of another.</p>
<p>In this simplified example, that is equally true in both the substrate and the enzyme.</p>
</div>

<div class="image-container"><img src="asiteph1.gif"></div>

<div class="text-block">
<p>Now think about what happens at a lower pH – in other words under acidic conditions.</p>
<p>It won't affect the -NH<sub>3</sub><sup>+</sup> group, but the -COO<sup>-</sup> will pick up a hydrogen ion.</p>
<p>What you will have will be this:</p>
</div>

<div class="image-container"><img src="asiteph2.gif"></div>

<div class="text-block">
<p>You no longer have the ability to form ionic bonds between the substrate and the enzyme. If those bonds were necessary to attach the substrate and activate it in some way, then at this lower pH, the enzyme won't work.</p>
<p>What if you have a pH higher than 7 – in other words under alkaline conditions.</p>
<p>This time, the -COO<sup>-</sup> group won't be affected, but the -NH<sub>3</sub><sup>+</sup> group will lose a hydrogen ion.</p>
<p>That leaves</p>
</div>

<div class="image-container"><img src="asiteph3.gif"></div>

<div class="text-block">
<p>Again, there is no possibility of forming ionic bonds, and so the enzyme probably won't work this time either.</p>
</div>

<div class="note">
<p>Note: If you think about it, this argument only works using ionic bonding if the enzyme has an optimum pH around 7. Under fairly acidic or alkaline conditions, all the charges present in the enzyme and substrate would be the same – as in the examples above.</p>
<p>Under those circumstances, you would be looking at other sorts of bonding – hydrogen bonding, for example, perhaps involving hydrogen bonds between an ion on one of either the enzyme and substrate, and a suitable hydrogen atom or lone pair on the other.</p>
<p>The example given is easy to follow. Don't worry about complications at this level. As long as you can see why changing the pH might affect a simple case, that's all you need.</p>
</div>

<div class="text-block">
<p>At extreme pH's, something more drastic can happen. Remember that the tertiary structure of the protein is in part held together by ionic bonds just like those we've looked at between the enzyme and its substrate.</p>
<p>At very high or very low pH's, these bonds within the enzyme can be disrupted, and it can lose its shape. If it loses its shape, the active site will probably be lost completely. This is essentially the same as denaturing the protein by heating it too much.</p>
<p>This enzyme topic continues onto a final page about enzyme inhibitors.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-proteinenz2.pdf" target="_blank">Questions on changing the conditions in enzyme catalysis</a>
<a href="../questions/a-proteinenz2.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="enzymes3.html#top">To continue to the final page about enzymes</a>
<a href="../aminoacidmenu.html#top">To the amino acid and proteins menu</a>
<a href="../../orgpropsmenu.html#top">To the menu of other organic compounds</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>