<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Enzyme Inhibitors | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A simple explanation of competitive and non-competitive enzyme inhibitors">
<meta name="keywords" content="protein, proteins, structure, structures, enzyme, enzymes, catalyst, catalysis, active site, inhibit, inhibition, inhibitor, inhibitors, competitive, non-competitive">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Enzyme Inhibitors</h1>

<div class="text-block">
<p>This page looks at the effect of inhibitors on reactions involving enzymes. This is the third and final page talking about how enzymes function as catalysts. Please remember that this series of pages is written for 16 – 18 year old chemistry students. If you want something more advanced, you are looking in the wrong place.</p>
</div>

<div class="note">
<p>Note: This page assumes that you have already read the page about <a href="enzymes.html#top">how proteins function as enzymes</a>. If you have come straight to this page via a search engine, you really ought to go back and read that page first. In fact, unless you already have a good knowledge of protein structure, you may have to go back further still to a page about <a href="proteinstruct.html#top">the structure of proteins</a>.</p>
</div>

<h2>Competitive and Non-competitive Inhibition</h2>

<h3>Competitive Inhibitors</h3>

<div class="text-block">
<p>This is the most straightforward and obvious form of enzyme inhibition – and the name tells you exactly what happens.</p>
<p>The inhibitor has a similar shape to the usual substrate for the enzyme, and competes with it for the active site. However, once it is attached to the active site, nothing happens to it. It doesn't react – essentially, it just gets in the way.</p>
<p>Remember the general equation for an enzyme reacting with a substrate?</p>
</div>

<div class="block-formula">
\text{E} + \text{S} \xrightleftharpoons{} \text{E}{-}\text{S}~\text{complex} \longrightarrow \text{E} + \text{P}
</div>

<div class="text-block">
<p>The equivalent equation for a competitive inhibitor looks like this:</p>
</div>

<div class="block-formula">
\text{E} + \text{I}_\text{C} \xrightleftharpoons{} \text{E}{-}\text{I}_\text{C}~\text{complex}
</div>

<div class="text-block">
<p>The complex doesn't react any further to form products – but its formation is still reversible. It breaks up again to form the enzyme and the inhibitor molecule.</p>
<p>That means that if you increase the concentration of the substrate, the substrate can out-compete the inhibitor, and so the normal reaction can take place at a reasonable rate.</p>
<p>A simple example of this involves malonate ions inhibiting the enzyme succinate dehydrogenase. This enzyme catalyses the conversion of succinate ions to fumarate ions. The modern names are:</p>
</div>

<ul>
<li>malonate: propanedioate</li>
<li>succinate: butanedioate</li>
<li>fumarate: trans-butenedioate</li>
</ul>

<div class="text-block">
<p>The conversion that succinic dehydrogenase carries out is:</p>
</div>

<div class="image-container"><img src="succdh.gif"></div>

<div class="text-block">
<p>The reaction is inhibited by malonate ions which have a very similar shape to succinate ions.</p>
</div>

<div class="image-container"><img src="malonate.gif"></div>

<div class="text-block">
<p>The similar shape lets the malonate ions bind to the active site, but the lack of the CH<sub>2</sub>-CH<sub>2</sub> bond in the centre of the ion stops any further reaction taking place.</p>
<p>The malonate ions therefore block the active site – but remember that this is reversible. The malonate ions will break away and free up the enzyme again. The malonate ions are in competition for the site – they aren't destroying it.</p>
<p>If the succinate ions have a greater concentration than the malonate ions, by chance they will get access to the site more often than the malonate ions. That means that you can overcome the effect of a competitive inhibitor by increasing the concentration of the substrate.</p>
</div>

<div class="note">
<p>Note: Some biochemists are very sloppy about the use of chemical names! If you read about this reaction in other places, you may find the reaction quoted as converting succinic acid into fumaric acid. Some sources even seem to imply that the words "succinate" and "succinic acid" mean the same thing. That's unacceptable from a chemistry point of view!</p>
<p>As far as I have been able to discover, the correct version is the conversion from succinate ions to fumarate ions (as above). At a typical cell pH of around pH 7, these substances will be present mainly as their ions, not as the unionised acids. And scientific papers from people working on the mechanisms for this conversion all talk about the ions, not the acids.</p>
</div>

<h3>Non-competitive Inhibitors</h3>

<div class="text-block">
<p>A non-competitive inhibitor doesn't attach itself to the active site, but attaches somewhere else on the enzyme. By attaching somewhere else it affects the structure of the enzyme and so the way the enzyme works. Because there isn't any competition involved between the inhibitor and the substrate, increasing the substrate concentration won't help.</p>
<p>If you look at various biochemistry sites on the web, you will find two explanations for this. We'll look at the simple, fairly obvious one in some detail in a minute. I want to have a brief word about the other one first.</p>
</div>

<h4>"Pure" non-competitive inhibitors</h4>

<div class="text-block">
<p>This explanation says that the inhibitor doesn't affect the ability of the substrate to bond with the active site, but stops it reacting once it is there.</p>
<p>I found a couple of biochemistry sites which said that inhibitors working like this (which they describe as pure non-competitive inhibitors) are virtually unknown. As a non-biochemist, I don't know what the truth is about this – if you want to find out, you will probably have to do a biochemistry degree!</p>
</div>

<h4>Other non-competitive inhibitors</h4>

<div class="text-block">
<p>The straightforward explanation (which would seem to apply to most enzymes) is that reaction with the inhibitor causes the shape of the active site to change. Remember that non-competitive inhibitors aren't attaching directly to the active site, but elsewhere on the enzyme.</p>
<p>The inhibitor attachs to a side group in the protein chain, and affects the way the protein folds into its tertiary structure. That in turn changes the shape of the active site. If the shape of the active site changes, then the substrate can't attach to it any more.</p>
<p>Some non-competitive inhibitors attach irreversibly to the enzyme, and therefore stop it working permanently. Others attach reversibly.</p>
<p>A relatively uncomplicated example of non-competitive inhibitors in a reasonably familiar situation is:</p>
</div>

<h5>Heavy metal poisoning</h5>

<div class="text-block">
<p>You are probably aware that compounds containing heavy metals such as lead, mercury, copper or silver are poisonous. This is because ions of these metals are non-competitive inhibitors for several enzymes.</p>
<p>I'm going to take silver as a simple example.</p>
<p>Silver ions react with -SH groups in the side groups of cysteine residues in the protein chain:</p>
</div>

<div class="image-container"><img src="silverinhib.gif"></div>

<div class="text-block">
<p>There isn't enough electronegativity difference between silver and sulfur for a full ionic bond and so the bond can be considered as covalent.</p>
<p>If the cysteine residue is somewhere on the protein chain which affects the way it folds into its tertiary structure, then altering this group could have an effect on the shape of the active site, and so stop the enzyme from working.</p>
<p>The 2+ ions from, for example, mercury, copper or lead can behave similarly – also attaching themselves to the sulfur in place of the hydrogen.</p>
</div>

<div class="note">
<p>Note: I have come across a couple of references to the ability of heavy metal ions to break sulfur-sulfur bridges, which would have a major effect on the folding of the protein chain.</p>
<p>A student suggested this page, which has a <a href="http://www.wissensdrang.com/auf1hg.htm">note about mercury poisoning</a> (see item 4 on the page). This suggests that mercury breaks sulfur-sulfur bridges by attaching to both of the sulfur atoms, distorting the bridge, and so the folding of the protein chains. I have no reason to suppose that this description is wrong, but I am a bit wary of relying on a single source.</p>
<p>The reason for choosing silver as my example rather than the more obvious mercury or lead is that it forms a 1+ ion. For 2+ ions, you would have to worry about what got attached to the metal as well as the sulfur – or whether it retained a single positive charge. All the biochemistry sources I've been able to find skip over this problem – as a chemistry teacher looking for chemical accuracy, I'm not prepared to do that!</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-proteinenz3.pdf" target="_blank">Questions on enzyme inibitors</a>
<a href="../questions/a-proteinenz3.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../aminoacidmenu.html#top">To the amino acid and proteins menu</a>
<a href="../../orgpropsmenu.html#top">To the menu of other organic compounds</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>