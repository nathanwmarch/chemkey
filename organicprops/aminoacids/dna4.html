<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Genetic Code | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="How the base sequences in DNA and RNA code for particular amino acids.">
<meta name="keywords" content="rna, dna, structure, genetic, code, base, sequence, codon">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Genetic Code</h1>

<div class="text-block">
<p>This page looks at how the base sequences in DNA and RNA are used to code for particular amino acids when it comes to building protein chains. It is designed for 16 – 18 year old chemistry students.</p>
</div>

<div class="note">
Note: If you have come straight to this page from a search engine, you should be aware that this is the fourth page in a sequence of pages about DNA and RNA. Unless you just want a quick reference to get the coding for a particular amino acid, it would pay you to start from the beginning with the <a href="dna1.html#top">structure of DNA</a>.
</div>

<h3>Introduction</h3>

<div class="text-block">
<p>You can think of the sequences of bases in the coding strand of DNA or in messenger RNA as coded instructions for building protein chains out of amino acids. There are 20 amino acids used in making proteins, but only four different bases to be used to code for them.</p>
<p>Obviously one base can't code for one amino acid. That would leave 16 amino acids with no codes.</p>
<p>If you took two bases to code for each amino acid, that would still only give you 16 possible codes (TT, TC, TA, TG, CT, CC, CA and so on) – still not enough.</p>
<p>However, if you took three bases per amino acid, that gives you 64 codes (TTT, TTC, TTA, TTG, TCT, TCC and so on). That's enough to code for everything with lots to spare. You will find a full table of these below.</p>
<p>A three base sequence in DNA or RNA is known as a codon.</p>
</div>

<h3>The Code in DNA</h3>

<div class="text-block">
<p>The codes in the coding strand of DNA and in messenger RNA aren't, of course, identical, because in RNA the base uracil (U) is used instead of thymine (T).</p>
<p>The table shows how the various combinations of three bases in the coding strand of DNA are used to code for individual amino acids – shown by their three letter abbreviation.</p>
</div>

<div class="image-container"><img src="dnacode.gif"></div>

<div class="text-block">
<p>The table is arranged in such a way that it is easy to find any particular combination you want. It is fairly obvious how it works and, in any case, it doesn't take very long just to scan through the table to find what you want.</p>
<p>The colours are to stress the fact that most of the amino acids have more than one code. Look, for example, at leucine in the first column. There are six different codons all of which will eventually produce a leucine (Leu) in the protein chain. There are also six for serine (Ser).</p>
<p>In fact there are only two amino acids which have only one sequence of bases to code for them – methionine (Met) and tryptophan (Trp).</p>
<p>You have probably noticed that three codons don't have an amino acid written beside them, but say "stop" instead. For obvious reasons these are known as stop codons. We'll leave talking about those until we have looked at the way the code works in messenger RNA.</p>
</div>

<h3>The Code in Messenger RNA</h3>

<div class="text-block">
<p>You will remember that when DNA is transcribed into messenger RNA, the sequence of bases remains exactly the same, except that each thymine (T) is replaced by uracil (U). That gives you the table:</p>
</div>

<div class="image-container"><img src="rnacode.gif"></div>

<div class="text-block">
<p>In many ways, this is the more useful table. Messenger RNA is directly involved in the production of the protein chains (see the next page in this sequence). The DNA coding chain is one stage removed from this because it must first be transcribed into a messenger RNA chain.</p>
</div>

<h3>Start and Stop Codons</h3>

<div class="text-block">
<p>The stop codons in the RNA table (UAA, UAG and UGA) serve as a signal that the end of the chain has been reached during protein synthesis – and we will come back to that on the next page.</p>
<p>There is also a start codon – but you won't find it called that in the table!</p>
<p>The codon that marks the start of a protein chain is AUG. If you check the table, that's the amino acid, methionine (Met). That ought to mean that every protein chain must start with methionine. That's not quite true because in some cases the methionine can get chopped off the chain after synthesis is complete.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-dna4.pdf" target="_blank">Questions on DNA – the genetic code</a>
<a href="../questions/a-dna4.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="dna5.html#top">To the next page about protein synthesis</a>
<a href="../aminoacidmenu.html#top">To the amino acid and other biochemistry menu</a>
<a href="../../orgpropsmenu.html#top">To the menu of other organic compounds</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>