<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to Grignard Reagents | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An introduction to the formation of Grignard reagents from halogenoalkanes, and to some of their reactions">
<meta name="keywords" content="halogenoalkanes, haloalkanes, alkyl halides, grignard, reagent, reagents">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to Grignard Reagents</h1>

<div class="text-block">
<p>This page takes an introductory look at how Grignard reagents are made from halogenoalkanes (haloalkanes or alkyl halides), and introduces some of their reactions.</p>
</div>

<h2>Making Grignard Reagents</h2>

<h3>What are Grignard Reagents?</h3>

<div class="text-block">
<p>A Grignard reagent has a formula RMgX where X is a halogen, and R is an alkyl or aryl (based on a benzene ring) group. For the purposes of this page, we shall take R to be an alkyl group.</p>
<p>A typical Grignard reagent might be CH<sub>3</sub>CH<sub>2</sub>MgBr.</p>
</div>

<h3>The Preparation of a Grignard Reagent</h3>

<div class="text-block">
<p>Grignard reagents are made by adding the halogenoalkane to small bits of magnesium in a flask containing ethoxyethane (commonly called diethyl ether or just "ether"). The flask is fitted with a reflux condenser, and the mixture is warmed over a water bath for 20 – 30 minutes.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{Br} + \text{Mg} \xrightarrow{\text{ethoxyethane}} \text{CH}_3\text{CH}_2\text{MgBr}
</div>

<div class="text-block">
<p>Everything must be perfectly dry because Grignard reagents react with water (see below).</p>
</div>

<div class="note">
<p>Warning! Ethoxyethane (ether) is very dangerous to work with. It is a liquid with a boiling point of only 34.5°C, and is extremely flammable. It is also an anaesthetic. Under no circumstances should you try to carry out this reaction without properly qualified guidance.</p>
</div>

<div class="text-block">
<p>Any reactions using the Grignard reagent are carried out with the mixture produced from this reaction. You can't separate it out in any way.</p>
</div>

<h2>Reactions of Grignard Reagents</h2>

<h3>Grignard Reagents and Water</h3>

<div class="text-block">
<p>Grignard reagents react with water to produce alkanes. This is the reason that everything has to be very dry during the preparation above.</p>
</div>

<h5>For example:</h5>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{MgBr} + \text{H}_2\text{O} \longrightarrow \text{CH}_3\text{CH}_3 + \text{Mg}(\text{OH})\text{Br}
</div>

<div class="text-block">
<p>The inorganic product, Mg(OH)Br, is referred to as a "basic bromide". You can think of it as a sort of half-way stage between magnesium bromide and magnesium hydroxide.</p>
</div>

<h3>Grignard Reagents and Carbon Dioxide</h3>

<div class="text-block">
<p>Grignard reagents react with carbon dioxide in two stages. In the first, you get an addition of the Grignard reagent to the carbon dioxide.</p>
<p>Dry carbon dioxide is bubbled through a solution of the Grignard reagent in ethoxyethane, made as described above.</p>
</div>

<h5>For example:</h5>

<div class="image-container"><img src="grignardco2a.gif"></div>

<div class="text-block">
<p>The product is then hydrolysed (reacted with water) in the presence of a dilute acid. Typically, you would add dilute sulfuric acid or dilute hydrochloric acid to the solution formed by the reaction with the CO<sub>2</sub>.</p>
<p>A carboxylic acid is produced with one more carbon than the original Grignard reagent.</p>
<p>The usually quoted equation is (without the red bits):</p>
</div>

<div class="image-container"><img src="grignardco2b.gif"></div>

<div class="text-block">
<p>Almost all sources quote the formation of a basic halide such as Mg(OH)Br as the other product of the reaction. That's actually misleading because these compounds react with dilute acids. What you end up with would be a mixture of ordinary hydrated magnesium ions, halide ions and sulfate or chloride ions – depending on which dilute acid you added.</p>
</div>

<div class="note">
<p>Note: What you need to learn about this depends on what your examiners want. The only way to find that out is to look at old exam papers and mark schemes. If you are a UK A-level student and haven't got copies of these, find out how to get hold of them by going to the <a href="../../syllabuses.html#top">syllabuses</a> page to find your Exam Board's web address.</p>
</div>

<h3>Grignard Reagents and Carbonyl Compounds</h3>

<h4>What are carbonyl compounds?</h4>

<div class="text-block">
<p>Carbonyl compounds contain the C=O double bond. The simplest ones have the form:</p>
</div>

<div class="image-container"><img src="carbonyl.gif"></div>

<div class="text-block">
<p>R and R' can be the same or different, and can be an alkyl group or hydrogen.</p>
</div>

<div class="note">
<p>Note: Other carbonyl compounds also react with Grignard reagents, but these are all that are required for UK A-level purposes.</p>
</div>

<div class="text-block">
<p>If one (or both) of the R groups are hydrogens, the compounds are called aldehydes. For example:</p>
</div>

<div class="inline">
<div class="chemical-structure" name="ethanal" type="2d"></div>
<div class="chemical-structure" name="propanal" type="2d"></div>
</div>

<div class="text-block">
<p>If both of the R groups are alkyl groups, the compounds are called ketones. Examples include:</p>
</div>

<div class="inline">
<div class="chemical-structure" name="propanone" type="2d"></div>
<div class="chemical-structure" name="butanone" type="2d"></div>
</div>

<h4>The general reaction between Grignard reagents and carbonyl compounds</h4>

<div class="text-block">
<p>The reactions between the various sorts of carbonyl compounds and Grignard reagents can look quite complicated, but in fact they all react in the same way – all that changes are the groups attached to the carbon-oxygen double bond.</p>
<p>It is much easier to understand what is going on by looking closely at the general case (using "R" groups rather than specific groups) – and then slotting in the various real groups as and when you need to.</p>
<p>The reactions are essentially identical to the reaction with carbon dioxide – all that differs is the nature of the organic product.</p>
<p>In the first stage, the Grignard reagent adds across the carbon-oxygen double bond:</p>
</div>

<div class="image-container"><img src="grigcarbgena.gif"></div>

<div class="text-block">
<p>Dilute acid is then added to this to hydrolyse it. (I am using the normally accepted equation ignoring the fact that the Mg(OH)Br will react further with the acid.)</p>
</div>

<div class="image-container"><img src="grigcarbgenb.gif"></div>

<div class="text-block">
<p>An alcohol is formed. One of the key uses of Grignard reagents is the ability to make complicated alcohols easily.</p>
<p>What sort of alcohol you get depends on the carbonyl compound you started with – in other words, what R and R' are.</p>
</div>

<h4>The reaction between Grignard reagents and methanal</h4>

<div class="text-block">
<p>In methanal, both R groups are hydrogen. Methanal is the simplest possible aldehyde.</p>
</div>

<div class="chemical-structure" name="formaldehyde" type="2d" label="methanal"></div>

<div class="text-block">
<p>Assuming that you are starting with CH<sub>3</sub>CH<sub>2</sub>MgBr and using the general equation above, the alcohol you get always has the form:</p>
</div>

<div class="image-container"><img src="genalcohol.gif"></div>

<div class="text-block">
<p>Since both R groups are hydrogen atoms, the final product will be:</p>
</div>

<div class="image-container"><img src="makeprimoh.gif"></div>

<div class="text-block">
<p>A primary alcohol is formed. A primary alcohol has only one alkyl group attached to the carbon atom with the -OH group on it.</p>
<p>You could obviously get a different primary alcohol if you started from a different Grignard reagent.</p>
</div>

<h4>The reaction between Grignard reagents and other aldehydes</h4>

<div class="text-block">
<p>The next biggest aldehyde is ethanal. One of the R groups is hydrogen and the other CH<sub>3</sub>.</p>
</div>

<div class="chemical-structure" name="ethanal" type="2d"></div>

<div class="text-block">
<p>Again, think about how that relates to the general case. The alcohol formed is:</p>
</div>

<div class="image-container"><img src="genalcohol.gif"></div>

<div class="text-block">
<p>So this time the final product has one CH<sub>3</sub> group and one hydrogen attached:</p>
</div>

<div class="image-container"><img src="makesecoh.gif"></div>

<div class="text-block">
<p>A secondary alcohol has two alkyl groups (the same or different) attached to the carbon with the -OH group on it.</p>
<p>You could change the nature of the final secondary alcohol by either:</p>
</div>

<ul>
<li>changing the nature of the Grignard reagent – which would change the CH<sub>3</sub>CH<sub>2</sub> group into some other alkyl group;</li>
<li>changing the nature of the aldehyde – which would change the CH<sub>3</sub> group into some other alkyl group.</li>
</ul>

<h4>The reaction between Grignard reagents and ketones</h4>

<div class="text-block">
<p>Ketones have two alkyl groups attached to the carbon-oxygen double bond. The simplest one is propanone.</p>
</div>

<div class="chemical-structure" name="propanone" type="2d"></div>

<div class="text-block">
<p>This time when you replace the R groups in the general formula for the alcohol produced you get a tertiary alcohol.</p>
</div>

<div class="image-container"><img src="maketertoh.gif"></div>

<div class="text-block">
<p>A tertiary alcohol has three alkyl groups attached to the carbon with the -OH attached. The alkyl groups can be any combination of same or different.</p>
<p>You could ring the changes on the product by</p>
</div>

<ul>
<li>changing the nature of the Grignard reagent – which would change the CH<sub>3</sub>CH<sub>2</sub> group into some other alkyl group;</li>
<li>changing the nature of the ketone – which would change the CH<sub>3</sub> groups into whatever other alkyl groups you choose to have in the original ketone.</li>
</ul>

<h4>Why do Grignard reagents react with carbonyl compounds?</h4>

<div class="text-block">
<p>The mechanisms for these reactions aren't required by any UK A-level syllabuses, but you might need to know a little about the nature of Grignard reagents.</p>
<p>The bond between the carbon atom and the magnesium is polar. Carbon is more electronegative than magnesium, and so the bonding pair of electrons is pulled towards the carbon.</p>
<p>That leaves the carbon atom with a slight negative charge.</p>
</div>

<div class="image-container"><img src="grigpolar.gif"></div>

<div class="note">
<p>Note: If you aren't sure about <a href="../../basicorg/bonding/eneg.html#top">electronegativity</a>, you can read about it in an organic context by following this link.</p>
</div>

<div class="text-block">
<p>The carbon-oxygen double bond is also highly polar with a significant amount of positive charge on the carbon atom. The nature of this bond is described in detail elsewhere on this site.</p>
</div>

<div class="note">
<p>Note: If you are interested, you could follow this link to the <a href="../../basicorg/bonding/carbonyl.html#top">bonding in a carbonyl group</a>. Reading the last couple of paragraphs on that page would be enough in the present context.</p>
</div>

<div class="text-block">
<p>The Grignard reagent can therefore serve as a nucleophile because of the attraction between the slight negativeness of the carbon atom in the Grignard reagent and the positiveness of the carbon in the carbonyl compound.</p>
<p>A nucleophile is a species that attacks positive (or slightly positive) centres in other molecules or ions.</p>
</div>

<div class="note">
<p>Note: I have included this because one of the UK A-level syllabuses says that candidates should "recall that Grignard reagents act as nucleophiles". That is all you need to know! The mechanism is more complex than this suggests at first sight, and isn't required. You won't find these mechanisms anywhere on this site.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>

<a href="../questions/q-halogrignard.pdf" target="_blank">Questions on Grignard reagents</a>
<a href="../questions/a-halogrignard.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../haloalkanemenu.html#top">To the halogenoalkanes menu</a>
<a href="../../orgpropsmenu.html#top">To the menu of other organic compounds</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>