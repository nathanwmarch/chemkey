<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Thin Layer Chromatography (TLC) | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An introduction to chromatography using thin layer chromatography as an example.">
<meta name="keywords" content="chromatography, thin layer, rf, stationary, mobile, phase, adsorb, adsorption, elute, elution, solvent">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js">
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Thin Layer Chromatography (TLC)</h1>

<div class="text-block">
<p>This page is an introduction to chromatography using thin layer chromatography as an example. Although if you are a beginner you may be more familiar with paper chromatography, thin layer chromatography is equally easy to describe and more straightforward to explain.</p>
</div>

<div class="note">
<p>Note: I'm taking a simple view of the way that thin layer chromatography works in terms of adsorption (see below) which should be adequate for students doing courses for 16 – 18 year olds. The reality is more complicated and the explanation will vary depending on what sort of solvent or solvent mixture you are using. Some similar problems are discussed on the page about paper chromatography, but I am unwilling to do the same thing on this page which is intended as a fairly gentle introduction to chromatography.</p>
</div>

<h2>Carrying out Thin Layer Chromatography</h2>

<h3>Background</h3>

<div class="text-block">
<p>Chromatography is used to separate mixtures of substances into their components. All forms of chromatography work on the same principle.</p>

<p>They all have a <i>stationary phase</i> (a solid, or a liquid supported on a solid) and a <i>mobile phase</i> (a liquid or a gas). The mobile phase flows through the stationary phase and carries the components of the mixture with it. Different components travel at different rates. We'll look at the reasons for this further down the page.</p>

<p>Thin layer chromatography is done exactly as it says – using a thin, uniform layer of silica gel or alumina coated onto a piece of glass, metal or rigid plastic.</p>

<p>The silica gel (or the alumina) is the stationary phase. The stationary phase for thin layer chromatography also often contains a substance which fluoresces in UV light – for reasons you will see later. The mobile phase is a suitable liquid solvent or mixture of solvents.</p>
</div>

<h3>Producing the Chromatogram</h3>

<div class="text-block">
<p>We'll start with a very simple case – just trying to show that a particular dye is in fact a mixture of simpler dyes.
</p>
</div>

<div class="image-container">
<img src="tlc1.gif">
</div>

<div class="note">
<p>Note: The chromatography plate will in fact be pure white – not pale grey. I'm forced to show it as off-white because of the way I construct the diagrams. Anything I draw as pure white allows the background colour of the page to show through.</p>
</div>

<div class="text-block">
<p>A pencil line is drawn near the bottom of the plate and a small drop of a solution of the dye mixture is placed on it. Any labelling on the plate to show the original position of the drop must also be in pencil. If any of this was done in ink, dyes from the ink would also move as the chromatogram developed.</p>
<p>When the spot of mixture is dry, the plate is stood in a shallow layer of solvent in a covered beaker. It is important that the solvent level is below the line with the spot on it.</p>
<p>The reason for covering the beaker is to make sure that the atmosphere in the beaker is saturated with solvent vapour. To help this, the beaker is often lined with some filter paper soaked in solvent. Saturating the atmosphere in the beaker with vapour stops the solvent from evaporating as it rises up the plate.</p>
<p>As the solvent slowly travels up the plate, the different components of the dye mixture travel at different rates and the mixture is separated into different coloured spots.</p>
</div>

<div class="image-container">
<img src="tlc2.gif">
</div>

<div class="text-block">
<p>The diagram shows the plate after the solvent has moved about half way up it.</p>
<p>The solvent is allowed to rise until it almost reaches the top of the plate. That will give the maximum separation of the dye components for this particular combination of solvent and stationary phase.</p>
</div>

<h3>Measuring R
<sub>f</sub> values</h3>

<div class="text-block">
<p>If all you wanted to know is how many different dyes made up the mixture, you could just stop there. However, measurements are often taken from the plate in order to help identify the compounds present. These measurements are the distance travelled by the solvent, and the distance travelled by individual spots.</p>
<p>When the solvent front gets close to the top of the plate, the plate is removed from the beaker and the position of the solvent is marked with another line before it has a chance to evaporate.</p>
<p>These measurements are then taken:</p>
</div>

<div class="image-container">
<img src="tlc3.gif">
</div>

<div class="text-block">
<p>The R<sub>f</sub> value for each dye is then worked out using the formula:</p>
</div>

<div class="block-formula">R_f = \frac{\text{distance travelled by compound}}{\text{distance travelled by solvent}}</div>

<div class="text-block">
<p>For example, if the red component travelled 1.7 cm from the base line while the solvent had travelled 5.0 cm, then the R<sub>f</sub> value for the red dye is:</p>
</div>

<div class="block-formula">\begin{aligned}
R_f = \frac{1.7}{5.0} \\
= 0.34
\end{aligned}
</div>

<div class="text-block">
<p>If you could repeat this experiment under exactly the same conditions, then the R<sub>f</sub> values for each dye would always be the same. For example, the R<sub>f</sub> value for the red dye would always be 0.34. However, if anything changes (the temperature, the exact composition of the solvent, and so on), that is no longer true. You have to bear this in mind if you want to use this technique to identify a particular dye. We'll look at how you can use thin layer chromatography for analysis further down the page.</p>
</div>

<h3>What if the Substances you are Interested in are Colourless?</h3>

<div class="text-block">
<p>There are two simple ways of getting around this problem.</p>
</div>

<h4>Using fluorescence</h4>

<div class="text-block">
<p>You may remember that I mentioned that the stationary phase on a thin layer plate often has a substance added to it which will fluoresce when exposed to UV light. That means that if you shine UV light on it, it will glow.</p>
<p>That glow is masked at the position where the spots are on the final chromatogram – even if those spots are invisible to the eye. That means that if you shine UV light on the plate, it will all glow apart from where the spots are. The spots show up as darker patches.</p>
</div>

<div class="image-container">
<img src="tlc4.gif">
</div>

<div class="text-block">
<p>While the UV is still shining on the plate, you obviously have to mark the positions of the spots by drawing a pencil circle around them. As soon as you switch off the UV source, the spots will disappear again.</p>
</div>

<h4>Showing the spots up chemically</h4>

<div class="text-block">
<p>In some cases, it may be possible to make the spots visible by reacting them with something which produces a coloured product. A good example of this is in chromatograms produced from amino acid mixtures.</p>
<p>The chromatogram is allowed to dry and is then sprayed with a solution of ninhydrin. Ninhydrin reacts with amino acids to give coloured compounds, mainly brown or purple.</p>
</div>

<div class="image-container">
<img src="tlc5.gif">
</div>

<div class="text-block">
<p>In another method, the chromatogram is again allowed to dry and then placed in an enclosed container (such as another beaker covered with a watch glass) along with a few iodine crystals.</p>
<p>The iodine vapour in the container may either react with the spots on the chromatogram, or simply stick more to the spots than to the rest of the plate. Either way, the substances you are interested in may show up as brownish spots.</p>
</div>

<h3>Using Thin Layer Chromatography to Identify Compounds</h3>

<div class="text-block">
<p>Suppose you had a mixture of amino acids and wanted to find out which particular amino acids the mixture contained. For simplicity we'll assume that you know the mixture can only possibly contain five of the common amino acids.</p>
<p>A small drop of the mixture is placed on the base line of the thin layer plate, and similar small spots of the known amino acids are placed alongside it. The plate is then stood in a suitable solvent and left to develop as before. In the diagram, the mixture is M, and the known amino acids are labelled 1 to 5.</p>
<p>The left-hand diagram shows the plate after the solvent front has almost reached the top. The spots are still invisible. The second diagram shows what it might look like after spraying with ninhydrin.</p>
</div>

<div class="image-container">
<img src="tlc6.gif">
</div>

<div class="text-block">
<p>There is no need to measure the R<sub>f</sub> values because you can easily compare the spots in the mixture with those of the known amino acids – both from their positions and their colours.</p>
<p>In this example, the mixture contains the amino acids labelled as 1, 4 and 5.</p>
<p>And what if the mixture contained amino acids other than the ones we have used for comparison? There would be spots in the mixture which didn't match those from the known amino acids. You would have to re-run the experiment using other amino acids for comparison.</p>
</div>

<h2>How Does Thin Layer Chromatography Work?</h2>

<h3>The Stationary Phase – Silica Gel</h3>
<div class="text-block">
<p>Silica gel is a form of silicon dioxide (silica). The silicon atoms are joined via oxygen atoms in a giant covalent structure. However, at the surface of the silica gel, the silicon atoms are attached to -OH groups.</p>
</div>
<div class="note">
<p>Note: If you aren't sure about it, you will find one possible <a href="../../atoms/structures/giantcov.html#top">structure of silicon dioxide</a> towards the bottom of the page you will get to by following this link.</p>
</div>

<div class="text-block">
<p>So, at the surface of the silica gel you have Si-O-H bonds instead of Si-O-Si bonds. The diagram shows a small part of the silica surface.</p>
</div>

<div class="image-container">
<img src="silicagel.gif">
</div>

<div class="text-block">
<p>The surface of the silica gel is very polar and, because of the -OH groups, can form hydrogen bonds with suitable compounds around it as well as van der Waals dispersion forces and dipole-dipole attractions.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="../../atoms/bonding/hbond.html#top">hydrogen bonds and van der Waals forces</a> follow this link to the page about hydrogen bonding. You will find a further link to van der Waals forces at the bottom of that page.</p>
</div>

<div class="text-block">
<p>The other commonly used stationary phase is alumina – aluminium oxide. The aluminium atoms on the surface of this also have -OH groups attached. Anything we say about silica gel therefore applies equally to alumina.</p>
</div>

<h3>What Separates the Compounds as a Chromatogram Develops?</h3>

<div class="text-block">
<p>As the solvent begins to soak up the plate, it first dissolves the compounds in the spot that you have put on the base line. The compounds present will then tend to get carried up the chromatography plate as the solvent continues to move upwards.</p>
<p>How fast the compounds get carried up the plate depends on two things:</p>
</div>

<ul>
<li>How soluble the compound is in the solvent. This will depend on how much attraction there is between the molecules of the compound and those of the solvent.
</li>
<li>How much the compound sticks to the stationary phase – the silica gel, for example. This will depend on how much attraction there is between the molecules of the compound and the silica gel.
</li>
</ul>

<div class="text-block">
<p>Suppose the original spot contained two compounds – one of which can form hydrogen bonds, and one of which can only take part in weaker van der Waals interactions.</p>
<p>The one which can hydrogen bond will stick to the surface of the silica gel more firmly than the other one. We say that one is adsorbed more strongly than the other. Adsorption is the name given to one substance forming some sort of bonds to the surface of another one.</p>
<p>Adsorption isn't permanent – there is a constant movement of a molecule between being adsorbed onto the silica gel surface and going back into solution in the solvent.</p>
<p>Obviously the compound can only travel up the plate during the time that it is dissolved in the solvent. While it is adsorbed on the silica gel, it is temporarily stopped – the solvent is moving on without it. That means that the more strongly a compound is adsorbed, the less distance it can travel up the plate.</p>
<p>In the example we started with, the compound which can hydrogen bond will adsorb more strongly than the one dependent on van der Waals interactions, and so won't travel so far up the plate.</p>
<p>What if both components of the mixture can hydrogen bond?</p>
<p>It is very unlikely that both will hydrogen bond to exactly the same extent, and be soluble in the solvent to exactly the same extent. It isn't just the attraction of the compound for the silica gel which matters. Attractions between the compound and the solvent are also important – they will affect how easily the compound is pulled back into solution away from the surface of the silica.</p>
<p>However, it may be that the compounds don't separate out very well when you make the chromatogram. In that case, changing the solvent may well help – including perhaps changing the pH of the solvent.</p>
<p>This is to some extent just a matter of trial and error – if one solvent or solvent mixture doesn't work very well, you try another one. (Or, more likely, given the level you are probably working at, someone else has already done all the hard work for you, and you just use the solvent mixture you are given and everything will work perfectly!)</p>
</div>

<div class="note">
<p>Note: You will find detailed descriptions with photographs of how to carry out thin layer chromatography archived from the <a href="https://web.archive.org/web/20151122201104/http://orgchem.colorado.edu/Technique/Procedures/TLC/TLC.html">Colorado University site</a>. Linking to other sites is always a little bit hazardous because sites change. If you find that this link doesn't work, please contact me via the address on the <a href="../../about.html#top">About this site</a> page.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-chromtlc.pdf" target="_blank">Questions on thin layer chromatography </a>
<a href="../questions/a-chromtlc.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../chromatogrmenu.html#top">To the chromatography menu</a>
<a href="../../analysismenu.html#top">To the analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>