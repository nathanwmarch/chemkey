<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>High Performance Liquid Chromatography (HPLC) | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A simple description of how high performance liquid chromatography works.">
<meta name="keywords" content="chromatography, hplc, high, performance, liquid, stationary, mobile, phase, adsorb, adsorption, elute, elution, solvent">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>

<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">
<h1>High Performance Liquid Chromatography (HPLC)</h1>

<div class="text-block">
<p>High performance liquid chromatography is a powerful tool in analysis. This page looks at how it is carried out and shows how it uses the same principles as in thin layer chromatography and column chromatography.</p>
</div>

<div class="note">
<p>Note: It is important to read the introductory page about <a href="thinlayer.html#top">thin layer chromatography</a> before you continue with this one – particularly the part about how thin layer chromatography works. High performance liquid chromatography works on the same basic principle. HPLC is essentially an adaptation of <a href="column.html#top">column chromatography</a> – so it might be a good idea to have a (very quick) look at that as well.</p>
</div>

<h2>Carrying out HPLC</h2>

<h3>Introduction</h3>

<div class="text-block">
<p>High performance liquid chromatography is basically a highly improved form of column chromatography. Instead of a solvent being allowed to drip through a column under gravity, it is forced through under high pressures of up to 400 atmospheres. That makes it much faster.</p>
<p>It also allows you to use a very much smaller particle size for the column packing material which gives a much greater surface area for interactions between the stationary phase and the molecules flowing past it. This allows a much better separation of the components of the mixture.</p>
<p>The other major improvement over column chromatography concerns the detection methods which can be used. These methods are highly automated and extremely sensitive.</p>
</div>

<div class="sidebarright">
<div class="image-container" label="a modern HPLC machine">
<div class="image-credit" credit="	Jascobrasil"></div>
<a class="image-source" href="https://commons.wikimedia.org/wiki/File:Hplc_jasco.jpg" target="_blank"></a>
<img src="hplc-machine.png">
</div>
</div>

<h3>The Column and the Solvent</h3>

<div class="text-block">
<p>Confusingly, there are two variants in use in HPLC depending on the relative polarity of the solvent and the stationary phase.</p>
</div>

<h4>Normal phase HPLC</h4>

<div class="text-block">
<p>This is essentially just the same as you will already have read about in thin layer chromatography or column chromatography. Although it is described as "normal", it isn't the most commonly used form of HPLC.</p>
<p>The column is filled with tiny silica particles, and the solvent is non-polar – hexane, for example. A typical column has an internal diameter of 4.6 mm (and may be less than that), and a length of 150 to 250 mm.</p>
<p>Polar compounds in the mixture being passed through the column will stick longer to the polar silica than non-polar compounds will. The non-polar ones will therefore pass more quickly through the column.</p>
</div>

<h4>Reversed phase HPLC </h4>

<div class="text-block">
<p>In this case, the column size is the same, but the silica is modified to make it non-polar by attaching long hydrocarbon chains to its surface – typically with either 8 or 18 carbon atoms in them. A polar solvent is used – for example, a mixture of water and an alcohol such as methanol.</p>
<p>In this case, there will be a strong attraction between the polar solvent and polar molecules in the mixture being passed through the column. There won't be as much attraction between the hydrocarbon chains attached to the silica (the stationary phase) and the polar molecules in the solution. Polar molecules in the mixture will therefore spend most of their time moving with the solvent.</p>
<p>Non-polar compounds in the mixture will tend to form attractions with the hydrocarbon groups because of van der Waals dispersion forces. They will also be less soluble in the solvent because of the need to break hydrogen bonds as they squeeze in between the water or methanol molecules, for example. They therefore spend less time in solution in the solvent and this will slow them down on their way through the column.</p>
<p>That means that now it is the polar molecules that will travel through the column more quickly.</p>
<p>Reversed phase HPLC is the most commonly used form of HPLC.</p>
</div>

<div class="note">
<p>Note: I have been a bit careful about how I have described the attractions of the non-polar molecules to the surface of the stationary phase. In particular, I have avoided the use of the word "adsorpion". Adsorption is when a molecule sticks to the surface of a solid. Especially if you had small molecules in your mixture, some could get in between the long C<sub>18</sub> chains to give what is essentially a solution.</p>

<p>You could therefore say that non-polar molecules were more soluble in the hydrocarbon on the surface of the silica than they are in the polar solvent – and so spend more time in this alternative "solvent". Where a solute divides itself between two different solvents because it is more soluble in one than the other, we call it partition.</p>

<p>So is this adsorption or partition? You could argue it both ways! Be prepared to find it described as either.</p>
</div>

<h2>Looking at the whole process</h2>

<div class="image-container">
<img src="hplc1.gif">
<p>A flow scheme for HPLC</p>
</div>

<h3>Injection of the sample</h3>

<div class="text-block">
<p>Injection of the sample is entirely automated, and you wouldn't be expected to know how this is done at this introductory level. Because of the pressures involved, it is not the same as in gas chromatography (if you have already studied that).</p>
</div>

<h3>Retention time</h3>

<div class="text-block">
<p>The time taken for a particular compound to travel through the column to the detector is known as its <i>retention time</i>. This time is measured from the time at which the sample is injected to the point at which the display shows a maximum peak height for that compound.</p>
<p>Different compounds have different retention times. For a particular compound, the retention time will vary depending on:</p>
</div>

<ul>
<li>the pressure used (because that affects the flow rate of the solvent)</li>
<li>the nature of the stationary phase (not only what material it is made of, but also particle size)</li>
<li>the exact composition of the solvent</li>
<li>the temperature of the column</li>
</ul>

<div class="text-block">
<p>That means that conditions have to be carefully controlled if you are using retention times as a way of identifying compounds.
</p>
</div>

<h3>The Detector</h3>

<div class="text-block">
<p>There are several ways of detecting when a substance has passed through the column. A common method which is easy to explain uses ultra-violet absorption.</p>
<p>Many organic compounds absorb UV light of various wavelengths. If you have a beam of UV light shining through the stream of liquid coming out of the column, and a UV detector on the opposite side of the stream, you can get a direct reading of how much of the light is absorbed.</p>
<p>The amount of light absorbed will depend on the amount of a particular compound that is passing through the beam at the time.</p>
</div>

<div class="image-container">
<img src="hplc2.gif">
</div>

<div class="text-block">
<p>You might wonder why the solvents used don't absorb UV light. They do! But different compounds absorb most strongly in different parts of the UV spectrum.</p>
<p>Methanol, for example, absorbs at wavelengths below 205 nm, and water below 190 nm. If you were using a methanol-water mixture as the solvent, you would therefore have to use a wavelength greater than 205 nm to avoid false readings from the solvent.</p>
</div>

<div class="note">
<p>Note: If you are interested, there is a whole section about <a href="../uvvisiblemenu.html">UV-visible spectroscopy</a> on the site. This explores the question of the absorption of UV and visible light by organic compounds in some detail.</p>
</div>

<h4>Interpreting the output from the detector</h4>

<div class="text-block">
<p>The output will be recorded as a series of peaks – each one representing a compound in the mixture passing through the detector and absorbing UV light. As long as you were careful to control the conditions on the column, you could use the retention times to help to identify the compounds present – provided, of course, that you (or somebody else) had already measured them for pure samples of the various compounds under those identical conditions.</p>
<p>But you can also use the peaks as a way of measuring the quantities of the compounds present. Let's suppose that you are interested in a particular compound, X.</p>
<p>If you injected a solution containing a known amount of pure X into the machine, not only could you record its retention time, but you could also relate the amount of X to the peak that was formed.</p>
<p>The area under the peak is proportional to the amount of X which has passed the detector, and this area can be calculated automatically by the computer linked to the display. The area it would measure is shown in green in the (very simplified) diagram.</p>
</div>

<div class="image-container">
<img src="hplc3.gif">
</div>

<div class="text-block">
<p>If the solution of X was less concentrated, the area under the peak would be less – although the retention time will still be the same. For example:</p>
</div>

<div class="image-container">
<img src="hplc4.gif">
</div>

<div class="text-block">
<p>This means that it is possible to calibrate the machine so that it can be used to find how much of a substance is present – even in very small quantities.</p>
<p>Be careful, though! If you had two different substances in the mixture (X and Y) could you say anything about their relative amounts? Not if you were using UV absorption as your detection method.</p>
</div>

<div class="image-container">
<img src="hplc5.gif">
</div>

<div class="text-block">
<p>In the diagram, the area under the peak for Y is less than that for X. That may be because there is less Y than X, but it could equally well be because Y absorbs UV light at the wavelength you are using less than X does. There might be large quantities of Y present, but if it only absorbed weakly, it would only give a small peak.</p>
</div>

<div class="note">
<p>Note: You will find a useful <a href="http://www.mournetrainingservices.co.uk/hplc_video.html">industry training video</a> which talks through the whole process by following this link. Linking to other sites is always a little bit hazardous because sites change. If you find that this link doesn't work, please contact me via the address on the <a href="../../about.html#top">About this site</a> page.</p>
</div>

<h4>Coupling HPLC to a mass spectrometer</h4>

<div class="text-block">
<p>This is where it gets really clever! When the detector is showing a peak, some of what is passing through the detector at that time can be diverted to a mass spectrometer. There it will give a fragmentation pattern which can be compared against a computer database of known patterns. That means that the identity of a huge range of compounds can be found without having to know their retention times.</p>
</div>

<div class="note">
<p>Note: If you have forgotten about mass spectrometry, explore the <a href="../masspecmenu.html">mass spectrometry menu</a> – particularly how a mass spectrometer works, and the formation of fragmentation patterns.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-chromhplc.pdf" target="_blank">Questions on HPLC</a>
<a href="../questions/a-chromhplc.pdf" target="_blank">Answers</a>
</div>
<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../chromatogrmenu.html#top">To the chromatography menu</a>
<a href="../../analysismenu.html#top">To the analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>

</div>
</body>

</html>