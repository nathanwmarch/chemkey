<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Infra-red Spectra – The Fingerprint Region | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="What the fingerprint region of an infra-red spectrum is, and how it can be used to identify a compound.">
<meta name="keywords" content="spectrum, spectra, infra-red, infrared, infra red, ir, spectroscopy, fingerprint">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Infra-red Spectra – The Fingerprint Region</h1>

<div class="text-block">
<p>This page explains what the fingerprint region of an infra-red spectrum is, and how it can be used to identify an organic molecule.</p>
</div>

<div class="note">
<p>Note: It would be helpful if you first read the introductory page on <a href="background.html#top">infra-red spectra</a> if you haven't already done so.</p>
</div>

<h2>What is the Fingerprint Region</h2>

<div class="text-block">
<p>This is a typical infra-red spectrum:</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="propan-1-ol" type="2d"></div>
</div>

<div class="image-container">
<img src="irpropan1olb.GIF">
</div>

<div class="note">
<p>Note: The infra-red spectra on this page have been produced from graphs taken from the Spectral Data Base System for Organic Compounds (<a href="http://sdbs.db.aist.go.jp/sdbs/cgi-bin/direct_frame_top.cgi">SDBS</a>) at the National Institute of Materials and Chemical Research in Japan. It is possible that small errors may have been introduced during the process of converting them for use on this site, but these won't affect the argument in any way.</p>
</div>

<div class="text-block">
<p>Each trough is caused because energy is being absorbed from that particular frequency of infra-red radiation to excite bonds in the molecule to a higher state of vibration – either stretching or bending.</p>
<p>Some of the troughs are easily used to identify particular bonds in a molecule. For example, the big trough at the left-hand side of the spectrum is used to identify the presence of an oxygen-hydrogen bond in an -OH group.</p>
</div>

<div class="note">
<p>Note: Using troughs in this way to <a href="interpret.html#top">identify particular bonds</a> is covered on a separate page.</p>
</div>

<div class="text-block">
<p>The region to the right-hand side of the diagram (from about 1500 to 500 cm<sup>-1</sup>) usually contains a very complicated series of absorptions. These are mainly due to all manner of bending vibrations within the molecule. This is called the fingerprint region.</p>
<p>It is much more difficult to pick out individual bonds in this region than it is in the "cleaner" region at higher wavenumbers. The importance of the fingerprint region is that each different compound produces a different pattern of troughs in this part of the spectrum.</p>
</div>

<h2>Using the Fingerprint Region</h2>

<div class="text-block">
<p>Compare the infra-red spectra of propan-1-ol and propan-2-ol. Both compounds contain exactly the same bonds. Both compounds have very similar troughs in the area around 3000 cm<sup>-1</sup> – but compare them in the fingerprint region between 1500 and 500 cm<sup>-1</sup>.</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="propan-1-ol" type="2d"></div>
</div>

<div class="image-container">
<img src="irpropan1olb.GIF">
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="propan-2-ol" type="2d"></div>
</div>

<div class="image-container">
<img src="irpropan2ol.GIF">
</div>

<div class="text-block">
<p>The pattern in the fingerprint region is completely different and could therefore be used to identify the compound.</p>
<p>So to positively identify an unknown compound, use its infra-red spectrum to identify what sort of compound it is by looking for specific bond absorptions. That might tell you, for example, that you had an alcohol because it contained an -OH group.</p>
<p>You would then compare the fingerprint region of its infra-red spectrum with known spectra measured under exactly the same conditions to find out which alcohol (or whatever) you had.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-irfingerprint.pdf" target="_blank">Questions on the fingerprint region</a>
<a href="../questions/a-irfingerprint.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../irmenu.html#top">To the infra-red spectroscopy menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>