<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Infra-red Spectroscopy Background | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="A simple explanation of how an infra-red spectrum is formed.">
<meta name="keywords" content="spectrum, spectra, infra-red, infrared, infra red, ir, spectroscopy">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js">
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Infra-red Spectroscopy Background</h1>

<div class="text-block">
<p>This page describes what an infra-red spectrum is and how it arises from bond vibrations within organic molecules.</p>
</div>

<h2>How an Infra-red Spectrum is Produced</h2>

<div class="text-block">
<p>You probably know that visible light is made up of a continuous range of different electromagnetic frequencies – each frequency can be seen as a different colour. Infra-red radiation also consists of a continuous range of frequencies – it so happens that our eyes can't detect them.</p>
<p>If you shine a range of infra-red frequencies one at a time through a sample of an organic compound, you find that some frequencies get absorbed by the compound. A detector on the other side of the compound would show that some frequencies pass through the compound with almost no loss, but other frequencies are strongly absorbed.</p>
<p>How much of a particular frequency gets through the compound is measured as percentage transmittance.</p>
<p>A percentage transmittance of 100 would mean that all of that frequency passed straight through the compound without any being absorbed. In practice, that never happens – there is always some small loss, giving a transmittance of perhaps 95% as the best you can achieve.</p>
<p>A transmittance of only 5% would mean that nearly all of that particular frequency is absorbed by the compound. A very high absorption of this sort tells you important things about the bonds in the compound.</p>
</div>

<h2>What an Infra-red Spectrum Looks Like</h2>

<div class="text-block">
<p>A graph is produced showing how the percentage transmittance varies with the frequency of the infra-red radiation.</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="propan-1-ol" type="2d"></div>
</div>

<div class="image-container">
<img src="irpropan1ol.GIF">
</div>

<div class="note">
<p>Note: The infra-red spectra on this page have been produced from graphs taken from the Spectral Data Base System for Organic Compounds (<a href="http://sdbs.db.aist.go.jp/sdbs/cgi-bin/direct_frame_top.cgi">SDBS</a>) at the National Institute of Materials and Chemical Research in Japan. It is possible that small errors may have been introduced during the process of converting them for use on this site, but these won't affect the argument in any way.</p>
</div>

<div class="text-block">
<p>Notice that an unusual measure of frequency is used on the horizontal axis. Wavenumber is defined like this:</p>
</div>

<div class="block-formula">
wavenumber = \frac{1}{\text{wavelength in cm}} cm^{-1}
</div>

<div class="text-block">
<p>Don't worry about this – just accept it!</p>
<p>Similarly, don't worry about the change of scale half-way across the horizontal axis. You will find infra-red spectra where the scale is consistent all the way across, infra-red spectra where the scale changes at around 2000 cm<sup>-1</sup>, and very occasionally where the scale changes again at around 1000 cm<sup>-1</sup>.</p>
<p>As you will see when we look at how to interpret infra-red spectra, this doesn't cause any problems – you simply need to be careful reading the horizontal scale.</p>
</div>

<h2>What causes some frequencies to be absorbed?</h2>

<div class="text-block">
<p>Each frequency of light (including infra-red) has a certain energy. If a particular frequency is being absorbed as it passes through the compound being investigated, it must mean that its energy is being transferred to the compound.</p>
<p>Energies in infra-red radiation correspond to the energies involved in bond vibrations.</p>
</div>

<h4>Bond stretching</h4>

<div class="text-block">
<p>In covalent bonds, atoms aren't joined by rigid links – the two atoms are held together because both nuclei are attracted to the same pair of electrons. The two nuclei can vibrate backwards and forwards – towards and away from each other – around an average position.</p>
<p>The diagram shows the stretching that happens in a carbon-oxygen single bond. There will, of course, be other atoms attached to both the carbon and the oxygen. For example, it could be the carbon-oxygen bond in methanol, CH<sub>3</sub>OH.</p>
</div>

<div class="image-container">
<img src="stretch.GIF">
</div>

<div class="text-block">
<p>The energy involved in this vibration depends on things like the length of the bond and the mass of the atoms at either end. That means that each different bond will vibrate in a different way, involving different amounts of energy.</p>
<p>Bonds are vibrating all the time, but if you shine exactly the right amount of energy on a bond, you can kick it into a higher state of vibration. The amount of energy it needs to do this will vary from bond to bond, and so each different bond will absorb a different frequency (and hence energy) of infra-red radiation.</p>
</div>

<h4>Bond bending</h4>

<div class="text-block">
<p>As well as stretching, bonds can also bend. The diagram shows the bending of the bonds in a water molecule. The effect of this, of course, is that the bond angle between the two hydrogen-oxygen bonds fluctuates slightly around its average value. Imagine a lab model of a water molecule where the atoms are joined together with springs. These bending vibrations are what you would see if you shook the model gently.</p>
</div>

<div class="image-container">
<img src="bend.GIF">
</div>

<div class="text-block">
<p>Again, bonds will be vibrating like this all the time and, again, if you shine exactly the right amount of energy on the bond, you can kick it into a higher state of vibration. Since the energies involved with the bending will be different for each kind of bond, each different bond will absorb a different frequency of infra-red radiation in order to make this jump from one state to a higher one.</p>
</div>

<h2>Tying All This Together</h2>

<div class="text-block">
<p>Look again at the infra-red spectrum of propan-1-ol, CH
<sub>3</sub>CH
<sub>2</sub>CH
<sub>2</sub>OH:</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="propan-1-ol" type="2d"></div>
</div>

<div class="image-container">
<img src="irpropan1ola.GIF">
</div>

<div class="text-block">
<p>In the diagram, three sample absorptions are picked out to show you the bond vibrations which produced them. Notice that bond stretching and bending produce different troughs in the spectrum.</p>
</div>

<div class="note">
<p>Note: If you want to go straight on to explore how you <a href="interpret.html#top">interpret infra-red spectra</a>, you could follow this link rather than going via the menu.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-irintro.pdf" target="_blank">Questions on an introduction to IR spectra</a>
<a href="../questions/a-irintro.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../irmenu.html#top">To the infra-red spectroscopy menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>

</div>
</body>

</html>