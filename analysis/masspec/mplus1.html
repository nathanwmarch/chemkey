<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Mass Spectra – The M+1 Peak | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="Explains how the M+1 peak in a mass spectrum can be used to estimate the number of carbon atoms in an organic compound">
<meta name="keywords" content="mass spectrometer, mass spectrometry, mass spectrum, mass spectra, spectrum, spectra, mass, organic, compounds, molecular ion, m+1">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Mass Spectra – The M+1 Peak</h1>

<div class="text-block">
<p>This page explains how the M+1 peak in a mass spectrum can be used to estimate the number of carbon atoms in an organic compound.</p>
</div>

<div class="note">
<p>Note: This is a small corner of mass spectrometry. It would be a good idea not to attack this page unless you have a reasonable idea about how a mass spectrum is produced and the sort of information you can get from it. If you haven't already done so, explore the <a href="../masspecmenu.html#top">mass spectrometry menu</a> before you go on.</p>
</div>

<h2>What Causes the M+1 Peak?</h2>

<h3>What is an M+1 Peak?</h3>

<div class="text-block">
<p>If you had a complete (rather than a simplified) mass spectrum, you will find a small line 1 m/z unit to the right of the main molecular ion peak. This small peak is called the M+1 peak.</p>
</div>

<div class="image-container">
<img src="mplus1peak.GIF">
</div>

<div class="text-block">
<p>In questions at this level (UK A-level or its equivalent), the M+1 peak is often left out to avoid confusion – particularly if you were being asked to find the relative formula mass of the compound from the molecular ion peak.</p>
</div>

<h3>The Carbon-13 Isotope</h3>

<div class="text-block">
<p>The M+1 peak is caused by the presence of the <sup>13</sup>C isotope in the molecule. <sup>13</sup>C is a stable isotope of carbon – don't confuse it with the <sup>14</sup>C isotope which is radioactive. Carbon-13 makes up 1.11% of all carbon atoms.</p>
<p>If you had a simple compound like methane, CH<sub>4</sub>, approximately 1 in every 100 of these molecules will contain carbon-13 rather than the more common carbon-12. That means that 1 in every 100 of the molecules will have a mass of 17 (13 + 4) rather than 16 (12 + 4).</p>
<p>The mass spectrum will therefore have a line corresponding to the molecular ion [<sup>13</sup>CH<sub>4</sub>]<sup>+</sup> as well as [<sup>12</sup>CH<sub>4</sub>]<sup>+</sup>.</p>
<p>The line at m/z = 17 will be much smaller than the line at m/z = 16 because the carbon-13 isotope is much less common. Statistically you will have a ratio of approximately 1 of the heavier ions to every 99 of the lighter ones. That's why the M+1 peak is much smaller than the M<sup>+</sup> peak.</p>
</div>

<h2>Using the M+1 Peak</h2>

<h3>What Happens When There is More Than 1 Carbon Atom in the Compound?</h3>

<div class="text-block">
<p>Imagine a compound containing 2 carbon atoms. Either of them has an approximately 1 in 100 chance of being <sup>13</sup>C.</p>
</div>

<div class="image-container">
<img src="mplus1stats.GIF">
</div>

<div class="text-block">
<p>There's therefore a 2 in 100 chance of the molecule as a whole containing one <sup>13</sup>C atom rather than a <sup>12</sup>C atom – which leaves a 98 in 100 chance of both atoms being <sup>12</sup>C.</p>
<p>That means that the ratio of the height of the M+1 peak to the M+ peak will be approximately 2 : 98. That's pretty close to having an M+1 peak approximately 2% of the height of the M<sup>+</sup> peak.</p>
</div>

<div class="note">
<p>Note: You might wonder why both atoms can't be carbon-13, giving you an M+2 peak. They can – and do! But statistically the chance of both carbons being <sup>13</sup>C is approximately 1 in 10,000. The M+2 peak will be so small that you couldn't observe it.</p>
</div>

<h3>Using the Relative Peak Heights to Predict the Number of Carbon Atoms</h3>

<h4>If there are small numbers of carbon atoms</h4>

<div class="text-block">
<p>If you measure the peak height of the M+1 peak as a percentage of the peak height of the M<sup>+</sup> peak, that gives you the number of carbon atoms in the compound.</p>
<p>We've just seen that a compound with 2 carbons will have an M+1 peak approximately 2% of the height of the M+ peak.</p>
<p>Similarly, you could show that a compound with 3 carbons will have the M+1 peak at about 3% of the height of the M<sup>+</sup> peak.</p>
</div>

<h4>With larger numbers of carbon atoms</h4>

<div class="text-block">
<p>The approximations we are making won't hold with more than 2 or 3 carbons. The proportion of carbon atoms which are <sup>13</sup>C isn't 1% – it's 1.11%. And the appoximation that a ratio of 2 : 98 is about 2% doesn't hold as the small number increases.</p>
<p>Consider a molecule with 5 carbons in it. You could work out that 5.55 (5 x 1.11) molecules will contain 1 <sup>13</sup>C to every 94.45 (100 – 5.55) which contain only <sup>12</sup>C. If you convert that to how tall the M+1 peak is as a percentage of the M<sup>+</sup> peak, you get an answer of 5.9% (5.55/94.45 x 100). That's close enough to 6% that you might assume wrongly that there are 6 carbon atoms.</p>
<p>Above 3 carbon atoms, then, you shouldn't really be making the approximation that the height of the M+1 peak as a percentage of the height of the M<sup>+</sup> peak tells you the number of carbons – you will need to do some fiddly sums!</p>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../masspecmenu.html#top">To the mass spectrometry menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>