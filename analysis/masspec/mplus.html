<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Mass Spectra – The Molecular Ion (M+) Peak | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="Explains how the molecular ion peak in a mass spectrum can be used to find the relative formula mass or the molecular formula of an organic compound">
<meta name="keywords" content="mass spectrometer, mass spectrometry, mass spectrum, mass spectra, spectrum, spectra, mass, organic, compounds, molecular ion, m+">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Mass Spectra – The Molecular Ion (M+) Peak</h1>

<div class="text-block">
<p>This page explains how to find the relative formula mass (relative molecular mass) of an organic compound from its mass spectrum. It also shows how high resolution mass spectra can be used to find the molecular formula for a compound.</p>
</div>

<h2>Using a Mass Spectrum to Find Relative Formula Mass</h2>

<h3>The Formation of Molecular Ions</h3>

<div class="text-block">
<p>When the vaporised organic sample passes into the ionisation chamber of a mass spectrometer, it is bombarded by a stream of electrons. These electrons have a high enough energy to knock an electron off an organic molecule to form a positive ion. This ion is called the molecular ion.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about how a mass spectrum is produced, it might be worth taking a quick look at the page describing <a href="howitworks.html#top">how a mass spectrometer works</a>.</p>
</div>

<div class="text-block">
<p>The molecular ion is often given the symbol M<sup>+</sup> or M• – the dot in this second version represents the fact that somewhere in the ion there will be a single unpaired electron. That's one half of what was originally a pair of electrons – the other half is the electron which was removed in the ionisation process.</p>
<p>The molecular ions tend to be unstable and some of them break into smaller fragments. These fragments produce the familiar stick diagram. Fragmentation is irrelevant to what we are talking about on this page – all we're interested in is the molecular ion.</p>
</div>

<div class="note">
<p>Note: If you are interested in a detailed look at <a href="fragment.html#top">fragmentation patterns</a> you could follow this link.</p>
</div>

<h3>Using the Molecular Ion to Find the Relative Formula Mass</h3>

<div class="text-block">
<p>In the mass spectrum, the heaviest ion (the one with the greatest m/z value) is likely to be the molecular ion. A few compounds have mass spectra which don't contain a molecular ion peak, because all the molecular ions break into fragments. That isn't a problem you are likely to meet at A-level.</p>
<p>For example, in the mass spectrum of pentane, the heaviest ion has an m/z value of 72.</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="pentane" type="2d">
</div>

<div class="image-container">
<img src="pentanemspec2.GIF">
</div>

<div class="note">
<p>Note: This mass spectrum has been drawn using data from the Spectral Data Base System for Organic Compounds (<a href="http://sdbs.db.aist.go.jp/sdbs/cgi-bin/direct_frame_top.cgi">SDBS</a>) at the National Institute of Materials and Chemical Research in Japan. It has been simplified by omitting all the minor lines with peak heights of 2% or less of the base peak (the tallest peak).</p>
</div>

<div class="text-block">
<p>Because the largest m/z value is 72, that represents the largest ion going through the mass spectrometer – and you can reasonably assume that this is the molecular ion. The relative formula mass of the compound is therefore 72.</p>
</div>

<div class="note">
<p>Note: This assumes that the charge on the ion is 1+. That's always the case when you are interpreting these mass spectra.</p>
</div>

<div class="text-block">
<p>Finding the relative formula mass (relative molecular mass) from a mass spectrum is therefore trivial. Look for the peak with the highest value for m/z, and that value is the relative formula mass of the compound.</p>
<p>There are, however, complications which arise because of the possibility of different isotopes (either of carbon or of chlorine or bromine) in the molecular ion. These cases are dealt with on separate pages.</p>
</div>

<div class="note">
<p>Note: The presence of the carbon-13 isotope in a molecular ion causes a little peak 1 unit to the right of the M+ peak. This is called the<a href="mplus1.html#top">M+1 peak</a>. The presence of a chlorine atom in a compound causes two peaks in the molecular ion region – the M+ peak and the <a href="mplus2.html#top">M+2 peak</a> depending on whether the particular molecular ion contains a chlorine-35 or chlorine-37 isotope. Bromine creates a similar problem. Follow these links if you are interested – or explore them later via the mass spectrometry menu.</p>
</div>

<h2>Using a Mass Spectrum to Find a Molecular Formula</h2>

<div class="text-block">
<p>So far we've been looking at m/z values in a mass spectrum as whole numbers, but it's possible to get far more accurate results using a high resolution mass spectrometer. You can use that more accurate information about the mass of the molecular ion to work out the molecular formula of the compound.</p>
</div>

<h3>Accurate Isotopic Masses</h3>

<div class="text-block">
<p>For normal calculation purposes, you tend to use rounded-off relative isotopic masses. For example, you are familiar with the numbers:</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>
<sup>1</sup>H</td>
<td align="right">1</td>
</tr>
<tr>
<td>
<sup>12</sup>C</td>
<td align="right">12</td>
</tr>
<tr>
<td>
<sup>14</sup>N</td>
<td align="right">14</td>
</tr>
<tr>
<td>
<sup>16</sup>O</td>
<td align="right">16</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>To 4 decimal places, however, these are the relative isotopic masses:</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>
<sup>1</sup>H</td>
<td align="right">1.0078</td>
</tr>
<tr>
<td>
<sup>12</sup>C</td>
<td align="right">12.0000</td>
</tr>
<tr>
<td>
<sup>14</sup>N</td>
<td align="right">14.0031</td>
</tr>
<tr>
<td>
<sup>16</sup>O</td>
<td align="right">15.9949</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>The carbon value is 12.0000, of course, because all the other masses are measured on the carbon-12 scale which is based on the carbon-12 isotope having a mass of exactly 12.</p>
</div>

<h3>Using These Accurate Values to Find a Molecular Formula</h3>

<div class="text-block">
<p>Two simple organic compounds have a relative formula mass of 44 – propane, C<sub>3</sub>H<sub>8</sub>, and ethanal, CH<sub>3</sub>CHO. Using a high resolution mass spectrometer, you could easily decide which of these you had.</p>
<p>On a high resolution mass spectrometer, the molecular ion peaks for the two compounds give the following m/z values:</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>
C<sub>3</sub>H<sub>8</sub>
</td>
<td align="right">44.0624</td>
</tr>
<tr>
<td>
CH<sub>3</sub>CHO
</td>
<td align="right">44.0261</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>You can easily check that by adding up numbers from the table of accurate relative isotopic masses above.</p>
</div>

<h4>A possible exam question</h4>

<div class="text-block">
<p>A gas was known to contain only elements from the following list:</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>
<sup>1</sup>H
</td>
<td align="right">1.0078</td>
</tr>
<tr>
<td>
<sup>12</sup>C
</td>
<td align="right">12.0000</td>
</tr>
<tr>
<td>
<sup>14</sup>N
</td>
<td align="right">14.0031</td>
</tr>
<tr>
<td>
<sup>16</sup>O
</td>
<td align="right">15.9949</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>The gas had a molecular ion peak at m/z = 28.0312 in a high resolution mass spectrometer. What was the gas?</p>
<p>After a bit of playing around, you might reasonably come up with 3 gases which had relative formula masses of approximately 28 and which contained the elements from the list. They are N<sub>2</sub>, CO and C<sub>2</sub>H<sub>4</sub>.</p>
<p>Working out their accurate relative formula masses gives:</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>
N<sub>2</sub></td>
<td align="right">28.0062</td>
</tr>
<tr>
<td>
CO
</td>
<td align="right">27.9949</td>
</tr>
<tr>
<td>
C<sub>2</sub>H<sub>4</sub>
</td>
<td align="right">28.0312</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>The gas is obviously C<sub>2</sub>H<sub>4</sub>.</p>
<p>In an exam, you would hope that – apart from the most simple cases – you would be given the possible formulae to work from. Trying to work out all the possible things which might add up to the value you want is quite time-consuming – and it's easy to miss an important possibility!</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-msmplus.pdf" target="_blank">Questions on the molecular ion peak</a>
<a href="../questions/a-msmplus.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../masspecmenu.html#top">To the mass spectrometry menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>

</div>
</body>

</html>