<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Mass Spectra – The M+2 Peak | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="Explains how the M+2 peak in a mass spectrum arises from the presence of halogen atoms (chlorine or bromine) in an organic compound.">
<meta name="keywords" content="mass spectrometer, mass spectrometry, mass spectrum, mass spectra, spectrum, spectra, mass, organic, compounds, molecular ion, m+2, chlorine, bromine, halogen">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Mass Spectra – The M+2 Peak</h1>

<div class="text-block">
<p>This page explains how the M+2 peak in a mass spectrum arises from the presence of chlorine or bromine atoms in an organic compound. It also deals briefly with the origin of the M+4 peak in compounds containing two chlorine atoms.</p>
</div>

<div class="note">
<p>Note: Before you start this page, it would be a good idea to have a reasonable understanding about how a mass spectrum is produced and the sort of information you can get from it. If you haven't already done so, explore the <a href="../masspecmenu.html#top">mass spectrometry menu</a> before you go on.</p>
</div>

<h2>The Effect of Chlorine or Bromine Atoms on the Mass Spectrum of an Organic Compound</h2>

<h3>Compounds Containing Chlorine Atoms</h3>

<h4>One chlorine atom in a compound</h4>

<div class="sidebarleft">
<div class="chemical-structure" name="2-chloropropane" type="2d"></div>
</div>

<div class="image-container">
<img src="c3h7clmspec.GIF">
</div>

<div class="note">
<p>Note: All the mass spectra on this page have been drawn using data from the Spectral Data Base System for Organic Compounds (<a href="http://sdbs.db.aist.go.jp/sdbs/cgi-bin/direct_frame_top.cgi">SDBS</a>) at the National Institute of Materials and Chemical Research in Japan. With one exception, they have been simplified by omitting all the minor lines with peak heights of 2% or less of the base peak (the tallest peak).</p>
</div>

<div class="text-block">
<p>The molecular ion peaks (M<sup>+</sup> and M+2) each contain one chlorine atom – but the chlorine can be either of the two chlorine isotopes, <sup>35</sup>Cl and <sup>37</sup>Cl.</p>
<p>The molecular ion containing the <sup>35</sup>Cl isotope has a relative formula mass of 78. The one containing <sup>37</sup>Cl has a relative formula mass of 80 – hence the two lines at m/z = 78 and m/z = 80.</p>
<p>Notice that the peak heights are in the ratio of 3 : 1. That reflects the fact that chlorine contains 3 times as much of the <sup>35</sup>Cl isotope as the <sup>37</sup>Cl one. That means that there will be 3 times more molecules containing the lighter isotope than the heavier one.</p>
<p>So if you look at the molecular ion region, and find two peaks separated by 2 m/z units and with a ratio of 3 : 1 in the peak heights, that tells you that the molecule contains 1 chlorine atom.</p>
<p>You might also have noticed the same pattern at m/z = 63 and m/z = 65 in the mass spectrum above. That pattern is due to fragment ions also containing one chlorine atom – which could either be <sup>35</sup>Cl or <sup>37</sup>Cl. The fragmentation that produced those ions was:</p>
</div>

<div class="block-formula">
[\text{CH}_3\text{CClHCH}_3]{\bullet}^+ \longrightarrow \text{CH}_3{\overset{+}{\text{C}}}\text{ClH} + {\bullet}\text{CH}_3
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="fragment.html#top">fragmentation</a> you might like to have a look at this link.</p>
</div>

<h4>Two chlorine atoms in a compound</h4>

<div class="sidebarleft">
<div class="chemical-structure" name="1,2-dichloroethane" type="2d"></div>
</div>

<div class="image-container">
<img src="c2h4cl2mspec.GIF">
</div>

<div class="note">
<p>Note: This spectrum has been simplified by omitting all the minor lines with peak heights of less than 1% of the base peak (the tallest peak). This contains more minor lines than other mass spectra in this section. It was necessary because otherwise an important line in the molecular ion region would have been missing.</p>
</div>

<div class="text-block">
<p>The lines in the molecular ion region (at m/z values of 98, 100 ands 102) arise because of the various combinations of chlorine isotopes that are possible. The carbons and hydrogens add up to 28 – so the various possible molecular ions could be:</p>
</div>

<div class="block-formula">
\begin{aligned}
28 + 35 + 35 &= 98 \\
28 + 35 + 37 &= 100 \\
28 + 37 + 37 &= 102
\end{aligned}
</div>

<div class="text-block">
<p>If you have the necessary maths, you could show that the chances of these arrangements occurring are in the ratio of 9:6:1 – and this is the ratio of the peak heights. If you don't know the right bit of maths, just learn this ratio!</p>
<p>So if you have 3 lines in the molecular ion region (M<sup>+</sup>, M+2 and M+4) with gaps of 2 m/z units between them, and with peak heights in the ratio of 9:6:1, the compound contains 2 chlorine atoms.</p>
</div>

<h3>Compounds Containing Bromine Atoms</h3>

<div class="text-block">
<p>Bromine has two isotopes, <sup>79</sup>Br and <sup>81</sup>Br in an approximately 1:1 ratio (50.5 : 49.5 if you want to be fussy!). That means that a compound containing 1 bromine atom will have two peaks in the molecular ion region, depending on which bromine isotope the molecular ion contains.</p>
<p>Unlike compounds containing chlorine, though, the two peaks will be very similar in height.</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="bromoethane" type="2d"></div>
</div>

<div class="image-container">
<img src="c2h5brmspec.GIF">
</div>

<div class="text-block">
<p>The carbons and hydrogens add up to 29. The M+ and M+2 peaks are therefore at m/z values given by:</p>
</div>

<div class="block-formula">
\begin{aligned}
29 + 79 &= 108 \\
29 + 81 &= 110
\end{aligned}
</div>

<div class="text-block">
<p>So if you have two lines in the molecular ion region with a gap of 2 m/z units between them and with almost equal heights, this shows the presence of a bromine atom in the molecule.</p>
</div>
<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-msmplus2.pdf" target="_blank">Questions on the M+2 peak</a>
<a href="../questions/a-msmplus2.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p></dt>
<a href="../masspecmenu.html#top">To the mass spectrometry menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>

</div>
</body>

</html>