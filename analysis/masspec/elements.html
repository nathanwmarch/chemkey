<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Mass Spectra of Elements | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="How to interpret the mass spectrum of an element">
<meta name="keywords" content="mass spectrometer, mass spectrometry, mass spectrum, mass spectra, spectrum, spectra, mass, element, relative atomic mass, atomic mass, atomic weight, ram, isotopes">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Mass Spectra of Elements</h1>

<div class="text-block">
<p>This page looks at the information you can get from the mass spectrum of an element. It shows how you can find out the masses and relative abundances of the various isotopes of the element and use that information to calculate the relative atomic mass of the element.</p>
<p>It also looks at the problems thrown up by elements with diatomic molecules – like chlorine, Cl<sub>2</sub>.</p>
</div>

<h2>The Mass Spectrum of Monatomic Elements</h2>

<div class="text-block">
<p>Monatomic elements include all those except for things like chlorine, Cl<sub>2</sub>, with molecules containing more than one atom.</p>
</div>

<h3>The Mass spectrum for Boron</h3>

<div class="image-container">
<img src="bmasspec.GIF">
</div>

<div class="note">
<p>Note: If you need to know how this diagram is obtained, you should read the page describing
<a href="howitworks.html#top">how a mass spectrometer works</a>.</p>
</div>

<h4>The number of isotopes</h4>

<div class="text-block">
<p>The two peaks in the mass spectrum shows that there are 2 isotopes of boron – with relative isotopic masses of 10 and 11 on the <sup>12</sup>C scale.</p>
</div>

<div class="note">
<p>Notes: Isotopes are atoms of the same element (and so with the same number of protons), but with different masses due to having different numbers of neutrons.</p>

<p>We are assuming (and shall do all through this page) that all the ions recorded have a charge of 1+. That means that the mass/charge ratio (m/z) gives you the mass of the isotope directly.</p>

<p>The carbon-12 scale is a scale on which the mass of the <sup>12</sup>C isotope weighs exactly 12 units.</p>
</div>

<h4>The abundance of the isotopes</h4>

<div class="text-block">
<p>The relative sizes of the peaks gives you a direct measure of the relative abundances of the isotopes. The tallest peak is often given an arbitrary height of 100 – but you may find all sorts of other scales used. It doesn't matter in the least.</p>
<p>You can find the relative abundances by measuring the lines on the stick diagram.</p>
<p>In this case, the two isotopes (with their relative abundances) are:</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>boron-10</td>
<td></td>
<td align="right">23</td>
</tr>
<tr>
<td>boron-11</td>
<td></td>
<td align="right">100</td>
</tr>
</tbody>
</table>

<h4>Working out the relative atomic mass</h4>

<div class="text-block">
<p>The relative atomic mass (RAM) of an element is given the symbol A<sub>r</sub> and is defined as:</p>
</div>

<div class="definition">
<p>The relative atomic mass of an element is the weighted average of the masses of the isotopes on a scale on which a carbon-12 atom has a mass of exactly 12 units.</p>
</div>

<div class="text-block">
<p>A "weighted average" allows for the fact that there won't be equal amounts of the various isotopes. The example coming up should make that clear.</p>
<p>Suppose you had 123 typical atoms of boron. 23 of these would be <sup>10</sup>B and 100 would be <sup>11</sup>B.</p>
<p>The total mass of these would be (23 \times 10) + (100 \times 11) = 1330</p>
<p>The average mass of these 123 atoms would be 1330 / 123 = 10.8 (to 3 significant figures).</p>
<p>10.8 is the relative atomic mass of boron.</p>
<p>Notice the effect of the "weighted" average. A simple average of 10 and 11 is, of course, 10.5. Our answer of 10.8 allows for the fact that there are a lot more of the heavier isotope of boron – and so the "weighted" average ought to be closer to that.</p>
</div>

<h3>The Mass Spectrum for Zirconium</h3>

<div class="image-container">
<img src="zrmasspec.GIF">
</div>

<h4>The number of isotopes</h4>

<div class="text-block">
<p>The 5 peaks in the mass spectrum shows that there are 5 isotopes of zirconium – with relative isotopic masses of 90, 91, 92, 94 and 96 on the <sup>12</sup>C scale.</p>
</div>

<h4>The abundance of the isotopes</h4>

<div class="text-block">
<p>This time, the relative abundances are given as percentages. Again you can find these relative abundances by measuring the lines on the stick diagram.</p>
<p>In this case, the 5 isotopes (with their relative percentage abundances) are:</p>
</div>

<table class="convertable-table">
<thead>
<tr><th>isotope</th><th>abundance<br>/ %</th></tr>
</thead>
<tbody>
<tr>
<td>zirconium-90</td>
<td align="right">51.5</td>
</tr>
<tr>
<td>zirconium-91</td>
<td align="right">11.2</td>
</tr>
<tr>
<td>zirconium-92</td>
<td align="right">17.1</td>
</tr>
<tr>
<td>zirconium-94</td>
<td align="right">17.4</td>
</tr>
<tr>
<td>zirconium-96</td>
<td align="right">2.8</td>
</tr>
</tbody>
</table>

<div class="note">
<p>Note: You almost certainly wouldn't be able to measure these peaks to this degree of accuracy, but your examiners may well give you the data in number form anyway. We'll do the sum with the more accurate figures.</p>
</div>

<h4>Working out the relative atomic mass</h4>

<div class="text-block">
<p>Suppose you had 100 typical atoms of zirconium. 51.5 of these would be <sup>90</sup>Zr, 11.2 would be <sup>91</sup>Zr and so on.</p>
</div>

<div class="note">
<p>Note: If you object to the idea of having 51.5 atoms or 11.2 atoms and so on, just assume you've got 1000 atoms instead of 100. That way you will have 515 atoms, 112 atoms, etc. Most people don't get in a sweat over this, and just use the numbers as they are!</p>
</div>

<div class="text-block">
<p>The total mass of these 100 typical atoms would be</p>
</div>

<div class="block-formula full-width">
(51.5 \times 90) + (11.2 \times 91) + (17.1 \times 92) + (17.4 \times 94) + (2.8 \times 96) = 9131.8
</div>

<div class="text-block">
<p>The average mass of these 100 atoms would be 9131.8 / 100 = 91.3 (to 3 significant figures).</p>
<p>91.3 is the relative atomic mass of zirconium.</p>
</div>

<h2>The Mass Spectrum of Chlorine</h2>

<div class="text-block">
<p>Chlorine is taken as typical of elements with more than one atom per molecule. We'll look at its mass spectrum to show the sort of problems involved.</p>
<p>Chlorine has two isotopes, <sup>35</sup>Cl and <sup>37</sup>Cl, in the approximate ratio of 3 atoms of <sup>35</sup>Cl to 1 atom of <sup>37</sup>Cl. You might suppose that the mass spectrum would look like this:</p>
</div>

<div class="image-container">
<img src="clmasspec.GIF">
</div>

<div class="text-block">
<p>You would be wrong!</p>
<p>The problem is that chlorine consists of molecules, not individual atoms. When chlorine is passed into the ionisation chamber, an electron is knocked off the molecule to give a molecular ion, Cl<sub>2</sub><sup>+</sup>. These ions won't be particularly stable, and some will fall apart to give a chlorine atom and a Cl<sup>+</sup> ion. The term for this is fragmentation.</p>
</div>

<div class="block-formula">
{\text{Cl}_2}^+ \longrightarrow \text{Cl} + \text{Cl}^+
</div>

<div class="text-block">
<p>If the Cl atom formed isn't then ionised in the ionisation chamber, it simply gets lost in the machine – neither accelerated nor deflected.</p>
<p>The Cl<sup>+</sup> ions will pass through the machine and will give lines at 35 and 37, depending on the isotope and you would get exactly the pattern in the last diagram. The problem is that you will also record lines for the unfragmented Cl<sup>2+</sup> ions.</p>
<p>Think about the possible combinations of chlorine-35 and chlorine-37 atoms in a Cl<sup>2+</sup> ion.</p>
<p>Both atoms could be <sup>35</sup>Cl, both atoms could be <sup>37</sup>Cl, or you could have one of each sort. That would give you total masses of the Cl<sub>2</sub><sup>+</sup> ion of:</p>
</div>

<div class="block-formula">
\begin{aligned} 35 + 35 = 70 \\ 35 + 37 = 72 \\ 37 + 37 = 74 \end{aligned}
</div>

<div class="text-block">
<p>That means that you would get a set of lines in the m/z = 70 region looking like this:</p>
</div>

<div class="image-container">
<img src="cl2masspec.GIF">
</div>

<div class="text-block">
<p>These lines would be in addition to the lines at 35 and 37.</p>
<p>The relative heights of the 70, 72 and 74 lines are in the ratio 9:6:1. If you know the right bit of maths, it's very easy to show this. If not, don't worry. Just remember that the ratio is 9:6:1.</p>
<p>What you can't do is make any predictions about the relative heights of the lines at 35/37 compared with those at 70/72/74. That depends on what proportion of the molecular ions break up into fragments. That's why you've got the chlorine mass spectrum in two separate bits so far. You must realise that the vertical scale in the diagrams of the two parts of the spectrum isn't the same.</p>
<p>The overall mass spectrum looks like this:</p>
</div>

<div class="image-container">
<img src="cl2overallms.gif">
</div>

<div class="note">
<p>Note: This is based on information from the <a href="http://webbook.nist.gov/chemistry/">NIST Chemistry WebBook</a>. NIST is the US National Institute of Standards and Technology.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-mselements.pdf" target="_blank">Questions on the mass spectra of elements</a>
<a href="../questions/a-mselements.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../masspecmenu.html#top">To the mass spectrometry menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>