<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Mass Spectra – Organic Compound Fragmentation Patterns | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Fragmentation patterns in the mass spectra of organic compounds – how they arise and how to get information from them.">
<meta name="keywords" content="mass spectrometer, mass spectrometry, mass spectrum, mass spectra, spectrum, spectra, mass, organic, compounds, fragmentation">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Fragmentation Patterns in the Mass Spectra of Organic Compounds</h1>

<div class="text-block">
<p>This page looks at how fragmentation patterns are formed when organic molecules are fed into a mass spectrometer, and how you can get information from the mass spectrum.</p>
</div>

<h2>The Origin of Fragmentation Patterns</h2>

<h3>The Formation of Molecular Ions</h3>

<div class="text-block">
<p>When the vaporised organic sample passes into the ionisation chamber of a mass spectrometer, it is bombarded by a stream of electrons. These electrons have a high enough energy to knock an electron off an organic molecule to form a positive ion. This ion is called the molecular ion – or sometimes the parent ion.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about how a mass spectrum is produced, it might be worth taking a quick look at the page describing <a href="howitworks.html#top">how a mass spectrometer works</a>.</p>
</div>

<div class="text-block">
<p>The molecular ion is often given the symbol M<sup>+</sup> or M⋅ – the dot in this second version represents the fact that somewhere in the ion there will be a single unpaired electron. That's one half of what was originally a pair of electrons – the other half is the electron which was removed in the ionisation process.</p>
</div>

<h3>Fragmentation</h3>

<div class="text-block">
<p>The molecular ions are energetically unstable, and some of them will break up into smaller pieces. The simplest case is that a molecular ion breaks into two parts – one of which is another positive ion, and the other is an uncharged free radical.</p>
</div>

<div class="block-formula">
{\text{M}}{\bullet}^+ \longrightarrow \text{X}^+ + {\text{Y}}{\bullet}
</div>

<div class="note">
<p>Note: A free radical is an atom or group of atoms which contains a single unpaired electron. More complicated break-ups are beyond the scope of A-level syllabuses.</p>
</div>

<div class="text-block">
<p>The uncharged free radical won't produce a line on the mass spectrum. Only charged particles will be accelerated, deflected and detected by the mass spectrometer. These uncharged particles will simply get lost in the machine – eventually, they get removed by the vacuum pump.</p>
<p>The ion, X<sup>+</sup>, will travel through the mass spectrometer just like any other positive ion – and will produce a line on the stick diagram.</p>
<p>All sorts of fragmentations of the original molecular ion are possible – and that means that you will get a whole host of lines in the mass spectrum. For example, the mass spectrum of pentane looks like this:</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="pentane" type="2d"></div>
</div>

<div class="image-container">
<img src="pentanemspec.GIF">
</div>

<div class="note">
<p>Note: All the mass spectra on this page have been drawn using data from the Spectral Data Base System for Organic Compounds (<a href="http://sdbs.db.aist.go.jp/sdbs/cgi-bin/direct_frame_top.cgi">SDBS</a>) at the National Institute of Materials and Chemical Research in Japan. They have been simplified by omitting all the minor lines with peak heights of 2% or less of the base peak (the tallest peak).</p>
</div>

<div class="text-block">
<p>It's important to realise that the pattern of lines in the mass spectrum of an organic compound tells you something quite different from the pattern of lines in the mass spectrum of an element. With an element, each line represents a different isotope of that element. With a compound, each line represents a different fragment produced when the molecular ion breaks up.</p>
</div>

<div class="note">
<p>Note: If you are interested in the <a href="elements.html#top">mass spectra of elements</a>, you could follow this link.</p>
</div>

<h3>The Molecular Ion Peak and the Base Peak</h3>

<div class="text-block">
<p>In the stick diagram showing the mass spectrum of pentane, the line produced by the heaviest ion passing through the machine (at m/z = 72) is due to the molecular ion.</p>
</div>

<div class="note">
<p>Note: You have to be a bit careful about this, because in some cases, the molecular ion is so unstable that every single one of them splits up, and none gets through the machine to register in the mass spectrum. You are very unlikely to come across such a case at A-level.</p>
</div>

<div class="text-block">
<p>The tallest line in the stick diagram (in this case at m/z = 43) is called the base peak. This is usually given an arbitrary height of 100, and the height of everything else is measured relative to this. The base peak is the tallest peak because it represents the commonest fragment ion to be formed – either because there are several ways in which it could be produced during fragmentation of the parent ion, or because it is a particularly stable ion.</p>
</div>

<h2>Using Fragmentation Patterns</h2>

<div class="text-block">
<p>This section will ignore the information you can get from the molecular ion (or ions). That is covered in three other pages which you can get at via the mass spectrometry menu. You will find a link at the bottom of the page.</p>
</div>

<h3>Working Out Which Ion Produces Which Line</h3>

<div class="text-block">
<p>This is generally the simplest thing you can be asked to do.</p>
</div>

<h4>The mass spectrum of pentane</h4>

<div class="text-block">
<p>Let's have another look at the mass spectrum for pentane:</p>
</div>

<div class="image-container">
<img src="pentanemspec.GIF">
</div>

<div class="text-block">
<p>What causes the line at m/z = 57?</p>
<p>How many carbon atoms are there in this ion? There can't be 5 because 5 × 12 = 60. What about 4? 4 × 12 = 48. That leaves 9 to make up a total of 57. How about C<sub>4</sub>H<sub>9</sub><sup>+</sup> then?</p>
<p>C<sub>4</sub>H<sub>9</sub><sup>+</sup> would be [CH<sub>3</sub>CH<sub>2</sub>CH<sub>2</sub>CH<sub>2</sub>]<sup>+</sup>, and this would be produced by the following fragmentation:</p>
</div>

<div class="block-formula">
[\text{CH}_3\text{CH}_2\text{CH}_2\text{CH}_2\text{CH}_3]{\bullet}^+ \longrightarrow [\text{CH}_3\text{CH}_2\text{CH}_2\text{CH}_2]^+ + {\bullet}\text{CH}_3
</div>

<div class="text-block">
<p>The methyl radical produced will simply get lost in the machine.</p>
<p>The line at m/z = 43 can be worked out similarly. If you play around with the numbers, you will find that this corresponds to a break producing a 3-carbon ion:</p>
</div>

<div class="block-formula">
[\text{CH}_3\text{CH}_2\text{CH}_2\text{CH}_2\text{CH}_3]{\bullet}^+ \longrightarrow [\text{CH}_3\text{CH}_2\text{CH}_2]^+ + {\bullet}\text{CH}_2\text{CH}_3
</div>

<div class="text-block">
<p>The line at m/z = 29 is typical of an ethyl ion, [CH<sub>3</sub>CH<sub>2</sub>]<sup>+</sup>:</p>
</div>

<div class="block-formula">
[\text{CH}_3\text{CH}_2\text{CH}_2\text{CH}_2\text{CH}_3]{\bullet}^+ \longrightarrow [\text{CH}_3\text{CH}_2]^+ + {\bullet}\text{CH}_2\text{CH}_2\text{CH}_3
</div>

<div class="text-block">
<p>The other lines in the mass spectrum are more difficult to explain. For example, lines with m/z values 1 or 2 less than one of the easy lines are often due to loss of one or more hydrogen atoms during the fragmentation process. You are very unlikely to have to explain any but the most obvious cases in an A-level exam.</p>
</div>

<h4>The mass spectrum of pentan-3-one</h4>

<div class="image-container">
<img src="p3onemspec.GIF">
</div>

<div class="text-block">
<p>This time the base peak (the tallest peak – and so the commonest fragment ion) is at m/z = 57. But this isn't produced by the same ion as the same m/z value peak in pentane.</p>
<p>If you remember, the m/z = 57 peak in pentane was produced by [CH<sub>3</sub>CH<sub>2</sub>CH<sub>2</sub>CH<sub>2</sub>]<sup>+</sup>. If you look at the structure of pentan-3-one, it's impossible to get that particular fragment from it.</p>
<p>Work along the molecule mentally chopping bits off until you come up with something that adds up to 57. With a small amount of patience, you'll eventually find [CH<sub>3</sub>CH<sub>2</sub>CO]<sup>+</sup> – which is produced by this fragmentation:</p>
</div>

<div class="block-formula">
[\text{CH}_3\text{CH}_2\text{COCH}_2\text{CH}_2\text{CH}_3]{\bullet}^+ \longrightarrow [\text{CH}_3\text{CH}_2\text{CO}]^+ + {\bullet}\text{CH}_2\text{CH}_3
</div>

<div class="text-block">
<p>You would get exactly the same products whichever side of the CO group you split the molecular ion.</p>
<p>The m/z = 29 peak is produced by the ethyl ion – which once again could be formed by splitting the molecular ion either side of the CO group.</p>
</div>

<div class="block-formula">
[\text{CH}_3\text{CH}_2\text{COCH}_2\text{CH}_2\text{CH}_3]{\bullet}^+ \longrightarrow [\text{CH}_3\text{CH}_2]^+ + {\bullet}\text{COCH}_2\text{CH}_3
</div>

<h3>Peak Heights and the Stability of Ions</h3>

<div class="text-block">
<p>The more stable an ion is, the more likely it is to form. The more of a particular sort of ion that's formed, the higher its peak height will be. We'll look at two common examples of this.</p>
</div>

<h4>Examples involving carbocations (carbonium ions)</h4>

<div class="note">
<p>Important! If you don't know what a <a href="../../mechanisms/eladd/carbonium.html#top">carbocation (or carbonium ion)</a> is, or why the various sorts vary in stability, it's essential that you follow this link before you go on.</p>
</div>

<div class="summary">
Order of stability of carbocations primary &lt; secondary &lt; tertiary
</div>

<div class="note">
<p>Note: The symbol "&lt;" means "is less than". So what this is saying is that primary ions are less stable than secondary
ones which in turn are less stable than tertiary ones.</p>
</div>

<div class="text-block">
<p>Applying the logic of this to fragmentation patterns, it means that a split which produces a secondary carbocation is going to be more successful than one producing a primary one. A split producing a tertiary carbocation will be more successful still.</p>
<p>Let's look at the mass spectrum of 2-methylbutane. 2-methylbutane is an isomer of pentane – isomers are molecules with the same molecular formula, but a different spatial arrangement of the atoms.</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="2-methylbutane" type="2d"></div>
</div>

<div class="image-container">
<img src="isopentmspec.GIF">
</div>

<div class="text-block">
<p>Look first at the very strong peak at m/z = 43. This is caused by a different ion than the corresponding peak in the pentane mass spectrum. This peak in 2-methylbutane is caused by:</p>
</div>

<div class="block-formula">
[\text{CH}_3\text{CH}(\text{CH}_3)\text{CH}_2\text{CH}_3]{\bullet}^+ \longrightarrow \text{CH}_3{\stackrel{+}\text{C}}\text{HCH}_3 + {\bullet}\text{CH}_2\text{CH}_3
</div>

<div class="text-block">
<p>The ion formed is a secondary carbocation – it has two alkyl groups attached to the carbon with the positive charge. As such, it is relatively stable.</p>
<p>The peak at m/z = 57 is much taller than the corresponding line in pentane. Again a secondary carbocation is formed – this time, by:</p>
</div>

<div class="block-formula">
[\text{CH}_3\text{CH}(\text{CH}_3)\text{CH}_2\text{CH}_3]{\bullet}^+ \longrightarrow \text{CH}_3{\stackrel{+}\text{C}}\text{HCH}_2\text{CH}_3 + {\bullet}\text{CH}_3
</div>

<div class="text-block">
<p>You would get the same ion, of course, if the left-hand CH<sub>3</sub> group broke off instead of the bottom one as we've drawn it.</p>
<p>In these two spectra, this is probably the most dramatic example of the extra stability of a secondary carbocation.</p>
</div>

<h4>Examples involving acylium ions, [RCO]<sup>+</sup></h4>

<div class="text-block">
<p>Ions with the positive charge on the carbon of a carbonyl group, C=O, are also relatively stable. This is fairly clearly seen in the mass spectra of ketones like pentan-3-one.</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="pentan-3-one" type="2d"></div>
</div>

<div class="image-container">
<img src="p3onemspec.GIF">
</div>

<div class="text-block">
<p>The base peak, at m/z = 57, is due to the [CH<sub>3</sub>CH<sub>2</sub>CO]<sup>+</sup> ion. We've already discussed the fragmentation that produces this.</p>
</div>

<div class="note">
<p>Note: There are lots of other examples of positive ions with extra stability and which are produced in large numbers in a mass spectrometer as a result. Without making this article even longer than it already is, it's impossible to cover every possible case. Check past exam papers to find out whether you are likely to need to know about other possibilities. If you haven't got past papers, follow the link on the <a href="../../syllabuses.html#top">syllabuses</a> page to find out how to get hold of them.</p>
</div>

<h3>Using Mass Spectra to Distinguish Between Compounds</h3>

<div class="text-block">
<p>Suppose you had to suggest a way of distinguishing between pentan-2-one and pentan-3-one using their mass spectra.</p>
</div>

<div class="inline">
<div class="chemical-structure" name="pentan-2-one" type="2d"></div>
<div class="chemical-structure" name="pentan-3-one" type="2d"></div>
</div>

<div class="text-block">
<p>Each of these is likely to split to produce ions with a positive charge on the CO group.</p>
<p>In the pentan-2-one case, there are two different ions like this:</p>
</div>

<ul>
<li>[CH<sub>3</sub>CO]<sup>+</sup></li>
<li>[COCH<sub>2</sub>CH<sub>2</sub>CH<sub>3</sub>]<sup>+</sup></li>
</ul>

<div class="text-block">
<p>That would give you strong lines at m/z = 43 and 71.</p>
<p>With pentan-3-one, you would only get one ion of this kind:</p>
</div>

<ul>
<li>[CH<sub>3</sub>CH<sub>2</sub>CO]<sup>+</sup></li>
</ul>

<div class="text-block">
<p>In that case, you would get a strong line at 57.</p>
<p>You don't need to worry about the other lines in the spectra – the 43, 57 and 71 lines give you plenty of difference between the two. The 43 and 71 lines are missing from the pentan-3-one spectrum, and the 57 line is missing from the pentan-2-one one.</p>
</div>

<div class="note">
<p>Note: Don't confuse the line at m/z = 58 in the pentan-2-one spectrum. That's due to a complicated rearrangement which you couldn't possibly predict at A-level.</p>
</div>

<div class="text-block">
<p>The two spectra look like this:</p>
</div>

<div class="image-container">
<img src="p2onemspec.GIF">
</div>

<div class="image-container">
<img src="p3onemspec.GIF">
</div>

<h3>Computer Matching of Mass Spectra</h3>

<div class="text-block">
<p>As you've seen, the mass spectrum of even very similar organic compounds will be quite different because of the different fragmentations that can occur. Provided you have a computer data base of mass spectra, any unkown spectrum can be computer analysed and simply matched against the data base.</p>
</div>
<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-msfragments.pdf" target="_blank">Questions on fragmentation patterns</a>
<a href="../questions/a-msfragments.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../masspecmenu.html#top">To the mass spectrometry menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>

</div>
</body>

</html>