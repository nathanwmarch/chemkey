<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Origin of Splitting in NMR Spectra | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="A simple explanation of the origin of splitting of peaks in a high resolution NMR spectrum">
<meta name="keywords" content="spectrum, spectra, nuclear magnetic resonance, NMR, nuclear, magnetic, resonance, spectroscopy, spectrometry, proton, hydrogen, high resolution, splitting">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Origin of Splitting in NMR Spectra</h1>

<div class="text-block">
<p>This page describes the reason that you get clusters of peaks in a high resolution NMR spectrum in place of simple peaks in the low-resolution spectrum. The effect is known as <i>spin-spin coupling</i> or <i>spin-spin splitting</i>.</p>
</div>

<div class="note">
<p>Note: If you have come straight to this page via a search engine, you should realise that it is only a "footnote" to a page on <a href="highres.html#top">high-resolution NMR spectra</a>.</p>
</div>

<h2>Spin-spin Coupling</h2>

<h3>The Origin of a Doublet</h3>

<div class="text-block">
<p>Consider what the high resolution NMR spectrum of the compound CH<sub>2</sub>Cl-CHCl<sub>2</sub> would look like.</p>
</div>

<div class="image-container">
<img src="anotate3.GIF">
</div>

<div class="note">
<p>Note: I'm sorry about the absence of a real spectrum. The SDBS database didn't have a suitable NMR spectrum for this compound.</p>
</div>

<div class="text-block">
<p>Focus on the CH<sub>2</sub> group. Why is that a doublet?</p>
<p>Remember that the peaks in an NMR spectrum are in different places because the hydrogens are experiencing different magnetic fields due to their different environments.</p>
<p>Two peaks close together must mean that those particular hydrogens are experiencing two slightly different magnetic fields. Those two slightly different fields are caused by the hydrogen in the CH group next door.</p>
<p>The hydrogen next door has a small magnetic field of its own, which could be aligned with the external magnetic field or opposed to it. Depending on which way around it is aligned, it will either strengthen or weaken the field felt by the CH<sub>2</sub> hydrogens.</p>
</div>

<div class="image-container">
<img src="doubfield.GIF">
</div>

<div class="text-block">
<p>There is an equal chance of either of these arrangements happening and so there will be two peaks due to the CH<sub>2</sub> hydrogens, close together and with equal areas under them (because of the 50/50 chance of either arrangement).</p>
</div>

<h3>The Origin of a Triplet</h3>

<div class="text-block">
<p>Now focus on the CH group in the compound CH<sub>2</sub>Cl-CHCl<sub>2</sub>. Why is that a triplet? It must be a triplet because that hydrogen is experiencing any one of three slightly different magnetic fields.</p>
<p>Think about the magnetic alignments of the hydrogens on the next door CH<sub>2</sub> group. These are the various possibilities:</p>
</div>

<div class="image-container">
<img src="tripfield.GIF">
</div>

<div class="text-block">
<p>The two arrangements in the centre of the diagram produce the same field (exactly the same as the external field). So there are three possible magnetic fields that the CH hydrogen could feel, and so there are three peaks close together – a triplet.</p>
<p>The areas under the peaks are in the ratio of 1:2:1 because that represents the chances of these various magnetic fields occurring.</p>
</div>

<h3>The Origin of a Quartet</h3>

<div class="text-block">
<p>If you apply the same sort of argument to hydrogens next door to a CH<sub>3</sub> group, you will find that they could be experiencing any one of four different magnetic fields depending on the alignment of the CH<sub>3</sub> hydrogens.</p>
</div>

<div class="image-container">
<img src="quartfield.GIF">
</div>

<div class="text-block">
<p>All the arrangements in the second line produce the same field. All the alignments in the third line also produce the same field, but this time a bit smaller. There are four different possible fields, with the chances of them arising in the ratio 1:3:3:1.</p>
<p>So a CH<sub>3</sub> group produces a quartet in the spectrum of the hydrogens of the next door group, with the peak sizes in the ratio 1:3:3:1.</p>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="highres.html#top">Return to the article on high resolution NMR</a>
<a href="../nmrmenu.html#top">To the NMR menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>