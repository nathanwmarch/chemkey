<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>C-13 NMR Spectroscopy Background | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="A simple explanation of how a carbon-13 NMR spectrum arises and the meaning of the term chemical shift.">
<meta name="keywords" content="spectrum, spectra, nuclear magnetic resonance, nmr, nuclear, magnetic, resonance, spectroscopy, spectrometry, chemical shift, carbon, 13, carbon-13, c13">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1><sup>13</sup>C NMR Spectroscopy Background</h1>

<h2>What is Carbon-13 NMR Spectroscopy?</h2>

<div class="text-block">
<p>This page describes what a <sup>13</sup>C NMR spectrum is and how it tells you useful things about the carbon atoms in organic molecules.</p>
<p>Nuclear magnetic resonance is concerned with the magnetic properties of certain nuclei. On this page we are focussing on the magnetic behaviour of carbon-13 nuclei.</p>
</div>

<h3>Carbon-13 nuclei as little magnets</h3>

<div class="text-block">
<p>About 1% of all carbon atoms are the <sup>13</sup>C isotope; the rest (apart from tiny amounts of the radioactive <sup>14</sup>C) is <sup>12</sup>C. Carbon-13 NMR relies on the magnetic properties of the <sup>13</sup>C nuclei.</p>
<p>Carbon-13 nuclei fall into a class known as "spin <sup>1</sup>/<sub>2</sub>" nuclei for reasons which don't really need to concern us at the introductory level this page is aimed at (UK A-level and its equivalents).</p>
<p>The effect of this is that a <sup>13</sup>C nucleus can behave as a little magnet. <sup>12</sup>C nuclei don't have this property.</p>
<p>If you have a compass needle, it normally lines up with the Earth's magnetic field with the north-seeking end pointing north. Provided it isn't sealed in some sort of container, you could twist the needle around with your fingers so that it pointed south – lining it up opposed to the Earth's magnetic field.</p>
<p>It is very unstable opposed to the Earth's field, and as soon as you let it go again, it will flip back to its more stable state.</p>
</div>

<div class="image-container">
<img src="compass.GIF">
</div>

<div class="text-block">
<p>Because a <sup>13</sup>C nucleus behaves like a little magnet, it means that it can also be aligned with an external magnetic field or opposed to it.</p>
<p>Again, the alignment where it is opposed to the field is less stable (at a higher energy). It is possible to make it flip from the more stable alignment to the less stable one by supplying exactly the right amount of energy.</p>
</div>

<div class="image-container">
<img src="magnetsc13.gif">
</div>

<div class="text-block">
<p>The energy needed to make this flip depends on the strength of the external magnetic field used, but is usually in the range of energies found in radio waves – at frequencies of about 25 – 100 MHz. (BBC Radio 4 is found between 92 – 95 MHz!) If you have also looked at proton-NMR, the frequency is about a quarter of that used to flip a hydrogen nucleus for a given magnetic field strength.</p>
<p>It's possible to detect this interaction between the radio waves of just the right frequency and the carbon-13 nucleus as it flips from one orientation to the other as a peak on a graph. This flipping of the carbon-13 nucleus from one magnetic alignment to the other by the radio waves is known as the resonance condition.</p>
</div>

<h3>The Importance of the Carbon's Environment</h3>

<div class="text-block">
<p>What we've said so far would apply to an isolated carbon-13 nucleus, but real carbon atoms in real bonds have other things around them – especially electrons. The effect of the electrons is to cut down the size of the external magnetic field felt by the carbon-13 nucleus.</p>
</div>

<div class="image-container">
<img src="screenc1.gif">
</div>

<div class="text-block">
<p>Suppose you were using a radio frequency of 25 MHz, and you adjusted the size of the magnetic field so that an isolated carbon-13 atom was in the resonance condition.</p>
<p>If you replaced the isolated carbon with the more realistic case of it being surrounded by bonding electrons, it wouldn't be feeling the full effect of the external field any more and so would stop resonating (flipping from one magnetic alignment to the other). The resonance condition depends on having exactly the right combination of external magnetic field and radio frequency.</p>
<p>How would you bring it back into the resonance condition again? You would have to increase the external magnetic field slightly to compensate for the shielding effect of the electrons.</p>
<p>Now suppose that you attached the carbon to something more electronegative. The electrons in the bond would be further away from the carbon nucleus, and so would have less of a lowering effect on the magnetic field around the carbon nucleus.</p>
</div>

<div class="image-container">
<img src="screenc2.gif">
</div>

<div class="note">
<p>Note: Electronegativity is a measure of the ability of an atom to attract a bonding pair of electrons. If you aren't happy about <a href="../../atoms/bonding/electroneg.html#top">electronegativity</a>, you could follow this link at some point in the future, but it probably isn't worth doing it now!</p>
</div>

<div class="text-block">
<p>The external magnetic field needed to bring the carbon into resonance will be smaller if it is attached to a more electronegative element, because the <sup>13</sup>C nucleus feels more of the field. Even small differences in the electronegativities of the attached atoms will make a difference to the magnetic field needed to achieve resonance.</p>
</div>

<h3>Summary</h3>

<div class="text-block">
<p>For a given radio frequency (say, 25 MHz) each carbon-13 atom will need a slightly different magnetic field applied to it to bring it into the resonance condition depending on what exactly it is attached to – in other words the magnetic field needed is a useful guide to the carbon atom's environment in the molecule.</p>
</div>

<h2>Features of a <sup>13</sup>C NMR Cpectrum</h2>

<h3>The <sup>13</sup>C NMR Spectrum for Ethanol</h3>

<div class="sidebarleft">
<div class="chemical-structure" name="ethanol" type="2d"></div>
</div>

<div class="text-block">
<p>This is a simple example of a <sup>13</sup>C NMR spectrum. Don't worry about the scale for now – we'll look at that in a minute.</p>
</div>

<div class="image-container">
<img src="c13ethanol.gif">
</div>

<div class="note">
<p>Note: The nmr spectra on this page have been produced from graphs taken from the Spectral Data Base System for Organic Compounds (<a href="http://riodb01.ibase.aist.go.jp/sdbs/cgi-bin/cre_index.cgi?lang=eng">SDBS</a>) at the National Institute of Materials and Chemical Research in Japan. It is possible that small errors may have been introduced during the process of converting them for use on this site, but these won't affect the argument in any way.</p>
</div>

<div class="text-block">
<p>There are two peaks because there are two different environments for the carbons.</p>
<p>The carbon in the CH<sub>3</sub> group is attached to 3 hydrogens and a carbon. The carbon in the CH<sub>2</sub> group is attached to 2 hydrogens, a carbon and an oxygen.</p>
<p>The two lines are in different places in the NMR spectrum because they need different external magnetic fields to bring them in to resonance at a particular radio frequency.</p>
</div>

<h3>The <sup>13</sup>C NMR Spectrum for a More Complicated Compound</h3>

<div class="text-block">
<p>This is the <sup>13</sup>C NMR Spectrum for 1-methylethyl propanoate (also known as isopropyl propanoate or isopropyl propionate).</p>
</div>

<div class="image-container">
<img src="c13isopropprop.gif">
</div>

<div class="text-block">
<p>This time there are 5 lines in the spectrum. That means that there must be 5 different environments for the carbon atoms in the compound. Is that reasonable from the structure?</p>
</div>

<div class="chemical-structure" name="1-methylethyl propanoate" type="2d"></div>

<div class="text-block">
<p>Well – if you count the carbon atoms, there are 6 of them. So why only 5 lines? In this case, two of the carbons are in exactly the same environment. They are attached to exactly the same things. Look at the two CH<sub>3</sub> groups on the right-hand side of the molecule.</p>
<p>You might reasonably ask why the carbon in the CH<sub>3</sub> on the left isn't also in the same environment. Just like the ones on the right, the carbon is attached to 3 hydrogens and another carbon. But the similarity isn't exact – you have to chase the similarity along the rest of the molecule as well to be sure.</p>
<p>The carbon in the left-hand CH<sub>3</sub> group is attached to a carbon atom which in turn is attached to a carbon with two oxygens on it – and so on down the molecule.</p>
<p>That's not exactly the same environment as the carbons in the right-hand CH<sub>3</sub> groups. They are attached to a carbon which is attached to a single oxygen – and so on down the molecule.</p>
<p>We'll look at this spectrum again in detail on the next page – and look at some more similar examples as well. This all gets easier the more examples you look at.</p>
<p>For now, all you need to realise is that each line in a <sup>13</sup>C NMR spectrum recognises a carbon atom in one particular environment in the compound. If two (or more) carbon atoms in a compound have exactly the same environment, they will be represented by a single line.</p>
</div>

<div class="note">
<p>Note: If you are fairly wide-awake, you might wonder why all this works, since only about 1% of carbon atoms are <sup>13</sup>C. These are the only ones picked up by this form of NMR. If you had a single molecule of ethanol, then the chances are only about 1 in 50 of there being one <sup>13</sup>C atom in it, and only about 1 in 10,000 of both being <sup>13</sup>C. But you have got to remember that you will be working with a sample containing huge numbers of molecules. The instrument can pick up the magnetic effect of the <sup>13</sup>C nuclei in the carbon of the CH<sub>3</sub> group and the carbon of the CH<sub>2</sub> group even if they are in separate molecules. There's no need for them to be in the same one.
</div>

<h3>The Need for a Standard for Comparison – TMS</h3>

<div class="text-block">
<p>Before we can explain what the horizontal scale means, we need to explain the fact that it has a zero point – at the right-hand end of the scale. The zero is where you would find a peak due to the carbon-13 atoms in tetramethylsilane – usually called TMS. Everything else is compared with this.</p>
</div>

<div class="chemical-structure" name="tetramethylsilane" type="2d"></div>

<div class="text-block">
<p>You will find that some NMR spectra show the peak due to TMS (at zero), and others leave it out. Essentially, if you have to analyse a spectrum which has a peak at zero, you can ignore it because that's the TMS peak.</p>
<p>TMS is chosen as the standard for several reasons. The most important are:</p>
</div>

<ul>
<li>It has 4 carbon atoms all of which are in exactly the same environment. They are joined to exactly the same things in exactly the same way. That produces a single peak, but it's also a strong peak (because there are lots of carbon atoms all doing the same thing).</li>

<li>The electrons in the C-Si bonds are closer to the carbons in this compound than in almost any other one. That means that these carbon nuclei are the most shielded from the external magnetic field, and so you would have to increase the magnetic field by the greatest amount to bring the carbons back into resonance.<br>The net effect of this is that TMS produces a peak on the spectrum at the extreme right-hand side. Almost everything else produces peaks to the left of it.</li>
</ul>

<h3>The Chemical Shift</h3>

<div class="text-block">
<p>The horizontal scale is shown as &delta; (ppm). &delta; is called the chemical shift and is measured in parts per million – ppm.</p>
<p>A peak at a chemical shift of, say, 60 means that the carbon atoms which caused that peak need a magnetic field 60 millionths less than the field needed by TMS to produce resonance.</p>
<p>A peak at a chemical shift of 60 is said to be downfield of TMS. The further to the left a peak is, the more downfield it is.</p>
</div>

<div class="note">
<p>Note: If you are familiar with proton-NMR, you will notice that the chemical shifts for <sup>13</sup>C NMR are much bigger than for proton-NMR. In <sup>13</sup>C NMR, they range up to about 200 ppm. In proton-NMR they only go up to about 12 ppm. You don't need to worry about the reasons for this at this level.</p>
</div>

<h3>Solvents for NMR Spectroscopy</h3>

<div class="text-block">
<p>NMR spectra are usually measured using solutions of the substance being investigated. A commonly used solvent is CDCl<sub>3</sub>. This is a trichloromethane (chloroform) molecule in which the hydrogen has been replaced by its isotope, deuterium.</p>
<p>CDCl<sub>3</sub> is also commonly used as the solvent in proton-NMR because it doesn't have any ordinary hydrogen nuclei (protons) which would give a line in a proton-NMR spectrum. It does, of course, have a carbon atom – so why doesn't it give a potentially confusing line in a <sup>13</sup>C NMR spectrum?</p>
<p>In fact it does give a line, but the line has an easily recognisable chemical shift and so can be removed from the final spectrum. All of the spectra from the SDBS have this line removed to avoid any confusion.</p>
</div>

<div class="note">
<p>Note: If you have already read the introductory page about proton NMR, you may have read a similar note to this. If so, there is no real need to read it again.</p>
<p>In the explanations above, I have described an NMR spectrum as being produced by having a fixed radio frequency, and varying the magnetic field to find the resonance conditions for the <sup>13</sup>C atoms in their various environments.</p>
<p>It has been pointed out to me that newer NMR machines actually fix the magnetic field and vary the radio frequency instead. In fact, the most modern machines send a pulse covering a wide spectrum of radio frequencies, and use a computer to sort it all out afterwards.</p>
<p>Do you need to worry about this? No. Provided you are meeting this at an introductory level for exams at an age of about 18, it is pretty unlikely that you will need details of how the machines work. What matters is that you can interpret the resulting NMR spectra.</p>
<p>Personally, I prefer the version involving changing the magnetic field because it is much more obvious to explain, even if the method itself is a bit old-fashioned.</p>
</div>

<div class="inline">
<div class="chemical-structure" name="trichloromethane" type="2d"></div>
<div class="chemical-structure" name="chloroform-d" type="2d"></div>
</div>

<h4>You may not need to bother about this next bit!</h4>

<div class="text-block">
<p>If you have come across this alternative method, some of the wording will be different, and that's all. Check your past exam papers and marks schemes to see whether your examiners ever ask questions about how NMR spectra are produced, and which method they talk about.</p>

<p>If they never ask questions about it, or if they expect answers in terms of varying the magnetic field, stop here!</p>
</div>

<h4>What differences are there if you vary the frequency?</h4>

<div class="text-block">
<p>Think about TMS, the standard which has the maximum amount of magnetic shielding of the carbon atoms by the electrons. That reduces the strength of the magnetic field experienced by the carbon nuclei more than in any other carbon compound. It therefore needs the maximum increase in the external magnetic field to reach the resonance condition if you are varying the magnetic field.</p>

<p>But what if you are varying the frequency of the radio waves instead of the magnetic field?</p>

<p>The weaker magnetic field experienced by the carbon nuclei in TMS will reduce the energy gap between the two states the carbon nuclei can take up – aligned with and against the magnetic field.</p>

<p>Think about it like this:</p>

<p>Suppose you had the compass needle described at the beginning of this page surrounded by a fairly weak magnetic field. It wouldn't take much effort to move it so that it was aligned against the magnetic field. The energy gap between the two states would be pretty small.</p>

<p>Now suppose it was in an intense magnetic field. It would be very difficult to move it so that it was aligned against the field. The energy gap would be much increased by the stronger magnetic field.</p>

<p>So the carbon atoms in TMS, which are better shielded by the electrons around them than any other carbon compound, will have the smallest energy gap between the two states.</p>

<p>The energy of the radio waves is related to their frequency. That means that if you are varying the frequency, TMS will need a lower frequency than any other carbon compound to bring it into resonance.</p>

<p>That has taken a long time to say, but it comes down to the fact that</p>
</div>

<ul>
<li>If you are varying magnetic field, TMS will need the greatest external magnetic field to reach the resonance condition.</li>
<li>If you are varying the radio wave frequencies, TMS will need the lowest frequency to reach the resonance condition.</li>
</ul>

<div class="text-block">
<p>The delta shift works exactly the same in both cases.</p>
</div>

<ul>
<li>A delta shift of 60 means that a particular compound needs a magnetic field 60 millionths less than that of TMS to reach resonance.</li>
<li>A delta shift of 60 means that a particular compound needs a radio frequency 60 millionths more than that of TMS to reach resonance.</li>
</ul>

<div class="note">
<p>DO NOT learn both of these. Just learn the way your examiners expect. None of this makes any difference to the way NMR spectra are interpreted which is probably all you need anyway.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-nmrc13intro.pdf" target="_blank">Questions on an introduction to
<sup>13</sup>C NMR</a>
<a href="../questions/a-nmrc13intro.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="interpretc13.html#top">To learn how to interpret a
<sup>13</sup>C spectrum</a>
<a href="../nmrmenu.html#top">To the NMR menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>