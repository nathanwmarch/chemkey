<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Nuclear Magnetic Resonance (NMR) Spectroscopy Background | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="A simple explanation of how a proton NMR spectrum arises and the meaning of the term chemical shift.">
<meta name="keywords" content="spectrum, spectra, nuclear magnetic resonance, nmr, nuclear, magnetic, resonance, spectroscopy, spectrometry, chemical shift, proton, hydrogen">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Nuclear Magnetic Resonance (NMR) Background</h1>

<div class="text-block">
<p>This page describes what a proton NMR spectrum is and how it tells you useful things about the hydrogen atoms in organic molecules.</p>
</div>

<h2>What is NMR Spectroscopy?</h2>

<div class="text-block">
<p>Nuclear magnetic resonance is concerned with the magnetic properties of certain nuclei. On this page we are focussing on the magnetic behaviour of hydrogen nuclei – hence the term proton NMR or <sup>1</sup>H-NMR.</p>
</div>

<h3>Hydrogen Atoms as Little Magnets</h3>

<div class="text-block">
<p>If you have a compass needle, it normally lines up with the Earth's magnetic field with the north-seeking end pointing north. Provided it isn't sealed in some sort of container, you could twist the needle around with your fingers so that it pointed south – lining it up opposed to the Earth's magnetic field.</p>
<p>It is very unstable opposed to the Earth's field, and as soon as you let it go again, it will flip back to its more stable state.</p>
</div>

<div class="image-container">
<img src="compass.GIF">
</div>

<div class="text-block">
<p>Hydrogen nuclei also behave as little magnets and a hydrogen nucleus can also be aligned with an external magnetic field or opposed to it. Again, the alignment where it is opposed to the field is less stable (at a higher energy). It is possible to make it flip from the more stable alignment to the less stable one by supplying exactly the right amount of energy.</p>
</div>

<div class="image-container">
<img src="magnets.GIF">
</div>

<div class="text-block">
<p>The energy needed to make this flip depends on the strength of the external magnetic field used, but is usually in the range of energies found in radio waves – at frequencies of about 60 – 100 MHz. (BBC Radio 4 is found between 92 – 95 MHz!)</p>
<p>It's possible to detect this interaction between the radio waves of just the right frequency and the proton as it flips from one orientation to the other as a peak on a graph. This flipping of the proton from one magnetic alignment to the other by the radio waves is known as the resonance condition.</p>
</div>

<h3>The Importance of the Hydrogen Atom's Environment</h3>

<div class="text-block">
<p>What we've said so far would apply to an isolated proton, but real protons have other things around them – especially electrons. The effect of the electrons is to cut down the size of the external magnetic field felt by the hydrogen nucleus.</p>
</div>

<div class="image-container">
<img src="screen1.GIF">
</div>

<div class="text-block">
<p>Suppose you were using a radio frequency of 90 MHz, and you adjusted the size of the magnetic field so that an isolated proton was in the resonance condition.</p>
<p>If you replaced the isolated proton with one that was attached to something, it wouldn't be feeling the full effect of the external field any more and so would stop resonating (flipping from one magnetic alignment to the other). The resonance condition depends on having exactly the right combination of external magnetic field and radio frequency.</p>
<p>How would you bring it back into the resonance condition again? You would have to increase the external magnetic field slightly to compensate for the effect of the electrons.</p>
<p>Now suppose that you attached the hydrogen to something more electronegative. The electrons in the bond would be further away from the hydrogen nucleus, and so would have less effect on the magnetic field around the hydrogen.</p>
</div>

<div class="image-container">
<img src="screen2.GIF">
</div>

<div class="note">
<p>Note: Electronegativity is a measure of the ability of an atom to attract a bonding pair of electrons. If you aren't happy about <a href="../../atoms/bonding/electroneg.html#top">electronegativity</a>, you could follow this link at some point in the future, but it probably isn't worth doing it now!</p>
</div>

<div class="text-block">
<p>The external magnetic field needed to bring the hydrogen into resonance will be smaller if it is attached to a more electronegative element, because the hydrogen nucleus feels more of the field. Even small differences in the electronegativities of the attached atom or groups of atoms will make a difference to the magnetic field needed to achieve resonance.</p>
</div>

<h3>Summary</h3>

<div class="text-block">
<p>For a given radio frequency (say, 90 MHz) each hydrogen atom will need a slightly different magnetic field applied to it to bring it into the resonance condition depending on what exactly it is attached to – in other words the magnetic field needed is a useful guide to the hydrogen atom's environment in the molecule.</p>
</div>

<h2>Features of an NMR Spectrum</h2>

<div class="text-block">
<p>A simple NMR spectrum looks like this:</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="ethanoic acid" type="2d"></div>
</div>

<div class="image-container">
<img src="nmrethanoicacid.GIF">
</div>

<div class="note">
<p>Note: The NMR spectra on this page have been produced from graphs taken from the Spectral Data Base System for Organic Compounds (<a href="http://riodb01.ibase.aist.go.jp/sdbs/cgi-bin/cre_index.cgi?lang=eng">SDBS</a>) at the National Institute of Materials and Chemical Research in Japan. It is possible that small errors may have been introduced during the process of converting them for use on this site, but these won't affect the
argument in any way.</p>
</div>

<h3>The Peaks</h3>

<div class="text-block">
<p>There are two peaks because there are two different environments for the hydrogens – in the CH<sub>3</sub> group and attached to the oxygen in the COOH group. They are in different places in the spectrum because they need slightly different external magnetic fields to bring them in to resonance at a particular radio frequency.</p>
<p>The sizes of the two peaks gives important information about the numbers of hydrogen atoms in each environment. It isn't the height of the peaks that matters, but the ratio of the areas under the peaks. If you could measure the areas under the peaks in the diagram above, you would find that they were in the ratio of 3 (for the larger peak) to 1 (for the smaller one).</p>
<p>That shows a ratio of 3:1 in the number of hydrogen atoms in the two environments – which is exactly what you would expect for CH_3COOH.</p>
</div>

<h3>The Need for a Standard for Comparison – TMS</h3>

<div class="text-block">
<p>Before we can explain what the horizontal scale means, we need to explain the fact that it has a zero point – at the right-hand end of the scale. The zero is where you would find a peak due to the hydrogen atoms in tetramethylsilane – usually called TMS. Everything else is compared with this.</p>
</div>

<div class="chemical-structure" name="tetramethylsilane" type="2d"></div>

<div class="text-block">
<p>You will find that some NMR spectra show the peak due to TMS (at zero), and others leave it out. Essentially, if you have to analyse a spectrum which has a peak at zero, you can ignore it because that's the TMS peak.</p>
<p>TMS is chosen as the standard for several reasons. The most important are:</p>
</div>

<ul>
<li>It has 12 hydrogen atoms all of which are in exactly the same environment. They are joined to exactly the same things in exactly the same way. That produces a single peak, but it's also a strong peak (because there are lots of hydrogen atoms).</li>

<li>The electrons in the C-H bonds are closer to the hydrogens in this compound than in almost any other compound. That means that these hydrogen nuclei are the most shielded from the external magnetic field, and so you would have to increase the magnetic field by the greatest amount to bring the hydrogens back into resonance.<br>
The net effect of this is that TMS produces a peak on the spectrum at the extreme right-hand side. Almost everything else produces peaks to the left of it.</li>
</ul>

<h3>The Chemical Shift</h3>

<div class="text-block">
<p>The horizontal scale is shown as &delta; (ppm). &delta; is called the chemical shift and is measured in parts per million – ppm.</p>
<p>A peak at a chemical shift of, say, 2.0 means that the hydrogen atoms which caused that peak need a magnetic field two millionths less than the field needed by TMS to produce resonance.</p>
<p>A peak at a chemical shift of 2.0 is said to be <i>downfield</i> of TMS. The further to the left a peak is, the more downfield it is.</p>
</div>

<h3>Solvents for NMR Spectroscopy</h3>

<div class="text-block">
<p>NMR spectra are usually measured using solutions of the substance being investigated. It is important that the solvent itself doesn't contain any simple hydrogen atoms, because they would produce confusing peaks in the spectrum.</p>
<p>There are two ways of avoiding this. You can use a solvent such as tetrachloromethane, CCl_4, which doesn't contain any hydrogen, or you can use a solvent in which any ordinary hydrogen atoms are replaced by its isotope, deuterium – for example, CDCl<sub>3</sub> instead of CHCl_3. All the NMR spectra used on this site involve CDCl<sub>3</sub> as the solvent.</p>
<p>Tetrachloromethane is widely banned, and is only used in exceptional circumstances.</p>
<p>Deuterium atoms have sufficiently different magnetic properties from ordinary hydrogen that they don't produce peaks in the area of the spectrum that we are looking at.</p>
</div>

<div class="note">
<p>Note: Several text books say that deuterium atoms don't have a magnetic field. It isn't true – they do have a field but it is less than an ordinary hydrogen atom.</p>
<p>If you have already read the introductory page about C-13 NMR, you may have read a similar note to this. If so, there is no real need to read it again.</p>
<p>In the explanations above, I have described an NMR spectrum as being produced by having a fixed radio frequency, and varying the magnetic field to find the resonance conditions for the hydrogen atoms in their various environments.</p>
<p>It has been pointed out to me that newer NMR machines actually fix the magnetic field and vary the radio frequency instead. In fact, the most modern machines send a pulse covering a wide spectrum of radio frequencies, and use a computer to sort it all out afterwards.</p>
<p>Do you need to worry about this? No. Provided you are meeting this at an introductory level for exams at an age of about 18, it is pretty unlikely that you will need details of how the machines work. What matters is that you can interpret the resulting NMR spectra.</p>
<p>Personally, I prefer the version involving changing the magnetic field because it is much more obvious to explain, even if the method itself is a bit old-fashioned.</p>
</div>

<div class="inline">
<div class="chemical-structure" name="tetrachloromethane" type="2d"></div>
<div class="chemical-structure" name="trichloromethane" type="2d"></div>
<div class="chemical-structure" name="chloroform-d" type="2d"></div>
</div>

<h4>You may not need to bother about this next bit!</h4>

<div class="text-block">
<p>If you have come across this alternative method, some of the wording will be different, and that's all. Check your past exam papers and marks schemes to see whether your examiners ever ask questions about how NMR spectra are produced, and which method they talk about.</p>

<p>If they never ask questions about it, or if they expect answers in terms of varying the magnetic field, stop here!</p>
</div>

<h4>What differences are there if you vary the frequency?</h4>

<div class="text-block">
<p>Think about TMS, the standard which has the maximum amount of magnetic shielding of the hydrogen atoms by the electrons. That reduces the strength of the magnetic field experienced by the hydrogen nuclei more than in any other organic compound. It therefore needs the maximum increase in the external magnetic field to reach the resonance condition if you are varying the magnetic field.</p>

<p>But what if you are varying the frequency of the radio waves instead of the magnetic field?</p>

<p>The weaker magnetic field experienced by the hydrogen nuclei in TMS will reduce the energy gap between the two states the hydrogen nuclei can take up – aligned with and against the magnetic field.</p>

<p>Think about it like this:</p>

<p>Suppose you had the compass needle described at the beginning of this page surrounded by a fairly weak magnetic field. It wouldn't take much effort to move it so that it was aligned against the magnetic field. The energy gap between the two states would be pretty small.</p>

<p>Now suppose it was in an intense magnetic field. It would be very difficult to move it so that it was aligned against the field. The energy gap would be much increased by the stronger magnetic field.</p>

<p>So the hydrogen atoms in TMS, which are better shielded by the electrons around them than any other organic compound, will have the smallest energy gap between the two states.</p>

<p>The energy of the radio waves is related to their frequency. That means that if you are varying the frequency, TMS will need a lower frequency than any other organic compound to bring it into resonance.</p>

<p>That has taken a long time to say, but it comes down to the fact that</p>
</div>

<ul>
<li>If you are varying magnetic field, TMS will need the greatest external magnetic field to reach the resonance condition.</li>
<li>If you are varying the radio wave frequencies, TMS will need the lowest frequency to reach the resonance condition.</li>
</ul>

<div class="text-block">
<p>The delta shift works exactly the same in both cases.</p>
</div>

<ul>
<li>A delta shift of 10 means that a particular compound needs a magnetic field 10 millionths less than that of TMS to reach resonance.</li>
<li>A delta shift of 10 means that a particular compound needs a radio frequency 10 millionths more than that of TMS to reach resonance.</li>
</ul>

<div class="note">
<p>Note: DO NOT learn both of these. Just learn the way your examiners expect. None of this makes any difference to the way NMR spectra are interpreted which is probably all you need anyway.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-nmrH1intro.pdf" target="_blank">Questions on an introduction to H-1 NMR</a>
<a href="../questions/a-nmrH1intro.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../nmrmenu.html#top">To the NMR menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>