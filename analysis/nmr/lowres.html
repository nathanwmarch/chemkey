<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Low Resolution Nuclear Magnetic Resonance (NMR) Spectra | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="A simple explanation of how to interpret a low resolution NMR spectrum">
<meta name="keywords" content="spectrum, spectra, nuclear magnetic resonance, NMR, nuclear, magnetic, resonance, spectroscopy, spectrometry, chemical shift, proton, hydrogen, low resolution">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Low Resolution Nuclear Magnetic Resonance (NMR) Spectra</h1>

<div class="text-block">
<p>This page describes how you interpret simple low-resolution nuclear magnetic resonance (NMR) spectra. It assumes that you have already read the background page on NMR so that you understand what an NMR spectrum looks like and the use of the term "chemical shift".</p>
</div>

<div class="note">
<p>Note: If you haven't read the <a href="background.html#top">background page</a> on NMR, you really ought to do that before you go on.</p>
</div>

<h2>The Difference Between High and Low Resolution Spectra</h2>

<div class="image-container">
<img src="nmrmethylprop.GIF">
</div>

<div class="note">
<p>Note: This high resolution NMR spectrum has been produced from a graph taken from the Spectral Data Base System for Organic Compounds (<a href="http://riodb01.ibase.aist.go.jp/sdbs/cgi-bin/cre_index.cgi?lang=eng">SDBS</a>) at the National Institute of Materials and Chemical Research in Japan.</p>
</div>

<div class="text-block">
<p>A low resolution spectrum looks much simpler because it can't distinguish between the individual peaks in the various groups of peaks.</p>
</div>

<div class="image-container">
<img src="lnmrmethylprop.GIF">
</div>

<div class="note">
<p>Note: I haven't been able to find a reliable source of low-resolution NMR spectra. The low-resolution spectra on this page are my best guess at what the low-resolution spectra would look like, based on the high-resolution ones.</p>
</div>

<div class="text-block">
<p>The numbers against the peaks represent the relative areas under each peak. That information is extremely important in interpreting the spectra.</p>
</div>

<h2>Interpreting a Low-resolution Spectrum</h2>

<h3>Using the Total Number of Peaks</h3>

<div class="text-block">
<p>Each peak represents a different environment for hydrogen atoms in the molecule. In the methyl propanoate spectrum above, there are three peaks because there are three different environments for the hydrogens.</p>
<p>Remember that methyl propanoate is CH<sub>3</sub>CH<sub>2</sub>COOCH<sub>3</sub>. The hydrogens in the CH<sub>2</sub> group are obviously in a different environment from those in the CH<sub>3</sub> groups. The two CH<sub>3</sub> groups aren't in the same environment either. One is attached to a CH<sub>2</sub> group, the other to an oxygen.</p>
</div>

<h3>Using the Areas Under the Peaks</h3>

<div class="text-block">
<p>The ratio of the areas under the peaks tell you the ratio of the numbers of hydrogens in the various environments. In the methyl propanoate case, the areas were in the ratio of 3:2:3, which is exactly what you want for the two differently placed CH<sub>3</sub> groups and the CH<sub>2</sub> group.</p>
<p>You will probably be told the relative areas under the peaks – especially if you are only looking at low resolution spectra, but it is just possible that you might have to work them out. NMR spectrometers have a device which draws another line on the spectrum called an integrator trace (or integration trace). You can measure the relative areas from this trace.</p>
</div>

<div class="note">
<p>Note: You need to find out whether your examiners expect you to know how to interpret an integrator trace. Check your syllabus and, particularly, past papers to see whether they ask it. If you are doing a UK-based exam and haven't got copies of your <a href="../../syllabuses.html#top">syllabus and past papers</a>, follow this link to find out how to get them. If you do need to be able to interpret <a href="integration.html#top">integrator traces</a>, you can find out how by following this link. You can also find it from the NMR menu.</p>
</div>

<h3>Using Chemical Shifts</h3>

<div class="text-block">
<p>The position of the peaks tells you useful things about what groups the various hydrogen atoms are in. In any exam, you will be given a table of chemical shifts if you need them. The important shifts for the groups present in methyl propanoate are:</p>
</div>

<div class="image-container">
<img src="shifttable1.GIF">
</div>

<div class="note">
<p>Notes: "R" represents an alkyl group (like methyl, ethyl, etc) which in this case may have other things substituted in it. The shifts are shown as ranges of values. The exact position varies depending on what else is near that particular group in the molecule.</p>
</div>

<div class="text-block">
<p>Showing these groups on the low resolution spectrum gives:</p>
</div>

<div class="image-container">
<img src="lNMRmetprop2.GIF">
</div>

<h2>Some Sample Questions</h2>

<h3>Example 1</h3>

<div class="text-block">
<p>An organic compound was known to be one of the following. Use its low resolution NMR spectrum to decide which it is.</p>
</div>

<div class="inline">
<div class="chemical-structure" name="propanoic acid" type="2d"></div>
<div class="chemical-structure" name="methyl ethanoate" type="2d"></div>
<div class="chemical-structure" name="ethyl ethanoate" type="2d"></div>
</div>

<div class="image-container">
<img src="lnmrpropacid.GIF">
</div>

<div class="text-block">
<p>Notice that there are three peaks showing three different environments for the hydrogens. That eliminates methyl ethanoate as a possibility because that would only give two peaks – due to the two differently situated CH<sub>3</sub> group hydrogens.</p>
<p>Does the ratio of the areas under the peaks help? Not in this case – both the other compounds would have three peaks in the ratio of 1:2:3.</p>
<p>Now you need to look at the chemical shifts:</p>
</div>

<div class="image-container">
<img src="shifttable2.GIF">
</div>

<div class="text-block">
<p>Checking the positions of the various hydrogens in the two possible compounds against the chemical shift table gives you this pattern of shifts:</p>
</div>

<div class="image-container">
<img src="anotate1.GIF">
</div>

<div class="text-block">
<p>Comparing these with the actual spectrum means that the substance was propanoic acid, CH<sub>3</sub>CH<sub>2</sub>COOH.</p>
</div>

<div class="image-container">
<img src="lnmrpropacid.GIF">
</div>

<h3>Example 2</h3>

<div class="text-block">
<p>How would you use low resolution NMR to distinguish between the isomers propanone and propanal?</p
></div>

<div class="inline">
<div class="chemical-structure" name="propanone" type="2d"></div>
<div class="chemical-structure" name="propanal" type="2d"></div>
</div>

<div class="text-block">
<p>The propanone would only give one peak in its NMR spectrum because both CH<sub>3</sub> groups are in an identical environment – both are attached to –COCH<sub>3</sub>.</p>
<p>The propanal would give three peaks with the areas underneath in the ratio 3:2:1.</p>
<p>You could refer to the chemical shift table above to decide where the peaks are likely to be found, but it isn't really necessary.</p>
</div>

<h3>Example 3</h3>

<div class="text-block">
<p>How many peaks would there be in the low resolution NMR spectrum of the following compound, and what would be the ratio of the areas under the peaks?</p>
</div>

<div class="chemical-structure" name="3,3-dimethylbutanoic acid" type="2d"></div>

<div class="text-block">
<p>All the CH<sub>3</sub> groups are exactly equivalent so would only produce 1 peak. There would also be peaks for the hydrogens in the CH<sub>2</sub> group and the COOH group.</p>
<p>There would be three peaks in total with areas in the ratio 9:2:1.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-nmrH1lowres.pdf" target="_blank">Questions on low resolution H-1 NMR</a>
<a href="../questions/a-nmrH1lowres.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../nmrmenu.html#top">To the NMR menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>