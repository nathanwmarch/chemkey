<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>High Resolution Nuclear Magnetic Resonance (NMR) Spectra | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="A simple explanation of how to interpret a high resolution nmr spectrum">
<meta name="keywords" content="spectrum, spectra, nuclear magnetic resonance, nmr, nuclear, magnetic, resonance, spectroscopy, spectrometry, chemical shift, proton, hydrogen, high resolution, splitting">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>High Resolution Nuclear Magnetic Resonance (NMR) Spectra</h1>

<div class="text-block">
<p>This page describes how you interpret simple high resolution nuclear magnetic resonance (NMR) spectra. It assumes that you have already read the background page on NMR so that you understand what an NMR spectrum looks like and the use of the term "chemical shift". It also assumes that you know how to interpret simple low resolution spectra.</p>
</div>

<div class="note">
<p>Note: If you haven't read the <a href="background.html#top">background page</a> on NMR or the page on <a href="lowres.html#top">low resolution NMR</a>, you really ought to read them before you go on.</p>
</div>

<h2>The Difference Between High and Low Resolution Spectra</h2>

<h3>What a Low Resolution NMR Spectrum Tells You</h3>

<div class="text-block">
<p>Remember:</p>
</div>

<ul>
<li>The number of peaks tells you the number of different environments the hydrogen atoms are in.</li>
<li>The ratio of the areas under the peaks tells you the ratio of the numbers of hydrogen atoms in each of these environments.</li>
<li>The chemical shifts give you important information about the sort of environment the hydrogen atoms are in.</li>
</ul>

<h3>High Resolution NMR Spectra</h3>

<div class="text-block">
<p>In a high resolution spectrum, you find that many of what looked like single peaks in the low resolution spectrum are split into clusters of peaks.</p>
<p>For A-level purposes, you will only need to consider these possibilities:</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>1 peak</td>
<td></td>
<td>a singlet</td>
</tr>
<tr>
<td>2 peaks in the cluster</td>
<td></td>
<td>a doublet</td>
</tr>
<tr>
<td>3 peaks in the cluster</td>
<td></td>
<td>a triplet</td>
</tr>
<tr>
<td>4 peaks in the cluster</td>
<td></td>
<td>a quartet</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>You can get exactly the same information from a high resolution spectrum as from a low resolution one – you simply treat each cluster of peaks as if it were a single one in a low resolution spectrum.</p>
<p>But in addition, the amount of splitting of the peaks gives you important extra information.</p>
</div>

<h2>Interpreting a High-resolution Spectrum</h2>

<h3>The n+1 Rule</h3>

<div class="text-block">
<p>The amount of splitting tells you about the number of hydrogens attached to the carbon atom or atoms next door to the one you are currently interested in.</p>
<p>The number of sub-peaks in a cluster is one more than the number of hydrogens attached to the next door carbon(s).</p>
<p>So – on the assumption that there is only one carbon atom with hydrogens on next door to the carbon we're interested in (usually true at A-level!):</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>singlet</td>
<td></td>
<td>next door to carbon with no hydrogens attached</td>
</tr>
<tr>
<td>doublet</td>
<td></td>
<td>next door to a CH group</td>
</tr>
<tr>
<td>triplet</td>
<td></td>
<td>next door to a CH<sub>2</sub> group</td>
</tr>
<tr>
<td>quartet</td>
<td></td>
<td>next door to a CH<sub>3</sub> group</td>
</tr>
</tbody>
</table>

<div class="note">
<p>Note: You probably won't need to know the origin of the n+1 rule, but if you are interested there is a page on <a href="splitting.html#top">the reasons for splitting</a> which you could look at.</p>
</div>

<h3>Using the n+1 Rule</h3>

<h4>What information can you get from this NMR spectrum?</h4>

<div class="image-container">
<img src="nmrethyleth.GIF">
</div>

<div class="note">
<p>Note: The nmr spectra on this page have been produced from data taken from the Spectral Data Base System for Organic Compounds (<a href="http://riodb01.ibase.aist.go.jp/sdbs/cgi-bin/cre_index.cgi?lang=eng">SDBS</a>) at the National Institute of Materials and Chemical Research in Japan. Any small errors that I've introduced during the process of converting them for use on this site won't affect the argument in any way.</p>
</div>

<div class="text-block">
<p>Assume that you know that the compound above has the molecular formula C<sub>4</sub>H<sub>8</sub>O<sub>2</sub>.</p>
<p>Treating this as a low resolution spectrum to start with, there are three clusters of peaks and so three different environments for the hydrogens. The hydrogens in those three environments are in the ratio 2:3:3. Since there are 8 hydrogens altogether, this represents a CH<sub>2</sub> group and two CH<sub>3</sub> groups.</p>
<p>What about the splitting?</p>
<p>The CH<sub>2</sub> group at about 4.1 ppm is a quartet. That tells you that it is next door to a carbon with three hydrogens attached – a CH<sub>3</sub> group.</p>
<p>The CH<sub>3</sub> group at about 1.3 ppm is a triplet. That must be next door to a CH<sub>3</sub> group.</p>
<p>This combination of these two clusters of peaks – one a quartet and the other a triplet – is typical of an ethyl group, CH<sub>3</sub>CH<sub>2</sub>. It is very common. Get to recognise it!</p>
<p>Finally, the CH<sub>3</sub> group at about 2.0 ppm is a singlet. That means that the carbon next door doesn't have any hydrogens attached.</p>
<p>So what is this compound? You would also use chemical shift data to help to identify the environment each group was in, and eventually you would come up with:</p>
</div>

<div class="image-container">
<img src="anotate2.GIF">
</div>

<div class="note">
<p>Note: You now know how to get the information you need from NMR spectra, but it often isn't easy to fit all that information together into a final formula. You simply need to practise! Go through all the examples in past papers from your Exam Board. How complicated they are will vary markedly from Board to Board. Some of the compounds you will come across may be very unfamiliar. Don't forget to use the information in chemical shift tables – if your examiners include some obscure group, it's almost certain you will need to use it. Take all the hints that are going!</p>
</div>

<h2>Two Special Cases</h2>

<h3>Alcohols</h3>

<h4>Where is the -OH peak?</h4>

<div class="text-block">
<p>This is very confusing! Different sources quote totally different chemical shifts for the hydrogen atom in the -OH group in alcohols – often inconsistently. For example:</p>
</div>

<ul>
<li>The Nuffield Data Book quotes 2.0 – 4.0, but the Nuffield text book shows a peak at about 5.4.</li>
<li>The OCR Data Sheet for use in their exams quotes 3.5 – 5.5.</li>
<li>A reliable degree level organic chemistry text book quotes 1.0 – 5.0, but then shows an NMR spectrum for ethanol with a peak at about 6.1.</li>
<li>The SDBS database (used throughout this site) gives the -OH peak in ethanol at about 2.6.</li>
</ul>

<div class="image-container">
<img src="nmrethanol.GIF">
</div>

<div class="text-block">
<p>The problem seems to be that the position of the -OH peak varies dramatically depending on the conditions – for example, what solvent is used, the concentration, and the purity of the alcohol – especially on whether or not it is totally dry.</p>
</div>

<div class="note">
<p>Help! Do you need to worry about this? Not really – you can assume that in an exam question, any NMR spectrum will be consistent with the chemical shift data you are given.</p>
</div>

<h4>A clever way of picking out the -OH peak</h4>

<div class="text-block">
<p>If you measure an NMR spectrum for an alcohol like ethanol, and then add a few drops of deuterium oxide, D<sub>2</sub>O, to the solution, allow it to settle and then re-measure the spectrum, the -OH peak disappears! By comparing the two spectra, you can tell immediately which peak was due to the -OH group.</p>
</div>

<div class="note">
<p>Note: Deuterium oxide (sometimes called "heavy water") is simply water in which all the normal hydrogen-1 atoms are replaced by its isotope, hydrogen-2 (or deuterium).</p>
</div>

<div class="inline">
<div class="chemical-structure" name="water" type="2d"></div>
<div class="chemical-structure" name="heavy water" type="2d"></div>
</div>

<div class="text-block">
<p>The reason for the loss of the peak lies in the interaction between the deuterium oxide and the alcohol. All alcohols, such as ethanol, are very, very slightly acidic. The hydrogen on the -OH group transfers to one of the lone pairs on the oxygen of the water molecule. The fact that here we've got "heavy water" makes no difference to that.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{O}{-}\text{H} + \text{D}{-}\text{O}{-}\text{D} \longrightarrow \text{CH}_3\text{CH}_2{-}\text{O}^- + \text{H}{-}{\stackrel{+}{\text{O}}}\text{D}_2
</div>

<div class="text-block">
<p>The negative ion formed is most likely to bump into a simple deuterium oxide molecule to regenerate the alcohol – except that now the -OH group has turned into an -OD group.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{O}^- + \text{D}{-}\text{O}{-}\text{D} \longrightarrow \text{CH}_3\text{CH}_2{-}\text{O}{-}\text{D} + {^-}\text{O}{-}\text{D}
</div>

<div class="text-block">
<p>Deuterium atoms don't produce peaks in the same region of an NMR spectrum as ordinary hydrogen atoms, and so the peak disappears.</p>
<p>You might wonder what happens to the positive ion in the first equation and the OD<sup>-</sup> in the second one. These get lost into the normal equilibrium which exists wherever you have water molecules – heavy or otherwise.</p>
</div>

<div class="block-formula">
2\text{D}_2\text{O} \leftrightharpoons \text{D}_3\text{O}^+ + \text{OD}^-
</div>

<h4>The lack of splitting with -OH groups</h4>

<div class="text-block">
<p>Unless the alcohol is absolutely free of any water, the hydrogen on the -OH group and any hydrogens on the next door carbon don't interact to produce any splitting. The -OH peak is a singlet and you don't have to worry about its effect on the next door hydrogens.</p>
</div>

<div class="image-container">
<img src="nmrethanol.GIF">
</div>

<div class="text-block">
<p>The left-hand cluster of peaks is due to the CH<sub>3</sub> group. It is a quartet because of the 3 hydrogens on the next door CH<sub>3</sub> group. You can ignore the effect of the -OH hydrogen.</p>
<p>Similarly, the -OH peak in the middle of the spectrum is a singlet. It hasn't turned into a triplet because of the influence of the CH<sub>3</sub> group.</p>
</div>

<div class="note">
<p>Note: The reason for this is quite complex, and certainly goes beyond A-level. It lies in the very rapid interchange that occurs between the hydrogen atoms on the -OH group and either water molecules or other alcohol molecules. To find out about it you will have to read either a degree level organic chemistry book or one specifically about NMR.</p>

<p>For A-level purposes just accept the fact that -OH produces a singlet and has no effect on neighbouring groups!</p>

<p>If you are interested, you can read more about the -OH group in NMR in two articles from Michigan State University:</p>

<p><a href="https://www2.chemistry.msu.edu/faculty/reusch/VirtTxtJml/Spectrpy/nmr/nmr1.htm">NMR1</a> Look under "Hydroxyl Proton Exchange and the Influence of Hydrogen Bonding"</p>

<p><a href="https://www2.chemistry.msu.edu/faculty/reusch/VirtTxtJml/Spectrpy/nmr/nmr2.htm">NMR2</a> Look under "Hydrogen Bonding Influences". Don't expect these to be easy reading though – this is university level stuff.</p>
</div>

<h3>Equivalent Hydrogen Atoms</h3>

<div class="text-block">
<p>Hydrogen atoms attached to the same carbon atom are said to be equivalent. Equivalent hydrogen atoms have no effect on each other – so that one hydrogen atom in a CH<sub>3</sub> group doesn't cause any splitting in the spectrum of the other one.</p>
<p>But hydrogen atoms on neighbouring carbon atoms can also be equivalent if they are in exactly the same environment. For example:</p>
</div>

<div class="image-container">
<img src="equiv.GIF">
</div>

<div class="text-block">
<p>These four hydrogens are all exactly equivalent. You would get a single peak with no splitting at all.</p>
<p>You only have to change the molecule very slightly for this no longer to be true.</p>
</div>

<div class="image-container">
<img src="notequiv.GIF">
</div>

<div class="text-block">
<p>Because the molecule now contains different atoms at each end, the hydrogens are no longer all in the same environment. This compound would give two separate peaks on a low resolution NMR spectrum. The high resolution spectrum would show that both peaks subdivided into triplets – because each is next door to a differently placed CH<sub>3</sub> group.</p>
</div>

<h3>A Comment About NMR and Benzene Rings</h3>

<div class="text-block">
<p>At this introductory level, all you can safely say about hydrogens attached to a benzene ring is how many of them there are. If you have a molecular formula which has 6 or more carbon atoms in it, then it could well contain a benzene ring.</p>
<p>Look for NMR peaks in the 6.0 – 9.0 range. If you are given a number like 5 or 4 alongside that peak, this just tells you how many hydrogen atoms are attached to the ring.</p>
<p>If there are 5 hydrogens attached to the ring, then there is only one group substituted into the ring. If there are 4 hydrogens attached, then there are two separate groups substituted in, and so on. There should always be a total of 6 things attached to the ring. Every hydrogen atom that is missing has been replaced by something else.</p>
<p>Splitting patterns involving benzene rings are far too complicated for this level, generally producing complicated patterns of splitting called multiplets.</p>
</div>

<div class="note">
<p>Note: If you are unfortunate enough to have examiners who ask about spitting in benzene rings at this level (for 16 – 18 year old chemistry students), look carefully at the question and mark scheme so that you can see exactly what they want, and just learn that. But it may well be that all they want is for you to notice the number of hydrogen atoms involved in the ring (see above).</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-nmrH1highres.pdf" target="_blank">Questions on high resolution H-1 NMR</a>
<a href="../questions/a-nmrH1highres.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../nmrmenu.html#top">To the NMR menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>