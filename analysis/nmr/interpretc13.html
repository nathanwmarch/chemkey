<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Interpreting C-13 NMR spectra | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="An explanation of how you interpret a C-13 NMR spectrum in simple cases.">
<meta name="keywords" content="interpret, interpreting, interpretation, spectrum, spectra, nuclear magnetic resonance, NMR, nuclear, magnetic, spectroscopy, spectrometry, chemical shift, carbon, 13, C-13, c13">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Interpreting a <sup>13</sup>C NMR spectra</h1>

<div class="text-block">
<p>This page takes an introductory look at how you can get useful information from a <sup>13</sup>C NMR spectrum.</p>
</div>

<div class="note">
<p>Important: If you have come straight to this page via a search engine, you should be aware that this is the second of two pages about <sup>13</sup>C NMR. Unless you are familiar with <sup>13</sup>C NMR, you should read the <a href="backgroundc13.html#top">introduction to <sup>13</sup>C NMR</a> first by following this link.
</div>

<h2>Taking a Close Look at Three <sup>13</sup>C NMR Spectra</h2>

<h3>The <sup>13</sup>C NMR spectrum for ethanol</h3>

<div class="sidebarleft">
<div class="chemical-structure" name="ethanol" type="2d"></div>
</div>

<div class="image-container">
<img src="c13ethanol2.gif">
</div>

<div class="note">
<p>Note: The NMR spectra on this page have been produced from graphs taken from the Spectral Data Base System for Organic Compounds (<a href="http://riodb01.ibase.aist.go.jp/sdbs/cgi-bin/cre_index.cgi?lang=eng">SDBS</a>) at the National Institute of Materials and Chemical Research in Japan. It is possible that small errors may have been introduced during the process of converting them for use on this site, but these won't affect the argument in any way.</p>
</div>

<div class="text-block">
<p>Remember that each peak identifies a carbon atom in a different environment within the molecule. In this case there are two peaks because there are two different environments for the carbons.</p>
<p>The carbon in the CH<sub>3</sub> group is attached to 3 hydrogens and a carbon. The carbon in the CH<sub>2</sub> group is attached to 2 hydrogens, a carbon and an oxygen.</p>
<p>So which peak is which?</p>
<p>You might remember from the introductory page that the external magnetic field experienced by the carbon nuclei is affected by the electronegativity of the atoms attached to them. The effect of this is that the chemical shift of the carbon increases if you attach an atom like oxygen to it. That means that the peak at about 60 (the larger chemical shift) is due to the CH<sub>2</sub> group because it has a more electronegative atom attached.</p>
</div>

<div class="note">
<p>Note: In principle, you should be able to work out the fact that the carbon attached to the oxygen will have the larger chemical shift. In practice, given the level I am aiming at (16 – 18 year old chemistry students), you always work from tables of chemical shift values for different groups (see below).</p>

<p>What if you needed to work it out? The electronegative oxygen pulls electrons away from the carbon nucleus leaving it more exposed to any external magnetic field. That means that you will need a smaller external magnetic field to bring the nucleus into the resonance condition than if it was attached to less electronegative things. The smaller the magnetic field needed, the higher the chemical shift.</p>
<p>All this is covered in more detail on the <a href="backgroundc13.html#top">introduction to <sup>13</sup>C NMR</a> page mentioned above.</p>
</div>

<h4>A table of typical chemical shifts in <sup>13</sup>C NMR spectra</h4>

<table class="data-table two-center">
<thead>
<tr>
<th>carbon environment</th>
<th>chemical shift<br>/ ppm</th>
</tr>
</thead>
<tbody>
<tr>
<td>C=O (in ketones)</td>
<td>205 – 220</td>
</tr>
<tr>
<td>C=O (in aldehydes)</td>
<td>190 – 200</td>
</tr>
<tr>
<td>C=O (in acids and esters)</td>
<td>160 – 185</td>
</tr>
<tr>
<td>C in aromatic rings</td>
<td>125 – 150</td>
</tr>
<tr>
<td>C=C (in alkenes)</td>
<td>115 – 140</td>
</tr>
<tr>
<td>RCH<sub>2</sub>O–</td>
<td>50 – 90</td>
</tr>
<tr>
<td>RCH<sub>2</sub>Cl</td>
<td>30 – 60</td>
</tr>
<tr>
<td>RCH<sub>2</sub>NH<sub>2</sub>
</td>
<td>30 – 65</td>
</tr>
<tr>
<td>R<sub>3</sub>CH</td>
<td>25 – 35</td>
</tr>
<tr>
<td>CH<sub>3</sub>CO–</td>
<td>20 – 50</td>
</tr>
<tr>
<td>R<sub>2</sub>CH<sub>2</sub></td>
<td>16 – 25</td>
</tr>
<tr>
<td>RCH<sub>3</sub></sub></td>
<td>10 – 15</td>
</tr>
</tbody>
</table>

<div class="note">
<p>Note: I have no confidence in the exact values above. The table is a composite of three separate tables, with values which I have selected in order to make sense of the spectra I am talking about. The values vary depending on the exact environment of the carbon, and these values should just be taken as an approximation. In an exam, your examiner should give you values which are consistent with the spectra they are asking you about.</p>
</div>

<div class="text-block">
<p>In the table, the "R" groups won't necessarily be simple alkyl groups. In each case there will be a carbon atom attached to the one shown in red, but there may well be other things substituted into the "R" group.</p>
<p>If a substituent is very close to the carbon in question, and very electronegative, that might affect the values given in the table slightly.</p>
<p>For example, ethanol has a peak at about 60 because of the CH<sub>2</sub>OH group. No problem!</p>
<p>It also has a peak due to the RCH<sub>3</sub></sub> group. The "R" group this time is CH<sub>2</sub>OH. The electron pulling effect of the oxygen atom increases the chemical shift slightly from the one shown in the table to a value of about 18.</p>
</div>

<h4>A simplification of the table</h4>

<div class="text-block">
<p>You may come across a simplification of the above table which is useful in easy cases just to pick out the main types of carbon environments in a compound:</p>
</div>

<table class="data-table two-center">
<thead>
<tr>
<th>carbon environment</th>
<th>chemical shift<br>/ ppm</th>
</tr>
</thead>
<tbody>
<tr>
<td>C–C</td>
<td>0 – 50</td>
</tr>
<tr>
<td>C–O</td>
<td>50 – 100</td>
</tr>
<tr>
<td>C=C</td>
<td>100 – 150</td>
</tr>
<tr>
<td>C=O</td>
<td>150 – 200</td>
</tr>
</tbody>
</table>

<h3>The <sup>13</sup>C NMR Spectrum for But-3-en-2-one</h3>

<div class="text-block">
<p>This is also known as 3-buten-2-one (amongst many other things!)</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="but-3-en-2-one" type="2d"></div>
</div>

<div class="image-container">
<img src="c13butenone.gif">
</div>

<div class="text-block">
<p>You can pick out all the peaks in this compound using the simplified table above.</p>
<p>The peak at just under 200 is due to a carbon-oxygen double bond. The two peaks at 137 and 129 are due to the carbons at either end of the carbon-carbon double bond. And the peak at 26 is the methyl group which, of course, is joined to the rest of the molecule by a carbon-carbon single bond.</p>
<p>If you want to use the more accurate table, you have to put a bit more thought into it – and, in particular, worry about the values which don't always exactly match those in the table!</p>
<p>The carbon-oxygen double bond in the peak for the ketone group has a slightly lower value than the table suggests for a ketone. There is an interaction between the carbon-oxygen and carbon-carbon double bonds in the molecule which affects the value slightly. This isn't something which we need to look at in detail for the purposes of this topic.</p>
<p>You must be prepared to find small discrepancies of this sort in more complicated molecules – but don't worry about this for exam purposes at this level. Your examiners should give you shift values which exactly match the compound you are given.</p>
<p>The two peaks for the carbons in the carbon-carbon double bond are exactly where they would be expected to be. Notice that they aren't in exactly the same environment, and so don't have the same shift values. The one closer to the carbon-oxygen double bond has the larger value.</p>
<p>And the methyl group on the end has exactly the sort of value you would expect for one attached to C=O. The table gives a range of 20 – 50, and that's where it is.</p>
<p>One final important thing to notice. There are four carbons in the molecule and four peaks because they are all in different environments. But they aren't all the same height. In <sup>13</sup>C NMR, you can't draw any simple conclusions from the heights of the various peaks.</p>
</div>

<h3>The <sup>13</sup>C NMR Spectrum for 1-methylethyl propanoate</h3>

<div class="text-block">
<p>1-methylethyl propanoate is also known as isopropyl propanoate or isopropyl propionate.</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="1-methylethyl propanoate" type="2d"></div>
</div>

<div class="image-container">
<img src="c13isopropprop.gif">
</div>

<h4>Two simple peaks</h4>

<div class="text-block">
<p>There are two very simple peaks in the spectrum which could be identified easily from the second table above.</p>
<p>The peak at 174 is due to a carbon in a carbon-oxygen double bond. (Looking at the more detailed table, this peak is due to the carbon in a carbon-oxygen double bond in an acid or ester.)</p>
<p>The peak at 67 is due to a different carbon singly bonded to an oxygen. Those two peaks are therefore due to:</p>
</div>

<div class="image-container">
<img src="c13isopropprop2.gif">
</div>

<div class="text-block">
<p>Before we go on to look at the other peaks, notice the heights of these two peaks we've been talking about. They are both due to a single carbon atom in the molecule, and yet they have different heights. Again, you can't read any reliable information directly from peak heights in these spectra.</p>
</div>

<h4>The three right-hand peaks</h4>

<div class="text-block">
<p>From the simplified table, all you can say is that these are due to carbons attached to other carbon atoms by single bonds. But because there are three peaks, the carbons must be in three different environments.</p>
<p>The more detailed table is more helpful.</p>
<p>Here are the structure and the spectrum again:</p>
</div>

<div class="sidebarleft">
<div class="chemical-structure" name="1-methylethyl propanoate" type="2d"></div>
</div>

<div class="image-container">
<img src="c13isopropprop.gif">
</div>

<div class="text-block">
<p>The easiest peak to sort out is the one at 28. If you look back at the table, that could well be a carbon attached to a carbon-oxygen double bond. The table quotes the group as CH<sub>3</sub>CO–, but replacing one of the hydrogens by a simple CH<sub>3</sub> group won't make much difference to the shift value.</p>
<p>The right-hand peak is also fairly easy. This is the left-hand methyl group in the molecule. It is attached to an admittedly complicated R group (the rest of the molecule). It is the bottom value given in the detailed table.</p>
<p>The tall peak at 22 must be due to the two methyl groups at the right-hand end of the molecule – because that's all that's left. These combine to give a single peak because they are both in exactly the same environment.</p>
<p>If you are looking at the detailed table, you need to think very carefully which of the environments you should be looking at. Without thinking, it is tempting to go for the R<sub>2</sub>CH<sub>2</sub> with peaks in the 16 – 25 region. But you would be wrong!</p>
<p>The carbons we are interested in are the ones in the methyl group, not in the R groups. These carbons are again in the environment: RCH<sub>3</sub></sub>. The R is the rest of the molecule.</p>
<p>The table says that these should have peaks in the range 10 – 15, but our peak is a bit higher. This is because of the presence of the nearby oxygen atom. Its electronegativity is pulling electrons away from the methyl groups – and this tends to increase the chemical shift slightly.</p>
<p>Once again, don't worry about the discrepancies. In an exam, your examiners should give you values which match the peaks in the spectra.</p>
<p>Remember that you are only doing an introduction to <sup>13</sup>C NMR at this level. It isn't going to be that hard in an exam!</p>
</div>

<h2>Working Out Structures From <sup>13</sup>C NMR Spectra</h2>

<div class="text-block">
<p>So far, we've just been trying to see the relationship between carbons in particular environments in a molecule and the spectrum produced. We've had all the information necessary. Now let's make it a little more difficult – but we'll work from much easier examples!</p>
<p>In each example, try to work it out for yourself before you read the explanation.</p>
</div>

<h3>Example 1</h3>

<div class="text-block">
<p>How could you tell from just a quick look at a <sup>13</sup>C NMR spectrum (and without worrying about chemical shifts) whether you had propanone or propanal (assuming those were the only options)?</p>
</div>

<div class="inline">
<div class="chemical-structure" name="propanone" type="2d"></div>
<div class="chemical-structure" name="propanal" type="2d"></div>
</div>

<div class="text-block">
<p>Because these are isomers, each has the same number of carbon atoms, but there is a difference between the environments of the carbons which will make a big impact on the spectra.</p>
<p>In propanone, the two carbons in the methyl groups are in exactly the same environment, and so will produce only a single peak. That means that the propanone spectrum will have only 2 peaks – one for the methyl groups and one for the carbon in the C=O group.</p>
<p>However, in propanal, all the carbons are in completely different environments, and the spectrum will have three peaks.</p>
</div>

<h3>Example 2</h3>

<div class="text-block">
<p>Thare are four alcohols with the molecular formula C<sub>4</sub>H<sub>10</sub>O.</p>
</div>

<div class="inline">
<div class="chemical-structure" name="butan-1-ol" type="2d" label="A: "></div>
<div class="chemical-structure" name="butan-2-ol" type="2d" label="B: "></div>
<div class="chemical-structure" name="2-methylpropan-2-ol" type="2d" label="C: "></div>
<div class="chemical-structure" name="2-methylpropan-2-ol" type="2d" label="D: "></div>
</div>

<div class="text-block">
<p>Which one produced the <sup>13</sup>C NMR spectrum below?</p>
</div>

<div class="image-container">
<img src="c13alcohols.gif">
</div>

<div class="text-block">
<p>You can do this perfectly well without referring to chemical shift tables at all.</p>
<p>In the spectrum there are a total of three peaks – that means that there are only three different environments for the carbons, despite there being four carbon atoms.</p>
<p>In A and B, there are four totally different environments. Both of these would produce four peaks.</p>
<p>In D, there are only two different environments – all the methyl groups are exactly equivalent.D would only produce two peaks.</p>
<p>That leaves C. Two of the methyl groups are in exactly the same environment – attached to the rest of the molecule in exactly the same way. They would only produce one peak. With the other two carbon atoms, that would make a total of three. The alcohol is C.</p>
</div>

<h3>Example 3</h3>

<div class="text-block">
<p>This follows on from Example 2, and also involves an isomer of C<sub>4</sub>H<sub>10</sub>O but which isn't an alcohol. Its <sup>13</sup>C NMR spectrum is below. Work out what its structure is.</p>
</div>

<div class="image-container">
<img src="c13ether.gif">
</div>

<div class="text-block">
<p>Because we don't know what sort of structure we are looking at, this time it would be a good idea to look at the shift values. The approximations are perfectly good, and we will work from this table:</p>
</div>

<table class="data-table two-center">
<tbody>
<tr>
<th>carbon environment</th>
<th>chemical shift<br>/ ppm</th>
</tr>
<tr>
<td>C–C</td>
<td>0 – 50</td>
</tr>
<tr>
<td>C–O</td>
<td>50 – 100</td>
</tr>
<tr>
<td>C=C</td>
<td>100 – 150</td>
</tr>
<tr>
<td>C=O</td>
<td>150 – 200</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>There is a peak for carbon(s) in a carbon-oxygen single bond and one for carbon(s) in a carbon-carbon single bond. That would be consistent with C–C–O in the structure.</p>
<p>It isn't an alcohol (you are told that in the question), and so there must be another carbon on the right-hand side of the oxygen in the structure in the last paragraph.</p>
<p>The molecular formula is C<sub>4</sub>H<sub>10</sub>O, and there are only two peaks. The only solution to that is to have two identical ethyl groups either side of the oxygen.</p>
<p>The compound is ethoxyethane (diethyl ether).</p>
</div>

<div class="chemical-structure" name="diethyl ether" type="2d"></div>

<h3>Example 4</h3>

<div class="text-block">
<p>Using the simplified table of chemical shifts above, work out the structure of the compound with the following <sup>13</sup>C NMR spectrum. Its molecular formula is C<sub>4</sub>H<sub>10</sub>O.</p>
</div>

<div class="image-container">
<img src="c13acrylate.gif">
</div>

<div class="text-block">
<p>Let's sort out what we've got.</p>
</div>

<ul>
<li>There are four peaks and four carbons. No two carbons are in exactly the same environment.</li>

<li>The peak at just over 50 must be a carbon attached to an oxygen by a single bond.</li>

<li>The two peaks around 130 must be the two carbons at either end of a carbon-carbon double bond.</li>

<li>The peak at just less than 170 is the carbon in a carbon-oxygen double bond.</li>
</ul>

<div class="text-block">
<p>Putting this together is a matter of playing around with the structures until you have come up with something reasonable. But you can't be sure that you have got the right structure using this simplified table.</p>

<p>In this particular case, the spectrum was for the compound:</p>
</div>

<div class="chemical-structure" name="but-3-en-2-one" type="2d"></div>

<div class="text-block">
<p>If you refer back to the more accurate table of chemical shifts towards the top of the page, you will get some better confirmation of this. The relatively low value of the carbon-oxygen double bond peak suggests an ester or acid rather than an aldehyde or ketone.</p>
<p>It can't be an acid because there has to be a carbon attached to an oxygen by a single bond somewhere – apart from the one in the –COOH group. We've already accounted for that carbon atom from the peak at about 170. If it was an acid, you would already have used up both oxygens in the structure in the –COOH group.</p>
<p>Without this information, though, you could probably come up with reasonable alternative structures. If you were working from the simplified table in an exam, your examiners would have to allow any valid alternatives.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-nmrc13interpret.pdf" target="_blank">Questions on interpreting
<sup>13</sup>C NMR</a>
<a href="../questions/a-nmrc13interpret.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../nmrmenu.html#top">To the NMR menu</a>
<a href="../../analysismenu.html#top">To the instrumental analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>