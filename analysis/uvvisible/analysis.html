<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Using UV-visible Absorption Spectra | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="A quick look at how UV-visible absorption spectra can be used to identify organic compounds and measure concentrations of solutions">
<meta name="keywords" content="UV, ultra-violet, spectrometer, spectrometry, spectrum, spectra, Beer, Lambert, Law, absorbance, absorptivity, use, uses, concentration, analysis, analyse">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Using UV-visible Absorption Spectra</h1>

<div class="text-block">
<p>This page takes a brief look at how UV-visible absorption spectra can be used to help identify compounds and to measure the concentrations of coloured solutions. It assumes that you know how these spectra arise, and know what is meant by terms such as absorbance, molar absorptivity and lambda-max. You also need to be familiar with the Beer-Lambert Law.</p>
</div>

<div class="note">
<p>Important: If you don't know about these things, this page is a waste of your time! Explore the rest of the <a href="../uvvisiblemenu.html#top">UV-visible spectroscopy menu</a> before you go on.</p>
</div>

<h2>Using UV-absorption Spectra to Help Identify Organic Compounds</h2>

<div class="text-block">
<p>If you have worked through the rest of this section, you will know that the wavelength of maximum absorption (&lambda;<sub>max</sub>) depends on the presence of particular chromophores (light-absorbing groups) in a molecule.</p>
<p>For example, on another page you will have come across the fact that a simple carbon-carbon double bond (for example in ethene) has a maximum absorption at 171 nm. The two conjugated double bonds in buta-1,3-diene have a maximum absorption at a longer wavelength of 217 nm.</p>
<p>We also talked about the two peaks in the spectrum of ethanal (containing a simple carbon-oxygen double bond) at 180 and 290 nm.</p>
<p>In carefully chosen simple cases (which is all you will get at this level), if you compared the peaks on a given UV-visible absorption spectrum with a list of known peaks, it would be fairly easy to pick out some structural features of an unknown molecule.</p>
<p>Lists of known peaks often include molar absorptivity values as well. That might help you to be even more sure. For example (again using the simple carbon-oxygen double bond), data shows that the peak at 290 has a molar absorptivity of only 15, compared with the one at 180 of 10000.</p>
<p>If your spectrum showed a very large peak at 180, and an extremely small one at 290, that just adds to your certainty.</p>
<p>Any question set at this level is going to be so trivial, and so obvious, that it isn't worth spending any more time on this. Let's look at something a bit more complicated!</p>
</div>

<h2>Using UV-absorption Spectra to Find Concentrations</h2>

<div class="text-block">
<p>You should remember the Beer-Lambert Law:</p>
</div>

<div class="block-formula">
\begin{aligned}
log_{10}\frac{I_0}{I} &amp;= \epsilon~l~c \\
\epsilon &amp;- the~Greek~letter~epsilon \\
l &amp;- length~of~solution~the~light~passes~through \\
c &amp;- concentration~of~solution
\end{aligned}
</div>

<div class="text-block">
<p>The expression on the left of the equation is known as the absorbance of the solution and is measured by a spectrometer. The equation is sometimes written in terms of that absorbance.</p>
</div>

<div class="block-formula">
A = \epsilon~l~c
</div>

<div class="text-block">
<p>The symbol &epsilon; (epsilon) is the molar absorptivity of the solution.</p>
</div>

<div class="note">
<p>Note: You will find this all explained in more detail on the page about the <a href="beerlambert.html#top">Beer-Lambert Law</a>.</p>
</div>

<h3>Finding Concentration Using the Molar Absorptivity</h3>

<div class="text-block">
<p>If you know the molar absorptivity of a solution at a particular wavelength, and you measure the absorbance of the solution at that wavelength, it is easy to calculate the concentration. The only other variable in the expression above is the length of the solution. That's easy to measure and, in fact, the cell containing the solution may well have been manufactured with a known length of 1 cm.</p>
<p>For example, let's suppose you have a solution in a cell of length 1 cm. You measure the absorbance of the solution at a particular wavelength using a spectrometer. The value is 1.92. You find a value for molar absorptivity in a table of 19400 for that wavelength.</p>
<p>Substituting those values:</p>
</div>

<div class="block-formula">
\begin{aligned}
A &amp;= \epsilon~l~c \\
1.92 &amp;= 19400 \times 1 \times c \\
c &amp;= \frac{1.92}{19400} \\
&amp;= 9.90 \times 10^{-5}~mol~dm^{-3}
\end{aligned}
</div>

<div class="text-block">
<p>Notice what a very low concentration can be measured provided you are working with a substance with a very high molar absorptivity.</p>
<p>This method, of course, depends on you having access to an accurate value of molar absorptivity. It also assumes that the Beer-Lambert Law works over the whole concentration range (not true!).</p>
<p>It is much better to measure the concentration by plotting a calibration curve. It saves doing any calculations for one thing!</p>
</div>

<h3>Finding concentration by plotting a calibration curve</h3>

<div class="text-block">
<p>Doing it this way you don't have to rely on a value of molar absorptivity, the reliability of the Beer-Lambert Law, or even know the dimensions of the cell containing the solution.</p>
<p>What you do is make up a number of solutions of the compound you are investigating – each of accurately known concentration. Those concentrations should bracket the concentration you are trying to find – some less concentrated; some more concentrated. With coloured solutions, this isn't a problem. You would just make up solutions of accurately known concentrations some of which are a bit lighter and some a bit darker in colour.</p>
<p>For each solution, you measure the absorbance at the wavelength of strongest absorption – using the same container for each one. Then you plot a graph of that absorbance against concentration. This is a calibration curve.</p>
<p>According to the Beer-Lambert Law, absorbance is proportional to concentration, and so you would expect a straight line. That is true as long as the solutions are dilute, but the Law breaks down for solutions of higher concentration, and so you might get a curve under these circumstances.</p>
<p>As long as you are working from values either side of the one you are trying to find, that isn't a problem.</p>
<p>Having drawn a best fit line, the calibration curve will probably look something like the next diagram. (I've drawn it as a straight line because it is easier for me to draw than a curve(!), and it's what you will probably get if you are working with really dilute solutions. But if it turns out to be a curve, so be it!)</p>
</div>

<div class="image-container">
<img src="calibration1.gif">
</div>

<div class="text-block">
<p>Notice that no attempt has been made to force the line back through the origin. If the Beer-Lambert Law worked perfectly, it would pass through the origin, but you can't guarantee that it is working properly at the concentrations you are using.</p>
<p>Now all you have to do is to measure the absorbance of the solution with the unknown concentration at the same wavelength. If, for example, it had an absorbance of 0.600, you can just read the corresponding concentration from the graph as below.</p>
</div>

<div class="image-container">
<img src="calibration2.gif">
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-uvvisanalysis.pdf" target="_blank">Questions on using UV-visible spectroscopy in analysis</a>
<a href="../questions/a-uvvisanalysis.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../uvvisiblemenu.html#top">To the UV-visible spectroscopy menu</a>
<a href="../../analysismenu.html#top">To the analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>