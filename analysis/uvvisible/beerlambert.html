<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Absorption Spectra – The Beer-Lambert Law | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of the Beer-Lambert Law, and the terms absorbance and molar absorptivity (molar absorption coefficient)">
<meta name="keywords" content="UV, ultra-violet, spectrometer, spectrometry, spectrum, spectra, Beer, Lambert, Law, absorbance, absorptivity, absorption coefficient">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Absorption Spectra – The Beer-Lambert Law</h1>

<div class="text-block">
<p>This page takes a brief look at the Beer-Lambert Law and explains the use of the terms absorbance and molar absorptivity relating to UV-visible absorption spectrometry.</p>
</div>

<h2>Absorbance</h2>

<h3>Measuring the Absorbance of a Solution</h3>

<div class="text-block">
<p>If you have read the page about how an absorption spectrometer works, you will know that it passes a whole series of wavelengths of light through a solution of a substance (the sample cell) and also through an identical container (the reference cell) which only has solvent in it.</p>
</div>

<div class="note">
<p>Note: It isn't essential to read about how the <a href="spectrometer.html#top">spectrometer</a> works, but you could follow this link if you are interested or if it is on your syllabus. Everything you need from that page to understand the present topic is repeated below.</p>
</div>

<div class="text-block">
<p>For each wavelength of light passing through the spectrometer, the intensity of the light passing through the reference cell is measured. This is usually referred to as I<sub>0</sub> – that's I for Intensity.</p>
<p>The intensity of the light passing through the sample cell is also measured for that wavelength – given the symbol, I.</p>
<p>If I is less than I<sub>0</sub>, then obviously the sample has absorbed some of the light. A simple bit of maths is then done in the computer to convert this into something called the absorbance of the sample – given the symbol, A.</p>
<p>For reasons to do with the form of the Beer-Lambert Law (below), the relationship between A (the absorbance) and the two intensities is given by:</p>
</div>

<div class="block-formula">
A = log_{10}\frac{I_0}{I}
</div>

<div class="text-block">
<p>On most of the diagrams you will come across, the absorbance ranges from 0 to 1, but it can go higher than that.</p>
<p>An absorbance of 0 at some wavelength means that no light of that particular wavelength has been absorbed. The intensities of the sample and reference beam are both the same, so the ratio I<sub>0</sub>/I is 1. log<sub>10</sub> of 1 is zero.</p>
<p>An absorbance of 1 happens when 90% of the light at that wavelength has been absorbed – which means that the intensity is 10% of what it would otherwise be.</p>
<p>In that case, I<sub>0</sub>/I = 100 / 10 = 10 and log<sub>10</sub> of 10 is 1.</p>
</div>

<div class="note">
<p>Note: If you don't feel comfortable with logarithms, don't worry about it. Just accept that an absorbance scale often runs from zero to 1, but could go higher than that in extreme cases (in other words where more than 90% of a wavelength of light is absorbed).</p>
</div>

<h3>Absorbance Isn't Very Good for Making Comparisons</h3>

<h4>The importance of concentration</h4>

<div class="text-block">
<p>The proportion of the light absorbed will depend on how many molecules it interacts with. Suppose you have got a strongly coloured organic dye. If it is in a reasonably concentrated solution, it will have a very high absorbance because there are lots of molecules to interact with the light.</p>
<p>However, in an incredibly dilute solution, it may be very difficult to see that it is coloured at all. The absorbance is going to be very low.</p>
<p>Suppose then that you wanted to compare this dye with a different compound. Unless you took care to make allowance for the concentration, you couldn't make any sensible comparisons about which one absorbed the most light.</p>
</div>

<h4>The importance of the container shape</h4>

<div class="text-block">
<p>Suppose this time that you had a very dilute solution of the dye in a cube-shaped container so that the light travelled 1 cm through it. The absorbance isn't likely to be very high. On the other hand, suppose you passed the light through a tube 100 cm long containing the same solution. More light would be absorbed because it interacts with more molecules.</p>
<p>Again, if you want to draw sensible comparisons between solutions, you have to allow for the length of the solution the light is passing through.</p>
<p>Both concentration and solution length are allowed for in the Beer-Lambert Law.</p>
</div>

<h2>The Beer-Lambert Law</h2>

<h3>What the Law Looks Like</h3>

<div class="text-block">
<p>You will find that various different symbols are given for some of the terms in the equation – particularly for the concentration and the solution length. I'm going to use the obvious form where the concentration of the solution is "c" and the length is "l".</p>
</div>

<div class="block-formula">
\begin{gathered} 
log_{10}\frac{I_0}{I} = \epsilon~l~c \\ 
\\
\begin{aligned}
\epsilon &- \text{the molar absorbitivity} \\ 
l &- \text{length of solution the light passes through} \\ 
c &- \text{concentration of solution}
\end{aligned}
\end{gathered}
</div>

<div class="text-block">
<p>You should recognise the expression on the left of this equation as what we have just defined as the absorbance, A. You might also find the equation written in terms of A:</p>
</div>

<div class="block-formula">
\text{absorbance} = A = \epsilon~l~c
</div>

<div class="text-block">
<p>That's obviously easier to remember than the first one, but you would still have to learn the equation for absorbance. It might be useful to learn it in the form:</p>
</div>

<div class="block-formula">
A = log_{10}\frac{I_0}{I} = \epsilon~l~c
</div>

<div class="text-block">
<p>The Greek letter &epsilon; (epsilon) in these equations is called the molar absorptivity – or sometimes the molar absorption coefficient.</p>
</div>

<h3>Molar Absorptivity</h3>

<div class="text-block">
<p>If you rearrange the simplest of the equations above to give an expression for epsilon (the molar absorptivity), you get:</p>
</div>

<div class="block-formula">
\epsilon = \frac{A}{l~c}
</div>

<div class="text-block">
<p>Remember that the absorbance of a solution will vary as the concentration or the size of the container varies. Molar absorptivity compensates for this by dividing by both the concentration and the length of the solution that the light passes through. Essentially, it works out a value for what the absorbance would be under a standard set of conditions – the light travelling 1 cm through a solution of 1 mol dm<sup>-3</sup>.</p>
<p>That means that you can then make comparisons between one compound and another without having to worry about the concentration or solution length.</p>
<p>Values for molar absorptivity can vary hugely. For example, ethanal has two absorption peaks in its UV-visible spectrum – both in the ultra-violet. One of these corresponds to an electron being promoted from a lone pair on the oxygen into a &pi; anti-bonding orbital; the other from a &pi; bonding orbital into a &pi; anti-bonding orbital.</p>
</div>

<div class="note">
<p>Note: These jumps are described in detail on the page explaining the <a href="theory.html#top">theory of UV-visible spectrometry</a>. Depending on your background knowledge, you may have to read another page first, but there is a link to that on the theory page.</p>
</div>

<div class="text-block">
<p>The table gives values for the molar absorptivity of a solution of ethanal in hexane. Notice that there are no units given for absorptivity. That's quite common. If you want them, and assuming the length is in cm and the concentration is mol dm<sup>-3</sup>, the units are mol<sup>-1</sup> dm<sup>3</sup> cm<sup>-1</sup>.</p>
</div>

<table class="data-table">
<tbody>
<tr><th>electron jump</th><th>wavelength of maximum absorption (nm)</th><th>molar absorptivity</th></tr>

<tr><td>lone pair to &pi; anti-bonding orbital</td><td>290</td><td>15</td></tr>

<tr><td>pi bonding to &pi; anti-bonding orbital</td><td>180</td><td>10000</td></tr>
</tbody>
</table>

<div class="text-block">
<p>The ethanal obviously absorbs much more strongly at 180 nm than it does at 290 nm. (Although, in fact, the 180 nm absorption peak is outside the range of most spectrometers.)</p>
<p>You may come across diagrams of absorption spectra plotting absorptivity on the vertical axis rather than absorbance. However, if you look at the figures above and the scales that are going to be involved, you aren't really going to be able to spot the absorption at 290 nm. It will be a tiny little peak compared to the one at 180 nm.</p>
<p>To get around this, you may also come across diagrams in which the vertical axis is plotted as log<sub>10</sub>(molar absorptivity).</p>
<p>If you take the logs of the two numbers in the table, 15 becomes 1.18, while 10000 becomes 4. That makes it possible to plot both values easily, but produces strangely squashed-looking spectra!</p>
</div>

<div class="note">
<p>Note: Don't worry too much about this for the purposes of UK A-level or its equivalents. What you are going to be mainly concerned with is what wavelengths the absorptions peak at. Exactly how you plot the vertical scale doesn't affect this in any way. Whether you are looking at a plot involving absorbance, molar absorptivity or log<sub>10</sub>(molar absorptivity), it doesn't affect the wavelengths absorbed. The peaks and troughs of the spectrum just look a bit different (vertically stretched out or squashed), and the vertical scale will have different units – but the peaks will occur at exactly the same wavelengths.
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-uvvislaw.pdf" target="_blank">Questions on the Beer-Lambert Law</a>
<a href="../questions/a-uvvislaw.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../uvvisiblemenu.html#top">To the UV-visible spectroscopy menu</a>
<a href="../../analysismenu.html#top">To the analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>