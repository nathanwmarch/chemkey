<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Electromagnetic Radiation | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A simple introduction to the electromagnetic spectrum ranging from X-rays to radio waves, but with the emphasis on the UV and visible regions.">
<meta name="keywords" content="electromagnetic, radiation, X-ray, UV, ultra-violet, IR, infra-red, radio waves, wavelength, frequency, energy, spectrum">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Electromagnetic Radiation</h1>

<div class="text-block">
<p>This page is a basic introduction to the electromagnetic spectrum sufficient for chemistry students interested in UV-visible absorption spectroscopy. If you are looking for any sort of explanations suitable for physics courses, then I'm afraid this isn't the right place for you.</p>
</div>

<h2>Light as a Wave Form</h2>

<h3>Waves</h3>

<div class="text-block">
<p>Any wave is essentially just a way of shifting energy from one place to another – whether the fairly obvious transfer of energy in waves on the sea or in the much more difficult-to-imagine waves in light.</p>
<p>In waves on water, the energy is transferred by the movement of water molecules. But a particular water molecule doesn't travel all the way across the Atlantic – or even all the way across a pond. Depending on the depth of the water, water molecules follow a roughly circular path. As they move up to the top of the circle, the wave builds to a crest; as they move down again, you get a trough.</p>
<p>The energy is transferred by relatively small local movements in the environment. With water waves it is fairly easy to draw diagrams to show this happening with real molecules. With light it is more difficult.</p>
<p>The energy in light travels because of local fluctuating changes in electrical and magnetic fields – hence "electromagnetic" radiation.</p>
</div>

<div class="note">
<p>Note: It would be quite wrong to pretend that I am comfortable with this. Having spent quite a lot of time looking at web pages which try to explain electromagnetic radiation (or even water waves), do I feel that I really understand it? To be honest – no! Although there are some excellent diagrams and animations out there, I still feel as if this is a foreign land.</p>

<p>On old maps, where people didn't know enough about the region they were drawing, they sometimes gave up and wrote "Here be dragons". For me, physics is definitely "Here be dragons" territory.</p>

<p>If you are a chemistry student, you don't need to worry too much about this. As long as you are prepared to accept diagrams of wave forms, and can understand the relationships between wavelength, frequency and energy (see below), that's all you need.</p>
</div>

<h3>Wavelength, Frequency and the Speed of Light</h3>

<div class="text-block">
<p>If you draw a beam of light in the form of a wave (without worrying too much about what exactly is causing the wave!), the distance between two crests is called the wavelength of the light. (It could equally well be the distance between two troughs or any other two identical positions on the wave.)</p>
</div>

<div class="image-container">
<img src="lightwave1.gif">
</div>

<div class="text-block">
<p>You have to picture these wave crests as moving from left to right. If you counted the number of crests passing a particular point per second, you have the frequency of the light. It is measured in what used to be called "cycles per second", but is now called Hertz, Hz. Cycles per second and Hertz mean exactly the same thing.</p>
<p>Orange light, for example, has a frequency of about 5 × 10<sup>14</sup> Hz</span> (often quoted as 5 × 10<sup>8</sup> MHz</span> MHz – megahertz). That means that 5 × 10<sup>14</sup> wave peaks pass a given point every second.</p>
<p>Light has a constant speed through a given substance. For example, it always travels at a speed of approximately 3 × 10<sup>8</sup> metres per second in a vacuum. This is actually the speed that all electromagnetic radiation travels – not just visible light.</p>
<p>There is a simple relationship between the wavelength and frequency of a particular colour of light and the speed of light:</p>
</div>

<div class="block-formula">
\begin{gathered}
c = \lambda~\nu \\ 
\\
\begin{aligned} 
c &- \text{speed of light} \\ 
\lambda &- \text{wavelength} \\ 
\nu &- \text{frequency (the Greek letter nu)}
\end{aligned}
\end{gathered}
</div>

<div class="text-block">
<p> and you can rearrange this to work out the wavelength from a given frequency and vice versa:</p>
</div>

<div class="block-formula">
\begin{matrix}
\displaystyle \lambda = \frac{c}{\nu} & \Bigg\vert & \displaystyle \nu = \frac{c}{\lambda}
\end{matrix}
</div>

<div class="text-block">
<p>These relationships mean that if you increase the frequency, you must decrease the wavelength.</p>
</div>

<div class="image-container">
<img src="lightwave2.gif">
</div>

<div class="text-block">
<p>Compare this diagram with the similar one above.</p>
<p>...and, of course, the opposite is true. If the wavelength is longer, the frequency is lower.</p>
<p>It is really important that you feel comfortable with the relationship between frequency and wavelength. If you are given two figures for the wavelengths of two different colours of light, you need to have an immediate feel for which one has the higher frequency.</p>
<p>For example, if you were told that a particular colour of red light had a wavelength of 650 nm, and a green had a wavelength of 540 nm, it is important for you to know which has the higher frequency. (It's the green – a shorter wavelength means a higher frequency. Don't go on until that feels right!)</p>
</div>

<div class="note">
<p>Note: nm = nanometre = 10<sup>-9</sup> metres.</p>
</div>

<h3>The Frequency of Light and its Energy</h3>

<div class="text-block">
<p>Each particular frequency of light has a particular energy associated with it, given by another simple equation:</p>
</div>

<div class="block-formula">
\begin{gathered}
E = h~\nu \\
\\
\begin{aligned}
E &- energy~of~the~light \\
h &- Planck's~constant \\
\nu &- frequency~of~the~light
\end{aligned}
\end{gathered}
</div>

<div class="text-block">
<p>You can see that the higher the frequency, the higher the energy of the light.</p>
<p>So have you got this sorted out? Try it!</p>
<p>Light which has wavelengths of around 380 – 435 nm is seen as a sequence of violet colours. Various red colours have wavelengths around 625 – 740 nm. Which has the highest energy?</p>
<p>The light with the highest energy will be the one with the highest frequency – that will be the one with the smallest wavelength. In other words, violet light at the 380 nm end of its range.</p>
</div>

<h2>The Electromagnetic Spectrum</h2>

<h3>Visible Light</h3>

<div class="text-block">
<p>The diagram shows an approximation to the spectrum of visible light.</p>
</div>

<div class="image-container">
<img src="spectrum.gif">
</div>

<div class="note">
<p>Important: This isn't a real spectrum – it's a made-up drawing. The colours are only an approximation, and so are the wavelengths assigned to them. It doesn't pretend to be accurate!</p>
</div>

<div class="text-block">
<p>The main colour regions of the spectrum are approximately:</p>
</div>

<table class="data-table two-center">
<tbody>
<tr><th>colour region</th><th>wavelength<br>/ nm</th></tr>
<tr><td>violet</td><td>380 – 435</td></tr>
<tr><td>blue</td><td>435 – 500</td></tr>
<tr><td>cyan</td><td>500 – 520</td></tr>
<tr><td>green</td><td>520 – 565</td></tr>
<tr><td>yellow</td><td>565 – 590</td></tr>
<tr><td>orange</td><td>590 – 625</td></tr>
<tr><td>red</td><td>625 – 740</td></tr>
</tbody>
</table>

<div class="text-block">
<p>Don't assume that there is some clear cut-off point between all these colours. In reality, the colours just merge seamlessly into one another – much more seamlessly than in my diagram!</p>
</div>

<div class="note">
<p>Note: You will find that almost every source you look at for this information gives slightly different wavelengths for the different colours. These values (and the names for the colours) come from the <a href="http://hyperphysics.phy-astr.gsu.edu/hbase/hframe.html">HyperPhysics</a> website.</p>
</div>

<h3>Placing the Visible Spectrum in the Whole Electromagnetic Spectrum</h3>

<div class="text-block">
<p>The electromagnetic spectrum doesn't stop with the colours you can see. It is perfectly possible to have wavelengths shorter than violet light or longer than red light.</p>
<p>On the spectrum further up the page, I have shown the ultra-violet and the infra-red, but this can be extended even further into x-rays and radio waves, amongst others. The diagram shows the approximate positions of some of these on the spectrum.</p>
</div>

<div class="image-container">
<img src="emagspectrum.gif">
</div>

<div class="text-block">
<p>Once again, don't worry too much about the exact boundaries between the various sorts of electromagnetic radiation – because there are no boundaries. Just as with visible light, one sort of radiation merges into the next. Just be aware of the general pattern.</p>
<p>Also be aware that the energy associated with the various kinds of radiation increases as the frequency increases.</p>
</div>

<div class="note">
<p>Note: As before, don't take this diagram too literally! It is a composite of several diagrams from different sources,
nearly all of which give slightly different boundaries between the different named sorts of radiation.</p>

<p>You also mustn't think that this is what the spectrum would actually look like if you were to observe it. The problem lies in the way the scales are drawn. Neither the wavelength nor the frequency scales are simple linear scales – the gap between 10<sup>2</sup> and 10<sup>4</sup> is not the same as between 10^{-2}</span> and 10<sup>-4</sup>, for example (although they are drawn as the same in the diagram). Work it out! One gap is the difference between 100 and 10,000 (in other words 9,900); the other works out at only 0.0099! That means that in some parts, the spectrum should really be very stretched out, and in others very, very compressed. And because the wavelength and frequency scales are heading in opposite directions, the stretching or compression would be totally different depending on whether you were plotting the spectrum against wavelength or frequency.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-uvvisradiation.pdf" target="_blank">Questions on electromagnetic radiation</a>
<a href="../questions/a-uvvisradiation.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../uvvisiblemenu.html#top">To the UV-visible spectroscopy menu</a>
<a href="../../analysismenu.html#top">To the analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>