<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Double-beam UV-visible Absorption Spectrometer | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="A simple explanation of how a double-beam UV-visible absorption spectrometer works">
<meta name="keywords" content="UV, ultra-violet, IR, infra-red, spectrometer, double-beam, dual beam, spectrum, spectrometry">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Double-beam UV-visible Absorption Spectrometer</h1>

<div class="text-block">
<p>This page describes a double-beam UV-visible absorption spectrometer in enough detail to meet the requirements of UK-based chemistry courses for 16 – 18 year olds.</p>
</div>

<h2>The Overall Design</h2>

<h3>The Aims</h3>

<div class="text-block">
<p>If you pass white light through a coloured substance, some of the light gets absorbed. A solution containing hydrated copper(II) ions, for example, looks pale blue because the solution absorbs light from the red end of the spectrum. The remaining wavelengths in the light combine in the eye and brain to give the appearance of cyan (pale blue).</p>
<p>Some colourless substances also absorb light – but in the ultra-violet region. Since we can't see UV light, we don't notice this absorption.</p>
<p>Different substances absorb different wavelengths of light, and this can be used to help to identify the substance – the presence of particular metal ions, for example, or of particular functional groups in organic compounds.</p>
<p>The amount of absorption is also dependent on the concentration of the substance if it is in solution. Measurement of the amount of absorption can be used to find concentrations of very dilute solutions.</p>
<p>An absorption spectrometer measures the way that the light absorbed by a compound varies across the UV and visible spectrum.</p>
</div>

<h3>A Simple Double-beam Spectrometer</h3>

<div class="text-block">
<p>We'll start with the full diagram, and then explain exactly what is going on at each stage.</p>
</div>

<div class="image-container">
<img src="spectrometer.gif">
</div>
<div class="note">
<p>IMPORTANT! The colour-coding of the light beams through the spectrometer is NOT to show that some light is red or blue or green. The colours are simply to emphasise the two different paths that light can take through the device. Where the light is shown as a blue line, this is the path that it will always take. Where it is shown red or green, it will go either one way or the other – depending on how it strikes the rotating disc (see below).</p>
</div>

<h2>Explaining What's Happening</h2>

<h3>The Light Source</h3>

<div class="text-block">
<p>You need a light source which gives the entire visible spectrum plus the near ultra-violet so that you are covering the range from about 200 nm to about 800 nm. (This extends slightly into the near infra-red as well.)</p>
</div>

<div class="note">
<p>Note: "Near UV" and "near IR" simply means the parts of the UV and IR spectra which are close to the visible spectrum. If you aren't happy about how the various types of <a href="radiation.html#top">electromagnetic radiation</a> relate to each other, follow this link before you go on.</p>
</div>

<div class="text-block">
<p>You can't get this range of wavelengths from a single lamp, and so a combination of two is used – a deuterium lamp for the UV part of the spectrum, and a tungsten / halogen lamp for the visible part.</p>
</div>

<div class="note">
<p>Note: A deuterium lamp contains deuterium gas under low pressure subjected to a high voltage. It produces a continuous spectrum in the part of the UV spectrum we are interested in.</p>
</div>

<div class="text-block">
<p>The combined output of these two bulbs is focussed on to a diffraction grating.</p>
</div>

<h3>The Diffraction Grating and the Slit</h3>

<div class="text-block">
<p>You are probably familiar with the way that a prism splits light into its component colours. A diffraction grating does the same job, but more efficiently.</p>
</div>

<div class="image-container">
<img src="slit.gif">
</div>

<div class="text-block">
<p>The blue arrows show the way the various wavelengths of the light are sent off in different directions. The slit only allows light of a very narrow range of wavelengths through into the rest of the spectrometer.</p>
<p>By gradually rotating the diffraction grating, you can allow light from the whole spectrum (a tiny part of the range at a time) through into the rest of the instrument.</p>
</div>

<h3>The Rotating Discs</h3>

<div class="text-block">
<p>This is the clever bit! Each disc is made up of a number of different segments. Those in the machine we are describing have three different sections – other designs may have a different number.</p>
</div>

<div class="image-container">
<img src="spinningdisc.gif">
</div>

<div class="text-block">
<p>The light coming from the diffraction grating and slit will hit the rotating disc and one of three things can happen.</p>
</div>

<ol>
<li>If it hits the transparent section, it will go straight through and pass through the cell containing the sample. It is then bounced by a mirror onto a second rotating disc.<br>
This disc is rotating such that when the light arrives from the first disc, it meets the mirrored section of the second disc. That bounces it onto the detector.<br>
It is following the red path in the diagram:</li>

<div class="image-container">
<img src="redpath.gif">
</div>

<li>If the original beam of light from the slit hits the mirrored section of the first rotating disc, it is bounced down along the green path. After the mirror, it passes through a reference cell (more about that later).<br>
Finally the light gets to the second disc which is rotating in such a way that it meets the transparent section. It goes straight through to the detector.</li>

<div class="image-container">
<img src="greenpath.gif">
</div>

<li>If the light meets the first disc at the black section, it is blocked – and for a very short while no light passes through the spectrometer. This just allows the computer to make allowance for any current generated by the detector in the absence of any light.</li>
</ol>

<h3>The Sample and Reference Cells</h3>

<div class="text-block">
<p>These are small rectangular glass or quartz containers. They are often designed so that the light beam travels a distance of 1 cm through the contents.</p>
<p>The sample cell contains a solution of the substance you are testing – usually very dilute. The solvent is chosen so that it doesn't absorb any significant amount of light in the wavelength range we are interested in (200 – 800 nm).</p>
<p>The reference cell just contains the pure solvent.</p>
</div>

<h3>The Detector and Computer</h3>

<div class="text-block">
<p>The detector converts the incoming light into a current. The higher the current, the greater the intensity of the light.</p>
<p>For each wavelength of light passing through the spectrometer, the intensity of the light passing through the reference cell is measured. This is usually referred to as I<sub>0</sub> – that's I for Intensity.</p>
<p>The intensity of the light passing through the sample cell is also measured for that wavelength – given the symbol, I.</p>
<p>If I is less than I<sub>0</sub>, then obviously the sample has absorbed some of the light. A simple bit of maths is then done in the computer to convert this into something called the absorbance of the sample – given the symbol, A.</p>
<p>For reasons which will become clearer when we do a bit of theory on another page, the relationship between A and the two intensities is given by:</p>
</div>

<div class="block-formula">
A = log_{10}\frac{I_0}{I}
</div>

<div class="text-block">
<p>On most of the diagrams you will come across, the absorbance ranges from 0 to 1, but it can go higher than that.</p>
<p>An absorbance of 0 at some wavelength means that no light of that particular wavelength has been absorbed. The intensities of the sample and reference beam are both the same, so the ratio I<sub>0</sub>/I is 1. log<sub>10</sub> of 1 is zero.</p>
<p>An absorbance of 1 happens when 90% of the light at that wavelength has been absorbed – which means that the intensity is 10% of what it would otherwise be.</p>
<p>In that case, I<sub>0</sub>/I = 100 / 10 = 10 and log<sub>10</sub> of 10 is 1.</p>
</div>

<div class="note">
<p>Note: If you don't feel comfortable with logarithms, don't worry about it. Just accept that an absorbance scale often runs from zero to 1, but could go higher than that in extreme cases (in other words where more than 90% of a wavelength of light is absorbed).</p>
</div>

<h3>The Chart Recorder</h3>

<div class="text-block">
<p>Chart recorders usually plot absorbance against wavelength. The output might look like this:</p>
</div>

<div class="image-container">
<img src="absgraph1.gif">
</div>

<div class="text-block">
<p>This particular substance has what are known as absorbance peaks at 255 and 395 nm. How these arise and how they are interpreted are discussed on another page.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-uvvisspectrometer.pdf" target="_blank">Questions on the UV-visible absorption spectrometer</a>
<a href="../questions/a-uvvisspectrometer.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../uvvisiblemenu.html#top">To the UV-visible spectroscopy menu</a>
<a href="../../analysismenu.html#top">To the analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>