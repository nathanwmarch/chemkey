<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>UV-visible Absorption Spectra | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains what is happening when organic molecules absorb UV or visible light, and why it varies from compound to compound">
<meta name="keywords" content="UV, ultra-violet, spectrometer, spectrometry, spectrum, spectra, anti-bonding, bonding, orbital, orbitals, conjugation, conjugated, delocalisation, delocalised, absorption, absorb">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>UV-visible Absorption Spectra</h1>

<div class="text-block">
<p>This page explains what happens when organic compounds absorb UV or visible light, and why the wavelength of light absorbed varies from compound to compound.</p>
</div>

<div class="note">
<p>Important: If you have come direct to this page from a search engine, you should be aware that it follows on from an introductory page explaining some <a href="bonding.html#top">essential bonding for UV-visible spectrometry</a>. You need to read this before you go on with this page.
</div>

<h2>What Happens When Light is Absorbed by Molecules?</h2>

<h3>Promotion of Electrons</h3>

<div class="text-block">
<p>When we were talking about the various sorts of orbitals present in organic compounds on the introductory page (see above), you will have come across this diagram showing their relative energies:</p>
</div>

<div class="image-container">
<img src="levels.gif">
</div>

<div class="text-block">
<p>Remember that the diagram isn't intended to be to scale – it just shows the relative placing of the different orbitals.</p>
<p>When light passes through the compound, energy from the light is used to promote an electron from a bonding or non-bonding orbital into one of the empty anti-bonding orbitals.</p>
</div>

<div class="note">
<p>Important: If you don't know exactly what I mean by bonding, non-bonding and anti-bonding orbitals, or don't remember the diagram, go back and read the <a href="bonding.html#top">introductory page</a> again.</p>
</div>

<div class="text-block">
<p>The possible electron jumps that light might cause are:</p>
</div>

<div class="image-container">
<img src="jumps.gif">
</div>

<div class="text-block">
<p>In each possible case, an electron is excited from a full orbital into an empty anti-bonding orbital. Each jump takes energy from the light, and a big jump obviously needs more energy than a small one.</p>
<p>Each wavelength of light has a particular energy associated with it. If that particular amount of energy is just right for making one of these energy jumps, then that wavelength will be absorbed – its energy will have been used in promoting an electron.</p>
<p>We need to work out what the relationship is between the energy gap and the wavelength absorbed. Does, for example, a bigger energy gap mean that light of a lower wavelength will be absorbed – or what?</p>
<p>It is easier to start with the relationship between the frequency of light absorbed and its energy:</p>
</div>

 <div class="block-formula">
\begin{gathered}
 E = h~\nu \\
 \\
 \begin{aligned}
 E &- \text{energy of the light} \\
 h &- \text{Planck's constant} \\
 \nu &- \text{frequency of the light}
 \end{aligned}
 \end{gathered}
 </div>

<div class="text-block">
<p>You can see that if you want a high energy jump, you will have to absorb light of a higher frequency. The greater the frequency, the greater the energy.</p>
<p>That's easy – but unfortunately UV-visible absorption spectra are always given using wavelengths of light rather than frequency. That means that you need to know the relationship between wavelength and frequency.</p>
</div>

<div class="block-formula">
\begin{gathered}
\lambda = \frac{c}{\nu} \\
\\
\begin{aligned}
\lambda &- \text{wavelength} \\
c &- \text{speed of light} \\
\nu &- \text{frequency}
\end{aligned}
\end{gathered}
</div>

<div class="text-block">
<p>You can see from this that the higher the frequency is, the lower the wavelength is.</p>
<p>So If you have a bigger energy jump, you will absorb light with a higher frequency – which is the same as saying that you will absorb light with a lower wavelength.</p>
</div>

<h4>Important summary</h4>

<div class="text-block">
<p>The larger the energy jump, the lower the wavelength of the light absorbed.</p>
</div>

<div class="note">
<p>Note: It is obviously better if you can work this out in case you forget it, but you may feel that it is a lot easier just to learn the last statement. However you do it, you must be confident about this relationship.</p>

<p>If you need more help with sorting out these relationships, you will find them discussed more slowly on a page in this section about <a href="radiation.html#top">electromagnetic radiation</a>.</p>
</div>

<h3>Some Jumps are More Important Than Others for Absorption Spectrometry</h3>

<div class="text-block">
<p>An absorption spectrometer works in a range from about 200 nm (in the near ultra-violet) to about 800 nm (in the very near infra-red). Only a limited number of the possible electron jumps absorb light in that region.</p>
</div>

<div class="note">
<p>Note: If you are interested, there is a description of how a <a href="spectrometer.html#top">double beam absorption spectrometer</a> works on another page in this section. It isn't necessary to know about this in order to understand the rest of this page.</p>
</div>

<div class="text-block">
<p>Look again at the possible jumps. This time, the important jumps are shown in black, and a less important one in grey. The grey dotted arrows show jumps which absorb light outside the region of the spectrum we are working in.</p>
</div>

<div class="image-container">
<img src="jumps2.gif">
</div>

<div class="text-block">
<p>Remember that bigger jumps need more energy and so absorb light with a shorter wavelength. The jumps shown with grey dotted arrows absorb UV light of wavelength less that 200 nm.</p>
<p>The important jumps are:</p>
</div>

<ul>
<li>from &pi; bonding orbitals to &pi; anti-bonding orbitals;</li>

<li>from non-bonding orbitals to &pi; anti-bonding orbitals;</li>

<li>from non-bonding orbitals to &sigma; anti-bonding orbitals.</li>
</ul>

<div class="text-block">
<p>That means that in order to absorb light in the region from 200 – 800 nm (which is where the spectra are measured), the molecule must contain either &pi; bonds or atoms with non-bonding orbitals. Remember that a non-bonding orbital is a lone pair on, say, oxygen, nitrogen or a halogen.</p>
<p>Groups in a molecule which absorb light are known as chromophores.</p>
</div>

<div class="note">
<p>Note: A Canadian university site describes "chromophore" as "one of those useful but sloppy words whose meaning depends somewhat on the context."</p>

<p>As you will find shortly, highly delocalised systems in a molecule are often responsible for absorbing light in the visible region and making the compound look coloured. The delocalisation may involve several different types of group – benzene rings, carbon-carbon double bonds, carbon-oxygen double bonds, lone pairs on nitrogen or oxygen – and so on.</p>

<p>Some people talk as if the whole of the delocalised system was the chromophore; others tend to think on terms of the contributions of individual parts of the system.</p>

<p>It seems to me to be more logical to think in terms of the whole system because it is electron jumps within the system as a whole that cause absorption. If your examiners take a different view, however, obviously you should go with what they want. The only way you will find that out is to look at recent exam papers and mark schemes. If you are a UK A-level (or equivalent) student follow this link to the <a href="../../syllabuses.html#top">syllabuses</a> page to find out how to get hold of these if you haven't already got them.</p>
</div>

<h3>What Does an Absorption Spectrum Look Like</h3>

<div class="text-block">
<p>The diagram below shows a simple UV-visible absorption spectrum for buta-1,3-diene – a molecule we will talk more about later. Absorbance (on the vertical axis) is just a measure of the amount of light absorbed. The higher the value, the more of a particular wavelength is being absorbed.</p>
</div>

<div class="image-container">
<img src="specbutadiene.gif">
</div>

<div class="text-block">
<p>You will see that absorption peaks at a value of 217 nm. This is in the ultra-violet and so there would be no visible sign of any light being absorbed – buta-1,3-diene is colourless. You read the symbol on the graph as "lambda-max".</p>
<p>In buta-1,3-diene, CH<sub>2</sub>=CH-CH=CH<sub>2</sub>, there are no non-bonding electrons. That means that the only electron jumps taking place (within the range that the spectrometer can measure) are from &pi; bonding to &pi; anti-bonding orbitals.</p>
</div>

<div class="note">
<p>Note: The spectrum shown is only a simplified sketch graph – it doesn't pretend to any great accuracy!</p>

<p>If you are really wide-awake you might wonder why the graph looks like it does with a broad absorption peak rather than a single line at 217 nm. A jump from a &pi; bonding orbital to a &pi; anti-bonding orbital ought to have a fixed energy and therefore absorb a fixed wavelength. The compound is in fact absorbing over a whole range of wavelengths suggesting a whole range of energy jumps.</p>

<p>This problem arises because rotations and vibrations in the molecule are continually changing the energies of the orbitals – and that, of course, means that the gaps between them are continually changing as well. The result is that absorption takes place over a range of wavelengths rather than at one fixed one.</p>You don't need to worry about this at this level.
</div>

<h3>A Chromophore Producing Two Peaks</h3>

<div class="text-block">
<p>A chromophore such as the carbon-oxygen double bond in ethanal, for example, obviously has &pi; electrons as a part of the double bond, but also has lone pairs on the oxygen atom.</p>
<p>That means that both of the important absorptions from the last energy diagram are possible.</p>
</div>

<div class="note">
<p>Note: If you aren't too sure about the bonding in a <a href="../../basicorg/bonding/carbonyl.html#top">carbon-oxygen double bond</a>, it would be worth following this link before you go on.</p>
</div>

<div class="text-block">
<p>You can get an electron excited from a &pi; bonding to a &pi; anti-bonding orbital, or you can get one excited from an oxygen lone pair (a non-bonding orbital) into a &pi; anti-bonding orbital.</p>
</div>

<div class="image-container">
<img src="jumps3.gif">
</div>

<div class="note">
<p>Note: Before you read on, work out which of these will absorb light with the longer wavelength. Try it! If you can work this out for yourself, you have cracked one of the most difficult things about this topic.</p>
</div>

<div class="text-block">
<p>The non-bonding orbital has a higher energy than a &pi; bonding orbital. That means that the jump from an oxygen lone pair into a &pi; anti-bonding orbital needs less energy. That means it absorbs light of a lower frequency and therefore a higher wavelength.</p>
<p>Ethanal can therefore absorb light of two different wavelengths:</p>
</div>

<ul>
<li>the &pi; bonding to &pi; anti-bonding absorption peaks at 180 nm;</li>

<li>the non-bonding to &pi; anti-bonding absorption peaks at 290 nm.</li>
</ul>

<div class="text-block">
<p>Both of these absorptions are in the ultra-violet, but most spectrometers won't pick up the one at 180 nm because they work in the range from 200 – 800 nm.</p>
</div>

<h3>The Importance of Conjugation and Delocalisation in What Wavelength is Absorbed</h3>

<div class="text-block">
<p>Consider these three molecules:</p>
</div>

<div class="inline">
<div class="chemical-structure" name="ethene" type="2d"></div>
<div class="chemical-structure" name="buta-1,3-diene" type="2d"></div>
<div class="chemical-structure" name="E-hexa-1,3,5-triene" type="2d"></div>
</div>

<div class="text-block">
<p>Ethene contains a simple isolated carbon-carbon double bond, but the other two have conjugated double bonds. In these cases, there is delocalisation of the &pi; bonding orbitals over the whole molecule.</p>
</div>

<div class="note">
<p>Important: If you don't know understand about conjugation, go back and read that section of the <a href="bonding.html#top">introductory page</a> again.</p>
</div>

<div class="text-block">
<p>Now look at the wavelengths of the light which each of these molecules absorbs.</p>
</div>

<table class="convertable-table">
<thead>
<tr><th>molecule</th><th>wavelength of maximum absorption<br>/ nm</th></tr>
</thead>
<tbody>
<tr><td>ethene</td><td>171</td></tr>
<tr><td>buta-1,3-diene</td><td>217</td></tr>
<tr><td>hexa-1,3,5-triene</td><td>258</td></tr>
</tbody>
</table>

<div class="text-block">
<p>All of the molecules give similar UV-visible absorption spectra – the only difference being that the absorptions move to longer and longer wavelengths as the amount of delocalisation in the molecule increases.</p>
<p>Why is this?</p>
<p>You can actually work out what must be happening.</p>
</div>

<ul>
<li>The maximum absorption is moving to longer wavelengths as the amount of delocalisation increases.</li>

<li>Therefore maximum absorption is moving to shorter frequencies as the amount of delocalisation increases.</li>

<li>Therefore absorption needs less energy as the amount of delocalisation increases.</li>

<li>Therefore there must be less energy gap between the bonding and anti-bonding orbitals as the amount of delocalisation increases.</li>
</ul>

<div class="text-block">
<p> and that's what is happening.</p>
<p>Compare ethene with buta-1,3-diene. In ethene, there is one &pi; bonding orbital and one &pi; anti-bonding orbital. In buta-1,3-diene, there are two &pi; bonding orbitals and two &pi; anti-bonding orbitals. This is all discussed in detail on the introductory page that you should have read.</p>
</div>

<div class="image-container">
<img src="jumps4.gif">
</div>

<div class="text-block">
<p>The highest occupied molecular orbital is often referred to as the HOMO – in these cases, it is a &pi; bonding orbital. The lowest unoccupied molecular orbital (the LUMO) is a &pi; anti-bonding orbital.</p>
<p>Notice that the gap between these has fallen. It takes less energy to excite an electron in the buta-1,3-diene case than with ethene.</p>
<p>In the hexa-1,3,5-triene case, it is less still.</p>
</div>

<div class="image-container">
<img src="jumps5.gif">
</div>

<div class="note">
<p>Note: In this case, you will have to work out for yourself why there are 3 bonding and 3 anti-bonding &pi; orbitals in the hexa-1,3,5-triene. If you are interested, you can work it out in the same way that I have used for buta-1,3-diene on the <a href="bonding.html#top">introductory page</a>. It isn't essential that you do this – all that matters is that you can see the pattern – the more the delocalisation, the longer the wavelength absorbed.</p>
</div>

<div class="text-block">
<p>If you extend this to compounds with really massive delocalisation, the wavelength absorbed will eventually be high enough to be in the visible region of the spectrum, and the compound will then be seen as coloured. A good example of this is the orange plant pigment, beta-carotene – present in carrots, for example.</p>
</div>

<h3>Why is Beta-carotene Orange?</h3>

<div class="text-block">
<p>Beta-carotene has the sort of delocalisation that we've just been looking at, but on a much greater scale with 11 carbon-carbon double bonds conjugated together. The diagram shows the structure of beta-carotene with the alternating double and single bonds shown in red.</p>
</div>

<div class="image-container">
<img src="carotene.gif">
</div>

<div class="text-block">
<p>The more delocalisation there is, the smaller the gap between the highest energy &pi; bonding orbital and the lowest energy &pi; anti-bonding orbital. To promote an electron therefore takes less energy in beta-carotene than in the cases we've looked at so far – because the gap between the levels is less.</p>
<p>Remember that less energy means a lower frequency of light gets absorbed – and that's equivalent to a longer wavelength.</p>
<p>Beta-carotene absorbs throughout the ultra-violet region into the violet – but particularly strongly in the visible region between about 400 and 500 nm with a peak about 470 nm.</p>
<p>If you have read the page in this section about electromagnetic radiation, you might remember that the wavelengths associated with the various colours are approximately:</p>
</div>

<table class="convertable-table">
<thead>
<tr><th>colour region</th><th>wavelength<br>/ nm</th></tr>
</thead>
<tbody>
<tr><td>violet</td><td>380 – 435</td></tr>
<tr><td>blue</td><td>435 – 500</td></tr>
<tr><td>cyan</td><td>500 – 520</td></tr>
<tr><td>green</td><td>520 – 565</td></tr>
<tr><td>yellow</td><td>565 – 590</td></tr>
<tr><td>orange</td><td>590 – 625</td></tr>
<tr><td>red</td><td>625 – 740</td></tr>
</tbody>
</table>

<div class="text-block">
<p>So if the absorption is strongest in the violet to cyan region, what colour will you actually see? It is tempting to think that you can work it out from the colours that are left – and in this particular case, you wouldn't be far wrong. Unfortunately, it isn't as simple as that!</p>
<p>Sometimes what you actually see is quite unexpected. Mixing different wavelengths of light doesn't give you the same result as mixing paints or other pigments.</p>
<p>You can, however, sometimes get some estimate of the colour you would see using the idea of complementary colours.</p>
</div>

<h4>Complementary colours</h4>

<div class="text-block">
<p>If you arrange some colours in a circle, you get a "colour wheel". The diagram shows one possible version of this. An internet search will throw up many different versions!</p>
</div>

<div class="image-container">
<img src="colourwheel.gif">
</div>

<div class="text-block">
<p>Colours directly opposite each other on the colour wheel are said to be complementary colours. Blue and yellow are complementary colours; red and cyan are complementary; and so are green and magenta.</p>
<p>Mixing together two complementary colours of light will give you white light.</p>
</div>

<div class="note">
<p>Beware: That is NOT the same as mixing together paint colours. If you mix yellow and blue paint you don't get white paint. Is this confusing? YES!</p>
</div>

<div class="text-block">
<p>What this all means is that if a particular colour is absorbed from white light, what your eye detects by mixing up all the other wavelengths of light is its complementary colour.</p>
<p>In the beta-carotene case, the situation is more confused because you are absorbing such a range of wavelengths. However, if you think of the peak absorption running from the blue into the cyan, it would be reasonable to think of the colour you would see as being opposite that where yellow runs into red – in other words, orange.</p>
<p>You will find the colours a bit more clear-cut in the other two examples we'll talk about below.</p>
</div>

<div class="note">
<p>Note: If you are interested in understanding the relationship between colour absorbed and colour seen (beyond the very basic description above), find your way to lesson 2 ("Color and Vision") of "Light Waves and Vision" on <a href="http://www.physicsclassroom.com/">The Physics Classroom</a>. I'm not giving a direct link to those pages, because that site is still developing and it is safer to give a link to the front page of the site. This is the most understandable explanation I have found anywhere on the web.</p>
</div>

<h2>Applying This to the Colour Changes of Two Indicators</h2>

<h3>Phenolphthalein</h3>

<div class="text-block">
<p>You have probably used phenolphthalein as an acid-base indicator, and will know that it is colourless in acidic conditions and magenta (bright pink) in an alkaline solution. How is this colour change related to changes in the molecule?</p>
<p>The structures of the two differently coloured forms are:</p>
</div>

<div class="image-container">
<img src="phph1.gif">
</div>

<div class="text-block">
<p>Both of these absorb light in the ultra-violet, but the one on the right also absorbs in the visible with a peak at 553 nm.</p>
<p>The molecule in acid solution is colourless because our eyes can't detect the fact that some light is being absorbed in the ultra-violet. However, our eyes do detect the absorption at 553 nm produced by the form in alkaline solution.</p>
<p>553 nm is in the green region of the spectrum. If you look back at the colour wheel, you will find that the complementary colour of green is magenta – and that's the colour you see.</p>
<p>So why does the colour change as the structure changes?</p>
<p>What we have is a shift to absorption at a higher wavelength in alkaline solution. As we've already seen, a shift to higher wavelength is associated with a greater degree of delocalisation.</p>
<p>Here is a modified diagram of the structure of the form in acidic solution – the colourless form. The extent of the delocalisation is shown in red.</p>
</div>

<div class="image-container">
<img src="phph2.gif">
</div>

<div class="text-block">
<p>Notice that there is delocalisation over each of the three rings – extending out over the carbon-oxygen double bond, and to the various oxygen atoms because of their lone pairs.</p>
<p>But the delocalisation doesn't extend over the whole molecule. The carbon atom in the centre with its four single bonds prevents the three delocalised regions interacting with each other.</p>
<p>Now compare that with the magenta form:</p>
</div>

<div class="image-container">
<img src="phph3.gif">
</div>

<div class="text-block">
<p>The rearrangement now lets the delocalisation extend over the entire ion. This greater delocalisation lowers the energy gap between the highest occupied molecular orbital and the lowest unoccupied &pi; anti-bonding orbital. It needs less energy to make the jump and so a longer wavelength of light is absorbed.</p>
</div>

<p>Remember: Increasing the amount of delocalisation shifts the absorption peak to a higher wavelength.</p>

<div class="note">
<p>Note: Don't panic at the thought that you might have to draw these structures in an exam. At UK A-level (and its equivalent) standard, it won't happen. However, if you were given the structures of the two forms of phenolphthalein and asked to explain the colour change, that could make a good question (depending, of course, on what exactly your syllabus expects you to be able to do).</p>
</div>

<h3>Methyl orange</h3>

<div class="text-block">
<p>You will know that methyl orange is yellow in alkaline solutions and red in acidic ones.</p>
<p>The structure in alkaline solution is:</p>
</div>

<div class="image-container">
<img src="mobase.gif">
</div>

<div class="text-block">
<p>In acid solution, a hydrogen ion is (perhaps unexpectedly) picked up on one of the nitrogens in the nitrogen-nitrogen double bond.</p>
</div>

<div class="image-container">
<img src="moacid2.gif">
</div>

<div class="text-block">
<p>This now gets a lot more complicated! The positive charge on the nitrogen is delocalised (spread around over the structure) – especially out towards the right-hand end of the molecule as we've written it. The normally drawn structure for the red form of methyl orange is</p>
</div>

<div class="image-container">
<img src="moacid.gif">
</div>

<div class="text-block">
<p>But this can be seriously misleading as regards the amount of delocalisation in the structure for reasons discussed below (after the red warning box) if you are interested.</p>
</div>

<div class="note">
<p>Important: If you have read the
<a href="bonding.html#top">introductory page</a>, you will know that the delocalisation doesn't extend out over the sulfonate group. That doesn't affect the rest of this argument in any way. This group isn't changed by the addition of a hydrogen ion. All we are actually interested in is the effect on the delocalisation over the rest of the molecule – the two benzene rings and the two nitrogen-containing groups.</p>
</div>

<h4>So which is the more delocalised structure – red or yellow?</h4>

<div class="text-block">
<p>Let's work backwards from the absorption spectra to see if that helps.</p>
<p>The yellow form has an absorption peak at about 440 nm. That's in the blue region of the spectrum, and the complementary colour of blue is yellow. That's exactly what you would expect.</p>
<p>The red form has an absorption peak at about 520 nm. That's at the edge of the cyan region of the spectrum, and the complementary colour of cyan is red. Again, there's nothing unexpected here.</p>
<p>Notice that the change from the yellow form to the red form has produced an increase in the wavelength absorbed. An increase in wavelength suggests an increase in delocalisation.</p>
<p>That means that there must be more delocalisation in the red form than in the yellow one.</p>
<p>Why? For the purposes of UK A-level (and its equivalents), that's probably unanswerable. However, if you are interested, there's a possible answer below</p>
</div>

<div class="note">
<p>Warning! The rest of this page is going to get seriously difficult. If you are a UK A-level (or equivalent) student, it goes well beyond the level of understanding that you are likely to need. The reason for including it is to try to remove the impression that the red form is less delocalised than the yellow one because of the way the structure is usually drawn.</p>
</div>

<div class="text-block">
<p>Here again is the structure of the yellow form:</p>
</div>

<div class="image-container">
<img src="mobase.gif">
</div>

<div class="text-block">
<p>Delocalisation will extend over most of the structure – out as far as the lone pair on the right-hand nitrogen atom.</p>
<p>If you use the normally written structure for the red form, the delocalisation seems to be broken in the middle – the pattern of alternating single and double bonds seems to be lost.</p>
</div>

<div class="image-container">
<img src="moacid.gif">
</div>

<div class="text-block">
<p>But that is to misunderstand what this last structure represents.</p>
</div>

<h4>Canonical forms</h4>

<div class="text-block">
<p>If you draw the two possible Kekulé structures for benzene, you will know that the real structure of benzene isn't like either of them. The real structure is somewhere between the two – all the bonds are identical and somewhere between single and double in character. That's because of the delocalisation in benzene.</p>
</div>

<div class="image-container">
<img src="benzene.gif">
</div>

<div class="text-block">
<p>The two structures are known as canonical forms, and they can each be thought of as adding some knowledge to the real structure. For example, the bond drawn at the top right of the molecule is neither truly single or double, but somewhere in between. Similarly with all the other bonds.</p>
</div>

<div class="note">
<p>Note: If you haven't come across canonical forms as a way of representing delocalisation, it is important that you don't imagine that the molecule is rapidly flipping from one structure to another. The double-headed arrows mean something different. A mule is a hybrid of a donkey and a horse. In this notation, you could represent a mule by writing donkey and horse connected by a double-headed arrow. Neitherdonkey nor horse accurately represents what a mule looks like, but with a bit of imagination you could build up a fairly good picture of a mule by combining together the characteristics of both donkey and horse. But a mule obviously doesn't spend its time rapidly changing back and forth between being a donkey and a horse!</p>
</div>

<div class="text-block">
<p>The two structures we've previously drawn for the red form of methyl orange are also canonical forms – two out of lots of forms that could be drawn for this structure. We could represent the delocalised structure by:</p>
</div>

<div class="image-container">
<img src="mocanonical.gif">
</div>

<div class="text-block">
<p>These two forms can be thought of as the result of electron movements in the structure, and curly arrows are often used to show how one structure can lead to the other.</p>
</div>

<div class="image-container">
<img src="mocanonical2.gif">
</div>

<div class="text-block">
<p>In reality, the electrons haven't shifted fully either one way or the other. Just as in the benzene case, the actual structure lies somewhere in between these.</p>
<p>You must also realise that drawing canonical forms has no effect on the underlying geometry of the structure. Bond types or lengths or angles don't change in the real structure.</p>
<p>For example, the lone pairs on the nitrogen atoms shown in the last diagram are both involved with the delocalisation. For this to happen all the bonds around these nitrogens must be in the same plane, with the lone pair sticking up so that it can overlap sideways with orbitals on the next-door atoms. The fact that in each of the two canonical forms one of these nitrogens is shown as if it had an ammonia-like arrangement of the bonds is potentially misleading – and makes it look as if the delocalisation is broken.</p>
<p>The problem is that there is no easy way of representing a complex delocalised structure in simple structural diagrams. It is bad enough with benzene – with something as complicated as methyl orange any method just leads to possible confusion if you aren't used to working with canonical forms.</p>
<p>It gets even more complicated! If you were doing this properly there would be a host of other canonical forms with different arrangements of double and single bonds and with the positive charge located at various places around the rings and on the other nitrogen atom.</p>
<p>The real structure can't be represented properly by any one of this multitude of canonical forms, but each gives a hint of how the delocalisation works.</p>
<p>If we take the two forms we have written as perhaps the two most important ones, it suggests that there is delocalisation of the electrons over the whole structure, but that electron density is a bit low around the two nitrogens carrying the positive charge on one canonical form or the other.</p>
</div>

<h4>So why is the red form more delocalised than the yellow one?</h4>

<div class="text-block">
<p>Finally, we get around to an attempt at an explanation as to why the delocalisation is greater in the red form of methyl orange in acid solution than in the yellow one in alkaline solution.</p>
<p>The answer may lie in the fact that the lone pair on the nitrogen at the right-hand end of the structure as we've drawn it is more fully involved in the delocalisation in the red form. The canonical form with the positive charge on that nitrogen suggests a significant movement of that lone pair towards the rest of the molecule.</p>
<p>Doesn't the same thing happen to the lone pair on the same nitrogen in the yellow form of methyl orange? Not to the same extent.</p>
<p>Any canonical form that you draw in which that happens produces another negatively charged atom somewhere in the rest of the structure. Separating negative and positive charges like this is energetically unfavourable. In the red form, we aren't producing a new separation of charge – just shifting a positive charge around the structure.</p>
<p>Please remember: This is high-level stuff. To really understand it you need to have met canonical forms before and practised drawing them. This is degree level chemistry and not A level (or its equivalent). You do NOT need any of this (from below the red warning) unless perhaps you are reading this as a university student.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-uvvistheory.pdf" target="_blank">Questions on the theory behind UV-visible absorption spectrometry</a>
<a href="../questions/a-uvvistheory.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../uvvisiblemenu.html#top">To the UV-visible spectroscopy menu</a>
<a href="../../analysismenu.html#top">To the analysis menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>