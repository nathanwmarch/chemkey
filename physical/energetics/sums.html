<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Hess's Law and Enthalpy Change Calculations | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="This page explains Hess's Law, and introduces simple enthalpy change calculations">
<meta name="keywords" content="enthalpy, change, reaction, formation, combustion, hess, hess's, law, calculations, cycles, sums">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Hess's Law and Enthalpy Change Calculations</h1>

<div class="text-block">
<p>This page explains Hess's Law, and uses it to do some simple enthalpy change calculations involving enthalpy changes of reaction, formation and combustion.</p>
</div>

<h2>Hess's Law</h2>

<h3>Stating Hess's Law</h3>

<div class="text-block">
<p>Hess's Law is the most important law in this part of chemistry. Most calculations follow from it.</p>
</div>

<div class="definition">
<p>Hess's Law: the enthalpy change accompanying a chemical change is independent of the route by which the chemical change occurs.</p>
</div>

<h3>Explaining Hess's Law</h3>

<div class="text-block">
<p>Hess's Law is saying that if you convert reactants A into products B, the overall enthalpy change will be exactly the same whether you do it in one step or two steps or however many steps.</p>
<p>If you look at the change on an enthalpy diagram, that is actually fairly obvious.</p>
</div>

<div class="image-container"><img src="hess1.gif"></div>

<div class="text-block">
<p>This shows the enthalpy changes for an exothermic reaction using two different ways of getting from reactants A to products B. In one case, you do a direct conversion; in the other, you use a two-step process involving some intermediates.</p>
<p>In either case, the overall enthalpy change must be the same, because it is governed by the relative positions of the reactants and products on the enthalpy diagram.</p>
<p>If you go via the intermediates, you do have to put in some extra heat energy to start with, but you get it back again in the second stage of the reaction sequence.</p>
<p>However many stages the reaction is done in, ultimately the overall enthalpy change will be the same, because the positions of the reactants and products on an enthalpy diagram will always be the same.</p>
</div>

<div class="note">
<p>Note: It is possibly confusing that I am switching between the terms enthalpy and energy. Enthalpy change is simply a particular measure of energy change. You will remember that the enthalpy change is the heat evolved or absorbed during a reaction happening at constant pressure.</p>
<p>I have labelled the vertical scale on this particular diagram as enthalpy rather than energy, because we are specifically thinking about enthalpy changes. I could have just kept to the more general term "energy", but I prefer to be accurate.</p>
</div>

<div class="text-block">
<p>You can do calculations by setting them out as enthalpy diagrams as above, but there is a much simpler way of doing it which needs virtually no thought.</p>
<p>You could set out the above diagram as:</p>
</div>

<div class="image-container"><img src="hess2.gif"></div>

<div class="text-block">
<p>Hess's Law says that the overall enthalpy change in these two routes will be the same. That means that if you already know two of the values of enthalpy change for the three separate reactions shown on this diagram (the three black arrows), you can easily calculate the third – as you will see below.</p>
<p>The big advantage of doing it this way is that you don't have to worry about the relative positions of everything on an enthalpy diagram. It is completely irrelevant whether a particular enthalpy change is positive or negative.</p>
</div>

<h4>Warnings!</h4>

<div class="text-block">
<p>Although most calculations you will come across will fit into a triangular diagram like the above, you may also come across other slightly more complex cases needing more steps. That doesn't make it any harder!</p>
<p>You need to take care in choosing your two routes. The pattern will not always look like the one above. You will see that in the examples below.</p>
</div>

<h2>Enthalpy Change Calculations Using Hess's Law Cycles</h2>

<h3>Working Out an Enthalpy Change of Formation From Enthalpy Changes of Combustion</h3>

<div class="text-block">
<p>If you have read an earlier page in this section, you may remember that I mentioned that the standard enthalpy change of formation of benzene was impossible to measure directly. That is because carbon and hydrogen won't react to make benzene.</p>
</div>

<div class="note">
<p>Important: If you don't know (without thinking about it too much) exactly what is meant by standard enthalpy change of formation or combustion, you must get this sorted out now. Re-read the page about <a href="definitions.html#top">enthalpy change definitions</a> before you go any further – and learn them!</p>
</div>

<div class="text-block">
<p>Standard enthalpy changes of combustion, &Delta;H°<sub>c</sub> are relatively easy to measure. For benzene, carbon and hydrogen, these are:</p>
</div>

<table class="data-table two-right">
<thead>
<tr><th> </th><th>&Delta;H°<sub>c</sub><br>/ kJ mol<sup>-1</sup></th></tr>
</thead>
<tbody>
<tr><td>C<sub>6</sub>H<sub>6</sub>(l)</td><td>-3267</td></tr>
<tr><td>C(s)</td><td>-394</td></tr>
<tr><td>H<sub>2</sub>(g)</td><td>-286</td></tr>
</tbody></table>

<div class="text-block">
<p>First you have to design your cycle.</p>
</div>

<ul>
<li>Write down the enthalpy change you want to find as a simple horizontal equation, and write &Delta;H over the top of the arrow. (In diagrams of this sort, we often miss off the standard symbol just to avoid clutter.)</li>
<li>Then fit the other information you have onto the same diagram to make a Hess's Law cycle, writing the known enthalpy changes over the arrows for each of the other changes.</li>
<li>Finally, find two routes around the diagram, always going with the flow of the various arrows. You must never have one of your route arrows going in the opposite direction to one of the equation arrows underneath it.</li>
</ul>

<div class="text-block">
<p>In this case, what we are trying to find is the standard enthalpy change of formation of benzene, so that equation goes horizontally.</p>
</div>

<div class="image-container"><img src="hess3.gif"></div>

<div class="text-block">
<p>You will notice that I haven't bothered to include the oxygen that the various things are burning in. The amount of oxygen isn't critical because you just use an excess anyway, and including it really confuses the diagram.</p>
<p>Why have I drawn a box around the carbon dioxide and water at the bottom of the cycle? I tend to do this if I can't get all the arrows to point to exactly the right things. In this case, there is no obvious way of getting the arrow from the benzene to point at both the carbon dioxide and the water. Drawing the box isn't essential – I just find that it helps me to see what is going on more easily.</p>
<p>Notice that you may have to multiply the figures you are using. For example, standard enthalpy changes of combustion start with 1 mole of the substance you are burning. In this case, the equations need you to burn 6 moles of carbon, and 3 moles of hydrogen molecules. Forgetting to do this is probably the most common mistake you are likely to make.</p>
<p>How were the two routes chosen? Remember that you have to go with the flow of the arrows. Choose your starting point as the corner that only has arrows leaving from it. Choose your end point as the corner which only has arrows arriving.</p>
</div>

<h5>Now do the calculation:</h5>

<div class="text-block">
<p>Hess's Law says that the enthalpy changes on the two routes are the same. That means that:</p>
</div>

<div class="block-formula">
\Delta H - 3267 = (6 \times {-}394) + (3 \times {-}286)
</div>

<h5>Rearranging and solving:</h5>

<div class="block-formula">
\begin{aligned}
\Delta H &= (6 \times {-}394) + (3 \times {-}286) + 3267 \\
\Delta H &= {+}45 \text{ kJ mol}^{-1}
\end{aligned}
</div>

<div class="note">
<p>Note: If you have a good memory, you might remember that I gave a figure of +49 kJ mol<sup>-1</sup> for the standard enthalpy change of formation of benzene on an earlier page in this section. So why is this answer different?</p>
<p>The main problem here is that I have taken values of the enthalpies of combustion of hydrogen and carbon to 3 significant figures (commonly done in calculations at this level). That introduces small errors if you are just taking each figure once. However, here you are multiplying the error in the carbon value by 6, and the error in the hydrogen value by 3. If you are interested, you could rework the calculation using a value of -393.5 for the carbon and -285.8 for the hydrogen. That gives an answer of +48.6.</p>
<p>So why didn't I use more accurate values in the first place? Because I wanted to illustrate this problem! Answers you get to questions like this are often a bit out. The reason usually lies either in rounding errors (as in this case), or the fact that the data may have come from a different source or sources. Trying to get consistent data can be a bit of a nightmare.</p>
</div>

<h3>Working Out an Enthalpy Change of Reaction From Enthalpy Changes of Formation</h3>

<div class="text-block">
<p>This is the commonest use of simple Hess's Law cycles that you are likely to come across.</p>
<p>In this case, we are going to calculate the enthalpy change for the reaction between ethene and hydrogen chloride gases to make chloroethane gas from the standard enthalpy of formation values in the table. If you have never come across this reaction before, it makes no difference.</p>
</div>

<table class="data-table two-right">
<tbody><tr><th> </th><th>&Delta;H°<sub>f</sub> (kJ mol<sup>-1</sup>)</th></tr>
<tr><td>C<sub>2</sub>H<sub>4</sub>(g)</td><td>+52.2</td></tr>
<tr><td>HCl(g)</td><td>-92.3</td></tr>
<tr><td>C<sub>2</sub>H<sub>5</sub>Cl(g)</td><td>-109</td></tr>
</tbody></table>

<div class="note">
<p>Note: I'm not too happy about the value for chloroethane! The data sources I normally use give a wide range of values. The one I have chosen is an average value from the <a href="http://webbook.nist.gov/cgi/cbook.cgi?ID=C75003&amp;Units=SI&amp;Mask=1#Thermo-Gas">NIST Chemistry WebBook</a>. This uncertainty doesn't affect how you do the calculation in any way, but the answer may not be exactly right – don't quote it as if it was right.</p>
</div>

<div class="text-block">
<p>In the cycle below, this reaction has been written horizontally, and the enthalpy of formation values added to complete the cycle.</p>
</div>

<div class="image-container"><img src="hess4.gif"></div>

<div class="text-block">
<p>Again, notice the box drawn around the elements at the bottom, because it isn't possible to connect all the individual elements to the compounds they are forming in any tidy way. Be careful to count up all the atoms you need to use, and make sure they are written as they occur in the elements in their standard state. You mustn't, for example, write the hydrogens as 5H(g), because the standard state for hydrogen is H<sub>2</sub>.</p>
</div>

<div class="note">
<p>Note: In truth, if I am doing this type of enthalpy sum myself (with nobody watching!), I tend to just write the word "elements" in the bottom box to save the bother of working out exactly how many of everything I need. I would be wary of doing that in an exam, though.</p>
</div>

<div class="text-block">
<p>And now the calculation. Just write down all the enthalpy changes which make up the two routes, and equate them.</p>
</div>

<div class="block-formula">
52.2 - 92.3 + \Delta H = {-}109
</div>

<h5>Rearranging and solving:</h5>

<div class="block-formula">
\begin{aligned}
\Delta H &= {-}52.2 + 92.3 -109 \\
\Delta H &= {-}68.9 \text{ kJ mol}^{-1}
\end{aligned}
</div>

<div class="note">
<p>Note: I am afraid that this is as much as I feel I can give you on this topic without risking sales of my book, or ending up in breach of contract with my publishers. Unfortunately, it isn't enough for you to be confident of being able to do these calculations every time. Apart from anything else, you need lots of practice.</p>
<p>I have talked this through more gently in the book, with lots of examples. If you chose to work through chapter 5 in the book, you would be confident that you could do any chemical energetics calculation that you were given.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-energeticshess.pdf" target="_blank">Questions on Hess's Law</a>
<a href="../questions/a-energeticshess.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../energeticsmenu.html#top">To the chemical energetics menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>