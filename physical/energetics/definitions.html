<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Various Enthalpy Change Definitions | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="This page explains what an enthalpy change is, and then gives a definition and brief comment on the various kinds of enthalpy change that you will need at this level.">
<meta name="keywords" content="enthalpy, change, reaction, formation, combustion">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Various Enthalpy Change Definitions</h1>

<div class="text-block">
<p>This page explains what an enthalpy change is, and then gives a definition and brief comment for three of the various kinds of enthalpy change that you will come across. You will find some more definitions on other pages in this section.</p>
<p>It is essential that you learn the definitions. You aren't going to be able to do any calculations successfully if you don't know exactly what all the terms mean.</p>
</div>

<h2>Enthalpy changes</h2>

<div class="text-block">
<p>Enthalpy change is the name given to the amount of heat evolved or absorbed in a reaction carried out at constant pressure. It is given the symbol &Delta;H, read as "delta H".</p>
</div>

<div class="note">
<p>Note: The term "enthalpy change" only applies to reactions done at constant pressure. That is actually how most lab reactions are done – in tubes or flasks (or whatever) open to the atmosphere, so that the pressure is constant at atmospheric pressure.</p>
<p>The phrase "at constant pressure" is an essential part of the definition but, apart from that, you are unlikely to need to worry about it if you are doing a UK-based exam at the equivalent of A level.</p>
</div>

<h3>Standard Enthalpy Changes</h3>

<div class="text-block">
<p>Standard enthalpy changes refer to reactions done under standard conditions, and with everything present in their standard states. Standard states are sometimes referred to as "reference states".</p>
<h4>Standard conditions</h4>
<p>Standard conditions are:</p>
</div>

<ul>
<li>298 K (25°C)</li>
<li>a pressure of 1 bar (100 kPa).</li>
<li>where solutions are involved, a concentration of 1 mol dm<sup>-3</sup></li>
</ul>

<div class="note">
<p>Warning! Standard pressure was originally defined as 1 atmosphere (101.325 kPa), and you will still find that in older books (including my calculations book). At the time of writing (August 2010) there was at least one UK-based syllabus that was still talking in terms of "1 atmosphere". It is essential to check your syllabus to find out exactly what you need to learn.</p>
</div>

<h4>Standard states</h4>

<div class="text-block">
<p>For a standard enthalpy change everything has to be present in its standard state. That is the physical and chemical state that you would expect to find it in under standard conditions.</p>
<p>That means that the standard state for water, for example, is liquid water, H<sub>2</sub>O(l) – not steam or water vapour or ice.</p>
<p>Oxygen's standard state is the gas, O<sub>2</sub>(g) – not liquid oxygen or oxygen atoms.</p>
<p>For elements which have allotropes (two different forms of the element in the same physical state), the standard state is the most energetically stable of the allotropes.</p>
<p>For example, carbon exists in the solid state as both diamond and graphite. Graphite is energetically slightly more stable than diamond, and so graphite is taken as the standard state of carbon.</p>
<p>Similarly, under standard conditions, oxygen can exist as O<sub>2</sub> (simply called oxygen) or as O<sub>3</sub> (called ozone – but it is just an allotrope of oxygen). The O<sub>2</sub> form is far more energetically stable than O<sub>3</sub>, so the standard state for oxygen is the common O<sub>2</sub>(g).</p>
</div>

<h4>The symbol for standard enthalpy changes</h4>

<div class="text-block">
<p>The symbol for a standard enthalpy change is &Delta;H°, read as "delta H standard" or, perhaps more commonly, as "delta H nought".</p>
</div>

<div class="note">
<p>Note: Technically, the "o" in the symbol should have a horizontal line through it, extending out at each side. This is such a bother to produce convincingly without the risk of different computers producing unreliable results, that I shall use the common practice of simplifying it to "o".</p>
</div>

<h4>Standard enthalpy change of reaction, &Delta;H°<sub>r</sub></h4>

<div class="text-block">
<p>Remember that an enthalpy change is the heat evolved or absorbed when a reaction takes place at constant pressure.</p>
</div>

<div class="definition">
<p>The standard enthalpy change of a reaction is the enthalpy change which occurs when equation quantities of materials react under standard conditions, and with everything in its standard state.</p>
</div>

<div class="text-block">
<p>That needs exploring a bit.</p>
<p>Here is a simple reaction between hydrogen and oxygen to make water:</p>
</div>

<div class="block-formula">
\begin{gathered}
2\text{H}_{2(g)} + \text{O}_{2(g)} \longrightarrow 2\text{H}_2\text{O}_{(l)} \\
\Delta H^{o}_r = {-}572 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<ul>
<li>First, notice that the symbol for a standard enthalpy change of reaction is &Delta;H°<sub>r</sub>. For enthalpy changes of reaction, the "r" (for reaction) is often missed off – it is just assumed.</li>
<li>The "kJ mol<sup>-1</sup>" (kilojoules per mole) doesn't refer to any particular substance in the equation. Instead it refers to the quantities of all the substances given in the equation. In this case, 572 kJ of heat is evolved when 2 moles of hydrogen gas react with 1 mole of oxygen gas to form 2 moles of liquid water.</li>
<li>Notice that everything is in its standard state. In particular, the water has to be formed as a liquid.</li>
<li>And there is a hidden problem! The figure quoted is for the reaction under standard conditions, but hydrogen and oxygen don't react under standard conditions.<br>Whenever a standard enthalpy change is quoted, standard conditions are assumed. If the reaction has to be done under different conditions, a different enthalpy change would be recorded. That has to be calculated back to what it would be under standard conditions. Fortunately, you don't have to know how to do that at this level.</li>
</ul>

<h3>Some Important Types of Enthalpy Change</h3>

<h4>Standard enthalpy change of formation, &Delta;H°<sub>f</sub></h4>

<div class="definition">
<p>The standard enthalpy change of formation: the enthalpy change which occurs when one mole of a compound is formed from its elements under standard conditions, and with everything in its standard state.</p>
</div>

<div class="note">
<p>Note: When you are trying to learn these definitions, you can make life easier for yourself by picking out the key bit, and adding the other bits on afterwards. The key bit about this definition is that you are forming 1 mole of a compound from its elements. All the stuff about enthalpy change and standard conditions and standard states is common to most of these definitions.</p>
</div>

<div class="text-block">
<p>The equation showing the standard enthalpy change of formation for water is:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{H}_{2(g)} + \frac{1}{2}\text{O}_{2(g)} \longrightarrow \overbrace{\text{H}_2\text{O}_{(l)}}^{\clap{\text{\color{#467abf}{1 mole formed}}}{}} \\
\Delta H^o_f = {-}286 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>When you are writing one of these equations for enthalpy change of formation, you must end up with 1 mole of the compound. If that needs you to write fractions on the left-hand side of the equation, that is OK. (In fact, it is not just OK, it is essential, because otherwise you will end up with more than 1 mole of compound, or else the equation won't balance!)</p>
<p>The equation shows that 286 kJ of heat energy is given out when 1 mole of liquid water is formed from its elements under standard conditions.</p>
<p>Standard enthalpy changes of formation can be written for any compound, even if you can't make it directly from the elements. For example, the standard enthalpy change of formation for liquid benzene is +49 kJ mol<sup>-1</sup>. The equation is:</p>
</div>

<div class="block-formula">
\begin{gathered}
6\text{C}_{(s)} + 3\text{H}_{2(g)} \longrightarrow \text{C}_6\text{H}_{6(l)} \\
\Delta H^o_f = {+}49 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>If carbon won't react with hydrogen to make benzene, what is the point of this, and how does anybody know what the enthalpy change is?</p>
<p>What the figure of +49 shows is the relative positions of benzene and its elements on an energy diagram:</p>
</div>

<div class="image-container"><img src="benzenediag.gif"></div>

<div class="text-block">
<p>How do we know this if the reaction doesn't happen? It is actually very simple to calculate it from other values which we can measure – for example, from enthalpy changes of combustion (coming up next). We will come back to this again when we look at calculations on another page.</p>
<p>Knowing the enthalpy changes of formation of compounds enables you to calculate the enthalpy changes in a whole host of reactions and, again, we will explore that in a bit more detail on another page.</p>
</div>

<h5>And one final comment about enthalpy changes of formation:</h5>

<div class="text-block">
<p>The standard enthalpy change of formation of an element in its standard state is zero. That's an important fact. The reason is obvious</p>
<p>For example, if you "make" one mole of hydrogen gas starting from one mole of hydrogen gas you aren't changing it in any way, so you wouldn't expect any enthalpy change. That is equally true of any other element. The enthalpy change of formation of any element has to be zero because of the way enthalpy change of formation is defined.</p>
</div>

<h4>Standard enthalpy change of combustion, &Delta;H°c</h4>

<div class="definition">
<p>The standard enthalpy change of combustion of a compound is the enthalpy change which occurs when one mole of the compound is burned completely in oxygen under standard conditions, and with everything in its standard state.</p>
</div>

<div class="text-block">
<p>The enthalpy change of combustion will always have a negative value, of course, because burning always releases heat.</p>
</div>

<h5>Two examples:</h5>

<div class="block-formula">
\begin{gathered}
\text{H}_{2(g)} + \frac{1}{2}\text{O}_{2(g)} \longrightarrow \text{H}_2\text{O}_{(l)} \\
\Delta H^o_c = {-}286 \text{ kJ mol}^{-1} \\
\\
\text{C}_2\text{H}_{6(g)} + 3\frac{1}{2}\text{O}_{2(g)} \longrightarrow 2\text{CO}_{2(g)} + 3\text{H}_2\text{O}_{(l)} \\
\Delta H^o_c = {-}1560 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<h5>Notice:</h5>

<ul>
<li>Enthalpy of combustion equations will often contain fractions, because you must start with only 1 mole of whatever you are burning.</li>
<li>If you are talking about standard enthalpy changes of combustion, everything must be in its standard state. One important result of this is that any water you write amongst the products must be there as liquid water.<br>Similarly, if you are burning something like ethanol, which is a liquid under standard conditions, you must show it as a liquid in any equation you use.</li>
<li>Notice also that the equation and amount of heat evolved in the hydrogen case is exactly the same as you have already come across further up the page. At that time, it was illustrating the enthalpy of formation of water. That can happen in some simple cases. Talking about the enthalpy change of formation of water is exactly the same as talking about the enthalpy change of combustion of hydrogen.</li>
</ul>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-energeticsdefs.pdf" target="_blank">Questions on enthalpy change definitions</a>
<a href="../questions/a-energeticsdefs.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../energeticsmenu.html#top">To the chemical energetics menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>