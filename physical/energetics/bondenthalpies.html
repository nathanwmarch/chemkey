<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Bond Enthalpy (Bond Energy) | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="This page introduces bond enthalpies and looks at some simple calculations involving them.">
<meta name="keywords" content="bond, enthalpy, energy, strength, vaporisation, change, calculations, sums">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Bond Enthalpy (Bond Energy)</h1>

<div class="text-block">
<p>This page introduces bond enthalpies (bond energies) and looks at some simple calculations involving them.</p>
<p>One of the most confusing things about this is the way the words are used. These days, the term "bond enthalpy" is normally used, but you will also find it described as "bond energy" – sometimes in the same article. An even older term is "bond strength". So you can take all these terms as being interchangeable.</p>
<p>As you will see below, though, "bond enthalpy" is used in several different ways, and you might need to be careful about this.</p>
</div>

<div class="note">
<p>Note: Bond enthalpies quoted from different sources often vary by a few kilojoules, even if they are referring to exactly the same thing. Don't worry if you come across slightly different values. As you will see later on this page, calculations involving bond enthalpies hardly ever give accurate answers anyway.</p>
</div>

<h2>Explaining the Terms</h2>

<h3>Bond Dissociation Enthalpy and Mean Bond Enthalpy</h3>

<h4>Simple Diatomic Molecules</h4>

<div class="text-block">
<p>A diatomic molecule is one that only contains two atoms. They could be the same (for example, Cl<sub>2</sub>) or different (for example, HCl).</p>
<p>The bond dissociation enthalpy is the energy needed to break one mole of the bond to give separated atoms – everything being in the gas state.</p>
<p>Important! The point about everything being in the gas state is essential. You cannot use bond enthalpies to do calculations directly from substances starting in the liquid or solid state.</p>
<p>As an example of bond dissociation enthalpy, to break up 1 mole of gaseous hydrogen chloride molecules into separate gaseous hydrogen and chlorine atoms takes 432 kJ. The bond dissociation enthalpy for the H-Cl bond is +432 kJ mol<sup>-1</sup>.</p>
</div>

<h4>More complicated molecules</h4>

<div class="text-block">
<p>What happens if the molecule has several bonds, rather than just 1?</p>
<p>Consider methane, CH<sub>4</sub>. It contains four identical C-H bonds, and it seems reasonable that they should all have the same bond enthalpy.</p>
<p>However, if you took methane to pieces one hydrogen at a time, it needs a different amount of energy to break each of the four C-H bonds. Every time you break a hydrogen off the carbon, the environment of those left behind changes. And the strength of a bond is affected by what else is around it.</p>
<p>In cases like this, the bond enthalpy quoted is an average value.</p>
<p>In the methane case, you can work out how much energy is needed to break a mole of methane gas into gaseous carbon and hydrogen atoms. That comes to +1662 kJ and involves breaking 4 moles of C-H bonds. The average bond energy is therefore +1662/4 kJ, which is +415.5 kJ per mole of bonds.</p>
<p>That means that many bond enthalpies are actually quoted as mean (or average) bond enthalpies, although it might not actually say so. Mean bond enthalpies are sometimes referred to as "bond enthalpy terms".</p>
<p>In fact, tables of bond enthalpies give average values in another sense as well, particularly in organic chemistry. The bond enthalpy of, say, the C-H bond varies depending on what is around it in the molecule. So data tables use average values which will work well enough in most cases.</p>
<p>That means that if you use the C-H value in some calculation, you can't be sure that it exactly fits the molecule you are working with. So don't expect calculations using mean bond enthalpies to give very reliable answers.</p>
<p>You may well have to know the difference between a bond dissociation enthalpy and a mean bond enthalpy, and you should be aware that the word mean (or average) is used in two slightly different senses. But for calculation purposes, it isn't something you need to worry about. Just use the values you are given.</p>
</div>

<div class="note">
<p>Important: The rest of this page assumes that you have already read the page about <a href="sums.html">Hess's Law and enthalpy change calculations</a>. If you have come straight to the current page from a search engine, you won't make sense of the way the calculations are set out unless you first read the Hess's Law page.</p>
</div>

<h2>Finding Enthalpy Changes of Reaction From Bond Enthalpies</h2>

<div class="text-block">
<p>I can only give a brief introduction here, because this is covered in careful, step-by-step detail in my <a href="../../book.html">chemistry calculations book</a>.</p>
</div>

<h3>Cases Where Everything Present is Gaseous</h3>

<div class="text-block">
<p>Remember that you can only use bond enthalpies directly if everything you are working with is in the gas state.</p>
</div>

<h4>Using the same method as for other enthalpy sums</h4>

<div class="text-block">
<p>We are going to estimate the enthalpy change of reaction for the reaction between carbon monoxide and steam. This is a part of the manufacturing process for hydrogen.</p>
</div>

<div class="block-formula">
\text{CO}_{(g)} + \text{H}_2\text{O}_{(g)} \longrightarrow \text{CO}_{2(g)} + \text{H}_{2(g)}
</div>

<div class="text-block">
<p>The bond enthalpies are:</p>
</div>

<table class="convertable-table two-right">
<thead>
<tr><th> </th><th>bond enthalpy<br>/ kJ mol<sup>-1</sup></th></tr>
</thead>
<tbody>
<tr><td>C-O in carbon monoxide</td><td>+1077</td></tr>
<tr><td>C-O in carbon dioxide</td><td>+805</td></tr>
<tr><td>O-H</td><td>+464</td></tr>
<tr><td>H-H</td><td>+436</td></tr>
</tbody></table>

<div class="text-block">
<p>So let's do the sum. Here is the cycle – make sure that you understand exactly why it is the way it is.</p>
</div>

<div class="image-container"><img src="watergascycle.gif"></div>

<div class="text-block">
<p>And now equate the two routes, and solve the equation to find the enthalpy change of reaction.</p>
</div>

<div class="block-formula">
\begin{aligned}
\Delta H + (2 \times 805) + 436 &= 1077 + (2 \times 464) \\
\Delta H &= 1077 + (2 \times 464) - (2 \times 805) - 436 \\
\Delta H &= {-}41 \text{ kJ mol}^{-1}
\end{aligned}
</div>

<h4>Using a short-cut method for simple cases</h4>

<div class="text-block">
<p>You could do any bond enthalpy sum by the method above – taking the molecules completely to pieces and then remaking the bonds. If you are happy doing it that way, just go on doing it that way.</p>
<p>However, if you are prepared to give it some thought, you can save a bit of time – although only in very simple cases where the changes in a molecule are very small.</p>
<p>For example, chlorine reacts with ethane to give chloroethane and hydrogen chloride gases.</p>
</div>

<div class="image-container"><img src="c2h6cl2.gif"></div>

<div class="text-block">
<p>(All of these are gases. I have left the state symbols out this time to avoid cluttering the diagram.)</p>
<p>It is always a good idea to draw full structural formulae when you are doing bond enthalpy calculations. It makes it much easier to count up how many of each type of bond you have to break and make.</p>
<p>If you look at the equation carefully, you can see what I mean by a "simple case". Hardly anything has changed in this reaction. You could work out how much energy is needed to break every bond, and how much is given out in making the new ones, but quite a lot of the time, you are just remaking the same bond.</p>
<p>All that has actually changed is that you have broken a C-H bond and a Cl-Cl bond, and made a new C-Cl bond and a new H-Cl bond. So you can just work those out.</p>
</div>

<table class="convertable-table two-right">
<thead>
<tr><th> </th><th>bond enthalpy<br>/ kJ mol<sup>-1</sup></th></tr>
</thead>
<tbody>
<tr><td>C-H</td><td>+413</td></tr>
<tr><td>Cl-Cl</td><td>+243</td></tr>
<tr><td>C-Cl</td><td>+346</td></tr>
<tr><td>H-Cl</td><td>+432</td></tr>
</tbody></table>

<div class="text-block">
<p>Work out the energy needed to break C-H and Cl-Cl:</p>
</div>

<div class="block-formula">
413 + 243 = {+}656 \text{ kJ mol}^{-1}
</div>

<div class="text-block">
<p>Work out the energy released when you make C-Cl and H-Cl:</p>
</div>

<div class="block-formula">
{-}346 - 432 = {-}778 \text{ kJ mol}^{-1}
</div>

<div class="text-block">
<p>So the net change is +656 – 778 = -122 kJ mol<sup>-1</sup></p>
</div>

<div class="note">
<p>Note: Even if you choose not to use this method, it might be a good idea to be aware of it. It is possible to imagine an examiner setting a question which assumes that you will use this method, and therefore doesn't give a particular bond enthalpy value that you would need if you did it by the longer method. For example, in the case above, you don't actually need to know the C-C bond enthalpy.</p>
</div>

<h3>Cases Where You Have a Liquid Present</h3>

<div class="text-block">
<p>I have to keep on saying this! Remember that you can only use bond enthalpies directly if everything you are working with is in the gas state.</p>
<p>If you have one or more liquids present, you need an extra energy term to work out the enthalpy change when you convert from liquid to gas, or vice versa. That term is the enthalpy change of vaporisation, and is given the symbol &Delta;H<sub>vap</sub> or &Delta;H<sub>v</sub>.</p>
<p>This is the enthalpy change when 1 mole of the liquid converts to gas at its boiling point with a pressure of 1 bar (100 kPa).</p>
<p>(Older sources might quote 1 atmosphere rather than 1 bar.)</p>
<p>For water, the enthalpy change of vaporisation is +41 kJ mol<sup>-1</sup>. That means that it take 41 kJ to change 1 mole of water into steam. If 1 mole of steam condenses into water, the enthalpy change would be -41 kJ. Changing from liquid to gas needs heat; changing gas back to liquid releases exactly the same amount of heat.</p>
<p>To see how this fits into bond enthalpy calculations, we will estimate the enthalpy change of combustion of methane – in other words, the enthalpy change for this reaction:</p>
</div>

<div class="image-container"><img src="burnch4eqn.gif"></div>

<div class="text-block">
<p>Notice that the product is liquid water. You cannot apply bond enthalpies to this. You must first convert it into steam. To do this you have to supply 41 kJ mol<sup>-1</sup>.</p>
<p>The bond enthalpies you need are:</p>
</div>


<table class="convertable-table two-right">
<thead>
<tr><th> </th><th>bond enthalpy<br>/ kJ mol<sup>-1</sup></th></tr>
</thead>
<tbody>
<tr><td>C-H</td><td>+413</td></tr>
<tr><td>O=O</td><td>+498</td></tr>
<tr><td>C=O in carbon dioxide</td><td>+805</td></tr>
<tr><td>O-H</td><td>+464</td></tr>
</tbody></table>
<div class="text-block">
<p>The cycle looks like this:</p>
</div>

<div class="image-container"><img src="burnch4cycle.gif"></div>

<div class="text-block">
<p>This obviously looks more confusing than the cycles we've looked at before, but apart from the extra enthalpy change of vaporisation stage, it isn't really any more difficult. Before you go on, make sure that you can see why every single number and arrow on this diagram is there.</p>
<p>In particular, make sure that you can see why the first 4 appears in the expression "4(+464)". That is an easy thing to get wrong. (In fact, when I first drew this diagram, I carelessly wrote 2 instead of 4 at that point!)</p>
<p>That's the hard bit done – now the calculation:</p>
</div>

<div class="block-formula">
\begin{gathered}
\Delta H + (2 \times 805) + (2 \times 41) + (4 \times 464) = (4 \times 413) + (2 \times 498) \\
\\
\Delta H = (4 \times 413) + (2 \times 498) - (2 \times 805) - (2 \times 41) - (4 \times 464) \\
\\
\Delta H = {-}900 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>The measured enthalpy change of combustion is -890 kJ mol<sup>-1</sup>, and so this answer agrees to within about 1%. As bond enthalpy calculations go, that's a pretty good estimate.</p>
</div>

<div class="note">
<p>Note: Because this is all covered in more detail in my <a href="../../book.html">calculations book</a>, I am afraid that this is as far as I am prepared to go with this topic. The book will give you a lot more examples, including some variations such as calculating bond enthalpies from enthalpies of formation, and vice versa.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-energeticsbonds.pdf" target="_blank">Questions on bond enthalpies</a>
<a href="../questions/a-energeticsbonds.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../energeticsmenu.html#top">To the chemical energetics menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>