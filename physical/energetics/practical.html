<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Simple Measurements of Enthalpy Changes | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="This page is a brief introduction to simple lab measurements of enthalpy changes, and some pointers to further information">
<meta name="keywords" content="enthalpy, change, measurement">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Simple Measurements of Enthalpy Changes</h1>

<div class="text-block">
<p>This page is just a brief introduction to simple measurements of enthalpy changes of reaction which can be easily carried out in a lab. There are also pointers to other sources of information.</p>
</div>

<h2>Two Common Enthalpy Change Measurements</h2>

<h3>Enthalpy Changes Involving Solutions</h3>

<h4>The experiments</h4>

<div class="text-block">
<p>There are a whole range of different enthalpy changes that can be measured by reacting solutions (or a solution plus a solid) in a simple expanded polystyrene cup. A common example would be the measurement of the enthalpy change of neutralisation of, say, hydrochloric acid and sodium hydroxide solution.</p>
<p>The polystyrene cup serves to insulate the reaction mixture, and slows heat losses from the side and bottom. Heat is still lost from the surface of the liquid mixture, of course, and that can be reduced by using a polystyrene lid with a hole for a thermometer.</p>
<p>You can allow for heat losses during the reaction by plotting a cooling curve.</p>
<p>All of this is described in some detail at the beginning of Chapter 5 of my <a href="../../book.html">chemistry calculations book</a>, and I can't repeat it on Chemguide.</p>
<p>There are other sources of error in these experiments – in particular in the accuracy of the thermometer used. Since the temperature rise probably won't be very great, you would need to use the most accurate thermometer possible in order to keep the percentage error low.</p>
</div>

<h4>The calculations</h4>

<div class="text-block">
<p>The heat evolved or absorbed during a reaction is given by the expression:</p>
</div>

<div class="definition full-width">
<span class="inline-formula">\text{heat in/out} = \text{mass} \times \text{specific heat capacity} \times \text{temperature change}</span>
</div>
<div class="text-block">
<p>That can be written in symbols as </p>
</div>
<div class="definition">
<span class="inline-formula">q = mc\Delta T</span>
</div>
<div class="text-block">
<p>You will find that the specific heat is sometimes given the symbol "c" and sometimes the symbol "s".</p>
<p>The specific heat of a substance is the amount of heat needed to increase the temperature of 1 gram of it by 1 K (which is the same as 1°C).</p>
<p>So for water (the value you are most likely to come across), the specific heat is 4.18 J g<sup>-1</sup> K<sup>-1</sup>.</p>
<p>That means that it takes 4.18 joules to increase the temperature of 1 gram of water by 1 K (or 1°C).</p>
<p>In experiments of this sort, you will quite commonly have measured out a volume of solution rather than a mass, and it is very common to assume that the density of the solution is exactly the same as water (1 g cm<sup>-3</sup>). That means that 25 cm<sup>3</sup> of solution would have a mass of 25 g.</p>
<p>That is an approximation, though, and it does introduce some error.</p>
<p>The assumption is also made that the specific heat of a solution is the same as the specific heat of water. That's another approximation, and it also introduces errors into the answer.</p>
<p>What is generally true is that these errors are relatively small compared with errors caused by heat losses, and so on.</p>
<p>To do the calculation, you would normally just work out the amount of heat evolved or absorbed in your particular reaction, and then scale it up to give an enthalpy change per mole.</p>
</div>

<h3>Enthalpy Changes of Combustion</h3>

<div class="text-block">
<p>As normally measured in a lab at this level, these are far less accurate than the simple solution reactions above. It is impossible to eliminate heat losses, or to be sure that you have complete combustion.</p>
<p>You will find that a Google search will throw up endless versions with practical details, and possibly sample calculations.</p>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../energeticsmenu.html#top">To the chemical energetics menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>