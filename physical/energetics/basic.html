<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to Chemical Energetics | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Basic ideas about energy changes during chemical reactions, including simple energy diagrams and the terms exothermic and endothermic.">
<meta name="keywords" content="energy, energetics, diagram, exothermic, endothermic, heat">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to Chemical Energetics</h1>

<div class="text-block">
<p>This page deals with the basic ideas about energy changes during chemical reactions, including simple energy diagrams and the terms exothermic and endothermic.</p>
</div>

<h2>Energy Changes During Chemical Reactions</h2>

<div class="text-block">
<p>Obviously, lots of chemical reactions give out energy as heat. Getting heat by burning a fuel is a simple example, but you will probably have come across lots of others in the lab.</p>
<p>Other reactions need a continuous supply of heat to make them work. Splitting calcium carbonate into calcium oxide and carbon dioxide is a simple example of this.</p>
<p>Any chemical reaction will involve breaking some bonds and making new ones. Energy is needed to break bonds, and is given out when the new bonds are formed. It is very unlikely that these two processes will involve exactly the same amount of energy – and so some energy will either be absorbed or released during a reaction. You will find this discussed in more detail in the page about <a href="bondenthalpies.html">bond enthalpies</a>.</p>
</div>

<h3>Simple Energy Diagrams</h3>

<div class="text-block">
<p>A reaction in which heat energy is given off is said to be exothermic.</p>
<p>A reaction in which heat energy is absorbed is said to be endothermic.</p>
<p>You can show this on simple energy diagrams.</p>
<p>For an exothermic change:</p>
</div>

<div class="image-container"><img src="exodiagram.gif"></div>

<div class="text-block">
<p>Notice that in an exothermic change, the products have a lower energy than the reactants. The energy that the system loses is given out as heat. The surroundings warm up.</p>
<p>For an endothermic change:</p>
</div>

<div class="image-container"><img src="endodiagram.gif"></div>

<div class="text-block">
<p>This time the products have a higher energy than the reactants. The system absorbs this extra energy as heat from the surroundings.</p>
</div>

<h3>Expressing Exothermic and Endothermic Changes in Numbers</h3>

<div class="text-block">
<p>Here is an exothermic reaction, showing the amount of heat evolved:</p>
</div>

<div class="block-formula">
\begin{matrix}
\text{C} + \text{O}_2 \longrightarrow \text{CO}_2 \\ \Delta H = {-}394 \text{ kJ mol}^{-1}
\end{matrix}
</div>

<div class="text-block">
<p>This shows that 394 kJ of heat energy are evolved when equation quantities of carbon and oxygen combine to give carbon dioxide. The mol<sup>-1</sup> (per mole) refers to the whole equation in mole quantities.</p>
<p>How do you know that heat is evolved? That is shown by the negative sign.</p>
<p>You always think of the energy change during a reaction from the point of view of the reactants. The reactants (carbon and oxygen) have lost energy during the reaction. When you burn carbon in oxygen, that is the energy which is causing the surroundings to get hotter.</p>
<p>And here is an endothermic change:</p>
</div>

<div class="block-formula">
\begin{matrix}
\text{CaCO}_3 \longrightarrow \text{CaO} + \text{CO}_2 \\ \Delta H = {+}178 \text{ kJ mol}^{-1}
\end{matrix}
</div>

<div class="text-block">
<p>In this case, 178 kJ of heat are absorbed when 1 mole of calcium carbonate reacts to give 1 mole of calcium oxide and 1 mole of carbon dioxide.</p>
<p>You can tell that energy is being absorbed because of the plus sign. A simple energy diagram for the reaction looks like this:</p>
</div>

<div class="image-container"><img src="endodiagram2.gif"></div>

<div class="text-block">
<p>The products have a higher energy than the reactants. Energy has been gained by the system – hence the plus sign.</p>
<p>Whenever you write values for any energy change, you must always write a plus or a minus sign in front of it. If you have an endothermic change, and write, say, 178 kJ mol<sup>-1</sup> instead of +178 kJ mol<sup>-1</sup>, you risk losing a mark in an exam.</p>
</div>

<h3>Energetic Stability</h3>

<div class="text-block">
<p>You are likely to come across statements that say that something is energetically more stable than something else. For example, in the next page in this section you will find that I have said that oxygen, O<sub>2</sub>, is more energetically stable than ozone, O<sub>3</sub>. What does this mean?</p>
<p>If you plot the positions of oxygen and ozone on an energy diagram, it looks like this:</p>
</div>

<div class="image-container"><img src="relativestab.gif"></div>

<div class="text-block">
<p>The lower down the energy diagram something is, the more energetically stable it is. If ozone converted into ordinary oxygen, heat energy would be released, and the oxygen would be in a more energetically stable form than it was before.</p>
<p>So why doesn't ozone immediately convert into the more energetically stable oxygen?</p>
<p>Similarly, if you mix petrol (gasoline) and air at ordinary temperatures (when you are filling up a car, for example), why doesn't it immediately convert into carbon dioxide and water? It would be much more energetically stable if it turned into carbon dioxide and water – you can tell that, because lots of heat is given out when petrol burns in air. But there is no reaction when you mix the two.</p>
<p>For any reaction to happen, bonds have to be broken, and new ones made. Breaking bonds takes energy. There is a minimum amount of energy needed before a reaction can start – activation energy. If the molecules don't, for example, hit each other with enough energy, then nothing happens. We say that the mixture is kinetically stable, even though it may be energetically unstable with respect to its possible products.</p>
<p>So a petrol and air mixture at ordinary temperatures doesn't react, even though a lot of energy would be released if the reaction took place. Petrol and air are energetically unstable with respect to carbon dioxide and water – they are much higher up the energy diagram. But a petrol and air mixture is kinetically stable at ordinary temperatures, because the activation energy barrier is too high.</p>
<p>If you expose the mixture to a flame or a spark, then you get a major fire or explosion. The initial flame supplies activation energy. The heat given out by the molecules that react first is more than enough to supply the activation energy for the next molecules to react – and so on.</p>
<p>The moral of all this is that you should be very careful using the word "stable" in chemistry!</p>
</div>

<div class="note">
<p>Note: You will find a bit more about activation energy on the introductory page about <a href="../basicrates/introduction.html">rates of reaction</a>.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-energeticsintro.pdf" target="_blank">Questions on the introduction to chemical energetics</a>
<a href="../questions/a-energeticsintro.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../energeticsmenu.html#top">To the chemical energetics menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>