<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Enthalpies of Solution and Hydration | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="This page introduces lattice enthalpies (lattice energies) and Born-Haber cycles">
<meta name="keywords" content="lattice, enthalpy, energy, cycle, solution, hydration">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Enthalpies of Solution and Hydration</h1>

<div class="text-block">
<p>This page looks at the relationship between enthalpies of solution, hydration enthalpies and lattice enthalpies.</p>
</div>

<div class="note">
<p>Note: You really ought to have read the pages about <a href="sums.html">Hess's Law cycles</a> and <a href="lattice.html">lattice enthalpies</a> before you continue with this page.</p>
</div>

<h2>Enthalpy Change of Solution</h2>

<h3>Defining Enthalpy Change of Solution</h3>

<div class="definition">
<p>Enthalpy change of solution: the enthalpy change when 1 mole of an ionic substance dissolves in water to give a solution of infinite dilution.</p>
</div>

<div class="text-block">
<p>Enthalpies of solution may be either positive or negative – in other words, some ionic substances dissolved endothermically (for example, NaCl); others dissolve exothermically (for example NaOH).</p>
<p>An infinitely dilute solution is one where there is a sufficiently large excess of water that adding any more doesn't cause any further heat to be absorbed or evolved.</p>
<p>So, when 1 mole of sodium chloride crystals are dissolved in an excess of water, the enthalpy change of solution is found to be +3.9 kJ mol<sup>-1</sup>. The change is slightly endothermic, and so the temperature of the solution will be slightly lower than that of the original water.</p>
</div>

<h3>Thinking About Dissolving as an Energy Cycle</h3>

<div class="text-block">
<p>Why is heat sometimes evolved and sometimes absorbed when a substance dissolves in water? To answer that it is useful to think about the various enthalpy changes that are involved in the process.</p>
<p>You can think of an imaginary process where the crystal lattice is first broken up into its separate gaseous ions, and then those ions have water molecules wrapped around them. That is how they exist in the final solution.</p>
<p>The heat energy needed to break up 1 mole of the crystal lattice is the lattice dissociation enthalpy.</p>
<p>The heat energy released when new bonds are made between the ions and water molecules is known as the hydration enthalpy of the ion.</p>
</div>

<div class="definition">
<p>Hydration enthalpy: the enthalpy change when 1 mole of gaseous ions dissolve in sufficient water to give an infinitely dilute solution. Hydration enthalpies are always negative.</p>
</div>

<h4>Factors affecting the size of hydration enthalpy</h4>

<div class="text-block">
<p>Hydration enthalpy is a measure of the energy released when attractions are set up between positive or negative ions and water molecules.</p>
<p>With positive ions, there may only be loose attractions between the slightly negative oxygen atoms in the water molecules and the positive ions, or there may be formal dative covalent (co-ordinate covalent) bonds.</p>
<p>With negative ions, hydrogen bonds are formed between lone pairs of electrons on the negative ions and the slightly positive hydrogens in water molecules.</p>
</div>

<div class="note">
<p>Note: You will find the attractions between water molecules and positive ions discussed on the page about <a href="../../atoms/bonding/dative.html">dative covalent bonding</a>. You will find the attractions between negative ions and water molecules discussed on the page about <a href="../../atoms/bonding/hbond.html">hydrogen bonding</a>.</p>
</div>

<div class="text-block">
<p>The size of the hydration enthalpy is governed by the amount of attraction between the ions and the water molecules.</p>
</div>

<ul>
<li>The attractions are stronger the smaller the ion. For example, hydration enthalpies fall as you go down a group in the Periodic Table. The small lithium ion has by far the highest hydration enthalpy in Group1, and the small fluoride ion has by far the highest hydration enthalpy in Group 7. In both groups, hydration enthalpy falls as the ions get bigger.</li>
<li>The attractions are stronger the more highly charged the ion. For example, the hydration enthalpies of Group 2 ions (like Mg<sup>2+</sup>) are much higher than those of Group 1 ions (like Na<sup>+</sup>).</li>
</ul>

<h4>Estimating enthalpies of solution from lattice enthalpies and hydration enthalpies</h4>

<div class="text-block">
<p>The hydration enthalpies for calcium and chloride ions are given by the equations:</p>
</div>

<div class="block-formula">
\begin{matrix}
\text{Ca}^{2+}_{(g)} \xrightarrow{\text{H}_2\text{O}} \text{Ca}^{2+}_{(aq)} & \Delta H = {-}1650 \text{ kJ mol}^{-1} \\
\\
\text{Cl}^-_{(g)} \xrightarrow{\text{H}_2\text{O}} \text{Cl}^-_{(aq)} & \Delta H = {-}364 \text{ kJ mol}^{-1}
\end{matrix}
</div>

<div class="text-block">
<p>The following cycle is for calcium chloride, and includes a lattice dissociation enthalpy of +2258 kJ mol<sup>-1</sup>.</p>
<p>We have to use double the hydration enthalpy of the chloride ion because we are hydrating 2 moles of chloride ions. Make sure you understand exactly how the cycle works.</p>
</div>

<div class="image-container"><img src="dhsolcacl2.gif"></div>

<h5>So</h5>

<div class="block-formula">
\begin{aligned}
\Delta H_{sol} &= 2258 - 1650 + (2 \times {-}364) \\
{} &= {-}120 \text{ kJ mol}^{-1}
\end{aligned}
</div>

<div class="text-block">
<p>Whether an enthalpy of solution turns out to be negative or positive depends on the relative sizes of the lattice enthalpy and the hydration enthalpies. In this particular case, the negative hydration enthalpies more than made up for the positive lattice dissociation enthalpy.</p>
</div>

<div class="note">
<p>Important: This diagram is basically just to show you how to do these calculations, but I have no confidence whatsoever in the accuracy of the data I have used. The measured value for the enthalpy of solution for anhydrous calcium chloride (the value which we are trying to calculate here) is about -80 kJ mol<sup>-1</sup>. That bears little relationship to the value calculated here!</p>
<p>I have no idea what the source of this discrepancy is. One or more of the figures I am using is obviously inaccurate. Trying to find reliable values for energy terms like lattice enthalpies or hydration enthalpies has been a total nightmare throughout the whole of this energetics section. Virtually every textbook I have available (and I have quite a few!) gives different values. Virtually every textbook that you can access via Google Books has different values. Virtually every website that you look at seems to have its own combination of values which may or may not agree with any of the books.</p>
<p>Despite the fact that I am now totally fed up with this whole topic, it shouldn't affect you as a student or a teacher working towards an exam the equivalent of UK A-level. You have to work with whatever values your examiners give you. What is important is that you understand what you are doing. As long as the answer you come up with is consistent with the data you are given, that is actually all that matters to you.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-energeticssoln.pdf" target="_blank">Questions on solution enthalpies</a>
<a href="../questions/a-energeticssoln.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../energeticsmenu.html#top">To the chemical energetics menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>