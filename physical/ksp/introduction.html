<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to Solubility Products | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="How solubility products are defined together with their units, and their relationship with the solubility of an ionic compound.">
<meta name="keywords" content="solubility, product, ionic, equilibrium">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to Solubility Products</h1>

<div class="text-block">
<p>This page looks at how solubility products are defined, together with their units. It also explores the relationship between the solubility product of an ionic compound and its solubility.</p>
</div>

<h3>What are Solubility Products, K<sub>sp</sub>?</h3>

<h4>Solubility products are equilibrium constants</h4>

<div class="text-block">
<p>Barium sulfate is almost insoluble in water. It isn't totally insoluble – very, very small amounts do dissolve. That's true of any so-called "insoluble" ionic compound.</p>
<p>if you shook some solid barium sulfate with water, a tiny proportion of the barium ions and sulfate ions would break away from the surface of the solid and go into solution. Over time, some of these will return from solution to stick onto the solid again.</p>
<p>You get an equilibrium set up when the rate at which some ions are breaking away is exactly matched by the rate at which others are returning.</p>
</div>

<div class="block-formula">
\text{BaSO}_{4(s)} \xrightleftharpoons{} \text{Ba}^{2+}_{(aq)} + {\text{SO}_4}^{2-}_{(aq)}
</div>

<div class="text-block">
<p>The position of this equilibrium lies very far to the left. The great majority of the barium sulfate is present as solid. In fact, if you shook solid barium sulfate with water you wouldn't be aware just by looking at it that any had dissolved at all.</p>
<p>But it is an equilibrium, and so you can write an equilibrium constant for it which will be constant at a given temperature – like all equilibrium constants.</p>
<p>The equilibrium constant is called the solubility product, and is given the symbol K<sub>sp</sub>.</p>
</div>

<div class="block-formula">
K_{sp} = [\text{Ba}^{2+}_{(aq)}][{\text{SO}_4}^{2-}_{(aq)}]
</div>

<div class="text-block">
<p>To avoid confusing clutter, solubility product expressions are often written without the state symbols. Even if you don't write them, you must be aware that the symbols for the ions that you write are for those in solution in water.</p>
</div>

<div class="block-formula">
K_{sp} = [\text{Ba}^{2+}][{\text{SO}_4}^{2-}]
</div>

<h5>Why doesn't the solid barium sulfate appear in the equilibrium expression?</h5>

<div class="text-block">
<p>For many simple equilibria, the equilibrium constant expression has terms for the right-hand side of the equation divided by terms for the left-hand side. But in this case, there is no term for the concentration of the solid barium sulfate. Why not?</p>
<p>This is a heterogeneous equilibrium – one which contains substances in more than one state. In a heterogeneous equilibrium, concentration terms for solids are left out of the expression.</p>
</div>

<div class="note">
<p>Note: The simplest explanation for this is that the concentration of a solid can be thought of as a constant. Rather than have an expression with two constants in it (the equilibrium constant and the concentration of the solid), the constants are merged to give a single value – the solubility product.</p>
</div>

<h5>Solubility products for more complicated solids</h5>

<div class="text-block">
<p>Here is the corresponding equilibrium for calcium phosphate, Ca<sub>3</sub>(PO<sub>4</sub>)<sub>2</sub>:</p>
</div>

<div class="block-formula">
\text{Ca}_3(\text{PO}_4)_{2(s)} \xrightleftharpoons{} 3\text{Ca}^{2+}_{(aq)} + 2{\text{PO}_4}^{3-}_{(aq)}
</div>

<div class="text-block">
<p>And this is the solubility product expression:</p>
</div>

<div class="block-formula">
K_{sp} = [\text{Ca}^{2+}]^3[{\text{PO}_4}^{3-}]^2
</div>

<div class="text-block">
<p>Just as with any other equilibrium constant, you raise the concentrations to the power of the number in front of them in the equilibrium equation. There's nothing new here.</p>
<p>Solubility products only apply to sparingly soluble ionic compounds</p>
<p>You can't use solubility products for normally soluble compounds like sodium chloride, for example. Interactions between the ions in the solution interfere with the simple equilibrium we are talking about.</p>
</div>

<h3>The Units for Solubility Products</h3>

<div class="text-block">
<p>The units for solubility products differ depending on the solubility product expression, and you need to be able to work them out each time.</p>
</div>

<h4>Working out the units in the barium sulfate case</h4>

<div class="text-block">
<p>Here is the solubility product expression for barium sulfate again:</p>
</div>

<div class="block-formula">
K_{sp} = [\text{Ba}^{2+}][{\text{SO}_4}^{2-}]
</div>

<div class="text-block">
<p>Each concentration has the unit mol dm<sup>-3</sup>. So the units for the solubility product in this case will be:</p>
</div>

<div class="block-formula">
(\text{mol dm}^{-3}) \times (\text{mol dm}^{-3}) = \text{mol}^2 \text{ dm}^{-6}
</div>

<h4>Working out the units in the calcium phosphate case</h4>

<div class="text-block">
<p>Here is the solubility product expression for calcium phosphate again:</p>
</div>

<div class="block-formula">
K_{sp} = [\text{Ca}^{2+}]^3[{\text{PO}_4}^{3-}]^2
</div>

<div class="text-block">
<p>The units this time will be:</p>
</div>

<div class="block-formula">
(\text{mol dm}^{-3})^3 \times (\text{mol dm}^{-3})^2 = \text{mol}^5 \text{ dm}^{-15}
</div>

<div class="text-block">
<p>If you are asked to calculate a solubility product in an exam, there will almost certainly be a mark for the correct units. It isn't very hard – just take care!</p>
</div>

<h3>Solubility Products Apply Only to Saturated Solutions</h3>

<div class="text-block">
<p>Let's look again at the barium sulfate case. Here is the equilibrium expression again:</p>
</div>

<div class="block-formula">
\text{BaSO}_{4(s)} \xrightleftharpoons{} \text{Ba}^{2+}_{(aq)} + {\text{SO}_4}^{2-}_{(aq)}
</div>

<div class="text-block">
<p>and here is the solubility product expression:</p>
</div>

<div class="block-formula">
K_{sp} = [\text{Ba}^{2+}][{\text{SO}_4}^{2-}]
</div>

<div class="text-block">
<p>K<sub>sp</sub> for barium sulfate at 298 K is 1.1 x 10<sup>-10</sup> mol<sup>2</sup> dm<sup>-6</sup>.</p>
<p>In order for this equilibrium constant (the solubility product) to apply, you have to have solid barium sulfate present in a saturated solution of barium sulfate. That's what the equilibrium equation is telling you.</p>
<p>If you have barium ions and sulfate ions in solution in the presence of some solid barium sulfate at 298 K, and multiply the concentrations of the ions together, your answer will be 1.1 x 10<sup>-10</sup> mol<sup>2</sup> dm<sup>-6</sup>.</p>
<p>What if you mixed incredibly dilute solutions containing barium ions and sulfate ions so that the product of the ionic concentrations was less than the solubility product?</p>
<p>All this means is that you haven't got an equilibrium. The reason for that is that there won't be any solid present. If you lower the concentrations of the ions enough, you won't get a precipitate – just a very, very dilute solution of barium sulfate.</p>
<p>So it is possible to get an answer less than the solubility product when you multiply the ionic concentrations together if the solution isn't saturated.</p>
<p>Can you get an answer greater than the solubility product if you multiply the ionic concentrations together (allowing for any powers in the solubility product expression, of course)? No!</p>
<p>The solubility product is a value which you get when the solution is saturated. If there is any solid present, you can't dissolve any more solid than there is in a saturated solution.</p>
</div>

<div class="note">
<p>Note: In the absence of any solid, a few substances produce unstable supersaturated solutions. As soon as you add any solid, or perhaps just scratch the glass to give a rough bit that crystals can form on, all the excess solid precipitates out to leave a normal saturated solution.</p>
</div>

<div class="text-block">
<p>If you mix together two solutions containing barium ions and sulfate ions and the product of the concentrations would exceed the solubility product, you get a precipitate formed. Enough solid is produced to reduce the concentrations of the barium and sulfate ions down to a value which the solubility product allows.</p>
</div>

<div class="summary">
<ul>
<li>The value of a solubility product relates to a saturated solution.</li>
<li>If the ionic concentrations give a value less than the solubility product, the solution isn't saturated. No precipitate would be formed.</li>
<li>If the ionic concentrations give a value more than the solubility product, enough precipitate would be formed to reduce the concentrations to give an answer equal to the solubility product.</li>
</ul>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-spsintro.pdf" target="_blank">Questions on an introduction to solubility products</a>
<a href="../questions/a-spsintro.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../kspmenu.html#top">To the solubility product menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>