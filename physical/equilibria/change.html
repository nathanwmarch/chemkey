<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Equilibrium Constants and Le Châtelier's Principle | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A look at the relationship between equilibrium constants and Le Châtelier's Principle.">
<meta name="keywords" content="equilibrium, constant, position, kc, kp, concentration, pressure, temperature, catalyst">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">
<h1>Equilibrium Constants and Le Châtelier's Principle</h1>

<div class="text-block">
<p>This page looks at the relationship between equilibrium constants and Le Châtelier's Principle. Students often get confused about how it is possible for the position of equilibrium to change as you change the conditions of a reaction, although the equilibrium constant may remain the same.</p>
<p>Be warned that this page assumes a good understanding of Le Châtelier's Principle and how to write expressions for equilibrium constants.</p>
</div>

<div class="note">
<p>Important: If you aren't happy about the basics of equilibrium, explore the <a href="../equilibmenu.html#top">equilibrium menu</a> before you waste your time on this page.</p>
<p>This page should only be read when you are confident about everything else to do with equilibria.</p>
</div>

<h2>Changing Concentrations</h2>

<h3>The Facts</h3>

<div class="text-block">
<p>Equilibrium constants aren't changed if you change the concentrations of things present in the equilibrium. The only thing that changes an equilibrium constant is a change of temperature.</p>
<p>The position of equilibrium is changed if you change the concentration of something present in the mixture. According to Le Châtelier's Principle, the position of equilibrium moves in such a way as to tend to undo the change that you have made.</p>
<p>Suppose you have an equilibrium established between four substances A, B, C and D.</p>
</div>

<div class="block-formula">
\text{A} + 2\text{B} \xrightleftharpoons{} \text{C} + \text{D}
</div>

<div class="text-block">
<p>According to Le Châtelier's Principle, if you decrease the concentration of C, for example, the position of equilibrium will move to the right to increase the concentration again.</p>
</div>

<div class="note">
<p>Note: The reason for choosing an equation with "2B" will become clearer when I deal with the effect of pressure further down the page.</p>
</div>

<h3>Explanation in Terms of the Constancy of the Equilibrium Constant</h3>

<div class="text-block">
<p>The equilibrium constant, K<sub>c</sub> for this reaction looks like this:</p>
</div>

<div class="block-formula">
K_c = \frac{[\text{C}][\text{D}]}{[\text{A}][\text{B}]^2}
</div>

<div class="text-block">
<p>If you have moved the position of the equilibrium to the right (and so increased the amount of C and D), why hasn't the equilibrium constant increased?</p>
<p>This is actually the wrong question to ask! We need to look at it the other way round.</p>
<p>Let's assume that the equilibrium constant mustn't change if you decrease the concentration of C – because equilibrium constants are constant at constant temperature. Why does the position of equilibrium move as it does?</p>
<p>If you decrease the concentration of C, the top of the K<sub>c</sub> expression gets smaller. That would change the value of K<sub>c</sub>. In order for that not to happen, the concentrations of C and D will have to increase again, and those of A and B must decrease. That happens until a new balance is reached when the value of the equilibrium constant expression reverts to what it was before.</p>
<p>The position of equilibrium moves – not because Le Châtelier says it must – but because of the need to keep a constant value for the equilibrium constant.</p>
<p>If you decrease the concentration of C:</p>
</div>

<div class="image-container"><img src="kcconcexpl.gif"></div>

<h2>Changing Pressure</h2>

<div class="text-block">
<p>This only applies to systems involving at least one gas.</p>
</div>

<h3>The Facts</h3>

<div class="text-block">
<p>Equilibrium constants aren't changed if you change the pressure of the system. The only thing that changes an equilibrium constant is a change of temperature.</p>
<p>The position of equilibrium may be changed if you change the pressure. According to Le Châtelier's Principle, the position of equilibrium moves in such a way as to tend to undo the change that you have made.</p>
<p>That means that if you increase the pressure, the position of equilibrium will move in such a way as to decrease the pressure again – if that is possible. It can do this by favouring the reaction which produces the fewer molecules. If there are the same number of molecules on each side of the equation, then a change of pressure makes no difference to the position of equilibrium.</p>
</div>

<h3>Explanation</h3>

<h4>Where there are different numbers of molecules on each side of the equation</h4>

<div class="text-block">
<p>Let's look at the same equilibrium we've used before. This one would be affected by pressure because there are 3 molecules on the left but only 2 on the right. An increase in pressure would move the position of equilibrium to the right.</p>
</div>

<div class="block-formula">
\text{A}_{(g)} + 2\text{B}_{(g)} \xrightleftharpoons{} \text{C}_{(g)} + \text{D}_{(g)}
</div>

<div class="text-block">
<p>Because this is an all-gas equilibriium, it is much easier to use K<sub>p</sub>:</p>
</div>

<div class="block-formula">
K_p = \frac{P_C \times P_D}{P_A \times {P_B}^2}
</div>

<div class="text-block">
<p>Once again, it is easy to suppose that, because the position of equilibrium will move to the right if you increase the pressure, K<sub>p</sub> will increase as well. Not so!</p>
<p>To understand why, you need to modify the K<sub>p</sub> expression.</p>
<p>Remember the relationship between partial pressure, mole fraction and total pressure?</p>
</div>

<div class="block-formula">
\begin{aligned}
P_A &= \text{mole fraction of A} \times \text{total pressure} \\
P_A &= x_A \times P
\end{aligned}
</div>

<div class="note">
<p>Note: If you aren't happy with this, read the beginning of the page about <a href="kp.html#top">K<sub>p</sub></a> before you go on.</p>
</div>

<div class="text-block">
<p>Replacing all the partial pressure terms by mole fractions and total pressure gives you this:</p>
</div>

<div class="block-formula">
K_p = \frac{(x_C \times P) \times (x_D \times P)}
{(x_A \times P) \times (x_B \times P)^2}
</div>

<div class="text-block">
<p>If you sort this out, most of the "P"s cancel out – but one is left at the bottom of the expression.</p>
</div>

<div class="block-formula">
K_p = \frac{x_C \times x_D}
{x_A \times {x_B}^2 \times P}
</div>

<div class="text-block">
<p>Now, remember that K<sub>p</sub> has got to stay constant because the temperature is unchanged. How can that happen if you increase P?</p>
<p>To compensate, you would have to increase the terms on the top, x<sub>C</sub> and x<sub>D</sub>, and decrease the terms on the bottom, x<sub>A</sub> and x<sub>B</sub>.</p>
<p>Increasing the terms on the top means that you have increased the mole fractions of the molecules on the right-hand side. Decreasing the terms on the bottom means that you have decreased the mole fractions of the molecules on the left.</p>
<p>That is another way of saying that the position of equilibrium has moved to the right – exactly what Le Châtelier's Principle predicts. The position of equilibrium moves so that the value of K<sub>p</sub> is kept constant.</p>
</div>

<h4>Where there are the same numbers of molecules on each side of the equation</h4>

<div class="text-block">
<p>In this case, the position of equilibrium isn't affected by a change of pressure. Why not?</p>
</div>

<div class="block-formula">
\text{A}_{(g)} + \text{B}_{(g)} \xrightleftharpoons{} \text{C}_{(g)} + \text{D}_{(g)}
</div>

<div class="text-block">
<p>Let's go through the same process as before:</p>
</div>

<div class="block-formula">
K_p = \frac{P_C \times P_D}{P_A \times P_B}
</div>

<div class="text-block">
<p>Substituting mole fractions and total pressure:</p>
</div>

<div class="block-formula">
K_p = \frac{(x_C \times P) \times (x_D \times P)}
{(x_A \times P) \times (x_B \times P)}
</div>

<div class="text-block">
<p> and cancelling out as far as possible:</p>
</div>

<div class="block-formula">
K_p = \frac{x_C \times x_D}
{x_A \times x_B}
</div>

<div class="text-block">
<p>There isn't a single "P" left in the expression. Changing the pressure can't make any difference to the K<sub>p</sub> expression. The position of equilibrium doesn't need to move to keep K<sub>p</sub> constant.</p>
</div>

<h2>Changing Temperature</h2>

<h3>The Facts</h3>

<div class="text-block">
<p>Equilibrium constants are changed if you change the temperature of the system. K<sub>c</sub> or K<sub>p</sub> are constant at constant temperature, but they vary as the temperature changes. </p>
<p>Look at the equilibrium involving hydrogen, iodine and hydrogen iodide:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{H}_{2(g)} + \text{I}_{2(g)} \xrightleftharpoons{} 2\text{HI}_{(g)} \\
\Delta H = {-}10.4 \text{kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>The K<sub>p</sub> expression is:</p>
</div>

<div class="block-formula">
K_p = \frac{{{P_{HI}}^2}}{P_{H_2} \times P_{I_2}}
</div>

<div class="text-block">
<p>Two values for K<sub>p</sub> are:</p>
</div>

<table class="data-table two-right">
<tbody><tr><th>temperature</th><th>K<sub>p</sub></th></tr>
<tr><td>500 K</td><td>160</td></tr>
<tr><td>700 K</td><td>54</td></tr>
</tbody></table>

<div class="text-block">
<p>You can see that as the temperature increases, the value of K<sub>p</sub> falls.</p>
</div>

<div class="note">
<p>Note: You might possibly be wondering what the units of K<sub>p</sub> are. This particular example was chosen because in this case, K<sub>p</sub> doesn't have any units. It is just a number.</p>
<p>The units for equilibrium constants vary from case to case. It is much easier to understand this from a book than from a lot of maths on screen. You will find this explained in my <a href="../../book.html#top">chemistry calculations book</a>.</p>
</div>

<div class="text-block">
<p>This is typical of what happens with any equilibrium where the forward reaction is exothermic. Increasing the temperature decreases the value of the equilibrium constant.</p>
<p>Where the forward reaction is endothermic, increasing the temperature increases the value of the equilibrium constant.</p>
</div>

<div class="note">
<p>Note: Any explanation for this needs knowledge beyond the scope of any UK A-level (or equivalent) syllabus.</p>
</div>

<div class="text-block">
<p>The position of equilibrium also changes if you change the temperature. According to Le Châtelier's Principle, the position of equilibrium moves in such a way as to tend to undo the change that you have made.</p>
<p>If you increase the temperature, the position of equilibrium will move in such a way as to reduce the temperature again. It will do that by favouring the reaction which absorbs heat.</p>
<p>In the equilibrium we've just looked at, that will be the back reaction because the forward reaction is exothermic.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{H}_{2(g)} + \text{I}_{2(g)} \xrightleftharpoons{} 2\text{HI}_{(g)} \\
\Delta H = {-}10.4 \text{kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>So, according to Le Châtelier's Principle the position of equilibrium will move to the left. Less hydrogen iodide will be formed, and the equilibrium mixture will contain more unreacted hydrogen and iodine.</p>
<p>That is entirely consistent with a fall in the value of the equilibrium constant.</p>
</div>

<div class="image-container"><img src="kphi2.gif"></div>

<h2>Adding a Catalyst</h2>

<h3>The Facts</h3>

<div class="text-block">
<p>Equilibrium constants aren't changed if you add (or change) a catalyst. The only thing that changes an equilibrium constant is a change of temperature.</p>
<p>The position of equilibrium is not changed if you add (or change) a catalyst.</p>
</div>

<h3>Explanation</h3>

<div class="text-block">
<p>A catalyst speeds up both the forward and back reactions by exactly the same amount. Dynamic equilibrium is established when the rates of the forward and back reactions become equal. If a catalyst speeds up both reactions to the same extent, then they will remain equal without any need for a shift in position of equilibrium.</p>
</div>

<div class="note">
<p>Note: If you know about the <a href="../basicrates/arrhenius.html#top">Arrhenius equation</a>, it isn't too difficult to use it to show that the ratio of the rate constants for the forward and back reactions isn't affected by adding a catalyst. Although the activation energies for the two reactions change when you add a catalyst, they both change by the same amount.</p>
<p>I'm not going to do this bit of algebra, because it would never be asked at this level (UK A-level or equivalent).</p>
</div>

<h2>Exploring Some of This Using a Simple Computer Program</h2>

<div class="text-block">
<p>The link below will take you to a page where you can explore the effect of changing conditions on the reaction:</p>
</div>

<div class="block-formula">
\text{H}_2\text{O}_{(g)} + \text{C}_{(s)} \xrightleftharpoons{} \text{H}_{2(g)} + \text{CO}_{(g)}
</div>

<div class="text-block">
<p>The page comes from <a href="http://www.chm.davidson.edu/java/lechatelier/lechatelier.html">Davidson College</a> in America. It needs you to have Java enabled in your browser.</p>
</div>

<div class="note">
<p>Note: If this link stops working, please let me know using the address on the <a href="../../about.html">about this site</a> page. If your browser doesn't have Java enabled, then you won't see the important part of the page, and you will have to enable Java. I'm afraid that is your problem – it varies from browser to browser. You could try reading this page about <a href="http://www.java.com/en/download/help/enable_browser.xml">enabling Java</a>.</p>
</div>

<div class="text-block">
<p>You are told that the reaction is endothermic, and can change things like the temperature, the volume of the mixture, and the amounts of all the reactants to see what would happen.</p>
<p>It would be best if you worked out what you expected to happen before you change anything. You change things by moving the grey sliders.</p>
<p>You will notice that there is no direct way of changing the pressure. Instead, you have to change the volume. Obviously, if you decrease the volume, keeping the amounts of everything constant, that increases the pressure.</p>
<p>If you do this to change the pressure, concentrate on the red bars showing what happens to the number of moles of substances present. The blue bars are more confusing. These represent concentrations, and these will change not only because of the change in quantities present, but also because of the change in volume. That's confusing!</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-equilchanges.pdf" target="_blank">Questions on equilibrium constants and Le Châtelier's Principle</a>
<a href="../questions/a-equilchanges.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../equilibmenu.html#top">To the equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>