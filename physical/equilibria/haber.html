<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Haber Process for the Manufacture of Ammonia | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A description of the Haber Process and an explanation of the conditions used in terms of the position of equilibrium, the rate of the reaction and the economics of the process.">
<meta name="keywords" content="equilibrium, equilibria, chemical, position of equilibrium, le chatelier, principle, concentration, temperature, pressure, catalyst, haber process, ammonia, manufacture">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Haber Process for the Manufacture of Ammonia</h1>

<div class="text-block">
<p>This page describes the Haber Process for the manufacture of ammonia from nitrogen and hydrogen, and then goes on to explain the reasons for the conditions used in the process. It looks at the effect of temperature, pressure and catalyst on the composition of the equilibrium mixture, the rate of the reaction and the economics of the process.</p>
</div>

<div class="note">
<p>Important: If you aren't sure about using <a href="lechatelier.html#top">Le Chatelier's Principle</a> or about the effect of changing conditions on <a href="../basicratesmenu.html#top">rates of reaction</a> you should explore these links before you go on.</p>
<p>When you are reading this page, if you find that you aren't understanding the effect of changing one of the conditions on the position of equilibrium or on the rate of the reaction, come back and follow up these links.</p>
</div>

<h2>A Brief Summary of the Haber Process</h2>

<div class="text-block">
<p>The Haber Process combines nitrogen from the air with hydrogen derived mainly from natural gas (methane) into ammonia. The reaction is reversible and the production of ammonia is exothermic.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{N}_{2(g)} + 3\text{H}_{2(g)} \xrightleftharpoons{} 2\text{NH}_{3(g)} \\
\Delta H = {-}92 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>A flow scheme for the Haber Process looks like this:</p>
</div>

<div class="image-container"><img src="haberflow.gif"></div>

<h3>Some Notes on the Conditions</h3>

<h4>The catalyst</h4>

<div class="text-block">
<p>The catalyst is actually slightly more complicated than pure iron. It has potassium hydroxide added to it as a promoter – a substance that increases its efficiency.</p>
</div>

<h4>The pressure</h4>

<div class="text-block">
<p>The pressure varies from one manufacturing plant to another, but is always high. You can't go far wrong in an exam quoting 200 atmospheres.</p>
</div>

<h4>Recycling</h4>

<div class="text-block">
<p>At each pass of the gases through the reactor, only about 15% of the nitrogen and hydrogen converts to ammonia. (This figure also varies from plant to plant.) By continual recycling of the unreacted nitrogen and hydrogen, the overall conversion is about 98%.</p>
</div>

<h2>Explaining the Conditions</h2>

<h3>The Proportions of Nitrogen and Hydrogen</h3>

<div class="text-block">
<p>The mixture of nitrogen and hydrogen going into the reactor is in the ratio of 1 volume of nitrogen to 3 volumes of hydrogen.</p>
<p>Avogadro's Law says that equal volumes of gases at the same temperature and pressure contain equal numbers of molecules. That means that the gases are going into the reactor in the ratio of 1 molecule of nitrogen to 3 of hydrogen.</p>
<p>That is the proportion demanded by the equation.</p>
<p>In some reactions you might choose to use an excess of one of the reactants. You would do this if it is particularly important to use up as much as possible of the other reactant – if, for example, it was much more expensive. That doesn't apply in this case.</p>
<p>There is always a down-side to using anything other than the equation proportions. If you have an excess of one reactant there will be molecules passing through the reactor which can't possibly react because there isn't anything for them to react with. This wastes reactor space – particularly space on the surface of the catalyst.</p>
</div>

<h3>The Temperature</h3>

<h4>Equilibrium considerations</h4>

<div class="text-block">
<p>You need to shift the position of the equilibrium as far as possible to the right in order to produce the maximum possible amount of ammonia in the equilibrium mixture.</p>
<p>The forward reaction (the production of ammonia) is exothermic.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{N}_{2(g)} + 3\text{H}_{2(g)} \xrightleftharpoons{} 2\text{NH}_{3(g)} \\
\Delta H = {-}92 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>According to Le Chatelier's Principle, this will be favoured if you lower the temperature. The system will respond by moving the position of equilibrium to counteract this – in other words by producing more heat.</p>
<p>In order to get as much ammonia as possible in the equilibrium mixture, you need as low a temperature as possible. However, 400 – 450°C isn't a low temperature!</p>
</div>

<h4>Rate considerations</h4>

<div class="text-block">
<p>The lower the temperature you use, the slower the reaction becomes. A manufacturer is trying to produce as much ammonia as possible per day. It makes no sense to try to achieve an equilibrium mixture which contains a very high proportion of ammonia if it takes several years for the reaction to reach that equilibrium.</p>
<p>You need the gases to reach equilibrium within the very short time that they will be in contact with the catalyst in the reactor.</p>
</div>

<h4>The compromise</h4>

<div class="text-block">
<p>400 – 450°C is a compromise temperature producing a reasonably high proportion of ammonia in the equilibrium mixture (even if it is only 15%), but in a very short time.</p>
</div>

<h3>The Pressure</h3>

<h4>Equilibrium considerations</h4>

<div class="block-formula">
\begin{gathered}
\text{N}_{2(g)} + 3\text{H}_{2(g)} \xrightleftharpoons{} 2\text{NH}_{3(g)} \\
\Delta H = {-}92 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>Notice that there are 4 molecules on the left-hand side of the equation, but only 2 on the right.</p>
<p>According to Le Chatelier's Principle, if you increase the pressure the system will respond by favouring the reaction which produces fewer molecules. That will cause the pressure to fall again.</p>
<p>In order to get as much ammonia as possible in the equilibrium mixture, you need as high a pressure as possible. 200 atmospheres is a high pressure, but not amazingly high.</p>
</div>

<h4>Rate considerations</h4>

<div class="text-block">
<p>Increasing the pressure brings the molecules closer together. In this particular instance, it will increase their chances of hitting and sticking to the surface of the catalyst where they can react. The higher the pressure the better in terms of the rate of a gas reaction.</p>
</div>

<h4>Economic considerations</h4>

<div class="text-block">
<p>Very high pressures are very expensive to produce on two counts.</p>
<p>You have to build extremely strong pipes and containment vessels to withstand the very high pressure. That increases your capital costs when the plant is built.</p>
<p>High pressures cost a lot to produce and maintain. That means that the running costs of your plant are very high.</p>
</div>

<h4>The compromise</h4>

<div class="text-block">
<p>200 atmospheres is a compromise pressure chosen on economic grounds. If the pressure used is too high, the cost of generating it exceeds the price you can get for the extra ammonia produced.</p>
</div>

<h3>The Catalyst</h3>

<h4>Equilibrium considerations</h4>

<div class="text-block">
<p>The catalyst has no effect whatsoever on the position of the equilibrium. Adding a catalyst doesn't produce any greater percentage of ammonia in the equilibrium mixture. Its only function is to speed up the reaction.</p>
</div>

<h4>Rate considerations</h4>

<div class="text-block">
<p>In the absence of a catalyst the reaction is so slow that virtually no reaction happens in any sensible time. The catalyst ensures that the reaction is fast enough for a dynamic equilibrium to be set up within the very short time that the gases are actually in the reactor.</p>
</div>

<h3>Separating the Ammonia</h3>

<div class="text-block">
<p>When the gases leave the reactor they are hot and at a very high pressure. Ammonia is easily liquefied under pressure as long as it isn't too hot, and so the temperature of the mixture is lowered enough for the ammonia to turn to a liquid. The nitrogen and hydrogen remain as gases even under these high pressures, and can be recycled.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-haber.pdf" target="_blank">Questions on the Haber Process</a>
<a href="../questions/a-haber.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../equilibmenu.html#top">To the equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>