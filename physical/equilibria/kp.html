<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Equilibrium Constants – Kp | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An introduction to the use of equilibrium constants expressed in terms of partial pressures.">
<meta name="keywords" content="equilibrium, constant, pressure, kp, partial pressure, mole fraction">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Equilibrium Constants – K<sub>p</sub></h1>

<div class="text-block">
<p>This page explains equilibrium constants expressed in terms of partial pressures of gases, K<sub>p</sub>. It covers an explanation of the terms mole fraction and partial pressure, and looks at K<sub>p</sub> for both homogeneous and heterogeneous reactions involving gases.</p>
<p>The page assumes that you are already familiar with the concept of an equilibrium constant, and that you know about K<sub>c</sub> – an equilibrium constant expressed in terms of concentrations</p>
</div>

<div class="note">
<p>Important: If you have come directly to this page via a search engine, you should first read the page on <a href="kc.html#top">equilibrium constants – K<sub>c</sub></a> before you go on – unless you are already fully confident about how to write expressions for K<sub>c</sub>.</p>
<p>You will find a link back to this page at the bottom of the K<sub>c</sub> page.</p>
</div>

<h2>Defining Some Terms</h2>

<div class="text-block">
<p>Before we can go any further, there are two terms relating to mixtures of gases that you need to be familiar with.</p>
</div>

<h3>Mole Fraction</h3>

<div class="text-block">
<p>If you have a mixture of gases (A, B, C, etc), then the mole fraction of gas A is worked out by dividing the number of moles of A by the total number of moles of gas.</p>
<p>The mole fraction of gas A is often given the symbol x<sub>A</sub>. The mole fraction of gas B would be x<sub>B</sub> – and so on.</p>
</div>

<div class="block-formula">
x_A = \frac{\text{number of moles of gas A}}{\text{total number of moles of gas}}
</div>

<div class="text-block">
<p>Pretty obvious really!</p>
<p>For example, in a mixture of 1 mole of nitrogen and 3 moles of hydrogen, there are a total of 4 moles of gas. The mole fraction of nitrogen is 1/4 (0.25) and of hydrogen is 3/4 (0.75).</p>
</div>

<h3>Partial Pressure</h3>

<div class="text-block">
<p>The partial pressure of one of the gases in a mixture is the pressure which it would exert if it alone occupied the whole container.</p>
<p>The partial pressure of gas A is often given the symbol P<sub>A</sub>. The partial pressure of gas B would be P<sub>B</sub> – and so on.</p>
<p>There are two important relationships involving partial pressures. The first is again fairly obvious.</p>
<p>The total pressure of a mixture of gases is equal to the sum of the partial pressures.</p>
</div>

<div class="block-formula">
\text{total pressure }(P) = P_A + P_B + P_C + ...
</div>

<div class="text-block">
<p>It is easy to see this visually:</p>
</div>

<div class="image-container"><img src="ppvisual.gif"></div>

<div class="text-block">
<p>Gas A is creating a pressure (its partial pressure) when its molecules hit the walls of its container. Gas B does the same. When you mix them up, they just go on doing what they were doing before. The total pressure is due to both molecules hitting the walls – in other words, the sum of the partial pressures.</p>
<p>The more important relationship is the second one:</p>
</div>

<div class="block-formula">
\begin{aligned}
P_A &= \text{mole fraction of A} \times \text{total pressure} \\
{} &= x_A \times P
\end{aligned}
</div>

<div class="text-block">
<p>Learn it!</p>
<p>That means that if you had a mixture made up of 20 moles of nitrogen, 60 moles of hydrogen and 20 moles of ammonia (a total of 100 moles of gases) at 200 atmospheres pressure, the partial pressures would be calculated like this:</p>
</div>

<table class="data-table">
<tbody><tr><th>gas</th><th>mole fraction</th><th>partial pressure</th></tr>
<tr><td>nitrogen</td><td><span class="inline-formula">20 / 100 = 0.2</td><td>0.2 ✖ 200 = 40 atm</td></tr>
<tr><td>hydrogen</td><td>60 / 100 = 0.6</td><td>0.6 ✖ 200 = 120 atm</td></tr>
<tr><td>ammonia</td><td>20 / 100 = 0.2</td><td>0.2 ✖ 200 = 40 atm</td></tr>
</tbody></table>

<div class="text-block">
<p>Partial pressures can be quoted in any normal pressure units. The common ones are atmospheres or pascals (Pa). Pascals are exactly the same as N m<sup>-2</sup> (newtons per square metre).</p>
</div>

<h2>K<sub>p</sub> in Homogeneous Gaseous Equilibria</h2>

<div class="text-block">
<p>A homogeneous equilibrium is one in which everything in the equilibrium mixture is present in the same phase. In this case, to use K<sub>p</sub>, everything must be a gas.</p>
<p>A good example of a gaseous homogeneous equilibrium is the conversion of sulfur dioxide to sulfur trioxide at the heart of the Contact Process:</p>
</div>

<div class="block-formula">
2\text{SO}_{2(g)} + \text{O}_{2(g)} \xrightleftharpoons{} 2\text{SO}_{3(g)}
</div>

<h3>Writing an Expression for K<sub>p</sub></h3>

<div class="text-block">
<p>We are going to start by looking at a general case with the equation:</p>
</div>

<div class="block-formula">
a\text{A}_{(g)} + b\text{B}_{(g)} \xrightleftharpoons{} c\text{C}_{(g)} + d\text{D}_{(g)}
</div>

<div class="text-block">
<p>If you allow this reaction to reach equilibrium and then measure (or work out) the equilibrium partial pressures of everything, you can combine these into the equilibrium constant, K<sub>p</sub>.</p>
<p>Just like K<sub>c</sub>, K<sub>p</sub> always has the same value (provided you don't change the temperature), irrespective of the amounts of A, B, C and D you started with.</p>
</div>

<div class="block-formula">
K_p = \frac{{P_C}^c \times {P_D}^d}{{P_A}^a \times {P_B}^b}
</div>

<div class="text-block">
<p>K<sub>p</sub> has exactly the same format as K<sub>c</sub>, except that partial pressures are used instead of concentrations. The gases on the right-hand side of the chemical equation are at the top of the expression, and those on the left at the bottom.</p>
</div>

<div class="note">
<p>Beware! People are sometimes tempted to write brackets around the individual partial pressure terms. Don't do it! Even if you intend to write normal round brackets, it is too easy in an exam to write them as square brackets instead. This makes it look as if you are confusing K<sub>p</sub> with K<sub>c</sub>. Examiners don't like it, and you could be penalised.</p>
</div>

<h4>The Contact Process equilibrium</h4>

<div class="text-block">
<p>You will remember that the equation for this is:</p>
</div>

<div class="block-formula">
2\text{SO}_{2(g)} + \text{O}_{2(g)} \xrightleftharpoons{} 2\text{SO}_{3(g)}
</div>

<div class="text-block">
<p>K<sub>p</sub> is given by:</p>
</div>

<div class="block-formula">
K_p = \frac{{P_{{SO}_3}}^2}{{P_{{SO}_2}}^2 \times P_{{O}_2}}
</div>

<h4>The Haber Process equilibrium</h4>

<div class="text-block">
<p>The equation for this is:</p>
</div>

<div class="block-formula">
\text{N}_{2(g)} + 3\text{H}_{2(g)} \xrightleftharpoons{} 2\text{NH}_{3(g)}
</div>

<div class="text-block">
<p> and the K<sub>p</sub> expression is:</p>
</div>

<div class="block-formula">
K_p = \frac{{P_{{NH}_3}}^2}{P_{{N}_2} \times {P_{{H}_2}}^2}
</div>

<h2>K<sub>p</sub> in Heterogeneous Equilibria</h2>

<div class="text-block">
<p>A typical example of a heterogeneous equilibrium will involve gases in contact with solids.</p>
</div>

<h3>Writing an Expression for K<sub>p</sub> for a Heterogeneous Equilibrium</h3>

<div class="text-block">
<p>Exactly as happens with K<sub>c</sub>, you don't include any term for a solid in the equilibrium expression.</p>
<p>The next two examples have already appeared on the K<sub>c</sub> page.</p>
</div>

<h4>The equilibrium produced on heating carbon with steam</h4>

<div class="block-formula">
\text{H}_2{O}_{(g)} + \text{C}_{(s)} \xrightleftharpoons{} \text{H}_{2(g)} + \text{CO}_{(g)}
</div>

<div class="text-block">
<p>Everything is exactly the same as before in the expression for K<sub>p</sub>, except that you leave out the solid carbon.</p>
</div>

<div class="block-formula">
K_p = \frac{P_{H_2} \times P_{CO}}{P_{H_2O}}
</div>

<h4>The equilibrium produced on heating calcium carbonate</h4>

<div class="text-block">
<p>This equilibrium is only established if the calcium carbonate is heated in a closed system, preventing the carbon dioxide from escaping.</p>
</div>

<div class="block-formula">
\text{CaCO}_{3(s)} \xrightleftharpoons{} \text{CaO}_{(s)} + \text{CO}_{2(g)}
</div>

<div class="text-block">
<p>The only thing in this equilibrium which isn't a solid is the carbon dioxide. That is all that is left in the equilibrium constant expression.</p>
</div>

<div class="block-formula">
K_p = P_{CO_2}
</div>

<h2>Calculations Involving K<sub>p</sub></h2>

<div class="text-block">
<p>On the K<sub>c</sub> page, I've already discussed the fact that the internet isn't a good medium for learning how to do calculations.</p>
<p>If you want lots of worked examples and problems to do yourself centred around K<sub>p</sub>, you might be interested in my book on chemistry calculations.</p>
</div>

<div class="note">
<p>Note: If you are interested in my <a href="../../book.html#top">chemistry calculations book</a> you might like to follow this link.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-kp.pdf" target="_blank">Questions on K<sub>p</sub></a>
<a href="../questions/a-kp.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../equilibmenu.html#top">To the equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>