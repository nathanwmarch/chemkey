<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Equilibrium Constants – Kc | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An introduction to the use of equilibrium constants expressed in terms of concentrations.">
<meta name="keywords" content="equilibrium, constant, concentration, kc">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Equilibrium Constants – K<sub>c</sub></h1>

<div class="text-block">
<p>This page explains what is meant by an equilibrium constant, introducing equilibrium constants expressed in terms of concentrations, K<sub>c</sub>. It assumes that you are familiar with the concept of a dynamic equilibrium, and know what is meant by the terms "homogeneous" and "heterogeneous" as applied to chemical reactions.</p>
</div>

<div class="note">
<p>Important: If you aren't sure about <a href="introduction.html#top">dynamic equilibria</a> it is important that you follow this link before you go on.</p>
<p>If you aren't sure what <a href="../catalysis/introduction.html#top">homogeneous and heterogeneous</a> mean, you would find it useful to follow this link and read the beginning of the page that you will find (actually on catalysis).</p>
</div>

<div class="text-block">
<p>We need to look at two different types of equilibria (homogeneous and heterogeneous) separately, because the equilibrium constants are defined differently.</p>
</div>

<ul>
<li>A homogeneous equilibrium has everything present in the same phase. The usual examples include reactions where everything is a gas, or everything is present in the same solution.</li>
<li>A heterogeneous equilibrium has things present in more than one phase. The usual examples include reactions involving solids and gases, or solids and liquids.</li> 
</ul>

<h2>K<sub>c</sub> in Homogeneous Equilibria</h2>

<div class="text-block">
<p>This is the more straightforward case. It applies where everything in the equilibrium mixture is present as a gas, or everything is present in the same solution.</p>
<p>A good example of a gaseous homogeneous equilibrium is the conversion of sulfur dioxide to sulfur trioxide at the heart of the Contact Process:</p>
</div>

<div class="block-formula">
2\text{SO}_{2(g)} + \text{O}_{2(g)} \xrightleftharpoons{} 2\text{SO}_{3(g)}
</div>

<div class="text-block">
<p>A commonly used liquid example is the esterification reaction between an organic acid and an alcohol – for example:</p>
</div>

<div class="block-formula full-width">
\text{CH}_3\text{COOH}_{(l)} + \text{CH}_3\text{CH}_2\text{OH}_{(l)} \xrightleftharpoons{} \text{CH}_3\text{COOCH}_2\text{CH}_{3(l)} + \text{H}_2\text{O}_{(l)}
</div>

<h3>Writing an Expression for K<sub>c</sub></h3>

<div class="text-block">
<p>We are going to look at a general case with the equation:</p>
</div>

<div class="block-formula">
a\text{A} + b\text{B} \xrightleftharpoons{} c\text{C} + d\text{D}
</div>

<div class="text-block">
<p>No state symbols have been given, but they will be all (g), or all (l), or all (aq) if the reaction was between substances in solution in water.</p>
<p>If you allow this reaction to reach equilibrium and then measure the equilibrium concentrations of everything, you can combine these concentrations into an expression known as an equilibrium constant.</p>
<p>The equilibrium constant always has the same value (provided you don't change the temperature), irrespective of the amounts of A, B, C and D you started with. It is also unaffected by a change in pressure or whether or not you are using a catalyst.</p>
</div>

<div class="image-container"><img src="definekc.gif"></div>

<div class="text-block">
<p>Compare this with the chemical equation for the equilibrium. The convention is that the substances on the right-hand side of the equation are written at the top of the K<sub>c</sub> expression, and those on the left-hand side at the bottom.</p>
<p>The indices (the powers that you have to raise the concentrations to – for example, squared or cubed or whatever) are just the numbers that appear in the equation.</p>
</div>

<div class="note">
<p>Note: If you have come across orders of reaction, don't confuse this with the powers that appear in the rate equation for a reaction. Those powers (the order of the reaction with respect to each of the reactants) are experimentally determined. They don't necessarily have any direct connection with the numbers that appear in the equation.</p>
<p>You may come across attempts to derive the expression for K<sub>c</sub> by writing rate equations for the forward and back reactions. These attempts make the fundamental mistake of obtaining the rate equation from the chemical equation. That's WRONG! You could only do that for the very simple cases where the reaction took place in a single step represented by the given equation.</p>
<p>Georgio Karam contacted me in July 2015 pointing out that you can in fact derive the K<sub>c</sub> expression for more complicated reactions by looking at each step of the reaction individually and then combining the results. You will find his <a href="http://basicinvestigations.blogspot.co.uk">article</a> by following this link. If you know about orders of reaction and rate equations, you will find this very easy to follow.</p>
<p>The fact that this is possible doesn't, however, seem to me to justify using the derivation in general terms of aA plus bB going to cC and dD, and assuming that the numbers in front of the substances in the overall equation represent orders of reaction. That is dangerously misleading.</p>
</div>

<h3>Some Specific Examples</h3>

<h4>The esterification reaction equilibrium</h4>

<div class="text-block">
<p>A typical equation might be:</p>
</div>

<div class="block-formula full-width">
\text{CH}_3\text{COOH}_{(l)} + \text{CH}_3\text{CH}_2\text{OH}_{(l)} \xrightleftharpoons{} \text{CH}_3\text{COOCH}_2\text{CH}_{3(l)} + \text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>There is only one molecule of everything shown in the equation. That means that all the powers in the equilibrium constant expression are "1". You don't need to write those into the K<sub>c</sub> expression.</p>
</div>

<div class="block-formula">
K_c = \frac{[\text{CH}_3\text{COOCH}_2\text{CH}_3][\text{H}_2\text{O}]}{[\text{CH}_3\text{COOH}][\text{CH}_3\text{CH}_2\text{OH}]}
</div>

<div class="text-block">
<p>As long as you keep the temperature the same, whatever proportions of acid and alcohol you mix together, once equilibrium is reached, K<sub>c</sub> always has the same value. At room temperature, this value is approximately 4 for this reaction.</p>
</div>

<h4>The equilibrium in the hydrolysis of esters</h4>

<div class="text-block">
<p>This is the reverse of the last reaction:</p>
</div>

<div class="block-formula full-width">
\text{CH}_3\text{COOCH}_2\text{CH}_{3(l)} + \text{H}_2\text{O}_{(l)} \xrightleftharpoons{} \text{CH}_3\text{COOH}_{(l)} + \text{CH}_3\text{CH}_2\text{OH}_{(l)}
</div>

<div class="text-block">
<p>The K<sub>c</sub> expression is:</p>
</div>

<div class="block-formula">
K_c = \frac{[\text{CH}_3\text{COOH}][\text{CH}_3\text{CH}_2\text{OH}]}{[\text{CH}_3\text{COOCH}_2\text{CH}_3][\text{H}_2\text{O}]}
</div>


<div class="text-block">
<p>If you compare this with the previous example, you will see that all that has happened is that the expression has turned upside-down. Its value at room temperature will be approximately 1/4 (0.25).</p>
<p>It is really important to write down the equilibrium reaction whenever you talk about an equilibrium constant. That is the only way that you can be sure that you have got the expression the right way up – with the right-hand substances on the top and the left-hand ones at the bottom.</p>
</div>

<h4>The Contact Process equilibrium</h4>

<div class="text-block">
<p>You will remember that the equation for this is:</p>
</div>

<div class="block-formula">
2\text{SO}_{2(g)} + \text{O}_{2(g)} \xrightleftharpoons{} 2\text{SO}_{3(g)}
</div>

<div class="text-block">
<p>This time the K<sub>c</sub> expression will include some visible powers:</p>
</div>

<div class="block-formula">
K_c = \frac{[\text{SO}_3]^2}{[\text{SO}_2]^2[\text{O}_2]}
</div>

<div class="text-block">
<p>Although everything is present as a gas, you still measure concentrations in mol dm<sup>-3</sup>. There is another equilibrium constant called K<sub>p</sub> which is more frequently used for gases. You will find a link to that at the bottom of the page.</p>
</div>

<h4>The Haber Process equilibrium</h4>

<h5>The equation for this is:</h5>

<div class="block-formula">
\text{N}_{2(g)} + 3\text{H}_{2(g)} \xrightleftharpoons{} 2\text{NH}_{3(g)}
</div>

<h5>and the K<sub>c</sub> expression is:</h5>

<div class="block-formula">
K_c = \frac{[\text{NH}_3]^2}{[\text{N}_2][\text{H}_2]^3}
</div>

<h2>K<sub>c</sub> in Heterogeneous Equilibria</h2>

<div class="text-block">
<p>Typical examples of a heterogeneous equilibrium include:</p>
<p>The equilibrium established if steam is in contact with red hot carbon. Here we have gases in contact with a solid.</p>
</div>

<div class="block-formula">
\text{H}_2\text{O}_{(g)} + \text{C}_{(s)} \xrightleftharpoons{} \text{H}_{2(g)} + \text{CO}_{(g)}
</div>

<div class="text-block">
<p>If you shake copper with silver nitrate solution, you get this equilibrium involving solids and aqueous ions:</p>
</div>

<div class="block-formula">
\text{Cu}_{(s)} + 2\text{Ag}^+_{(aq)} \xrightleftharpoons{} \text{Cu}^{2+}_{(aq)} + 2\text{Ag}_{(s)}
</div>

<h3>Writing an Expression for K<sub>c</sub> for a Heterogeneous Equilibrium</h3>

<div class="text-block">
<p>The important difference this time is that you don't include any term for a solid in the equilibrium expression.</p>
<p>Taking another look at the two examples above, and adding a third one:</p>
</div>

<h4>The equilibrium produced on heating carbon with steam</h4>

<div class="block-formula">
\text{H}_2\text{O}_{(g)} + \text{C}_{(s)} \xrightleftharpoons{} \text{H}_{2(g)} + \text{CO}_{(g)}
</div>

<div class="text-block">
<p>Everything is exactly the same as before in the equilibrium constant expression, except that you leave out the solid carbon.</p>
</div>

<div class="block-formula">
K_c = \frac{[\text{H}_2][\text{CO}]}{[\text{H}_2\text{O}]}
</div>

<h4>The equilibrium produced between copper and silver ions</h4>

<div class="block-formula">
\text{Cu}_{(s)} + 2\text{Ag}^+_{(aq)} \xrightleftharpoons{} \text{Cu}^{2+}_{(aq)} + 2\text{Ag}_{(s)}
</div>

<div class="text-block">
<p>Both the copper on the left-hand side and the silver on the right are solids. Both are left out of the equilibrium constant expression.</p>
</div>

<div class="block-formula">
K_c = \frac{[\text{Cu}^{2+}]}{[\text{Ag}^+]^2}
</div>

<h4>The equilibrium produced on heating calcium carbonate</h4>

<div class="text-block">
<p>This equilibrium is only established if the calcium carbonate is heated in a closed system, preventing the carbon dioxide from escaping.</p>
</div>

<div class="block-formula">
\text{CaCO}_{3(s)} \xrightleftharpoons{} \text{CaO}_{(s)} + \text{CO}_{2(g)}
</div>

<div class="text-block">
<p>The only thing in this equilibrium which isn't a solid is the carbon dioxide. That is all that is left in the equilibrium constant expression.</p>
</div>

<div class="block-formula">
K_c = [\text{CO}_2]
</div>

<h2>Calculations Involving K<sub>c</sub></h2>

<div class="text-block">
<p>There are all sorts of calculations you might be expected to do which are centred around equilibrium constants. You might be expected to calculate a value for K<sub>c</sub> including its units (which vary from case to case). Alternatively you might have to calculate equilibrium concentrations from a given value of K<sub>c</sub> and given starting concentrations.</p>
<p>This is simply too huge a topic to be able to deal with satisfactorily on the internet. It isn't the best medium for learning how to do chemistry calculations. It is much easier to do this from a carefully structured book giving you lots of worked examples and lots of problems to try yourself.</p>
<p>If you have found this site useful, you might like to have a look at my book on chemistry calculations. It covers equilibrium constant calculations starting with the most trivial cases, and gradually getting harder – up to the moderately difficult examples which may be asked in a UK A-level examination.</p>
</div>

<div class="note">
<p>Note: If you are interested in my <a href="../../book.html#top">chemistry calculations book</a> you might like to follow this link.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-kc.pdf" target="_blank">Questions on K<sub>c</sub></a>
<a href="../questions/a-kc.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="kp.html#top">To look at K<sub>p</sub></a>
<a href="../equilibmenu.html#top">To the equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>