<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Manufacture of Ethanol From Ethene | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A description of the manufacture of ethanol by the direct hydration of ethene, and an explanation of the conditions used in terms of the position of equilibrium, the rate of the reaction and the economics of the process.">
<meta name="keywords" content="equilibrium, equilibria, chemical, position of equilibrium, le chatelier, principle, concentration, temperature, pressure, catalyst, ethanol, ethene, hydration, manufacture">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Manufacture of Ethanol From Ethene</h1>

<div class="text-block">
<p>This page describes the manufacture of ethanol by the direct hydration of ethene, and then goes on to explain the reasons for the conditions used in the process. It looks at the effect of proportions, temperature, pressure and catalyst on the composition of the equilibrium mixture and the rate of the reaction.</p>
</div>

<div class="note">
<p>Important: If you aren't sure about using <a href="lechatelier.html#top">Le Chatelier's Principle</a> or about the effect of changing conditions on <a href="../basicratesmenu.html#top">rates of reaction</a> you should explore these links before you go on.</p>
<p>When you are reading this page, if you find that you aren't understanding the effect of changing one of the conditions on the position of equilibrium or on the rate of the reaction, come back and follow up these links.</p>
</div>

<h2>A Brief Summary of the Manufacture of Ethanol</h2>

<div class="text-block">
<p>Ethanol is manufactured by reacting ethene with steam. The reaction is reversible, and the formation of the ethanol is exothermic.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{CH}_2{=}\text{CH}_{2(g)} + \text{H}_2\text{O}_{(g)} \xrightleftharpoons{} \text{CH}_3\text{CH}_2\text{OH}_{(g)} \\
\Delta H = {-}45 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>Only 5% of the ethene is converted into ethanol at each pass through the reactor. By removing the ethanol from the equilibrium mixture and recycling the ethene, it is possible to achieve an overall 95% conversion.</p>
<p>A flow scheme for the reaction looks like this:</p>
</div>

<div class="image-container"><img src="ethanolflow.gif"></div>

<div class="note">
<p>Note: This is a bit of a simplification! When the gases from the reactor are cooled, then excess steam will condense as well as the ethanol. The ethanol will have to be separated from the water by fractional distillation.</p>
<p>All the sources I have looked at gloss over this, so I don't have any details. I assume it is a normal fractional distillation of an ethanol-water mixture.</p>
</div>

<h2>Explaining the Conditions</h2>

<h3>The Proportions of Ethene and Steam</h3>

<div class="text-block">
<p>The equation shows that the ethene and steam react 1 : 1. In order to get this ratio, you would have to use equal volumes of the two gases.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{CH}_2{=}\text{CH}_{2(g)} + \text{H}_2\text{O}_{(g)} \xrightleftharpoons{} \text{CH}_3\text{CH}_2\text{OH}_{(g)} \\
\Delta H = {-}45 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>Because water is cheap, it would seem sensible to use an excess of steam in order to move the position of equilibrium to the right according to Le Chatelier's Principle. In practice, an excess of ethene is used.</p>
<p>This is very surprising at first sight. Even if the reaction was one-way, you couldn't possibly convert all the ethene into ethanol. There isn't enough steam to react with it.</p>
<p>The reason for this oddity lies with the nature of the catalyst. The catalyst is phosphoric(V) acid coated onto a solid silicon dioxide support. If you use too much steam, it dilutes the catalyst and can even wash it off the support, making it useless.</p>
</div>

<h3>The Temperature</h3>

<h4>Equilibrium considerations</h4>

<div class="text-block">
<p>You need to shift the position of the equilibrium as far as possible to the right in order to produce the maximum possible amount of ethanol in the equilibrium mixture.</p>
<p>The forward reaction (the production of ethanol) is exothermic.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{CH}_2{=}\text{CH}_{2(g)} + \text{H}_2\text{O}_{(g)} \xrightleftharpoons{} \text{CH}_3\text{CH}_2\text{OH}_{(g)} \\
\Delta H = {-}45 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>According to Le Chatelier's Principle, this will be favoured if you lower the temperature. The system will respond by moving the position of equilibrium to counteract this – in other words by producing more heat.</p>
<p>In order to get as much ethanol as possible in the equilibrium mixture, you need as low a temperature as possible. However, 300°C isn't particularly low.</p>
</div>

<h4>Rate considerations</h4>

<div class="text-block">
<p>The lower the temperature you use, the slower the reaction becomes. A manufacturer is trying to produce as much ethanol as possible per day. It makes no sense to try to achieve an equilibrium mixture which contains a very high proportion of ethanol if it takes several years for the reaction to reach that equilibrium.</p>
<p>You need the gases to reach equilibrium within the very short time that they will be in contact with the catalyst in the reactor.</p>
</div>

<h4>The compromise</h4>

<div class="text-block">
<p>300°C is a compromise temperature producing an acceptable proportion of ethanol in the equilibrium mixture, but in a very short time. Under these conditions, about 5% of the ethene reacts to give ethanol at each pass over the catalyst.</p>
</div>

<h3>The Pressure</h3>

<h4>Equilibrium considerations</h4>

<div class="block-formula">
\begin{gathered}
\text{CH}_2{=}\text{CH}_{2(g)} + \text{H}_2\text{O}_{(g)} \xrightleftharpoons{} \text{CH}_3\text{CH}_2\text{OH}_{(g)} \\
\Delta H = {-}45 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>Notice that there are 2 molecules on the left-hand side of the equation, but only 1 on the right.</p>
<p>According to Le Chatelier's Principle, if you increase the pressure the system will respond by favouring the reaction which produces fewer molecules. That will cause the pressure to fall again.</p>
<p>In order to get as much ethanol as possible in the equilibrium mixture, you need as high a pressure as possible. High pressures also increase the rate of the reaction. However, the pressure used isn't all that high.</p>
</div>

<h4>Problems with high pressures</h4>

<div class="text-block">
<p>There are two quite separate problems in this case:</p>
</div>

<ul>
<li>High pressures are expensive. It costs more to build the original plant because you need extremely strong pipes and containment vessels. It also needs a lot of energy to produce the high pressures. That can make the ethanol uneconomic to produce.</li>
<li>At high pressures, the ethene polymerises to make poly(ethene). Apart from wasting ethene, this could also clog up the plant.</li>
</ul>

<div class="note">
<p>Note: If you are interested in the <a href="../../mechanisms/freerad/polym.html#top">mechanism for the polymerisation of ethene</a>, you might like to follow this link – although it isn't relevant to the current topic.</p>
</div>

<h3>The Catalyst</h3>

<h4>Equilibrium considerations</h4>

<div class="text-block">
<p>The catalyst has no effect whatsoever on the position of the equilibrium. Adding a catalyst doesn't produce any greater percentage of ethanol in the equilibrium mixture. Its only function is to speed up the reaction.</p>
</div>

<h4>Rate considerations</h4>

<div class="text-block">
<p>In the absence of a catalyst the reaction is so slow that virtually no reaction happens in any sensible time. The catalyst ensures that the reaction is fast enough for a dynamic equilibrium to be set up within the very short time that the gases are actually in the reactor.</p>
</div>

<div class="note">
<p>Note: If you are interested in the <a href="../catalysis/hydrate.html#top">mechanism for the hydration of ethene</a> and the role of the catalyst in it, you will find it in a section on catalysis by following this link.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-ethanol.pdf" target="_blank">Questions on the manufacture of ethanol</a>
<a href="../questions/a-ethanol.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../equilibmenu.html#top">To the equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>