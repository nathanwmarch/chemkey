<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Le Châtelier's Principle | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Le Châtelier's Principle and how to use it to work out what happens to the position of equilibrium if the conditions are changed for a reaction which is in dynamic equilibrium.">
<meta name="keywords" content="equilibrium, equilibria, chemical, reversible reaction, dynamic, position of equilibrium, le châtelier, principle, concentration, temperature, pressure, catalyst">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Le Châtelier's Principle</h1>

<div class="text-block">
<p>This page looks at Le Châtelier's Principle and explains how to apply it to reactions in a state of dynamic equilibrium. It covers changes to the position of equilibrium if you change concentration, pressure or temperature. It also explains very briefly why catalysts have no effect on the position of equilibrium.</p>
</div>

<div class="note">
<p>Important: If you aren't sure about the words dynamic equilibrium or position of equilibrium you should read the <a href="introduction.html#top">introductory page</a> before you go on.</p>
</div>

<div class="text-block">
<p>It is important in understanding everything on this page to realise that Le Châtelier's Principle is no more than a useful guide to help you work out what happens when you change the conditions in a reaction in dynamic equilibrium. It doesn't explain anything. I'll keep coming back to that point!</p>
</div>

<h2>Using Le Châtelier's Principle</h2>

<div class="principle">
<p>Le Châtelier's Principle: if a dynamic equilibrium is disturbed by changing the conditions, the position of equilibrium moves to counteract the change.</p>
</div>

<h3>Using Le Châtelier's Principle With a Change of Concentration</h3>

<div class="text-block">
<p>Suppose you have an equilibrium established between four substances A, B, C and D.</p>
</div>

<div class="block-formula">
\text{A} + 2\text{B} \xrightleftharpoons{} \text{C} + \text{D}
</div>

<div class="note">
<p>Note: In case you wonder, the reason for choosing this equation rather than having just A + B on the left-hand side is because further down this page I need an equation which has different numbers of molecules on each side. I am going to use that same equation throughout this page.</p>
</div>

<h4>What would happen if you changed the conditions by increasing the concentration of A?</h4>

<div class="text-block">
<p>According to Le Châtelier, the position of equilibrium will move in such a way as to counteract the change. That means that the position of equilibrium will move so that the concentration of A decreases again – by reacting it with B and turning it into C + D. The position of equilibrium moves to the right.</p>
</div>

<div class="block-formula">
\begin{gathered}
\color{#467abf}{\uparrow}\text{A} + 2\text{B} \xrightleftharpoons{} \text{C} + \text{D} \\
\color{#467abf}{\xrightarrow[
\begin{gathered}
\scriptstyle\text{the position of equilibrium moves to the right} \\
\scriptstyle\text{if you increase the concentration of A}
\end{gathered}
]{}} \\
\end{gathered}
</div>

<div class="text-block">
<p>This is a useful way of converting the maximum possible amount of B into C and D. You might use it if, for example, B was a relatively expensive material whereas A was cheap and plentiful.</p>
</div>

<h4>What would happen if you changed the conditions by decreasing the concentration of A?</h4>

<div class="text-block">
<p>According to Le Châtelier, the position of equilibrium will move so that the concentration of A increases again. That means that more C and D will react to replace the A that has been removed. The position of equilibrium moves to the left.</p>
</div>

<div class="block-formula">
\begin{gathered}
\color{#467abf}{\downarrow}\text{A} + 2\text{B} \xrightleftharpoons{} \text{C} + \text{D} \\
\color{#467abf}{\xleftarrow[
\begin{gathered}
\scriptstyle\text{the position of equilibrium moves to the left} \\
\scriptstyle\text{if you decrease the concentration of A}
\end{gathered}
]{}} \\
\end{gathered}
</div>

<div class="text-block">
<p>This is esssentially what happens if you remove one of the products of the reaction as soon as it is formed. If, for example, you removed C as soon as it was formed, the position of equilibrium would move to the right to replace it. If you kept on removing it, the equilibrium position would keep on moving rightwards – turning this into a one-way reaction.</p>
</div>

<h4>Important</h4>

<div class="text-block">
<p>This isn't in any way an explanation of why the position of equilibrium moves in the ways described. All Le Châtelier's Principle gives you is a quick way of working out what happens.</p>
</div>

<div class="note">
<p>Note: If you know about equilibrium constants, you will find a <a href="change.html#top">more detailed explanation</a> of the effect of a change of concentration by following this link. If you don't know anything about equilibrium constants, you should ignore this link.</p>
</div>

<h3>Using Le Châtelier's Principle With a Change of Pressure</h3>

<div class="text-block">
<p>This only applies to reactions involving gases:</p>
</div>

<div class="block-formula">
\text{A}_{(g)} + 2\text{B}_{(g)} \xrightleftharpoons{} \text{C}_{(g)} + \text{D}_{(g)}
</div>

<h4>What would happen if you changed the conditions by increasing the pressure?</h4>

<div class="text-block">
<p>According to Le Châtelier, the position of equilibrium will move in such a way as to counteract the change. That means that the position of equilibrium will move so that the pressure is reduced again.</p>
<p>Pressure is caused by gas molecules hitting the sides of their container. The more molecules you have in the container, the higher the pressure will be. The system can reduce the pressure by reacting in such a way as to produce fewer molecules.</p>
<p>In this case, there are 3 molecules on the left-hand side of the equation, but only 2 on the right. By forming more C and D, the system causes the pressure to reduce.</p>
<p>Increasing the pressure on a gas reaction shifts the position of equilibrium towards the side with fewer molecules.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{A}_{(g)} + 2\text{B}_{(g)} \xrightleftharpoons{} \text{C}_{(g)} + \text{D}_{(g)} \\
\color{#467abf}{\xrightarrow[
\begin{gathered}
\scriptstyle\text{the position of equilibrium moves to the right} \\
\scriptstyle\text{if you increase the pressure}
\end{gathered}
]{}} \\
\end{gathered}
</div>

<h4>What would happen if you changed the conditions by decreasing the pressure?</h4>

<div class="text-block">
<p>The equilibrium will move in such a way that the pressure increases again. It can do that by producing more molecules. In this case, the position of equilibrium will move towards the left-hand side of the reaction.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{A}_{(g)} + 2\text{B}_{(g)} \xrightleftharpoons{} \text{C}_{(g)} + \text{D}_{(g)} \\
\color{#467abf}{\xleftarrow[
\begin{gathered}
\scriptstyle\text{the position of equilibrium moves to the left} \\
\scriptstyle\text{if you decrease the pressure}
\end{gathered}
]{}} \\
\end{gathered}
</div>

<h4>What happens if there are the same number of molecules on both sides of the equilibrium reaction?</h4>

<div class="text-block">
<p>In this case, increasing the pressure has no effect whatsoever on the position of the equilibrium. Because you have the same numbers of molecules on both sides, the equilibrium can't move in any way that will reduce the pressure again.</p>
<h4>Important</h4>
<p>Again, this isn't an explanation of why the position of equilibrium moves in the ways described. You will find a rather mathematical treatment of the explanation by following the link below.</p>
</div>

<div class="note">
<p>Note: You will find a <a href="change.html#top">detailed explanation</a> by following this link. If you don't know anything about equilibrium constants (particularly K<sub>p</sub>), you should ignore this link. The same thing applies if you don't like things to be too mathematical! If you are a UK A-level student, you won't need this explanation.</p>
</div>

<h3>Using Le Châtelier's Principle With a Change of Temperature</h3>

<div class="text-block">
<p>For this, you need to know whether heat is given out or absorbed during the reaction. Assume that our forward reaction is exothermic (heat is evolved):</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{A} + 2\text{B} \xrightleftharpoons{} \text{C} + \text{D} \\
\Delta H = {-}250 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>This shows that 250 kJ is evolved (hence the negative sign) when 1 mole of A reacts completely with 2 moles of B. For reversible reactions, the value is always given as if the reaction was one-way in the forward direction.</p>
<p>The back reaction (the conversion of C and D into A and B) would be endothermic by exactly the same amount.</p>
</div>

<div class="block-formula full-width">
\begin{matrix}
\color{#467abf}{\xrightarrow[
\begin{gathered}
\scriptstyle\text{250 kJ is given out when 1 mol A and 2 mol B} \\
\scriptstyle\text{react completely to give 1 mol C and 1 mol D}
\end{gathered}
]{}} \\
\text{A} + 2\text{B} \xrightleftharpoons{} \text{C} + \text{D} &
\Delta H = {-}250 \text{ kJ mol}^{-1} \\
\color{#00CC99}{\xleftarrow[
\begin{gathered}
\scriptstyle\text{250 kJ is absorbed when 1 mol C and 1 mol D} \\
\scriptstyle\text{react completely to give 1 mol A and 2 mol B}
\end{gathered}
]{}}
\end{matrix}
</div>
 
<h4>What would happen if you changed the conditions by increasing the temperature?</h4>

<div class="text-block">
<p>According to Le Châtelier, the position of equilibrium will move in such a way as to counteract the change. That means that the position of equilibrium will move so that the temperature is reduced again.</p>
<p>Suppose the system is in equilibrium at 300°C, and you increase the temperature to 500°C. How can the reaction counteract the change you have made? How can it cool itself down again?</p>
<p>To cool down, it needs to absorb the extra heat that you have just put in. In the case we are looking at, the back reaction absorbs heat. The position of equilibrium therefore moves to the left. The new equilibrium mixture contains more A and B, and less C and D.</p>
</div>

<div class="block-formula full-width">
\begin{matrix}
\scriptstyle\color{#467abf}{\text{the reverse reaction absorbs heat}} \\
\text{A} + 2\text{B} \xrightleftharpoons{} \text{C} + \text{D} &
\Delta H = {-}250 \text{ kJ mol}^{-1} \\
\color{#467abf}{\xleftarrow[
\begin{gathered}
\scriptstyle\text{the position of equilibrium moves to the left} \\
\scriptstyle\text{if you increase the temperature}
\end{gathered}
]{}}
\end{matrix}
</div>

<div class="text-block">
<p>If you were aiming to make as much C and D as possible, increasing the temperature on a reversible reaction where the forward reaction is exothermic isn't a good idea!</p>
<h4>What would happen if you changed the conditions by decreasing the temperature?</h4>
<p>The equilibrium will move in such a way that the temperature increases again.</p>
<p>Suppose the system is in equilibrium at 500°C and you reduce the temperature to 400°C. The reaction will tend to heat itself up again to return to the original temperature. It can do that by favouring the exothermic reaction.</p>
<p>The position of equilibrium will move to the right. More A and B are converted into C and D at the lower temperature.</p>
</div>

<div class="block-formula full-width">
\begin{matrix}
\scriptstyle\color{#467abf}{\text{the forward reaction releases heat}} \\
\text{A} + 2\text{B} \xrightleftharpoons{} \text{C} + \text{D} &
\Delta H = {-}250 \text{ kJ mol}^{-1} \\
\color{#467abf}{\xrightarrow[
\begin{gathered}
\scriptstyle\text{the position of equilibrium moves to the right} \\
\scriptstyle\text{if you decrease the temperature}
\end{gathered}
]{}}
\end{matrix}
</div>


<div class="summary">
<ul>
<li>Increasing the temperature of a system in dynamic equilibrium favours the endothermic reaction. The system counteracts the change you have made by absorbing the extra heat.</li>

<li>Decreasing the temperature of a system in dynamic equilibrium favours the exothermic reaction. The system counteracts the change you have made by producing more heat.</li>
</ul>
</div>

<h4>Important</h4>

<div class="text-block">
<p>Again, this isn't in any way an explanation of why the position of equilibrium moves in the ways described. It is only a way of helping you to work out what happens.</p>
</div>

<div class="note">
<p>Note: I am not going to attempt an explanation of this anywhere on the site. To do it properly is far too difficult for this level. It is possible to come up with an explanation of sorts by looking at how the rate constants for the forward and back reactions change relative to each other by using the Arrhenius equation, but this isn't a standard way of doing it, and is liable to confuse those of you going on to do a Chemistry degree. If you aren't going to do a Chemistry degree, you won't need to know about this anyway!</p>
</div>

<h3>Le Châtelier's Principle and Catalysts</h3>

<div class="text-block">
<p>Catalysts have sneaked onto this page under false pretences, because adding a catalyst makes absolutely no difference to the position of equilibrium, and Le Châtelier's Principle doesn't apply to them.</p>
<p>This is because a catalyst speeds up the forward and back reaction to the same extent. Because adding a catalyst doesn't affect the relative rates of the two reactions, it can't affect the position of equilibrium. So why use a catalyst?</p>
<p>For a dynamic equilibrium to be set up, the rates of the forward reaction and the back reaction have to become equal. This doesn't happen instantly. For a very slow reaction, it could take years! A catalyst speeds up the rate at which a reaction reaches dynamic equilibrium.</p>
</div>

<div class="note">
<p>Note: You might try imagining how long it would take to establish a dynamic equilibrium if you took the visual model on the <a href="introduction.html#top">introductory page</a> and reduced the chances of the colours changing by a factor of 1000 – from 3 in 6 to 3 in 6000 and from 1 in 6 to 1 in 6000.</p>
<p>Starting with blue squares, by the end of the time taken for the examples on that page, you would most probably still have entirely blue squares. Eventually, though, you would end up with the same sort of patterns as before – containing 25% blue and 75% orange squares.</p>
<p>Just for fun, here is that exact setup...</p>
<div class="svg-container" id="dynamic-svg-container">
<svg class="small-responsive-svg" id="dynamic-svg" viewBox="0 0 39 39">
<rect class="dynamic-rect" x="0" y="0" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="10" y="0" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="20" y="0" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="30" y="0" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="0" y="10" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="10" y="10" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="20" y="10" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="30" y="10" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="0" y="20" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="10" y="20" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="20" y="20" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="30" y="20" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="0" y="30" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="10" y="30" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="20" y="30" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
<rect class="dynamic-rect" x="30" y="30" width="9" height="9" style="fill: rgb(0, 0, 255)"/>
</svg>
</div>

<style>
.dynamic-rect {
 transition: fill 0.5s ease-in-out;
}

#stop-stopButton {
display: inline-block;
}

#start-stopButton {
display: inline-block;
}

#dynamic-svg-container {
text-align: center;
}
</style>

<script>
window.addEventListener("load", startDynamicSVG);

var interval;

function startDynamicSVG() {
var color1 = "rgb(0, 0, 255)";
var color2 = "rgb(255, 0, 0)";
var oddsOfColor1to2 = 0.0005;
var oddsOfColor2to1 = 1/6000;

var startDynamicSVG = document.getElementById("dynamic-svg");

var dynamicRects = startDynamicSVG.getElementsByClassName("dynamic-rect");

interval = setInterval(function() {swapFill(dynamicRects,color1,color2,oddsOfColor1to2,oddsOfColor2to1);},1000);

var stopButton = document.createElement("a");
stopButton.setAttribute("onclick", "stopDynamicSVG()");
stopButton.innerHTML = "Stop Reaction";
stopButton.setAttribute("id", "stop-button");

var container = document.getElementById("dynamic-svg").parentElement;
console.log(container);
container.appendChild(stopButton);
}

function stopDynamicSVG() {
clearInterval(interval);
}

function swapFill(elements, color1, color2, odds1to2, odds2to1) {
for (var i = elements.length - 1; i >= 0; i--) {
var element = elements[i];
var fill = element.style.fill;

if (fill == color1) {
if (randomInRange(odds1to2)) {
element.style.fill = color2;
}
} else if (fill == color2) {
if (randomInRange(odds2to1)) {
element.style.fill = color1;
}
}
}
}

function randomInRange(odds) {
var roll = Math.random();

if (roll <= odds) {
return true
}

return false
}
</script>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-lechâtelier.pdf" target="_blank">Questions on Le Châtelier's Principle</a>
<a href="../questions/a-lechâtelier.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../equilibmenu.html#top">To the equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>