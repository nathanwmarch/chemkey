<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to Saturated Vapour Pressure | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of how the saturated vapour pressure of a pure substance arises and how it varies with temperature">
<meta name="keywords" content="equilibrium, equilibria, vapour, vapor, pressure, saturated, sublimation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to Saturated Vapour Pressure</h1>

<div class="text-block">
<p>This page looks at how the equilibrium between a liquid (or a solid) and its vapour leads to the idea of a saturated vapour pressure. It also looks at how saturated vapour pressure varies with temperature, and the relationship between saturated vapour pressure and boiling point.</p>
</div>

<h2>The Origin of Saturated Vapour Pressure</h2>

<h3>The Evaporation of a Liquid</h3>

<div class="text-block">
<p>The average energy of the particles in a liquid is governed by the temperature. The higher the temperature, the higher the average energy. But within that average, some particles have energies higher than the average, and others have energies lower than the average.</p>
<p>Some of the more energetic particles on the surface of the liquid can be moving fast enough to escape from the attractive forces holding the liquid together. They evaporate.</p>
<p>The diagram shows a small region of a liquid near its surface.</p>
</div>

<div class="image-container"><img src="evaporation.gif"></div>

<div class="text-block">
<p>Notice that evaporation only takes place on the surface of the liquid. That's quite different from boiling which happens when there is enough energy to disrupt the attractive forces throughout the liquid. That's why, if you look at boiling water, you see bubbles of gas being formed all the way through the liquid.</p>
<p>If you look at water which is just evaporating in the sun, you don't see any bubbles. Water molecules are simply breaking away from the surface layer.</p>
<p>Eventually, the water will all evaporate in this way. The energy which is lost as the particles evaporate is replaced from the surroundings. As the molecules in the water jostle with each other, new molecules will gain enough energy to escape from the surface.</p>
</div>

<h3>The Evaporation of a Liquid in a Closed Container</h3>

<div class="text-block">
<p>Now imagine what happens if the liquid is in a closed container. Common sense tells you that water in a sealed bottle doesn't seem to evaporate – or at least, it doesn't disappear over time.</p>
<p>But there is constant evaporation from the surface. Particles continue to break away from the surface of the liquid – but this time they are trapped in the space above the liquid.</p>
</div>

<div class="image-container"><img src="sealed1.gif"></div>

<div class="text-block">
<p>As the gaseous particles bounce around, some of them will hit the surface of the liquid again, and be trapped there. There will rapidly be an equilibrium set up in which the number of particles leaving the surface is exactly balanced by the number rejoining it.</p>
</div>

<div class="image-container"><img src="sealed2.gif"></div>

<div class="text-block">
<p>In this equilibrium, there will be a fixed number of the gaseous particles in the space above the liquid.</p>
<p>When these particles hit the walls of the container, they exert a pressure. This pressure is called the saturated vapour pressure (also known as saturation vapour pressure) of the liquid.</p>
</div>

<h3>Measuring the Saturated Vapour Pressure</h3>

<div class="text-block">
<p>It isn't difficult to show the existence of this saturated vapour pressure (and to measure it) using a simple piece of apparatus.</p>
</div>

<div class="note">
<p>Note: This experiment is much easier to talk about than do, given the safety problems in handling mercury because of its poisonous vapour. This is particularly going to be a problem if you want to find the saturated vapour pressure of a liquid at a higher temperature. You would have to use a more complex bit of apparatus. That isn't a problem you need to worry about for UK A-level purposes.</p>
</div>

<div class="text-block">
<p>If you have a mercury barometer tube in a trough of mercury, at 1 atmosphere pressure the column will be 760 mm tall. 1 atmosphere is sometimes quoted as 760 mmHg ("millimetres of mercury").</p>
</div>

<div class="image-container"><img src="barometer1.gif"></div>

<div class="text-block">
<p>If you squirt a few drops of liquid into the tube, it will rise to form a thin layer floating on top of the mercury. Some of the liquid will evaporate and you will get the equilibrium we've just been talking about – provided there is still some liquid on top of the mercury. It is only an equilibrium if both liquid and vapour are present.</p>
</div>

<div class="image-container"><img src="barometer2.gif"></div>

<div class="text-block">
<p>The saturated vapour pressure of the liquid will force the mercury level down a bit. You can measure the drop – and this gives a value for the saturated vapour pressure of the liquid at this temperature. In this case, the mercury has been forced down by a distance of 760 – 630 mm. The saturated vapour pressure of this liquid at the temperature of the experiment is 130 mmHg.</p>
<p>You could convert this into proper SI units (pascals) if you wanted to. 760 mmHg is equivalent to 101325 Pa.</p>
<p>A value of 130 mmHg is quite a high vapour pressure if we are talking about room temperature. Water's saturated vapour pressure is about 20 mmHg at this temperature. A high vapour pressure means that the liquid must be volatile – molecules escape from its surface relatively easily, and aren't very good at sticking back on again either.</p>
<p>That will result in larger numbers of them in the gas state once equilibrium is reached.</p>
<p>The liquid in the example must have significantly weaker intermolecular forces than water.</p>
</div>

<h2>The Variation of Saturated Vapour Pressure with Temperature</h2>

<h3>The Effect of Temperature on the Equilibrium Between Liquid and Vapour</h3>

<div class="text-block">
<p>You can look at this in two ways.</p>
<p>There is a common sense way. If you increase the temperature, you are increasing the average energy of the particles present. That means that more of them are likely to have enough energy to escape from the surface of the liquid. That will tend to increase the saturated vapour pressure.</p>
<p>Or you can look at it in terms of Le Chatelier's Principle – which works just as well in this kind of physical situation as it does in the more familiar chemical examples.</p>
</div>

<div class="note">
<p>Note: You could follow this link if you aren't sure about <a href="../equilibria/lechatelier.html#top">Le Chatelier's Principle</a>.</p>
</div>

<div class="text-block">
<p>When the space above the liquid is saturated with vapour particles, you have this equilibrium occurring on the surface of the liquid:</p>
</div>

<div class="block-formula">
\begin{matrix}
\text{liquid} \xrightleftharpoons{} \text{vapour} & \Delta H \text{ is +ve}
\end{matrix}
</div>

<div class="text-block">
<p>The forward change (liquid to vapour) is endothermic. It needs heat to convert the liquid into the vapour.</p>
<p>According to Le Chatelier, increasing the temperature of a system in a dynamic equilibrium favours the endothermic change. That means that increasing the temperature increases the amount of vapour present, and so increases the saturated vapour pressure.</p>
</div>

<h3>The Effect of Temperature on the Saturated Vapour Pressure of Water</h3>

<div class="text-block">
<p>The graph shows how the saturated vapour pressure (svp) of water varies from 0°C to 100 °C. The pressure scale (the vertical one) is measured in kilopascals (kPa). 1 atmosphere pressure is 101.325 kPa.</p>
</div>

<div class="image-container"><img src="svpwaterplot.gif"></div>

<h3>Saturated Vapour Pressure and Boiling Point</h3>

<div class="text-block">
<p>A liquid boils when its saturated vapour pressure becomes equal to the external pressure on the liquid. When that happens, it enables bubbles of vapour to form throughout the liquid – those are the bubbles you see when a liquid boils.</p>
<p>If the external pressure is higher than the saturated vapour pressure, these bubbles are prevented from forming, and you just get evaporation at the surface of the liquid.</p>
<p>If the liquid is in an open container and exposed to normal atmospheric pressure, the liquid boils when its saturated vapour pressure becomes equal to 1 atmosphere (or 101325 Pa or 101.325 kPa or 760 mmHg). This happens with water when the temperature reaches 100°C.</p>
<p>But at different pressures, water will boil at different temperatures. For example, at the top of Mount Everest the pressure is so low that water will boil at about 70°C. Depressions from the Atlantic can easily lower the atmospheric pressure in the UK enough so that water will boil at 99°C – even lower with very deep depressions.</p>
<p>Whenever we just talk about "the boiling point" of a liquid, we always assume that it is being measured at exactly 1 atmosphere pressure. In practice, of course, that is rarely exactly true.</p>
</div>

<h2>Saturated Vapour Pressure and Solids</h2>

<h3>Sublimation</h3>

<div class="text-block">
<p>Solids can also lose particles from their surface to form a vapour, except that in this case we call the effect sublimation rather than evaporation. Sublimation is the direct change from solid to vapour (or vice versa) without going through the liquid stage.</p>
<p>In most cases, at ordinary temperatures, the saturated vapour pressures of solids range from low to very, very, very low. The forces of attraction in many solids are too high to allow much loss of particles from the surface.</p>
<p>However, there are some which do easily form vapours. For example, naphthalene (used in old-fashioned "moth balls" to deter clothes moths) has quite a strong smell. Molecules must be breaking away from the surface as a vapour, because otherwise you wouldn't be able to smell it.</p>
<p>Another fairly common example (discussed in detail on another page) is solid carbon dioxide – "dry ice". This never forms a liquid at atmospheric pressure and always converts directly from solid to vapour. That's why it is known as dry ice.</p>
</div>

<div class="note">
<p>Note: This link will take you straight to the page about <a href="phasediags.html#top">phase diagrams</a> where this is discussed in detail. Follow this link as well if you are interested in the logical next step in this topic which is a discussion of the phase diagrams of pure substances, including water and carbon dioxide.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-svp.pdf" target="_blank">Questions on an introduction to saturated vapour pressure</a>
<a href="../questions/a-svp.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../phaseeqiamenu.html#top">To the phase equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>