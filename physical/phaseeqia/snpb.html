<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Solid-liquid Phase Diagrams: Tin and Lead | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the relationship between the cooling curves for liquid mixtures of tin and lead, and the resulting phase diagram. Includes the concept of a eutectic mixture.">
<meta name="keywords" content="phase, diagram, solid, liquid, freezing, melting, point, lowering, tin, lead, eutectic, cooling curve">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Solid-liquid Phase Diagrams: Tin and Lead</h1>

<div class="text-block">
<p>This page explains the relationship between the cooling curves for liquid mixtures of tin and lead, and the resulting phase diagram. It also offers a simple introduction to the idea of a eutectic mixture.</p>
</div>

<div class="note">
<p>Important: This page is only really designed to be an introduction to the topic suitable for courses for 16-18 year olds such as UK A-level chemistry. Be aware that the phase diagram used is a simplified version of the real thing. In particular, it ignores the formation of solid solutions of tin and lead. I will give a link to the correct phase diagram later.</p>
</div>

<h2>Cooling Curves</h2>

<h3>Cooling Curves for Pure Substances</h3>

<div class="text-block">
<p>Suppose you have some pure molten lead and allow it to cool down until it has all solidified, plotting the temperature of the lead against time as you go. You would end up with a typical cooling curve for a pure substance.</p>
</div>

<div class="image-container"><img src="ccpurelead.gif"></div>

<div class="note">
<p>Note: Just before the liquid freezes, there is sometimes a slight dip in the curve below the freezing point. This is called supercooling. As soon as some solid forms, the temperature recovers to the normal freezing point. Supercooling isn't important for the present discussion, so I'm ignoring it on the diagrams.</p>
</div>

<div class="text-block">
<p>Throughout the whole experiment, heat is being lost to the surroundings – and yet the temperature doesn't fall at all while the lead is freezing. This is because the freezing process liberates heat at exactly the same rate that it is being lost to the surroundings.</p>
<p>Energy is released when new bonds form – in this case, the strong metallic bonds in the solid lead.</p>
<p>If you repeated this process for pure liquid tin, the shape of the graph would be exactly the same, except that the freezing point would now be at 232°C. (The graph for this is further down the page.)</p>
</div>

<h3>Cooling Curves for Tin-lead Mixtures</h3>

<h4>A sample curve</h4>

<div class="text-block">
<p>If you add some tin to the lead, the shape of the cooling curve changes. The next graph shows what happens if you cool a liquid mixture containing about 67% lead and 33% tin by mass.</p>
</div>

<div class="image-container"><img src="cc33tin.gif"></div>

<div class="text-block">
<p>There are lots of things to look at:</p>
</div>

<ul>
<li>Notice that nothing happens at all at the normal freezing point of the lead. Adding the tin to it lowers its freezing point.</li>
<li>Freezing starts for this mixture at about 250°C. You would start to get some solid lead formed – but no tin. At that point the rate of cooling slows down – the curve gets less steep.<br>However, the graph doesn't go horizontal yet. Although energy is being given off as the lead turns to a solid, there isn't anything similar happening to the tin. That means that there isn't enough energy released to keep the temperature constant.</li>
<li>The temperature does stop falling at 183°C. Now both tin and lead are freezing. Once everything has solidified, the temperature continues to fall.</li>
</ul>

<h4>Changing the proportions of tin and lead</h4>

<div class="text-block">
<p>If you had less tin in the mixture, the overall shape of the curve stays much the same, but the point at which the lead first starts to freeze changes.</p>
<p>The less tin there is, the smaller the drop in the freezing point of the lead.</p>
<p>For a mixture containing only 20% of tin, the freezing point of the lead is about 275°C.That's where the graph would suddenly become less steep.</p>
<p>BUT you will still get the graph going horizontal (showing the freezing of both the tin and lead) at exactly the same temperature: 183°C.</p>
<p>As you increase the proportion of tin, the first signs of solid lead appear at lower and lower temperatures, but the final freezing of the whole mixture still happens at 183°C.</p>
<p>That continues until you have added enough tin that the mixture contains 62% tin and 38% lead. At that point, the graph changes.</p>
</div>

<div class="image-container"><img src="ccpbsneut.gif"></div>

<div class="text-block">
<p>This particular mixture of lead and tin has a cooling curve which looks exactly like that of a pure substance rather than a mixture. There is just the single horizontal part of the graph where everything is freezing.</p>
<p>However, it is still a mixture. If you use a microscope to look at the solid formed after freezing, you can see the individual crystals of tin and lead.</p>
<p>This particular mixture is known as a eutectic mixture. The word "eutectic" comes from Greek and means "easily melted".</p>
<p>The eutectic mixture has the lowest melting point (which is, of course, the same as the freezing point) of any mixture of lead and tin. The temperature at which the eutectic mixture freezes or melts is known as the eutectic temperature.</p>
</div>

<h4>What happens if there is more than 62% of tin in the mixture?</h4>

<div class="text-block">
<p>You can trace it through in exactly the same way, by imagining starting with pure tin and then adding lead to it.</p>
<p>The cooling curve for pure liquid tin looks like this:</p>
</div>

<div class="image-container"><img src="ccpuretin.gif"></div>

<div class="text-block">
<p>It's just like the pure lead cooling curve except that tin's freezing point is lower.</p>
<p>If you add small amounts of lead to the tin, so that you have perhaps 80% tin and 20% lead, you will get a curve like this:</p>
</div>

<div class="image-container"><img src="cc80tin.gif"></div>

<div class="text-block">
<p>Notice the lowered freezing point of the tin. Notice also the final freezing of the whole mixture again takes place at 183°C.</p>
<p>As you increase the amount of lead (or decrease the amount of tin – same thing!) until there is 62% of tin and 38% of lead, you will again get the eutectic mixture with the curve we've already looked at.</p>
</div>

<div class="image-container"><img src="ccpbsneut.gif"></div>

<h2>The Phase Diagram</h2>

<h3>Constructing the Phase Diagram</h3>

<div class="text-block">
<p>You start from data obtained from the cooling curves.You draw a graph of the temperature at which freezing first starts against the proportion of tin and lead in the mixture. The only unusual thing is that you draw the temperature scale at each end of the diagram instead of only at the left-hand side.</p>
</div>

<div class="image-container"><img src="eutdiagsnpb1.gif"></div>

<div class="text-block">
<p>Notice that at the left-hand side and right-hand sides of the curves you have the freezing points (melting points) of the pure lead and tin.</p>
</div>

<div class="note">
<p>Note: The two lines meeting at the eutectic point have been simplified slightly so that they are drawn as straight lines rather than slight curves. It doesn't affect the argument in any way. I haven't been able to find the actual data to plot them accurately, so the simplification is to avoid giving the impression that I actually know exactly what the curves look like!</p>
</div>

<div class="text-block">
<p>To finish off the phase diagram, all you have to do is draw a single horizontal line across at the eutectic temperature. Then you label each area of the diagram with what you would find under the various different conditions.</p>
</div>

<div class="image-container"><img src="eutdiagsnpb2.gif"></div>

<div class="note">
<p>Important: This is a simplified version of the real tin-lead phase diagram. In particular, it ignores the formation of solid solutions of tin and lead. You will find the correct diagram on <a href=" http://www.metallurgy.nist.gov/phase/solder/pbsn.html">this NIST web page</a>. Beware that on that page, the tin-lead axis is reversed from the one I have drawn above – in other words 100% lead is on the right rather than the left.</p>
<p>If you are just using this page as an introduction to this sort of phase diagram, you don't need to worry about this.</p>
</div>

<h3>Using the Phase Diagram</h3>

<div class="note">
<p>Important: One of the problems with the various sorts of phase diagrams is that they are all interpreted slightly differently. You can't assume that because you know how to use one sort of phase diagram that you can treat others exactly the same. If you already know about other phase diagrams, look at this one completely afresh.</p>
</div>

<div class="text-block">
<p>Suppose you have a mixture of 67% lead and 33% tin. That's the mixture from the first cooling curve plotted above. Suppose it is at a temperature of 300°C.</p>
<p>That corresponds to a set of conditions in the area of the phase diagram labelled as molten tin and lead.</p>
</div>

<div class="image-container"><img src="eutdiagsnpb3.gif"></div>

<div class="text-block">
<p>Now consider what happens if you cool that mixture. Eventually the temperature will drop to a point where it crosses the line into the next region of the diagram.</p>
<p>At that point, the mixture will start to produce some solid lead – in other words, the lead (but not the tin) starts to freeze. That happens at a temperature of about 250°C.</p>
</div>

<div class="image-container"><img src="eutdiagsnpb4.gif"></div>

<div class="text-block">
<p>Now there is a bit of a problem, because you might come across two different ways of explaining what happens next. We'll look at both.</p>
</div>

<h4>Thinking about changes in the composition of the liquid</h4>

<div class="text-block">
<p>When the first of the lead freezes, the composition of the remaining liquid changes. It obviously becomes proportionally richer in tin. That lowers the freezing point of the lead a bit more, and so the next bit of lead freezes at a slightly lower temperature – leaving a liquid still richer in tin.</p>
<p>This process goes on. The liquid gets richer and richer in tin, and the temperature needed to freeze the next lot of lead continues to fall. The set of conditions of temperature and composition of the liquid essentially moves down the curve – until it reaches the eutectic point.</p>
</div>

<div class="image-container"><img src="eutdiagsnpb5.gif"></div>

<div class="text-block">
<p>Once it has reached the eutectic point, if the temperature continues to fall, you obviously just move into the region of a mixture of solid lead and solid tin – in other words, all the remaining liquid freezes.</p>
<p>If you haven't come across this way of looking at it before, then please don't bother to learn it now! The second way is more in line with how we look at other phase diagrams, and actually needs less thinking about.</p>
</div>

<h4>Thinking about the composition of the system as a whole</h4>

<div class="text-block">
<p>We've seen that as the liquid gradually freezes, its composition changes. But if you look at the system as a whole, obviously the proportions of lead and tin remain constant – you aren't taking anything away or adding anything. All that is happening is that things are changing from liquids to solids.</p>
<p>So suppose we continue the cooling beyond the temperature that the first solid lead appears and the temperature drops to the point shown in the next diagram – a point clearly in the "solid lead and molten mixture" area.</p>
</div>

<div class="image-container"><img src="eutdiagsnpb6.gif"></div>

<div class="text-block">
<p>What would you see in the mixture? To find out, you draw a horizontal tie line through that point, and then look at the ends of it.</p>
</div>

<div class="image-container"><img src="eutdiagsnpb7.gif"></div>

<div class="text-block">
<p>At the left-hand end, you have 100% lead. That represents the solid lead that has frozen from the mixture. At the right-hand end, you have the composition of the liquid mixture. This is now much richer in tin than the whole system is – because obviously a fair bit of solid lead has separated out.</p>
<p>As the temperature continues to fall, the composition of the liquid mixture (as shown by the right-hand end of the tie line) will get closer and closer to the eutectic mixture.</p>
</div>

<div class="image-container"><img src="eutdiagsnpb8.gif"></div>

<div class="text-block">
<p>It will finally reach the eutectic composition when the temperature drops to the eutectic temperature – and the whole lot then freezes.</p>
<p>At a temperature lower than the eutectic temperature, you are obviously in the solid lead plus solid tin region. That's fairly obvious!</p>
<p>If you cooled a liquid mixture on the right-hand side of the phase diagram (to the right of the eutectic mixture), everything would work exactly the same except that solid tin would be formed instead of solid lead. If you have understood what has gone before, it isn't at all difficult to work out what happens.</p>
<p>Finally what happens if you cool a liquid mixture which has exactly the eutectic composition?</p>
<p>It simply stays as a liquid mixture until the temperature falls enough that it all solidifies. You never get into the awkward areas of the phase diagram.</p>
</div>

<h3>Tin-lead Mixtures as Solder</h3>

<div class="text-block">
<p>Traditionally, tin-lead mixtures have been used as solder, but these are being phased out because of health concerns over the lead. This is especially the case where the solder is used to join water pipes where the water is used for drinking. New non-lead solders have been developed as safer replacements.</p>
<p>Typical old-fashioned solders include:</p>
</div>

<ul>
<li>60% tin and 40% lead. This is close to the eutectic composition (62% tin and 38% lead), giving a low melting point. It will also melt and freeze cleanly over a very limited temperature range. This is useful for electrical work.</li>
<li>50% tin and 50% lead. This will melt and freeze over a wider range of temperatures. When it is molten it will start to freeze at about 220°C and finally solidify at the eutectic temperature of 183°C. That means that it stays workable for a useful amount of time. That's helpful if it is being used for plumbing joints.</li>
</ul>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-snpbeutectic.pdf" target="_blank">Questions on the tin and lead phase diagram</a>
<a href="../questions/a-snpbeutectic.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../phaseeqiamenu.html#top">To the phase equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>