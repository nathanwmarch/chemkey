<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Immiscible Liquids and Steam Distillation | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the background to the steam distillation of systems containing two immiscible liquids.">
<meta name="keywords" content="liquid, liquids, mixture, mixtures, immiscible, steam, distillation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Immiscible Liquids and Steam Distillation</h1>

<div class="text-block">
<p>This page looks at systems containing two immiscible liquids. Immiscible liquids are those which won't mix to give a single phase. Oil and water are examples of immiscible liquids – one floats on top of the other. It explains the background to steam distillation and looks at a simple way of carrying this out.</p>
</div>

<div class="note">
<p>Important: Read the page about <a href="vapourpress.html#top">vapour pressure</a> first (particularly the bit about vapour pressure and boiling point) unless you are reasonably confident about it.</p>
</div>

<h2>The Vapour Pressure of Mixtures of Immiscible Liquids</h2>

<div class="text-block">
<p>Obviously if you have two immiscible liquids in a closed flask and keep everything still, the vapour pressure you measure will simply be the vapour pressure of the one which is floating on top. There is no way that the bottom liquid can turn to vapour. The top one is sealing it in.</p>
<p>For the purposes of the rest of this topic, we always assume that the mixture is being stirred or agitated in some way so that the two liquids are broken up into drops. At any one time there will be drops of both liquids on the surface. That means that both of them contribute to the overall vapour pressure of the mixture.</p>
</div>

<div class="note">
<p>Note: I find it very hard to talk about "mixtures" of immiscible liquids, since "immiscible" means that they don't mix! However, using the word "systems" seems so awkward.</p>
</div>

<h3>Total Vapour Pressure of the Mixture</h3>

<div class="text-block">
<p>Assuming that the mixture is being agitated, then both of the liquids will be in equilibrium with their vapours. The total vapour pressure is then simply the sum of the individual vapour pressures:</p>
</div>

<div class="block-formula">
\text{total vapour pressure} = p^o_A + p^o_B
</div>

<div class="text-block">
<p> where p<sup>o</sup> refers to the saturated vapour pressure of the pure liquid.</p>
<p>Notice that this is independent of the amount of each sort of liquid present. All you need is enough of each so that both can exist in equilibrium with their vapour.</p>
<p>For example, phenylamine and water can be treated as if they were completely immiscible. (That isn't actually true, but they are near enough immiscible to be usable as an example.)</p>
<p>At 98°C, the saturated vapour pressures of the two pure liquids are:</p>
</div>


<table class="list-table">
<tbody><tr><td>phenylamine</td><td>7.07 kPa</td></tr>
<tr><td>water</td><td>94.30 kPa</td></tr>
</tbody></table>

<div class="text-block">
<p>The total vapour pressure of an agitated mixture would just be the sum of these – in other words, 101.37 kPa</p>
</div>

<h3>Boiling Point of the Mixture</h3>

<div class="text-block">
<p>Liquids boil when their vapour pressure becomes equal to the external pressure. Normal atmospheric pressure is 101.325 kPa.</p>
<p>Compare that with the figure we have just got for the total vapour pressure of a mixture of water and phenylamine at 98°C. Its total vapour pressure is fractionally higher than the normal external pressure.</p>
<p>This means that such a mixture would boil at a temperature just a shade less than 98°C – in other words lower than the boiling point of pure water (100°C) and much lower than the phenylamine (184°C).</p>
<p>Exactly the same sort of argument could be applied to any other mixture of immiscible liquids. I've chosen the phenylamine-water mixture just because I happen to have some figures for it!</p>
</div>

<h4>Important conclusion</h4>

<div class="text-block">
<p>Agitated mixtures of immiscible liquids will boil at a temperature lower than the boiling point of either of the pure liquids. Their combined vapour pressures are bound to reach the external pressure before the vapour pressure of either of the individual components get there.</p>
</div>

<h2>Steam Distillation</h2>

<div class="text-block">
<p>Notice that in the presence of water, phenylamine (or any other liquid which is immiscible with water) boils well below its normal boiling point. This has an important advantage in separating molecules like this from mixtures.</p>
<p>Normal distillation of these liquids would need quite high temperatures. On the whole these tend to be big molecules we are talking about. Quite a lot of molecules of this sort will be broken up by heating at high temperatures. Distilling them in the presence of water avoids this by keeping the temperature low.</p>
<p>That's what steam distillation achieves.</p>
</div>

<h3>Carrying Out Steam Distillation</h3>

<div class="text-block">
<p>We will carry on with the phenylamine example for now. During the preparation of phenylamine it is produced as a part of a mixture containing a solution of all sorts of inorganic compounds. It is removed from this by steam distillation.</p>
<p>Steam is blown through the mixture and the water and phenylamine turn to vapour. This vapour can be condensed and collected.</p>
</div>

<div class="image-container"><img src="steam.gif"></div>

<div class="text-block">
<p>The steam can be generated by heating water in another flask (or something similar).</p>
<p>As the hot steam passes through the mixture it condenses, releasing heat. This will be enough to boil the mixture of water and phenylamine at 98°C provided the volume of the mixture isn't too great. For large volumes, it is better to heat the flask as well to avoid having to condense too much steam and increase the volume of liquid in the flask too much.</p>
<p>The condensed vapour will consist of both water and phenylamine. If these were truly immiscible, they would form two layers which could be separated using a separating funnel. In fact, the phenylamine has a slight solubility in water and various other techniques have to be used in this particular case to get the maximum yield of phenylamine. These aren't relevant to this topic.</p>
</div>

<h3>Some Other Applications of Steam Distillation</h3>

<div class="text-block">
<p>Steam distillation can be used to extract some natural products – for example, to extract eucalyptus oil from eucalyptus, citrus oils from lemon or orange peel, and to extract oils used in perfumes from various plant materials.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-immiscible.pdf" target="_blank">Questions on immiscible liquids and steam distillation</a>
<a href="../questions/a-immiscible.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../phaseeqiamenu.html#top">To the phase equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>