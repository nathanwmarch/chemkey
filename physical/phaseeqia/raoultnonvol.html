<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Raoult's Law and Non-volatile Solutes | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains Raoult's Law and how it applies to solutions containing non-volatile solutes like salt. Shows how the lowering of vapour pressure affects the boiling point and freezing point of the solvent.">
<meta name="keywords" content="equilibrium, equilibria, phase, diagram, solid, liquid, vapour, pressure, boiling, freezing, point, lowering, depression, elevation, raoult, raoult's, law, solvent, solute, non-volatile, salt, sodium chloride">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Raoult's Law and Non-volatile Solutes</h1>

<div class="text-block">
<p>This page deals with Raoult's Law and how it applies to solutions in which the solute is non-volatile – for example, a solution of salt in water. A non-volatile solute (the salt, for example) hasn't got any tendency to form a vapour at the temperature of the solution.</p>
<p>It goes on to explain how the resulting lowering of vapour pressure affects the boiling point and freezing point of the solution.</p>
</div>

<div class="note">
<p>Important: If you haven't already read the page about <a href="vapourpress.html#top">saturated vapour pressure</a>, you should follow this link before you go on.</p>
</div>

<h2>Raoult's Law</h2>

<div class="text-block">
<p>There are several ways of stating Raoult's Law, and you tend to use slightly different versions depending on the situation you are talking about. You can use the simplified definition in the box below in the case of a single volatile liquid (the solvent) and a non-volatile solute.</p>
</div>

<div class="definition">
<p>The vapour pressure of a solution of a non-volatile solute is equal to the vapour pressure of the pure solvent at that temperature multiplied by its mole fraction.</p>
</div>

<div class="text-block">
<p>In equation form, this reads:</p>
</div>

<div class="block-formula">
p = x_{solv} \times P^o_{solv}
</div>

<div class="text-block">
<p>In this equation, P<sup>o</sup> is the vapour pressure of the pure solvent at a particular temperature.</p>
<p>x<sub>solv</sub> is the mole fraction of the solvent. That is exactly what it says it is – the fraction of the total number of moles present which is solvent.</p>
<p>You calculate this using:</p>
</div>

<div class="block-formula">
x_{solv} = \frac{\text{moles of solvent}}{\text{total number of moles}}
</div>

<div class="text-block">
<p>Suppose you had a solution containing 10 moles of water and 0.1 moles of sugar. The total number of moles is therefore 10.1</p>
<p>The mole fraction of the water is:</p>
</div>

<div class="block-formula">
\begin{aligned}
x_{water} &= \frac{10}{10.1} \\
{} &= 0.99
\end{aligned}
</div>

<h3>A Simple Explanation of Why Raoult's Law Works</h3>

<div class="text-block">
<p>There are two ways of explaining why Raoult's Law works – a simple visual way, and a more sophisticated way based on entropy. Because of the level I am aiming at, I'm just going to look at the simple way.</p>
<p>Remember that saturated vapour pressure is what you get when a liquid is in a sealed container. An equilibrium is set up where the number of particles breaking away from the surface is exactly the same as the number sticking on to the surface again.</p>
</div>

<div class="image-container"><img src="sealed3.gif"></div>

<div class="text-block">
<p>Now suppose you added enough solute so that the solvent molecules only occupied 50% of the surface of the solution.</p>
</div>

<div class="image-container"><img src="sealed4.gif"></div>

<div class="text-block">
<p>A certain fraction of the solvent molecules will have enough energy to escape from the surface (say, 1 in 1000 or 1 in a million, or whatever). If you reduce the number of solvent molecules on the surface, you are going to reduce the number which can escape in any given time.</p>
<p>But it won't make any difference to the ability of molecules in the vapour to stick to the surface again. If a solvent molecule in the vapour hits a bit of surface occupied by the solute particles, it may well stick. There are obviously attractions between solvent and solute otherwise you wouldn't have a solution in the first place.</p>
<p>The net effect of this is that when equilibrium is established, there will be fewer solvent molecules in the vapour phase – it is less likely that they are going to break away, but there isn't any problem about them returning.</p>
<p>If there are fewer particles in the vapour at equilibrium, the saturated vapour pressure is lower.</p>
</div>

<h3>Limitations of Raoult's Law</h3>

<div class="text-block">
<p>Raoult's Law only works for ideal solutions. An ideal solution is defined as one which obeys Raoult's Law.</p>
</div>

<h4>Features of an ideal solution</h4>

<div class="text-block">
<p>In practice, there's no such thing! However, very dilute solutions obey Raoult's Law to a reasonable approximation. The solution in the last diagram wouldn't actually obey Raoult's Law – it is far too concentrated. I had to draw it that concentrated to make the point more clearly.</p>
<p>In an ideal solution, it takes exactly the same amount of energy for a solvent molecule to break away from the surface of the solution as it did in the pure solvent. The forces of attraction between solvent and solute are exactly the same as between the original solvent molecules – not a very likely event!</p>
</div>

<div class="image-container"><img src="equalforces.gif"></div>

<div class="text-block">
<p>Suppose that in the pure solvent, 1 in 1000 molecules had enough energy to overcome the intermolecular forces and break away from the surface in any given time. In an ideal solution, that would still be exactly the same proportion.</p>
<p>Fewer would, of course, break away because there are now fewer solvent molecules on the surface – but of those that are on the surface, the same proportion still break away.</p>
<p>If there were strong solvent-solute attractions, this proportion may be reduced to 1 in 2000, or 1 in 5000 or whatever.</p>
<p>In any real solution of, say, a salt in water, there are strong attractions between the water molecules and the ions.That would tend to slow down the loss of water molecules from the surface. However, if the solution is sufficiently dilute, there will be good-sized regions on the surface where you still have water molecules on their own. The solution will then approach ideal behaviour.</p>
</div>

<h4>The nature of the solute</h4>

<div class="text-block">
<p>There is another thing that you have to be careful of if you are going to do any calculations on Raoult's Law (beyond the scope of this site). You may have noticed in the little calculation about mole fraction further up the page, that I used sugar as a solute rather than salt. There was a good reason for that!</p>
<p>What matters isn't actually the number of moles of substance that you put into the solution, but the number of moles of particles formed. For each mole of sodium chloride dissolved, you get 1 mole of sodium ions and 1 mole of chloride ions – in other words, you get twice the number of moles of particles as of original salt.</p>
</div>

<div class="block-formula">
\underbrace{\text{Na}^+\text{Cl}^-_{(s)}}_{\text{\color{#467abf}{1 mole of solid salt}}} \longrightarrow \underbrace{\text{Na}^+_{(aq)} + \text{Cl}^-_{(aq)}}_{\text{\color{#00CC99}{2 moles of ions in solution}}}
</div>

<div class="text-block">
<p>So, if you added 0.1 moles of sodium chloride, there would actually be 0.2 moles of particles in the solution – and that's the figure you would have to use in the mole fraction calculation.</p>
<p>Unless you think carefully about it, Raoult's Law only works for solutes which don't change their nature when they dissolve. For example, they mustn't ionise or associate (in other words, if you put in substance A, it mustn't form A<sub>2</sub> in solution).</p>
<p>If it does either of these things, you have to treat Raoult's law with great care.</p>
</div>

<div class="note">
<p>Note: This isn't a problem you are likely to have to worry about if you are a UK A-level student. Just be aware that the problem exists.</p>
</div>

<h2>Raoult's Law and Melting and Boiling Points</h2>

<div class="text-block">
<p>The effect of Raoult's Law is that the saturated vapour pressure of a solution is going to be lower than that of the pure solvent at any particular temperature. That has important effects on the phase diagram of the solvent.</p>
<p>The next diagram shows the phase diagram for pure water in the region around its normal melting and boiling points. The 1 atmosphere line shows the conditions for measuring the normal melting and boiling points.</p>
</div>

<div class="image-container"><img src="pdh2opart.gif"></div>

<div class="note">
<p>Note: In common with most phase diagrams, this is drawn highly distorted in order to show more clearly what is going on.</p>
<p>If you haven't already read my page about <a href="phasediags.html#top">phase diagrams</a> for pure substances, you should follow this link before you go on to make proper sense of what comes next.</p>
</div>

<div class="text-block">
<p>The line separating the liquid and vapour regions is the set of conditions where liquid and vapour are in equilibrium.</p>
<p>It can be thought of as the effect of pressure on the boiling point of the water, but it is also the curve showing the effect of temperature on the saturated vapour pressure of the water. These two ways of looking at the same line are discussed briefly in a note about half-way down the page about phase diagrams (follow the last link above).</p>
<p>If you draw the saturated vapour pressure curve for a solution of a non-volatile solute in water, it will always be lower than the curve for the pure water.</p>
</div>

<div class="image-container"><img src="pdsolution1.gif"></div>

<div class="note">
<p>Note: The curves for the pure water and for the solution are often drawn parallel to each other. That has got to be wrong!</p>
<p>Suppose you have a solution where the mole fraction of the water is 0.99 and the vapour pressure of the pure water at that temperature is 100 kPa. The vapour pressure of the solution will be 99 kPa – a fall of 1 kPa. At a lower temperature, where the vapour pressure of the pure water is 10 kPa, the fall will only be 0.1 kPa. For the curves to be parallel the falls would have to be the same over the whole temperature range. They aren't!</p>
</div>

<div class="text-block">
<p>If you look closely at the last diagram, you will see that the point at which the liquid-vapour equilibrium curve meets the solid-vapour curve has moved. That point is the triple point of the system – a unique set of temperature and pressure conditions at which it is possible to get solid, liquid and vapour all in equilibrium with each other at the same time.</p>
<p>Since the triple point has solid-liquid equilibrium present (amongst other equilibria), it is also a melting point of the system – although not the normal melting point because the pressure isn't 1 atmosphere.</p>
<p>That must mean that the phase diagram needs a new melting point line (a solid-liquid equilibrium line) passing through the new triple point. That is shown in the next diagram.</p>
</div>

<div class="image-container"><img src="pdsolution2.gif"></div>

<div class="text-block">
<p>Now we are finally in a position to see what effect a non-volatile solute has on the melting and freezing points of the solution. Look at what happens when you draw in the 1 atmosphere pressure line which lets you measure the melting and boiling points. The diagram also includes the melting and boiling points of the pure water from the original phase diagram for pure water (black lines).</p>
</div>

<div class="image-container"><img src="pdsolution3.gif"></div>

<div class="text-block">
<p>Because of the changes to the phase diagram, you can see that:</p>
</div>

<ul>
<li>the boiling point of the solvent in a solution is higher than that of the pure solvent;</li>
<li>the freezing point (melting point) of the solvent in a solution is lower than that of the pure solvent.</li>
</ul>

<div class="text-block">
<p>We have looked at this with water as the solvent, but using a different solvent would make no difference to the argument or the conclusions.</p>
<p>The only difference is in the slope of the solid-liquid equilibrium lines. For most solvents, these slope forwards whereas the water line slopes backwards. You could prove to yourself that that doesn't affect what we have been looking at by re-drawing all these diagrams with the slope of that particular line changed.</p>
<p>You will find it makes no difference whatsoever.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-raoultnonvol.pdf" target="_blank">Questions on Raoult's Law and non-volatile solutes</a>
<a href="../questions/a-raoultnonvol.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../phaseeqiamenu.html#top">To the phase equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>