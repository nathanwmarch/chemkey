<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Solid-liquid Phase Diagrams: Salt Solution | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Shows how the phase diagram for mixtures of salt and water is built up, and how this leads to a eutectic mixture of salt and water. Includes a brief summary of solubility curves.">
<meta name="keywords" content="phase, diagram, solid, liquid, freezing, melting, point, lowering, salt, water, ice, eutectic, solubility curve">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Solid-liquid Phase Diagrams: Salt Solution</h1>

<div class="text-block">
<p>This page looks at the phase diagram for mixtures of salt and water – how the diagram is built up, and how to interpret it. It includes a brief discussion of solubility curves.</p>
</div>

<div class="note">
<p>Important: This page is only really designed to be an introduction to the topic suitable for courses for 16 -18 year olds, such as UK A-level chemistry.</p>
<p>If you have come to this page straight from a search engine, it would pay you to read the page about the <a href="snpb.html#top">tin-lead phase diagram</a> first. This is a rather easier system to talk about and introduces some of the ideas that you will need.</p>
</div>

<h2>Solubility Curves</h2>

<h3>Two Typical Solubility Curves</h3>

<div class="text-block">
<p>A solubility curve shows how the solubility of a salt like sodium chloride or potassium nitrate varies with temperature.</p>
<p>The solubility is often (although not always) measured as the mass of salt which would saturate 100 grams of water at a particular temperature. A solution is saturated if it won't dissolve any more of the salt at that particular temperature – in the presence of crystals of the salt.</p>
</div>

<div class="note">
<p>Note: In the absence of some solid crystals, you can occasionally get super-saturated solutions. These solutions are usually unstable and the excess material crystallises out if the crystals have something to form around. This can be just one tiny extra added crystal, or a scratch on the glass of the container.</p>
</div>

<div class="text-block">
<p>For most substances, solubility increases with temperature. For some (like potassium nitrate), the increase is quite fast. For others (like sodium chloride), there is only a small change in solubility with temperature.</p>
</div>

<div class="image-container"><img src="solcurve1.gif"></div>

<div class="note">
<p>Note: These solubility curves are just sketches. The values are approximately right, but you shouldn't use the curves to try to get any exact data.</p>
</div>

<h3>Interpreting a Solubility Curve</h3>

<div class="text-block">
<p>Obviously, a solubility curve shows you the solubility of a substance at a particular temperature. For phase diagram purposes, one important way of looking at this is to examine what happens if you decrease the temperature of a solution with some given concentration.</p>
<p>For example, suppose you have a near-boiling solution of potassium nitrate in water. We'll take a solution containing 100 g of potassium nitrate and 100 g of water. Now let the solution cool.</p>
</div>

<div class="image-container"><img src="solcurve2.gif"></div>

<div class="text-block">
<p>At all temperatures above that marked on the graph (about 57°C), 100 g of water will dissolve more than 100 g of potassium nitrate. All the potassium nitrate will stay in solution.</p>
<p>At 57°C, you hit the solubility curve. This is the temperature at which 100 g of water will dissolve 100 g of potassium nitrate to give a saturated solution.</p>
<p>If the temperature falls even the tiniest bit below 57°C, the water will no longer dissolve as much potassium nitrate – and so some crystallises out. The lower the temperature falls, the more potassium nitrate crystallises, because the water will dissolve less and less of it.</p>
<p>You can think of this as a simple phase diagram. If you have a mixture of 100 g of potassium nitrate and 100 g of water and the temperature is above 57°C, you have a single phase – a solution of potassium nitrate.</p>
<p>If the temperature is below 57°C for this mixture, you will have a mixture of two phases – the solution and some solid potassium nitrate.</p>
<p>The solubility curve represents the boundary between these two different situations.</p>
<p>We'll look now at the phase diagram for sodium chloride solution in some detail.</p>
</div>

<h2>The Phase Diagram for Sodium Chloride Solution</h2>

<h3>What the Phase Diagram Looks Like</h3>

<div class="image-container"><img src="salteutect1.gif"></div>

<div class="text-block">
<p>We'll talk through what everything means in detail, but first of all notice two things compared with the similar tin-lead phase diagram I hope you have already read about.</p>
<p>First we are only looking at a very restricted temperature range. The top of this particular diagram is only about 50°C – although it could go higher than that.</p>
<p>Secondly, all the action takes place in the left-hand side of the diagram. That's why a lot of the horizontal scale is missing (represented by the broken zigzag in the scale).</p>
</div>

<h4>The labelled areas in the phase diagram</h4>

<div class="text-block">
<p>These areas all show what you would see if you had a particular mixture of salt and water at a given temperature.</p>
<p>For example, if the temperature was below -21.1°C, you would always see a mixture of solid salt and ice. There would never be any liquid whatever proportions of salt and water you had.</p>
<p>At temperatures above this, what you would see would depend on where your particular set of conditions fell in the diagram. You just need to notice what area the conditions fall into.</p>
</div>

<div class="image-container"><img src="salteutect2.gif"></div>

<h4>What the lines mean</h4>

<div class="text-block">
<p>Let's take the lines one at a time. The first one to look at is the one in bold green in the next diagram.</p>
</div>

<div class="image-container"><img src="salteutect3.gif"></div>

<div class="text-block">
<p>This line represents the effect of increasing amounts of salt on the freezing point of water. Up to the point where there is 23.3% of salt in the mixture, the more salt the lower the freezing point of the water.</p>
</div>

<div class="note">
<p>Note: If you are interested (although it isn't essential for understanding the current page), you can find out why salt lowers the freezing point of water by reading the page about <a href="raoultnonvol.html#top">Raoult's Law and non-volatile solutes</a>.</p>
</div>

<div class="text-block">
<p>The second line is actually a solubility curve for salt in water – although it doesn't look quite like the one we described earlier on this page.</p>
</div>

<div class="image-container"><img src="salteutect4.gif"></div>

<div class="text-block">
<p>The reason that it looks different is because the axes have been reversed. On a normal solubility curve, temperature is the horizontal axis and the vertical axis shows the solubility in grams per 100 g of water – a measure of concentration.</p>
<p>This time the measure of concentration is the horizontal axis and temperature the vertical one.</p>
<p>If you have already read about the tin-lead system, you might think it strange to include a solubility curve in the phase diagram. In the tin-lead system, both of the sloping lines showed the effect of one of the components on the freezing point of the other.</p>
<p>But if you think what this means, it isn't so odd.</p>
<p>The freezing point is the temperature at which crystals start to appear when you cool a liquid mixture.</p>
<p>In the case of a salt solution with concentrations of salt greater than 23.3%, the solubility curve shows the temperature at which crystals of salt will appear when you cool a solution of a given concentration. If you aren't clear about that, go back and re-read the beginning of this page.</p>
<p>We'll have more to say about this when we describe how to use the phase diagram in a short while.</p>
<p>There is also a hidden difference between this line and the corresponding line on the tin-lead diagram. In that case, both of the sloping lines eventually reached the the left-hand or the right-hand axis. It was possible to talk about the case where you had 100% lead or 100% tin.</p>
<p>In this case it is impossible to get to 100% salt. This line eventually comes to an end. At 1 atmosphere pressure it won't get very far above 100°C because the water will boil and you won't have a solution any more.</p>
</div>

<div class="note">
<p>Note: The salt solution will boil at a temperature greater than 100°C (depending on its concentration), because adding a non-volatile solute to a solvent increases its boiling point. This is discussed on another page about <a href="raoultnonvol.html#top">Raoult's Law and non-volatile solutes</a>.</p>
<p>It isn't essential to understanding the rest of this page.</p>
</div>

<div class="text-block">
<p>Even if you raise the pressure, the maximum temperature you could achieve would be 374°C – the critical temperature of the water. Water doesn't exist as a liquid above this temperature. To get to the right-hand side of the graph where you have 100% salt, you would have to get the temperature up to 801°C – the melting point of the salt.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about the critical temperature of water, you could read the page about the <a href="phasediags.html#top">phase diagrams of pure substances</a>. Again, it isn't essential to understanding the rest of this page. You just need to be aware that the line we are discussing doesn't get all the way to the right-hand axis.</p>
</div>

<div class="text-block">
<p>After all this hassle, the final line is easy!</p>
</div>

<div class="image-container"><img src="salteutect5.gif"></div>

<div class="text-block">
<p>This line is simply drawn across at the lowest temperature at which a mixture of salt and water can contain any liquid. This is known as the eutectic temperature. The particular mixture of salt and water (containing 23.3% salt) which freezes at this temperature is known as a eutectic mixture.</p>
</div>

<div class="note">
<p>Note: Eutectic mixtures and the eutectic temperature are discussed in more detail on the page about the <a href="snpb.html#top">tin-lead system</a> that I keep going on about!</p>
</div>

<h3>Using the Phase Diagram</h3>

<div class="text-block">
<p>The phase diagram is used to find out what happens if you cool salt solution of a particular concentration. We need to look at three separate cases.</p>
</div>

<h4>Cooling salt solution with the eutectic composition</h4>

<div class="text-block">
<p>This is the easy one!</p>
</div>

<div class="image-container"><img src="salteutect6.gif"></div>

<div class="text-block">
<p>Nothing happens until you get down to the eutectic temperature. At that point, you will start to get both ice crystals and salt crystals forming.</p>
<p>If you keep cooling it, you will obviously end up with a solid mixture of ice and salt. You are moving straight from the "salt solution" area of the phase diagram into the "solid salt + ice" area.</p>
</div>

<h4>Cooling salt solution more dilute than the eutectic composition</h4>

<div class="image-container"><img src="salteutect7.gif"></div>

<div class="text-block">
<p>When the temperature drops enough so that it reaches the boundary between the two areas of the phase diagram, ice crystals start to form – in other words, the solution starts to freeze.</p>
</div>

<div class="note">
<p>Note: There are two ways of looking at what happens when you go on cooling the mixture. On the page about the tin-lead phase diagram, I have looked at both of these ways in detail. On the present page, though, I have decided to stick with the one which I feel is the simpler to understand. This page is already complicated enough and describing both ways of looking at it is going to make it too confusing – especially since I would have to do it again further down the page.</p>
<p>If you want to use the other method (which considers changes in the composition of the liquid present) you should be able to work it out by reference to the <a href="snpb.html#top">tin-lead</a> page. Read the paragraph headed "Thinking about changes in the composition of the liquid" and just change the words tin and lead into salt and water!</p>
</div>

<div class="text-block">
<p>As the solution continues to cool, it moves down into the "ice + salt solution" region.</p>
<p>Obviously, the composition of the solution has changed because it contains less water – some of it has frozen to give ice. But the composition of the system as a whole is still the same, and so we continue down the same line.</p>
<p>To find out what is actually in the mixture, you draw a horizontal tie line through the point showing the temperature you have got to, and look at what it hits at either end.</p>
</div>

<div class="image-container"><img src="salteutect8.gif"></div>

<div class="text-block">
<p>On the left it hits the vertical axis showing 100% water – in this case, that's the pure ice crystals.</p>
<p>On the other end, it hits the sloping line – this tells you the composition of the remaining salt solution.</p>
<p>As the mixture continues to cool, it will eventually reach the eutectic temperature, and at this point all of the rest of the solution will turn to solid – a mixture of ice and salt crystals.</p>
</div>

<h4>Cooling salt solution more concentrated than the eutectic composition</h4>

<div class="text-block">
<p>The phase diagram is interpreted in just the same way – except that this time, salt crystals are formed at first rather than ice crystals.</p>
<p>In the next diagram, the first salt crystals will form when the temperature hits the boundary line. As the temperature continues to drop into the "solid salt + salt solution" area, more and more salt will crystallise out.</p>
<p>To find out exactly what is present at any temperature, you can again draw a tie line and look at what is at either end.</p>
</div>

<div class="image-container"><img src="salteutect9.gif"></div>

<div class="text-block">
<p>The diagram shows the tie line when the temperature has dropped to some random place in the "solid salt + salt solution" area. Again, by looking at the ends of this tie line, you can see that the mixture contains solid salt (the 100% salt at the right-hand end of the line) and a solution whose concentration can be found by looking at the left-hand end of the line.</p>
<p>As the temperature continues to fall, you will eventually reach the eutectic temperature, when everything left – salt and water – will turn to solid giving you a mixture of solid salt and ice.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-nacleutectic.pdf" target="_blank">Questions on the salt solution phase diagram</a>
<a href="../questions/a-nacleutectic.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../phaseeqiamenu.html#top">To the phase equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>