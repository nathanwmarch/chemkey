<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Non-ideal Mixtures of Liquids | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the phase diagrams for non-ideal mixtures of liquids, introducing the idea of azeotropic mixtures. Looks at how this affects the fractional distillation of such mixtures.">
<meta name="keywords" content="phase, diagram, liquid, liquids, non-ideal, mixture, mixtures, composition, boiling, point, fractional, fraction, distil, distillation, azeotrope, azeotropic">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Non-ideal Mixtures of Liquids</h1>

<div class="text-block">
<p>This page looks at the phase diagrams for non-ideal mixtures of liquids, and introduces the idea of an azeotropic mixture (also known as an azeotrope or constant boiling mixture). It goes on to explain how this complicates the process of fractionally distilling such a mixture.</p>
<p>This is the final page in a sequence of three pages.</p>
</div>

<div class="note">
<p>Important: If you have come straight to this page from a search engine, you won't make much sense of it unless you first read the previous two pages in the sequence starting with the one about <a href="idealpd.html#top">phase diagrams for ideal mixtures</a>.</p>
</div>

<h2>Vapour Pressure / Composition Diagrams for Non-ideal Mixtures</h2>

<div class="text-block">
<p>You will remember that, because of Raoult's Law, if you plot the vapour pressure of an ideal mixture of two liquids against their composition, you get a straight line graph like this:</p>
</div>

<div class="image-container"><img src="vpcomp4.gif"></div>

<div class="text-block">
<p>In this case, pure A has the higher vapour pressure and so is the more volatile component.</p>
</div>

<div class="note">
<p>Note: In the previous pages in this series, the diagrams were drawn with B being the more volatile. If this change worries you, then you haven't really understood the bit about vapour pressure / composition diagrams on the page about <a href="idealpd.html#top">phase diagrams for ideal mixtures</a>.</p>
<p>It is essential that you are happy with these diagrams whichever way they are drawn. It is really dangerous to get used to always drawing them one way around. You may meet them with all sorts of variations, and so you must understand what is going on.</p>
<p>I've made the change on this page partly to make the two main examples slightly more logical, but also so that you can find out whether this does bother you and then do something to sort it out!</p>
</div>

<div class="text-block">
<p>Raoult's Law only works for ideal mixtures. In these, the forces between the particles in the mixture are exactly the same as those in the pure liquids. The tendency for the particles to escape is the same in the mixture and in the pure liquids.</p>
<p>That's not true in non-ideal mixtures.</p>
</div>

<h3>Positive Deviations From Raoult's Law</h3>

<div class="text-block">
<p>In mixtures showing a positive deviation from Raoult's Law, the vapour pressure of the mixture is always higher than you would expect from an ideal mixture.</p>
<p>The deviation can be small – in which case, the straight line in the last graph turns into a slight curve.</p>
</div>

<div class="image-container"><img src="vpcomp5.gif"></div>

<div class="text-block">
<p>Notice that the highest vapour pressure anywhere is still the vapour pressure of pure A. Cases like this, where the deviation is small, behave just like ideal mixtures as far as distillation is concerned, and we don't need to say anything more about them.</p>
<p>But some liquid mixtures have very large positive deviations from Raoult's Law, and in these cases, the curve becomes very distorted.</p>
</div>

<div class="image-container"><img src="vpcomp6.gif"></div>

<div class="text-block">
<p>Notice that mixtures over a range of compositions have higher vapour pressures than either pure liquid. The maximum vapour pressure is no longer that of one of the pure liquids. This has important consequences when we look at boiling points and distillation further down the page.</p>
</div>

<h4>Explaining the deviations</h4>

<div class="text-block">
<p>The fact that the vapour pressure is higher than ideal in these mixtures means that molecules are breaking away more easily than they do in the pure liquids.</p>
<p>That is because the intermolecular forces between molecules of A and B are less than they are in the pure liquids.</p>
<p>You can see this when you mix the liquids. Less heat is evolved when the new attractions are set up than was absorbed to break the original ones. Heat will therefore be absorbed when the liquids mix. The enthalpy change of mixing is endothermic.</p>
<p>The classic example of a mixture of this kind is ethanol and water. This produces a highly distorted curve with a maximum vapour pressure for a mixture containing 95.6% of ethanol by mass.</p>
</div>

<div class="note">
<p>Note: After 40-odd years of involvement with chemistry at this level, I've just noticed for the first time that when we start talking about this we suddenly switch the description for the composition from "mole fractions" to "percentage by mass". How very silly! A fairly simple sum will show you that 95.6% ethanol by mass is equivalent to a mole fraction of 0.895.</p>
<p>For simplicity, and to be consistent with what other sources tend to do, I shall relabel my composition axis as "percentage by mass" when I start drawing boiling point / composition diagrams further down this page. It won't affect the argument in any way, but will keep my numbers consistent with those you may have from elsewhere.</p>
</div>

<h3>Negative Deviations From Raoult's Law</h3>

<div class="text-block">
<p>In exactly the same way, you can have mixtures with vapour pressures which are less than would be expected by Raoult's Law. In some cases, the deviations are small, but in others they are much greater giving a minimum value for vapour pressure lower than that of either pure component.</p>
</div>

<div class="image-container"><img src="vpcomp7.gif"></div>

<h4>Explaining the deviations</h4>

<div class="text-block">
<p>These are cases where the molecules break away from the mixture less easily than they do from the pure liquids. New stronger forces must exist in the mixture than in the original liquids.</p>
<p>You can recognise this happening because heat is evolved when you mix the liquids – more heat is given out when the new stronger bonds are made than was used in breaking the original weaker ones.</p>
<p>Many (although not all) examples of this involve actual reaction between the two liquids. The example of a major negative deviation that we are going to look at is a mixture of nitric acid and water. These two covalent molecules react to give hydroxonium ions and nitrate ions.</p>
</div>

<div class="block-formula">
\text{H}_2\text{O}_{(l)} + \text{HNO}_{3(l)} \xrightleftharpoons{} \text{H}_3\text{O}^+_{(aq)} + {\text{NO}_3}^-_{(aq)}
</div>

<div class="text-block">
<p>You now have strong ionic attractions involved.</p>
</div>

<h2>Boiling Point / Composition Diagrams for Non-ideal Mixtures</h2>

<h3>A Large Positive Deviation From Raoult's Law: Ethanol and Water Mixtures</h3>

<div class="text-block">
<p>If you look back up the page, you will remember that a large positive deviation from Raoult's Law produces a vapour pressure curve with a maximum value at some composition other than pure A or B.</p>
<p>If a mixture has a high vapour pressure it means that it will have a low boiling point. The molecules are escaping easily and you won't have to heat the mixture much to overcome the intermolecular attractions completely.</p>
<p>The implication of this is that the boiling point / composition curve will have a minimum value lower than the boiling points of either A or B.</p>
<p>In the case of mixtures of ethanol and water, this minimum occurs with 95.6% by mass of ethanol in the mixture. The boiling point of this mixture is 78.2°C, compared with the boiling point of pure ethanol at 78.5°C, and water at 100°C.</p>
<p>You might think that this 0.3°C doesn't matter much, but it has huge implications for the separation of ethanol / water mixtures.</p>
<p>The next diagram shows the boiling point / composition curve for ethanol / water mixtures. I've also included on the same diagram a vapour composition curve in exactly the same way as we looked at on the previous pages about phase diagrams for ideal mixtures.</p>
</div>

<div class="image-container"><img src="bpcompn1.gif"></div>

<div class="note">
<p>Note: This diagram is drawn grossly distorted. Look, for example, at the temperature scale and the position of the 95.6% value on the composition scale. The shapes of the two separate areas between the curves are also exaggerated. This is to make it easier to see what is going on in what comes next. It doesn't affect the argument in any way.</p>
</div>

<h4>Using the diagram</h4>

<div class="text-block">
<p>Suppose you are going to distil a mixture of ethanol and water with composition C<sub>1</sub> as shown on the next diagram. It will boil at a temperature given by the liquid curve and produce a vapour with composition C<sub>2</sub>.</p>
</div>

<div class="image-container"><img src="bpcompn2.gif"></div>

<div class="text-block">
<p>When that vapour condenses it will, of course, still have the composition C<sub>2</sub>. If you reboil that, it will produce a new vapour with composition C<sub>3</sub>.</p>
</div>

<div class="image-container"><img src="bpcompn3.gif"></div>

<div class="text-block">
<p>You can see that if you carried on with this boiling-condensing-reboiling sequence, you would eventually end up with a vapour with a composition of 95.6% ethanol. If you condense that you obviously get a liquid with 95.6% ethanol.</p>
</div>

<h5>What happens if you reboil that liquid?</h5>

<div class="text-block">
<p>The liquid curve and the vapour curve meet at that point. The vapour produced will have that same composition of 95.6% ethanol. If you condense it again, it will still have that same composition.</p>
<p>You have hit a barrier. It is impossible to get pure ethanol by distiling any mixture of ethanol and water containing less than 95.6% of ethanoll.</p>
<p>This particular mixture of ethanol and water boils as if it were a pure liquid. It has a constant boiling point, and the vapour composition is exactly the same as the liquid.</p>
<p>It is known as a constant boiling mixture or an azeotropic mixture or an azeotrope.</p>
<p>The implications of this for fractional distillation of dilute solutions of ethanol are obvious. The liquid collected by condensing the vapour from the top of the fractionating column can't be pure ethanol. The best you can produce by simple fractional distillation is 95.6% ethanol.</p>
<p>What you can get (although it isn't very useful!) from the mixture is pure water. As ethanol rich vapour is given off from the liquid boiling in the distillation flask, it will eventually lose all the ethanol to leave just water.</p>
</div>

<div class="summary">
<p>Distilling a mixture of ethanol containing less than 95.6% of ethanol by mass lets you collect:</p>

<ul>
<li>a distillate containing 95.6% of ethanol in the collecting flask (provided you are careful with the temperature control, and the fractionating column is long enough);</li>
<li>pure water in the boiling flask.</li>
</ul>
</div>

<h5>What if you distil a mixture containing more than 95.6% ethanol?</h5>

<div class="text-block">
<p>Work it out for yourself using the phase diagram, and starting with a composition to the right of the azeotropic mixture. You should find that you get:</p>
</div>

<ul>
<li>a distillate containing 95.6% of ethanol in the collecting flask (provided you are careful with the temperature control, and the fractionating column is long enough);</li>
<li>pure ethanol in the boiling flask.</li>
</ul>

<h3>A Large Negative Deviation From Raoult's Law: Nitric Acid and Water Mixtures</h3>

<div class="text-block">
<p>Nitric acid and water form mixtures in which particles break away to form the vapour with much more difficulty than in either of the pure liquids. You can see this from the vapour pressure / composition curve discussed further up the page.</p>
<p>That means that mixtures of nitric acid and water can have boiling points higher than either of the pure liquids because it needs extra heat to break the stronger attractions in the mixture.</p>
<p>In the case of mixtures of nitric acid and water, there is a maximum boiling point of 120.5°C when the mixture contains 68% by mass of nitric acid. That compares with the boiling point of pure nitric acid at 86°C, and water at 100°C.</p>
<p>Notice the much bigger difference this time due to the presence of the new ionic interactions (see above).</p>
</div>

<div class="note">
<p>Note: Various sources quote the boiling point of pure nitric acid as 83, 84 or 86°C. I have no idea which of these is right! My choice of 86°C is entirely arbitrary. It doesn't affect any of the arguments in what follows.</p>
</div>

<h5>The phase diagram looks like this:</h5>

<div class="image-container"><img src="bpcompn4.gif"></div>

<div class="note">
<p>Warning: This is one case where you really do have to look at the scale being used for the composition. In mole fraction terms, 68% by mass of nitric acid corresponds to a mole fraction of only 0.378. The whole balance of the diagram will look different if you use the alternative scale.</p>
</div>

<h4>Using the diagram</h4>

<h5>Distilling dilute nitric acid</h5>

<div class="text-block">
<p>Start with a dilute solution of nitric acid with a composition of C<sub>1</sub> and trace through what happens.</p>
</div>

<div class="image-container"><img src="bpcompn5.gif"></div>

<div class="text-block">
<p>The vapour produced is richer in water than the original acid. If you condense the vapour and reboil it, the new vapour is even richer in water. Fractional distillation of dilute nitric acid will enable you to collect pure water from the top of the fractionating column.</p>
<p>As the acid loses water, it becomes more concentrated. Its concentration gradually increases until it gets to 68% by mass of nitric acid. At that point, the vapour produced has exactly the same concentration as the liquid, because the two curves meet.</p>
<p>You produce a constant boiling mixture (or azeotropic mixture or azeotrope). If you distil dilute nitric acid, that's what you will eventually be left with in the distillation flask. You can't produce pure nitric acid from the dilute acid by distilling it.</p>
</div>

<h5>Distilling nitric acid more concentrated than 68% by mass</h5>

<div class="text-block">
<p>This time you are starting with a concentration C<sub>2</sub> to the right of the azeotropic mixture.</p>
</div>

<div class="image-container"><img src="bpcompn6.gif"></div>

<div class="text-block">
<p>The vapour formed is richer in nitric acid. If you condense and reboil this, you will get a still richer vapour. If you continue to do this all the way up the fractionating column, you can get pure nitric acid out of the top.</p>
<p>As far as the liquid in the distillation flask is concerned, it is gradually losing nitric acid. Its concentration drifts down towards the azeotropic composition. Once it reaches that, there can't be any further change, because it then boils to give a vapour with the same composition as the liquid.</p>
<p>Distilling a nitric acid / water mixture containing more than 68% by mass of nitric acid gives you pure nitric acid from the top of the fractionating column and the azeotropic mixture left in the distillation flask.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-nonidealmix.pdf" target="_blank">Questions on non-ideal mixtures of liquids</a>
<a href="../questions/a-nonidealmix.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../phaseeqiamenu.html#top">To the phase equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>