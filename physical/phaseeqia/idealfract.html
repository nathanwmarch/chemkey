<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Fractional Distillation of Ideal Mixtures of Liquids | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how the fractional distillation of an ideal mixture of liquids is related to its phase diagram.">
<meta name="keywords" content="phase, diagram, liquid, liquids, ideal, mixture, mixtures, composition, boiling, point, fractional, fraction, distil, distillation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Fractional Distillation of Ideal Mixtures of Liquids</h1>

<div class="text-block">
<p>This page explains how the fractional distillation (both in the lab and industrially) of an ideal mixture of liquids relates to their phase diagram. This is the second page in a sequence of three pages.</p>
</div>

<div class="note">
<p>Important: If you have come straight to this page from a search engine and are looking for simple factual information about fractional distillation, this is probably not the page for you! It deals with the theory behind fractional distillation.</p>
<p>Again, if you have come straight to this page, you won't make much sense of it unless you first read the page about <a href="idealpd.html#top">phase diagrams for ideal mixtures</a>.</p>
</div>

<h2>Using the Phase Diagram</h2>

<div class="text-block">
<p>On the last page, we looked at how the phase diagram for an ideal mixture of two liquids was built up. I want to start by looking again at material from the last part of that page.</p>
<p>The next diagram is new – a modified version of diagrams from the previous page.</p>
</div>

<div class="image-container"><img src="bpcompi7.gif"></div>

<div class="text-block">
<p>If you boil a liquid mixture C<sub>1</sub>, you will get a vapour with composition C<sub>2</sub>, which you can condense to give a liquid of that same composition (the pale blue lines).</p>
<p>If you reboil that liquid C<sub>2</sub>, it will give a vapour with composition C<sub>3</sub>. Again you can condense that to give a liquid of the same new composition (the red lines).</p>
<p>Reboiling the liquid C<sub>3</sub> will give a vapour still richer in the more volatile component B (the green lines). You can see that if you were to do this once or twice more, you would be able to collect a liquid which was virtually pure B.</p>
<p>The secret of getting the more volatile component from a mixture of liquids is obviously to do a succession of boiling-condensing-reboiling operations.</p>
<p>It isn't quite so obvious how you get a sample of pure A out of this. That will become clearer in a while.</p>
</div>

<h2>Fractional Distillation in the Lab</h2>

<h3>The Apparatus</h3>

<div class="text-block">
<p>A typical lab fractional distillation would look like this:</p>
</div>

<div class="image-container"><img src="apparatus.gif"></div>

<h4>Some notes on the apparatus</h4>

<div class="text-block">
<p>The fractionating column is packed with glass beads (or something similar) to give the maximum possible surface area for vapour to condense on. You will see why this is important in a minute. Some fractionating columns have spikes of glass sticking out from the sides which serve the same purpose.</p>
<p>If you sketch this, make sure that you don't completely seal the apparatus. There has to be a vent in the system otherwise the pressure build-up when you heat it will blow the apparatus apart.</p>
<p>In some cases, where you are collecting a liquid with a very low boiling point, you may need to surround the collecting flask with a beaker of cold water or ice.</p>
<p>The mixture is heated at such a rate that the thermometer is at the temperature of the boiling point of the more volatile component. Notice that the thermometer bulb is placed exactly at the outlet from the fractionating column.</p>
</div>

<h3>Relating What Happens in the Fractionating Column to the Phase Diagram</h3>

<div class="text-block">
<p>Suppose you boil a mixture with composition C<sub>1</sub>.</p>
<p>The vapour over the top of the boiling liquid will be richer in the more volatile component, and will have the composition C<sub>2</sub>.</p>
</div>

<div class="image-container"><img src="bpcompi8.gif"></div>

<div class="text-block">
<p>That vapour now starts to travel up the fractionating column. Eventually it will reach a height in the column where the temperature is low enough that it will condense to give a liquid. The composition of that liquid will, of course, still be C<sub>2</sub>.</p>
</div>

<div class="note">
<p>Note: As you will see shortly, that is an oversimplification because "our" vapour will become mixed with other vapours generated by various other reboilings happening in the column. I can't see any way around this simplification!</p>
</div>

<div class="text-block">
<p>So what happens to that liquid now? It will start to trickle down the column where it will meet new hot vapour rising. That will cause the already condensed vapour to reboil.</p>
</div>

<div class="image-container"><img src="bpcompi9.gif"></div>

<div class="text-block">
<p>Some of the liquid of composition C<sub>2</sub> will boil to give a vapour of composition C<sub>3</sub>. Let's concentrate first on that new vapour and think about the unvaporised part of the liquid afterwards.</p>
</div>

<h4>The vapour</h4>

<div class="text-block">
<p>This new vapour will again move further up the fractionating column until it gets to a temperature where it can condense. Then the whole process repeats itself.</p>
<p>Each time the vapour condenses to a liquid, this liquid will start to trickle back down the column where it will be reboiled by up-coming hot vapour. Each time this happens the new vapour will be richer in the more volatile component.</p>
<p>The aim is to balance the temperature of the column so that by the time vapour reaches the top after huge numbers of condensing and reboiling operations, it consists only of the more volatile component – in this case, B.</p>
<p>Whether or not this is possible depends on the difference between the boiling points of the two liquids. The closer they are together, the longer the column has to be.</p>
</div>

<h4>The liquid</h4>

<div class="text-block">
<p>So what about the liquid left behind at each reboiling? Obviously, if the vapour is richer in the more volatile component, the liquid left behind must be getting richer in the other one.</p>
<p>As the condensed liquid trickles down the column constantly being reboiled by up-coming vapour, each reboiling makes it richer and richer in the less volatile component – in this case, A. By the time the liquid drips back into the flask, it will be very rich in A indeed.</p>
<p>So, over time, as B passes out of the top of the column into the condenser, the liquid in the flask will become richer in A. If you are very, very careful over temperature control, eventually you will have separated the mixture into B in the collecting flask and A in the original flask.</p>
</div>

<h5>Finally, what is the point of the packing in the column?</h5>

<div class="text-block">
<p>To make the boiling-condensing-reboiling process as effective as possible, it has to happen over and over again. By having a lot of surface area inside the column, you aim to have the maximum possible contact between the liquid trickling down and the hot vapour rising.</p>
<p>If you didn't have the packing, the liquid would all be on the sides of the condenser, while most of the vapour would be going up the middle and never come into contact with it.</p>
</div>

<h2>Fractional Distillation Industrially</h2>

<div class="text-block">
<p>There is no difference whatsoever in the theory involved. All that is different is what the fractionating column looks like. The diagram shows a simplified cross-section through a small part of a typical column.</p>
</div>

<div class="image-container"><img src="bubblecaps.gif"></div>

<div class="text-block">
<p>The column contains a number of trays that the liquid collects on as the vapour condenses. The up-coming hot vapour is forced through the liquid on the trays by passing through a number of bubble caps. This produces the maximum possible contact between the vapour and liquid. This all makes the boiling-condensing-reboiling process as efficient as possible.</p>
<p>The overflow pipes are simply a controlled way of letting liquid trickle down the column.</p>
<p>If you have a mixture of lots of liquids to separate (such as in petroleum fractionation), it is possible to tap off the liquids from some of the trays rather than just collecting what comes out of the top of the column. That leads to simpler mixtures such as gasoline, kerosene and so on.</p>
</div>

<div class="note">
<p>Note: At the moment, I don't have any intention to write specifically about petroleum (crude oil) distillation. If you do a Google search, you will already find far more than you could possibly read if you spent the whole of the next week at it!</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-fractdistil.pdf" target="_blank">Questions on the fractional distillation of ideal mixtures of liquids</a>
<a href="../questions/a-fractdistil.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="nonideal.html#top">Continue to the page about non-ideal liquid mixtures</a>
<a href="../phaseeqiamenu.html#top">To the phase equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>