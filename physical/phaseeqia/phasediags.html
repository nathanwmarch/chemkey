<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Phase Diagrams of Pure Substances | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of how to interpret the phase diagrams for pure substances including carbon dioxide and water.">
<meta name="keywords" content="equilibrium, equilibria, phase, diagram, solid, liquid, vapour, triple, point, critical, temperature, pressure, water, carbon dioxide">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Phase Diagrams of Pure Substances</h1>

<div class="text-block">
<p>This page explains how to interpret the phase diagrams for simple pure substances – including a look at the special cases of the phase diagrams of water and carbon dioxide. This is going to be a long page, because I have tried to do the whole thing as gently as possible.</p>
</div>

<h2>The Basic Phase Diagram</h2>

<h3>What is a Phase?</h3>

<div class="definition">
<p>Phase: a distinct and homogeneous form of matter (i.e. a solid, liquid, gas or mixture) with a distinct surface which separates it from other forms of matter.</p>
</div>

<div class="text-block">
<p>At its simplest, a phase can be just another term for solid, liquid or gas. If you have some ice floating in water, you have a solid phase present and a liquid phase. If there is air above the mixture, then that is another phase.</p>
<p>But the term can be used more generally than this. For example, oil floating on water also consists of two phases – in this case, two liquid phases. If the oil and water are contained in a bucket, then the solid bucket is yet another phase. In fact, there might be more than one solid phase if the handle is attached separately to the bucket rather than moulded as a part of the bucket.</p>
<p>You can recognise the presence of the different phases because there is an obvious boundary between them – a boundary between the solid ice and the liquid water, for example, or the boundary between the two liquids.</p>
</div>

<h3>Phase Diagrams</h3>

<div class="text-block">
<p>A phase diagram lets you work out exactly what phases are present at any given temperature and pressure. In the cases we'll be looking at on this page, the phases will simply be the solid, liquid or vapour (gas) states of a pure substance.</p>
</div>

<h5>This is the phase diagram for a typical pure substance.</h5>

<div class="image-container"><img src="pdusual.gif"></div>

<div class="text-block">
<p>These diagrams (including this one) are nearly always drawn highly distorted in order to see what is going on more easily. There are usually two major distortions. We'll discuss these when they become relevant.</p>
<p>If you look at the diagram, you will see that there are three lines, three areas marked "solid", "liquid" and "vapour", and two special points marked "C" and "T".</p>
</div>

<h4>The three areas</h4>

<div class="text-block">
<p>These are easy! Suppose you have a pure substance at three different sets of conditions of temperature and pressure corresponding to 1, 2 and 3 in the next diagram.</p>
</div>

<div class="image-container"><img src="pdareas.gif"></div>

<div class="text-block">
<p>Under the set of conditions at 1 in the diagram, the substance would be a solid because it falls into that area of the phase diagram. At 2, it would be a liquid; and at 3, it would be a vapour (a gas).</p>
</div>

<div class="note">
<p>Note: I'm using the terms vapour and gas as if they were interchangeable. There are subtle differences between them that I'm not ready to explain for a while yet. Be patient!</p>
</div>

<h4>Moving from solid to liquid by changing the temperature:</h4>

<div class="text-block">
<p>Suppose you had a solid and increased the temperature while keeping the pressure constant – as shown in the next diagram. As the temperature increases to the point where it crosses the line, the solid will turn to liquid. In other words, it melts.</p>
</div>

<div class="image-container"><img src="pdstol1.gif"></div>

<div class="text-block">
<p>If you repeated this at a higher fixed pressure, the melting temperature would be higher because the line between the solid and liquid areas slopes slightly forward.</p>
</div>

<div class="image-container"><img src="pdstol2.gif"></div>

<div class="note">
<p>Note: This is one of the cases where we distort these diagrams to make them easier to discuss. This line is much more vertical in practice than we normally draw it. There would be very little change in melting point at a higher pressure. The diagram would be very difficult to follow if we didn't exaggerate it a bit.</p>
</div>

<div class="text-block">
<p>So what actually is this line separating the solid and liquid areas of the diagram?</p>
<p>It simply shows the effect of pressure on melting point.</p>
<p>Anywhere on this line, there is an equilibrium between solid and liquid.</p>
<p>You can apply Le Chatelier's Principle to this equilibrium just as if it was a chemical equilibrium. If you increase the pressure, the equilibrium will move in such a way as to counter the change you have just made.</p>
</div>

<div class="block-formula">
\underbrace{\text{solid}}_{\clap{\text{\color{#467abf}{for most substances, the solid is more dense than the liquid}}}{}} \xrightleftharpoons{} \text{liquid}
</div>

<div class="text-block">
<p>If it converted from liquid to solid, the pressure would tend to decrease again because the solid takes up slightly less space for most substances.</p>
<p>That means that increasing the pressure on the equilibrium mixture of solid and liquid at its original melting point will convert the mixture back into the solid again. In other words, it will no longer melt at this temperature.</p>
<p>To make it melt at this higher pressure, you will have to increase the temperature a bit. Raising the pressure raises the melting point of most solids. That's why the melting point line slopes forward for most substances.</p>
<h4>Moving from solid to liquid by changing the pressure:</h4>
<p>You can also play around with this by looking at what happens if you decrease the pressure on a solid at constant temperature.</p>
</div>

<div class="note">
<p>Note: You have got to be a bit careful about this, because exactly what happens if you decrease the pressure depends on exactly what your starting conditions are. We'll talk some more about this when we look at the line separating the solid region from the vapour region.</p>
</div>

<div class="image-container"><img src="pdstol3.gif"></div>

<h4>Moving from liquid to vapour:</h4>

<div class="text-block">
<p>In the same sort of way, you can do this either by changing the temperature or the pressure.</p>
</div>

<div class="image-container"><img src="ltov1.gif"></div>

<div class="text-block">
<p>The liquid will change to a vapour – it boils – when it crosses the boundary line between the two areas. If it is temperature that you are varying, you can easily read off the boiling temperature from the phase diagram. In the diagram above, it is the temperature where the red arrow crosses the boundary line.</p>
<p>So, again, what is the significance of this line separating the two areas?</p>
<p>Anywhere along this line, there will be an equilibrium between the liquid and the vapour. The line is most easily seen as the effect of pressure on the boiling point of the liquid.</p>
<p>As the pressure increases, so the boiling point increases.</p>
</div>

<div class="note">
<p>Note: I don't want to make any very big deal over this, but this line is actually exactly the same as the graph for the effect of temperature on the saturated vapour pressure of the liquid. <a href="vapourpress.html#top">Saturated vapour pressure</a> is dealt with on a separate page. A liquid will boil when its saturated vapour pressure is equal to the external pressure.</p>
<p>Suppose you measured the saturated vapour pressure of a liquid at 50°C, and it turned out to be 75 kPa. You could plot that as one point on a vapour pressure curve, and then go on to measure other saturated vapour pressures at different temperatures and plot those as well.</p>
<p>Now, suppose that you had the liquid exposed to a total external pressure of 75 kPa, and gradually increased the temperature. The liquid would boil when its saturated vapour pressure became equal to the external pressure – in this case at 50°C. If you have the complete vapour pressure curve, you could equally well find the boiling point corresponding to any other external pressure.</p>
<p>That means that the plot of saturated vapour pressure against temperature is exactly the same as the curve relating boiling point and external pressure – they are just two ways of looking at the same thing.</p>
<p>If all you are interested in doing is interpreting one of these phase diagrams, you probably don't have to worry too much about this.</p>
</div>

<h4>The critical point</h4>

<div class="text-block">
<p>You will have noticed that this liquid-vapour equilibrium curve has a top limit that I have labelled as C in the phase diagram.</p>
<p>This is known as the critical point. The temperature and pressure corresponding to this are known as the critical temperature and critical pressure.</p>
<p>If you increase the pressure on a gas (vapour) at a temperature lower than the critical temperature, you will eventually cross the liquid-vapour equilibrium line and the vapour will condense to give a liquid.</p>
</div>

<div class="image-container"><img src="pdvtol.gif"></div>

<div class="text-block">
<p>This works fine as long as the gas is below the critical temperature. What, though, if your temperature was above the critical temperature? There wouldn't be any line to cross!</p>
<p>That is because, above the critical temperature, it is impossible to condense a gas into a liquid just by increasing the pressure. All you get is a highly compressed gas. The particles have too much energy for the intermolecular attractions to hold them together as a liquid.</p>
<p>The critical temperature obviously varies from substance to substance and depends on the strength of the attractions between the particles. The stronger the intermolecular attractions, the higher the critical temperature.</p>
</div>

<div class="note">
<p>Note: This is now a good point for a quick comment about the use of the words "gas" and "vapour". To a large extent you just use the term which feels right. You don't usually talk about "ethanol gas", although you would say "ethanol vapour". Equally, you wouldn't talk about oxygen as being a vapour – you always call it a gas.</p>
<p>There are various guide-lines that you can use if you want to. For example, if the substance is commonly a liquid at or around room temperature, you tend to call what comes away from it a vapour. A slightly wider use would be to call it a vapour if the substance is below its critical point, and a gas if it is above it. Certainly it would be unusual to call anything a vapour if it was above its critical point at room temperature – oxygen or nitrogen or hydrogen, for example. These would all be described as gases.</p>
<p>This is absolutely NOT something that is at all worth getting worked up about!</p>
</div>

<h4>Moving from solid to vapour:</h4>

<div class="text-block">
<p>There's just one more line to look at on the phase diagram. This is the line in the bottom left-hand corner between the solid and vapour areas.</p>
<p>That line represents solid-vapour equilibrium. If the conditions of temperature and pressure fell exactly on that line, there would be solid and vapour in equilibrium with each other – the solid would be subliming. (Sublimation is the change directly from solid to vapour or vice versa without going through the liquid phase.)</p>
<p>Once again, you can cross that line by either increasing the temperature of the solid, or decreasing the pressure.</p>
<p>The diagram shows the effect of increasing the temperature of a solid at a (probably very low) constant pressure. The pressure obviously has to be low enough that a liquid can't form – in other words, it has to happen below the point labelled as T.</p>
</div>

<div class="image-container"><img src="pdstov.gif"></div>

<div class="text-block">
<p>You could read the sublimation temperature off the diagram. It will be the temperature at which the line is crossed.</p>
</div>

<h4>The triple point</h4>

<div class="text-block">
<p>Point T on the diagram is called the triple point.</p>
<p>If you think about the three lines which meet at that point, they represent conditions of:</p>
</div>

<ul>
<li>solid-liquid equilibrium</li>
<li>liquid-vapour equilibrium</li>
<li>solid-vapour equilibrium</li>
</ul>

<div class="text-block">
<p>Where all three lines meet, you must have a unique combination of temperature and pressure where all three phases are in equilibrium together. That's why it is called a triple point.</p>
<p>If you controlled the conditions of temperature and pressure in order to land on this point, you would see an equilibrium which involved the solid melting and subliming, and the liquid in contact with it boiling to produce a vapour – and all the reverse changes happening as well.</p>
<p>If you held the temperature and pressure at those values, and kept the system closed so that nothing escaped, that's how it would stay. A strange set of affairs!</p>
</div>

<h4>Normal melting and boiling points</h4>

<div class="text-block">
<p>The normal melting and boiling points are those when the pressure is 1 atmosphere. These can be found from the phase diagram by drawing a line across at 1 atmosphere pressure.</p>
</div>

<div class="image-container"><img src="mptbpt.gif"></div>

<h2>The Phase Diagram for Water</h2>

<div class="image-container"><img src="pdh2o1.gif"></div>

<div class="text-block">
<p>There is only one difference between this and the phase diagram that we've looked at up to now. The solid-liquid equilibrium line (the melting point line) slopes backwards rather than forwards.</p>
<p>In the case of water, the melting point gets lower at higher pressures. Why?</p>
</div>

<div class="block-formula" label="ice is less dense than water, so it contracts when it melts">
\text{ice} \xrightleftharpoons{} \text{water}
</div>

<div class="text-block">
<p>If you have this equilibrium and increase the pressure on it, according to Le Chatelier's Principle the equilibrium will move to reduce the pressure again. That means that it will move to the side with the smaller volume. Liquid water is produced.</p>
<p>To make the liquid water freeze again at this higher pressure, you will have to reduce the temperature. Higher pressures mean lower melting (freezing) points.</p>
<p>Now lets put some numbers on the diagram to show the exact positions of the critical point and triple point for water.</p>
</div>

<div class="image-container"><img src="pdh2o2.gif"></div>

<div class="text-block">
<p>Notice that the triple point for water occurs at a very low pressure. Notice also that the critical temperature is 374°C. It would be impossible to convert water from a gas to a liquid by compressing it above this temperature.</p>
<p>The normal melting and boiling points of water are found in exactly the same way as we have already discussed – by seeing where the 1 atmosphere pressure line crosses the solid-liquid and then the liquid-vapour equilibrium lines.</p>
</div>

<div class="note">
<p>Note: Further up the page I mentioned two ways in which these diagrams are distorted to make them easier to follow. I have already pointed out that the solid-liquid equilibrium line should really be much more vertical. This last diagram illustrates the other major distortion – which is to the scales of both pressure and temperature. Look, for example, at the gaps between the various quoted pressure figures and then imagine that you had to plot those on a bit of graph paper! The temperature scale is equally haphazard.</p>
</div>

<div class="text-block">
<p>Just one final example of using this diagram (because it appeals to me). Imagine lowering the pressure on liquid water along the line in the diagram below.</p>
</div>

<div class="image-container"><img src="pdh2o3.gif"></div>

<div class="text-block">
<p>The phase diagram shows that the water would first freeze to form ice as it crossed into the solid area. When the pressure fell low enough, the ice would then sublime to give water vapour. In other words, the change is from liquid to solid to vapour. I find that satisfyingly bizarre!</p>
</div>

<h2>The Phase Diagram for Carbon Dioxide</h2>

<div class="image-container"><img src="pdco2.gif"></div>

<div class="text-block">
<p>The only thing special about this phase diagram is the position of the triple point which is well above atmospheric pressure. It is impossible to get any liquid carbon dioxide at pressures less than 5.11 atmospheres.</p>
<p>That means that at 1 atmosphere pressure, carbon dioxide will sublime at a temperature of -78°C.</p>
<p>This is the reason that solid carbon dioxide is often known as "dry ice". You can't get liquid carbon dioxide under normal conditions – only the solid or the vapour.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-phasediagpure.pdf" target="_blank">Questions on phase diagrams for pure substances</a>
<a href="../questions/a-phasediagpure.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../phaseeqiamenu.html#top">To the phase equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>