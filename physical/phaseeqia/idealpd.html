<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Raoult's Law and Ideal Mixtures of Liquids | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how Raoult's Law applies to cases of two volatile liquids which form an ideal mixture. It explains how a phase diagram for such a mixture is built up and how to interpret it.">
<meta name="keywords" content="phase, diagram, liquid, liquids, ideal, mixture, mixtures, composition, vapour, pressure, boiling, point, raoult, raoult's, law">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Raoult's Law and Ideal Mixtures of Liquids</h1>

<div class="text-block">
<p>This page deals with Raoult's Law and how it applies to mixtures of two volatile liquids. It covers cases where the two liquids are entirely miscible in all proportions to give a single liquid – NOT those where one liquid floats on top of the other (immiscible liquids).</p>
<p>The page explains what is meant by an ideal mixture and looks at how the phase diagram for such a mixture is built up and used.</p>
</div>

<div class="note">
<p>Important: If you haven't already read the page about <a href="vapourpress.html#top">saturated vapour pressure</a>, you should follow this link before you go on.</p>
</div>

<h2>Ideal Mixtures</h2>

<div class="text-block">
<p>An ideal mixture is one which obeys Raoult's Law, but I want to look at the characteristics of an ideal mixture before actually stating Raoult's Law. The page will flow better if I do it this way around.</p>
</div>

<h3>Examples of Ideal Mixtures</h3>

<div class="text-block">
<p>There is actually no such thing as an ideal mixture! However, some liquid mixtures get fairly close to being ideal. These are mixtures of two very closely similar substances.</p>
<p>Commonly quoted examples include:</p>
</div>

<ul>
<li>hexane and heptane</li>
<li>benzene and methylbenzene</li>
<li>propan-1-ol and propan-2-ol</li>
</ul>

<h3>Ideal Mixtures and Intermolecular Forces</h3>

<div class="text-block">
<p>In a pure liquid, some of the more energetic molecules have enough energy to overcome the intermolecular attractions and escape from the surface to form a vapour.</p>
<p>The smaller the intermolecular forces, the more molecules will be able to escape at any particular temperature.</p>
</div>

<div class="image-container"><img src="blueliq.gif"></div>

<div class="text-block">
<p>If you have a second liquid, the same thing is true. At any particular temperature a certain proportion of the molecules will have enough energy to leave the surface.</p>
</div>

<div class="image-container"><img src="redliq.gif"></div>

<div class="text-block">
<p>In an ideal mixture of these two liquids, the tendency of the two different sorts of molecules to escape is unchanged.</p>
</div>

<div class="image-container"><img src="rbmixliq.gif"></div>

<div class="text-block">
<p>You might think that the diagram shows only half as many of each molecule escaping – but the proportion of each escaping is still the same. The diagram is for a 50/50 mixture of the two liquids. That means that there are only half as many of each sort of molecule on the surface as in the pure liquids. If the proportion of each escaping stays the same, obviously only half as many will escape in any given time.</p>
<p>If the red molecules still have the same tendency to escape as before, that must mean that the intermolecular forces between two red molecules must be exactly the same as the intermolecular forces between a red and a blue molecule.</p>
<p>If the forces were any different, the tendency to escape would change.</p>
<p>Exactly the same thing is true of the forces between two blue molecules and the forces between a blue and a red. They must also be the same otherwise the blue ones would have a different tendency to escape than before.</p>
<p>If you follow the logic of this through, the intermolecular attractions between two red molecules, two blue molecules or a red and a blue molecule must all be exactly the same if the mixture is to be ideal.</p>
<p>This is why mixtures like hexane and heptane get close to ideal behaviour. They are similarly sized molecules and so have similarly sized van der Waals attractions between them. However, they obviously aren't identical – and so although they get close to being ideal, they aren't actually ideal.</p>
<p>For the purposes of this topic, getting close to ideal is good enough!</p>
</div>

<h3>Ideal Mixtures and Enthalpy Change of Mixing</h3>

<div class="text-block">
<p>When you make any mixture of liquids, you have to break the existing intermolecular attractions (which needs energy), and then remake new ones (which releases energy).</p>
<p>If all these attractions are the same, there won't be any heat either evolved or absorbed.</p>
<p>That means that an ideal mixture of two liquids will have zero enthalpy change of mixing. If the temperature rises or falls when you mix the two liquids, then the mixture isn't ideal.</p>
</div>

<h2>Raoult's Law</h2>

<div class="text-block">
<p>You may have come cross a slightly simplified version of Raoult's Law if you have studied the effect of a non-volatile solute like salt on the vapour pressure of solvents like water. The definition below is the one to use if you are talking about mixtures of two volatile liquids.</p>
</div>

<div class="definition">
<p>The partial vapour pressure of a component in a mixture is equal to the vapour pressure of the pure component at that temperature multiplied by its mole fraction in the mixture.</p>
</div>

<div class="text-block">
<p>Raoult's Law only works for ideal mixtures.</p>
<p>In equation form, for a mixture of liquids A and B, this reads:</p>
</div>

<div class="block-formula">
\begin{gathered}
p_A = x_A \times P^o_A \\
\\
p_B = x_B \times P^o_B
\end{gathered}
</div>

<div class="text-block">
<p>In this equation, P<sub>A</sub> and P<sub>B</sub> are the partial vapour pressures of the components A and B. In any mixture of gases, each gas exerts its own pressure. This is called its partial pressure and is independent of the other gases present. Even if you took all the other gases away, the remaining gas would still be exerting its own partial pressure.</p>
<p>The total vapour pressure of the mixture is equal to the sum of the individual partial pressures.</p>
</div>

<div class="block-formula">
\text{total vapour pressure} = p_A + p_B
</div>

<div class="text-block">
<p>The P<sup>o</sup> values are the vapour pressures of A and B if they were on their own as pure liquids.</p>
<p>x<sub>A</sub> and x<sub>B</sub> are the mole fractions of A and B. That is exactly what it says it is – the fraction of the total number of moles present which is A or B.</p>
<p>You calculate mole fraction using, for example:</p>
</div>

<div class="block-formula">
x_A = \frac{\text{moles of A}}{\text{total number of moles}}
</div>

<h5>Putting all this together in a simple example:</h5>

<div class="text-block">
<p>Suppose you had a mixture of 2 moles of methanol and 1 mole of ethanol at a particular temperature. The vapour pressure of pure methanol at this temperature is 81 kPa, and the vapour pressure of pure ethanol is 45 kPa.</p>
<p>There are 3 moles in the mixture in total.</p>
<p>2 of these are methanol. The mole fraction of methanol is 2/3.</p>
<p>Similarly, the mole fraction of ethanol is 1/3.</p>
<p>You can easily find the partial vapour pressures using Raoult's Law – assuming that a mixture of methanol and ethanol is ideal.</p>
</div>

<h5>First for methanol:</h5>

<div class="block-formula">
\begin{aligned}
p_{methanol} &= \frac{2}{3} \times 81 \\
{} &= 54 \text{ kPa}
\end{aligned}
</div>

<div class="text-block">
<p> and then for ethanol:</p>
</div>

<div class="block-formula">
\begin{aligned}
p_{ethanol} &= \frac{1}{3} \times 45 \\
{} &= 15 \text{ kPa}
\end{aligned}
</div>

<div class="text-block">
<p>You get the total vapour pressure of the liquid mixture by adding these together.</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{total vapour pressure} &= 54 + 15 \\
{} &= 69 \text{ kPa}
\end{aligned}
</div>

<div class="image-container"><img src="vpethmeth.gif"></div>

<div class="text-block">
<p>In practice, this is all a lot easier than it looks when you first meet the definition of Raoult's Law and the equations!</p>
</div>

<h2>Vapour Pressure / Composition Diagrams</h2>

<div class="text-block">
<p>Suppose you have an ideal mixture of two liquids A and B. Each of A and B is making its own contribution to the overall vapour pressure of the mixture – as we've seen above.</p>
<p>Let's focus on one of these liquids – A, for example.</p>
<p>Suppose you double the mole fraction of A in the mixture (keeping the temperature constant). According to Raoult's Law, you will double its partial vapour pressure. If you triple the mole fraction, its partial vapour pressure will triple – and so on.</p>
<p>In other words, the partial vapour pressure of A at a particular temperature is proportional to its mole fraction. If you plot a graph of the partial vapour pressure of A against its mole fraction, you will get a straight line.</p>
</div>

<div class="image-container"><img src="vpcomp1.gif"></div>

<div class="note">
<p>Beware! These diagrams (and the ones that follow) only work properly if you plot the partial vapour pressure of a substance against its mole fraction. If you plot it against its mass or its percentage by mass, you don't get a straight line. Instead, you get a slight curve. This is a result of the way the maths works. You don't need to worry about this unless you come across a diagram for ideal mixtures showing these plots as curves rather than straight lines. (Non-ideal mixtures will produce curves – see below.) If you find curves for an ideal mixture, look careful at the labelling on the graph – and then go and find another book. Presenting a graph in that way is just plain misleading!</p>
</div>

<div class="text-block">
<p>Now we'll do the same thing for B – except that we will plot it on the same set of axes. The mole fraction of B falls as A increases so the line will slope down rather than up. As the mole fraction of B falls, its vapour pressure will fall at the same rate.</p>
</div>

<div class="image-container"><img src="vpcomp2.gif"></div>

<div class="text-block">
<p>Notice that the vapour pressure of pure B is higher than that of pure A. That means that molecules must break away more easily from the surface of B than of A. B is the more volatile liquid.</p>
</div>

<div class="note">
<p>Note: Two things. First, because the intermolecular forces in the two liquids aren't exactly the same, they aren't going to form a strictly ideal mixture. However, if we make them identical, it would turn out that everything else we say in this topic would be completely pointless!</p>
<p>Secondly, the choice of which of these liquids is the more volatile is totally arbitrary. I could equally well have drawn a different diagram where A was the more volatile and had the higher vapour pressure. I could also have reversed the mole fraction scale with pure A on the left-hand side and pure B on the right. All of this, of course, will mean that these diagrams (and all those that follow) could look subtly different if you find them from different sources. It is really important with this to understand what is going on, otherwise you risk getting seriously confused.</p>
</div>

<div class="text-block">
<p>To get the total vapour pressure of the mixture, you need to add the values for A and B together at each composition. The net effect of that is to give you a straight line as shown in the next diagram.</p>
</div>

<div class="image-container"><img src="vpcomp3.gif"></div>

<div class="note">
<p>Following on from the last note: For non-ideal mixtures, these straight lines become curves. For a nearly ideal mixture, they are near enough straight lines – that's the assumption we are working on here. The less ideal the mixture is, the more curved the lines become. This is dealt with in more detail on another page. I'm not giving you a link to that at the moment, because you shouldn't visit that page until you've finished this one – it would be too scary!</p>
</div>

<h2>Boliling Point / Composition Diagrams</h2>

<h3>The Relationship Between Boiling Point and Vapour Pressure</h3>

<div class="text-block">
<p>If a liquid has a high vapour pressure at a particular temperature, it means that its molecules are escaping easily from the surface.</p>
<p>If, at the same temperature, a second liquid has a low vapour pressure, it means that its molecules aren't escaping so easily.</p>
<p>What does that imply about the boiling points of the two liquids? Don't read on until you have tried to think this out!</p>
<p>There are two ways of looking at this. Choose whichever seems easiest to you. It doesn't matter how you work this out – it is the result that is important.</p>
</div>

<h5>Either:</h5>

<ul>
<li>If the molecules are escaping easily from the surface, it must mean that the intermolecular forces are relatively weak. That means that you won't have to supply so much heat to break them completely and boil the liquid.<br>The liquid with the higher vapour pressure at a particular temperature is the one with the lower boiling point.</li>
</ul>

<h5>Or:</h5>

<ul>
<li>Liquids boil when their vapour pressure becomes equal to the external pressure. If a liquid has a high vapour pressure at some temperature, you won't have to increase the temperature very much until the vapour pressure reaches the external pressure. On the other hand if the vapour pressure is low, you will have to heat it up a lot more to reach the external pressure.<br>The liquid with the higher vapour pressure at a particular temperature is the one with the lower boiling point.</li>
</ul>

<div class="text-block">
<p>Don't go on until you are sure about this:</p>
<p>For two liquids at the same temperature:</p>
</div>

<ul>
<li>The one with the higher vapour pressure is the one with the lower boiling point.</li>
</ul>

<h3>Constructing a Boiling Point / Composition Diagram</h3>

<div class="text-block">
<p>To remind you – we've just ended up with this vapour pressure / composition diagram:</p>
</div>

<div class="image-container"><img src="vpcomp3.gif"></div>

<div class="text-block">
<p>We're going to convert this into a boiling point / composition diagram.</p>
<p>We'll start with the boiling points of pure A and B.</p>
<p>B has the higher vapour pressure. That means that it will have the lower boiling point. If that isn't obvious to you, go back and read the last section again!</p>
</div>

<div class="image-container"><img src="bpcompi1.gif"></div>

<div class="text-block">
<p>For mixtures of A and B, you might perhaps have expected that their boiling points would form a straight line joining the two points we've already got. Not so!</p>
<p>In fact, it turns out to be a curve.</p>
</div>

<div class="image-container"><img src="bpcompi2.gif"></div>

<div class="note">
<p>Important: Take great care drawing this curve. The boiling point of B is the lowest boiling point. Don't let your curve droop below this. That happens with certain non-ideal mixtures and has consequences which are explored on another page.</p>
</div>

<div class="text-block">
<p>To make this diagram really useful (and finally get to the phase diagram we've been heading towards), we are going to add another line. This second line will show the composition of the vapour over the top of any particular boiling liquid.</p>
<p>If you boil a liquid mixture, you would expect to find that the more volatile substance escapes to form a vapour more easily than the less volatile one.</p>
<p>That means that in the case we've been talking about, you would expect to find a higher proportion of B (the more volatile component) in the vapour than in the liquid. You can discover this composition by condensing the vapour and analysing it. That would give you a point on the diagram.</p>
</div>

<div class="image-container"><img src="bpcompi3.gif"></div>

<div class="text-block">
<p>The diagram just shows what happens if you boil a particular mixture of A and B. Notice that the vapour over the top of the boiling liquid has a composition which is much richer in B – the more volatile component.</p>
<p>If you repeat this exercise with liquid mixtures of lots of different compositions, you can plot a second curve – a vapour composition line.</p>
</div>

<div class="image-container"><img src="bpcompi4.gif"></div>

<div class="text-block">
<p>This is now our final phase diagram.</p>
</div>

<div class="note">
<p>Important: Once again, take great care drawing this second curve. No point on the curve must be higher than the boiling temperature of the pure A.</p>
</div>

<h3>Using the Phase Diagram</h3>

<div class="text-block">
<p>The diagram is used in exactly the same way as it was built up. If you boil a liquid mixture, you can find out the temperature it boils at, and the composition of the vapour over the boiling liquid.</p>
<p>For example, in the next diagram, if you boil a liquid mixture C<sub>1</sub>, it will boil at a temperature T<sub>1</sub> and the vapour over the top of the boiling liquid will have the composition C<sub>2</sub>.</p>
</div>

<div class="image-container"><img src="bpcompi5.gif"></div>

<div class="text-block">
<p>All you have to do is to use the liquid composition curve to find the boiling point of the liquid, and then look at what the vapour composition would be at that temperature.</p>
<p>Notice again that the vapour is much richer in the more volatile component B than the original liquid mixture was.</p>
</div>

<h3>The Beginnings of Fractional Distillation</h3>

<div class="text-block">
<p>Suppose that you collected and condensed the vapour over the top of the boiling liquid and reboiled it.</p>
<p>You would now be boiling a new liquid which had a composition C<sub>2</sub>.</p>
<p>That would boil at a new temperature T<sub>2</sub>, and the vapour over the top of it would have a composition C<sub>3</sub>.</p>
</div>

<div class="image-container"><img src="bpcompi6.gif"></div>

<div class="text-block">
<p>You can see that we now have a vapour which is getting quite close to being pure B. If you keep on doing this (condensing the vapour, and then reboiling the liquid produced) you will eventually get pure B.</p>
<p>This is obviously the basis for fractional distillation. However, doing it like this would be incredibly tedious, and unless you could arrange to produce and condense huge amounts of vapour over the top of the boiling liquid, the amount of B which you would get at the end would be very small.</p>
<p>Real fractionating columns (whether in the lab or in industry) automate this condensing and reboiling process. How these work will be explored on another page (see below).</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-idealliquids.pdf" target="_blank">Questions on the Raoult's Law and ideal mixtures of liquids</a>
<a href="../questions/a-idealliquids.pdf" target="_blank">Answers</a>
</div>


<div class="link-list">
<p>Where would you like to go now?</p>
<a href="idealfract.html#top">Continue to the page about distillation of ideal liquid mixtures</a>
<a href="../phaseeqiamenu.html#top">To the phase equilibrium menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>