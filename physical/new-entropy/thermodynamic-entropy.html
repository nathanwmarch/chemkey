<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Thermodynamic Entropy | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A simple, non-mathematical introduction to entropy">
<meta name="keywords" content="entropy, disorder, predict">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Thermodynamic Entropy</h1>

<div class="text-block">
<p>What is thermodynamic entropy?</p>
</div>

<div class="definition">
<p>Thermodynamic entropy:</p>
<p>A measure of the how spread out thermal energy is.</p>
</div>

<div class="text-block">
<p>You understand what it means for heat to spread out: the outside of a cup of coffee gets warm because the liquid inside it is hot; your hand grows cold when holding ice.</p>
<p>It is a spontaneous process which boils down to how individual particles interact and share their energy.</p>
<p><strong>Thermodynamic entropy is a measure of exactly <i>how</i> that heat energy is spread across all the particles of a system</strong>.</p>
</div>

<div class="block-formula">
    \begin{matrix}
    \text{heat spread unevenly} & \text{heat spread evenly} \\
    \text{\color{#467abf}{low entropy}} & \text{\color{#467abf}{high entropy}}
    \end{matrix}
</div>

<div class="note">
<p>You may have heard that particles have a natural distribution of energies (called the Maxwell-Boltzmann distribution) – this is completely true, and it is not a contradiction with what I have already said. Entropy is a macroscopic property, dealing with regions, while the Maxwell-Boltzmann distribution deals with individual particles.</p>
</div>

<h2>Measuring Thermodynamic Entropy</h2>

<div class="text-block">
<p>Rudolf Clausius first described entropy based on the heat lost from a heat engine to the surroundings. </p>
<p>A heat engine turns a difference in temperature into mechanical work. For instance, steam rising and turning the blades of a turbine is an example of a heat engine. The steam loses energy as it performs work, leading to a drop in temperature between the steam at the start and the steam at the end.</p>
<p>A certain amount of wasted heat energy (q) is generated from this work, and is lost to the surroundings.</p>
<p>The ratio of this wasted heat to the temperature of the surroundings was given the name <i>entropy</i>, and the symbol <i>S</i>:</p>
</div>

<div class="block-formula" label="Classius' definition of entropy">
    \begin{gathered}
    S = \frac{q}{T_{surroundings}} \\
    \\
    \begin{aligned}
    S &- \text{entropy transferred from the system to the surroundings} \\
    q &- \text{heat transferred from the system to the surroundings}
    \end{aligned}
    \end{gathered}
</div>

<div class="text-block">
<p>Modern conventions focus on absolute measurements of the system and surroundings, rather than entropy transferred from one place to another, so the equation is written:</p>
</div>

<div class="block-formula" label="modern expression of entropy">
    \begin{gathered}
    \delta S_{system} = \frac{\delta q_{system}}{T_{surroundings}} \\
    \\
    \begin{aligned}
    \delta S &- \text{the instantaneous change in the system's entropy} \\
    \delta q &- \text{the instantaneous heat transferred to or from the system} \\
    T &- \text{the temperature of the surroundings}
    \end{aligned}
    \end{gathered}
</div>

<div class="text-block">
<p>When a system absorbs or loses heat, the entropy of the system will change accordingly:</p>
</div>

<div class="block-formula">
    S_{final} = S_{initial} + \Delta S
</div>

<div class="text-block">
<p>Calculating the change in entropy is straightforward when the temperature of the surroundings is constant:</p>
</div>

<div class="block-formula">
    \begin{aligned}
    \int \delta S &= \int \frac{\delta q_{system}}{T} \\
    \Delta S &= \frac{q_{system}}{T}

    \end{aligned}
</div>

<div class="text-block">
<p>Unfortunately, under real-world conditions the temperature of the surroundings may vary, so the integral becomes:</p>
</div>

<div class="block-formula">
    \Delta S = \int_{T_{initial}}^{T_{final}} \frac{\delta q_{system}}{T}
</div>

</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>