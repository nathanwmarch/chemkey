<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Redox (Electrode) Potentials and Test Tube Reactions | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the relationship between redox potentials and simple test tube reactions">
<meta name="keywords" content="equilibrium, equilibria, redox, potential, electrode, standard, electrochemical series, ecs">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Redox (Electrode) Potentials and Test Tube Reactions</h1>

<div class="text-block">
<p>This page explains how standard electrode potentials (redox potentials) relate to simple and familiar reactions that can be done in test tubes.</p>
</div>

<div class="note">
<p>Important: If you have come straight to this page via a search engine, you should be aware that this is just one page in a linked series of pages about redox potentials. You will find it much easier to understand if you <a href="introduction.html#top">start from the beginning</a>. Links at the bottom of each page will bring you back here again.</p>
<p>It is also important that you understand about redox reactions – particularly <a href="../../inorganic/redox/equations.html#top">building ionic equations from electron-half-equations</a>. Follow this link if you aren't confident about this.</p>
</div>

<h2>Combining Zinc With a Copper Half Cell</h2>

<div class="text-block">
<p>So far in this series of pages, we have looked at combinations of a hydrogen electrode with the half cell we have been interested in. However, there isn't any reason why you can't couple any two half cells together.</p>
<p>This next bit looks at what happens if you combine a zinc half cell with a copper half cell.</p>
</div>

<h3>In the Presence of a High Resistance Voltmeter</h3>

<div class="image-container"><img src="zncucell.gif"></div>

<div class="text-block">
<p>The two equilibria which are set up in the half cells are:</p>
</div>

<div class="block-formula">
\begin{matrix}
\text{Zn}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Zn}_{(s)} & E^o = {-}0.76 \text{ V} \\
\\
\text{Cu}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Cu}_{(s)} & E^o = {+}0.34 \text{ V}
\end{matrix}
</div>

<div class="text-block">
<p>The negative sign of the zinc E° value shows that it releases electrons more readily than hydrogen does. The positive sign of the copper E° shows that it releases electrons less readily than hydrogen.</p>
<p>That means that you can compare any two equilibria directly. For example, in this case you can see that the zinc releases electrons more readily than the copper does – the position of the zinc equilibrium lies further to the left than the copper equilibrium.</p>
<p>Stripping everything else out of the diagram, and looking only at the build up of electrons on the two pieces of metal:</p>
</div>

<div class="image-container"><img src="zncucell2.gif"></div>

<div class="text-block">
<p>Obviously, the voltmeter will show that the zinc is the negative electrode, and copper is the (relatively) positive one. It will register a voltage showing the difference between them.</p>
</div>

<div class="note">
<p>Note: It is very simple to calculate this voltage from the E° values. If you are interested in doing this, then you may like to look at my <a href="../../book.html#top">chemistry calculations book</a>. It is impossible for me to include these calculations on this site without upsetting my publishers!</p>
</div>

<h3>Removing the Voltmeter</h3>

<div class="text-block">
<p>The high resistance of the voltmeter is deliberately designed to stop any current flow in the circuit. What happens if you remove the voltmeter and replace it with a bit of wire?</p>
<p>Electrons will flow from where there are a lot of them (on the zinc) to where there are fewer (on the copper). The movement of the electrons is an electrical current.</p>
</div>

<div class="note">
<p>Important: In chemistry, you should always think in terms of electron flow, and never in terms of current flow. The problem is that conventionally in physics current flows in the opposite direction to the electrons. That's totally silly! Avoid the problem.</p>
</div>

<div class="image-container"><img src="zncucell3.gif"></div>

<h4>The effect of this on the equilibria</h4>

<div class="text-block">
<p>These are just simple equilibria, and you can apply Le Chatelier's Principle to them.</p>
</div>

<div class="note">
<p>Note: If you are unsure about <a href="../equilibria/lechatelier.html#top">Le Chatelier's Principle</a>, then you must follow this link.You don't need to read the whole page – just the beginning.</p>
</div>

<div class="text-block">
<p>Electrons are flowing away from the zinc equilibrium. According to Le Chatelier's Principle, the position of equilibrium will move to replace the lost electrons.</p>
<p>Electrons are being dumped onto the piece of copper in the copper equilibrium. According to Le Chatelier's Principle, the position of equilibrium will move to remove these extra electrons.</p>
</div>

<div class="image-container"><img src="zncushifts.gif"></div>

<div class="text-block">
<p>If electrons continue to flow, the positions of equilibrium keep on shifting. The two equilibria essentially turn into two one-way reactions. The zinc continues to ionise, and the copper(II) ions keep on picking up electrons.</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{Zn}_{(s)} &\longrightarrow \text{Zn}^{2+}_{(aq)} + 2\text{e}^- \\
\\
\text{Cu}^{2+}_{(aq)} + 2\text{e}^- &\longrightarrow \text{Cu}_{(s)}
\end{aligned}
</div>

<div class="text-block">
<p>Taking the apparatus as a whole, there is a chemical reaction going on in which zinc is going into solution as zinc ions, and is giving electrons to copper(II) ions to turn them into metallic copper.</p>
</div>

<h3>Relating This to a Test Tube Reaction</h3>

<div class="text-block">
<p>This is exactly the same reaction that occurs when you drop a piece of zinc into some copper(II) sulfate solution. The blue colour of the solution fades as the copper(II) ions are converted into brown copper metal. The final solution contains zinc sulfate. (The sulfate ions are spectator ions.)</p>
<p>You can add the two electron-half-equations above to give the overall ionic equation for the reaction.</p>
</div>

<div class="image-container"><img src="zncuoverall.gif"></div>

<div class="text-block">
<p>The only difference in this case is that the zinc gives the electrons directly to the copper(II) ions rather than the electrons having to travel along a bit of wire first.</p>
<p>The test tube reaction happens because of the relative tendency of the zinc and copper to lose electrons to form ions. You can find out this relative tendency by looking at the E° values. That means that any redox reaction could be discussed in a similar way.</p>
<p>The rest of the examples on this page illustrate this.</p>
</div>

<h2>The Reaction Between Copper and Silver Nitrate Solution</h2>

<h3>The Reaction in a Test Tube</h3>

<div class="text-block">
<p>If you hang a coil of copper wire in some colourless silver nitrate solution, the copper gets covered in silver – partly as a grey fur, and partly as delicate crystals. The solution turns blue.</p>
<p>Building the ionic equation from the two half-equations:</p>
</div>

<div class="image-container"><img src="cuagoverall.gif"></div>

<div class="note">
<p>Note: If you don't understand why the second equation is multiplied by 2, you really must read about <a href="../../inorganic/redox/equations.html#top">building ionic equations from electron-half-equations</a>.</p>
</div>

<h3>Working From the Redox Potentials</h3> 

<div class="text-block">
<p>How does this relate to the E° values for copper and silver?</p>
</div>

<div class="block-formula">
\begin{matrix}
\text{Cu}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Cu}_{(s)} & E^o = {+}0.34 \text{ V} \\
\\
\text{Ag}^+_{(aq)} + \text{e}^- \xrightleftharpoons{} \text{Ag}_{(s)} & E^o = {+}0.80 \text{ V}
\end{matrix}
</div>

<div class="text-block">
<p>You can see that both of these E° values are positive. Neither copper nor silver produce ions and release electrons as easily as hydrogen does.</p>
<p>However, of the two, copper releases electrons more readily. In a cell, the copper would have the greater build up of electrons, and be the negative electrode. If the copper and silver were connected by a bit of wire, electrons would flow from the copper to the silver.</p>
<p>That, of course, will upset the two equilibria:</p>
</div>

<div class="image-container"><img src="cuagshifts.gif"></div>

<div class="text-block">
<p>Electrons will continue to flow and the two equilibria will again turn into one-way reactions to give the electron-half-equations we've just used to build the ionic equation. Showing that again:</p>
</div>

<div class="image-container"><img src="cuagoverall.gif"></div>

<div class="note">
Note: People sometimes worry whether the fact that one of the equations has to be multiplied by two affects the argument in any way. It doesn't! It makes no difference whatsoever to any stage of the explanation in terms of shifts in the positions of the two equilibria.
</div>

<h3>A Useful "Rule of Thumb"</h3>

<div class="text-block">
<p>Whenever you link two of these equilibria together (either via a bit of wire, or by allowing one of the substances to give electrons directly to another one in a test tube):</p>
</div>

<ul>
<li>The equilibrium with the more negative (or less positive) E° value will move to the left.</li>
<li>The equilibrium with the more positive (or less negative) E° value will move to the right.</li>
</ul>

<div class="text-block">
<p>It sounds simple, but that is the key to using E° values in most of the situations you will come across.</p>
</div>

<h2>The Reaction Between Magnesium and Dilute Sulfuric acid</h2>

<h3>The Reaction in a Test Tube</h3>

<div class="text-block">
<p>Magnesium reacts with dilute sulfuric acid to give hydrogen and a colourless solution containing magnesium sulfate.</p>
<p>Is this what you would expect from the E° values?</p>
</div>

<h3>Working From the Redox Potentials</h3> 

<div class="text-block">
<p>Putting all the argument onto one diagram:</p>
</div>

<div class="image-container"><img src="mgh2shifts.gif"></div>

<div class="text-block">
<p>The two equilibria become one-way reactions which you can use to build the ionic equation:</p>
</div>

<div class="image-container"><img src="mgh2overall.gif"></div>

<h2>Using Potassium dichromate(VI) as an Oxidising Agent</h2>

<h3>The Reaction in a Test Tube</h3>

<div class="text-block">
<p>Potassium dichromate(VI) acidified with dilute sulfuric acid oxidises iron(II) ions to iron(III) ions. The orange solution containing the dichromate(VI) ions turns green as chromium(III) ions are formed.</p>
</div>

<h3>How Does This Relate to the E° Values?</h3>

<div class="image-container"><img src="cr2o7feshifts.gif"></div>

<div class="note">
<p>Warning! Those of you who have come across other ways of working with E° values may have learnt rules which force you to write the equilibria down in a particular order. I have deliberately written this example down so that it disobeys these rules!</p>
<p>This is to show that these rules are completely unnecessary. All you have to do is remember that the more negative (less positive) equilibrium will shift to the left, and the other one to the right.</p>
</div>

<div class="text-block">
<p>Building the ionic equation then works like this:</p>
</div>

<div class="image-container"><img src="cr2o7feoverall.gif"></div>

<h3>Coming Up Next</h3>

<div class="text-block">
<p>The final page in the sequence simply expands on this one, and looks at how you can use E° values to predict whether or not redox reactions are feasible. It would be a good idea to be fairly confident about the present page before you went on to the final one.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-eo4.pdf" target="_blank">Questions on redox potentials and test tube reactions</a>
<a href="../questions/a-eo4.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="predict.html#top">To the final page on electrode potentials</a>
<a href="../redoxeqiamenu.html#top">To the redox equilibria menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>