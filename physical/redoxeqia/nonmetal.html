<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Redox Potentials for Other Systems | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes the origin of the electrochemical series, and explains how it relates to the ability of the various substances in it to act as oxidising or reducing agents.">
<meta name="keywords" content="equilibrium, equilibria, redox, potential, electrode, standard, electrochemical series, ecs, oxidising agent, reducing agent, oxidation, reduction">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Redox Potentials for Other Systems</h1>

<div class="text-block">
<p>This page explains how non-metals like chlorine can be included in the electrochemical series, and how other oxidising and reducing agents can have their standard electrode potentials (redox potentials) measured and fitted into the series.</p>
</div>

<div class="note">
<p>Important: If you have come straight to this page via a search engine, you should be aware that this is just one page in a linked series of pages about redox potentials. You will find it much easier to understand if you <a href="introduction.html#top">start from the beginning</a>. Links at the bottom of each page will bring you back here again.</p>
<p>It is also important that you understand about <a href="../../inorganic/redoxmenu.html#top">redox reactions</a>. Follow this link if you aren't confident about oxidation and reduction in terms of electron transfer.</p>
</div>

<h2>Measuring Redox Potentials for More Complicated Systems</h2>

<h3>Systems Involving Gases</h3>

<div class="text-block">
<p>The obvious example here is chlorine. Chlorine is well known as an oxidising agent. Since the electrochemical series is about ranking substances according to their oxidising or reducing ability, it makes sense to include things like chlorine.</p>
<p>This time we are measuring the position of this equilibium relative to the hydrogen equilibrium.</p>
</div>

<div class="block-formula">
\text{Cl}_{2(g)} + 2\text{e}^- \xrightleftharpoons{} 2\text{Cl}^-_{(aq)}
</div>

<div class="text-block">
<p>Notice that the equilibrium is still written with the electrons on the left-hand side of the equation. That's why the chlorine gas has to appear on the left-hand side rather than on the right (which is where the metals and hydrogen appeared).</p>
<p>How can this equilibrium be connected into a circuit? The half-cell is built just the same as a hydrogen electrode. Chlorine gas is bubbled over a platinum electrode, which is immersed in a solution containing chloride ions with a concentration of 1 mol dm<sup>-3</sup>.</p>
<p>The conventional way of writing the whole cell looks like this.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{Pt}_{(s)}[\text{H}_{2(g)}]\Big|2\text{H}^+_{(aq)}\Big\|\text{Cl}_{2(g)}, 2\text{Cl}^-_{(aq)}\Big|\text{Pt}_{(s)} \\
E^o = {+}1.36 \text{ V}
\end{gathered}
</div>

<div class="text-block">
<p>Notice the way that the chlorine half cell is written. The convention is that the substance losing electrons is written closest to the electrode. In this case, the chloride ions are losing electrons.</p>
</div>

<div class="note">
<p>Note: Assuming that you know about oxidation states (oxidation numbers), that is exactly the same as saying that the substance with the lowest oxidation state is written closest to the electrode.</p>
</div>

<div class="text-block">
<p>If you had the chlorine half cell on the left-hand side in a different situation, then the convention still has to hold. The half cell would then be written:</p>
</div>

<div class="block-formula">
\text{Pt}_{(s)} \Big| \underbrace{2\text{Cl}^-_{(aq)}}_{\clap{\text{\color{#467abf}{chloride ions written closest to the electrode}}}{}}, \text{Cl}_{2(g)} \overbrace{\Big\|}^{\clap{\text{salt bridge}}{}} \boxed{\text{other half-cell}}
</div>

<h4>What does the E° value show in the Cl<sub>2</sub> / Cl<sup>-</sup>?</h4>

<div class="text-block">
<p>The value is positive and moderately high as E° values go. That means that the position of the Cl<sub>2</sub> / Cl<sup> -</sup> equilibrium lies more to the right than the hydrogen equilibrium. Chlorine is much more likely to pick up electrons than hydrogen ions are.</p>
</div>

<div class="block-formula">
\begin{gathered}
2\text{H}^+_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{H}_{2(g)} \\
\\
\underbrace{\text{Cl}_{2(g)} + 2\text{e}^- \xrightleftharpoons{} 2\text{Cl}^-_{(aq)}}_{\clap{\text{\color{#467abf}{this equilibrium lies further to the right}}}{}}
\end{gathered}
</div>

<div class="text-block">
<p>Chlorine is therefore quite good at removing electrons from other things. It is a good oxidising agent.</p>
</div>

<h3>Measuring Redox Potentials for Other Systems</h3>

<h4>The Fe<sup>2+</sup> / Fe<sup>3+</sup> system</h4>

<div class="text-block">
<p>Iron(II) ions are easily oxidised to iron(III) ions, and iron(III) ions are fairly easily reduced to iron(II) ions. The equilibrium we are interested in this time is:</p>
</div>

<div class="block-formula">
\text{Fe}^{3+}_{(aq)} + \text{e}^- \xrightleftharpoons{} \text{Fe}^{2+}_{(aq)}
</div>

<div class="text-block">
<p>To measure the redox potential of this, you would simply insert a platinum electrode into a beaker containing a solution containing both iron(II) and iron(III) ions (1 mol dm<sup>-3</sup> with respect to both), and couple this to a hydrogen electrode.</p>
<p>The cell diagram would look like this:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{Pt}_{(s)}[\text{H}_{2(g)}] \Big| 2\text{H}^+_{(aq)} \Big\| \text{Fe}^{3+}_{(aq)},\text{Fe}^{2+}_{(aq)} \Big| \text{Pt}_{(s)} \\
E^o = {+}0.77 \text{ V}
\end{gathered}
</div>

<div class="text-block">
<p>Notice that the E° value isn't as positive as the chlorine one. The position of the iron(III) / iron(II) equilibrium isn't as far to the right as the chlorine equilibrium. That means that Fe<sup>3+</sup> ions don't pick up electrons as easily as chlorine does. Chlorine is a stronger oxidising agent than Fe<sup>3+</sup> ions.</p>
</div>

<h4>Potassium dichromate(VI) as an oxidising agent</h4>

<div class="text-block">
<p>A commonly used oxidising agent, especially in organic chemistry, is potassium dichromate(VI) solution acidified with dilute sulfuric acid. The potassium ions are just spectator ions and aren't involved in the equilibrium in any way.</p>
<p>The equilibrium is more complicated this time because it contains more things:</p>
</div>

<div class="block-formula">
{\text{Cr}_2\text{O}_7}^{2-}_{(aq)} + 14\text{H}^+_{(aq)} + 6\text{e}^- \xrightleftharpoons{} 2\text{Cr}^{3+}_{(aq)} + 7\text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>The half cell would have a piece of platinum dipping into a solution containing all the ions (dichromate(VI) ions, hydrogen ions and chromium(III) ions) all at 1 mol dm<sup>-3</sup>.</p>
<p>There is yet another convention when it comes to writing these more complicated cell diagrams. Where there is more than one thing on either side of the equilibrium, square brackets are written around them to keep them tidy. The substances losing electrons are written next to the electrode, just as before.</p>
</div>

<div class="block-formula full-width">
\begin{gathered}
\boxed{\text{hydrogen electrode}} \Bigg\| \left[{\text{Cr}_2\text{O}_7}^{2-}_{(aq)} + 14\text{H}^+_{(aq)}\right],\left[2\text{Cr}^{3+}_{(aq)} + 7\text{H}_2\text{O}_{(l)}\right] \Bigg| \text{Pt}_{(s)} \\
\\
E^o = {+}1.33 \text{ V}
\end{gathered}
</div>

<h2>Including These New Redox Potentials in the Electrochemical Series</h2>

<div class="text-block">
<p>These values can be slotted seamlessly into the electrochemical series that so far has only included metals and hydrogen.</p>
</div>

<h3>An Updated Electrochemical Series</h3>

<table class="data-table one-center two-right">
<thead>
<tr><th>equilibrium</th><th>E°<br>/ V</th></tr>
</thead>
<tbody>
<tr><td>
<span class="inline-formula">
\text{Li}^+_{(aq)} + e^- \xrightleftharpoons{} \text{Li}_{(s)}
</span>
</td><td>-3.03</td></tr>
<tr><td>
<span class="inline-formula">
\text{K}^+_{(aq)} + e^- \xrightleftharpoons{} \text{K}_{(s)}
</span>
</div>
</td><td>-2.92</td></tr>
<tr><td>
<span class="inline-formula">
\text{Ca}^{2+}_{(aq)} + 2e^- \xrightleftharpoons{} \text{Ca}_{(s)}
</span>
</div>
</td><td>-2.87</td></tr>
<tr><td>
<span class="inline-formula">
\text{Na}^+_{(aq)} + e^- \xrightleftharpoons{} \text{Na}_{(s)}
</span>
</div>
</td><td>-2.71</td></tr>
<tr><td>
<span class="inline-formula">
\text{Mg}^{2+}_{(aq)} + 2e^- \xrightleftharpoons{} \text{Mg}_{(s)}
</span>
</td><td>-2.37</td></tr>
<tr><td>
<span class="inline-formula">
\text{Al}^{3+}_{(aq)} + 3e^- \xrightleftharpoons{} \text{Al}_{(s)}
</span>
</td><td>-1.66</td></tr>
<tr><td>
<span class="inline-formula">
\text{Zn}^{2+}_{(aq)} + 2e^- \xrightleftharpoons{} \text{Zn}_{(s)}
</span>
</td><td>-0.76</td></tr>
<tr><td>
<span class="inline-formula">
\text{Fe}^{2+}_{(aq)} + 2e^- \xrightleftharpoons{} \text{Fe}_{(s)}
</span>
</td><td>-0.44</td></tr>
<tr><td>
<span class="inline-formula">
\text{Pb}^{2+}_{(aq)} + 2e^- \xrightleftharpoons{} \text{Pb}_{(s)}
</span>
</td><td>-0.13</td></tr>
<tr><td>
<span class="inline-formula">
2H^+_{(aq)} + 2e^- \xrightleftharpoons{} H_{2(g)}
</span>
</td><td>0</td></tr>
<tr><td>
<span class="inline-formula">
\text{Cu}^{2+}_{(aq)} + 2e^- \xrightleftharpoons{} \text{Cu}_{(s)}
</span>
</td><td>+0.34</td></tr>
<tr><td>
<span class="inline-formula">
\text{Fe}^{3+}_{(aq)} + e^- \xrightleftharpoons{} \text{Fe}^{2+}_{(aq)}
</span>
</td><td>+0.77</td></tr>
<tr><td>
<span class="inline-formula">
\text{Ag}^+_{(aq)} + e^- \xrightleftharpoons{} \text{Ag}_{(s)}
</span>
</td><td>+0.80</td></tr>
<tr><td>
<span class="inline-formula">
{\text{Cr}_2\text{O}_7}^{2-}_{(aq)} + 14\text{H}^+_{(aq)} + 6\text{e}^- \xrightleftharpoons{} 2\text{Cr}^{3+}_{(aq)} + 7\text{H}_2\text{O}_{(l)}
</span>
</td><td>+1.33</td></tr>
<tr><td>
<span class="inline-formula">
\text{Cl}_{2(g)} + 2\text{e}^- \xrightleftharpoons{} 2\text{Cl}^-_{(aq)}
</span>
</td><td>+1.36</td></tr>
<tr><td>
<span class="inline-formula">
\text{Au}^{3+}_{(aq)} + 3e^- \xrightleftharpoons{} \text{Au}_{(s)}
</span>
</td><td>+1.50</td></tr>
</tbody></table>

<div class="text-block">
<p>By coincidence, all the new equilibria we've looked at have positive E° values. It so happens that most of the equilibria with negative E° values that you meet at this level are ones involving simple metal / metal ion combinations.</p>
</div>

<div class="summary">
<ul>
<li>The more positive the E° value, the further the position of equilibrium lies to the right.</li>
<li>That means that the more positive the E° value, the more likely the substances on the left-hand side of the equations are to pick up electrons.</li>
<li>A substance which picks up electrons from something else is an oxidising agent.</li>
<li>The more positive the E° value, the stronger the substances on the left-hand side of the equation are as oxidising agents.</li>
</ul>
</div>

<div class="text-block">
<p>Of the new ones we've added to the electrochemical series:</p>
</div>

<ul>
<li>Chlorine gas is the strongest oxidising agent (E° = +1.36 v).</li>
<li>A solution containing dichromate(VI) ions in acid is almost as strong an oxidising agent (E° = +1.33 v).</li>
<li>Iron(III) ions are the weakest of the three new ones (E° = +0.77 v).</li>
<li>None of these three are as strong an oxidising agent as Au<sup>3+</sup> ions (E° = +1.50 v).</li>
</ul>

<div class="text-block">
<p>There will be more to say about this later in this series of pages.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-eo3.pdf" target="_blank">Questions on redox potentials for other systems</a>
<a href="../questions/a-eo3.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="combinations.html#top">To the next page on electrode potentials</a>
<a href="../redoxeqiamenu.html#top">To the redox equilibria menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>