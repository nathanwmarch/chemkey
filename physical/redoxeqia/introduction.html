<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to Redox Equilibria and Electrode Potentials | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how electrode potentials of metal – metal ion systems arise, and how they are measured. Introduces the important conventions used.">
<meta name="keywords" content="equilibrium, equilibria, redox, potential, electrode, standard, hydrogen electrode, reference electrode, cell, half cell">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to Redox Equilibria and Electrode Potentials</h1>

<div class="text-block">
<p>This page explains the background to standard electrode potentials (redox potentials), showing how they arise from simple equilibria, and how they are measured.</p>
<p>There are as many ways of teaching this as there are teachers and writers, and too many people make the fundamental mistake of forgetting that these are just simple equilibria. Too often, you will find the equations involved written as one-way rather than reversible. That small mistake makes the whole topic quite unnecessarily difficult to understand.</p>
<p>The approach you will find on this page (and whenever redox potentials are discussed on this site) avoids this problem completely by always talking in terms of equilibria.</p>
</div>

<div class="note">
<p>Important: If you aren't too happy about <a href="../equilibmenu.html#top">simple equilibria</a> (particularly about Le Chatelier's Principle), you should explore the equilibrium section of this site before you go any further.</p>
<p>The whole of this topic would also be a nightmare if you didn't understand about <a href="../../inorganic/redoxmenu.html#top">redox reactions</a>.</p>
</div>

<h2>Background</h2>

<h3>The Differing Reactivities of Metals</h3>

<div class="text-block">
<p>When metals react, they give away electrons and form positive ions. This particular topic sets about comparing the ease with which a metal does this to form hydrated ions in solution – for example, Mg<sup>2+</sup><sub>(aq)</sub> or Cu<sup>2+</sup><sub>(aq)</sub>.</p>
<p>We might want to compare the ease with which these two changes take place:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{Mg}_{(s)} \longrightarrow \text{Mg}^{2+}_{(aq)} + 2\text{e}^- \\
\\
\text{Cu}_{(s)} \longrightarrow \text{Cu}^{2+}_{(aq)} + 2\text{e}^-
\end{gathered}
</div>

<div class="text-block">
<p>Everybody who has done chemistry for more than a few months knows that magnesium is more reactive than copper. The first reaction happens much more readily than the second one. What this topic does is to try to express this with some numbers.</p>
</div>

<h3>Looking at This From an Equilibrium Point of View</h3>

<div class="text-block">
<p>Suppose you have a piece of magnesium in a beaker of water. There will be some tendency for the magnesium atoms to shed electrons and go into solution as magnesium ions. The electrons will be left behind on the magnesium.</p>
</div>

<div class="image-container"><img src="mgequil1.gif"></div>

<div class="text-block">
<p>In a very short time, there will be a build-up of electrons on the magnesium, and it will be surrounded in the solution by a layer of positive ions. These will tend to stay close because they are attracted to the negative charge on the piece of metal.</p>
<p>Some of them will be attracted enough that they will reclaim their electrons and stick back on to the piece of metal.</p>
</div>

<div class="image-container"><img src="mgequil2.gif"></div>

<div class="text-block">
<p>A dynamic equilibrium will be established when the rate at which ions are leaving the surface is exactly equal to the rate at which they are joining it again. At that point there will be a constant negative charge on the magnesium, and a constant number of magnesium ions present in the solution around it.</p>
<p>Simplifying the diagram to get rid of the "bites" out of the magnesium, you would be left with a situation like this:</p>
</div>

<div class="image-container"><img src="mgequil3.gif"></div>

<div class="text-block">
<p>Don't forget that this is just a snapshot of a dynamic equilibrium. Ions are continually leaving and rejoining the surface.</p>
<p>How would this be different if you used a piece of copper instead of a piece of magnesium?</p>
<p>Copper is less reactive and so forms its ions less readily. Any ions which do break away are more likely to reclaim their electrons and stick back on to the metal again. You will still reach an equilibrium position, but there will be less charge on the metal, and fewer ions in solution.</p>
</div>

<div class="image-container"><img src="mgvcudiag.gif"></div>

<div class="text-block">
<p>If we write the two reactions as equilibria, then what we are doing is comparing the two positions of equilibrium.</p>
<p>The position of the magnesium equilibrium</p>
</div>

<div class="block-formula">
 \text{Mg}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Mg}_{(s)}
</div>

<div class="text-block">
<p> lies further to the left than that of the copper equilibrium.</p>
</div>

<div class="block-formula">
 \text{Cu}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Cu}_{(s)}
</div>

<div class="text-block">
<p>Notice the way that the two equilibria are written. By convention, all these equilibria are written with the electrons on the left-hand side of the equation. If you stick with this convention without fail, you will find that it makes the rest of this topic much easier to visualise.</p>
<p>Everything else concerning electrode potentials is simply an attempt to attach some numbers to these differing positions of equilibrium.</p>
<p>In principle, that is quite easy to do. In the magnesium case, there is a lot of difference between the negativeness of the metal and the positiveness of the solution around it. In the copper case, the difference is much less.</p>
<p>This potential difference could be recorded as a voltage – the bigger the difference between the positiveness and the negativeness, the bigger the voltage. Unfortunately, that voltage is impossible to measure!</p>
<p>It would be easy to connect a voltmeter to the piece of metal, but how would you make a connection to the solution? By putting a probe into the solution near the metal? No – it wouldn't work!</p>
<p>Any probe you put in is going to have a similar sort of equilibrium happening around it. The best you could measure would be some sort of combination of the effects at the probe and the piece of metal you are testing.</p>
</div>

<h3>Understanding the Ideas Behind a Reference Electrode</h3>

<div class="text-block">
<p>Suppose you had an optical device for measuring heights some distance away, and wanted to use it to find out how tall a particular person was. Unfortunately, you can't see their feet because they are standing in long grass.</p>
</div>

<div class="image-container"><img src="grass1.gif"></div>

<div class="text-block">
<p>Although you can't measure their absolute height, what you can do is to measure their height relative to the convenient post. Suppose that in this case, the person turned out to be 15 cm taller than the post.</p>
<p>You could repeat this for a range of people</p>
</div>

<div class="image-container"><img src="grass2.gif"></div>

<div class="text-block">
<p> and come up with a set of results like this:</p>
</div>

<table class="data-table two-right">
<tbody><tr><th>person</th><th>height relative to post<br>/ cm</th></tr>
<tr><td>C</td><td>+20</td></tr>
<tr><td>A</td><td>+15</td></tr>
<tr><td>B</td><td>-15</td></tr>
</tbody></table>

<div class="text-block">
<p>Although you don't know any of their absolute heights, you can usefully rank them in order, and do some very simple sums to work out exactly how much taller one is than another. For example, C is 5 cm taller than A.</p>
<p>This turns out to be exactly what we need to do with the equilibria we started talking about. We don't actually need to know the absolute position of any of these equilibria. Going back to the magnesium and copper equilibria:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{Mg}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Mg}_{(s)} \\
\\
\text{Cu}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Cu}_{(s)} \\
\end{gathered}
</div>

<div class="text-block">
<p>All we need to know is that the magnesium equilibrium lies further to the left than the copper one. We need to know that magnesium sheds electrons and forms ions more readily than copper does.</p>
<p>That means that we don't need to be able to measure the absolute voltage between the metal and the solution. It is enough to compare the voltage with a standardised system called a reference electrode.</p>
<p>The system used is called a standard hydrogen electrode.</p>
</div>

<h2>Measuring Standard Electrode Potentials (Standard Redox Potentials)</h2>

<div class="note">
<p>Note: It is going to take a while before these terms get defined. Be patient! It is more important to fully understand what is going on first.</p>
</div>

<h3>The Standard Hydrogen Electrode</h3>

<div class="text-block">
<p>The standard hydrogen electrode looks like this:</p>
</div>

<div class="image-container"><img src="helectrode.gif"></div>

<h4>What is happening?</h4>

<div class="text-block">
<p>As the hydrogen gas flows over the porous platinum, an equilibrium is set up between hydrogen molecules and hydrogen ions in solution. The reaction is catalysed by the platinum.</p>
</div>

<div class="block-formula">
2\text{H}^+_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{H}_{2(g)}
</div>

<div class="text-block">
<p>This is the equilibrium that we are going to compare all the others with.</p>
</div>

<h4>Standard conditions</h4>

<div class="text-block">
<p>The position of any equilibrium can be changed by changing conditions. That means that the conditions must be standardised so that you can make fair comparisons.</p>
<p>The hydrogen pressure is 1 bar (100 kPa). (You may find 1 atmosphere quoted in older sources.) The temperature is 298 K (25°C).</p>
<p>The concentration of the hydrogen ions in solution is also important. Changing concentrations is one of the ways of changing the position of an equilibrium. Throughout this topic, all ion concentrations are taken as being 1 mol dm<sup>-3</sup>.</p>
</div>

<h3>Using the Standard Hydrogen Electrode</h3>

<div class="text-block">
<p>The standard hydrogen electrode is attached to the electrode system you are investigating – for example, a piece of magnesium in a solution containing magnesium ions.</p>
</div>

<div class="image-container"><img src="eomgdiag.gif"></div>

<h4>Cells and half cells</h4>

<div class="text-block">
<p>The whole of this set-up is described as a cell. It is a simple system which generates a voltage. Each of the two beakers and their contents are described as half cells.</p>
</div>

<h4>The salt bridge</h4>

<div class="text-block">
<p>The salt bridge is included to complete the electrical circuit but without introducing any more bits of metal into the system. It is just a glass tube filled with an electrolyte like potassium nitrate solution. The ends are "stoppered" by bits of cotton wool. This stops too much mixing of the contents of the salt bridge with the contents of the two beakers.</p>
<p>The electrolyte in the salt bridge is chosen so that it doesn't react with the contents of either beaker.</p>
</div>

<h4>What happens?</h4>

<div class="text-block">
<p>These two equilibria are set up on the two electrodes (the magnesium and the porous platinum):</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{Mg}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Mg}_{(s)} \\
\\
2\text{H}^+_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{H}_{2(g)}
\end{gathered}
</div>

<div class="text-block">
<p>Magnesium has a much greater tendency to form its ions than hydrogen does. The position of the magnesium equilibrium will be well to the left of that of the hydrogen equilibrium.</p>
<p>That means that there will be a much greater build-up of electrons on the piece of magnesium than on the platinum. Stripping all the rest of the diagram out, apart from the essential bits:</p>
</div>

<div class="image-container"><img src="h2vmgdiag.gif"></div>

<div class="text-block">
<p>There is a major difference between the charge on the two electrodes – a potential difference which can be measured with a voltmeter. The voltage measured would be 2.37 volts and the voltmeter would show the magnesium as the negative electrode and the hydrogen electrode as being positive.</p>
<p>This sometimes confuses people! Obviously, the platinum in the hydrogen electrode isn't positive in real terms – there is a slight excess of electrons built up on it. But voltmeters don't deal in absolute terms – they simply measure a difference.</p>
<p>The magnesium has the greater amount of negativeness – the voltmeter records that as negative. The platinum of the hydrogen electrode isn't as negative – it is relatively more positive. The voltmeter records it as positive.</p>
<p>Throughout the whole of this redox potential work, you have to think in relative terms. For example, +0.4 is relatively more negative than +1.2. Or, another example, -0.3 is relatively more positive than -0.9.</p>
</div>

<h4>What if you replace the magnesium half cell by a copper one?</h4>

<div class="text-block">
<p>This means replacing the magnesium half cell by one with a piece of copper suspended in a solution containing Cu<sup>2+</sup> ions with a concentration of 1 mol dm<sup>-3</sup>. You would probably choose to use copper(II) sulfate solution.</p>
<p>Copper forms its ions less readily than hydrogen does. Of the two equilibria</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{Cu}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Cu}_{(s)} \\
\\
2\text{H}^+_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{H}_{2(g)}
\end{gathered}
</div> 

<div class="text-block">
<p> the hydrogen one lies further to the left. That means that there will be less build-up of electrons on the copper than there is on the platinum of the hydrogen electrode.</p>
</div>

<div class="image-container"><img src="h2vcudiag.gif"></div>

<div class="text-block">
<p>There is less difference between the electrical charges on the two electrodes, so the voltage measured will be less. This time it is only 0.34 volts.</p>
<p>The other major change is that this time the copper is the more positive (less negative) electrode. The voltmeter will show the hydrogen electrode as the negative one and the copper electrode as positive.</p>
</div>

<h4>The voltmeter</h4>

<div class="text-block">
<p>You may have noticed that the voltmeter was described further up the page as having a "high resistance". Ideally, it wants to have an infinitely high resistance.</p>
<p>This is to avoid any flow of current through the circuit. If there was a low resistance in the circuit, electrons would flow from where there are a lot of them (around the magnesium, for example) to where there are less (on the hydrogen electrode).</p>
<p>If any current flows, the voltage measured drops. In order to make proper comparisons, it is important to measure the maximum possible voltage in any situation. This is called the electromotive force or emf.</p>
<p>The emf of a cell measured under standard conditions is given the symbol E°<sub>cell</sub>.</p>
</div>

<div class="note">
<p>Note: You read E° as "E nought" or "E standard".</p>
<p>For technical reasons to do with the way that people use different browsers which may be set to display text differently, it has been difficult to think of a way of showing the "standard" symbol in the text that will display reliably in all browsers.The symbol should actually be a circle with a horizontal line through it.</p>
</div>

<h3>Cell Conventions</h3>

<h4>A quick way of drawing a cell</h4>

<div class="text-block">
<p>Drawing a full diagram to represent a cell takes too long. Instead, the cell in which a magnesium electrode is coupled to a hydrogen electrode is represented like this:</p>
</div>

<div class="image-container"><img src="cellconvs.gif"></div>

<div class="text-block">
<p>You will often find variants on the way the hydrogen electrode is represented, such as:</p>
</div>

<div class="image-container"><img src="althelectrode.gif"></div>

<h4>Attaching a sign to the cell voltage</h4>

<div class="text-block">
<p>The convention is that you show the sign of the right-hand electrode (as you have drawn it) when you quote the E°<sub>cell</sub> value. For example:</p>
</div>

<div class="image-container"><img src="cellconvs2.gif"></div>

<div class="text-block">
<p>In the copper case:</p>
</div>

<div class="image-container"><img src="cellconvs3.gif"></div>

<h3>Defining Standard Electrode Potential (Standard Redox Potential)</h3>

<div class="text-block">
<p>The values that we have just quoted for the two cells are actually the standard electrode potentials of the Mg<sup>2+</sup> / Mg and Cu<sup>2+</sup> / Cu systems.</p>
<p>The emf measured when a metal / metal ion electrode is coupled to a hydrogen electrode under standard conditions is known as the standard electrode potential of that metal / metal ion combination.</p>
<p>By convention, the hydrogen electrode is always written as the left-hand electrode of the cell. That means that the sign of the voltage quoted always gives you the sign of the metal electrode.</p>
<p>Standard electrode potential is given the symbol E°.</p>
</div>

<div class="note">
<p>Note: In case you are wondering about the alternative name (standard redox potential), this comes from the fact that loss or gain of electrons is a redox reaction. This will be explored in later pages in this series of linked pages.</p>
</div>

<h2>Summarizing What Standard Electrode Potentials Tell You</h2>

<div class="text-block">
<p>Remember that the standard electrode potential of a metal / metal ion combination is the emf measured when that metal / metal ion electrode is coupled to a hydrogen electrode under standard conditions.</p>
<p>What you are doing is comparing the position of the metal / metal ion equilibrium with the equilibrium involving hydrogen.</p>
<p>Here are a few typical standard electrode potentials:</p>
</div>

<table class="data-table one-center two-right">
<thead>
<tr><th>metal ion / metal combination</th><th>E°<br>/ volts</th></tr>
</thead>
<tbody>
<tr><td>Mg<sup>2+</sup> / Mg</td><td>-2.37</td></tr>
<tr><td>Zn<sup>2+</sup> / Zn</td><td>-0.76</td></tr>
<tr><td>Cu<sup>2+</sup> / Cu</td><td>+0.34</td></tr>
<tr><td>Ag<sup>+</sup> / Ag</td><td>+0.80</td></tr>
</tbody></table>

<div class="text-block">
<p>Remember that each of these is comparing the position of the metal / metal ion equilibrium with the equilibrium involving hydrogen.</p>
<p>Here are the five equilibria (including the hydrogen one):</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{Mg}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Mg}_{(s)} \\
\\
\text{Zn}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Zn}_{(s)} \\
\\
2\text{H}^+_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{H}_{2(g)} \\
\\
\text{Cu}^{2+}_{(aq)} + 2\text{e}^- \xrightleftharpoons{} \text{Cu}_{(s)} \\
\\
\text{Ag}^+_{(aq)} + \text{e}^- \xrightleftharpoons{} \text{Ag}_{(s)}
\end{gathered}
</div>

<div class="text-block">
<p>If you compare these with the E° values, you can see that the ones whose positions of equilibrium lie furthest to the left have the most negative E° values. That is because they form ions more readily – and leave more electrons behind on the metal, making it more negative.</p>
<p>Those which don't shed electrons as readily have positions of equilibrium further to the right. Their E° values get progressively more positive.</p>
</div>

<div class="note">
<p>Note: Remember that, in each case, we are comparing the position of equilibrium with the hydrogen equilibrium. For example, we aren't saying that an equilibrium lies to the left in absolute terms – just that it is further to the left than the hydrogen equilibrium.</p>
</div>

<div class="summary">
<p>E° values give you a way of comparing the positions of equilibrium when these elements lose electrons to form ions in solution.</p>
<ul>
<li>The more negative the E° value, the further the equilibrium lies to the left – the more readily the element loses electrons and forms ions.</li>
<li>The more positive (or less negative) the E° value, the further the equilibrium lies to the right – the less readily the element loses electrons and forms ions.</li>
</ul>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-eo1.pdf" target="_blank">Questions on the introduction to standard electrode potentials</a>
<a href="../questions/a-eo1.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="ecs.html#top">To the next page on electrode potentials</a>
<a href="../redoxeqiamenu.html#top">To the redox equilibria menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>