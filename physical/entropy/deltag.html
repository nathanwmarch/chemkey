<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to Gibbs Free Energy | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An introduction to Gibbs free energy and its use in predicting the feasibility of a reaction.">
<meta name="keywords" content="Gibbs, free energy, feasible, feasibility">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to Gibbs Free Energy</h1>

<div class="text-block">
<p>This page introduces Gibbs free energy (often just called free energy), and shows how it can be used to predict the feasibility of reactions. If you have already read the page about how to do this with total entropy changes, you will find a little bit of repetition on this page.</p>
</div>

<h3>Standard Gibbs free energy change, &Delta;G°</h3>

<h4>Calculating &Delta;G°</h4>

<div class="text-block">
<p>This is how standard Gibbs free energy change is calculated:</p>
</div>

<div class="block-formula">
\Delta G^o = \Delta H^o - T\Delta S^o
</div>

<div class="text-block">
<p>That's all you need to know. Learn it!</p>
<p>If you know (or can work out) the enthalpy change for the reaction, and you know (or can work out) the entropy change, and you know the temperature (in kelvin), then it would seem to be really easy to work out &Delta;G°.</p>
<p>There is an easy mistake to be made though! You have to remember that the entropy change is calculated in energy units of joules, but &Delta;G° and &Delta;H° are both measured in kJ.</p>
<p>Because you are calculating &Delta;G, your answer will be in kJ mol<sup>-1</sup>. You must remember to change the entropy change value into kJ before you start, otherwise you will get the calculation completely wrong.</p>
<p>On an earlier page in this section, we calculated the entropy change for the reaction</p>
</div>

<div class="block-formula">
\text{CH}_{4(g)} + 2\text{O}_{2(g)} \longrightarrow \text{CO}_{2(g)} + 2\text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>&Delta;S° worked out as -242.2 J K<sup>-1</sup>mol<sup>-1</sup>.</p>
<p>Before you do anything else, convert this to kJ by dividing by 1000.</p>
</div>

<div class="block-formula">
\Delta S^o = {-}0.2422 \text{ kJ K}^{-1} \text{ mol}^{-1}
</div>

<div class="text-block">
<p>This reaction is actually the combustion of methane, and so we can just take a value of this from a Data Book. But you should, of course, know how to calculate this from enthalpy changes of formation.</p>
</div>

<div class="block-formula">
\Delta H^o = {-}890.4 \text{ kJ mol}^{-1}
</div>

<div class="text-block">
<p>So if you had to calculate the Gibbs free energy change at, say, 298 K, you can just slot the numbers in:</p>
</div>

<div class="block-formula">
\begin{aligned}
\Delta G^o &= \Delta H^o -T\Delta S^o \\
{} &= {-}890.4 - (298 \times {-}0.2442) \\
{} &= {-}817.6 \text{ kJ mol}^{-1}
\end{aligned}
</div>

<div class="text-block">
<p>It is easy as long as you remember to convert the entropy change value into kJ.</p>
</div>

<h3>Feasible (Spontaneous) Changes</h3>

<div class="text-block">
<p>In everyday life, something is spontaneous if it happens of its own accord, without any input from outside. The same thing is true in chemistry, but there is one major difference which defies everyday common sense.</p>
<p>If you drop marble chips (calcium carbonate) into dilute hydrochloric acid, there is an immediate fizzing. You don't need to do anything else – the reaction happens entirely of its own accord. It is a spontaneous change.</p>
<p>But in chemistry, a spontaneous change doesn't have to be rapid; in fact, it can be very, very, very slow indeed – even infinitely slow!</p>
<p>For example, carbon burns in oxygen to make carbon dioxide, but a piece of carbon will stay totally unchanged however long you keep it unless you first heat it. The energetics are right for a reaction to happen, but there is a huge activation energy.</p>
<p>Chemistry counts the reaction between carbon and oxygen as spontaneous! Personally, I think that is daft, and I prefer the word "feasible", which is often used in this topic. Throughout this topic I shall use the word "feasible", but if your examiners expect you to use the word "spontaneous", that is what you should do.</p>
</div>

<h3>Feasible Changes and &Delta;G</h3>

<div class="text-block">
<p>I am dropping the "standard" symbol after &Delta;G from now on, because most of the time we shall be using it at non-standard temperatures.</p>
<p>Whether or not a reaction (or other physical change) is feasible depends on the sign of &Delta;G. If &Delta;G is positive, then the reaction isn't feasible – it can't happen.</p>
<p>For a reaction to be feasible, &Delta;G has to be negative.</p>
<p>Remember that although it may be feasible, the reaction may not actually happen in any sensible time scale if there is a high activation energy barrier.</p>
</div>

<div class="note">
<p>Note: If your syllabus also wants you to work out feasibility in terms of &Delta;S<sub>total</sub>, then beware that the the signs for feasibility are the opposite way around. So for a reaction to be feasible, &Delta;S<sub>total</sub> has to be positive, but &Delta;G has to be negative.</p>
<p>This is potentially very confusing, and if you possible can, I suggest you stick to one method.</p>
</div>

<h3>&Delta;G Changes With Temperature</h3>

<div class="text-block">
<p>Here is the important equation again:</p>
</div>

<div class="block-formula">
\Delta G^o = \Delta H^o - T\Delta S^o
</div>

<div class="note">
<p>Note: Remember that I have removed the standard symbols. We are no longer going to be talking about standard conditions, and so these don't apply any more. At this level, we always make the approximation that the values of &Delta;H and &Delta;S aren't affected by temperature.</p>
</div>

<div class="text-block">
<p>Using this equation is easily illustrated using the decomposition of calcium carbonate:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{CaCO}_3 \longrightarrow \text{CaO} + \text{CO}_2 \\
\Delta H = {+}178 \text{ kJ mol}^{-1}
\end{gathered}
</div>

<div class="text-block">
<p>The value of &Delta;S for this reaction is +160.4 J K<sup>-1</sup> mol<sup>-1</sup>.</p>
</div>

<div class="note">
<p>Note: In an exam, you may well be sked to calculate both the enthalpy change and the entropy change. Make sure that you can do that. If you find yourself values for enthalpies of formation, and for standard entropies, you should get answers similar to the ones I am using. Data sources vary, and so don't be worried if your answer is slightly different. If it is a lot different, then worry about it!</p>
</div>

<div class="text-block">
<p>Remember that if you are using this in &Delta;G calculations, you first have to convert into kJ.</p>
</div>

<div class="block-formula">
\Delta S = {+}0.1604 \text{ kJ K}^{-1} \text{ mol}^{-1}
</div>

<div class="text-block">
<p>Suppose you had some calcium carbonate in the lab at 293 K. You can calculate a value of &Delta;G as:</p>
</div>

<div class="block-formula">
\begin{aligned}
\Delta G &= 178 - (293 \times 0.1604) \\
{} &= {+}131 \text{ kJ mol}^{-1}
\end{aligned}
</div>

<div class="text-block">
<p>The value is positive and so the reaction isn't feasible. It cannot happen at this temperature.</p>
<p>But suppose you heated it to 1000°C (1273 K). Recalculating gives:</p>
</div>

<div class="block-formula">
\begin{aligned}
\Delta G &= 178 - (1273 \times 0.1604) \\
{} &= {-}26.2 \text{ kJ mol}^{-1}
\end{aligned}
</div>

<div class="text-block">
<p>This value is negative, and so the reaction is feasible at this temperature. And you know, of course, that if you heat calcium carbonate strongly enough, it decomposes to give calcium oxide and carbon dioxide.</p>
<p>So how strongly do you have to heat it? You can work out an approximate temperature by finding out at what point &Delta;G becomes negative (i.e. less than 0).</p>
</div>

<div class="note">
<p>Note: It will only be an "approximate temperature" because of the approximations we make about &Delta;H and &Delta;S not changing with temperature. Don't worry about this at this level.</p>
</div>

<div class="text-block">
<p>For a reaction to be feasible, the value of &Delta;G has to be less than 0. In mathematical terms, it is feasible if:</p>
</div>

<div class="block-formula">
\Delta G < 0
</div>

<div class="text-block">
<p>Because &Delta;G = &Delta;H – T&Delta;S, that means that for a feasible reaction:</p>
</div>

<div class="block-formula">
\Delta H - T\Delta S < 0
</div>

<div class="text-block">
<p>If you know values for &Delta;H and &Delta;S, then you can work out a value for T which makes this expression less than 0.</p>
<p>In this case we are looking at &Delta;H = +178 kJ mol<sup>-1</sup>, and &Delta;S = +0.1604 kJ K<sup>-1</sup> mol<sup>-1</sup></p>
<p>Putting those values into the expression &Delta;H – T&Delta;S &lt; 0 gives</p>
</div>

<div class="block-formula">
178 - T \times 0.1604 < 0
</div>

<div class="text-block">
<p>You can treat the "less than" sign just like an equals sign, and so rearranging this gives:</p>
</div>

<div class="block-formula">
\begin{aligned}
178 &< T \times 0.1604 \\
\frac{178}{0.1604} &< T \\
1110 &< T
\end{aligned}
</div>

<div class="text-block">
<p>That's a strange way of looking at it, of course ("1110 is less than T."). But that is just the same as saying that T has to be greater than 1110 K.</p>
</div>

<h3>Working Out the Effect of Temperature Without Doing Calculations</h3>

<div class="text-block">
<p>Look again at the equation:</p>
</div>

<div class="block-formula">
\Delta G^o = \Delta H^o - T\Delta S^o
</div>

<div class="text-block">
<p>Remember that for a reaction to be feasible, &Delta;G has to be negative.</p>
<p>&Delta;H could be negative (an exothermic reaction) or positive (an endothermic reaction). Similarly &Delta;S could be either positive or negative.</p>
<p>There are four possible combinations of the signs of &Delta;H and &Delta;S. I want to look at those in turn.</p>
</div>

<h4>Where &Delta;H is negative and &Delta;S is positive</h4>

<div class="text-block">
<p>In the equation &Delta;G = &Delta;H – T&Delta;S:<br>if <span class="attention">&Delta;H is negative</span> and <span class="attention">T&Delta;S is positive</span> (and therefore<span class="second-attention">-T&Delta;S is negative</span>):</p>
<p>Both terms are negative irrespective of the temperature, and so &Delta;G is also bound to be negative. <span class="attention">The reaction will be feasible at all temperatures</span>.</p>
</div>

<h4>Where &Delta;H is positive and &Delta;S is negative</h4>

<div class="text-block">
<p>In the equation &Delta;G = &Delta;H – T&Delta;S:<br>if <span class="attention">&Delta;H is positive</span> and <span class="attention">T&Delta;S is negative</span> (and therefore <span class="second-attention">-T&Delta;S is positive</span>):</p>
<p>Both terms are positive irrespective of the temperature, and so &Delta;G is also bound to be positive. <span class="attention">The reaction will not be feasible at any temperature</span>.</p>
</div>

<h4>Where &Delta;H is positive and &Delta;S is positive</h4>

<div class="text-block">
<p>In the equation &Delta;G = &Delta;H – T&Delta;S:<br>if <span class="attention">&Delta;H is positive</span> and <span class="attention">T&Delta;S is positive</span> (and therefore <span class="second-attention">-T&Delta;S is negative</span>):</p>
<p>Now increasing the temperature will change things. At higher temperatures, -T&Delta;S will become more and more negative, and will eventually outweigh the effect of &Delta;H.</p>
<p><span class="attention">The reaction won't be feasible at low temperatures, but if you heat it, there will be a temperature at which it becomes feasible</span>, because &Delta;G becomes negative.</p>
<p>The decomposition of calcium carbonate is a case like this, and we have done three calculations around it.</p>
</div>

<h4>Where &Delta;H is negative and &Delta;S is negative</h4>

<div class="text-block">
<p>In the equation &Delta;G = &Delta;H – T&Delta;S:<br> if <span class="attention">&Delta;H is negative</span> and <span class="attention">T&Delta;S is negative</span> (and therefore <span class="second-attention">-T&Delta;S is positive</span>:</p>
<p>Again there will be a temperature effect. As temperature increases, -T&Delta;S will become more and more positive, and will eventually outweigh the effect of &Delta;H.</p>
<p><span class="attention">At low temperatures, &Delta;G will be negative because of the effect of the negative &Delta;H, but as you increase the temperature, the effect of the positive -T&Delta;S will eventually outweigh that. The value of &Delta;G will then become positive, and the reaction will no longer be feasible.</span></p>
</div>

<div class="summary">
<table class="data-table all-center">
<thead>
<th>&Delta;H</th>
<th>&Delta;S</th>
<th>T&Delta;S</th>
<th>&#x2011;T&Delta;S</th>
<th>&Delta;G</th>
<th>effect of temperature</th>
</thead>
<tbody>
<tr>
<td>–</td>
<td>+</td>
<td>+</td>
<td>–</td>
<td>–</td>
<td>&Delta;G negative at all temperatures</td>
</tr>
<tr>
<td>+</td>
<td>–</td>
<td>–</td>
<td>+</td>
<td>+</td>
<td>&Delta;G positive at all temperatures</td>
</tr>
<tr>
<td>+</td>
<td>+</td>
<td>+</td>
<td>–</td>
<td>+/–</td>
<td>&Delta;G positive at low temperatures and negative at high temperatures</td>
</tr>
<tr>
<td>–</td>
<td>+</td>
<td>+</td>
<td>–</td>
<td>–/+</td>
<td>&Delta;G negative at low temperatures and positive at high temperatures</td>
</tr>
</tbody>
</table>
</div>

<h4>In conclusion</h4>

<div class="text-block">
<p>I really wouldn't suggest you tried to learn this – it is too confusing. Make sure that you understand it, so that when a question comes up you can work it out at the time.</p>
<p>You should where possible practise by finding questions from past papers from your examiners, checking them carefully against the mark schemes and examiner's reports if they are available. That way you will get a feel for what they think is important.</p>
<p>But it isn't too difficult to make up questions for yourself which would help you through the whole topic. Choose a simple reaction where you can find entropy values for everything involved. Use a data book if you have one, or find the equivalent information online, and avoid anything which involves solutions.</p>
<p>Use enthalpies of formation to work out the enthalpy change for the reaction. (If you use combustion reactions, for example, then you can just look up the enthalpy of combustion to save you the bother!)</p>
<p>Then work out the entropy change during the reaction. Decide what the effect of temperature will be on the feasibility of the reaction, and then work out values of &Delta;G at some widely different temperatures to see if you are right. (When you are doing those calculations, don't forget to correct the entropy change value from joules to kJ!)</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../entropymenu.html#top">To the entropy and free energy menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>