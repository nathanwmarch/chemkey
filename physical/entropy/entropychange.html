<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Calculating Entropy Changes | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Calculating entropy changes from data book values">
<meta name="keywords" content="entropy, change, calculate, calculation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Calculating Entropy Changes</h1>

<div class="text-block">
<p>This page looks at how you can calculate entropy changes during reactions from given values of entropy for each of the substances taking part.</p>
</div>

<div class="note">
<p>Note: If you haven't already read the page about <a href="introduction.html#top">introducing entropy</a>, you should do so before you go on.</p>
<p>This page deals only with entropy changes to the system. Entropy change to the surroundings and the total entropy change are dealt with on another page. If your syllabus doesn't mention all these different sorts, just ignore this comment.</p>
</div>

<h3>Standard Entropies, S°</h3>

<div class="text-block">
<p>Entropy is given the symbol S, and standard entropy (measured at 298 K and a pressure of 1 bar) is given the symbol S°. You might find the pressure quoted as 1 atmosphere rather than 1 bar in less recent sources. Don't worry about it – they are nearly the same. 1 bar is 100 kPa; 1 atmosphere is 101.325 kPa. Use whatever units the examiners give you.</p>
<p>The units of entropy are J K<sup>-1</sup>mol<sup>-1</sup>. The thing you must be most careful about is the fact that entropy is measured in joules, not kilojoules, unlike most of the other energy terms you will have come across.</p>
<p>In an exam, you will be given values for all the standard entropies you need.</p>
</div>

<h3>Entropy Changes, &Delta;S°</h3>

<div class="text-block">
<p>Working out entropy changes for a reaction is very easy.</p>
<p>You add up the entropies for everything you end up with, and take away the entropies of everything you started with.</p>
</div>

<div class="definition">
<p>change in entropy = final entropy - starting entropy</p>
<p>Or if you like things mathematical:</p>
<div class="block-formula">
\Delta S^o = \sum S^o(products) - \sum S^o(reactants)
</div>
</div>

<div class="text-block">
<p>Where &sum; (sigma) simply means "the sum of".</p>
<p>In the introductory page we looked at the following reaction and worked out that there would be a decrease in entropy. Let's now do the calculation.</p>
</div>

<div class="block-formula">
\text{CH}_{4(g)} + 2\text{O}_{2(g)} \longrightarrow \text{CO}_{2(g)} + 2\text{H}_2\text{O}_{(l)}
</div>

<table class="data-table all-center">
<tbody><tr><td></td><td>CH<sub>4(g)</sub></td><td>O<sub>2(g)</sub></td><td>CO<sub>2(g)</sub></td><td>H<sub>2</sub>O<sub>(l)</sub></td></tr>
<tr><td>S° (J K<sup>-1</sup>mol<sup>-1</sup>)</td><td>186</td><td>205</td><td>214</td><td>69.9</td></tr>
</tbody></table>

<div class="text-block">
<p>You started with 1 mole of methane and 2 moles of oxygen.</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{starting entropy} &= 186 + (2 \times 205) \\
{} &= 596 \text{ J K}^{-1} \text{ mol}^{-1}
\end{aligned}
</div>

<div class="text-block">
<p>You ended up with 1 mole of carbon dioxide and two moles of liquid water.</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{final entropy} &= 214 + (2 \times 69.9) \\
{} &= 353.8 \text{ J K}^{-1} \text{ mol}^{-1}
\end{aligned}
</div>

<div class="text-block">
<p>Entropy change = what you end up with – what you started with.</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{entropy change} &= 353.8 - 596 \\
{} &= {-}242.2 \text{ J K}^{-1} \text{ mol}^{-1}
\end{aligned}
</div>

<div class="text-block">
<p>Notice that it is a negative value. The entropy has decreased – as we predicted it would in the earlier page. That's because there is a decrease in the total number of gas molecules present.</p>
<p>And that is all there is to it! You will, of course, need to practise doing this until you are completely confident, but you will need to find your own examples. There are lots in my calculations book if you have a copy. You will find examples on pages 260 to the top of page 262, and in problems 15 and 16 in the end-of-chapter questions. For the purposes of this page, you can ignore any reference to the word "system".</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../entropymenu.html#top">To the entropy and free energy menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>