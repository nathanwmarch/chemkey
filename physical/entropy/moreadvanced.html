<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Taking Entropy Changes Further | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="The relationships between total entropy change, entropy change of the system and entropy change of the surroundings. Using total entropy change as a means of determining the feasibility of a reaction">
<meta name="keywords" content="entropy, change, calculate, calculation, system, surroundings, total, feasible, feasibility">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">
<h1>Taking Entropy Changes Further</h1>

<div class="text-block">
<p>This page considers various entropy changes: of the system, of the surroundings, and the total change. It goes on to look at how you can use the total entropy change to decide whether or not a reaction is feasible.</p>
<p>This is going to be quite a long page. Don't rush it.</p>
</div>

<div class="note">
<p>Note: If your syllabus doesn't specifically mention entropy change terms like system, surroundings and total, you could safely ignore this page. You will still probably have to be able to work out the feasibility of reactions, but that will be done by the rather less confusing use of an equation based on Gibbs free energy.</p>
<p>If you do need to read this page, make sure you have read the page explaining how you <a href="entropychange.html#top">calculate the entropy change of the system</a> first.</p>
</div>

<h3>Introducing Total Entropy Changes</h3>

<div class="text-block">
<p>If you only calculate the entropy change of the reaction (the entropy change of the system), you are leaving out an important factor.</p>
<p>Suppose your reaction is exothermic. Heat is given off to the surroundings, and that extra heat increases the entropy of the surroundings.</p>
<p>If you add more energy to the surroundings, the number of different possibilities for arranging the energy over the molecules increases. And so increasing the temperature increases the entropy of the surroundings.</p>
<p>The reverse is true for an endothermic change. An endothermic reaction will cool the surroundings, and so the entropy of the surroundings decreases.</p>
<p>What matters is the total entropy change, which is the sum of the entropy changes of the system and the surroundings.</p>
</div>

<div class="block-formula">
\Delta S_{total} = \Delta S_{surroundings} + \Delta S_{system}
</div>

<div class="note">
<p>Note: I have deliberately left out the "standard" symbols in this equation. That is because we shall be using this equation under non-standard conditions. You should be able to judge what is acceptable to your examiners by looking at how this is presented in your syllabus.</p>
</div>

<h4>Calculating the entropy change of the surroundings.</h4>

<div class="text-block">
<p>So far, you know how to work out the entropy change of the system for a given reaction if you are told the entropies of all the substances involved in the reaction.</p>
<p>There is a simple equation for the entropy change of the surroundings.</p>
</div>

<div class="block-formula">
\Delta S_{surroundings} = {-}\frac{\Delta H}{T}
</div>

<div class="text-block">
<p>&Delta;H is the enthalpy change for the reaction. T is the temperature.</p>
<p>That seems easy, but there is a major trap to fall in here, and if you manage to get through your course without falling into it at least once, you will have done really well!</p>
<p>There is a mismatch between the units of enthalpy change and entropy change. When you quote figures for enthalpy change they will have energy units of kJ. But entropy change is quoted in energy units of J.</p>
<p>That means that if you are calculating entropy change, you must multiply the enthalpy change value by 1000.</p>
<p>So if, say, you have an enthalpy change of -92.2 kJ mol<sup>-1</sup>, the value you must put into the equation is -92200 J mol<sup>-1</sup>.</p>
<p>If the temperature was 298 K</p>
</div>

<div class="block-formula">
\begin{aligned}
\Delta S_{surroundings} &= {-}\frac{-92200}{298} \\
{} &= {+}309 \text{ J K}^{-1} \text{ mol}^{-1}
\end{aligned}
</div>

<div class="text-block">
<p>Notice that the negative sign in the equation converts the negative exothermic enthalpy change into a positive entropy change. An exothermic change heats the surroundings, and increases the entropy of the surroundings.</p>
</div>

<h4>Working out the total entropy change</h4>

<div class="text-block">
<p>If, for example, the entropy change of the reaction (the system) was +112 J K<sup>-1</sup> mol<sup>-1</sup>, then the total entropy change would be</p>
</div>

<div class="block-formula">
309 + 112 = {+}421 \text{ J K}^{-1} \text{ mol}^{-1}
</div>

<h3>The Importance of Total Entropy Change</h3>

<div class="text-block">
<p>For a reaction to be feasible, the total entropy has to increase – in other words the sign of the total entropy change must be positive.</p>
</div>

<h5>So what does "feasible" mean in reaction terms?</h5>

<div class="text-block">
<p>A feasible reaction is one that is possible in terms of energy, but it doesn't mean that it will necessarily happen. Although energetically it might be feasible, it may have a large activation energy barrier that will slow it down, or even prevent it from happening altogether at a particular temperature.</p>
<p>If the total entropy change is negative (if entropy decreases) then the reaction isn't feasible.</p>
</div>

<h4>Feasible or spontaneous?</h4>

<div class="text-block">
<p>The word spontaneous is often used in place of feasible.</p>
<p>In everyday life, something is spontaneous if it happens of its own accord, without any input from outside. The same thing is true in chemistry, but there is one major difference which defies everyday common sense.</p>
<p>If you drop marble chips (calcium carbonate) into dilute hydrochloric acid, there is an immediate fizzing. You don't need to do anything else – the reaction happens entirely of its own accord. It is a spontaneous change.</p>
<p>But in chemistry, a spontaneous change doesn't have to be rapid; in fact, it can be very, very, very slow indeed – even infinitely slow!</p>
<p>For example, carbon burns in oxygen to make carbon dioxide, but a piece of carbon will stay totally unchanged however long you keep it unless you first heat it. The energetics are right for a reaction to happen, but there is a huge activation energy.</p>
<p>Chemistry counts the reaction between carbon and oxygen as spontaneous! Personally, I think that is daft, and I prefer the word "feasible", which is often used in this topic. However, If your examiners use the word spontaneous, then you will be expected to as well.</p>
</div>

<h4>Some examples</h4>

<h5>Example 1: Dissolving ammonium nitrate in water</h5>

<div class="text-block">
<p>This is a simple example of an endothermic change which nevertheless happens because there is a large increase in disorder when the crystal breaks up into its separate ions and mixes with the water.</p>
<p>The entropy change to the surroundings will be negative because of the cooling caused by the ammonium nitrate dissolving, but this is more than made up for by the large increase in the entropy of the system. So the total entropy change is positive, and the change is feasible – actually also literally spontaneous in this instance.</p>
</div>

<h5>Example 2: The reaction between concentrated ethanoic acid and solid ammonium carbonate</h5>

<div class="block-formula full-width">
2\text{CH}_3\text{COOH} + (\text{NH}_4)_2\text{CO}_3 \longrightarrow 2\text{CH}_3\text{COONH}_4 + \text{H}_2\text{O} + \text{CO}_2
</div>

<div class="text-block">
<p>This is another endothermic change which becomes feasible because the increase in entropy due to the gaseous carbon dioxide formed outweighs the fall in entropy of the surroundings. The total entropy increases and so the reaction is feasible (and again literally spontaneous).</p>
</div>

<h5>Example 3: The reaction between magnesium ribbon and oxygen</h5>

<div class="block-formula">
2\text{Mg} + \text{O}_2 \longrightarrow 2\text{MgO}
</div>

<div class="text-block">
<p>This is simple to do as a calculation. We will work out the total entropy change if a reaction happened at room temperature (say 293 K).</p>
<p>The enthalpy change for the reaction is -1204 kJ mol<sup>-1</sup>, and the entropy change of the system is -216 J K<sup>-1</sup>mol<sup>-1</sup>.</p>
<p>The overall calculation looks like this, with the answer quoted to 3 signficant figures:</p>
</div>

<div class="block-formula">
\begin{aligned}
\Delta S_{surroundings} &= {-}\frac{\Delta H}{T} \\
{} &= {-}\frac{-1204000}{293} \\
{} &= 4,110 \text{ J K}^{-1} \text{ mol}^{-1}
\end{aligned}
</div>

<div class="block-formula">
\begin{aligned}
\Delta S_{total} &= {-}\frac{-1204000}{293} - 216 \\
{} &= {+}3890 \text{ J K}^{-1} \text{ mol}^{-1}
\end{aligned}
</div>

<div class="text-block">
<p>Don't forget to convert the enthalpy change into joules!</p>
<p>There has been a very large increase in entropy overall, so is the reaction feasible? Yes! Is it spontaneous (in the usual meaning of the word) at 293 K (room temperature)? No!</p>
<p>Magnesium may tarnish forming magnesium oxide or hydroxide very, very slowly in air, but you need to supply a lot of activation energy to get it to burn.</p>
<p>You mustn't assume that all feasible reactions actually happen quickly (or even at all) in the lab. </p>
</div>

<h5>Example 4: The reaction between solid hydrated barium hydroxide and solid ammonium chloride</h5>

<div class="block-formula full-width">
\text{Ba}(\text{OH})_2\centerdot8\text{H}_2\text{O} + 2\text{NH}_4\text{Cl} \longrightarrow \text{BaCl}_2 + 10\text{H}_2\text{O} + 2\text{NH}_3
</div>

<div class="text-block">
<p>I am including this rather obscure reaction because it is mentioned specifically by one of the UK's A-level Exam Boards.</p>
<p>The enthalpy change for the reaction is +164 kJ mol<sup>-1</sup> assuming the barium chloride is as shown in the equation, or somewhat less if it is formed as BaCl<sub>2</sub>,2H<sub>2</sub>O.</p>
<p>Assuming it is produced as BaCl<sub>2</sub>, the entropy change for the system is +591J K<sup>-1</sup>mol<sup>-1</sup>. That is a high value because of the gas and liquid molecules being formed from two more ordered solids.</p>
<p>If you use these figures to calculate the total entropy change, and assuming a temperature of 293 K, you should find that it comes to +31.3 J K<sup>-1</sup>mol<sup>-1</sup>. It is positive, and the reaction is feasible.</p>
</div>

<div class="note">
<p>Note: You will find this reaction described and explained in more detail in this page from the <a href="http://www.rsc.org/learn-chemistry/resource/res00000739/endothermic-solid-solid-reactions?cmpid=CMP00005021">RSC's Practical Chemistry project</a>. That page also contains all the raw data for the calculations involved. Work the whole lot out for yourself!</p>
If you find this link doesn't work, please contact me via the address on the <a href="../../about.html">about this site</a> page.
</div>

<h5>Example 5: Making benzene from its elements</h5>

<div class="block-formula">
6\text{C}_{(s)} + 3\text{H}_{2(g)} \longrightarrow \text{C}_6\text{H}_{6(l)}
</div>

<div class="text-block">
<p>The enthalpy change of the reaction is +49.0 kJ mol<sup>-1</sup>, and the entropy change of the system is -254 J K<sup>-1</sup>mol<sup>-1</sup>.</p>
<p>So it is an endothermic reaction with a decrease in entropy of the system. We don't need to do a calculation with this. It is quicker just to give it a bit of thought!</p>
<p>The entropy change of the surroundings is going to be negative because of the minus sign in the equation. Look back at the equation further up the page if you aren't sure.</p>
<p>Since the entropy change of the system is also negative, the total entropy change is bound to be negative whatever the temperature you choose. The reaction isn't feasible at any temperature.</p>
</div>

<h3>A Quick Introduction to Gibbs Free Energy</h3>

<div class="text-block">
<p>The next page in this sequence of pages looks at Gibbs free energy and how you can predict the feasibility of reactions using that concept. All I want to do for now is to see how this new term comes from what we have already discussed on this page.</p>
</div>

<div class="block-formula">
\Delta S_{total} = \Delta S_{surroundings} + \Delta S_{system}
</div>

<div class="text-block">
<p>You also know how the entropy change of the surroundings is related to the enthalpy change of the reaction:</p>
</div>

<div class="block-formula">
\Delta S_{surroundings} = {-}\frac{\Delta H}{T}
</div>

<div class="text-block">
<p>Putting these together, and rearranging slightly by multiplying everything by T to get rid of the fraction gives:</p>
</div>

<div class="block-formula">
\begin{aligned}
\Delta S_{total} &= {-}\frac{\Delta H}{T} + \Delta S_{system} \\
T\Delta S_{total} &= {-}\Delta H + T\Delta S_{system} \\
{-}T\Delta S_{total} &= \Delta H - T\Delta S_{system}
\end{aligned}
</div>

<div class="text-block">
<p>The term on the left-hand side is known as the Gibbs free energy, and is given the symbol &Delta;G. That means that you can write the fairly simple looking equation:</p>
</div>

<div class="block-formula">
\Delta G = \Delta H - T\Delta S
</div>

<div class="text-block">
<p>The &Delta;S in this version is always just the entropy change of the system.</p>
<p>You will see how this equation is used in the next page of this sequence.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../entropymenu.html#top">To the entropy and free energy menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>