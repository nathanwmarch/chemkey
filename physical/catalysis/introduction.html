<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Types of Catalysis | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A look at heterogeneous and homogeneous catalysis plus autocatalysis, with examples of each.">
<meta name="keywords" content="catalysis, catalyst, heterogeneous, homogeneous, autocatalysis">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Types of Catalysis</h1>

<div class="text-block">
<p>This page looks at the the different types of catalyst (heterogeneous and homogeneous) with examples of each kind, and explanations of how they work. You will also find a description of one example of autocatalysis – a reaction which is catalysed by one of its products.</p>
</div>

<div class="note">
<p>Note: This page doesn't deal with the <a href="../basicrates/catalyst.html#top">effect of catalysts on rates of reaction</a>. If you don't already know about that, you might like to follow this link first.</p>
</div>

<h2>Types of Catalytic Reactions</h2>

<div class="text-block">
<p>Catalysts can be divided into two main types – heterogeneous and homogeneous. In a heterogeneous reaction, the catalyst is in a different phase from the reactants. In a homogeneous reaction, the catalyst is in the same phase as the reactants.</p>
</div>

<h5>What is a phase?</h5>

<div class="text-block">
<p>If you look at a mixture and can see a boundary between two of the components, those substances are in different phases. A mixture containing a solid and a liquid consists of two phases. A mixture of various chemicals in a single solution consists of only one phase, because you can't see any boundary between them.</p>
</div>

<div class="image-container"><img src="phases1.gif"></div>

<div class="text-block">
<p>You might wonder why phase differs from the term physical state (solid, liquid or gas). It includes solids, liquids and gases, but is actually a bit more general. It can also apply to two liquids (oil and water, for example) which don't dissolve in each other. You could see the boundary between the two liquids.</p>
</div>

<div class="image-container"><img src="phases2.gif"></div>

<div class="text-block">
<p>If you want to be fussy about things, the diagrams actually show more phases than are labelled. Each, for example, also has the glass beaker as a solid phase. All probably have a gas above the liquid – that's another phase. We don't count these extra phases because they aren't a part of the reaction.</p>
</div>

<h2>Heterogeneous catalysis</h2>

<div class="text-block">
<p>This involves the use of a catalyst in a different phase from the reactants. Typical examples involve a solid catalyst with the reactants as either liquids or gases.</p>
</div>

<div class="note">
<p>Note: It is important that you remember the difference between the two terms heterogeneous and homogeneous.</p>
<p><i>hetero</i> implies different (as in heterosexual). Heterogeneous catalysis has the catalyst in a different phase from the reactants.</p>
<p><i>homo</i> implies the same (as in homosexual). Homogeneous catalysis has the catalyst in the same phase as the reactants.</p>
</div>

<h3>How the Heterogeneous Catalyst Works (in general terms)</h3>

<div class="text-block">
<p>Most examples of heterogeneous catalysis go through the same stages:</p>
</div>

<div class="sequence">
<div>
<p>One or more of the reactants are adsorbed on to the surface of the catalyst at active sites.</p>
<p>Adsorption is where something sticks to a surface. It isn't the same as absorption where one substance is taken up within the structure of another. Be careful!</p>
</div>
<div>
<p>An active site is a part of the surface which is particularly good at adsorbing things and helping them to react.</p>
<p>There is some sort of interaction between the surface of the catalyst and the reactant molecules which makes them more reactive.</p>
<p>This might involve an actual reaction with the surface, or some weakening of the bonds in the attached molecules.</p>
</div>
<div>
<p>The reaction happens.</p>
<p>At this stage, both of the reactant molecules might be attached to the surface, or one might be attached and hit by the other one moving freely in the gas or liquid.</p>
</div>
<div>
<p>The product molecules are desorbed.</p>
<p>Desorption simply means that the product molecules break away. This leaves the active site available for a new set of molecules to attach to and react.</p>
</div>
</div>

<div class="text-block">
<p>A good catalyst needs to adsorb the reactant molecules strongly enough for them to react, but not so strongly that the product molecules stick more or less permanently to the surface.</p>
<p>Silver, for example, isn't a good catalyst because it doesn't form strong enough attachments with reactant molecules. Tungsten, on the other hand, isn't a good catalyst because it adsorbs too strongly.</p>
<p>Metals like platinum and nickel make good catalysts because they adsorb strongly enough to hold and activate the reactants, but not so strongly that the products can't break away.</p>
</div>

<h3>Examples of Heterogeneous Catalysis</h3>

<h4>The hydrogenation of a carbon-carbon double bond</h4>

<div class="text-block">
<p>The simplest example of this is the reaction between ethene and hydrogen in the presence of a nickel catalyst.</p>
</div>

<div class="block-formula">
\text{CH}_2{=}\text{CH}_2 + \text{H}_2 \xrightarrow{\text{Ni}} \text{CH}_3\text{CH}_3
</div>

<div class="text-block">
<p>In practice, this is a pointless reaction, because you are converting the extremely useful ethene into the relatively useless ethane. However, the same reaction will happen with any compound containing a carbon-carbon double bond.</p>
<p>One important industrial use is in the hydrogenation of vegetable oils to make margarine, which also involves reacting a carbon-carbon double bond in the vegetable oil with hydrogen in the presence of a nickel catalyst.</p>
<p>Ethene molecules are adsorbed on the surface of the nickel. The double bond between the carbon atoms breaks and the electrons are used to bond it to the nickel surface.</p>
</div>

<div class="image-container"><img src="nickel1.gif"></div>

<div class="text-block">
<p>Hydrogen molecules are also adsorbed on to the surface of the nickel. When this happens, the hydrogen molecules are broken into atoms. These can move around on the surface of the nickel.</p>
</div>

<div class="image-container"><img src="nickel2.gif"></div>

<div class="text-block">
<p>If a hydrogen atom diffuses close to one of the bonded carbons, the bond between the carbon and the nickel is replaced by one between the carbon and hydrogen.</p>
</div>

<div class="image-container"><img src="nickel3.gif"></div>

<div class="text-block">
<p>That end of the original ethene now breaks free of the surface, and eventually the same thing will happen at the other end.</p>
</div>

<div class="image-container"><img src="nickel4.gif"></div>

<div class="text-block">
<p>As before, one of the hydrogen atoms forms a bond with the carbon, and that end also breaks free. There is now space on the surface of the nickel for new reactant molecules to go through the whole process again.</p>
</div>

<div class="image-container"><img src="nickel5.gif"></div>

<h4>Catalytic converters</h4>

<div class="text-block">
<p>Catalytic converters change poisonous molecules like carbon monoxide and various nitrogen oxides in car exhausts into more harmless molecules like carbon dioxide and nitrogen. They use expensive metals like platinum, palladium and rhodium as the heterogeneous catalyst.</p>
<p>The metals are deposited as thin layers onto a ceramic honeycomb. This maximises the surface area and keeps the amount of metal used to a minimum.</p>
<p>Taking the reaction between carbon monoxide and nitrogen monoxide as typical:</p>
</div>

<div class="block-formula">
2\text{CO} + 2\text{NO} \xrightarrow{\text{Pt/Pd/Rh}} 2\text{CO}_2 + \text{N}_2
</div>

<div class="note">
<p>Note: This is a simplification which is adequate for what most examiners at this level ask. The equation represents an overall change in the catalytic converter, but takes place in more than one stage. If you were interested, you could follow this up by reading this <a href="http://en.wikipedia.org/wiki/Catalytic_converter">Wikipedia page</a>.</p>
</div>

<div class="text-block">
<p>Catalytic converters can be affected by catalyst poisoning. This happens when something which isn't a part of the reaction gets very strongly adsorbed onto the surface of the catalyst, preventing the normal reactants from reaching it.</p>
<p>Lead is a familiar catalyst poison for catalytic converters. It coats the honeycomb of expensive metals and stops it working.</p>
<p>In the past, lead compounds were added to petrol (gasoline) to make it burn more smoothly in the engine. But you can't use a catalytic converter if you are using leaded fuel. So catalytic converters have not only helped remove poisonous gases like carbon monoxide and nitrogen oxides, but have also forced the removal of poisonous lead compounds from petrol.</p>
</div>

<h4>The use of vanadium(V) oxide in the Contact Process</h4>

<div class="text-block">
<p>During the Contact Process for manufacturing sulfuric acid, sulfur dioxide has to be converted into sulfur trioxide. This is done by passing sulfur dioxide and oxygen over a solid vanadium(V) oxide catalyst.</p>
</div>

<div class="block-formula">
\text{SO}_2 + \frac{1}{2}\text{O}_2 \xrightarrow{\text{V}_2\text{O}_5} \text{SO}_3
</div>

<div class="note">
<p>Note: The equation is written with the half in it to make the explanation below tidier. You may well be familiar with the equation written as twice that shown, but the present version is perfectly acceptable. It is also shown as a one-way rather than a reversible reaction to avoid complicating things.</p>
</div>

<div class="text-block">
<p>This example is slightly different from the previous ones because the gases actually react with the surface of the catalyst, temporarily changing it. It is a good example of the ability of transition metals and their compounds to act as catalysts because of their ability to change their oxidation state.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="../../inorganic/redox/oxidnstates.html#top">oxidation states</a>, it might be useful to follow this link before you go on.</p>
</div>

<div class="text-block">
<p>The sulfur dioxide is oxidised to sulfur trioxide by the vanadium(V) oxide. In the process, the vanadium(V) oxide is reduced to vanadium(IV) oxide.</p>
</div>

<div class="block-formula">
\text{SO}_2 + \text{V}_2\text{O}_5 \longrightarrow \text{SO}_3 + \text{V}_2\text{O}_4
</div>

<div class="text-block">
<p>The vanadium(IV) oxide is then re-oxidised by the oxygen.</p>
</div>

<div class="block-formula">
\text{V}_2\text{O}_4 + \frac{1}{2}\text{O}_2 \longrightarrow \text{V}_2\text{O}_5
</div>

<div class="image-container"><img src="contact3.gif"></div>

<div class="text-block">
<p>This is a good example of the way that a catalyst can be changed during the course of a reaction. At the end of the reaction, though, it will be chemically the same as it started.</p>
</div>

<h2>Homogeneous catalysis</h2>

<div class="text-block">
<p>This has the catalyst in the same phase as the reactants. Typically everything will be present as a gas or contained in a single liquid phase. The examples contain one of each of these</p>
</div>

<h3>Examples of Homogeneous Catalysis</h3>

<h4>The reaction between persulfate ions and iodide ions</h4>

<div class="text-block">
<p>This is a solution reaction that you may well only meet in the context of catalysis, but it is a lovely example!</p>
<p>Persulfate ions (peroxodisulfate ions), S<sub>2</sub>O<sub>8</sub><sup>2-</sup>, are very powerful oxidising agents. Iodide ions are very easily oxidised to iodine. And yet the reaction between them in solution in water is very slow.</p>
<p>If you look at the equation, it is easy to see why that is:</p>
</div>

<div class="block-formula">
{\text{S}_2\text{O}_8}^{2-} + 2\text{I}^- \longrightarrow 2{\text{SO}_4}^{2-} + \text{I}_2
</div>

<div class="text-block">
<p>The reaction needs a collision between two negative ions. Repulsion is going to get seriously in the way of that!</p>
<p>The catalysed reaction avoids that problem completely. The catalyst can be either iron(II) or iron(III) ions which are added to the same solution. This is another good example of the use of transition metal compounds as catalysts because of their ability to change oxidation state.</p>
<p>For the sake of argument, we'll take the catalyst to be iron(II) ions. As you will see shortly, it doesn't actually matter whether you use iron(II) or iron(III) ions.</p>
<p>The persulfate ions oxidise the iron(II) ions to iron(III) ions. In the process the persulfate ions are reduced to sulfate ions.</p>
</div>

<div class="block-formula">
{\text{S}_2\text{O}_8}^{2-} + 2\text{Fe}^{2+} \longrightarrow 2{\text{SO}_4}^{2-} + 2\text{Fe}^{3+}
</div>

<div class="text-block">
<p>The iron(III) ions are strong enough oxidising agents to oxidise iodide ions to iodine. In the process, they are reduced back to iron(II) ions again.</p>
</div>

<div class="block-formula">
2\text{Fe}^{3+} + 2\text{I}^- \longrightarrow 2\text{Fe}^{2+} + \text{I}_2
</div>

<div class="text-block">
<p>Both of these individual stages in the overall reaction involve collision between positive and negative ions. This will be much more likely to be successful than collision between two negative ions in the uncatalysed reaction.</p>
<p>What happens if you use iron(III) ions as the catalyst instead of iron(II) ions? The reactions simply happen in a different order.</p>
</div>

<h4>The destruction of atmospheric ozone</h4>

<div class="text-block">
<p>This is a good example of homogeneous catalysis where everything is present as a gas.</p>
<p>Ozone, O<sub>3</sub>, is constantly being formed and broken up again in the high atmosphere by the action of ultraviolet light. Ordinary oxygen molecules absorb ultraviolet light and break into individual oxygen atoms. These have unpaired electrons, and are known as <i>free radicals</i>. They are very reactive.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{O}_2 \xrightarrow{\text{ultraviolet light}} {\color{#467abf}{\bullet}}\text{O}{\color{#467abf}{\bullet}} + {\color{#467abf}{\bullet}}\text{O}{\color{#467abf}{\bullet}} \\
\\
{\color{#467abf}{\bullet}} \leftarrow \text{unpaired electron}
\end{gathered}
</div>

<div class="text-block">
<p>The oxygen radicals can then combine with ordinary oxygen molecules to make ozone.</p>
</div>

<div class="block-formula">
\text{O}_2 + {\bullet}\text{O}{\bullet} \longrightarrow \text{O}_3
</div>

<div class="text-block">
<p>Ozone can also be split up again into ordinary oxygen and an oxygen radical by absorbing ultraviolet light.</p>
</div>

<div class="block-formula">
\text{O}_3 \xrightarrow{\text{ultraviolet light}} \text{O}_2 + {\bullet}\text{O}{\bullet}
</div>

<div class="text-block">
<p>This formation and breaking up of ozone is going on all the time. Taken together, these reactions stop a lot of harmful ultraviolet radiation penetrating the atmosphere to reach the surface of the Earth.</p>
<p>The catalytic reaction we are interested in destroys the ozone and so stops it absorbing UV in this way.</p>
<p>Chlorofluorocarbons (CFCs) like CF<sub>2</sub>Cl<sub>2</sub>, for example, were used extensively in aerosols and as refrigerants. Their slow breakdown in the atmosphere produces chlorine atoms – chlorine free radicals. These catalyse the destruction of the ozone.</p>
<p>This happens in two stages. In the first, the ozone is broken up and a new free radical is produced.</p>
</div>

<div class="block-formula">
\text{Cl}{\bullet} + \text{O}_3 \longrightarrow \text{ClO}{\bullet} + \text{O}_2
</div>

<div class="text-block">
<p>The chlorine radical catalyst is regenerated by a second reaction. This can happen in two ways depending on whether the ClO radical hits an ozone molecule or an oxygen radical.</p>
<p>If it hits an oxygen radical (produced from one of the reactions we've looked at previously):</p>
</div>

<div class="block-formula">
\text{ClO}{\bullet} + {\bullet}\text{O}{\bullet} \longrightarrow \underbrace{\text{Cl}{\bullet}}_{\clap{\text{\color{#467abf}{chlorine radical formed}}}{}} + \text{O}_2
</div>

<div class="text-block">
<p>Or if it hits an ozone molecule:</p>
</div>

<div class="block-formula">
\text{ClO}{\bullet} + \text{O}_3 \longrightarrow \underbrace{\text{Cl}{\bullet}}_{\clap{\text{\color{#467abf}{chlorine radical formed}}}{}} + 2\text{O}_2
</div>

<div class="text-block">
<p>Because the chlorine radical keeps on being regenerated, each one can destroy thousands of ozone molecules.</p>
</div>

<div class="note">
<p>Note: If you are a UK A-level student it is probable that you will only want one of these last two equations depending on what your syllabus says. Unfortunately, at the time of writing, different A-level syllabuses were quoting different final equations. You need to check your current syllabus. If you don't have a copy of your <a href="../../syllabuses.html">syllabus</a>, follow this link to find out how to get one.</p>
</div>

<h2>Autocatalysis</h2>

<div class="text-block">
<p>The oxidation of ethanedioic acid by manganate(VII) ions</p>
<p>In autocatalysis, the reaction is catalysed by one of its products. One of the simplest examples of this is in the oxidation of a solution of ethanedioic acid (oxalic acid) by an acidified solution of potassium manganate(VII) (potassium permanganate).</p>
</div>

<div class="block-formula full-width">
2{\text{MnO}_4}^- + 6\text{H}^+ + 5~ \text{HOOC}{-}\text{COOH} \longrightarrow 2\text{Mn}^{2+} + 10\text{CO}_2 + 8\text{H}_2\text{O}
</div>

<div class="text-block">
<p>The reaction is very slow at room temperature. It is used as a titration to find the concentration of potassium manganate(VII) solution and is usually carried out at a temperature of about 60°C. Even so, it is quite slow to start with.</p>
<p>The reaction is catalysed by manganese(II) ions. There obviously aren't any of those present before the reaction starts, and so it starts off extremely slowly at room temperature. However, if you look at the equation, you will find manganese(II) ions amongst the products. More and more catalyst is produced as the reaction proceeds and so the reaction speeds up.</p>
<p>You can measure this effect by plotting the concentration of one of the reactants as time goes on. You get a graph quite unlike the normal rate curve for a reaction.</p>
</div>

<h5>Most reactions give a rate curve which looks like this:</h5>

<div class="image-container">
<img src="autocatgr1.gif">
</div>

<div class="text-block">
<p>Concentrations are high at the beginning and so the reaction is fast – shown by a rapid fall in the reactant concentration. As things get used up, the reaction slows down and eventually stops as one or more of the reactants are completely used up.</p>
</div>

<h5>An example of autocatalysis gives a curve like this:</h5>

<div class="image-container">
<img src="autocatgr2.gif">
</div>

<div class="text-block">
<p>You can see the slow (uncatalysed) reaction at the beginning. As catalyst begins to be formed in the mixture, the reaction speeds up – getting faster and faster as more and more catalyst is formed. Eventually, of course, the rate falls again as things get used up.</p>
</div>

<div class="note">
<p>Warning! Don't assume that a rate curve which looks like this necessarily shows an example of autocatalysis. There are other effects which might produce a similar graph.</p>
<p>For example, if the reaction involved a solid reacting with a liquid, there might be some sort of surface coating on the solid which the liquid has to penetrate before the expected reaction can happen.</p>
<p>A more common possibility is that you have a strongly exothermic reaction and aren't controlling the temperature properly. The heat evolved during the reaction speeds the reaction up.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<p>If this is the first set of questions you have done, please read the introductory page before you start.</p>
<a href="../questions/q-catalysistypes.pdf">Questions on types of catalysis</a>
<a href="../questions/a-catalysistypes.pdf">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>

<a href="../catalysismenu.html#top">To the catalysis menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>