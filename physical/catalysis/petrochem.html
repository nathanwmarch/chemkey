<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Catalysts in the Petrochemical Industry | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A look at a few processes in the petrochemical industry and the catalysts they use.">
<meta name="keywords" content="catalysis, catalyst, catalytic, cracking, reforming, isomerisation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Catalysts in the Petrochemical Industry</h1>

<div class="text-block">
<p>This page looks briefly at some of the basic processes in the petrochemical industry (cracking, isomerisation and reforming) as examples of important catalytic reactions.</p>
</div>

<h2>Catalytic Cracking</h2>

<div class="text-block">
<p>Cracking is the name given to breaking up large hydrocarbon molecules into smaller and more useful bits. This is achieved by using high pressures and temperatures without a catalyst, or lower temperatures and pressures in the presence of a catalyst.</p>
<p>The source of the large hydrocarbon molecules is often the naphtha fraction or the gas oil fraction from the fractional distillation of crude oil (petroleum). These fractions are obtained from the distillation process as liquids, but are re-vaporised before cracking.</p>
<p>The hydrocarbons are mixed with a very fine catalyst powder. These days the catalysts are zeolites (complex aluminosilicates) – these are more efficient than the older mixtures of aluminium oxide and silicon dioxide.</p>
<p>The whole mixture is blown rather like a liquid through a reaction chamber at a temperature of about 500°C. Because the mixture behaves like a liquid, this is known as fluid catalytic cracking (or fluidised catalytic cracking).</p>
<p>Although the mixture of gas and fine solid behaves as a liquid, this is nevertheless an example of heterogeneous catalysis – the catalyst is in a different phase from the reactants.</p>
</div>

<div class="note">
<p>Note: If you don't understand the term heterogeneous catalysis, follow this link to the <a href="introduction.html#top">introductory page on catalysis</a>.</p>
</div>

<div class="text-block">
<p>The catalyst is recovered afterwards, and the cracked mixture is separated by cooling and further fractional distillation.</p>
</div>
<p>There isn't any single unique reaction happening in the cracker. The hydrocarbon molecules are broken up in a fairly random way to produce mixtures of smaller hydrocarbons, some of which have carbon-carbon double bonds. One possible reaction involving the hydrocarbon C<sub>15</sub>H<sub>32</sub> might be:</p>

<div class="block-formula">
\text{C}_{15}\text{H}_{32} \xrightarrow{\text{zeolite catalyst}} 2\underset{\color{#467abf}{\text{ethene}}}{\text{C}_2\text{H}_4} + \underset{\color{#467abf}{\text{propene}}}{\text{C}_3\text{H}_6} + \underset{\color{#467abf}{\text{octane}}}{\text{C}_8\text{H}_{10}}
</div>

<div class="text-block">
<p>Or, showing more clearly what happens to the various atoms and bonds:</p>
</div>

<div class="image-container"><img src="cracking2.gif"></div>

<div class="text-block">
<p>This is only one way in which this particular molecule might break up. The ethene and propene are important materials for making plastics or producing other organic chemicals. The octane is one of the molecules found in petrol (gasoline).</p>
</div>

<h2>Isomerisation</h2>

<div class="text-block">
<p>Hydrocarbons used in petrol (gasoline) are given an octane rating which relates to how effectively they perform in the engine. A hydrocarbon with a high octane rating burns more smoothly than one with a low octane rating.</p>
<p>Molecules with "straight chains" have a tendency to pre-ignition. When the petrol / air mixture is compressed they tend to explode, and then explode a second time when the spark is passed through them. This double explosion produces knocking in the engine.</p>
</div>

<div class="note">
<p>Note: A straight chain hydrocarbon isn't literally straight! It just means that all the carbon atoms are joined up one after another in a row. If you made a model it would be extremely bendy!</p>
</div

<div class="text-block">
<p>Octane ratings are based on a scale on which heptane is given a rating of 0, and 2,2,4-trimethylpentane (an isomer of octane) a rating of 100.</p>
</div>

<div class="image-container"><img src="isomer1.gif"></div>

<div class="note">
<p>Note: If you don't understand what is meant by the term <a href="../../basicorg/isomerism/structural.html#top">structural isomer</a> you really ought to follow this link before you go on.</p>
<p>If you aren't happy about <a href="../../basicorg/conventions/names.html#top">naming organic compounds</a>, you might like to follow up this link at some time in the future. It isn't particularly important for the purposes of understanding the current page.</p>
</div>

<div class="text-block">
<p>In order to raise the octane rating of the molecules found in petrol (gasoline) and so make the petrol burn better in modern engines, the oil industry rearranges straight chain molecules into their isomers with branched chains.</p>
<p>One process uses a platinum catalyst on a zeolite base at a temperature of about 250°C and a pressure of 13 – 30 atmospheres. It is used particularly to change straight chains containing 5 or 6 carbon atoms into their branched isomers.</p>
<p>For example:</p>
</div>

<div class="image-container"><img src="isomer2.gif"></div>

<h2>Reforming</h2>

<div class="text-block">
<p>Reforming is another process used to improve the octane rating of hydrocarbons to be used in petrol, but is also a useful source of aromatic compounds for the chemical industry. Aromatic compounds are ones based on a benzene ring.</p>
</div>

<div class="note">
<p>Note: If you aren't familiar with the <a href="../../basicorg/bonding/benzene1.html#top">structure of benzene</a> you might like to have a quick look at this link before you go on. There's no need to get too bogged down in it though for the purposes of the current page.</p>
</div>

<div class="text-block">
<p>Reforming uses a platinum catalyst suspended on aluminium oxide together with various promoters to make the catalyst more efficient. The original molecules are passed as vapours over the solid catalyst at a temperature of about 500°C.</p>
<p>Isomerisation reactions occur (as above) but, in addition, chain molecules get converted into rings with the loss of hydrogen. Hexane, for example, gets converted into benzene, and heptane into methylbenzene.</p>
</div>

<div class="image-container"><img src="reform.gif"></div>

<div class="note">
<p>Note: The Kekulé structure has been used for benzene because it is easier to understand if you aren't familiar with benzene chemistry. You could perfectly well use the standard symbol of a circle inside a hexagon instead.</p>
<p>The original hexane and heptane molecules have been drawn bent into ring shapes so that you can easily see how they are changed during reforming.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<p>I am not providing questions for anything other than the first page from the catalysis menu. The other pages (including this one) are largely factual with little need for understanding. Pick out what you need to know and then learn it.</p>
<p>If you are meeting reactions here for the first time, I would be inclined to leave learning them until you meet them in context. For example, learn the reactions involving benzene when you do some benzene chemistry; learn the conditions for the Haber or Contact Processes when you meet them in the context of equilibria, and so on.</p>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../catalysismenu.html#top">To the catalysis menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>