<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Acid-Base Indicators | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes how indicators work, and their use in various acid-base titrations">
<meta name="keywords" content="pH, curve, titration, acid, base, weak, strong, indicator, phenolphthalein, methyl orange">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Acid-Base Indicators</h1>

<div class="text-block">
<p>This page describes how simple acid-base indicators work, and how to choose the right one for a particular titration.</p>
</div>

<div class="note">
<p>Warning: This page assumes that you know about <a href="phcurves.html#top">pH curves</a> for all the commonly quoted acid-base combinations, and <a href="acids.html#top">weak acids</a> (including pK<sub>a</sub>). If you aren't happy about either of these things, you must follow these links before you go any further.</p>
</div>

<h2>How Simple Indicators Work</h2>

<h3>Indicators as Weak Acids</h3>

<h4>Litmus</h4>

<div class="text-block">
<p>Litmus is a weak acid. It has a seriously complicated molecule which we will simplify to HLit. The "H" is the proton which can be given away to something else. The "Lit" is the rest of the weak acid molecule.</p>
<p>There will be an equilibrium established when this acid dissolves in water. Taking the simplified version of this equilibrium:</p>
</div>

<div class="block-formula">
\underset{\colorbox{#e26e6e}{}}{\text{HLit}_{(aq)}} \longrightarrow \text{H}^+_{(aq)} + \underset{\colorbox{#7ca3ce}{}}{\text{Lit}^-_{(aq)}}
</div>

<div class="note">
<p>Note: If you don't understand what I mean by "the simplified version of this equilibrium", you need to follow up the <a href="acids.html#top">weak acids</a> link before you go any further.</p>
</div>

<div class="text-block">
<p>The un-ionised litmus is red, whereas the ion is blue.</p>
<p>Now use Le Chatelier's Principle to work out what would happen if you added hydroxide ions or some more hydrogen ions to this equilibrium.</p>
</div>

<div class="note">
<p>Note: If you don't understand <a href="../equilibria/lechatelier.html#top">Le Chatelier's Principle</a>, follow this link before you go any further, and make sure that you understand about the effect of changes of concentration on the position of equilibrium.</p>
</div>

<h5>Adding hydroxide ions:</h5>

<div class="image-container"><img src="litbase.gif"></div>

<h5>Adding hydrogen ions:</h5>

<div class="image-container"><img src="litacid.gif"></div>

<h5>If the concentrations of HLit and Lit <sup>-</sup> are equal:</h5>

<div class="text-block">
<p>At some point during the movement of the position of equilibrium, the concentrations of the two colours will become equal. The colour you see will be a mixture of the two.</p>
</div>

<div class="image-container"><img src="litneutral.gif"></div>

<div class="text-block">
<p>The reason for the inverted commas around "neutral" is that there is no reason why the two concentrations should become equal at pH 7. For litmus, it so happens that the 50 / 50 colour does occur at close to pH 7 – that's why litmus is commonly used to test for acids and alkalis. As you will see below, that isn't true for other indicators.</p>
</div>

<h4>Methyl orange</h4>

<div class="text-block">
<p>Methyl orange is one of the indicators commonly used in titrations. In an alkaline solution, methyl orange is yellow and the structure is:</p>
</div>

<div class="image-container"><img src="mostructbase.gif"></div>

<div class="text-block">
<p>Now, you might think that when you add an acid, the hydrogen ion would be picked up by the negatively charged oxygen. That's the obvious place for it to go. Not so!</p>
<p>In fact, the hydrogen ion attaches to one of the nitrogens in the nitrogen-nitrogen double bond to give a structure which might be drawn like this:</p>
</div>

<div class="image-container"><img src="mostructacid.gif"></div>

<div class="note">
<p>Note: You may find other structures for this with different arrangements of the bonds (although always with the hydrogen attached to that same nitrogen). The truth is that there is delocalisation over the entire structure, and no simple picture will show it properly. Don't worry about this exact structure – it is just to show a real case where the colour of a compound is drastically changed by the presence or absence of a hydrogen ion.</p>
</div>

<div class="text-block">
<p>You have the same sort of equilibrium between the two forms of methyl orange as in the litmus case – but the colours are different.</p>
</div>

<div class="block-formula">
\underset{\colorbox{#e9603d}{}}{\text{HMeor}_{(aq)}} \xrightleftharpoons{} \text{H}^+_{(aq)} + \underset{\colorbox{#eeeb5a}{}}{\text{Meor}^-_{(aq)}}
</div>

<div class="text-block">
<p>You should be able to work out for yourself why the colour changes when you add an acid or an alkali. The explanation is identical to the litmus case – all that differs are the colours.</p>
</div>

<div class="note">
<p>Note: If you have problems with this, it is because you don't really understand <a href="../equilibria/lechatelier.html#top">Le Chatelier's Principle</a>. Sort it out!</p>
</div>

<div class="text-block">
<p>In the methyl orange case, the half-way stage where the mixture of red and yellow produces an orange colour happens at pH 3.7 – nowhere near neutral. This will be explored further down this page.</p>
</div>

<h4>Phenolphthalein</h4>

<div class="text-block">
<p>Phenolphthalein is another commonly used indicator for titrations, and is another weak acid.</p>
</div>

<div class="block-formula">
\text{HPhph}_{(aq)} \xrightleftharpoons{} \text{H}^+_{(aq)} + \underset{\colorbox{#f850f8}{}}{\text{Phph}^-_{(aq)}}
</div>

<div class="text-block">
<p>In this case, the weak acid is colourless and its ion is bright pink. Adding extra hydrogen ions shifts the position of equilibrium to the left, and turns the indicator colourless. Adding hydroxide ions removes the hydrogen ions from the equilibrium which tips to the right to replace them – turning the indicator pink.</p>
<p>The half-way stage happens at pH 9.3. Since a mixture of pink and colourless is simply a paler pink, this is difficult to detect with any accuracy!</p>
</div>

<div class="note">
<p>Note: If you are interested in understanding the reason for the colour changes in methyl orange and phenolphthalein, they are discussed on a page in the analysis section of the site about <a href="../../analysis/uvvisible/theory.html#top">UV-visible spectroscopy</a>. This is quite difficult stuff, and if you are coming at this from scratch you will have to explore at least one other page before you can make sense of what is on that page. There is a link to help you to do that. Don't start this lightly!</p>
</div>

<h3>The pH Range of Indicators</h3>

<h4>The importance of pK<sub>ind</sub></h4>

<div class="text-block">
<p>Think about a general indicator, HInd – where "Ind" is all the rest of the indicator apart from the hydrogen ion which is given away:</p>
</div>

<div class="block-formula">
\text{HInd}_{(aq)} \xrightleftharpoons{} \text{H}^+_{(aq)} + \text{Ind}^-_{(aq)}
</div>

<div class="text-block">
<p>Because this is just like any other weak acid, you can write an expression for K<sub>a</sub> for it. We will call it K<sub>ind</sub> to stress that we are talking about the indicator.</p>
</div>

<div class="block-formula">
K_{ind} = \frac{[\text{H}^+][\text{Ind}^-]}{[\text{HInd}]}
</div>

<div class="note">
<p>Note: If this doesn't mean anything to you, then you won't be able to understand any of what follows without first reading the page on <a href="acids.html#top">weak acids</a>.</p>
</div>

<div class="text-block">
<p>Think of what happens half-way through the colour change. At this point the concentrations of the acid and its ion are equal. In that case, they will cancel out of the K<sub>ind</sub> expression.</p>
</div>

<div class="block-formula" label="at the half-way point">
\begin{aligned}
K_{ind} &= \frac{[\text{H}^+]\cancel{[\text{Ind}^-]}}{\cancel{[\text{HInd}]}} \\
K_{ind} &= [\text{H}^+]
\end{aligned}
</div>

<div class="text-block">
<p>You can use this to work out what the pH is at this half-way point. If you re-arrange the last equation so that the hydrogen ion concentration is on the left-hand side, and then convert to pH and pK<sub>ind</sub>, you get:</p>
</div>

<div class="block-formula">
\begin{gathered}
[\text{H}^+] = K_{ind} \\
\\
pH = pK_{ind} \\
\end{gathered}
</div>

<div class="text-block">
<p>That means that the end point for the indicator depends entirely on what its pK<sub>ind</sub> value is. For the indicators we've looked at above, these are:</p>
</div>

<table class="data-table">
<tbody><tr><th>indicator</th><th>pK<sub>ind</sub></th></tr>
<tr><td>litmus</td><td>6.5</td></tr>
<tr><td>methyl orange</td><td>3.7</td></tr>
<tr><td>phenolphthalein</td><td>9.3</td></tr>
</tbody></table>

<h4>The pH range of indicators</h4>

<div class="text-block">
<p>Indicators don't change colour sharply at one particular pH (given by their pK<sub>ind</sub>). Instead, they change over a narrow range of pH.</p>
<p>Assume the equilibrium is firmly to one side, but now you add something to start to shift it. As the equilibrium shifts, you will start to get more and more of the second colour formed, and at some point the eye will start to detect it.</p>
<p>For example, suppose you had methyl orange in an alkaline solution so that the dominant colour was yellow. Now start to add acid so that the equilibrium begins to shift.</p>
<p>At some point there will be enough of the red form of the methyl orange present that the solution will begin to take on an orange tint. As you go on adding more acid, the red will eventually become so dominant that you can no longe see any yellow.</p>
<p>There is a gradual smooth change from one colour to the other, taking place over a range of pH. As a rough "rule of thumb", the visible change takes place about 1 pH unit either side of the pK<sub>ind</sub> value.</p>
<p>The exact values for the three indicators we've looked at are:</p>
</div>

<table class="data-table">
<tbody><tr><th>indicator</th><th>pK<sub>ind</sub></th><th>pH range</th></tr>
<tr><td>litmus</td><td>6.5</td><td>5 – 8</td></tr>
<tr><td>methyl orange</td><td>3.7</td><td>3.1 – 4.4</td></tr>
<tr><td>phenolphthalein</td><td>9.3</td><td>8.3 – 10.0</td></tr>
</tbody></table>

<div class="text-block">
<p>The litmus colour change happens over an unusually wide range, but it is useful for detecting acids and alkalis in the lab because it changes colour around pH 7. Methyl orange or phenolphthalein would be less useful.</p>
<p>This is more easily seen diagramatically.</p>
</div>

<div class="image-container"><img src="indranges.gif"></div>

<div class="text-block">
<p>For example, methyl orange would be yellow in any solution with a pH greater than 4.4. It couldn't distinguish between a weak acid with a pH of 5 or a strong alkali with a pH of 14.</p>
</div>

<h2>Choosing Indicators for Titrations</h2>

<div class="text-block">
<p>Remember that the equivalence point of a titration is where you have mixed the two substances in exactly equation proportions. You obviously need to choose an indicator which changes colour as close as possible to that equivalence point. That varies from titration to titration.</p>
</div>

<h3>Strong Acid vs. Strong Base</h3>

<div class="text-block">
<p>The next diagram shows the pH curve for adding a strong acid to a strong base. Superimposed on it are the pH ranges for methyl orange and phenolphthalein.</p>
</div>

<div class="image-container"><img src="sasbinds.gif"></div>

<div class="text-block">
<p>You can see that neither indicator changes colour at the equivalence point.</p>
<p>However, the graph is so steep at that point that there will be virtually no difference in the volume of acid added whichever indicator you choose. However, it would make sense to titrate to the best possible colour with each indicator.</p>
<p>If you use phenolphthalein, you would titrate until it just becomes colourless (at pH 8.3) because that is as close as you can get to the equivalence point.</p>
<p>On the other hand, using methyl orange, you would titrate until there is the very first trace of orange in the solution. If the solution becomes red, you are getting further from the equivalence point.</p>
</div>

<h3>Strong Acid vs. Weak Base</h3>

<div class="image-container"><img src="sawbinds.gif"></div>

<div class="text-block">
<p>This time it is obvious that phenolphthalein would be completely useless. However, methyl orange starts to change from yellow towards orange very close to the equivalence point.</p>
<p>You have to choose an indicator which changes colour on the steep bit of the curve.</p>
</div>

<h3>Weak Acid vs. Strong Base</h3>

<div class="image-container"><img src="wasbinds.gif"></div>

<div class="text-block">
<p>This time, the methyl orange is hopeless! However, the phenolphthalein changes colour exactly where you want it to.</p>
</div>

<h3>Weak Acid vs. Weak Base</h3>

<div class="text-block">
<p>The curve is for a case where the acid and base are both equally weak – for example, ethanoic acid and ammonia solution. In other cases, the equivalence point will be at some other pH.</p>
</div>

<div class="image-container"><img src="wawbinds.gif"></div>

<div class="text-block">
<p>You can see that neither indicator is any use. Phenolphthalein will have finished changing well before the equivalence point, and methyl orange falls off the graph altogether.</p>
<p>It may be possible to find an indicator which starts to change or finishes changing at the equivalence point, but because the pH of the equivalence point will be different from case to case, you can't generalise.</p>
<p>On the whole, you would never titrate a weak acid and a weak base in the presence of an indicator.</p>
</div>

<h3>Sodium carbonate Solution and Dilute Hydrochloric acid</h3>

<div class="text-block">
<p>This is an interesting special case. If you use phenolphthalein or methyl orange, both will give a valid titration result – but the value with phenolphthalein will be exactly half the methyl orange one.</p>
</div>

<div class="image-container"><img src="carbtitinds.gif"></div>

<div class="text-block">
<p>It so happens that the phenolphthalein has finished its colour change at exactly the pH of the equivalence point of the first half of the reaction in which sodium hydrogencarbonate is produced.</p>
</div>

<div class="block-formula">
\text{Na}_2\text{CO}_{3(aq)} + \text{HCl}_{(aq)} \longrightarrow \text{NaCl}_{(aq)} + \text{NaHCO}_{3(aq)}
</div>

<div class="text-block">
<p>The methyl orange changes colour at exactly the pH of the equivalence point of the second stage of the reaction.</p>
</div>

<div class="block-formula">
\text{NaHCO}_{3(aq)} + \text{HCl}_{(aq)} \longrightarrow \text{NaCl}_{(aq)} + \text{CO}_{2(g)} + \text{H}_2\text{O}_{(l)}
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-indicators.pdf" target="_blank">Questions on indicators</a>
<a href="../questions/a-indicators.pdf" target="_blank">Answers</a>

</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../acideqiamenu.html#top">To the acid-base equilibria menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>