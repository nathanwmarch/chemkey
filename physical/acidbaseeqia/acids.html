<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Strong and Weak Acids | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the meaning of the terms strong and weak as applied to acids, and introduces pH, Ka and pKa">
<meta name="keywords" content="acid, weak, strong, pH, Ka, pKa">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Strong and Weak Acids</h1>

<div class="text-block">
<p>This page explains the terms strong and weak as applied to acids. As a part of this it defines and explains what is meant by pH, K<sub>a</sub> and pK<sub>a</sub>.</p>
<p>It is important that you don't confuse the words strong and weak with the terms concentrated and dilute.</p>
<p>As you will see below, the strength of an acid is related to the proportion of it which has reacted with water to produce ions. The concentration tells you about how much of the original acid is dissolved in the solution.</p>
<p>It is perfectly possible to have a concentrated solution of a weak acid, or a dilute solution of a strong acid. Read on</p>
</div>

<h2>Strong Acids</h2>

<h3>Explaining the Term "Strong Acid"</h3>

<div class="text-block">
<p>We are going to use the Bronsted-Lowry definition of an acid.</p>
</div>

<div class="note">
<p>Note: If you don't know what the Bronsted-Lowry theory of acids is, you should read about <a href="theories.html#top">theories of acids and bases</a> on another page in this section. You don't need to spend time reading about Lewis acids and bases for the purposes of this present page.</p>
</div>

<div class="text-block">
<p>When an acid dissolves in water, a proton (hydrogen ion) is transferred to a water molecule to produce a hydroxonium ion and a negative ion depending on what acid you are starting from.</p>
</div>

<h5>In the general case</h5>

<div class="block-formula">
\text{HA} + \text{H}_2\text{O} \xrightleftharpoons{} \text{H}_3\text{O}^+ + \text{A}^-
</div>

<div class="text-block">
<p>These reactions are all reversible, but in some cases, the acid is so good at giving away hydrogen ions that we can think of the reaction as being one-way. The acid is virtually 100% ionised.</p>
<p>For example, when hydrogen chloride dissolves in water to make hydrochloric acid, so little of the reverse reaction happens that we can write:</p>
</div>

<div class="block-formula">
\text{H}_2\text{O}_{(l)} + \text{HCl}_{(g)} \longrightarrow \text{H}_3\text{O}^+_{(aq)} + \text{Cl}^-_{(aq)}
</div>

<div class="text-block">
<p>At any one time, virtually 100% of the hydrogen chloride will have reacted to produce hydroxonium ions and chloride ions. Hydrogen chloride is described as a strong acid.</p>
</div>

<div class="definition">
<p>A strong acid is one which is virtually 100% ionised in solution.</p>
</div>

<div class="text-block">
<p>Other common strong acids include sulfuric acid and nitric acid.</p>
<p>You may find the equation for the ionisation written in a simplified form:</p>
</div>

<div class="block-formula">
\text{HCl}_{(aq)} \longrightarrow \text{H}^+_{(aq)} + \text{Cl}^-_{(aq)}
</div>

<div class="text-block">
<p>This shows the hydrogen chloride dissolved in the water splitting to give hydrogen ions in solution and chloride ions in solution.</p>
<p>This version is often used in this work just to make things look easier. If you use it, remember that the water is actually involved, and that when you write H<sup>+</sup><sub>(aq)</sub> what you really mean is a hydroxonium ion, H<sub>3</sub>O<sup>+</sup>.</p>
</div>

<div class="note">
<p>Note: You should find out what your examiners prefer on this. You are unlikely to find this from your syllabus, but should look at recent exam papers and mark schemes. If you are doing a UK-based exam and haven't got copies of your <a href="../../syllabuses.html#top">syllabus and past papers</a>, you should have! Follow this link to find out how to get hold of them.</p>
</div>

<h3>Strong Acids and pH</h3>

<div class="text-block">
<p>pH is a measure of the concentration of hydrogen ions in a solution. Strong acids like hydrochloric acid at the sort of concentrations you normally use in the lab have a pH around 0 to 1. The lower the pH, the higher the concentration of hydrogen ions in the solution.</p>
</div>

<h4>Defining pH</h4>

<div class="block-formula">
pH = -\log_{10}[\text{H}^+]
</div>

<ul>
<li>[H<sup>+</sup>] means "The concentration of H<sup>+</sup>"</li>
<li>Note the minus sign - as hydrogen ion concentration <i>increases</i>, the pH <i>decreases</i></li>
<li>log<sub>10</sub> is read as "log to the base 10"</li>
</ul>

<div class="note">
<p>Note: If you are asked to define pH in an exam, simply write down the expression in black. Never try to define it in words – it is a waste of time, and you are too likely to miss something out (like mentioning that the concentration has to be in mol dm<sup>-3</sup>). In the expression, above, the square brackets imply that, so you don't need to mention it.</p>
</div>

<h4>Working out the pH of a strong acid</h4>

<div class="text-block">
<p>Suppose you had to work out the pH of 0.1 mol dm<sup>-3</sup> hydrochloric acid. All you have to do is work out the concentration of the hydrogen ions in the solution, and then use your calculator to convert it to a pH.</p>
<p>With strong acids this is easy.</p>
<p>Hydrochloric acid is a strong acid – virtually 100% ionised. Each mole of HCl reacts with the water to give 1 mole of hydrogen ions and 1 mole of chloride ions</p>
<p>That means that if the concentration of the acid is 0.1 mol dm<sup>-3</sup>, then the concentration of hydrogen ions is also 0.1 mol dm<sup>-3</sup>.</p>
<p>Use your calculator to convert this into pH. My calculator wants me to enter 0.1, and then press the "log" button. Yours might want you to do it in a different order. You need to find out!</p>
</div>

<div class="block-formula">
\begin{aligned}
\log_{10}[0.1] &= {-}1 \\
\\
\text{but...}~pH &= -\log_{10}[0.1] \\
{-}({-}1) &= 1 \\
\therefore pH &= 1
\end{aligned}
</div>

<div class="text-block">
<p>The pH of this acid is 1.</p>
</div>

<div class="note">
<p>Note: If you want more examples to look at and to try yourself (with fully worked solutions given), you may be interested in my <a href="../../book.html#top">chemistry calculations book</a>. This also includes the slightly more confusing problem of converting pH back into hydrogen ion concentration.</p>
</div>

<h2>Weak Acids</h2>

<h3>Explaining the Term "Weak Acid"</h3>

<div class="definition">
<p>A weak acid is one which doesn't ionise fully when it is dissolved in water.</p>
</div>

<div class="text-block">
<p>Ethanoic acid is a typical weak acid. It reacts with water to produce hydroxonium ions and ethanoate ions, but the back reaction is more successful than the forward one. The ions react very easily to reform the acid and the water.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{COOH} + \text{H}_2\text{O} \xrightleftharpoons{} \text{CH}_3\text{COO}^- + \text{H}_3\text{O}^+
</div>

<div class="text-block">
<p>At any one time, only about 1% of the ethanoic acid molecules have converted into ions. The rest remain as simple ethanoic acid molecules.</p>
<p>Most organic acids are weak. Hydrogen fluoride (dissolving in water to produce hydrofluoric acid) is a weak inorganic acid that you may come across elsewhere.</p>
</div>

<div class="note">
<p>Note: If you are interested in exploring <a href="../../basicorg/acidbase/acids.html#top">organic acids</a> further, you will find them explained elsewhere on the site. It might be a good idea to read the rest of this page first, though.</p>
<p>If you want to know <a href="../../inorganic/group7/acidityhx.html#top">why hydrogen fluoride is a weak acid</a>, you can find out by following this link. But beware! The explanation is very complicated and definitely not for the faint-hearted!</p>
<p>These pages are in completely different parts of this site.</p>
</div>

<h3>Comparing the Strengths of Weak Acids</h3>

<div class="text-block">
<p>The position of equilibrium of the reaction between the acid and water varies from one weak acid to another. The further to the left it lies, the weaker the acid is.</p>
</div>

<div class="block-formula">
\text{HA} + \text{H}_2\text{O} \xrightleftharpoons{} \text{H}_3\text{O}^+ + \text{A}^-
</div>

<div class="note">
<p>Note: If you don't understand about <a href="../equilibria/introduction.html#top">position of equilibrium</a> follow this link before you go any further.</p>
<p>You are also going to need to know about <a href="../equilibria/kc.html#top">equilibrium constants, K<sub>c</sub></a> for homogeneous equilibria. There is no point in reading any more of this page unless you do!</p>
</div>
 
<h4>The acid dissociation constant, K<sub>a</sub></h4>

<div class="text-block">
<p>You can get a measure of the position of an equilibrium by writing an equilibrium constant for the reaction. The lower the value for the constant, the more the equilibrium lies to the left.</p>
<p>The dissociation (ionisation) of an acid is an example of a homogeneous reaction. Everything is present in the same phase – in this case, in solution in water. You can therefore write a simple expression for the equilibrium constant, K<sub>c</sub>.</p>
<p>Here is the equilibrium again:</p>
</div>

<div class="block-formula">
\text{HA} + \text{H}_2\text{O} \xrightleftharpoons{} \text{H}_3\text{O}^+ + \text{A}^-
</div>

<div class="text-block">
<p>You might expect the equilibrium constant to be written as:</p>
</div>

<div class="block-formula">
K_c = \frac{[\text{H}_3\text{O}^+][\text{A}^-]}{[\text{HA}][\text{H}_2\text{O}]}
</div>

<div class="text-block">
<p>However, if you think about this carefully, there is something odd about it.</p>
<p>At the bottom of the expression, you have a term for the concentration of the water in the solution. That's not a problem – except that the number is going to be very large compared with all the other numbers.</p>
<p>In 1 dm<sup>3</sup> of solution, there are going to be about 55 moles of water.</p>
</div>

<div class="note">
<p>Note: 1 mole of water weighs 18 g. 1 dm<sup>3</sup> of solution contains approximately 1000 g of water. Divide 1000 by 18 to get approximately 55.</p>
</div>

<div class="text-block">
<p>If you had a weak acid with a concentration of about 1 mol dm<sup>-3</sup>, and only about 1% of it reacted with the water, the number of moles of water is only going to fall by about 0.01. In other words, if the acid is weak the concentration of the water is virtually constant.</p>
<p>In that case, there isn't a lot of point in including it in the expression as if it were a variable. Instead, a new equilibrium constant is defined which leaves it out. This new equilibrium constant is called K<sub>a</sub>.</p>
</div>

<div class="block-formula">
K_a = \frac{[\text{H}_3\text{O}^+][\text{A}^-]}{[\text{HA}]}
</div>

<div class="note">
<p>Note: The term for the concentration of water hasn't just been ignored. What has happened is that the first expression has been rearranged to give K<sub>c</sub> (a constant) times the concentration of water (another constant) on the left-hand side. The product of those is then given the name K<sub>a</sub>.</p>
<p>You don't need to worry about this unless you really insist! All you need to do is to learn the format of the expression for K<sub>a</sub>.</p>
</div>

<div class="text-block">
<p>You may find the K<sub>a</sub> expression written differently if you work from the simplified version of the equilibrium reaction:</p>
</div>

<div class="block-formula">
\text{HA}_{(aq)} \xrightleftharpoons{} \text{H}^+_{(aq)} + \text{A}^-_{(aq)}
</div>

<div class="block-formula">
K_a = \frac{[\text{H}^+][\text{A}^-]}{[\text{HA}]}
</div>

<div class="text-block">
<p>This may be written with or without state symbols.</p>
<p>It is actually exactly the same as the previous expression for K<sub>a</sub>! Remember that although we often write H<sup>+</sup> for hydrogen ions in solution, what we are actually talking about are hydroxonium ions.</p>
<p>This second version of the K<sub>a</sub> expression isn't as precise as the first one, but your examiners may well accept it. Find out!</p>
<p>To take a specific common example, the equilibrium for the dissociation of ethanoic acid is properly written as:</p>
</div>

<div class="block-formula">
\text{CH}_3\text{COOH} + \text{H}_2\text{O} \xrightleftharpoons{} \text{CH}_3\text{COO}^- + \text{H}_3\text{O}^+
</div>

<div class="text-block">
<p>The K<sub>a</sub> expression is:</p>
</div>

<div class="block-formula">
K_a = \frac{[\text{CH}_3\text{COO}^-][\text{H}_3\text{O}^+]}{[\text{CH}_3\text{COOH}]}
</div>

<div class="text-block">
<p>If you are using the simpler version of the equilibrium</p>
</div>


<div class="block-formula">
\text{CH}_3\text{COOH} \xrightleftharpoons{} \text{CH}_3\text{COO}^- + \text{H}^+
</div>

<div class="text-block">
<p> the K<sub>a</sub> expression is:</p>
</div>

<div class="block-formula">
K_a = \frac{[\text{CH}_3\text{COO}^-][\text{H}^+]}{[\text{CH}_3\text{COOH}]}
</div>

<div class="note">
<p>Note: Because you are likely to come across both of these versions depending on where you read about K<sub>a</sub>, you would be wise to get used to using either. For exam purposes, though, use whichever your examiners seem to prefer.</p>
</div>

<div class="text-block">
<p>The table shows some values of K<sub>a</sub> for some simple acids:</p>
</div>

<table class="data-table two-right">
<thead>
<tr><th>acid</th><th>K<sub>a</sub><br>/mol dm<sup>-3</sup></th></tr>
</thead>
<tbody>
<tr><td>hydrofluoric acid</td><td>5.6 ✖️ 10<sup>-4</sup></td></tr>
<tr><td>methanoic acid</td><td>1.6 ️️✖️ 10<sup>-4</sup></td></tr>
<tr><td>ethanoic acid</td><td>1.7 ✖️ 10<sup>-5</sup></td></tr>
<tr><td>hydrogen sulfide</td><td>8.9 ✖️ 10<sup>-8</sup></td></tr>
</tbody></table>

<div class="text-block">
<p>These are all weak acids because the values for K<sub>a</sub> are very small. They are listed in order of decreasing acid strength – the K<sub>a</sub> values get smaller as you go down the table.</p>
<p>However, if you aren't very happy with numbers, that isn't immediately obvious. Because the numbers are in two parts, there is too much to think about quickly!</p>
<p>To avoid this, the numbers are often converted into a new, easier form, called pK<sub>a</sub>.</p>
</div>

<h4>An introduction to pKa</h4>

<div class="text-block">
<p>pK<sub>a</sub> bears exactly the same relationship to K<sub>a</sub> as pH does to the hydrogen ion concentration:</p>
</div>

<div class="block-formula">
pK_a = -\log_{10}K_a
</div>

<div class="text-block">
<p>If you use your calculator on all the K<sub>a</sub> values in the table above and convert them into pK<sub>a</sub> values, you get:</p>
</div>

<table class="data-table two-right three-right">
<thead>
<tr><th>acid</th><th>K<sub>a</sub><br>/ mol dm<sup>-3</sup></th><th>pK<sub>a</sub></th></tr>
</thead>
<tbody>
<tr><td>hydrofluoric acid</td><td>5.6 ✖️ 10<sup>-4</sup></td><td>3.3</td></tr>
<tr><td>methanoic acid</td><td>1.6 ✖️ 10<sup>-4</sup></td><td>3.8</td></tr>
<tr><td>ethanoic acid</td><td>1.7 ✖️ 10<sup>-5</sup></td><td>4.8</td></tr>
<tr><td>hydrogen sulfide</td><td>8.9 ✖️ 10<sup>-8</sup></td><td>7.1</td></tr>
</tbody></table>

<div class="note">
<p>Note: Notice that unlike K<sub>a</sub>, pK<sub>a</sub> doesn't have any units.</p>
</div>

<div class="text-block">
<p>Notice that the weaker the acid, the larger the value of pK<sub>a</sub>. It is now easy to see the trend towards weaker acids as you go down the table.</p>
</div>

<div class="principle">
<ul>
<li>The lower the value for pK<sub>a</sub>, the stronger the acid.</li>
 <li>The higher the value for pK<sub>a</sub>, the weaker the acid.</li>
</ul>
</div>

<div class="note">
<p>Note: If you need to know about K<sub>a</sub> and pK<sub>a</sub>, you are quite likely to need to be able to do calculations with them. You will probably need to be able to calculate the pH of a weak acid from its concentration and K<sub>a</sub> or pK<sub>a</sub>. You may need to reverse this and calculate a value for pK<sub>a</sub> from pH and concentration. I can't help you with these calculations on this site, but they are all covered in detail in my <a href="../../book.html#top">chemistry calculations book</a>.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-strongandweakacids.pdf" target="_blank">Questions on strong and weak acids</a>
<a href="../questions/a-strongandweakacids.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../acideqiamenu.html#top">To the acid-base equilibria menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>