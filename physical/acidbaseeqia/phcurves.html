<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>pH Curves (Titration Curves) | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes how pH changes during various acid-base titrations">
<meta name="keywords" content="pH, curve, titration, acid, base, weak, strong">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>pH Curves (Titration Curves)</h1>

<div class="text-block">
<p>This page describes how pH changes during various acid-base titrations.</p>
</div>

<h2>The Equivalence Point of a Titration</h2>

<h3>Sorting Out Some Confusing Terms</h3>

<div class="text-block">
<p>When you carry out a simple acid-base titration, you use an indicator to tell you when you have the acid and alkali mixed in exactly the right proportions to "neutralise" each other. When the indicator changes colour, this is often described as the end point of the titration.</p>
<p>In an ideal world, the colour change would happen when you mix the two solutions together in exactly equation proportions. That particular mixture is known as the equivalence point.</p>
<p>For example, if you were titrating sodium hydroxide solution with hydrochloric acid, both with a concentration of 1 mol dm<sup>-3</sup>, 25 cm<sup>3</sup> of sodium hydroxide solution would need exactly the same volume of the acid – because they react 1 : 1 according to the equation.</p>
</div>

<div class="block-formula">
\text{NaOH}_{(aq)} + \text{HCl}_{(aq)} \longrightarrow \text{NaCl}_{(aq)} + \text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>In this particular instance, this would also be the neutral point of the titration, because sodium chloride solution has a pH of 7.</p>
<p>But that isn't necessarily true of all the salts you might get formed.</p>
<p>For example, if you titrate ammonia solution with hydrochloric acid, you would get ammonium chloride formed. The ammonium ion is slightly acidic, and so pure ammonium chloride has a slightly acidic pH.</p>
<p>That means that at the equivalence point (where you had mixed the solutions in the correct proportions according to the equation), the solution wouldn't actually be neutral. To use the term "neutral point" in this context would be misleading.</p>
<p>Similarly, if you titrate sodium hydroxide solution with ethanoic acid, at the equivalence point the pure sodium ethanoate formed has a slightly alkaline pH because the ethanoate ion is slightly basic.</p>
</div>

<div class="summary">
<ul>
<li>The term "neutral point" is best avoided.</li>
<li>The term "equivalence point" means that the solutions have been mixed in exactly the right proportions according to the equation.</li>
<li>The term "end point" is where the indicator changes colour. As you will see on the page about indicators, that isn't necessarily exactly the same as the equivalence point.</li>
</ul>
</div>

<div class="note">
<p>Note: You can find out about <a href="indicators.html#top">indicators</a> by following this link (also available from the acid-base equilibria menu). You should read the present page first though.</p>
</div>

<h2>Simple pH curves</h2>

<div class="text-block">
<p>All the following titration curves are based on both acid and alkali having a concentration of 1 mol dm<sup>-3</sup>. In each case, you start with 25 cm<sup>3</sup> of one of the solutions in the flask, and the other one in a burette.</p>
<p>Although you normally run the acid from a burette into the alkali in a flask, you may need to know about the titration curve for adding it the other way around as well. Alternative versions of the curves have been described in most cases.</p>
</div>

<h3>Titration Curves for Strong Acid vs. Strong Base</h3>

<div class="text-block">
<p>We'll take hydrochloric acid and sodium hydroxide as typical of a strong acid and a strong base.</p>
</div>

<div class="image-container"><img src="naohhcleqn.gif"></div>

<h4>Running acid into the alkali</h4>

<div class="image-container"><img src="sasb1.gif"></div>

<div class="text-block">
<p>You can see that the pH only falls a very small amount until quite near the equivalence point. Then there is a really steep plunge. If you calculate the values, the pH falls all the way from 11.3 when you have added 24.9 cm<sup>3</sup> to 2.7 when you have added 25.1 cm<sup>3</sup>.</p>
</div>

<div class="note">
<p>Note: If you need to know how to calculate pH changes during a titration, you may be interested in my <a href="../../book.html#top">chemistry calculations book</a>.</p>
</div>

<h4>Running alkali into the acid</h4>

<div class="text-block">
<p>This is very similar to the previous curve except, of course, that the pH starts off low and increases as you add more sodium hydroxide solution.</p>
</div>

<div class="image-container"><img src="sasb2.gif"></div>

<div class="text-block">
<p>Again, the pH doesn't change very much until you get close to the equivalence point. Then it surges upwards very steeply.</p>
</div>

<h3>Titration Curves for Strong Acid vs. Weak Base</h3>

<div class="text-block">
<p>This time we are going to use hydrochloric acid as the strong acid and ammonia solution as the weak base.</p>
</div>

<div class="image-container"><img src="nh3hclaq.gif"></div>

<h4>Running acid into the alkali</h4>

<div class="image-container"><img src="sawb1.gif"></div>

<div class="text-block">
<p>Because you have got a weak base, the beginning of the curve is obviously going to be different. However, once you have got an excess of acid, the curve is essentially the same as before.</p>
<p>At the very beginning of the curve, the pH starts by falling quite quickly as the acid is added, but the curve very soon gets less steep. This is because a buffer solution is being set up – composed of the excess ammonia and the ammonium chloride being formed.</p>
</div>

<div class="note">
<p>Note: You can find out more about <a href="buffers.html#top">buffer solutions</a> by following this link. However, this is a very minor point in the present context, and you would probably do better to read the whole of the current page before you follow this up.</p>
</div>

<div class="text-block">
<p>Notice that the equivalence point is now somewhat acidic ( a bit less than pH 5), because pure ammonium chloride isn't neutral. However, the equivalence point still falls on the steepest bit of the curve. That will turn out to be important in choosing a suitable indicator for the titration.</p>
</div>

<h4>Running alkali into the acid</h4>

<div class="text-block">
<p>At the beginning of this titration, you have an excess of hydrochloric acid. The shape of the curve will be the same as when you had an excess of acid at the start of a titration running sodium hydroxide solution into the acid.</p>
<p>It is only after the equivalence point that things become different.</p>
<p>A buffer solution is formed containing excess ammonia and ammonium chloride. This resists any large increase in pH – not that you would expect a very large increase anyway, because ammonia is only a weak base.</p>
</div>

<div class="image-container"><img src="sawb2.gif"></div>

<h3>Titration Curves for Weak Acid vs. Strong Base</h3>

<div class="text-block">
<p>We'll take ethanoic acid and sodium hydroxide as typical of a weak acid and a strong base.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{COOH}_{(aq)} + \text{NaOH}_{(aq)} \longrightarrow \text{CH}_3\text{COONa}_{(aq)} + \text{H}_2\text{O}_{(l)}
</div>

<h4>Running acid into the alkali</h4>

<div class="text-block">
<p>For the first part of the graph, you have an excess of sodium hydroxide. The curve will be exactly the same as when you add hydrochloric acid to sodium hydroxide. Once the acid is in excess, there will be a difference.</p>
</div>

<div class="image-container"><img src="wasb1.gif"></div>

<div class="text-block">
<p>Past the equivalence point you have a buffer solution containing sodium ethanoate and ethanoic acid. This resists any large fall in pH.</p>
</div>

<h4>Running alkali into the acid</h4>

<div class="image-container"><img src="wasb2.gif"></div>

<div class="text-block">
<p>The start of the graph shows a relatively rapid rise in pH but this slows down as a buffer solution containing ethanoic acid and sodium ethanoate is produced. Beyond the equivalence point (when the sodium hydroxide is in excess) the curve is just the same as that end of the HCl – NaOH graph.</p>
</div>

<h3>Titration Curves for Weak Acid vs. Weak Base</h3>

<div class="text-block">
<p>The common example of this would be ethanoic acid and ammonia.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{COOH}_{(aq)} + \text{NH}_{3(aq)} \longrightarrow \text{CH}_3\text{COONH}_{4(aq)}
</div>

<div class="text-block">
<p>It so happens that these two are both about equally weak – in that case, the equivalence point is approximately pH 7.</p>
</div>

<h4>Running acid into the alkali</h4>

<div class="text-block">
<p>This is really just a combination of graphs you have already seen. Up to the equivalence point it is similar to the ammonia – HCl case. After the equivalence point it is like the end of the ethanoic acid – NaOH curve.</p>
</div>

<div class="image-container"><img src="wawb.gif"></div>

<div class="text-block">
<p>Notice that there isn't any steep bit on this graph. Instead, there is just what is known as a "point of inflexion". That lack of a steep bit means that it is difficult to do a titration of a weak acid against a weak base.</p>
</div>

<div class="note">
<p>Note: Because you almost never do titrations with this combination, there is no real point in giving the graph where they are added the other way round. It isn't difficult to work out what it might look like if you are interested – take the beginning of the sodium hydroxide added to ethanoic acid curve, and the end of the ammonia added to hydrochloric acid one.</p>
<p>The reason that it is difficult to do these titrations is discussed on the page about <a href="indicators.html#top">indicators</a>.</p>
</div>

<div class="summary">
<p>The way you normally carry out a titration involves adding the acid to the alkali. Here are reduced versions of the graphs described above so that you can see them all together.</p>
<div class="image-container"><img src="summary.gif"></div>
</div>

<h2>More Complicated Titration Curves</h2>

<h3>Adding Hydrochloric acid to Sodium carbonate Solution</h3>

<div class="text-block">
<p>The overall equation for the reaction between sodium carbonate solution and dilute hydrochloric acid is:</p>
</div>

<div class="block-formula">
\text{Na}_2\text{CO}_{3(aq)} + 2\text{HCl}_{(aq)} \longrightarrow 2\text{NaCl}_{(aq)} + \text{CO}_{2(g)} + \text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>If you had the two solutions of the same concentration, you would have to use twice the volume of hydrochloric acid to reach the equivalence point – because of the 1 : 2 ratio in the equation.</p>
<p>Suppose you start with 25 cm<sup>3</sup> of sodium carbonate solution, and that both solutions have the same concentration of 1 mol dm<sup>-3</sup>. That means that you would expect the steep drop in the titration curve to come after you had added 50 cm<sup>3</sup> of acid.</p>
<p>The actual graph looks like this:</p>
</div>

<div class="image-container"><img src="carbtitrate.gif"></div>

<div class="text-block">
<p>The graph is more complicated than you might think – and curious things happen during the titration.</p>
<p>You expect carbonates to produce carbon dioxide when you add acids to them, but in the early stages of this titration, no carbon dioxide is given off at all.</p>
<p>Then – as soon as you get past the half-way point in the titration – lots of carbon dioxide is suddenly released.</p>
<p>The graph is showing two end points – one at a pH of 8.3 (little more than a point of inflexion), and a second at about pH 3.7. The reaction is obviously happening in two distinct parts.</p>
<p>In the first part, complete at A in the diagram, the sodium carbonate is reacting with the acid to produce sodium hydrogencarbonate:</p>
</div>

<div class="block-formula">
\text{Na}_2\text{CO}_{3(aq)} + \text{HCl}_{(aq)} \longrightarrow \text{NaCl}_{(aq)} + \text{NaHCO}_{3(aq)}
</div>

<div class="text-block">
<p>You can see that the reaction doesn't produce any carbon dioxide.</p>
<p>In the second part, the sodium hydrogencarbonate produced goes on to react with more acid – giving off lots of CO<sub>2</sub>.</p>
</div>

<div class="block-formula">
\text{NaHCO}_{3(aq)} + \text{HCl}_{(aq)} \longrightarrow \text{NaCl}_{(aq)} + \text{CO}_{2(g)} + \text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>That reaction is finished at B on the graph.</p>
<p>It is possible to pick up both of these end points by careful choice of indicator. That is explained on the separate page on indicators.</p>
</div>

<h3>Adding Sodium hydroxide Solution to Dilute Ethanedioic acid</h3>

<div class="text-block">
<p>Ethanedioic acid was previously known as oxalic acid. It is a diprotic acid, which means that it can give away 2 protons (hydrogen ions) to a base. Something which can only give away one (like HCl) is known as a monoprotic acid.</p>
</div>

<div class="image-container"><img src="oxionise.gif"></div>

<div class="text-block">
<p>The reaction with sodium hydroxide takes place in two stages because one of the hydrogens is easier to remove than the other. The two successive reactions are:</p>
</div>

<div class="image-container"><img src="oxnaoh1.gif"></div>

<div class="image-container"><img src="oxnaoh2.gif"></div>

<div class="text-block">
<p>If you run sodium hydroxide solution into ethanedioic acid solution, the pH curve shows the end points for both of these reactions.</p>
</div>

<div class="image-container"><img src="oxtitration.gif"></div>

<div class="text-block">
<p>The curve is for the reaction between sodium hydroxide and ethanedioic acid solutions of equal concentrations.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-phcurves.pdf" target="_blank">Questions on pH curves</a>
<a href="../questions/a-phcurves.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../acideqiamenu.html#top">To the acid-base equilibria menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>