<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Buffer Solutions | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes simple acidic and alkaline buffer solutions and explains how they work">
<meta name="keywords" content="pH, acid, base, weak, buffer, solution">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Buffer Solutions</h1>

<div class="text-block">
<p>This page describes simple acidic and alkaline buffer solutions and explains how they work.</p>
</div>

<h2>What is a Buffer Solution?</h2>

<div class="definition">
<p>A buffer solution is one which resists changes in pH when small quantities of an acid or an alkali are added to it.</p>
</div>

<h3>Acidic Buffer Solutions</h3>

<div class="text-block">
<p>An acidic buffer solution is simply one which has a pH less than 7. Acidic buffer solutions are commonly made from a weak acid and one of its salts – often a sodium salt.</p>
<p>A common example would be a mixture of ethanoic acid and sodium ethanoate in solution. In this case, if the solution contained equal molar concentrations of both the acid and the salt, it would have a pH of 4.76. It wouldn't matter what the concentrations were, as long as they were the same.</p>
<p>You can change the pH of the buffer solution by changing the ratio of acid to salt, or by choosing a different acid and one of its salts.</p>
</div>

<div class="note">
<p>Note: If you have a very weak acid and one of its salts, this can produce a buffer solution which is actually alkaline! I will comment briefly about this further down the page, but if you are doing buffer solutions at an introductory level this isn't likely to bother you.</p>
<p>If you need to know about calculations involving buffer solutions, you may be interested in my <a href="../../book.html#top">chemistry calculations book</a>.</p>
</div>

<h3>Alkaline Buffer Solutions</h3>

<div class="text-block">
<p>An alkaline buffer solution has a pH greater than 7. Alkaline buffer solutions are commonly made from a weak base and one of its salts.</p>
<p>A frequently used example is a mixture of ammonia solution and ammonium chloride solution. If these were mixed in equal molar proportions, the solution would have a pH of 9.25. Again, it doesn't matter what concentrations you choose as long as they are the same.</p>
</div>

<h2>How Do Buffer Solutions Work?</h2>

<div class="text-block">
<p>A buffer solution has to contain things which will remove any hydrogen ions or hydroxide ions that you might add to it – otherwise the pH will change. Acidic and alkaline buffer solutions achieve this in different ways.</p>
</div>

<h3>Acidic Buffer Solutions</h3>

<div class="text-block">
<p>We'll take a mixture of ethanoic acid and sodium ethanoate as typical.</p>
<p>Ethanoic acid is a weak acid, and the position of this equilibrium will be well to the left:</p>
</div>

<div class="block-formula">
\text{CH}_3\text{COOH}_{(aq)} \xrightleftharpoons{} \text{CH}_3\text{COO}^-_{(aq)} + \text{H}^+_{(aq)}
</div>

<div class="text-block">
<p>Adding sodium ethanoate to this adds lots of extra ethanoate ions. According to Le Chatelier's Principle, that will tip the position of the equilibrium even further to the left.</p>
</div>

<div class="note">
<p>Note: If you don't understand <a href="../equilibria/lechatelier.html#top">Le Chatelier's Principle</a>, follow this link before you go any further, and make sure that you understand about the effect of changes of concentration on the position of equilibrium.</p>
</div>

<div class="text-block">
<p>The solution will therefore contain these important things:</p>
</div>

<ul>
<li>lots of un-ionised ethanoic acid;</li>
<li>lots of ethanoate ions from the sodium ethanoate;</li>
<li>enough hydrogen ions to make the solution acidic.</li>
</ul>

<div class="text-block">
<p>Other things (like water and sodium ions) which are present aren't important to the argument.</p>
</div>

<h4>Adding an acid to this buffer solution</h4>

<div class="text-block">
<p>The buffer solution must remove most of the new hydrogen ions otherwise the pH would drop markedly.</p>
<p>Hydrogen ions combine with the ethanoate ions to make ethanoic acid. Although the reaction is reversible, since the ethanoic acid is a weak acid, most of the new hydrogen ions are removed in this way.</p>
</div>

<div class="image-container"><img src="acidbuffh.gif"></div>

<div class="text-block">
<p>Since most of the new hydrogen ions are removed, the pH won't change very much – but because of the equilibria involved, it will fall a little bit.</p>
</div>

<h4>Adding an alkali to this buffer solution</h4>

<div class="text-block">
<p>Alkaline solutions contain hydroxide ions and the buffer solution removes most of these.</p>
<p>This time the situation is a bit more complicated because there are two processes which can remove hydroxide ions.</p>
</div>

<h5>Removal by reacting with ethanoic acid</h5>

<div class="text-block">
<p>The most likely acidic substance which a hydroxide ion is going to collide with is an ethanoic acid molecule. They will react to form ethanoate ions and water.</p>
</div>

<div class="image-container"><img src="acidbuffoh.gif"></div>

<div class="note">
<p>Note: You might be surprised to find this written as a slightly reversible reaction. Because ethanoic acid is a weak acid, its conjugate base (the ethanoate ion) is fairly good at picking up hydrogen ions again to re-form the acid. It can get these from the water molecules. You may well find this reaction written as one-way, but to be fussy about it, it is actually reversible!</p>
</div>

<div class="text-block">
<p>Because most of the new hydroxide ions are removed, the pH doesn't increase very much.</p>
</div>

<h5>Removal of the hydroxide ions by reacting with hydrogen ions</h5>

<div class="text-block">
<p>Remember that there are some hydrogen ions present from the ionisation of the ethanoic acid.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{COOH}_{(aq)} \xrightleftharpoons{} \text{CH}_3\text{COO}^-_{(aq)} + \text{H}^+_{(aq)}
</div>

<div class="text-block">
<p>Hydroxide ions can combine with these to make water. As soon as this happens, the equilibrium tips to replace them. This keeps on happening until most of the hydroxide ions are removed.</p>
</div>

<div class="image-container"><img src="acidbuffoh2.gif"></div>

<div class="text-block">
<p>Again, because you have equilibria involved, not all of the hydroxide ions are removed – just most of them. The water formed re-ionises to a very small extent to give a few hydrogen ions and hydroxide ions.</p>
</div>

<h3>Alkaline Buffer Solutions</h3>

<div class="text-block">
<p>We'll take a mixture of ammonia and ammonium chloride solutions as typical.</p>
<p>Ammonia is a weak base, and the position of this equilibrium will be well to the left:</p>
</div>

<div class="block-formula">
\text{NH}_{3(aq)} + \text{H}_2\text{O}_{(l)} \xrightleftharpoons{} + {\text{NH}_4}^+_{(aq)} + {}^-\text{OH}_{(aq)}
</div>

<div class="text-block">
<p>Adding ammonium chloride to this adds lots of extra ammonium ions. According to Le Chatelier's Principle, that will tip the position of the equilibrium even further to the left.</p>
<p>The solution will therefore contain these important things:</p>
</div>

<ul>
<li>lots of unreacted ammonia;</li>
<li>lots of ammonium ions from the ammonium chloride;</li>
<li>enough hydroxide ions to make the solution alkaline.</li>
</ul>

<div class="text-block">
<p>Other things (like water and chloride ions) which are present aren't important to the argument.</p>
</div>

<h4>Adding an acid to this buffer solution</h4>

<div class="text-block">
<p>There are two processes which can remove the hydrogen ions that you are adding.</p>
</div>

<h5>Removal by reacting with ammonia</h5>

<div class="text-block">
<p>The most likely basic substance which a hydrogen ion is going to collide with is an ammonia molecule. They will react to form ammonium ions.</p>
</div>

<div class="image-container"><img src="alkbuffh1.gif"></div>

<div class="text-block">
<p>Most, but not all, of the hydrogen ions will be removed. The ammonium ion is weakly acidic, and so some of the hydrogen ions will be released again.</p>
<p>Removal of the hydrogen ions by reacting with hydroxide ions</p>
<p>Remember that there are some hydroxide ions present from the reaction between the ammonia and the water.</p>
</div>

<div class="block-formula">
\text{NH}_{3(aq)} + \text{H}_2\text{O}_{(l)} \xrightleftharpoons{} {\text{NH}_4}^+_{(aq)} + {}^-\text{OH}_{(aq)}
</div>

<div class="text-block">
<p>Hydrogen ions can combine with these hydroxide ions to make water. As soon as this happens, the equilibrium tips to replace the hydroxide ions. This keeps on happening until most of the hydrogen ions are removed.</p>
</div>

<div class="image-container"><img src="alkbuffh2.gif"></div>

<div class="text-block">
<p>Again, because you have equilibria involved, not all of the hydrogen ions are removed – just most of them.</p>
</div>

<h4>Adding an alkali to this buffer solution</h4>

<div class="text-block">
<p>The hydroxide ions from the alkali are removed by a simple reaction with ammonium ions.</p>
</div>

<div class="image-container"><img src="alkbuffoh.gif"></div>

<div class="text-block">
<p>Because the ammonia formed is a weak base, it can react with the water – and so the reaction is slightly reversible. That means that, again, most (but not all) of the the hydroxide ions are removed from the solution.</p>
</div>

<h2>Calculations involving buffer solutions</h2>

<div class="text-block">
<p>This is only a brief introduction. There are more examples, including several variations, over 10 pages in my <a href="../../book.html">chemistry calculations book</a>.</p>
</div>

<h3>Acidic Buffer Solutions</h3>

<div class="text-block">
<p>This is easier to see with a specific example. Remember that an acid buffer can be made from a weak acid and one of its salts.</p>
<p>Let's suppose that you had a buffer solution containing 0.10 mol dm<sup>-3</sup> of ethanoic acid and 0.20 mol dm<sup>-3</sup> of sodium ethanoate. How do you calculate its pH?</p>
<p>In any solution containing a weak acid, there is an equilibrium between the un-ionised acid and its ions. So for ethanoic acid, you have the equilibrium:</p>
</div>

<div class="block-formula">
\text{CH}_3\text{COOH}_{(aq)} \xrightleftharpoons{} \text{CH}_3\text{COO}^-_{(aq)} + \text{H}^+_{(aq)}
</div>

<div class="text-block">
<p>The presence of the ethanoate ions from the sodium ethanoate will have moved the equilibrium to the left, but the equilibrium still exists.</p>
<p>That means that you can write the equilibrium constant, K<sub>a</sub>, for it:</p>
</div>

<div class="block-formula">
K_a = \frac{[\text{CH}_3\text{COO}^-][\text{H}^+]}{[\text{CH}_3\text{COOH}]}
</div>

<div class="text-block">
<p>Where you have done calculations using this equation previously with a weak acid, you will have assumed that the concentrations of the hydrogen ions and ethanoate ions were the same. Every molecule of ethanoic acid that splits up gives one of each sort of ion.</p>
<p>That's no longer true for a buffer solution:</p>
</div>

<div class="principle">
<p>In a buffer solution, the concentration of A<sup>-</sup> and H<sup>+</sup> are rarely, if ever, equal</p>
</div>

<div class="text-block">
<p>If the equilibrium has been pushed even further to the left, the number of ethanoate ions coming from the ethanoic acid will be completely negligible compared to those from the sodium ethanoate.</p>
<p>We therefore assume that the ethanoate ion concentration is the same as the concentration of the sodium ethanoate – in this case, 0.20 mol dm<sup>-3</sup>.</p>
<p>In a weak acid calculation, we normally assume that so little of the acid has ionised that the concentration of the acid at equilibrium is the same as the concentration of the acid we used. That is even more true now that the equilibrium has been moved even further to the left.</p>
<p>So the assumptions we make for a buffer solution are:</p>
</div>

<div class="block-formula">
K_a = \frac{\overbrace{[\text{CH}_3\text{COO}^-]}^{\clap{\text{\color{#467abf}{assume this is the same as the concentration of the sodium ethanoate}}}{}}[\text{H}^+]}{\underbrace{[\text{CH}_3\text{COOH}]}_{
\clap{
\text{
\color{#467abf}{assume this is the same as the concentration of the original acid}
}
}{}
}}
</div>

<div class="text-block">
<p>Now, if we know the value for K<sub>a</sub>, we can calculate the hydrogen ion concentration and therefore the pH.</p>
<p>K<sub>a</sub> for ethanoic acid is 1.74 x 10<sup>-5</sup> mol dm<sup>-3</sup>.</p>
<p>Remember that we want to calculate the pH of a buffer solution containing 0.10 mol dm<sup>-3</sup> of ethanoic acid and 0.20 mol dm<sup>-3</sup> of sodium ethanoate.</p>
</div>

<div class="block-formula">
\begin{aligned}
K_a &= \frac{[\text{CH}_3\text{COO}^-][\text{H}^+]}{[\text{CH}_3\text{COOH}]} \\
1.74 \times 10^{-5} &= \frac{0.20 \times [\text{H}^+]}{0.10} \\
[\text{H}^+] &= 1.74 \times 10^{-5} \times \frac{0.20}{0.10} \\
{} &= 8.7 \times 10^{-6} \text{ mol} \text{ dm}^{-3}
\end{aligned}
</div>

<div class="text-block">
<p>Then all you have to do is to find the pH using the expression pH = -log<sub>10</sub> [H<sup>+</sup>]</p>
<p>You will still have the value for the hydrogen ion concentration on your calculator, so press the log button and ignore the negative sign (to allow for the minus sign in the pH expression).</p>
<p>You should get an answer of 5.1 to two significant figures. You can't be more accurate than this, because your concentrations were only given to two figures.</p>
</div>

<div class="note">
<p>Note: I commented further up the page that if you had a very weak acid and one of its salts, the buffer solution formed could well be alkaline. An example is a mixture of HCN and NaCN.</p>
<p>HCN is a very weak acid with a K<sub>a</sub> of 4.9 x 10<sup>-10</sup> mol dm<sup>-3</sup>. If you had a solution containing an equal numbers of moles of HCN and NaCN, you could calculate (exactly as above) that this buffer solution would have a pH of 9.3.</p>
<p>This isn't something that you need to worry about. Just don't assume that every combination of weak acid and one of its salts will necessarily produce a buffer solution with a pH less than 7.</p>
</div>

<div class="text-block">
<p>You could, of course, be asked to reverse this and calculate in what proportions you would have to mix ethanoic acid and sodium ethanoate to get a buffer solution of some desired pH. It is no more difficult than the calculation we have just looked at.</p>
<p>Suppose you wanted a buffer with a pH of 4.46. If you un-log this to find the hydrogen ion concentration you need, you will find it is 3.47 x 10<sup>-5</sup> mol dm<sup>-3</sup>.</p>
<p>Feed that into the K<sub>a</sub> expression.</p>
</div>

<div class="block-formula">
\begin{aligned}
K_a &= \frac{[\text{CH}_3\text{COO}^-][\text{H}^+]}{[\text{CH}_3\text{COOH}]} \\
1.74 \times 10^{-5} &= \frac{[\text{CH}_3\text{COO}^-] \times 3.47 \times 10^{-5}}{[\text{CH}_3\text{COOH}]} \\
\frac{[\text{CH}_3\text{COO}^-]}{[\text{CH}_3\text{COOH}]} &= \frac{1.74 \times 10^{-5}}{3.47 \times 10^{-5}} \\
{} &= 0.50
\end{aligned}
</div>

<div class="text-block">
<p>All this means is that to get a solution of pH 4.46, the concentration of the ethanoate ions (from the sodium ethanoate) in the solution has to be 0.5 times that of the concentration of the acid. All that matters is that ratio.</p>
<p>In other words, the concentration of the ethanoate has to be half that of the ethanoic acid.</p>
<p>One way of getting this, for example, would be to mix together 10 cm<sup>3</sup> of 1.0 mol dm<sup>-3</sup> sodium ethanoate solution with 20 cm<sup>3</sup> of 1.0 mol dm<sup>-3</sup> ethanoic acid. Or 10 cm<sup>3</sup> of 1.0 mol dm<sup>-3</sup> sodium ethanoate solution with 10 cm<sup>3</sup> of 2.0 mol dm<sup>-3</sup> ethanoic acid. And there are all sorts of other possibilities.</p>
</div>

<div class="note">
<p>Note: If your maths isn't very good, these examples can look a bit scary, but in fact they aren't. Go through the calculations line by line, and make sure that you can see exactly what is happening in each line – where the numbers are coming from, and why they are where they are. Then go away and practise similar questions.</p>
<p>If you are good at maths and can't see why anyone should think this is difficult, then feel very fortunate. Most people aren't so lucky!</p>
</div>

<h3>Alkaline Buffer Solutions</h3>

<div class="text-block">
<p>We are talking here about a mixture of a weak base and one of its salts – for example, a solution containing ammonia and ammonium chloride.</p>
<p>The modern, and easy, way of doing these calculations is to re-think them from the point of view of the ammonium ion rather than of the ammonia solution. Once you have taken this slightly different view-point, everything becomes much the same as before.</p>
<p>So how would you find the pH of a solution containing 0.100 mol dm<sup>-3</sup> of ammonia and 0.0500 mol dm<sup>-3</sup> of ammonium chloride?</p>
<p>The mixture will contain lots of unreacted ammonia molecules and lots of ammonium ions as the essential ingredients.</p>
<p>The ammonium ions are weakly acidic, and this equilibrium is set up whenever they are in solution in water:</p>
</div>

<div class="block-formula">
{\text{NH}_4}^+_{(aq)} \xrightleftharpoons{} + \text{NH}_{3(aq)} + \text{H}^+_{(aq)}
</div>

<div class="text-block">
<p>You can write a K<sub>a</sub> expression for the ammonium ion, and make the same sort of assumptions as we did in the previous case:</p>
</div>

<div class="block-formula">
K_a = \frac{
\overbrace{
[\text{NH}_3]
}^{
\clap{
\text{
\color{#467abf}{assume this is the same as provided concentration of ammonia}
}
}{}
}[\text{H}^+]
}
{\underbrace{
[{\text{NH}_4}^+]
}_{
\text{
\clap{
\color{#467abf}{assume this is the same as the concentration of the ammonium chloride}
}{}
}
}
}
</div>

<div class="text-block">
<p>The presence of the ammonia in the mixture forces the equilibrium far to the left. That means that you can assume that the ammonium ion concentration is what you started off with in the ammonium chloride, and that the ammonia concentration is all due to the added ammonia solution.</p>
<p>The value for K<sub>a</sub> for the ammonium ion is 5.62 x 10<sup>-10</sup> mol dm<sup>-3</sup>.</p>
<p>Remember that we want to calculate the pH of a buffer solution containing 0.100 mol dm<sup>-3</sup> of ammonia and 0.0500 mol dm<sup>-3</sup> of ammonium chloride.</p>
<p>Just put all these numbers in the K<sub>a</sub> expression, and do the sum:</p>
</div>

<div class="block-formula">
\begin{aligned}
K_a &= \frac{
[\text{NH}_3][\text{H}^+]
}{
[{\text{NH}_4}^+]
} \\
5.62 \times 10^{-10} &= \frac{
0.100 \times [\text{H}^+]
}{
0.0500
} \\
[\text{H}^+] &= \frac{
0.0500
}{
0.100
} \times 5.62 \times 10^{-10} \\
{} &= 2.81 \times 10^{-10}
\end{aligned}
</div>

<div class="block-formula">
\begin{aligned}
pH &= {-}\log_{10}[\text{H}^+] \\
{} &= 9.55
\end{aligned}
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-buffers.pdf" target="_blank">Questions on buffer solutions</a>
<a href="../questions/a-buffers.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../acideqiamenu.html#top">To the acid-base equilibria menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>