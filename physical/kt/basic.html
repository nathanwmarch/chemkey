<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to Kinetic Theory | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A simple description of solids, liquids and gases, and their changes of state.">
<meta name="keywords" content="solid, liquid, gas, vapour, vapor, pressure, melting, boiling, evaporation, sublimation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to Kinetic Theory</h1>

<div class="text-block">
<p>This page takes a simple look at solids, liquids and gases, and changes of state such as melting and boiling, in terms of the behaviour of the particles present.</p>
</div>

<h2>The Arrangements of Particles in Solids, Liquids and Gases</h2>

<div class="text-block">
<p>A simple view of the arrangement of the particles in solids, liquids and gases looks like this:</p>
</div>

<div class="image-container"><img src="slg.gif"></div>

<h3>Solids</h3>

<div class="text-block">
<p>In the solid, the particles are touching, and the only motion allowed to them is vibration. The particles may be arranged regularly (in which case, the solid is crystalline), or at random (giving waxy solids like candles or some forms of polythene, for example).</p>
<p>The particles are held in the solid by forces which depend on the actual substance – ionic bonds, covalent bonds, metallic bonds, hydrogen bonds or van der Waals attractions.</p>
</div>

<h3>Liquids</h3>

<div class="text-block">
<p>In a liquid, the particles are mainly touching, but some gaps have appeared in the structure. These gaps allow the particles to move, and so the particles are arranged randomly.</p>
<p>The forces that held the solid particles together are also present in the liquid (unless melting has broken up a substance consisting only of covalent bonds – a giant covalent structure). However, the particles in the liquid have enough energy to prevent the forces holding them in a fixed arrangement.</p>
<p>For most liquids, the density of the liquid is slightly less than that of the solid, but there isn't much difference. That means that the particles in the liquid are almost as close together as they are in a solid. If you draw diagrams of liquids, make sure that most of the particles are touching, but at random, with a few gaps.</p>
</div>

<div class="note">
<p>Note: Water is an exception to this. Ice floats on water, and so the liquid must be denser than the solid. You will find an explanation of this on a page about <a href="../../atoms/structures/molecular.html">molecular structures</a>.</p>
</div>

<h3>Gases</h3>

<div class="text-block">
<p>In a gas, the particles are entirely free to move. At ordinary pressures, the distance between individual particles is of the order of ten times the diameter of the particles. At that distance, any attractions between the particles are fairly negligible at ordinary temperatures and pressures.</p>
</div>

<h2>Changes of State</h2>

<h3>Melting and Freezing</h3>

<div class="text-block">
<p>If energy is supplied by heating a solid, the heat energy causes stronger vibrations until the particles eventually have enough energy to break away from the solid arrangement to form a liquid. The heat energy required to convert 1 mole of solid into a liquid at its melting point is called the enthalpy of fusion.</p>
<p>When a liquid freezes, the reverse happens. At some temperature, the motion of the particles is slow enough for the forces of attraction to be able to hold the particles as a solid. As the new bonds are formed, heat energy is evolved.</p>
</div>

<h3>Boiling and Condensing</h3>

<div class="text-block">
<p>If more heat energy is supplied, the particles eventually move fast enough to break all the attractions between them, and the liquid boils. The heat energy required to convert 1 mole of liquid into a gas at its boiling point is called the enthalpy of vaporisation.</p>
<p>If the gas is cooled, at some temperature the gas particles will slow down enough for the attractions to become effective enough to condense it back into a liquid. Again, as those forces are re-established, heat energy is released.</p>
</div>

<div class="note">
<p>Remember: Breaking bonds needs energy; making bonds releases it.</p>
</div>

<h3>The Evaporation of a Liquid</h3>

<div class="text-block">
<p>The average energy of the particles in a liquid is governed by the temperature. The higher the temperature, the higher the average energy. But within that average, some particles have energies higher than the average, and others have energies lower than the average.</p>
<p>Some of the more energetic particles on the surface of the liquid can be moving fast enough to escape from the attractive forces holding the liquid together. They evaporate.</p>
<p>The diagram shows a small region of a liquid near its surface.</p>
</div>

<div class="image-container"><img src="evaporation.gif"></div>

<div class="text-block">
<p>Notice that evaporation only takes place on the surface of the liquid. That's quite different from boiling which happens when there is enough energy to disrupt the attractive forces throughout the liquid. That's why, if you look at boiling water, you see bubbles of gas being formed all the way through the liquid.</p>
<p>If you look at water which is just evaporating in the sun, you don't see any bubbles. Water molecules are simply breaking away from the surface layer.</p>
<p>Eventually, the water will all evaporate in this way. The energy which is lost as the particles evaporate is replaced from the surroundings. As the molecules in the water jostle with each other, new molecules will gain enough energy to escape from the surface.</p>
</div>

<h4>The evaporation of a liquid in a closed container</h4>

<div class="text-block">
<p>Now imagine what happens if the liquid is in a closed container. Common sense tells you that water in a sealed bottle doesn't seem to evaporate – or at least, it doesn't disappear over time.</p>
<p>But there is constant evaporation from the surface. Particles continue to break away from the surface of the liquid – but this time they are trapped in the space above the liquid.</p>
</div>

<div class="image-container"><img src="sealed1.gif"></div>

<div class="text-block">
<p>As the gaseous particles bounce around, some of them will hit the surface of the liquid again, and be trapped there. There will rapidly be an equilibrium set up in which the number of particles leaving the surface is exactly balanced by the number rejoining it.</p>
</div>

<div class="image-container"><img src="sealed2.gif"></div>

<div class="text-block">
<p>In this equilibrium, there will be a fixed number of the gaseous particles in the space above the liquid.</p>
<p>When these particles hit the walls of the container, they exert a pressure. This pressure is called the saturated vapour pressure (also known as saturation vapour pressure) of the liquid.</p>
</div>

<h3>Sublimation</h3>

<div class="text-block">
<p>Solids can also lose particles from their surface to form a vapour, except that in this case we call the effect sublimation rather than evaporation. Sublimation is the direct change from solid to vapour (or vice versa) without going through the liquid stage.</p>
<p>In most cases, at ordinary temperatures, the saturated vapour pressures of solids range from low to very, very, very low. The forces of attraction in many solids are too high to allow much loss of particles from the surface.</p>
<p>However, there are some which do easily form vapours. For example, naphthalene (used in old-fashioned "moth balls" to deter clothes moths) has quite a strong smell. Molecules must be breaking away from the surface as a vapour, because otherwise you wouldn't be able to smell it.</p>
<p>Another fairly common example (discussed in detail elsewhere on the site) is solid carbon dioxide – "dry ice". This never forms a liquid at atmospheric pressure and always converts directly from solid to vapour. That's why it is known as dry ice.</p>
</div>

<div class="note">
<p>Note: This link will take you straight to the page about <a href="../../physical/phaseeqia/phasediags.html#top">phase diagrams</a> where this is discussed in detail. Be aware that this is a much more advanced topic than the page you are on at the moment, and may not be on many syllabuses.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-kt.pdf" target="_blank">Questions on simple kinetic theory</a>
<a href="../questions/a-kt.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../ktmenu.html#top">To the kinetic theory menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>