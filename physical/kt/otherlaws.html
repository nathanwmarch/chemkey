<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Other Gas Laws – Boyle's Law and Charles' Law | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A brief account of how Boyle's Law and Charles' Law relate to kinetic theory of gases">
<meta name="keywords" content="boyle, boyle's, boyles, bharles, law, kinetic theory, gas, gases">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Other Gas Laws – Boyle's Law and Charles' Law</h1>

<div class="text-block">
<p>This page takes a simple look at Boyle's Law and Charles' Law, and is suitable for 16 – 18 year old chemistry students doing a course the equivalent of UK A-level. The aim is simply to show how these laws relate to Kinetic Theory (in a non-mathematical way), and to the ideal gas equation.</p>
<p>Before you waste time on this, be sure that you actually need to know about it. Certainly in the UK exam system, it is pretty rare for chemistry students to be expected to know either of these laws these days. They have been almost completely replaced by the ideal gas equation.</p>
</div>

<h2>Boyle's Law</h2>

<div class="principle">
<p>For a fixed mass of gas at constant temperature, the volume is inversely proportional to the pressure.</p>
</div>

<div class="text-block">
<p>That means that, for example, if you double the pressure, you will halve the volume. If you increase the pressure 10 times, the volume will decrease 10 times.</p>
<p>You can express this mathematically as</p>
</div>

<div class="block-formula">
pV = \text{constant}
</div>

<h3>Is This Consistent With pV = nRT?</h3>

<ul>
<li>You have a fixed mass of gas, so n (the number of moles) is constant.</li>
<li>R is always constant – it is called the gas constant.</li>
<li>Boyle's Law demands that temperature is constant as well.</li>
</ul>

<div class="text-block">
<p>That means that everything on the right-hand side of pV = nRT is constant, and so pV is constant – which is what we have just said is a result of Boyle's Law.</p>
</div>

<h3>Simple Kinetic Theory Explanation</h3>

<div class="text-block">
<p>I'm not going to try to prove the relationship between pressure and volume mathematically – I'm just showing that it is reasonable.</p>
<p>This is easiest to see if you think about the effect of decreasing the volume of a fixed mass of gas at constant temperature.</p>
<p>Pressure is caused by gas molecules hitting the walls of the container.</p>
<p>With a smaller volume, the gas molecules will hit the walls more frequently, and so the pressure increases.</p>
<p>You might argue that this isn't actually what Boyle's Law says – it wants you to increase the pressure first and see what effect that has on the volume. But, in fact, it amounts to the same thing.</p>
<p>If you want to increase the pressure of a fixed mass of gas without changing the temperature, the only way you can do it is to squeeze it into a smaller volume. That causes the molecules to hit the walls more often, and so the pressure increases.</p>
</div>

<h2>Charles' Law</h2>

<div class="principle">
<p>For a fixed mass of gas at constant pressure, the volume is directly proportional to the kelvin temperature.</p>
</div>

<div class="text-block">
<p>That means, for example, that if you double the kelvin temperature from, say to 300 K to 600 K, at constant pressure, the volume of a fixed mass of the gas will double as well.</p>
<p>You can express this mathematically as</p>
</div>

<div class="block-formula">
V = \text{constant} \times T
</div>

<h3>Is This Consistent With pV = nRT?</h3>

<ul>
<li>You have a fixed mass of gas, so n (the number of moles) is constant.</li>
<li>R is the gas constant.</li>
<li>Charles' Law demands that pressure is constant as well.</li>
</ul>

<div class="text-block">
<p>If you rearrange the pV = nRT equation by dividing both sides by p, you will get</p>
</div>

<div class="block-formula">
V = \frac{nR}{p} \times T
</div>

<div class="text-block">
<p>But everything in the nR/p part of this is constant.</p>
<p>That means that V = constant x T, which is Charles' Law.</p>
</div>

<h3>Simple Kinetic Theory explanation</h3>

<div class="text-block">
<p>Again, I'm not trying to prove the relationship between pressure and volume mathematically – just that it is reasonable.</p>
<p>Suppose you have a fixed mass of gas in a container with a moveable barrier – something like a gas syringe, for example. The barrier can move without any sort of resistance.</p>
</div>

<div class="image-container"><img src="charleslaw.gif"></div>

<div class="text-block">
<p>The barrier will settle so that the pressure inside and outside is identical.</p>
<p>Now suppose you heat the gas, but not the air outside.</p>
<p>The gas molecules will now be moving faster, and so will hit the barrier more frequently, and harder. Meanwhile, the air molecules on the outside are hitting it exactly as before.</p>
<p>Obviously, the barrier will be forced to the right, and the volume of the gas will increase. That will go on until the pressure inside and outside is the same. In other words, the pressure of the gas will be back to the same as the air again.</p>
<p>So we have fulfilled what Charles' Law says. We have a fixed mass of gas (nothing has been added, and nothing has escaped). The pressure is the same before and after (in each case, the same as the external air pressure). And the volume increases when you increase the temperature of the gas.</p>
<p>What we haven't shown, of course, is that there is a "directly proportional" relationship. It can be done, but it needs some maths.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-otherlaws.pdf" target="_blank">Questions on other gas laws</a>
<a href="../questions/a-otherlaws.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../ktmenu.html#top">To the kinetic theory menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>