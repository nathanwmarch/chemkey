<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Ideal Gases and the Ideal Gas Law: pV = nRT | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An introduction to ideal gases and the ideal gas law: pV = nRT">
<meta name="keywords" content="ideal, gas, gases, law, assumptions">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Ideal Gases and the Ideal Gas Law: pV = nRT</h1>

<div class="text-block">
<p>This page looks at the assumptions which are made in the Kinetic Theory about ideal gases, and takes an introductory look at the Ideal Gas Law: pV = nRT. This is intended only as an introduction suitable for chemistry students at about UK A-level standard (for 16 – 18 year olds), and so there is no attempt to derive the ideal gas law using physics-style calculations.</p>
</div>

<h2>Kinetic Theory Assumptions About Ideal Gases</h2>

<div class="text-block">
<p>There is no such thing as an ideal gas, of course, but many gases behave approximately as if they were ideal at ordinary working temperatures and pressures. Real gases are dealt with in more detail on another page.</p>
<p>The assumptions are:</p>
</div>

<ul>
<li>Gases are made up of molecules which are in constant random motion in straight lines.</li>
<li>The molecules behave as rigid spheres.</li>
<li>Pressure is due to collisions between the molecules and the walls of the container.</li>
<li>All collisions, both between the molecules themselves, and between the molecules and the walls of the container, are perfectly elastic. (That means that there is no loss of kinetic energy during the collision.)</li>
<li>The temperature of the gas is proportional to the average kinetic energy of the molecules.</li>
</ul>

<div class="text-block">
<p>And then two absolutely key assumptions, because these are the two most important ways in which real gases differ from ideal gases:</p>
</div>

<ul>
<li>There are no (or entirely negligible) intermolecular forces between the gas molecules.</li>
<li>The volume occupied by the molecules themselves is entirely negligible relative to the volume of the container.</li>
</ul>

<h2>The Ideal Gas Equation</h2>

<div class="block-formula" label="the ideal gas equation">
pV = nRT
</div>

<div class="text-block">
<p>On the whole, this is an easy equation to remember and use. The problems lie almost entirely in the units. I am assuming below that you are working in strict SI units (as you will be if you are doing a UK-based exam, for example).</p>
</div>

<h3>Exploring the Terms</h3>

<h4>Pressure, p</h4>

<div class="text-block">
<p>Pressure is measured in pascals, Pa – sometimes expressed as newtons per square metre, N m<sup>-2</sup>. These mean exactly the same thing.</p>
<p>Be careful if you are given pressures in kPa (kilopascals). For example, 150 kPa is 150000 Pa. You must make that conversion before you use the ideal gas equation.</p>
<p>Should you want to convert from other pressure measurements:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{1 atmosphere} = 101,325 \text{ Pa} \\
\\
1 \text{ bar} = 100 \text{ kPa} = 100,000 \text{ Pa}
\end{gathered}
</div>

<h4>Volume, V</h4>

<div class="text-block">
<p>This is the most likely place for you to go wrong when you use this equation. That's because the SI unit of volume is the cubic metre, m<sup>3</sup> – not cm<sup>3</sup> or dm<sup>3</sup>.</p>
</div>

<div class="block-formula">
1 \text{ m}^3 = 1,000 \text{ dm}^3 = 1,000,000 \text{ cm}^3
</div>

<div class="text-block">
<p>So if you are inserting values of volume into the equation, you first have to convert them into cubic metres.</p>
<p>You would have to divide a volume in dm<sup>3</sup> by 1000, or in cm<sup>3</sup> by a million.</p>
<p>Similarly, if you are working out a volume using the equation, remember to covert the answer in cubic metres into dm<sup>3</sup> or cm<sup>3</sup> if you need to – this time by multiplying by a 1000 or a million.</p>
<p>If you get this wrong, you are going to end up with a silly answer, out by a factor of a thousand or a million. So it is usually fairly obvious if you have done something wrong, and you can check back again.</p>
</div>

<h4>Number of moles, n</h4>

<div class="text-block">
<p>This is easy, of course – it is just a number. You already know that you work it out by dividing the mass in grams by the mass of one mole in grams.</p>
<p>You will most often use the ideal gas equation by first making the substitution to give:</p>
</div>

<div class="block-formula">
pV = \frac{\text{mass (g)}}{\text{mass of 1 mole (g)}} \times RT
</div>

<div class="text-block">
<p>I don't recommend that you remember the ideal gas equation in this form, but you must be confident that you can convert it into this form.</p>
</div>

<h4>The gas constant, R</h4>

<div class="text-block">
<p>A value for R will be given you if you need it, or you can look it up in a data source. The SI value for R is 8.31441 J K<sup>-1</sup> mol<sup>-1</sup>.</p>
</div>

<div class="note">
<p>Note: You may come across other values for this with different units. A commonly used one in the past was 82.053 cm<sup>3</sup> atm K<sup>-1</sup> mol<sup>-1</sup>. The units tell you that the volume would be in cubic centimetres and the pressure in atmospheres. Unfortunately the units in the SI version aren't so obviously helpful.</p>
</div>

<h4>The temperature, T</h4>

<div class="text-block">
<p>The temperature has to be in kelvin. Don't forget to add 273 if you are given a temperature in degrees Celsius.</p>
</div>

<h3>Using the Ideal Gas Equation</h3>

<div class="text-block">
<p>Calculations using the ideal gas equation are included in my calculations book (see the link at the very bottom of the page), and I can't repeat them here. There are, however, a couple of calculations that I haven't done in the book which give a reasonable idea of how the ideal gas equation works.</p>
</div>

<h4>The molar volume at stp</h4>

<div class="text-block">
<p>If you have done simple calculations from equations, you have probably used the molar volume of a gas.</p>
<p>1 mole of any gas occupies 22.4 dm<sup>3</sup> at stp (standard temperature and pressure, taken as 0°C and 1 atmosphere pressure). You may also have used a value of 24.0 dm<sup>3</sup> at room temperature and pressure (taken as about 20°C and 1 atmosphere).</p>
<p>These figures are actually only true for an ideal gas, and we'll have a look at where they come from.</p>
<p>We can use the ideal gas equation to calculate the volume of 1 mole of an ideal gas at 0°C and 1 atmosphere pressure.</p>
</div>

<h5>First, we have to get the units right.</h5>

<div class="block-formula">
\begin{gathered}
0\degree\text{C is 273 K} \\
\\
T = 273 \text{ K}
\end{gathered}
</div>
<div class="block-formula">
\begin{gathered}
\text{1 atmosphere} = \text{101,325 Pa} \\
\\
p = \text{101,325 Pa}
\end{gathered}
</div>

<div class="text-block">
<p>We know that n = 1, because we are trying to calculate the volume of 1 mole of gas.</p>
</div>

<h5>And, finally:</h5>

<div class="block-formula">
R = 8.31441 \text{ J K}^{-1} \text{ mol}^{-1}
</div>

<div class="text-block">
<p>Slotting all of this into the ideal gas equation and then rearranging it gives:</p>
</div>

<div class="block-formula">
\begin{aligned}
101,325 \times V &= 1 \times 8.31441 \times 273 \\
V &= \frac{1 \times 8.31441 \times 273}{101,325} \text{ m}^3 \\
{} &= 0.0224 \text{ m}^3 \text{ (3 s.f.)}
\end{aligned}
</div>

<div class="text-block">
<p>And finally, because we are interested in the volume in cubic decimetres, you have to remember to multiply this by 1000 to convert from cubic metres into cubic decimetres.</p>
<p>The molar volume of an ideal gas is therefore 22.4 dm<sup>3</sup> at stp.</p>
<p>And, of course, you could redo this calculation to find the volume of 1 mole of an ideal gas at room temperature and pressure – or any other temperature and pressure.</p>
</div>

<h4>Finding the relative formula mass of a gas from its density</h4>

<div class="text-block">
<p>This is about as tricky as it gets using the ideal gas equation.</p>
<p>The density of ethane is 1.264 g dm<sup>-3</sup> at 20°C and 1 atmosphere. Calculate the relative formula mass of ethane.</p>
<p>The density value means that 1 dm<sup>3</sup> of ethane weighs 1.264 g.</p>
<p>Again, before we do anything else, get the awkward units sorted out.</p>
<p>A pressure of 1 atmosphere is 101325 Pa.</p>
<p>The volume of 1 dm<sup>3</sup> has to be converted to cubic metres, by dividing by 1000. We have a volume of 0.001 m<sup>3</sup>.</p>
<p>The temperature is 293 K.</p>
<p>Now put all the numbers into the form of the ideal gas equation which lets you work with masses, and rearrange it to work out the mass of 1 mole.</p>
</div>

<div class="block-formula">
\begin{aligned}
pV &= \frac{\text{mass (g)}}{\text{mass of 1 mole (g)}} \times RT \\
101,325 \times 0.001 &= \frac{1.264}{\text{mass of 1 mole (g)}} \times 8.31441 \times 293 \\
\text{mass of 1 mole (g)} &= \frac{1.264 \times 8.3441 \times 293}{101,325 \times 0.001} \\
{} &= 30.4 \text{ g}
\end{aligned}
</div>

<div class="text-block">
<p>The mass of 1 mole of anything is simply the relative formula mass in grams.</p>
<p>So the relative formula mass of ethane is 30.4, to 3 sig figs.</p>
<p>Now, if you add up the relative formula mass of ethane, C<sub>2</sub>H<sub>6</sub> using accurate values of relative atomic masses, you get an answer of 30.07 to 4 significant figures. Which is different from our answer – so what's wrong?</p>
</div>

<h5>There are two possibilities:</h5>

<ul>
<li>The density value I have used may not be correct. I did the sum again using a slightly different value quoted at a different temperature from another source. This time I got an answer of 30.3. So the density values may not be entirely accurate, but they are both giving much the same sort of answer.</li>
<li>Ethane isn't an ideal gas. Well, of course it isn't an ideal gas – there's no such thing! However, assuming that the density values are close to correct, the error is within 1% of what you would expect. So although ethane isn't exactly behaving like an ideal gas, it isn't far off.</li>
</ul>

<div class="text-block">
<p>If you need to know about real gases, now is a good time to read about them.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-idealgases.pdf" target="_blank">Questions on ideal gases</a>
<a href="../questions/a-idealgases.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="realgases.html#top">To explore real gases</a>
<a href="../ktmenu.html#top">To the kinetic theory menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>