<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Energy Profiles for Simple Reactions | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A closer look at energy profiles for reactions involving either a transition state or an intermediate">
<meta name="keywords" content="rate, reaction, kinetics, energy, profile, transition state, intermediate, activation, mechanism">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Energy Profiles for Simple Reactions</h1>

<div class="text-block">
<p>This page takes a closer look at simple energy profiles for reactions, and shows how they are slightly different for reactions involving an intermediate or just a transition state. Both of those terms are explained as well.</p>
</div>

<h2>Types of Energy Profile</h2>

<h3>What is an Energy Profile?</h3>

<div class="text-block">
<p>If you have done any work involving activation energy or catalysis, you will have come across diagrams like this:</p>
</div>

<div class="image-container"><img src="catprofile.gif"></div>

<div class="text-block">
<p>This diagram shows that, overall, the reaction is exothermic. The products have a lower energy than the reactants, and so energy is released when the reaction happens.</p>
<p>It also shows that the molecules have to possess enough energy (called activation energy) to get the reactants over what we think of as the "activation energy barrier".</p>
<p>In this example of a reaction profile, you can see that a catalyst offers a route for the reaction to follow which needs less activation energy. That, of course, causes the reaction to happen faster.</p>
</div>

<div class="note">
<p>Note: If you aren't very happy about this, read the page about <a href="catalyst.html#top">catalysts</a> before you go on.</p>
</div>

<div class="text-block">
<p>Diagrams like this are described as energy profiles. In the diagram above, you can clearly see that you need an input of energy to get the reaction going. Once the activation energy barrier has been passed, you can also see that you get even more energy released, and so the reaction is overall exothermic.</p>
<p>If you had an endothermic reaction, a simple energy profile for a non-catalysed reaction would look like this:</p>
</div>

<div class="image-container"><img src="endoprofile.gif"></div>

<div class="text-block">
<p>Unfortunately, for many reactions, the real shapes of the energy profiles are slightly different from these, and the rest of this page explores some simple differences. What matters is whether the reaction goes via a single transition state or an intermediate. We will look at these two different cases in some detail.</p>
</div>

<h3>Energy Profiles for Reactions Which Go Via a Single Transition State Only</h3>

<div class="text-block">
<p>This is much easier to talk about with a real example. The equation below shows an organic chemistry reaction in which a bromine atom is being replaced by an OH group in an organic compound. The starting compound is bromoethane, and the organic product is ethanol.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{Br} + {}^-\text{OH} \longrightarrow \text{CH}_3\text{CH}_2\text{OH} + \text{Br}^-
</div>

<div class="text-block">
<p>During the reaction one of the lone pairs of electrons on the negatively charged oxygen in the -OH group is attracted to the carbon atom with the bromine attached.</p>
<p>That's because the bromine is more electronegative than carbon, and so the electron pair in the C-Br bond is slightly closer to the bromine. The carbon atom becomes slightly positively charged and the bromine slightly negative.</p>
</div>

<div class="image-container"><img src="sn2model1.gif"></div>

<div class="text-block">
<p>As the hydroxide ion approaches the slightly positive carbon, a new bond starts to be set up between the oxygen and the carbon. At the same time, the bond between the carbon and bromine starts to break as the electrons in the bond are repelled towards the bromine.</p>
<p>At some point, the process is exactly half complete. The carbon atom now has the oxygen half-attached, the bromine half-attached, and the three other groups still there, of course.</p>
</div>

<div class="image-container"><img src="sn2model2.gif"></div>

<div class="text-block">
<p>And then the process completes:</p>
</div>

<div class="image-container"><img src="sn2model3.gif"></div>


<div class="note">
<p>Note: These diagrams have been simplified in various ways to make the process clearer. For example, the true arrangement of the lone pairs of electrons around the oxygen in the first diagram has been simplified for clarity. The bromine also has 3 lone pairs as well as the bonding pair, but they play no part. And, of course, the other groups attached to the carbon have been left out in order to concentrate on what is important.</p>
</div>

<div class="text-block">
<p>The second diagram where the bonds are half-made and half-broken is called the transition state, and it is at this point that the energy of the system is at its maximum. This is what is at the top of the activation energy barrier.</p>
</div>

<div class="image-container"><img src="sn2profile.gif"></div>

<div class="text-block">
<p>But the transition state is entirely unstable. Any tiny change in either direction will send it either forward to make the products or back to the reactants again. Neither is there anything special about a transition state except that it has this maximum energy. You can't isolate it, even for a very short time.</p>
<p>The situation is entirely different if the reaction goes through an intermediate. Again, we'll look at a specific example.</p>
</div>

<h3>Energy Profiles for Reactions Which Go Via an Intermediate</h3>

<div class="text-block">
<p>For reasons which you may well meet in the organic chemistry part of your course, a different organic bromine-containing compound reacts with hydroxide ions in an entirely different way.</p>
<p>In this case, the organic compound ionises slightly in a slow reaction to produce an intermediate positive organic ion. This then goes on to react very rapidly with hydroxide ions.</p>
</div>

<div class="image-container"><img src="sn1slow.gif"></div>

<div class="image-container"><img src="sn1ohfast.gif"></div>

<div class="note">
<p>Note: If you haven't come across the use of curly arrows in organic chemistry yet, all you need to know for now is that they show the movement of a pair of electrons. In the first equation, for example, the bonding pair of electrons in the C-Br bond moves entirely on to the bromine to make a bromide ion. In the second equation, a lone pair on the hydroxide ion moves towards the positive carbon to form a covalent bond.</p>
</div>

<div class="text-block">
<p>The big difference in this case is that the positively charged organic ion can actually be detected in the mixture. It is very unstable, and soon reacts with a hydroxide ion (or picks up its bromide ion again). But, for however short a time, it does have a real presence in the system. That shows itself in the energy profile.</p>
</div>

<div class="image-container"><img src="sn1profile.gif"></div>

<div class="text-block">
<p>The stability (however temporary and slight) of the intermediate is shown by the fact that there are small activation barriers to its conversion either into the products or back into the reactants again.</p>
<p>Notice that the barrier on the product side of the intermediate is lower than that on the reactant side. That means that there is a greater chance of it finding the extra bit of energy to convert into products. It would need a greater amount of energy to convert back to the reactants again.</p>
<p>I've labelled these peaks "ts1" and "ts2" – they both represent transition states between the intermediate and either the reactants or the products. During either conversion, there will be some arrangement of the atoms which causes an energy maximum – that's all a transition state is.</p>
</div>

<h5>And finally</h5>

<div class="text-block">
<p>It is perfectly possible to get reactions which take several steps – going through a number of different intermediates and transition states. In cases like this, you would end up with a whole "mountain range" of peaks, some of which might be simple transition states, and others with the little dips which hold intermediates. You wouldn't expect to come across problems like this at levels equivalent to UK A-level.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-eprofiles.pdf" target="_blank">Questions on energy profiles</a>
<a href="../questions/a-eprofiles.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../basicratesmenu.html#top">To the rates of reaction menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>