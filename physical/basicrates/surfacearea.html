<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Effect of Surface Area on Rates of Reaction | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes and explains the effect of changing the surface area of a solid has on determining how fast reactions take place.">
<meta name="keywords" content="rate, reaction, kinetics, surface, area">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Effect of Surface Area on Rates of Reaction</h1>

<div class="text-block">
<p>This page describes and explains the effect of changing the surface area of a solid on the rate of a reaction it is involved in. This applies to reactions involving a solid and a gas, or a solid and a liquid. It includes cases where the solid is acting as a catalyst.</p>
</div>

<h2>The Facts</h2>

<h3>What Happens?</h3>

<div class="text-block">
<p>The more finely divided the solid is, the faster the reaction happens. A powdered solid will normally produce a faster reaction than if the same mass is present as a single lump. The powdered solid has a greater surface area than the single lump.</p>
</div>

<div class="note">
<p>Note: Why normally? What exceptions can there be?</p>
<p>Imagine a case of a very fine powder reacting with a gas. If the powder was in one big heap, the gas may not be able to penetrate it. That means that its effective surface area is much the same as (or even less than) it would be if it were present in a single lump.</p>
<p>A small heap of fine magnesium powder tends to burn rather more slowly than a strip of magnesium ribbon, for example.</p>
</div>

<h3>Some Examples</h3>

<h4>Calcium carbonate and hydrochloric acid</h4>

<div class="text-block">
<p>In the lab, powdered calcium carbonate reacts much faster with dilute hydrochloric acid than if the same mass was present as lumps of marble or limestone.</p>
</div>

<div class="block-formula">
\text{CaCO}_{3(s)} + 2\text{HCl}_{(aq)} \longrightarrow \text{CaCl}_{2(aq)} + \text{H}_2\text{O}_{(l)} + \text{CO}_{s(g)}
</div>

<h4>The catalytic decomposition of hydrogen peroxide</h4>

<div class="text-block">
<p>This is another familiar lab reaction. Solid manganese(IV) oxide is often used as the catalyst. Oxygen is given off much faster if the catalyst is present as a powder than as the same mass of granules.</p>
</div>

<div class="block-formula">
2\text{H}_2\text{O}_{2(aq)} \xrightarrow{\text{MnO}_{2(s)}} 2\text{H}_2\text{O}_{(l)} + \text{O}_{2(g)}
</div>

<h4>Catalytic converters</h4>

<div class="text-block">
<p>Catalytic converters use metals like platinum, palladium and rhodium to convert poisonous compounds in vehicle exhausts into less harmful things. For example, a reaction which removes both carbon monoxide and an oxide of nitrogen is:</p>
</div>

<div class="block-formula">
2\text{CO}_{(g)} + 2\text{NO}_{(g)} \longrightarrow 2\text{CO}_{2(g)} + \text{N}_{2(g)}
</div>

<div class="text-block">
<p>Because the exhaust gases are only in contact with the catalyst for a very short time, the reactions have to be very fast. The extremely expensive metals used as the catalyst are coated as a very thin layer onto a ceramic honeycomb structure to maximise the surface area.</p>
</div>

<h2>The Explanation</h2>

<div class="text-block">
<p>You are only going to get a reaction if the particles in the gas or liquid collide with the particles in the solid. Increasing the surface area of the solid increases the chances of collision taking place.</p>
<p>Imagine a reaction between magnesium metal and a dilute acid like hydrochloric acid. The reaction involves collision between magnesium atoms and hydrogen ions.</p>
</div>

<div class="image-container"><img src="mgacideq.gif"></div>

<div class="image-container"><img src="sa.gif"></div>

<div class="text-block">
<p>Increasing the number of collisions per second increases the rate of reaction.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../basicratesmenu.html#top">To the rates of reaction menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>