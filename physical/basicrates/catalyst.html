<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Effect of Catalysts on Rates of Reaction | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes and explains the effect of adding a catalyst on the rate of a chemical reaction.">
<meta name="keywords" content="rate, reaction, kinetics, collision, activation energy, maxwell, boltzmann, boltzman, distribution, catalyst, catalysis">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Effect of Catalysts on Rates of Reaction</h1>

<div class="text-block">
<p>This page describes and explains the way that adding a catalyst affects the rate of a reaction. It assumes that you are already familiar with basic ideas about the collision theory of reaction rates, and with the Maxwell-Boltzmann distribution of molecular energies in a gas.</p>
</div>

<div class="note">
<p>Note: If you haven't already read the page about <a href="introduction.html#top">collision theory</a>, you should do so before you go on.</p>
</div>

<div class="text-block">
<p>Note that this is only a preliminary look at catalysis as far as it affects rates of reaction. If you are looking for more detail, there is a separate section dealing with catalysts which you can access via a link at the bottom of the page.</p>
</div>

<h2>The Facts</h2>

<h3>What are Catalysts?</h3>

<div class="text-block">
<p>A catalyst is a substance which speeds up a reaction, but is chemically unchanged at the end of the reaction. When the reaction has finished, you would have exactly the same mass of catalyst as you had at the beginning.</p>
</div>

<h3>Some Examples</h3>

<div class="text-block">
<p>Some common examples which you may need for other parts of your syllabus include:</p>
</div>

<table class="data-table full-width">
<tbody><tr><th>reaction</th><th>catalyst</th></tr>
<tr><td>Decomposition of hydrogen peroxide</td><td>manganese(IV) oxide, MnO<sub>2</sub></td></tr>
<tr><td>Nitration of benzene</td><td>concentrated sulfuric acid</td></tr>
<tr><td>Manufacture of ammonia by the Haber Process</td><td>iron</td></tr>
<tr><td>Conversion of SO<sub>2</sub> into SO<sub>3</sub> during the Contact Process to make sulfuric acid</td><td>vanadium(V) oxide, V<sub>2</sub>O<sub>5</sub></td></tr>
<tr><td>Hydrogenation of a C=C double bond</td><td>nickel</td></tr>
</tbody></table>

<div class="note">
<p>Note: You can find details of these and other catalytic reactions by exploring the menu for the main section on catalysis. You will find a link at the bottom of this page.</p>
</div>

<h2>The Explanation</h2>

<h3>The Key Importance of Activation Energy</h3>

<div class="text-block">
<p>Collisions only result in a reaction if the particles collide with a certain minimum energy called the activation energy for the reaction.</p>
</div>

<div class="note">
<p>Note: What follows assumes you have a reasonable idea about activation energy and its relationship with the Maxwell-Boltzmann distribution. This is covered on the introductory page about <a href="introduction.html#top">collision theory</a>.</p>
</div>

<div class="text-block">
<p>You can mark the position of activation energy on a Maxwell-Boltzmann distribution to get a diagram like this:</p>
</div>

<div class="image-container"><img src="mbdistrib2.gif"></div>

<div class="text-block">
<p>Only those particles represented by the area to the right of the activation energy will react when they collide. The great majority don't have enough energy, and will simply bounce apart.</p>
</div>

<h3>Catalysts and Activation Energy</h3>

<div class="text-block">
<p>To increase the rate of a reaction you need to increase the number of successful collisions. One possible way of doing this is to provide an alternative way for the reaction to happen which has a lower activation energy.</p>
<p>In other words, to move the activation energy on the graph like this:</p>
</div>

<div class="image-container"><img src="mbdistrib5.gif"></div>

<div class="text-block">
<p>Adding a catalyst has exactly this effect on activation energy. A catalyst provides an alternative route for the reaction. That alternative route has a lower activation energy. Showing this on an energy profile:</p>
</div>

<div class="image-container"><img src="catprofile.gif"></div>

<h3>A Word of Caution!</h3>

<div class="text-block">
<p>Be very careful if you are asked about this in an exam. The correct form of words is:</p>
<p class="quote">"A catalyst provides an alternative route for the reaction with a lower activation energy."</p>
<p>It does not "lower the activation energy of the reaction". There is a subtle difference between the two statements that is easily illustrated with a simple analogy.</p>
<p>Suppose you have a mountain between two valleys so that the only way for people to get from one valley to the other is over the mountain. Only the most active people will manage to get from one valley to the other.</p>
<p>Now suppose a tunnel is cut through the mountain. Many more people will now manage to get from one valley to the other by this easier route. You could say that the tunnel route has a lower activation energy than going over the mountain.</p>
<p>But you haven't lowered the mountain! The tunnel has provided an alternative route but hasn't lowered the original one. The original mountain is still there, and some people will still choose to climb it.</p>
<p>In the chemistry case, if particles collide with enough energy they can still react in exactly the same way as if the catalyst wasn't there. It is simply that the majority of particles will react via the easier catalysed route.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<p>These questions cover all the various factors which affect rates of reaction, not just catalysts.</p>
<a href="../questions/q-ratefactors.pdf" target="_blank">Questions on factors affecting rates of reaction</a>
<a href="../questions/a-ratefactors.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../catalysismenu.html#top">To the more detailed catalysis menu</a>
<a href="../basicratesmenu.html#top">To the rates of reaction menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>