<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Orders of Reaction and Rate Equations | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An introduction to order of reaction and rate equations">
<meta name="keywords" content="rate, reaction, kinetics, concentration, rate equation, order, rate constant">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Orders of Reaction and Rate Equations</h1>

<div class="text-block">
<p>Changing the concentration of substances taking part in a reaction usually changes the rate of the reaction. A rate equation shows this effect mathematically. Orders of reaction are a part of the rate equation. This page introduces and explains the various terms you will need to know about.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about why <a href="concentration.html#top">changing concentration affects rates of reaction</a> you might like to follow this link and come back to this page afterwards.</p>
</div>

<h2>Rate Equations</h2>

<h3>Measuring a Rate of Reaction</h3>

<div class="text-block">
<p>There are several simple ways of measuring a reaction rate. For example, if a gas was being given off during a reaction, you could take some measurements and work out the volume being given off per second at any particular time during the reaction.</p>
<p>A rate of 2 cm<sup>3</sup> s<sup>-1</sup> is obviously twice as fast as one of 1 cm<sup>3</sup> s<sup>-1</sup>.</p>
</div>

<div class="note">
<p>Note: Read cm<sup>3</sup> s<sup>-1</sup> as "cubic centimetres per second".</p>
</div>

<div class="text-block">
<p>However, for this more formal and mathematical look at rates of reaction, the rate is usually measured by looking at how fast the concentration of one of the reactants is falling at any one time.</p>
<p>For example, suppose you had a reaction between two substances A and B. Assume that at least one of them is in a form where it is sensible to measure its concentration – for example, in solution or as a gas.</p>
</div>

<div class="block-formula">
\text{A} + \text{B} \longrightarrow \text{products}
</div>

<div class="text-block">
<p>For this reaction you could measure the rate of the reaction by finding out how fast the concentration of, say, A was falling per second.</p>
<p>You might, for example, find that at the beginning of the reaction, its concentration was falling at a rate of 0.0040 mol dm<sup>-3</sup> s<sup>-1</sup>.</p>
</div>

<div class="note">
<p>Note: Read mol dm<sup>-3</sup> s<sup>-1</sup> as "moles per cubic decimetre (or litre) per second".</p>
</div>

<div class="text-block">
<p>This means that every second the concentration of A was falling by 0.0040 moles per cubic decimetre. This rate will decrease during the reaction as A gets used up.</p>
</div>

<div class="summary">
<p>For the purposes of rate equations and orders of reaction, the rate of a reaction is measured in terms of how fast the concentration of one of the reactants is falling. Its units are mol dm<sup>-3</sup> s<sup>-1</sup>.</p>
</div>
 
<h3>Orders of Reaction</h3>

<div class="text-block">
<p>I'm not going to define what order of reaction means straight away – I'm going to sneak up on it!</p>
<p>Orders of reaction are always found by doing experiments. You can't deduce anything about the order of a reaction just by looking at the equation for the reaction.</p>
<p>So let's suppose that you have done some experiments to find out what happens to the rate of a reaction as the concentration of one of the reactants, A, changes. Some of the simple things that you might find are:</p>
</div>

<h4>One possibility: The rate of reaction is proportional to the concentration of A</h4>

<div class="text-block">
<p>That means that if you double the concentration of A, the rate doubles as well. If you increase the concentration of A by a factor of 4, the rate goes up 4 times as well.</p>
<p>You can express this using symbols as:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{rate } \propto~[\text{A}] \\
\begin{aligned}
\propto~-&~\text{proportionality symbol} \\
[\text{X}]~-&~\text{concentration of X in mol dm}^{-3}
\end{aligned}
\end{gathered}
</div>

<div class="text-block">
<p>Writing a formula in square brackets is a standard way of showing a concentration measured in moles per cubic decimetre (litre).</p>
<p>You can also write this by getting rid of the proportionality sign and introducing a constant, k.</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{rate} = k[\text{A}] \\
k~-~\text{the rate constant}
\end{gathered}
</div>

<h4>Another possibility: The rate of reaction is proportional to the square of the concentration of A</h4>

<div class="text-block">
<p>This means that if you doubled the concentration of A, the rate would go up 4 times (2<sup>2</sup>). If you tripled the concentration of A, the rate would increase 9 times (3<sup>2</sup>). In symbol terms:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{rate}~\alpha~[\text{A}]^2 \\
\\
\text{rate} = k[\text{A}]^2
\end{gathered}
</div>

<h4>Generalising this</h4>

<div class="text-block">
<p>By doing experiments involving a reaction between A and B, you would find that the rate of the reaction was related to the concentrations of A and B in this way:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{rate} = k[\text{A}]^a[\text{B}]^b \\
\\
\begin{aligned}
\text{rate } -& \text{ in units of mol dm}^{-3} \text{ s}^{-1} \\
k~-&~\text{rate constant} \\
[\text{X}]~-&~\text{concentration of X} \\
a~-&~\text{order of reaction with respect to A} \\
b~-&~\text{order of reaction with respect to B}
\end{aligned}
\end{gathered}
</div>

<div class="text-block">
<p>This is called the rate equation for the reaction.</p>
<p>The concentrations of A and B have to be raised to some power to show how they affect the rate of the reaction. These powers are called the orders of reaction with respect to A and B.</p>
<p>For UK A-level purposes, the orders of reaction you are likely to meet will be 0, 1 or 2. But other values are possible including fractional ones like 1.53, for example.</p>
<p>If the order of reaction with respect to A is 0 (zero), this means that the concentration of A doesn't affect the rate of reaction. Mathematically, any number raised to the power of zero (x<sup>0</sup>) is equal to 1. That means that that particular term disappears from the rate equation.</p>
<p>The overall order of the reaction is found by adding up the individual orders. For example, if the reaction is first order with respect to both A and B (a = 1 and b = 1), the overall order is 2. We call this an overall second order reaction.</p>
</div>

<h4>Some examples</h4>

<div class="text-block">
<p>Each of these examples involves a reaction between A and B, and each rate equation comes from doing some experiments to find out how the concentrations of A and B affect the rate of reaction.</p>
</div>

<h5>Example 1</h5>

<div class="block-formula">
\text{rate} = k[\text{A}][\text{B}]
</div>

<div class="text-block">
<p>In this case, the order of reaction with respect to both A and B is 1. The overall order of reaction is 2 – found by adding up the individual orders.</p>
</div>

<div class="note">
<p>Note: Where the order is 1 with respect to one of the reactants, the "1" isn't written into the equation. [A] means [A]<sup>1</sup>.</p>
</div>

<h5>Example 2</h5>

<div class="block-formula">
\text{rate} = k[\text{B}]^2
</div>

<div class="text-block">
<p>This reaction is zero order with respect to A because the concentration of A doesn't affect the rate of the reaction. The order with respect to B is 2 – it's a second order reaction with respect to B. The reaction is also second order overall (because 0 + 2 = 2).</p>
</div>

<h5>Example 3</h5>

<div class="block-formula">
\text{rate} = k[\text{A}]
</div>

<div class="text-block">
<p>This reaction is first order with respect to A and zero order with respect to B, because the concentration of B doesn't affect the rate of the reaction. The reaction is first order overall (because 1 + 0 = 1).</p>
</div>

<h4>What if you have some other number of reactants?</h4>

<div class="text-block">
<p>It doesn't matter how many reactants there are. The concentration of each reactant will occur in the rate equation, raised to some power. Those powers are the individual orders of reaction. The overall order of the reaction is found by adding them all up.</p>
</div> 

<h3>The Rate Constant</h3>

<div class="text-block">
<p>Surprisingly, the rate constant isn't actually a true constant! It varies, for example, if you change the temperature of the reaction, add a catalyst, or change the catalyst.</p>
<p>The rate constant is constant for a given reaction only if all you are changing is the concentration of the reactants. You will find more about the effect of temperature and catalysts on the rate constant on another page.</p>
</div>

<div class="note">
<p>Note: If you want to follow up this <a href="arrhenius.html#top">further look at rate constants</a> you might like to follow this link. Alternatively, you could visit it later via the rates of reaction menu.</p>
</div>

<h3>Calculations Involving Orders of Reaction</h3>

<div class="text-block">
<p>You will almost certainly have to be able to calculate orders of reaction and rate constants from given data or from your own experiments.</p>
<p>There are all sorts of ways of doing these sums, and it is important that you practice the methods that your syllabus wants. Check your syllabus and past exam papers to see what sort of examples you need to be able to work out.</p>
</div>

<div class="note">
<p>Note: For UK A-level students, if you haven't got copies of your <a href="../../syllabuses.html#top">syllabus and past papers</a> follow this link to find out how to get hold of them.</p>
</div>

<div class="text-block">
<p>Many text books make these sums look really difficult. In fact for A-level purposes, the calculations are usually fairly trivial. You will find them explained in detail in my chemistry calculations book.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-reactionorders.pdf" target="_blank">Questions on orders or reaction</a>
<a href="../questions/a-reactionorders.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="ordermech.html#top">To a simple look at how orders of reaction are related to reaction mechanisms</a>
<a href="../basicratesmenu.html#top">To the rates of reaction menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>