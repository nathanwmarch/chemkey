<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Effect of Concentration on Rates of Reaction | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes and explains the effect of changing the concentration of a liquid or gas on how fast reactions take place.">
<meta name="keywords" content="rate, reaction, kinetics, concentration">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Effect of Concentration on Rates of Reaction</h1>

<div class="text-block">
<p>This page describes and explains the way that changing the concentration of a solution affects the rate of a reaction. Be aware that this is an introductory page only. If you are interested in orders of reaction, you will find separate pages dealing with these. You can access these via the rates of reaction menu (link at the bottom of the page).</p>
</div>

<h2>The Facts</h2>

<h3>What Happens?</h3>

<div class="text-block">
<p>For many reactions involving liquids or gases, increasing the concentration of the reactants increases the rate of reaction. In a few cases, increasing the concentration of one of the reactants may have little noticeable effect of the rate. These cases are discussed and explained further down this page.</p>
<p>Don't assume that if you double the concentration of one of the reactants that you will double the rate of the reaction. It may happen like that, but the relationship may well be more complicated.</p>
</div>

<div class="note">
<p>Note: The mathematical relationship between concentration and rate of reaction is dealt with on the page about <a href="orders.html#top">orders of reaction</a>. If you are interested, you can use this link or read about it later via the rate of reaction menu (link at the bottom of the page).</p>
</div>

<h3>Some Examples</h3>

<div class="text-block">
<p>The examples on this page all involve solutions. Changing the concentration of a gas is achieved by changing its pressure. This is covered on a separate page.</p>
</div>

<div class="note">
<p>Note: If you want to explore the <a href="pressure.html#top">effect of changing pressure</a> on the rate of a reaction, you could use this link. Alternatively, use the link to the rates of reaction menu at the bottom of this page.</p>
</div>

<h4>Zinc and hydrochloric acid</h4>

<div class="text-block">
<p>In the lab, zinc granules react fairly slowly with dilute hydrochloric acid, but much faster if the acid is concentrated.</p>
</div>

<div class="block-formula">
\text{Zn}_{(s)} + 2\text{HCl}_{(aq)} \longrightarrow \text{ZnCl}_{2(aq)} + \text{H}_{2(g)}
</div>

<h4>The catalytic decomposition of hydrogen peroxide</h4>

<div class="text-block">
<p>Solid manganese(IV) oxide is often used as a catalyst in this reaction. Oxygen is given off much faster if the hydrogen peroxide is concentrated than if it is dilute.</p>
</div>

<div class="block-formula">
2\text{H}_2\text{O}_{2(aq)} \xrightarrow{\text{MnO}_{2(s)}} 2\text{H}_2\text{O}_{(l)} + \text{O}_{2(g)}
</div>

<h4>The reaction between sodium thiosulfate solution and hydrochloric acid</h4>

<div class="text-block">
<p>This is a reaction which is often used to explore the relationship between concentration and rate of reaction in introductory courses (like GCSE). When a dilute acid is added to sodium thiosulfate solution, a pale yellow precipitate of sulfur is formed.</p>
</div>

<div class="block-formula full-width">
\text{Na}_2\text{S}_2\text{O}_{3(aq)} + 2\text{HCl}_{(aq)} \longrightarrow 2\text{NaCl}_{(aq)} + \text{H}_2\text{O}_{(l)} + \text{S}_{(s)} + \text{SO}_{2(g)}
</div>

<div class="text-block">
<p>As the sodium thiosulfate solution is diluted more and more, the precipitate takes longer and longer to form.</p>
</div>

<h2>The Explanation</h2>

<h3>Cases Where Changing the Concentration Affects the Rate of the Reaction</h3>

<div class="text-block">
<p>This is the common case, and is easily explained.</p>
</div>

<h4>Collisions involving two particles</h4>

<div class="text-block">
<p>The same argument applies whether the reaction involves collision between two different particles or two of the same particle.</p>
<p>In order for any reaction to happen, those particles must first collide. This is true whether both particles are in solution, or whether one is in solution and the other a solid. If the concentration is higher, the chances of collision are greater.</p>
</div>

<div class="image-container"><img src="conceffect.gif"></div>

<h4>Reactions involving only one particle</h4>

<div class="text-block">
<p>If a reaction only involves a single particle splitting up in some way, then the number of collisions is irrelevant. What matters now is how many of the particles have enough energy to react at any one time.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about this, then read the page about <a href="introduction.html#top">collision theory and activation energy</a> before you go on.</p>
</div>

<div class="text-block">
<p>Suppose that at any one time 1 in a million particles have enough energy to equal or exceed the activation energy. If you had 100 million particles, 100 of them would react. If you had 200 million particles in the same volume, 200 of them would now react. The rate of reaction has doubled by doubling the concentration.</p>
</div>

<h3>Cases Where Changing the Concentration Doesn't Affect the Rate of the Reaction</h3>

<div class="text-block">
<p>At first glance this seems very surprising!</p>
</div>

<h4>Where a catalyst is already working as fast as it can</h4>

<div class="text-block">
<p>Suppose you are using a small amount of a solid catalyst in a reaction, and a high enough concentration of reactant in solution so that the catalyst surface was totally cluttered up with reacting particles.</p>
<p>Increasing the concentration of the solution even more can't have any effect because the catalyst is already working at its maximum capacity.</p>
</div>

<h4>In certain multi-step reactions</h4>

<div class="text-block">
<p>This is the more important effect from an A-level point of view. Suppose you have a reaction which happens in a series of small steps. These steps are likely to have widely different rates – some fast, some slow.</p>
<p>For example, suppose two reactants A and B react together in these two stages:</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{\color{#467abf}{A}}_{(aq)} &\xrightarrow{\text{\color{#00CC99}{a very slow reaction}}} \text{X}_{(aq)} + \text{Y}_{(aq)} \\
\text{X}_{(aq)} + \text{\color{#467abf}{B}}_{(aq)} &\xrightarrow{\text{\color{#00CC99}{a very fast reaction}}} \text{other products}
\end{aligned}
</div>

<div class="text-block">
<p>The overall rate of the reaction is going to be governed by how fast A splits up to make X and Y. This is described as the rate determining step of the reaction.</p>
<p>If you increase the concentration of A, you will increase the chances of this step happening for reasons we've looked at above.</p>
<p>If you increase the concentration of B, that will undoubtedly speed up the second step, but that makes hardly any difference to the overall rate. You can picture the second step as happening so fast already that as soon as any X is formed, it is immediately pounced on by B. That second reaction is already "waiting around" for the first one to happen.</p>
</div>

<div class="note">
<p>Note: The overall rate of reaction isn't entirely independent of the concentration of B. If you lowered its concentration enough, you will eventually reduce the rate of the second reaction to the point where it is similar to the rate of the first. Both concentrations will matter if the concentration of B is low enough.</p>
<p>However, for ordinary concentrations, you can say that (to a good approximation) the overall rate of reaction is unaffected by the concentration of B.</p>
</div>

<div class="text-block">
<p>The best specific examples of reactions of this type comes from organic chemistry. These involve the reaction between a tertiary halogenoalkane (alkyl halide) and a number of possible substances – including hydroxide ions. These are examples of nucleophilic substitution using a mechanism known as S<sub>N</sub>1.</p>
</div>

<div class="note">
<p>Note: If you are interested in exploring <a href="../../mechanisms/nucsubmenu.html#top">nucleophilic substitution reactions</a> further, you could follow this link.</p>
<p>Otherwise, you can find more about how the relationship between concentration and rate of reaction is affected by reaction mechanisms by exploring the topics at the bottom of the rates of reaction menu (link below).</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<p>You will find questions about all the factors affecting rates of reaction on the page about catalysts at the end of this sequence of pages.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../basicratesmenu.html#top">To the rates of reaction menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>