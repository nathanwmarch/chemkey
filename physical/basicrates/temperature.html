<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Effect of Temperature on Rates of Reaction | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes and explains the effect of changing the temperature on how fast reactions take place.">
<meta name="keywords" content="rate, reaction, kinetics, collision, activation energy, maxwell, boltzmann, boltzman, distribution, temperature">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Effect of Temperature on Rates of Reaction</h1>

<div class="text-block">
<p>This page describes and explains the way that changing the temperature affects the rate of a reaction. It assumes that you are already familiar with basic ideas about the collision theory, and with the Maxwell-Boltzmann distribution of molecular energies in a gas.</p>
</div>

<div class="note">
<p>Note: If you haven't already read the page about <a href="introduction.html#top">collision theory</a>, you should do so before you go on.</p>
</div>

<h2>The Facts</h2>

<h3>What Happens?</h3>

<div class="text-block">
<p>As you increase the temperature the rate of reaction increases. As a rough approximation, for many reactions happening at around room temperature, the rate of reaction doubles for every 10°C rise in temperature.</p>
<p>You have to be careful not to take this too literally. It doesn't apply to all reactions. Even where it is approximately true, it may be that the rate doubles every 9°C or 11°C or whatever. The number of degrees needed to double the rate will also change gradually as the temperature increases.</p>
</div>

<div class="note">
<p>Note: You will find the <a href="arrhenius.html#top">effect of temperature on rate</a> explored in a slightly more mathematical way on a separate page.</p>
</div>

<h3>Examples</h3>

<div class="text-block">
<p>Some reactions are virtually instantaneous – for example, a precipitation reaction involving the coming together of ions in solution to make an insoluble solid, or the reaction between hydrogen ions from an acid and hydroxide ions from an alkali in solution. So heating one of these won't make any noticeable difference to the rate of the reaction.</p>
<p>Almost any other reaction you care to name will happen faster if you heat it – either in the lab, or in industry.</p>
</div>

<h2>The Explanation</h2>

<h3>Increasing the Collision Frequency</h3>

<div class="text-block">
<p>Particles can only react when they collide. If you heat a substance, the particles move faster and so collide more frequently. That will speed up the rate of reaction.</p>
<p>That seems a fairly straightforward explanation until you look at the numbers!</p>
<p>It turns out that the frequency of two-particle collisions in gases is proportional to the square root of the kelvin temperature. If you increase the temperature from 293 K to 303 K (20°C to 30°C), you will increase the collision frequency by a factor of:</p>
</div>

<div class="block-formula">
\sqrt{\frac{303}{293}} = 1.017
</div>

<div class="text-block">
<p>That's an increase of 1.7% for a 10° rise. The rate of reaction will probably have doubled for that increase in temperature – in other words, an increase of about 100%. The effect of increasing collision frequency on the rate of the reaction is very minor. The important effect is quite different</p>
</div>

<h3>The Key Importance of Activation Energy</h3>

<div class="text-block">
<p>Collisions only result in a reaction if the particles collide with enough energy to get the reaction started. This minimum energy required is called the activation energy for the reaction.</p>
</div>

<div class="note">
<p>Note: What follows assumes you have a reasonable idea about activation energy and its relationship with the Maxwell-Boltzmann distribution. This is covered on the introductory page about <a href="introduction.html#top">collision theory</a>.</p>
</div>

<div class="text-block">
<p>You can mark the position of activation energy on a Maxwell-Boltzmann distribution to get a diagram like this:</p>
</div>

<div class="image-container"><img src="mbdistrib2.gif"></div>

<div class="text-block">
<p>Only those particles represented by the area to the right of the activation energy will react when they collide. The great majority don't have enough energy, and will simply bounce apart.</p>
<p>To speed up the reaction, you need to increase the number of the very energetic particles – those with energies equal to or greater than the activation energy. Increasing the temperature has exactly that effect – it changes the shape of the graph.</p>
<p>In the next diagram, the graph labelled T is at the original temperature. The graph labelled T+t is at a higher temperature.</p>
</div>

<div class="image-container"><img src="mbdistrib3.gif"></div>

<div class="text-block">
<p>If you now mark the position of the activation energy, you can see that although the curve hasn't moved very much overall, there has been such a large increase in the number of the very energetic particles that many more now collide with enough energy to react.</p>
</div>

<div class="image-container"><img src="mbdistrib4.gif"></div>

<div class="text-block">
<p>Remember that the area under a curve gives a count of the number of particles. On the last diagram, the area under the higher temperature curve to the right of the activation energy looks to have at least doubled – therefore at least doubling the rate of the reaction.</p>
</div>

<h3>Summary</h3>

<div class="text-block">
<p>Increasing the temperature increases reaction rates because of the disproportionately large increase in the number of high energy collisions. It is only these collisions (possessing at least the activation energy for the reaction) which result in a reaction.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../basicratesmenu.html#top">To the rates of reaction menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>