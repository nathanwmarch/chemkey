<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Effect of Pressure on Rates of Reaction | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes and explains the effect of changing the pressure of a gas on how fast reactions take place.">
<meta name="keywords" content="rate, reaction, kinetics, pressure">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Effect of Pressure on Rates of Reaction</h1>

<div class="text-block">
<p>This page describes and explains the way that changing the pressure of a gas changes the rate of a reaction.</p>
</div>

<h2>The Facts</h2>

<h3>What Happens?</h3>

<div class="text-block">
<p>Increasing the pressure on a reaction involving reacting gases increases the rate of reaction. Changing the pressure on a reaction which involves only solids or liquids has no effect on the rate.</p>
</div>

<h3>An Example</h3>

<div class="text-block">
<p>In the manufacture of ammonia by the Haber Process, the rate of reaction between the hydrogen and the nitrogen is increased by the use of very high pressures.</p>
</div>

<div class="block-formula">
\text{N}_{2(g)} + 3\text{H}_{2(g)} \xrightleftharpoons{} 2\text{NH}_{3(g)}
</div>

<div class="text-block">
<p>In fact, the main reason for using high pressures is to improve the percentage of ammonia in the equilibrium mixture, but there is a useful effect on rate of reaction as well.</p>
</div>

<div class="note">
<p>Note: If you want to explore <a href="../../physmenu.html#top">equilibria</a> you will find the topic covered in a separate section of the site.</p>
</div>

<h2>The Explanation</h2>

<h3>The Relationship Between Pressure and Concentration</h3>

<div class="text-block">
<p>Increasing the pressure of a gas is exactly the same as increasing its concentration. If you have a given mass of gas, the way you increase its pressure is to squeeze it into a smaller volume. If you have the same mass in a smaller volume, then its concentration is higher.</p>
<p>You can also show this relationship mathematically if you have come across the ideal gas equation:</p>
</div>

<div class="block-formula">
\begin{gathered}
pV = nRT \\
\\
\begin{aligned}
p~-&~\text{pressure} \\
V~-&~\text{volume} \\
n~-&~\text{number of moles} \\
R~-&~\text{gas constant} \\
T~-&~\text{temperature in Kelvin}
\end{aligned}
\end{gathered}
</div> 

<div class="text-block">
<p>Rearranging this gives:</p>
</div>

<div class="block-formula">
\begin{gathered}
p = \frac{n}{V} \times \overbrace{RT}^{\clap{\text{\color{#467abf}{constant at constant temperature}}}{}} \\
\\
\begin{aligned}
p~-&~\text{pressure} \\
\frac{n}{V}~-&~\text{number of moles divided by volume = concentration}
\end{aligned}
\end{gathered}
</div>

<div class="text-block">
<p>Because "RT" is constant as long as the temperature is constant, this shows that the pressure is directly proportional to the concentration. If you double one, you will also double the other.</p>
</div>

<div class="note">
<p>Note: If you should be able to do calculations involving the ideal gas equation, but aren't very happy about them, you might be interested in my <a href="../../book.html#top">chemistry calculations book</a>.</p>
</div>

<h3>The Effect of Increasing the Pressure on the Rate of Reaction</h3>

<h4>Collisions involving two particles</h4>

<div class="text-block">
<p>The same argument applies whether the reaction involves collision between two different particles or two of the same particle.</p>
<p>In order for any reaction to happen, those particles must first collide. This is true whether both particles are in the gas state, or whether one is a gas and the other a solid. If the pressure is higher, the chances of collision are greater.</p>
</div>

<div class="image-container"><img src="gascollision.gif"></div>

<h4>Reactions involving only one particle</h4>

<div class="text-block">
<p>Things get more difficult to explain if you have reactions involving something happening to a single particle rather than being caused by a collision between two particles. The relationship between pressure and rate is much more complicated and varies from reaction to reaction. This is beyond A level.</p>
</div>

<div class="note">
<p>Note: There are theories which show that even for unimolecular reactions (reactions only involving one molecule) collisions are necessary in order for molecules to gain activation energy. If you want to know more about this, you will have to do chemistry to a higher level. You could google it, but you will find explanations are at a high level and quite mathematical.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../basicratesmenu.html#top">To the rates of reaction menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>