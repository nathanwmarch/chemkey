<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to the Collision Theory in Rates of Reaction | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes and explains the collision theory for determining how fast reactions take place.">
<meta name="keywords" content="rate, reaction, kinetics, collision, theory, activation energy, maxwell, boltzmann, boltzman, distribution, orientation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to the Collision Theory in Rates of Reaction</h1>

<div class="text-block">
<p>This page describes the collision theory of reaction rates. It concentrates on the key things which decide whether a particular collision will result in a reaction – in particular, the energy of the collision, and whether or not the molecules hit each other the right way around (the orientation of the collision).</p>
<p>The individual factors which affect the rate of a reaction (temperature, concentration, and so on) are discussed on separate pages. You can get at these via the rates of reaction menu – there is a link at the bottom of the page.</p>
<p>We are going to look in detail at reactions which involve a collision between two species.</p>
</div>

<div class="definition">
<p>Species: This is a useful term which covers any sort of particle you like – molecule, ion, or free radical.</p>
</div>

<div class="text-block">
<p>Reactions where a single species falls apart in some way are slightly simpler because you won't be involved in worrying about the orientation of collisions. Reactions involving collisions between more than two species are going to be extremely uncommon (see below).</p>
</div>

<h2>Reactions Involving Collisions Between Two Species</h2>

<div class="text-block">
<p>It is pretty obvious that if you have a situation involving two species they can only react together if they come into contact with each other. They first have to collide, and then they may react.</p>
<p>Why "may react"? It isn't enough for the two species to collide – they have to collide the right way around, and they have to collide with enough energy for bonds to break.</p>
<p>(The chances of all this happening if your reaction needed a collision involving more than 2 particles are remote. All three (or more) particles would have to arrive at exactly the same point in space at the same time, with everything lined up exactly right, and having enough energy to react. That's not likely to happen very often!)</p>
</div>

<h3>The Orientation of Collision</h3>

<div class="text-block">
<p>Consider a simple reaction involving a collision between two molecules – ethene, CH<sub>2</sub>=CH<sub>2</sub>, and hydrogen chloride, HCl, for example. These react to give chloroethane.</p>
</div>

<div class="block-formula">
\text{CH}_2{=}\text{CH}_2 + \text{HCl} \longrightarrow \text{CH}_3\text{CH}_2\text{Cl}
</div>

<div class="text-block">
<p>As a result of the collision between the two molecules, the double bond between the two carbons is converted into a single bond. A hydrogen atom gets attached to one of the carbons and a chlorine atom to the other.</p>
</div>

<div class="note">
<p>Note: The <a href="../../mechanisms/eladd/symhbr.html#top">mechanism for this reaction</a> is dealt with on a separate page. This might help you to understand why the orientation of the two molecules is so important.</p>
</div>

<div class="text-block">
<p>The reaction can only happen if the hydrogen end of the H-Cl bond approaches the carbon-carbon double bond. Any other collision between the two molecules doesn't work. The two simply bounce off each other.</p>
</div> 

<div class="image-container"><img src="collisions.gif"></div>

<div class="text-block">
<p>Of the collisions shown in the diagram, only collision 1 may possibly lead on to a reaction.</p>
<p>If you haven't read the page about the mechanism of the reaction, you may wonder why collision 2 won't work as well. The double bond has a high concentration of negative charge around it due to the electrons in the bonds. The approaching chlorine atom is also slightly negative because it is more electronegative than hydrogen. The repulsion simply causes the molecules to bounce off each other.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="../../basicorg/bonding/eneg.html#top">electronegativity</a> , you might like to follow this link.</p>
</div>

<div class="text-block">
<p>In any collision involving unsymmetrical species, you would expect that the way they hit each other will be important in deciding whether or not a reaction happens.</p>
</div>

<h3>The Energy of the Collision</h3> 

<h4>Activation Energy</h4>

<div class="text-block">
<p>Even if the species are orientated properly, you still won't get a reaction unless the particles collide with a certain minimum energy called the activation energy of the reaction.</p>
<p>Activation energy is the minimum energy required before a reaction can occur. You can show this on an energy profile for the reaction. For a simple over-all exothermic reaction, the energy profile looks like this:</p>
</div>

<div class="image-container"><img src="profile.gif"></div>

<div class="note">
<p>Note: The only difference if the reaction was endothermic would be the relative positions of the reactants and products lines. For an endothermic change, the products would have a higher energy than the reactants, and so the green arrow would be pointing upwards. It makes no difference to the discussion about the activation energy.</p>
</div>

<div class="text-block">
<p>If the particles collide with less energy than the activation energy, nothing important happens. They bounce apart. You can think of the activation energy as a barrier to the reaction. Only those collisions which have energies equal to or greater than the activation energy result in a reaction.</p>
<p>Any chemical reaction results in the breaking of some bonds (needing energy) and the making of new ones (releasing energy). Obviously some bonds have to be broken before new ones can be made. Activation energy is involved in breaking some of the original bonds.</p>
<p>Where collisions are relatively gentle, there isn't enough energy available to start the bond-breaking process, and so the particles don't react.</p>
</div>

<h4>The Maxwell-Boltzmann Distribution</h4>

<div class="text-block">
<p>Because of the key role of activation energy in deciding whether a collision will result in a reaction, it would obviously be useful to know what sort of proportion of the particles present have high enough energies to react when they collide.</p>
<p>In any system, the particles present will have a very wide range of energies. For gases, this can be shown on a graph called the Maxwell-Boltzmann Distribution which is a plot of the number of particles having each particular energy.</p>
</div>

<div class="note">
<p>Note: The graph only applies to gases, but the conclusions that we can draw from it can also be applied to reactions involving liquids.</p>
</div>

<div class="image-container"><img src="mbdistrib.gif"></div>

<div class="text-block">
<p>The area under the curve is a measure of the total number of particles present.</p>
</div>

<div class="note">
<p>Note: The reason for this lies in some maths beyond the scope of an A-level chemistry course. It is important that you remember that the area under the curve gives a count of the number of particles even if you don't understand why!</p>
</div>

<h4>The Maxwell-Boltzmann Distribution and activation energy</h4>

<div class="text-block">
<p>Remember that for a reaction to happen, particles must collide with energies equal to or greater than the activation energy for the reaction. We can mark the activation energy on the Maxwell-Boltzmann distribution:</p>
</div>

<div class="image-container"><img src="mbdistrib2.gif"></div>

<div class="text-block">
<p>Notice that the large majority of the particles don't have enough energy to react when they collide. To enable them to react we either have to change the shape of the curve, or move the activation energy further to the left. This is described on other pages.</p>
</div>

<div class="note">
<p>Note: You can change the shape of the curve by <a href="temperature.html#top">changing the temperature</a> of the reaction. You can change the position of the activation energy by <a href="catalyst.html#top">adding a catalyst</a> to the reaction.</p>
<p>You could either go straight to these pages if you are interested, or access them later via the rates of reaction menu (link at the bottom of the page).</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-basicrates.pdf" target="_blank">Questions on the collision theory</a>
<a href="../questions/a-basicrates.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../basicratesmenu.html#top">To the rates of reaction menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>