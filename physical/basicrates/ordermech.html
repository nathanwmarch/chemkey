<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Orders of Reaction and Mechanisms | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A look at the relationship between orders of reaction and mechanisms in simple cases">
<meta name="keywords" content="rate, reaction, kinetics, concentration, rate equation, order, mechanism">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">
<h1>Orders of Reaction and Mechanisms</h1>

<div class="text-block">
<p>This page looks at the relationship between orders of reaction and mechanisms in some simple cases. It explores what a mechanism is, and the idea of a rate determining step. It also explains the difference between the sometimes confusing terms "order of reaction" and "molecularity of reaction".</p>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="orders.html#top">orders of reaction</a> you ought to read the introductory page before you go on. You will find a link back to here at the bottom of the introductory page.</p>
</div>

<h2>Reaction Mechanisms</h2>

<h3>What is a Reaction Mechanism?</h3>

<div class="text-block">
<p>In any chemical change, some bonds are broken and new ones are made. Quite often, these changes are too complicated to happen in one simple stage. Instead, the reaction may involve a series of small changes one after the other.</p>
<p>A reaction mechanism describes the one or more steps involved in the reaction in a way which makes it clear exactly how the various bonds are broken and made. The following example comes from organic chemistry. It doesn't matter in the least if it is unfamiliar to you!</p>
<p>This is a reaction between 2-bromo-2-methylpropane and the hydroxide ions from sodium hydroxide solution:</p>
</div>

<div class="block-formula">
(\text{CH}_3)_3\text{CBr} + {}^-\text{OH} \longrightarrow (\text{CH}_3)_3\text{COH} + \text{Br}^-
</div>

<div class="text-block">
<p>The overall reaction replaces the bromine atom in the organic compound by an OH group.</p>
<p>The first thing that happens is that the carbon-bromine bond in a small proportion of the organic compound breaks to give ions:</p>
</div>

<div class="image-container"><img src="sn1slow.gif"></div>

<div class="text-block">
<p>Carbon-bromine bonds are reasonably strong, so this is a slow change. If the ions hit each other again, the covalent bond will reform. The curly arrow in the equation shows the movement of a pair of electrons.</p>
<p>If there is a high concentration of hydroxide ions present, the positive ion stands a high chance of hitting one of those. This step of the overall reaction will be very fast. A new covalent bond is made between the carbon and the oxygen, using one of the lone pairs on the oxygen atom.</p>
</div>

<div class="image-container"><img src="sn1ohfast.gif"></div>

<div class="text-block">
<p>Because carbon-oxygen bonds are strong, once the OH group has attached to the carbon atom, it tends to stay attached.</p>
<p>The mechanism shows that the reaction takes place in two steps and describes exactly how those steps happen in terms of bonds being broken or made. It also shows that the steps have different rates of reaction – one slow and one fast.</p>
</div>

<div class="note">
<p>Note: If you are interested in exploring more <a href="../../mechmenu.html#top">organic reaction mechanisms</a> you will find probably more than you will want to know about by following this link!</p>
</div>

<h3>The Rate-determining Step</h3>

<div class="text-block">
<p>The overall rate of a reaction (the one which you would measure if you did some experiments) is controlled by the rate of the slowest step. In the example above, the hydroxide ion can't combine with the positive ion until that positive ion has been formed. The second step is in a sense waiting around for the first slow step to happen.</p>
<p>The slow step of a reaction is known as the <i>rate-determining step</i>.</p>
<p>As long as there is a lot of difference between the rates of the various steps, when you measure the rate of a reaction, you are actually measuring the rate of the rate determining step.</p>
</div>

<h2>Reaction Mechanisms and Orders of Reaction</h2>

<div class="text-block">
<p>The examples we use at this level are the very simple ones where the orders of reaction with respect to the various substances taking part are 0, 1 or 2. These tend to have the slow step of the reaction happening before any fast step(s).</p>
<p>To try to explain how fractional orders of reaction can arise is beyond the scope of UK A-level courses.</p>
</div>

<h3>Example 1</h3>

<div class="text-block">
<p>Here is the mechanism we have already looked at. How do we know that it works like this?</p>
</div>

<div class="image-container"><img src="sn1slow.gif"></div>

<div class="image-container"><img src="sn1ohfast.gif"></div>

<div class="text-block">
<p>By doing rate of reaction experiments, you find this rate equation:</p>
</div>

<div class="block-formula">
\text{rate} = k[(\text{CH}_3)_3\text{CBr}]
</div>

<div class="text-block">
<p>The reaction is first order with respect to the organic compound, and zero order with respect to the hydroxide ions. The concentration of the hydroxide ions isn't affecting the overall rate of the reaction.</p>
<p>If the hydroxide ions were taking part in the slow step of the reaction, increasing their concentration would speed the reaction up. Since their concentration doesn't seem to matter, they must be taking part in a later fast step.</p>
<p>Increasing the concentration of the hydroxide ions will speed up the fast step, but that won't have a noticeable effect on the overall rate of the reaction. That is governed by the speed of the slow step.</p>
</div>

<div class="note">
<p>Note: If you decreased the concentration of hydroxide ions enough, you will eventually slow down the second step of this reaction to the point where both steps have similar rates. At that point, the concentration of the hydroxide ions will matter.</p>
<p>At normal concentrations, the rates of the two steps differ widely, and so this problem doesn't arise.</p>
</div>

<div class="text-block">
<p>In a simple case like this, where the slow step of the reaction is the first step, the rate equation tells you what is taking part in that slow step. In this case, the reaction is first order with respect to the organic molecule – and that's all.</p>
<p>This gives you a starting point for working out a possible mechanism. Having come up with a mechanism, you would need to find more evidence to confirm it. For example, in this case you might try to detect the presence of the positive ion that is formed in the first step.</p>
</div>

<h3>Example 2</h3>

<div class="text-block">
<p>At first sight this reaction seems identical with the last one. A bromine atom is being replaced by an OH group in an organic compound.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{Br} + {}^-\text{OH} \longrightarrow \text{CH}_3\text{CH}_2\text{OH} + \text{Br}^-
</div>

<div class="text-block">
<p>However, the rate equation for this apparently similar reaction turns out to be quite different. That means that the mechanism must be different.</p>
</div>

<div class="block-formula">
\text{rate} = k[\text{CH}_3\text{CH}_2\text{Br}][{}^-\text{OH}]
</div>

<div class="text-block">
<p>The reaction this time is first order with respect to both the organic compound and the hydroxide ions. Both of these must be taking part in the slow step of the reaction. The reaction must happen by a straightforward collision between them.</p>
</div>

<div class="image-container"><img src="sn2oh.gif"></div>

<div class="text-block">
<p>The carbon atom which is hit by the hydroxide ion has a slight positive charge on it and the bromine a slight negative one because of the difference in their electronegativities.</p>
<p>As the hydroxide ion approaches, the bromine is pushed off in one smooth action.</p>
</div>

<div class="note">
<p>Note: If you are interested in <a href="../../mechanisms/nucsub/hydroxide.html">understanding these mechanisms</a> in more detail, you could follow this link. For the purposes of this page, all that matters is that the rate equations show that the two apparently similar reactions happen by different mechanisms.</p>
</div>

<h2>Molecularity of a Reaction</h2>

<div class="text-block">
<p>You may come across an older term known as the molecularity of a reaction. This has largely dropped out of UK A-level syllabuses, but if you meet it, it is important that you understand the difference between this and the order of a reaction. The terms were sometimes used carelessly as if they mean the same thing – they don't!</p>
</div>

<h3>Order of Reaction</h3>

<div class="text-block">
<p>The important thing to realise is that this is something which can only be found by doing experiments. It gives you information about which concentrations affect the rate of the reaction. You cannot look at an equation for a reaction and deduce what the order of the reaction is going to be – you have to do some practical work!</p>
<p>Having found the order of the reaction experimentally, you may be able to make suggestions about the mechanism for the reaction – at least in simple cases.</p>
</div>

<h3>Molecularity of a Reaction</h3>

<div class="text-block">
<p>This starts at the other end! If you know the mechanism for a reaction, you can write down equations for a series of steps which make it up. Each of those steps has a molecularity.</p>
<p>The molecularity of a step simply counts the number of species (molecules, ions, atoms or free radicals) taking part in that step. For example, going back to the mechanisms we've been looking at:</p>
</div>

<div class="image-container"><img src="sn1slow.gif"></div>

<div class="text-block">
<p>This step involves a single molecule breaking into ions. Because only one species is involved in the reaction, it has a molecularity of 1. It could be described as unimolecular.</p>
<p>The second step of this mechanism, involves two ions reacting together.</p>
</div>

<div class="image-container"><img src="sn1ohfast.gif"></div>

<div class="text-block">
<p>This step has a molecularity of 2 – a bimolecular reaction.</p>
<p>The other reaction we looked at happened in a single step:</p>
</div>

<div class="image-container"><img src="sn2oh.gif"></div>

<div class="text-block">
<p>Because of the two species involved (one molecule and one ion), this reaction is also bimolecular.</p>
<p>Unless an overall reaction happens in one step (like this last one), you can't assign it a molecularity. You have to know the mechanism, and then each individual step has its own molecularity.</p>
<p>There's nothing the least bit complicated about the term molecularity. The only confusion is that you may sometimes find it used as if it meant the same as order. It doesn't!</p>
</div>

<h2>More About Reaction Mechanisms and Orders of Reaction</h2>

<div class="text-block">
<p>Relating orders of reaction to mechanisms is relatively easy where the slow step is the first step of the reaction mechanism. It isn't so easy when it is one of the later steps. I want to look at this in a bit more detail in case your syllabus requires it.</p>
<p>We will revisit the simple case first where the slow step is the first step of the mechanism.</p>
</div>

<h4>Cases where the slow step is the first step in the mechanism</h4>

<div class="text-block">
<p>Suppose you had a reaction between A and B, and it turned out (from doing some experiments) to be first order with respect to both A and B. So the rate equation is:</p>
</div>

<div class="block-formula">
\text{rate} = k[\text{A}][\text{B}]
</div>

<div class="text-block">
<p>Which of these two mechanisms is consistent with this experimental finding?</p>
</div>

<h5>Mechanism 1</h5>

<div class="block-formula">
\begin{gathered}
\text{A} \xrightarrow{\text{slow}} \text{C} + \text{D} \\
\\
\text{B} + \text{C} \xrightarrow{\text{fast}} \text{E}
\end{gathered}
</div>

<h5>Mechanism 2</h5>

<div class="block-formula">
\text{A} + \text{B} \xrightarrow{\text{slow}} \text{D} + \text{E}
</div>

<div class="text-block">
<p>Remember that in simple cases, where the slow step is the first step of the mechanism, the orders tell you what is taking part in the slow step.</p>
<p>In this case, the reaction is first order with respect to both A and B, so one molecule of each must be taking part in the slow step. That means that mechanism 2 is possible.</p>
<p>However, mechanism 1 must be wrong. One molecule of A is taking part in the slow step, but no B. The rate equation for that would be:</p>
</div>

<div class="block-formula">
\text{rate} = k[\text{A}]
</div>

<div class="note">
<p>Note: Be careful! You can't be sure that mechanism 2 is correct – it may be, but you can't be sure. A and B could, for example, react to give some sort of intermediate, which went on to turn into D and E by one or more fast steps. The rate equation would be the same, because it is governed by the same slow step, but you can't be sure about what happens after that.</p>
<p>All you can be sure of is that mechanism 1 is inconsistent with the rate equation, and so is wrong.</p>
</div>

<h4>Cases where the slow step isn't the first step in the mechanism</h4>

<div class="text-block">
<p>This is much more difficult to do and explain. I'm going to start with as simple example as possible</p>
</div>

<h5>Example 1</h5>

<div class="text-block">
<p>Suppose the mechanism for a reaction between A and B looks like this:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{A} + \text{B} \xrightleftharpoons{\text{fast}} \text{X} \\
\\
\text{A} + \text{X} \xrightarrow{\text{slow}} \text{C}
\end{gathered}
</div>

<div class="text-block">
<p>This time the slow step is the second step. Notice that the first (fast) step is reversible.</p>
<p>I need to assume that the fast step is much faster than the slow step – for reasons that I'm not going to explain. We are looking well beyond A level here!</p>
<p>The rate of the reaction will be governed by the slow step, and so the rate equation might look like this:</p>
</div>

<div class="block-formula">
\text{rate} = k[\text{A}][\text{X}]
</div>

<div class="text-block">
<p>except, of course, that X isn't one of the things you are starting with!</p>
<p>At an introductory level, the flawed discussion often goes something like this:</p>
</div>

<ul>
<li>X is made from one molecule of A and one molecule of B, and so its concentration will depend on the concentrations of A and B.</li>
<li>That means that you can replace [X] by [A][B].</li>

<li>That gives a rate equation:
<div class="block-formula">
\begin{aligned}
\text{rate} &= k[\text{A}][\text{A}][\text{B}] \\
{} &= k[\text{A}]^2[\text{B}]
\end{aligned}
</div></li>
</ul>

<div class="text-block">
<p>As it happens, that gives the right answer in this case, but this simplistic view doesn't work in all cases – as I will show below.</p>
<p>So let's start again and do it better!</p>
<p>The rate of the reaction will be governed by the slow step, and so the rate equation might look like this:</p>
</div>

<div class="block-formula">
\text{rate} = k[\text{A}][\text{X}]
</div>

<div class="text-block">
<p>except, of course, that X isn't one of the things you are starting with!</p>
<p>You need to be able to express the concentration of X in terms of [A] and [B], and you can do that because the first step is an equilibrium.</p>
<p>The equilibrium constant for the first reaction is:</p>
</div>

<div class="block-formula">
K_c = \frac{[\text{X}]}{[\text{A}][\text{B}]}
</div>

<div class="text-block">
<p>You can rearrange this to give an expression for [X]:</p>
</div>

<div class="block-formula">
[\text{X}] = K_c[\text{A}][\text{B}]
</div>

<div class="text-block">
<p> and then substitute this value into the rate expression we started with:</p>
</div>

<div class="block-formula">
\text{rate} = k[\text{A}] \times K_c[\text{A}][\text{B}]
</div>

<div class="text-block">
<p>If you sort this out, and combine the two different constants into a new rate constant, you get the rate expression:</p>
</div>

<div class="block-formula">
\text{rate} = k_1[\text{A}]^2[\text{B}]
</div>

<div class="text-block">
<p>So the reaction is second order with respect to A and first order with respect to B.</p>
</div>

<h5>Example 2</h5>

<div class="text-block">
<p>I am going to take another similar-looking example now, chosen to be deliberately awkward. This is to try to show that you can't reliably work these problems out just by looking at them.</p>
<p>This relates to the following reaction:</p>
</div>

<div class="block-formula">
\text{A} + \text{B} + \text{C} \longrightarrow \text{Y} + \text{Z}
</div>

<div class="text-block">
<p>The mechanism for this is:</p>
</div>

<div class="block-formula">
\begin{gathered}
\text{A} + \text{B} \xrightleftharpoons{\text{fast}} \text{X} + \text{Y} \\
\\
\text{C} + \text{X} \xrightarrow{\text{slow}} \text{Z}
\end{gathered}
</div>

<div class="text-block">
<p>From the slow step of the reaction, the rate equation would like like this:</p>
</div>

<div class="block-formula">
\text{rate} = k[\text{C}][\text{X}]
</div>

<div class="text-block">
<p>Up to now, this looks just the same as the previous example – but it isn't!</p>
<p>Let's do it properly, and think about the equilibrium expression for the first step in order to find a value for [X] that we can substitute into the rate equation.</p>
</div>

<div class="block-formula">
K_c = \frac{[\text{X}][\text{Y}]}{[\text{A}][\text{B}]}
</div>

<div class="text-block">
<p>The problem is that we have now got an extra variable in this equation that wasn't there before – we have got the concentration of Y. What do we do about that?</p>
<p>Well, for every mole of X formed, a mole of Y will be formed as well.</p>
<p>Provided that there was no X or Y in the mixture to start with, that means that the concentration of Y is equal to the concentration of X, and so we can substitute a value of [X] in place of the [Y], giving:</p>
</div>

<div class="block-formula">
\begin{aligned}
K_c &= \frac{[\text{X}][\text{X}]}{[\text{A}][\text{B}]} \\
{} &= \frac{[\text{X}]^2}{[\text{A}][\text{B}]}
\end{aligned}
</div>

<div class="text-block">
<p>Rearranging that to get an expression for [X] gives:</p>
</div>

<div class="block-formula">
\begin{aligned}
[\text{X}] &= \sqrt{K_c[\text{A}][\text{B}]} \\
{} &= {K_c}^{\frac{1}{2}}[\text{A}]^{\frac{1}{2}}[\text{B}]^{\frac{1}{2}}
\end{aligned}
</div>

<div class="text-block">
<p>And now you can substitute this into the rate equation:</p>
</div>

<div class="block-formula">
\text{rate} = k[\text{C}] \times {K_c}^{\frac{1}{2}}[\text{A}]^{\frac{1}{2}}[\text{B}]^{\frac{1}{2}}
</div>

<div class="text-block">
<p>and then tidy it up by combining the two constants into a single new rate constant:</p>
</div>

<div class="block-formula">
\text{rate} = k_1[\text{A}]^{\frac{1}{2}}[\text{B}]^{\frac{1}{2}}[\text{C}]
</div>

<div class="text-block">
<p>The order is 0.5 with respect to both A and B, and 1 with respect to C.</p>
<p>There is no way you could have come up with that answer if you had just looked at the equations and assumed that the concentration of X is proportional to the concentrations of A and B.</p>
<p>The presence of the extra Y makes a serious difference to the result – but just looking at the equations, that isn't at all obvious.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-ordermech.pdf" target="_blank">Questions on orders of reaction and mechanisms</a>
<a href="../questions/a-ordermech.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../basicratesmenu.html#top">To the rates of reaction menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>