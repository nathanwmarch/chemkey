<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Finding Orders of Reaction Experimentally | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An introduction to how order of reaction can be found experimentally">
<meta name="keywords" content="rate, reaction, kinetics, concentration, order, experiment, practical">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Finding Orders of Reaction Experimentally</h1>

<div class="text-block">
<p>This page is an introduction to some of the experimental methods that can be used in school labs to find orders of reaction.</p>
<p>There are two fundamentally different approaches to this – you can either investigate what happens to the initial rate of the reaction as you change concentrations, or you can follow a particular reaction all the way through, and process the results from that single reaction. We will look at these two approaches separately. Don't expect full practical details.</p>
<p>This is going to be a very long page. I wouldn't really recommend that you try to read it all in one go.</p>
</div>

<h2>Initial Rate Experiments</h2>

<h3>How Initial Rate Experiments Work</h3>

<h4>An outline of the experiments</h4>

<div class="text-block">
<p>The simplest initial rate experiments involve measuring the time taken for some easily recognisable event to happen very early on in a reaction.</p>
<p>This could include the time taken for, say, 5 cm<sup>3</sup> of gas to be produced. Or it could be the time taken for a small measurable amount of precipitate to be formed. Or you could measure the time taken for some dramatic colour change to occur. We will look at examples of all these below.</p>
<p>You then change the concentration of one of the components of the reaction, keeping everything else constant – the concentrations of other reactants, the total volume of the solution and the temperature and so on. Then you find the time taken for the same event to take place with that new concentration.</p>
<p>This is repeated for a range of concentrations of the substance you are interested in. You would need to cover a reasonably wide range of concentrations, taking perhaps 5 or so different concentrations varying from the original one down to half of it or less.</p>
<p>Obviously, you could then repeat the process by changing something else – the concentration of a different substance, or the temperature, for example.</p>
</div>

<h4>Understanding the results</h4>

<div class="text-block">
<p>We will take a simple example of an initial rate experiment where you have a gas being produced. This could be a reaction between a metal and an acid, for example, or the catalytic decomposition of hydrogen peroxide.</p>
<p>If you plotted the volume of gas given off against time, you would probably get the first graph below.</p>
</div>

<div class="image-container"><img src="inrategraph1.gif"></div>

<div class="text-block">
<p>A measure of the rate of the reaction at any point is found by measuring the slope of the graph. The steeper the slope, the faster the rate. Since we are interested in the initial rate, we would need the slope at the very beginning.</p>
<p>If you then look at the second graph, enlarging the very beginning of the first curve, you will see that it is approximately a straight line at that point. That is only a reasonable approximation if you are considering a very early stage in the reaction. The further into the reaction you go, the more the graph will start to curve.</p>
<p>Measuring the slope of a straight line is very easy. The slope in this case is simply V/t.</p>
<p>Now suppose you did the experiment again with a different (lower) concentration of the reagent. Again, we will measure the time taken for the same volume of gas to be given off, and so we are still just looking at the very beginning of the reaction:</p>
</div>

<div class="image-container"><img src="inrategraph2.gif"></div>

<div class="text-block">
<p>The initial rates (in terms of volume of gas produced per second) are:</p>
</div>

<div class="block-formula">
\Large
\begin{matrix}
\text{experiment } 1 & \frac{V}{t_1} \\
\\
\text{experiment } 2 & \frac{V}{t_2}
\end{matrix}
</div>

<div class="text-block">
<p>Now suppose you didn't actually know what the volume V was.</p>
<p>Suppose, for example, that instead of measuring the time taken to collect 5 cm<sup>3</sup> of gas, you just collected the gas up to a mark which you had made on the side of a test tube. Does it matter?</p>
<p>If you are simply wanting to compare initial rates, then it doesn't matter. If you look at the expressions in the table above, you should recognise that the initial rate is inversely proportional to the time taken. In symbols:</p>
</div>

<div class="block-formula">
\text{initial rate } \alpha~\frac{1}{t}
</div>

<div class="text-block">
<p>In experiments of this sort, you often just use 1/t as a measure of the initial rate without any further calculations.</p>
<p>You can then plot 1/t as a measure of rate against the varying concentrations of the reactant you are investigating. If the reaction is first order with respect to that substance, then you would get a straight line. That's because in a first order reaction, the rate is proportional to the concentration.</p>
</div>

<div class="image-container"><img src="rateconc1st.gif"></div>

<div class="text-block">
<p>If you get a curve, then it isn't first order. It might be second order – but it could equally well have some sort of fractional order like 1.5 or 1.78.</p>
<p>The best way around this is to plot what is known as a "log graph". The maths of this might not be familiar to you, but you may find that you are asked to do this as a part of a practical exam or practical exercise. If it is an exam, you would probably be given help as to how to go about it.</p>
</div>

<h5>The maths goes like this:</h5>

<div class="text-block">
<p>If you have a reaction involving A, with an order of n with respect to A, the rate equation says:</p>
</div>

<div class="block-formula">
\text{rate} = k[\text{A}]n
</div>

<div class="text-block">
<p>If you take the log of each side of the equation, you get:</p>
</div>

<div class="block-formula">
\log (\text{rate}) = \log k + n\log [\text{A}]
</div>

<div class="text-block">
<p>If you plotted log(rate) agains log[A], this second equation would plot as a straight line with slope n. If you measure the slope of this line, you get the order of the reaction.</p>
<p>So you would convert all the values you had for rate into log(rate). Convert all the values for [A] into log[A], and then plot the graph. This should be a straight line. If it isn't, then you have done something wrong! Measure the slope to find the order, n.</p>
</div>

<div class="note">
<p>Note: Don't worry if you don't understand logs (logarithms), or how I got from the first equation to the second one! I suspect that in the unlikely event of you needing it in an exam at this level, it would be given to you.</p>
<p>All you need to do is find the log button on your calculator and use it to convert your numbers. Practise to start with by trying to find log 2. It should give a value of 0.3010(etc). You probably have to enter 2 and then press the log button, but on some calculators it might be the other way around. If you do it the wrong way around, you will just get an error message.</p>
</div>

<h3>Some Sample Reactions</h3>

<h4>The catalytic decomposition of hydrogen peroxide</h4>

<div class="text-block">
<p>This is a simple example of measuring the initial rate of a reaction producing a gas.</p>
</div>

<div class="block-formula">
2\text{H}_2\text{O}_{2(aq)} \xrightarrow{\text{MnO}_{2(s)}} 2\text{H}_2\text{O}_{(l)} + \text{O}_{2(g)}
</div>

<div class="text-block">
<p>A simple set-up to do this might be:</p>
</div>

<div class="image-container"><img src="h2o2expt.gif"></div>

<div class="text-block">
<p>The reason for the weighing bottle containing the catalyst is to prevent introducing errors at the beginning of the experiment. Since this is the part of the reaction you are most interested in, introducing errors here would be stupid!</p>
<p>You have to find a way of adding the catalyst to the hydrogen peroxide solution without changing the volume of gas collected. If you added it to the flask using a spatula, and then quickly put the bung in, you might lose some gas before you got the bung in. Alternatively, as you pushed the bung in, you might force some air into the measuring cylinder. Either way, it makes your results meaningless.</p>
<p>To start the reaction, you just need to shake the flask so that the weighing bottle falls over, and then continue shaking to make sure the catalyst mixes evenly with the solution.</p>
<p>You could also use a special flask with a divided bottom, with the catalyst in one side, and the hydrogen peroxide solution in the other. They are easy to mix by tipping the flask.</p>
<p>If you use a 10 cm<sup>3</sup> measuring cylinder, initially full of water, you can reasonably accurately record the time taken to collect a small fixed volume of gas.</p>
<p>You could, of course, use a small gas syringe instead.</p>
<p>If you were looking at the effect of the concentration of hydrogen peroxide on the rate, then you would have to change its concentration, but keep everything else constant.</p>
<p>The temperature would have to be kept constant, so would the total volume of the solution and the mass of manganese(IV) oxide. You would also have to be sure that the manganese(IV) oxide used always came from the same bottle so that its state of division was always the same.</p>
<p>You could, of course, use much the same apparatus to find out what happened if you varied the temperature, or the mass of the catalyst, or the state of division of the catalyst.</p>
</div>

<h4>The thiosulfate-acid reaction</h4>

<div class="text-block">
<p>If you add dilute hydrochloric acid to sodium thiosulfate solution, you get the slow formation of a pale yellow precipitate of sulfur.</p>
</div>

<div class="block-formula full-width">
\text{Na}_2\text{S}_2\text{O}_{3(aq)} + 2\text{HCl}_{(aq)} \longrightarrow 2\text{NaCl}_{(aq)} + \text{H}_2\text{O}_{(l)} + \text{S}_{(s)} + \text{SO}_{2(g)}
</div>

<div class="text-block">
<p>There is a very simple, but very effective, way of measuring the time taken for a small fixed amount of precipitate to form. Stand the flask on a piece of paper with a cross drawn on it, and then look down through the solution until the cross disappears.</p>
<p>So you put a known volume of sodium thiosulfate solution in a flask. Then you add a small known volume of dilute hydrochloric acid, start timing, swirl the flask to mix everything up, and stand it on the paper with the cross on. Time how long it takes for the cross to disappear.</p>
<p>Then repeat using a smaller volume of sodium thiosulfate, but topped up to the same original volume with water. Everything else should be exactly as before.</p>
<p>If you started with, say, 50 cm<sup>3</sup> of sodium thiosulfate solution, you would repeat the experiment with perhaps, 40, 30, 20, 15 and 10 cm<sup>3</sup> – each time made up to a total of 50 cm<sup>3</sup> with water.</p>
<p>The actual concentration of the sodium thiosulfate doesn't have to be known. In each case, you could record its relative concentration. The solution with 40 cm<sup>3</sup> of sodium thiosulfate solution plus 10 cm<sup>3</sup> of water has a concentration which is 80% of the original one, for example. The one with 10 cm<sup>3</sup> of sodium thiosulfate solution plus 40 cm<sup>3</sup> of water has a concentration which is 20% of the original one.</p>
<p>When you came to plotting a rate against concentration graph, as we looked at further up the page, you would plot 1/t as a measure of the rate, and volume of sodium thiosulfate solution as a measure of concentration. Alternatively, you could plot relative concentrations – from, say, 20% to 100%. It doesn't actually matter – the shape of the graph will be identical.</p>
<p>You could also look at the effect of temperature on this reaction, by warming the sodium thiosulfate solution before you added the acid. Take the temperature after adding the acid, though, because the cold acid will cool the solution slightly.</p>
<p>This time you would change the temperature between experiments, but keep everything else constant. To get reasonable times, you would have to use a diluted version of your sodium thiosulfate solution. Using the full strength solution hot will produce enough precipitate to hide the cross almost instantly.</p>
</div>

<h4>Iodine clock reactions</h4>

<div class="text-block">
<p>There are several reactions which go under the name "iodine clock". They are all reactions which give iodine as one of the products. This is the simplest of them, but only because it involves the most familiar reagents.</p>
<p>The reaction we are looking at is the oxidation of iodide ions by hydrogen peroxide under acidic conditions.</p>
</div>

<div class="block-formula">
\text{H}_2\text{O}_{2(aq)} + 2\text{I}^-_{(aq)} + 2\text{H}^+_{(aq)} \longrightarrow \text{I}_{2(aq)} + 2\text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>The iodine is formed first as a pale yellow solution darkening to orange and then dark red, before dark grey solid iodine is precipitated.</p>
<p>There is a very clever way of picking out a when a particular very small amount of iodine has been formed.</p>
<p>Iodine reacts with starch solution to give a very deep blue solution. If you added some starch solution to the reaction above, as soon as the first trace of iodine was formed, the solution would turn blue. That doesn't actually help!</p>
<p>However, iodine also reacts with sodium thiosulfate solution.</p>
</div>

<div class="block-formula">
2{\text{S}_2\text{O}_3}^{2-}_{(aq)} + \text{I}_{2(aq)} \longrightarrow {\text{S}_2\text{O}_6}^{2-}_{(aq)} + 2\text{I}^-_{(aq)}
</div>

<div class="text-block">
<p>If you add a very small amount of sodium thiosulfate solution to your reaction mixture (including the starch solution), it will react with the iodine that is initially produced, and so the iodine won't affect the starch, and you won't get any blue colour.</p>
<p>However, when that small amount of sodium thiosulfate has been used up, there is nothing to stop the next lot of iodine produced from reacting with the starch. The mixture suddenly goes blue.</p>
</div>

<div class="note">
<p>Note: There is a neat piece of video on <a href="http://www.youtube.com/watch?v=Db0eCVt3W-E">YouTube</a> showing an iodine clock reaction (not necessarily the one I am talking about here, but it doesn't matter).</p>
<p>It shows three reactions side by side:</p>
<ul>
<li>The right-hand one is done at room temperature.</li>
<li>The left-hand one is also done at room temperature, but at three times the concentration of one of the reagents.</li>
<li>The central one is colder than room temperature, and presumably at the same concentration as the right-hand one.</li>
</ul>
<p>The blue colours appear in exactly the order you would predict.</p>
</div>

<div class="text-block">
<p>In our example, you could obviously look at the effect of changing the hydrogen peroxide concentration, or the iodide ion concentration, or the hydrogen ion concentration – each time, of course, keeping everything else constant.</p>
<p>That would let you find the orders with respect to everything taking part in the reaction.</p>
</div>

<h2>Following the course of a single reaction</h2>

<div class="text-block">
<p>Rather than doing a whole set of initial rate experiments, you can also get information about orders of reaction by following a particular reaction from start to finish.</p>
<p>There are two different ways you can do this. You can take samples of the mixture at intervals and do titrations to find out how the concentration of one of the reagents is changing. Or (and this is much easier!) you can measure some physical property of the reaction which changes as the reaction continues – for example, the volume of gas produced.</p>
<p>We need to look at these two different approaches separately.</p>
</div>

<h3>Sampling the Reaction Mixture</h3>

<div class="text-block">
<p>Bromoethane reacts with sodium hydroxide solution as follows:</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{Br} + {}^-\text{OH} \longrightarrow \text{CH}_3\text{CH}_2\text{OH} + \text{Br}^-
</div>

<div class="text-block">
<p>During the course of the reaction, both bromoethane and sodium hydroxide will get used up. However, it is relatively easy to measure the concentration of the sodium hydroxide at any one time by doing a titration with some standard acid – for example, with hydrochloric acid of a known concentration.</p>
<p>You start with known concentrations of sodium hydroxide and bromoethane, and usually it makes sense to have them both the same. Because the reaction is 1:1, if the concentrations start the same as each other, they will stay the same as each other all through the reaction.</p>
<p>So all you need to do is to take samples using a pipette at regular intervals during the reaction, and titrate them with standard hydrochloric acid in the presence of a suitable indicator.</p>
<p>That is a lot easier said than done!</p>
<p>The problem is that the reaction will still be going on in the time it takes for you to do the titration. And, of course, you only get one attempt at the titration. By the time you take another sample, the concentration of everything will have changed!</p>
<p>There are two ways around this.</p>
<p>You can slow the reaction down by diluting it, adding your sample to a larger volume of cold water before you do the titration. Then do the titration as quickly as possible. That's most effective if you are doing your reaction at a temperature above room temperature. Cooling it as well as diluting it will slow it down even more.</p>
<p>But if possible (and it is possible in the case we are talking about) it is better to stop the reaction completely before you do the titration.</p>
<p>In this case, you can stop it by adding the sample to a known volume (chosen to be an excess) of standard hydrochloric acid. That will use up all the sodium hydroxide in the mixture so that the reaction stops.</p>
<p>Now you would titrate the resulting solution with standard sodium hydroxide solution, so that you can find out how much hydrochloric acid is left over in the mixture.</p>
<p>That lets you calculate how much was used up, and so how much sodium hydroxide must have been present in the original reaction mixture.</p>
<p>This sort of technique is known as a back titration. These calculations can be quite confusing to do without some guidance. If you are interested, you will find back titrations discussed on pages 72-75 of my <a href="../../book.html">chemistry calculations book</a>.</p>
</div>

<h4>Processing the results</h4>

<div class="text-block">
<p>You will end up with a set of values for concentration of (in this example) sodium hydroxide against time. The concentrations of the bromoethane are, of course, the same as these if you started with the same concentrations of each reagent.</p>
<p>You can plot these values to give a concentration-time graph which will look something like this:</p>
</div>

<div class="image-container"><img src="cvtgraph1.gif"></div>

<div class="text-block">
<p>Now it all gets pretty tedious!</p>
<p>You need to find the rates of reaction at a number of points on the graph, and you do this by drawing tangents to the graph, and measuring their slopes.</p>
</div>

<div class="image-container"><img src="cvtgraph2.gif"></div>

<div class="text-block">
<p>You would then draw up a simple table of rate against concentration.</p>
<p>The quickest way to go on from here is to plot a log graph as described further up the page. You would convert all your rates into log(rate), and all the concentrations into log(concentration). Then plot log(rate) against log(concentration).</p>
<p>The slope of the graph gives you the order of reaction.</p>
<p>In our example of the reaction between bromoethane and sodium hydroxide solution, the order would turn out to be 2.</p>
<p>Notice that this is the overall order of the reaction – not just the order with respect to the reagent whose concentration you were measuring. The rate of reaction was falling because the concentrations of both of the reactants were falling.</p>
</div>

<div class="note">
<p>Note: This all takes ages to do – not just the practical which would probably take at least an hour, but all the graph drawing, and processing the results from the graphs. There is no obvious way this could be asked in any normal practical or written exam at this level.</p>
<p>I can see that it is just possible that you might be asked in principle how you would do it, but actually doing it could only reasonably be a part of a coursework exercise.</p>
</div>

<h3>Following the Course of the Reaction Using a Physical Property</h3>

<h4>An example where a gas is given off</h4>

<div class="text-block">
<p>A familiar example of this is the catalytic decomposition of hydrogen peroxide that we have already looked at above as an example of an initial rate experiment.</p>
</div>

<div class="block-formula">
2\text{H}_2\text{O}_{2(aq)} \xrightarrow{\text{MnO}_{2(s)}} 2\text{H}_2\text{O}_{(l)} + \text{O}_{2(g)}
</div>

<div class="text-block">
<p>This time, you would measure the oxygen given off using a gas syringe, recording the volume of oxygen collected at regular intervals.</p>
<p>So the practical side of this experiment is straightforward, but the calculation isn't.</p>
<p>The problem is that you are measuring the volume of product, whereas to find an order of reaction you have to be working in terms of the concentration of the reactants – in this case, hydrogen peroxide.</p>
<p>That means that you will have to work out the concentration of hydrogen peroxide remaining in the solution for each volume of oxygen you record. To do this, you have to be happy with calculations involving the ideal gas law, and also basic mole calculations.</p>
<p>Having got a table of concentrations against time, you will then process them in exactly the same way as I described above. Plot the graph, draw tangents to find rates at various concentrations, and then plot a log graph to find the order.</p>
</div>

<div class="note">
<p>Note: It seems to me fairly unlikely that you could ever be asked to do this in an exam situation. And, at this level, you would almost certainly be given some guidance with the calculations.</p>
<p>In a practical exam, few schools could provide a class set of the very expensive gas syringes accurate enough to produce meaningful results, and the time taken to process the results would be far greater than was available in any normal exam. That is equally true of a theory paper.</p>
<p>If you know that you can follow the course of a reaction which produces a gas using this method, that is probably all you will need. But check your syllabus, and past papers and mark schemes.</p>
</div>

<h4>Colorimetry</h4>

<div class="text-block">
<p>In any reaction involving a coloured substance (either reacting or being produced), you can follow the course of the reaction using a colorimeter.</p>
</div>

<div class="image-container"><img src="colorimeter.gif"></div>

<div class="text-block">
<p>All of this is contained in one fairly small box.</p>
<p>The colour of the light can be changed by selecting a particular coloured filter (or using some more sophisticated device like a diffraction grating). The colour is chosen so that it is the frequency of light which is absorbed by the sample.</p>
<p>Taking copper(II) sulfate solution as a familiar example, you would choose to use a red filter, because copper(II) sulfate solution absorbs red light. The more concentrated the solution is, the more of the red light it will absorb.</p>
</div>

<div class="note">
<p>Note: For an explanation of why absorbing red light makes copper(II) sulfate solution blue, see the first part of the page about <a href="../../inorganic/complexions/colour.html">the colours of complex metal ions</a>. You don't need to read about the origin of the colour for now.</p>
</div>

<div class="text-block">
<p>A commonly quoted example of the use of colorimetry in rates of reaction is the reaction between propanone and iodine in the presence of an acid catalyst.</p>
</div>

<div class="block-formula">
\text{CH}_3\text{COCH}_3 + \text{I}_2 \xrightarrow{\text{H}^+} \text{CH}_3\text{COCH}_2\text{I} + \text{H}^+ + \text{I}^- 
</div>

<div class="text-block">
<p>The solution of iodine in propanone starts off brown, and then fades through orange to yellow to colourless as the iodine is used up.</p>
<p>A colorimeter lets you measure the amount of light which is absorbed as it passes through a solution – recorded as the absorbance of the solution.</p>
<p>It is common to plot a calibration curve for a colorimeter by making up solutions of the coloured substance of known concentration and then measuring the absorbance of each under the same conditions as you will do the experiment. You then plot a graph of absorbance against concentration to give your calibration curve.</p>
<p>During your rate of reaction experiment, you read the absorbance from the meter at regular intervals, and then use your calibration curve to convert those values into concentrations.</p>
<p>Then you are faced with the same graphical methods as before.</p>
</div>

<div class="note">
<p>Note: In truth, these days, you are more likely to plug your colorimeter into a computer with the right software to do it all for you!</p>
</div>

<h4>Two other methods</h4>

<div class="text-block">
<h5>pH measurements</h5>
<p>If you have a reaction in which hydrogen ions are reacting or being produced, in principle you should be able to follow changes in their concentration using a pH meter.</p>
<p>You may be aware that pH is a measure of hydrogen ion concentration, and it isn't difficult to calculate an actual hydrogen ion concentration from a pH.</p>
<p>However, if you are measuring pH over a fairly narrow range of hydrogen ion concentrations, the pH doesn't change all that much.</p>
<p>For example, the pH of a solution containing 0.2 mol dm<sup>-3</sup> H<sup>+</sup> has a pH of 0.70. By the time that the concentration has fallen to 0.1 mol dm<sup>-3</sup>, the pH has only increased to 1.00.</p>
<p>Whether it is feasible to use a pH meter obviously depends on how accurate it is. If the pH meter only recorded to 0.1 pH units, your results aren't going to be very good.</p>
<h5>Conductivity measurements</h5>
<p>The electrical conductivity of a liquid depends on the number of ions present, and the nature of the ions. For example, we looked at this reaction much further up the page:</p>
</div>

<div class="block-formula">
\text{H}_2\text{O}_{2(aq)} + 2\text{I}^-_{(aq)} + 2\text{H}^+_{(aq)} \longrightarrow \text{I}_{2(aq)} + 2\text{H}_2\text{O}_{(l)}
</div>

<div class="text-block">
<p>During the course of the reaction, as hydrogen ions and iodide ions get used up, the conductivity of the mixture will fall.</p>
</div>

<div class="note">
<p>Note: I am not giving any more detail on this, because conductivity measurements aren't a part of any of the syllabuses that I am tracking. CIE expect you to know that it is possible to use conductivity measurements to follow the course of a reaction involving changes in the ions present, but not how you would actually carry out the experiments or process the results.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-rateexpts.pdf" target="_blank">Questions on experiments to find orders of reactions</a>
<a href="../questions/a-rateexpts.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../basicratesmenu.html#top">To the rates of reaction menu</a>
<a href="../../physmenu.html#top">To the Physical Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>