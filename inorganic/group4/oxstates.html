<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Oxidation State Trends in Group 4 | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes the increasing tendency as you go down Group 4 of the Periodic Table of elements to form compounds in which the element has an oxidation state of +2.">
<meta name="keywords" content="carbon, silicon, germanium, tin, lead, group 4, oxidation, state, number, trend, pattern">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Oxidation State Trends in Group 4</h1>

<div class="text-block">
<p>This page explores the oxidation states (oxidation numbers) shown by the Group 4 elements – carbon (C), silicon (Si), germanium (Ge), tin (Sn) and lead (Pb). It looks at the increasing tendency of the elements to form compounds in which their oxidation states are +2, particularly with reference to tin and lead.</p>
</div>

<div class="note">
<p>Note: If you aren't happy about <a href="../redoxmenu.html#top">oxidation and reduction</a> (including the use of oxidation states), it is essential to follow this link before you go any further.</p>
</div>

<h2>Some Examples of the Trends in Oxidation States</h2>

<h3>The Overall Trend</h3>

<div class="text-block">
<p>The typical oxidation state shown by elements in Group 4 is +4, found in compounds like CCl<sub>4</sub>, SiCl<sub>4</sub> and SnO<sub>2</sub>.</p>
</div>

<div class="note">
<p>Warning: Don't fall into the trap of quoting CH<sub>4</sub> as an example of carbon with a typical oxidation state of +4. Because carbon is more electronegative than hydrogen, its oxidation state in this instance is -4!</p>
</div>

<div class="text-block">
<p>However, as you go down the Group, there are more and more examples where the oxidation state is +2, such as SnCl<sub>2</sub>, PbO, and Pb<sup>2+</sup>.</p>
<p>With tin, the +4 state is still more stable than the +2, but by the time you get to lead, the +2 state is the more stable – and dominates the chemistry of lead.</p>
</div>

<h3>An Example From Carbon Chemistry</h3>

<div class="text-block">
<p>The only common example of the +2 oxidation state in carbon chemistry occurs in carbon monoxide, CO. Carbon monoxide is a strong reducing agent because it is easily oxidised to carbon dioxide – where the oxidation state is the more thermodynamically stable +4.</p>
<p>For example, carbon monoxide reduces many hot metal oxides to the metal – a reaction which is used, for example, in the extraction of iron in a blast furnace.</p>
</div>

<div class="image-container"><img src="coasra.gif"></div>

<h3>Examples From Tin Chemistry</h3>

<div class="text-block">
<p>By the time you get down the Group as far as tin, the +2 state has become increasingly common, and there is a good range of both tin(II) and tin(IV) compounds. However, tin(IV) is still the more stable oxidation state of tin.</p>
<p>That means that it will be fairly easy to convert tin(II) compounds into tin(IV) compounds. This is best shown in the fact that Sn<sup>2+</sup> ions in solution are good reducing agents.</p>
<p>For example, a solution containing tin(II) ions (for example, tin(II) chloride solution) will reduce a solution of iodine to iodide ions. In the process, the tin(II) ions are oxidised to tin(IV) ions.</p>
</div>

<div class="image-container"><img src="tini2.gif"></div>

<div class="note">
<p>Note: For simplicity, I am writing this equation (and the next few) as if the product contained simple tin(IV) ions. In fact, simple tin(IV) ions don't exist in solution. In these examples, they will usually be a part of a much larger complex ion. Don't worry about this at this level.</p>
</div>

<div class="text-block">
<p>Tin(II) ions also reduce iron(III) ions to iron(II) ions. For example, tin(II) chloride solution will reduce iron(III) chloride solution to iron(II) chloride solution. In the process, the tin(II) ions are oxidised to the more stable tin(IV) ions.</p>
</div>

<div class="image-container"><img src="tinfe3.gif"></div>

<div class="text-block">
<p>Tin(II) ions will also, of course, be easily oxidised by powerful oxidising agents like acidified potassium manganate(VII) solution (potassium permanganate solution). This reaction could be used as a titration to find the concentration of tin(II) ions in a solution.</p>
</div>

<div class="image-container"><img src="tinmno4.gif"></div>

<div class="note">
<p>Note: If you aren't happy about titration calculations (including those involving potassium manganate(VII) ), you might be interested in my <a href="../../book.html#top">chemistry calculations book</a>.</p>
</div>

<h5>And as a final example</h5>

<div class="text-block">
<p>In organic chemistry, tin and concentrated hydrochloric acid are traditionally used to reduce nitrobenzene to phenylamine (aniline). This reaction involves the tin first being oxidised to tin(II) ions and then further to the preferred tin(IV) ions.</p>
</div>

<div class="note">
<p>Note: This reaction is dealt with in some detail in the organic chemistry section of the site on a page about the <a href="../../organicprops/aniline/preparation.html#top">preparation of phenylamine</a>.</p>
</div>

<h3>Examples From Lead Chemistry</h3>

<div class="text-block">
<p>With lead, the situation is reversed. This time, the lead(II) oxidation state is the more stable, and there is a strong tendency for lead(IV) compounds to react to give lead(II) compounds.</p>
<p>Lead(IV) chloride, for example, decomposes at room temperature to give lead(II) chloride and chlorine gas:</p>
</div>

<div class="image-container"><img src="pbcl4dec.gif"></div>

<div class="text-block">
<p> and lead(IV) oxide decomposes on heating to give lead(II) oxide and oxygen.</p>
</div>

<div class="image-container"><img src="pbo2dec.gif"></div>

<div class="text-block">
<p>Lead(IV) oxide also reacts with concentrated hydrochloric acid, oxidising some of the chloride ions in the acid to chlorine gas. Once again, the lead is reduced from the +4 to the more stable +2 state.</p>
</div>

<div class="image-container"><img src="pbo2hcl.gif"></div>

<h2>Trying to Explain the Trends in Oxidation States</h2>

<div class="text-block">
<p>There's nothing surprising about the normal Group oxidation state of +4.</p>
<p>All of the elements in the group have the outer electronic structure ns<sup>2</sup>np<sub>x</sub><sup>1</sup>np<sub>y</sub><sup>1</sup>, where n varies from 2 (for carbon) to 6 (for lead). The oxidation state of +4 is where all these outer electrons are directly involved in the bonding.</p>
<p>As you get closer to the bottom of the Group, there is an increasing tendency for the s<sup>2</sup> pair not to be used in the bonding. This is often known as the inert pair effect – and is dominant in lead chemistry.</p>
<p>However, just giving it a name like "inert pair effect" explains nothing. You need to look at two different explanations depending on whether you are talking about the formation of ionic or covalent bonds.</p>
</div>

<div class="note">
<p>Note: What follows is quite likely to be way beyond what you need for the purposes of UK A-level (or its equivalent) – and is there mainly for interest. To be sure, refer to your syllabus and, more importantly, to past exam papers and mark schemes. If you are working towards a UK-based exam and haven't got these, follow this link to the <a href="../../syllabuses.html#top">syllabuses page</a> to find out how to get hold of them.</p>
</div>

<h3>The Inert Pair Effect in the Formation of Ionic Bonds</h3>

<div class="text-block">
<p>If the elements in Group 4 form 2+ ions, they will lose the p electrons, leaving the s<sup>2</sup> pair unused. For example, to form a lead(II) ion, lead will lose the two 6p electrons, but the 6s electrons will be left unchanged – an "inert pair".</p>
<p>You would normally expect ionisation energies to fall as you go down a Group as the electrons get further from the nucleus. That doesn't quite happen in Group 4.</p>
<p>This first chart shows how the total ionisation energy needed to form the 2+ ions varies as you go down the Group. The values are all in kJ mol<sup>-1</sup>.</p>
</div>

<div class="image-container"><img src="iechart12.gif"></div>

<div class="text-block">
<p>Notice the slight increase between tin and lead.</p>
<p>This means that it is slightly more difficult to remove the p electrons from lead than from tin.</p>
<p>However, if you look at the pattern for the loss of all four electrons, the discrepancy between tin and lead is much more marked. The relatively large increase between tin and lead must be because the 6s<sup>2</sup> pair is significantly more difficult to remove in lead than the corresponding 5s<sup>2</sup> pair in tin.</p>
</div>

<div class="image-container"><img src="iechart1234.gif"></div>

<div class="text-block">
<p>Again, the values are all in kJ mol<sup>-1</sup>, and the two charts are to approximately the same scale.</p>
<p>The reasons for all this lie in the Theory of Relativity. With the heavier elements like lead, there is what is known as a relativistic contraction of the electrons which tends to draw the electrons closer to the nucleus than you would expect. Because they are closer to the nucleus, they are more difficult to remove. The heavier the element, the greater this effect.</p>
<p>This affects s electrons much more than p electrons.</p>
<p>In the case of lead, the relativistic contraction makes it energetically more difficult to remove the 6s electrons than you might expect. The energy releasing terms when ions are formed (like lattice enthalpy or hydration enthalpy) obviously aren't enough to compensate for this extra energy. That means that it doesn't make energetic sense for lead to form 4+ ions.</p>
</div>

<div class="note">
<p>Note: If you want to find out more about the relativistic contraction, try a Google search on relativistic contraction electrons – but expect to get involved in some heavy reading!</p>
</div>

<h3>The Inert Pair Effect in the Formation of Covalent Bonds</h3>

<div class="text-block">
<p>You need to think about why carbon normally forms four covalent bonds rather than two.</p>
<p>Using the electrons-in-boxes notation, the outer electronic structure of carbon looks like this:</p>
</div>

<div class="image-container"><img src="cground.gif"></div>

<div class="text-block">
<p>There are only two unpaired electrons. Before carbon forms bonds, though, it normally promotes one of the s electrons to the empty p orbital.</p>
</div>

<div class="image-container"><img src="cpromote.gif"></div>

<div class="text-block">
<p>That leaves 4 unpaired electrons which (after hybridisation) can go on to form 4 covalent bonds.</p>
<p>It is worth supplying the energy to promote the s electron, because the carbon can then form twice as many covalent bonds. Each covalent bond that forms releases energy, and this is more than enough to supply the energy needed for the promotion.</p>
<p>One possible explanation for the reluctance of lead to do the same thing lies in falling bond energies as you go down the Group. Bond energies tend to fall as atoms get bigger and the bonding pair is further from the two nuclei and better screened from them.</p>
<p>For example, the energy released when two extra Pb-X bonds (where X is H or Cl or whatever) are formed may no longer be enough to compensate for the extra energy needed to promote a 6s electron into the empty 6p orbital.</p>
<p>This would would be made worse, of course, if the energy gap between the 6s and 6p orbitals was increased by the relativistic contraction of the 6s orbital.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-gp4oxstates.pdf" target="_blank">Questions on oxidation state trends in Group 4</a>
<a href="../questions/a-gp4oxstates.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../group4menu.html#top">To the Group 4 menu</a>
<a href="../../inorgmenu.html#top">To the Inorganic Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>