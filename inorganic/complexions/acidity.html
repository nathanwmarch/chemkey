<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Complex Ions – Acidity of the Hexaaqua Ions | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains why complex ions containing water as the ligand are acidic.">
<meta name="keywords" content="complex, ion, aqua, water, acid, acidity, ph">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Complex Ions – Acidity of the Hexaaqua Ions</h1>

<div class="text-block">
<p>This page explains why complex ions of the type [M(H<sub>2</sub>O)<sub>6</sub>]<sup>n+</sup> are acidic.</p>
</div>

<h2>The General Explanation</h2>

<h3>A Closer Look at the Distribution of Charge in the Ion</h3>

<div class="text-block">
<p>The pH's of solutions containing hexaaqua ions vary a lot from one metal to another (assuming you are comparing solutions of equal concentrations). However, the underlying explanation is the same for all of them.</p>
<p>We'll take the hexaaquairon(III) ion, [Fe(H<sub>2</sub>O)<sub>6</sub>]<sup>3+</sup> as typical. The structure of the ion is:</p>
</div>

<div class="image-container">
<img src="feh2oion.gif">
</div>

<div class="text-block">
<p>Each of the six water molecules are attached to the central iron(III) ion via a co-ordinate bond using one of the lone pairs on the oxygen.</p>
</div>

<div class="note">
<p>Note: This bonding is explained in some detail on the <a href="whatis.html#top">introductory page on complex ions</a>. Follow this link if you aren't sure.</p>
</div>

<div class="text-block">
<p>We'll choose one of these water molecules at random (it doesn't make any difference which one!), and look at the bonding in a bit more detail – showing all the bonds around the oxygen.</p>
<p>Imagine for the moment that the 3+ charge is located entirely on the iron.</p>
</div>

<div class="image-container">
<img src="feh2oion2.gif">
</div>

<div class="text-block">
<p>When the lone pairs on the oxygens form co-ordinate bonds with the iron, there is obviously a movement of electrons towards the iron.</p>
<p>That has an effect on the electrons in the O-H bonds. These electrons, in turn, get pulled towards the oxygen even more than usual. That leaves the hydrogen nuclei more exposed than normal.</p>
<p>The overall effect is that each of the hydrogen atoms is more positive than it is in ordinary water molecules. The 3+ charge is no longer located entirely on the iron but spread out over the whole ion – much of it on the hydrogen atoms.</p>
</div>

<h3>The Effect of Dissolving This Ion in Water</h3>

<h4>The theory</h4>

<div class="text-block">
<p>The hydrogen atoms attached to the water ligands are sufficiently positive that they can be pulled off in a reaction involving water molecules in the solution.</p>
<p>The first stage of this process is:</p>
</div>

<div class="block-formula">
\left[Fe(H_2O)_6\right]^{3+} + H_2O \xrightleftharpoons{} \left[Fe(H_2O)_5(OH)\right]^{2+} + H_3O^+
</div>

<div class="text-block">
<p>The complex ion is acting as an acid by donating a hydrogen ion to water molecules in the solution. The water is, of course, acting as a base by accepting the hydrogen ion.</p>
<p>Because of the confusing presence of water from two different sources (the ligands and the solution), it is easier to simplify this:</p>
</div>

<div class="block-formula">
\left[Fe(H_2O)_6\right]^{3+}_{(aq)} \xrightleftharpoons{} \left[Fe(H_2O)_5(OH)\right]^{2+}_{(aq)} + H^+_{(aq)}
</div>

<div class="text-block">
<p>However, if you write it like this, remember that the hydrogen ion isn't just falling off the complex ion. It is being pulled off by a water molecule in the solution. Whenever you write "H<sup>+</sup><sub>(aq)</sub>" what you really mean is a hydroxonium ion, H<sub>3</sub>O<sup>+</sup>.</p>
<p>The hexaaquairon(III) ion is quite strongly acidic giving solutions with pH's around 1.5, depending on concentration. You can get further loss of hydrogen ions as well, from a second and a third water molecule.</p>
<p>Losing a second hydrogen ion:</p>
</div>

<div class="block-formula">
\left[Fe(H_2O)_5(OH)\right]^{2+}_{(aq)} \xrightleftharpoons{} \left[Fe(H_2O)_4(OH)_2\right]^{+}_{(aq)} + H^+_{(aq)}
</div>

<div class="text-block">
<p> and a third one:</p>
</div>

<div class="block-formula">
\left[Fe(H_2O)_4(OH)_2\right]^{+}_{(aq)} \xrightleftharpoons{} \left[Fe(H_2O)_3(OH)_3\right]_{(s)} + H^+_{(aq)}
</div>

<div class="text-block">
<p>This time you end up with a complex with no charge on it. This is described as a neutral complex. Because it has no charge, it doesn't dissolve in water to any extent, and a precipitate is formed.</p>
</div>

<h4>In practice</h4>

<div class="text-block">
<p>What do you actually get in solution if you dissolve an iron(III) salt in water? In fact you get a mixture of all the complexes that you have seen in the equations above. These reactions are all equilibria, so everything will be present. The proportions depend on how concentrated the solution is.</p>
<p>The colour of the solution is very variable and depends in part on the concentration of the solution. Dilute solutions containing iron(III) ions can be pale yellow. More concentrated ones are much more orange, and may even produce some orange precipitate.</p>
<p>None of these colours represents the true colour of the [Fe(H<sub>2</sub>O)<sub>6</sub>]<sup>3+</sup> ion – which is a very pale lilac colour! That colour is only really easy to see in solids containing the ion.</p>
</div>

<div class="image-container">
<img src="fe3colours1.gif">
</div>

<div class="text-block">
<p>Looking at the equilibrium showing the loss of the first hydrogen ion:</p>
</div>

<div class="block-formula">
\underset{\text{pale lilac}}{\left[Fe(H_2O)_6\right]^{3+}} + \color{#467abf}{H_2O} \xrightleftharpoons{} \underset{\text{orange}}{\left[Fe(H_2O)_5(OH)\right]^{2+}}
+ \color{#467abf}{H_3O^+}
</div>

<div class="text-block">
<p>The colour of the new complex ion on the right-hand side is so strong that it completely masks the colour of the hexaaqua ion.</p>
<p>In concentrated solutions, the equilibrium position will be even further to the right-hand side (Le Chatelier's Principle), and so the colour darkens. You will also get significant loss of other hydrogen ions leading to some formation of the neutral complex – and so you get some precipitate.</p>
</div>

<div class="note">
<p>Note: This is a bit of a simplification, but adequate for this level. In fact you get other changes happening to the new complex ions as well. For example, the first one dimerises – introducing further equilibria and colours! Don't worry about it!</p>
</div>

<div class="text-block">
<p>You can move the position of this equilibrium by adding extra hydrogen ions from a concentrated acid – for example, by adding concentrated nitric acid to a solution of iron(III) nitrate.</p>
<p>The new hydrogen ions push the position of the equilibrium to the left so that you can see the colour of the hexaaqua ion. This is slightly easier to follow if you write the simplified version of the equilibrium.</p>
</div>

<div class="image-container">
<img src="fe3colours3.gif">
</div>

<div class="note">
<p>Note: I haven't tried to draw this colour change because when I last tried this I couldn't make it work properly (although I have done in the past)! The solution concentration is quite critical. If it is too dilute, the yellow colour disappears and you are left with a solution which is so faintly lilac that it is (in all honesty!) colourless. If the solution is too concentrated, you never get rid of the strong orange colour of the other complex ions, and so the lilac colour remains masked.</p>
</div>

<h2>Why are 3+ Ions More Acidic Than 2+ Ions</h2>

<h3>The Effect of Charge on the Acidity of the Hexaaqua Ions</h3>

<div class="text-block">
<p>Solutions containing 3+ hexaaqua ions tend to have pH's in the range from 1 to 3. Solutions containing 2+ ions have higher pH's – typically around 5 – 6, although they can go down to about 3.</p>
<p>Remember that the reason that these ions are acidic is because of the pull of the electrons towards the positive central ion. An ion with 3+ charges on it is going to pull the electrons more strongly than one with only 2+ charges.</p>
<p>In 3+ ions, the electrons in the O-H bonds will be pulled further away from the hydrogens than in 2+ ions.</p>
<p>That means that the hydrogen atoms in the ligand water molecules will have a greater positive charge in a 3+ ion, and so will be more attracted to water molecules in the solution.</p>
<p>If they are more attracted, they will be more readily lost – and so the 3+ ions are more acidic.</p>
</div>

<h2>The Effect of Ionic Radius on Acidity</h2>

<h3>Charge Density and Acidity</h3>

<div class="text-block">
<p>If you have ions of the same charge, it seems reasonable that the smaller the volume this charge is packed into, the greater the distorting effect on the electrons in the O-H bonds.</p>
<p>Ions with the same charge but in a smaller volume (a higher charge density) would be expected to be more acidic.</p>
<p>You would therefore expect to find that the smaller the radius of the metal ion, the stronger the acid. Unfortunately, it's not that simple!</p>
</div>

<div class="note">
<p>Warning! If you like your chemistry all tidy and simple, don't read any further. I have got to the point that most text books get to at this level. In fact, as I will show below, it is quite difficult to show this effect of ionic radius in any simple ions. There are too many exceptions.</p>
</div>

<h3>Plotting pKa Values Against Ionic Radius</h3>

<h4>pKa</h4>

<div class="text-block">
<p>pK<sub>a</sub> is a scale for comparing the strengths of weak acids. All you really need to know for the present topic is that the lower the value of pK<sub>a</sub>, the stronger the acid. As a reasonable approximation, if you divide the pK<sub>a</sub> value of a weak acid by 2, that will give you the pH of a 1 mol dm<sup>-3</sup> solution of the acid.</p>
</div>

<div class="note">
<p>Note: It isn't necessary for this topic, but if you need to know more about <a href="../../physical/acidbaseeqia/acids.html#top">pK<sub>a</sub></a>, you can find it by following this link.</p>
</div>

<h4>Ionic radius</h4>

<div class="text-block">
<p>A real problem arises here! Almost every data source that you refer to quotes different values for ionic radii. This is because the radius of a metal ion varies depending on what negative ion it is associated with. We are interested in what happens when the metal ion is bonded to water molecules, so haven't got simple ions at all! Whatever values we take are unlikely to represent the real situation.</p>
<p>In the graphs which follow I have taken values for ionic radii from two common sources so that you can see what a lot of difference it makes to the argument.</p>
</div>

<h4>The graphs</h4>

<div class="text-block">
<p>If it is true that the smaller the ionic radius, the stronger the acidity of the hexaaqua ion, you would expect some sort of regular increase in pK<sub>a</sub> (showing weaker acids) as ionic radius increases.</p>
<p>The following graphs plot pK<sub>a</sub> against ionic radii for the 2+ ions of the elements in the first transition series from vanadium to copper.</p>
<p>The first graph plots pK<sub>a</sub> against ionic radii taken from Chemistry Data Book by Stark and Wallace.</p>
</div>

<div class="image-container">
<img src="graph1.gif">
</div>

<div class="text-block">
<p>You can see that there is a trend for several of the ions, but it is completely broken by vanadium and chromium.</p>
<p>The second graph uses ionic radii taken from the Nuffield Advanced Science Book of Data.</p>
</div>

<div class="image-container">
<img src="graph2.gif">
</div>

<div class="text-block">
<p>In this case, the pattern is much more confused. Which is right? I don't know!!</p>
</div>

<h3>A Final Comment</h3>

<div class="text-block">
<p>During the couple of weeks I spent trying to understand this, I failed completely. The only conclusion I came to is that there probably is a relationship between ionic radius and acid strength, but that it is nothing like as simple and straightforward as most books at this level pretend.</p>
<p>The problem is that there are other more important effects operating as well (quite apart from differences in charge) and these can completely swamp the effect of the changes in ionic radius. You have to look in far more detail at the bonding in the hexaaqua ions and the product ions. This is all just too difficult to understand at this level. In fact, to be honest, I did wonder while I was researching this whether anyone really understood all of this! There is, of course, no reason why they should – it is nonsense to think that we can explain everything in chemistry at the present moment.</p>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../complexmenu.html#top">To the complex ion menu</a>
<a href="../../inorgmenu.html#top">To the Inorganic Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>