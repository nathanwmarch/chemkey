<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Colours of Complex Metal Ions | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains why many complex ions of transition metals are coloured, whereas those of other metals are not.">
<meta name="keywords" content="complex, ion, ligand, colour, d block, transition, metal, spectrum, complimentary, complementary">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Colours of Complex Metal Ions</h1>

<div class="text-block">
<p>This page is going to take a simple look at the origin of colour in complex ions – in particular, why so many transition metal ions are coloured. Be aware that this is only an introduction to what can grow into an extremely complicated topic.</p>
</div>

<h2>Why Do We See Some Compounds as Being Coloured?</h2>

<h3>White Light</h3>

<div class="text-block">
<p>You will know, of course, that if you pass white light through a prism it splits into all the colours of the rainbow. Visible light is simply a small part of an electromagnetic spectrum most of which we can't see – gamma rays, X-rays, infra-red, radio waves and so on.</p>
<p>Each of these has a particular wavelength, ranging from 10<sup>-16</sup> metres for gamma rays to several hundred metres for radio waves. Visible light has wavelengths from about 400 to 750 nm. (1 nanometre = 10<sup>-9</sup> metres.)</p>
<p>The diagram shows an approximation to the spectrum of visible light.</p>
</div>

<div class="image-container full-width">
<img src="spectrum.gif">
</div>

<div class="note">
<p>Important: This isn't a real spectrum – it's a made-up drawing. The colours are only an approximation, and so are the wavelengths assigned to them. Anyone choosing to use this spectrum as anything more than an illustration should be aware that it lacks any pretence of accuracy!</p>
</div>

<h3>Why is copper(II) sulfate solution blue?</h3>

<div class="text-block">
<p>If white light (ordinary sunlight, for example) passes through copper(II) sulfate solution, some wavelengths in the light are absorbed by the solution. Copper(II) ions in solution absorb light in the red region of the spectrum.</p>
<p>The light which passes through the solution and out the other side will have all the colours in it except for the red. We see this mixture of wavelengths as pale blue (cyan).</p>
<p>The diagram gives an impression of what happens if you pass white light through copper(II) sulfate solution.</p>
</div>

<div class="image-container">
<img src="cuabsorb.gif">
</div>

<div class="text-block">
<p>Working out what colour you will see isn't easy if you try to do it by imagining "mixing up" the remaining colours. You wouldn't have thought that all the other colours apart from some red would look cyan, for example.</p>
<p>Sometimes what you actually see is quite unexpected. Mixing different wavelengths of light doesn't give you the same result as mixing paints or other pigments.</p>
<p>You can, however, sometimes get some estimate of the colour you would see using the idea of complementary colours.</p>
</div>

<h3>Complementary Colours</h3>

<div class="text-block">
<p>If you arrange some colours in a circle, you get a "colour wheel". The diagram shows one possible version of this. An internet search will throw up many different versions!</p>
</div>

<div class="image-container">
<img src="colourwheel.gif">
</div>

<div class="text-block">
<p>Colours directly opposite each other on the colour wheel are said to be complementary colours. Blue and yellow are complementary colours; red and cyan are complementary; and so are green and magenta.</p>
<p>Mixing together two complementary colours of light will give you white light. </p>
</div>

<div class="note">
<p>Beware: That is NOT the same as mixing together paint colours. If you mix yellow and blue paint you don't get white paint. Is this confusing? YES!</p>
</div>

<div class="text-block">
<p>What this all means is that if a particular colour is absorbed from white light, what your eye detects by mixing up all the other wavelengths of light is its complementary colour. Copper(II) sulfate solution is pale blue (cyan) because it absorbs light in the red region of the spectrum. Cyan is the complementary colour of red.</p>
</div>

<div class="note">
<p>Note: If you are interested in understanding the relationship between colour absorbed and colour seen (beyond the very basic description above), find your way to lesson 2 ("Color and Vision") of "Light Waves and Vision" on <a href="http://www.physicsclassroom.com/">The Physics Classroom</a>. I'm not giving a direct link to those pages, because that site is still developing and it is safer to give a link to the front page of the site. This is the most understandable explanation I have found anywhere on the web.</p>
</div>

<h2>The Origin of Colour in Complex Ions</h2>

<h3>Transition Metal v Other Metal Complex Ions</h3>

<div class="text-block">
<h4>What is a transition metal?</h4>
<p>We often casually talk about the transition metals as being those in the middle of the Periodic Table where d orbitals are being filled, but these should really be called d block elements rather than transition elements (or metals).</p>
</div>

<div class="note">
<p>Note: If you don't understand about the <a href="../../atoms/properties/elstructs.html#top">filling of orbitals in the Periodic Table</a>, then you must follow this link before you go on.</p>
</div>

<div class="text-block">
<p>This shortened version of the Periodic Table shows the first row of the d block, where the 3d orbitals are being filled.</p>
</div>

<div class="image-container">
<img src="ptdblock.gif">
</div>

<div class="text-block">
<p>The usual definition of a transition metal is one which forms one or more stable ions which have incompletely filled d orbitals. </p>
</div>

<div class="note">
<p>Note: The most recent IUPAC definition includes the possibility of the element itself having incomplete d orbitals as well. This is unlikely to be a big problem (it only really arises with scandium), but it would pay you to learn the version your syllabus wants. Both versions of the definition are currently in use in various UK-based syllabuses. If you are working towards a UK-based exam and haven't got a copy of your <a href="../../syllabuses.html">syllabus</a>, follow this link to find out how to get one.</p>
</div>

<div class="text-block">
<p>Zinc with the electronic structure [Ar] 3d<sup>10</sup>4s<sup>2</sup> doesn't count as a transition metal whichever definition you use. In the metal, it has a full 3d level. When it forms an ion, the 4s electrons are lost – again leaving a completely full 3d level.</p>
<p>At the other end of the row, scandium ( [Ar] 3d<sup>1</sup>4s<sup>2</sup> ) doesn't really counts as a transition metal either. Although there is a partially filled d level in the metal, when it forms its ion, it loses all three outer electrons.</p>
<p>The Sc<sup>3+</sup> ion doesn't count as a transition metal ion because its 3d level is empty.</p>
</div>

<h4>Some sample colours</h4>

<div class="text-block">
<p>The diagrams show the approximate colours of some typical hexaaqua metal ions, with the formula [ M(H<sub>2</sub>O)<sub>6</sub> ] <sup>n+</sup>. The charge on these ions is typically 2+ or 3+.</p>
</div>

<div class="note">
<p>Note: If you aren't happy about <a href="names.html#top">naming complex ions</a>, you might find it useful to follow this link.</p>
</div>

<div class="text-block">
<p>Non-transition metal ions</p>
</div>

<div class="image-container">
<img src="nontmions.gif">
</div>

<div class="text-block">
<p>These ions are all colourless. (Sorry, I can't do genuinely colourless!)</p>
<p>Transition metal ions</p>
</div>

<div class="image-container">
<img src="tmions.gif">
</div>

<div class="text-block">
<p>The corresponding transition metal ions are coloured. Some, like the hexaaquamanganese(II) ion (not shown) and the hexaaquairon(II) ion, are quite faintly coloured – but they are coloured.</p>
<p>So what causes transition metal ions to absorb wavelengths from visible light (causing colour) whereas non-transition metal ions don't? And why does the colour vary so much from ion to ion?</p>
</div>

<h3>The Origin of Colour in Complex Ions Containing Transition mMtals</h3>

<div class="text-block">
<p>Complex ions containing transition metals are usually coloured, whereas the similar ions from non-transition metals aren't. That suggests that the partly filled d orbitals must be involved in generating the colour in some way. Remember that transition metals are defined as having partly filled d orbitals.</p>
</div>

<h4>Octahedral complexes</h4>

<div class="text-block">
<p>For simplicity we are going to look at the octahedral complexes which have six simple ligands arranged around the central metal ion. The argument isn't really any different if you have multidentate ligands – it's just slightly more difficult to imagine!</p>
</div>

<div class="note">
<p>Note: If you aren't sure about the <a href="shapes.html#top">shapes of complex ions</a>, you might find it useful to follow this link before you go on. You only need to read the beginning of that page.</p>
<p>If you don't know what a ligand is, you should read the <a href="whatis.html#top">introduction to complex ions</a> as a matter of urgency!</p>
</div>

<div class="text-block">
<p>When the ligands bond with the transition metal ion, there is repulsion between the electrons in the ligands and the electrons in the d orbitals of the metal ion. That raises the energy of the d orbitals.</p>
<p>However, because of the way the d orbitals are arranged in space, it doesn't raise all their energies by the same amount. Instead, it splits them into two groups.</p>
<p>The diagram shows the arrangement of the d electrons in a Cu<sup>2+</sup> ion before and after six water molecules bond with it.</p>
</div>

<div class="image-container">
<img src="dsplitcu.gif">
</div>

<div class="text-block">
<p>Whenever 6 ligands are arranged around a transition metal ion, the d orbitals are always split into 2 groups in this way – 2 with a higher energy than the other 3.</p>
<p>The size of the energy gap between them (shown by the blue arrows on the diagram) varies with the nature of the transition metal ion, its oxidation state (whether it is 3+ or 2+, for example), and the nature of the ligands.</p>
<p>When white light is passed through a solution of this ion, some of the energy in the light is used to promote an electron from the lower set of orbitals into a space in the upper set.</p>
</div>

<div class="image-container">
<img src="dpromotecu.gif">
</div>

<div class="text-block">
<p>Each wavelength of light has a particular energy associated with it. Red light has the lowest energy in the visible region. Violet light has the greatest energy.</p>
<p>Suppose that the energy gap in the d orbitals of the complex ion corresponded to the energy of yellow light.</p>
</div>

<div class="image-container">
<img src="absorbyellow.gif">
</div>

<div class="text-block">
<p>The yellow light would be absorbed because its energy would be used in promoting the electron. That leaves the other colours.</p>
<p>Your eye would see the light passing through as a dark blue, because blue is the complementary colour of yellow.</p>
</div>

<div class="note">
<p>Warning: This is a major simplification, but is adequate for this level (UK A-level or the equivalent). It doesn't, for example, account for absorption happening over a broad range of wavelengths rather than a single one, or for cases where there is more than one colour absorbed from different parts of the spectrum. If your syllabus wants you to know about the way the <a href="colour2.html">shapes of the d orbitals</a> determine how the energies split, then follow this link for a brief explanation.</p>
</div>

<div class="text-block">
<p>What about non-transition metal complex ions?</p>
<p>Non-transition metals don't have partly filled d orbitals. Visible light is only absorbed if some energy from the light is used to promote an electron over exactly the right energy gap. Non-transition metals don't have any electron transitions which can absorb wavelengths from visible light.</p>
<p>For example, although scandium is a member of the d block, its ion (Sc<sup>3+</sup>) hasn't got any d electrons left to move around. This is no different from an ion based on Mg<sup>2+</sup> or Al<sup>3+</sup>. Scandium(III) complexes are colourless because no visible light is absorbed.</p>
<p>In the zinc case, the 3d level is completely full – there aren't any gaps to promote an electron in to. Zinc complexes are also colourless.</p>
</div>

<h4>Tetrahedral complexes</h4>

<div class="text-block">
<p>Simple tetrahedral complexes have four ligands arranged around the central metal ion. Again the ligands have an effect on the energy of the d electrons in the metal ion. This time, of course, the ligands are arranged differently in space relative to the shapes of the d orbitals.</p>
<p>The net effect is that when the d orbitals split into two groups, three of them have a greater energy, and the other two a lesser energy (the opposite of the arrangement in an octahedral complex).</p>
<p>Apart from this difference of detail, the explanation for the origin of colour in terms of the absorption of particular wavelengths of light is exactly the same as for octahedral complexes.</p>
</div>

<h3>The Factors Affecting the Colour of a Transition Metal Complex Ion</h3>

<div class="text-block">
<p>In each case we are going to choose a particular metal ion for the centre of the complex, and change other factors. Colour changes in a fairly haphazard way from metal to metal across a transition series.</p>
</div>

<h4>The nature of the ligand</h4>

<div class="text-block">
<p>Different ligands have different effects on the energies of the d orbitals of the central ion. Some ligands have strong electrical fields which cause a large energy gap when the d orbitals split into two groups. Others have much weaker fields producing much smaller gaps.</p>
<p>Remember that the size of the gap determines what wavelength of light is going to get absorbed.</p>
<p>The list shows some common ligands. Those at the top produce the smallest splitting; those at the bottom the largest splitting.</p>
</div>

<div class="image-container">
<img src="ligandsplit.gif">
</div>

<div class="text-block">
<p>The greater the splitting, the more energy is needed to promote an electron from the lower group of orbitals to the higher ones. In terms of the colour of the light absorbed, greater energy corresponds to shorter wavelengths.</p>
<p>That means that as the splitting increases, the light absorbed will tend to shift away from the red end of the spectrum towards orange, yellow and so on.</p>
<p>There is a fairly clear-cut case in copper(II) chemistry.</p>
<p>If you add an excess of ammonia solution to hexaaquacopper(II) ions in solution, the pale blue (cyan) colour is replaced by a dark inky blue as some of the water molecules in the complex ion are replaced by ammonia.</p>
</div>

<div class="image-container">
<img src="cucolours.gif">
</div>

<div class="text-block">
<p>The first complex must be absorbing red light in order to give the complementary colour cyan. The second one must be absorbing in the yellow region in order to give the complementary colour dark blue.</p>
<p>Yellow light has a higher energy than red light. You need that higher energy because ammonia causes more splitting of the d orbitals than water does.</p>
<p>It isn't often as simple to see as this, though! Trying to sort out what is being absorbed when you have murky colours not on the simple colour wheel further up the page is much more of a problem.</p>
<p>The diagrams show some approximate colours of some ions based on chromium(III).</p>
</div>

<div class="image-container">
<img src="cr3colours.gif">
</div>

<div class="text-block">
<p>It is obvious that changing the ligand is changing the colour, but trying to explain the colours in terms of our simple theory isn't easy.</p>
</div>

<div class="note">
<p>Note: To be honest, I spent a couple of weeks trying to find a way of doing this simply, based on a simple colour wheel, and eventually gave up. Life is too short!</p>
</div>

<h4>The oxidation state of the metal</h4>

<div class="text-block">
<p>As the oxidation state of the metal increases, so also does the amount of splitting of the d orbitals.</p>
<p>Changes of oxidation state therefore change the colour of the light absorbed, and so the colour of the light you see.</p>
<p>Taking another example from chromium chemistry involving only a change of oxidation state (from +2 to +3):</p>
</div>

<div class="image-container">
<img src="cr23colours.gif">
</div>

<div class="text-block">
<p>The 2+ ion is almost the same colour as the hexaaquacopper(II) ion, and the 3+ ion is the hard-to-describe violet-blue-gey colour.</p>
</div>

<h4>The co-ordination of the ion</h4>

<div class="text-block">
<p>Splitting is greater if the ion is octahedral than if it is tetrahedral, and therefore the colour will change with a change of co-ordination. Unfortunately, I can't think of a single simple example to illustrate this with!</p>
<p>The problem is that an ion will normally only change co-ordination if you change the ligand – and changing the ligand will change the colour as well. You can't isolate out the effect of the co-ordination change.</p>
<p>For example, a commonly quoted case comes from cobalt(II) chemistry, with the ions [Co(H<sub>2</sub>O)<sub>6</sub>]<sup>2+</sup> and [CoCl<sub>4</sub>]<sup>2-</sup>.</p>
</div>

<div class="image-container">
<img src="co2colours.gif">
</div>

<div class="text-block">
<p>The difference in the colours is going to be a combination of the effect of the change of ligand, and the change of the number of ligands.</p>
</div>

<div class="note">
<p>Note: There is an interesting range of compounds described as "thermochromic". These change colour on heating. An example is [(CH<sub>3</sub>CH<sub>2</sub>)<sub>2</sub>NH<sub>2</sub>]<sub>2</sub>CuCl<sub>4</sub>. This contains the tetrachlorocuprate(II) ion combined with a positive ion which is essentially an ammonium ion where two of the hydrogens have been replaced by ethyl groups.</p>
<p>This is a bright green solid in the cold, but changes to bright yellow at 43°C.</p>
<p>The bright green solid has the four chlorines arranged around the central copper(II) ion in a square planar arrangement. The yellow one has them arranged in a distorted tetrahedron. The change of colour is due to the change of ligand arrangement.</p><p>This is degree level stuff. If you want to follow it up, you could do a Google (including Google Books) search for thermochromic tetrachlorocuprate(II).</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-complexcolours.pdf" target="_blank">Questions on the origin of colour in complex ions</a>
<a href="../questions/a-complexcolours.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../complexmenu.html#top">To the complex ion menu</a>
<a href="../../inorgmenu.html#top">To the Inorganic Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="extended-link-list">
<p>You might also be interested in:<p>
<div class="link-with-description">
<a href="../../analysis/uvvisiblemenu.html">UV-visible spectrometry</a>
<p>This looks at the origin of colour in organic compounds in some detail. It has more about the electromagnetic spectrum, a description of a UV-visible absorption spectrometer, and an explanation of how this can be used to measure the concentrations of dilute solutions of coloured compounds.</p>
</div>
</div>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>