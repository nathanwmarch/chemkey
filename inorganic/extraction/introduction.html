<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>An Introduction to the Extraction of Metals | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Looks at the various factors which influence the choice of method for extracting metals from their ores">
<meta name="keywords" content="metal, metals, ore, ores, extract, extraction, reduction, reduce, reducing, agent, carbon, electrolysis, iron, aluminium, titanium, copper">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>An Introduction to the Extraction of Metals</h1>

<div class="text-block">
<p>This page looks at the various factors which influence the choice of method for extracting metals from their ores, including reduction by carbon, reduction by a reactive metal (like sodium or magnesium), and by electrolysis.</p>
<p>Details for the extraction of aluminium, copper, iron and titanium are given in separate pages in this section.</p>
</div>

<h2>From Ore to Metal</h2>

<h3>What are "Ores"?</h3>

<div class="text-block">
<p>An ore is any naturally-occurring source of a metal that you can economically extract the metal from.</p>
<p>Aluminium, for example, is the most common metal in the Earth's crust, occurring in all sorts of minerals. However, it isn't economically worthwhile to extract it from most of these minerals. Instead, the usual ore of aluminium is bauxite – which contains from 50 – 70% of aluminium oxide.</p>
</div>

<div class="note">
<p>Note: We always treat bauxite as if it was aluminium oxide for chemistry purposes, although it is actually more complicated than that in reality.</p>
</div>

<div class="text-block">
<p>Copper is much rarer, but fortunately can be found in high-grade ores (ones containing a high percentage of copper) in particular places. Because copper is a valuable metal, it is also worth extracting it from low-grade ores as well.</p>
<p>Ores are commonly oxides – for example:</p>
</div>

<table class="list-table">
<tbody><tr><td>bauxite</td><td>Al<sub>2</sub>O<sub>3</sub></td></tr>
<tr><td>haematite</td><td>Fe<sub>2</sub>O<sub>3</sub></td></tr>
<tr><td>rutile</td><td>TiO<sub>2</sub></td></tr>
</tbody></table>

<div class="text-block">
<p> or sulfides – for example:</p>
</div>

<table class="list-table">
<tbody><tr><td>pyrite</td><td>FeS<sub>2</sub></td></tr>
<tr><td>chalcopyrite</td><td>CuFeS<sub>2</sub></td></tr>
</tbody></table>

<div class="text-block">
<p> and a whole lot of other things as well which we won't actually come across as a part of this topic for UK A-level purposes.</p>
</div>

<h3>Concentrating the Ore</h3>

<div class="text-block">
<p>This simply means getting rid of as much of the unwanted rocky material as possible before the ore is converted into the metal.</p>
<p>In some cases this is done chemically. For example, pure aluminium oxide is obtained from bauxite by a process involving a reaction with sodium hydroxide solution. This is described in detail on the aluminium page in this section.</p>
<p>Some copper ores can be converted into copper(II) sulfate solution by leaving the crushed ore in contact with dilute sulfuric acid for a long time. Copper can then be extracted from the copper(II) sulfate solution.</p>
<p>But, in many cases, it is possible to separate the metal compound from unwanted rocky material by physical means. A common example of this involves froth flotation.</p>
</div>

<h4>Froth flotation</h4>

<div class="text-block">
<p>The ore is first crushed and then treated with something which will bind to the particles of the metal compound that you want and make those particles hydrophobic. "Hydrophobic" literally means "water fearing".</p>
<p>In concentrating copper ores, for example, pine oil is often used. The pine oil binds to the copper compounds, but not to the unwanted rocky material.</p>
<p>The treated ore is then put in a large bath of water containing a foaming agent (a soap or detergent of some kind), and air is blown through the mixture to make a lot of bubbles.</p>
<p>Because they are water-repellent, the coated particles of the metal compound tend to be picked up by the air bubbles, float to the top of the bath, and are allowed to flow out over the sides.</p>
<p>The rest of the rocky material stays in the bath.</p>
</div>

<h3>Reducing the Metal Compound to the Metal</h3>

<h4>Why is this reduction?</h4>

<div class="text-block">
<p>At its simplest, where you are starting from metal oxides, the ore is being reduced because oxygen is being removed.</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{Fe}_2\text{O}_3 &\xrightarrow{\text{removal of oxygen = reduction}} \text{Fe} \\
\\
\text{Al}_2\text{O}_3 &\xrightarrow{\text{removal of oxygen = reduction}} \text{Al}
\end{aligned}
</div>

<div class="text-block">
<p>However, if you are starting with a sulfide ore, for example, that's not a lot of help!</p>
<p>It is much more helpful to use the definition of reduction in terms of addition of electrons.</p>
<p>To a reasonable approximation, you can think of these ores as containing positive metal ions. To convert them to the metal, you need to add electrons – reduction.</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{Fe}^{3+} + 3\text{e}^- &\xrightarrow{\text{addition of electrons = reduction}} \text{Fe} \\
\\
\text{Al}^{3+} + 3\text{e}^- &\xrightarrow{\text{addition of electrons = reduction}} \text{Al} \\
\\
\text{Cu}^{2+} + 2\text{e}^- &\xrightarrow{\text{addition of electrons = reduction}} \text{Cu}
\end{aligned}
</div>

<div class="note">
<p>Note: In some compounds the metal may not literally be present as a positive ion. Instead, it may be part of a covalent bond – but will always be the least electronegative element present, and so will carry some degree of positive charge. That means that its oxidation state will always be positive. Reducing that oxidation state to zero (in the raw element) will always involve adding electrons.</p>
<p>If you aren't sure about <a href="../../inorganic/redox/oxidnstates.html#top">oxidation states</a> you could follow this link to find out about them.</p>
</div>

<h4>Choosing a method of reduction</h4>

<div class="text-block">
<p>There are various economic factors you need to think about in choosing a method of reduction for a particular ore. These are all covered in detail on other pages in this section under the extractions of particular metals. What follows is a quick summary.</p>
<p>You need to consider:</p>
</div>

<ul>
<li>the cost of the reducing agent</li>
<li>energy costs</li>
<li>the desired purity of the metal</li>
</ul>

<div class="text-block">
<p>There may be various environmental considerations as well – some of which will have economic costs.</p>
</div>

<h5>Carbon reduction</h5>

<div class="text-block">
<p>Carbon (as coke or charcoal) is cheap. It not only acts as a reducing agent, but it also acts as the fuel to provide heat for the process.</p>
<p>However, in some cases (for example with aluminium) the temperature needed for carbon reduction is too high to be economic – so a different method has to be used.</p>
<p>Carbon may also be left in the metal as an impurity. Sometimes this can be removed afterwards (for example, in the extraction of iron); sometimes it can't (for example in producing titanium), and a different method would have to be used in cases like this.</p>
</div>

<h5>Reduction using a more reactive metal</h5>

<div class="text-block">
<p>Titanium is produced by reducing titanium(IV) chloride using a more reactive metal such as sodium or magnesium. As you will see if you read the page about titanium extraction, this is the only way of producing high purity metal.</p>
</div>

<div class="block-formula">
\text{TiCl}_4 + 4\text{Na} \longrightarrow \text{Ti} + 4\text{NaCl}
</div>

<div class="text-block">
<p>The more reactive metal sodium releases electrons easily as it forms its ions:</p>
</div>

<div class="block-formula">
4\text{Na} \longrightarrow 4\text{Na}^+ + 4\text{e}^-
</div>

<div class="text-block">
<p>These electrons are used to reduce the titanium(IV) chloride:</p>
</div>

<div class="block-formula">
\text{TiCl}_4 + 4\text{e}^- \longrightarrow \text{Ti} + 4\text{Cl}^-
</div>

<div class="note">
<p>Note: This is a good example of a reduction in which the metal isn't originally present as an ion. Titanium(IV) chloride is a covalent liquid. The reduction is from titanium in the +4 oxidation state to the metal in the zero oxidation state.</p>
</div>

<div class="text-block">
<p>The downside of this is expense. You have first to extract (or to buy) the sodium or magnesium. The more reactive the metal is, the more difficult and expensive the extraction becomes. That means that you are having to use a very expensive reducing agent to extract the titanium.</p>
<p>As you will see if you read the page about titanium extraction, there are other problems in its extraction which also add to the cost.</p>
</div>

<h5>Reduction by electrolysis</h5>

<div class="text-block">
<p>This is a common extraction process for the more reactive metals – for example, for aluminium and metals above it in the electrochemical series. You may also come across it in other cases such as one method of extracting copper and in the purification of copper.</p>
<p>During electrolysis, electrons are being added directly to the metal ions at the cathode (the negative electrode).</p>
<p>The downside (particularly in the aluminium case) is the cost of the electricity. An advantage is that it can produce very pure metals.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-extractintro.pdf" target="_blank">Questions on an introduction to metal extraction</a>
<a href="../questions/a-extractintro.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../extractionmenu.html#top">To the Metal Extraction menu</a>
<a href="../../inorgmenu.html#top">To the Inorganic Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>