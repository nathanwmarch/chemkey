<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Iron Extraction and Purification / Steel | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Extraction of iron and its conversion into steel">
<meta name="keywords" content="iron, ore, haematite, extract, extraction, reduction, reduce, coke, carbon, steel, oxygen, limestone, slag, blast furnace">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Iron Extraction and Purification / Steel</h1>

<div class="text-block">
<p>This page looks at the use of the blast furnace in the extraction of iron from iron ore, and the conversion of the raw iron from the furnace into various kinds of steel.</p>
</div>

<h2>Extracting Iron from Iron Ore Using a Blast Furnace</h2>

<h3>Introduction</h3>

<div class="text-block">
<p>The common ores of iron are both iron oxides, and these can be reduced to iron by heating them with carbon in the form of coke. Coke is produced by heating coal in the absence of air.</p>
<p>Coke is cheap and provides both the reducing agent for the reaction and also the heat source – as you will see below.</p>
</div>

<h3>Iron Ores</h3>

<div class="text-block">
<p>The most commonly used iron ores are haematite (US: hematite), Fe<sub>2</sub>O<sub>3</sub>, and magnetite, Fe<sub>3</sub>O<sub>4</sub>.</p>
</div>

<div class="note">
<p>Note: The two equations for the reduction of the ore on this page are for haematite. In the fairly unlikely event that you need the equations for magnetite, they aren't difficult to work out for yourself.</p>
</div>

<h3>The Blast Furnace</h3>

<div class="image-container"><img src="blastfurnace.gif"></div>

<h4>The heat source</h4>

<div class="text-block">
<p>The air blown into the bottom of the furnace is heated using the hot waste gases from the top. Heat energy is valuable, and it is important not to waste any.</p>
<p>The coke (essentially impure carbon) burns in the blast of hot air to form carbon dioxide – a strongly exothermic reaction. This reaction is the main source of heat in the furnace.</p>
</div>

<div class="block-formula">
\underset{m.p.~3,642\degree{C}}{C_{(s)}} +~O_{2(g)} \longrightarrow CO_{2(g)}
</div>

<h4>The reduction of the ore</h4>

<div class="text-block">
<p>At the high temperature at the bottom of the furnace, carbon dioxide reacts with carbon to produce carbon monoxide.</p>
</div>

<div class="block-formula">
C_{(s)} + CO_{2(g)} \longrightarrow 2CO_{(g)}
</div>

<div class="text-block">
<p>It is the carbon monoxide which is the main reducing agent in the furnace.</p>
</div>

<div class="block-formula">
{\underset{m.p.~1,475{-}1,565\degree{C}}{\text{Fe}_2O_{3(l)}} +~3CO_{(g)}} \longrightarrow{\underset{m.p.~1,538\degree{C}}{2\text{Fe}_{(l)}}} + 3CO_{2(g)}
</div>

<div class="text-block">
<p>In the hotter parts of the furnace, the carbon itself also acts as a reducing agent. Notice that at these temperatures, the other product of the reaction is carbon monoxide, not carbon dioxide.</p>
</div>

<div class="block-formula">
\text{Fe}_2O_{3(l)} + 3C_{(s)} \longrightarrow 2\text{Fe}_{(l)} + 3CO_{(g)}
</div>

<div class="text-block">
<p>The temperature of the furnace is hot enough to melt the iron which trickles down to the bottom where it can be tapped off.</p>
</div>

<h4>The function of the limestone</h4>

<div class="text-block">
<p>Iron ore isn't pure iron oxide – it also contains an assortment of rocky material. This wouldn't melt at the temperature of the furnace, and would eventually clog it up. The limestone is added to convert this into slag which melts and runs to the bottom.</p>
<p>The heat of the furnace decomposes the limestone to give calcium oxide.</p>
</div>

<div class="block-formula">
{\underset{m.p.~825\degree{C}}{\text{Ca}CO_{3(l)}}} \xrightarrow{\Delta} {\underset{m.p.~2,572\degree{C}}{\text{Ca}O_{(s)}}} + CO_{2(g)}
</div>

<div class="text-block">
<p>This is an endothermic reaction, absorbing heat from the furnace. It is therefore important not to add too much limestone because it would otherwise cool the furnace.</p>
<p>Calcium oxide is a basic oxide and reacts with acidic oxides such as silicon dioxide present in the rock. Calcium oxide reacts with silicon dioxide to give calcium silicate.</p>
</div>

<div class="block-formula">
\text{Ca}O_{(s)} + {\underset{m.p.~1,710\degree{C}}{\text{Si}O_{2(l)}}} \longrightarrow {\underset{m.p.~1,540\degree{C}}{\text{CaSi}O_{3(l)}}}
</div>

<div class="text-block">
<p>The calcium silicate melts and runs down through the furnace to form a layer on top of the molten iron. It can be tapped off from time to time as slag.</p>
<p>Slag is used in road making and as "slag cement" – a final ground slag which can be used in cement, often mixed with Portland cement.</p>
</div>

<h4>Cast iron</h4>

<div class="text-block">
<p>The molten iron from the bottom of the furnace can be used as cast iron.</p>
<p>Cast iron is very runny when it is molten and doesn't shrink much when it solidifies. It is therefore ideal for making castings – hence its name. However, it is very impure, containing about 4% by weight of carbon. This carbon makes it very hard, but also very brittle. If you hit it hard, it tends to shatter rather than bend or dent.</p>
<p>Cast iron is used for things like manhole covers, cast iron pipes, valves and pump bodies in the water industry, guttering and drainpipes, cylinder blocks in car engines, Aga-type cookers, and very expensive and very heavy cookware.</p>
<p>At the time of writing (2015), world production of iron castings was about 75 million tonnes per year.</p>
</div>

<h2>Steel</h2>

<div class="text-block">
<p>Most of the molten iron from a Blast Furnace is used to make one of a number of types of steel. There isn't just one substance called steel – they are a family of alloys of iron with carbon or various metals. More about this later</p>
</div>

<h3>Steel-making: The Basic Oxygen Process</h3>

<div class="text-block">
<p>Impurities in the iron from the blast furnace include carbon, sulfur, phosphorus and silicon. These have to be removed.</p>
</div>

<h4>Removal of sulfur</h4>

<div class="text-block">
<p>Sulphur has to be removed first in a separate process. Magnesium powder is blown through the molten iron and the sulfur reacts with it to form magnesium sulfide. This forms a slag on top of the iron and can be removed.</p>
</div>

<div class="block-formula">
{\underset{m.p.~650\degree{C}}{\text{Mg}_{(l)}}} + S_{(iron)} \longrightarrow {\underset{m.p.~2,000\degree{C}}{\text{Mg}S_{(s)}}}
</div>

<h4>Removal of carbon etc.</h4>

<div class="text-block">
<p>The still impure molten iron is mixed with scrap iron (from recycling) and oxygen is blown on to the mixture. The oxygen reacts with the remaining impurities to form various oxides.</p>
<p>The carbon forms carbon monoxide. Since this is a gas it removes itself from the iron! This carbon monoxide can be cleaned and used as a fuel gas.</p>
<p>Elements like phosphorus and silicon react with the oxygen to form acidic oxides. These are removed using quicklime (calcium oxide) which is added to the furnace during the oxygen blow. They react to form compounds such as calcium silicate or calcium phosphate which form a slag on top of the iron.</p>
</div>

<h3>Types of Iron and Steel</h3>

<div class="text-block">
<p>Cast iron has already been mentioned above. This section deals with the types of iron and steel which are produced as a result of the steel-making process.</p>
</div>

<h4>Wrought iron</h4>

<div class="text-block">
<p>If all the carbon is removed from the iron to give high purity iron, it is known as <i>wrought iron</i>. Wrought iron is quite soft and easily worked and has little structural strength. It was once used to make decorative gates and railings, but these days mild steel is normally used instead.</p>
</div>

<h4>Mild steel</h4>

<div class="text-block">
<p>Mild steel is iron containing up to about 0.25% of carbon. The presence of the carbon makes the steel stronger and harder than pure iron. The higher the percentage of carbon, the harder the steel becomes.</p>
<p>Mild steel is used for lots of things – nails, wire, car bodies, ship building, girders and bridges amongst others.</p>
</div>

<h4>High carbon steel</h4>

<div class="text-block">
<p>High carbon steel contains up to about 1.5% of carbon. The presence of the extra carbon makes it very hard, but it also makes it more brittle. High carbon steel is used for cutting tools and masonry nails (nails designed to be driven into concrete blocks or brickwork without bending). You have to be careful with high carbon steel because it tends to fracture rather than bend if you mistreat it.</p>
</div>

<h4>Special steels</h4>

<div class="text-block">
<p>These are iron alloyed with other metals. For example:</p>
</div>

<table class="data-table">
<tbody><tr><th></th><th>iron mixed with</th><th>special properties</th><th>uses include</th></tr>
<tr><td>stainless steel</td><td>chromium and nickel</td><td>resists corrosion</td><td>cutlery, cooking utensils, kitchen sinks, industrial equipment for food and drink processing</td></tr>
<tr><td>titanium steel</td><td>titanium</td><td>withstands high temperatures</td><td>gas turbines, spacecraft</td></tr>
<tr><td>manganese steel</td><td>manganese</td><td>very hard</td><td>rock-breaking machinery, some railway track (e.g. points), military helmets</td></tr>
</tbody></table>

<h2>Some Environmental Considerations</h2>

<div class="text-block">
<p>This section is designed to give you a brief idea of the sort of environmental issues involved with the extraction of iron and its conversion to steel. I wouldn't claim that it covers everything!</p>
</div>

<div class="note">
<p>Note: This is deliberately brief because a lot of it is just common sense, and you will probably already have met it in detail in earlier chemistry courses, in geography, in general studies, or wherever.</p>
<p>If you aren't sure about the various environmental problems like acid rain, global warming and the like, the very best site to find out about them is the <a href="http://www.epa.gov/">US Environmental Protection Agency</a>.</p>
</div>

<h3>Environmental Problems in Mining and Transporting the Raw Materials</h3>

<div class="text-block">
<p>Think about:</p>
</div>

<ul>
<li>Loss of landscape due to mining, processing and transporting the iron ore, coke and limestone.</li>
<li>Noise and air pollution (greenhouse effect, acid rain) involved in these operations.</li>
</ul>

<h3>Extracting Iron From the Ore</h3>

<div class="text-block">
<p>Think about:</p>
</div>

<ul>
<li>Loss of landscape due to the size of the chemical plant needed.</li>
<li>Noise.</li>
<li>Atmospheric pollution from the various stages of extraction. For example: carbon dioxide (greenhouse effect); carbon monoxide (poisonous); sulfur dioxide from the sulfur content of the ores (poisonous, acid rain).</li>
<li>Disposal of slag, some of which is just dumped.</li>
<li>Transport of the finished iron.</li>
</ul>

<h3>Recycling</h3>

<div class="text-block">
<p>Think about:</p>
</div>

<ul>
<li>Saving of raw materials and energy by not having to first extract the iron from the ore.</li>
<li>Avoiding the pollution problems in the extraction of iron from the ore.</li>
<li>Not having to find space to dump the unwanted iron if it wasn't recycled.</li>
<li>(Offsetting these to a minor extent) Energy and pollution costs in collecting and transporting the recycled iron to the steel works.</li>
</ul>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-extractiron.pdf" target="_blank">Questions on iron extraction</a>
<a href="../questions/a-extractiron.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../extractionmenu.html#top">To the Metal Extraction menu</a>
<a href="../../inorgmenu.html#top">To the Inorganic Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>