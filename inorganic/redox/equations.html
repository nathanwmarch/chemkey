<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Writing Ionic Equations for Redox Reactions | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how you construct electron half-equations for redox reactions and combine them to give the ionic equation for the reaction.">
<meta name="keywords" content="oxidation, reduction, redox, electron, half equation, half reaction, ionic equation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Writing Ionic Equations for Redox Reactions</h1>

<div class="text-block">
<p>This page explains how to work out electron-half-reactions for oxidation and reduction processes, and then how to combine them to give the overall ionic equation for a redox reaction. This is an important skill in inorganic chemistry.</p>
<p>Don't worry if it seems to take you a long time in the early stages. It is a fairly slow process even with experience. Take your time and practise as much as you can.</p>
</div>

<h2>Electron Half-equations</h2>

<h3>What is an Electron Half-equation?</h3>

<div class="text-block">
<p>When magnesium reduces hot copper(II) oxide to copper, the ionic equation for the reaction is:</p>
</div>

<div class="block-formula">
\text{Cu}^{2+} + \text{Mg} \longrightarrow \text{Cu} + \text{Mg}^{2+}
</div>

<div class="note">
<p>Note: I am going to leave out state symbols in all the equations on this page. This topic is awkward enough anyway without having to worry about state symbols as well as everything else. Practice getting the equations right, and then add the state symbols in afterwards if your examiners are likely to want them.</p>
<p>How do you know whether your examiners will want you to include them? The best way is to look at their mark schemes. You should be able to get these from your examiners' website. There are links on the <a href="../../syllabuses.html">syllabuses</a> page for students studying for UK-based exams.</p>
</div>

<div class="text-block">
<p>You can split the ionic equation into two parts, and look at it from the point of view of the magnesium and of the copper(II) ions separately. This shows clearly that the magnesium has lost two electrons, and the copper(II) ions have gained them.</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{Mg} &\longrightarrow \text{Mg}^{2+} + \color{#467abf}{2\text{e}^-} \\
\\
\text{Cu}^{2+} + \color{#467abf}{2\text{e}^-} &\longrightarrow \text{Cu}
\end{aligned}
</div>

<div class="text-block">
<p>These two equations are described as "electron half-equations" or "half-equations" or "ionic half-equations" or "half-reactions" – lots of variations all meaning exactly the same thing!</p>
<p>Any redox reaction is made up of two half-reactions: in one of them electrons are being lost (an oxidation process) and in the other one those electrons are being gained (a reduction process).</p>
</div>

<div class="note">
<p>Note: If you aren't happy about redox reactions in terms of electron transfer, you MUST read the <a href="definitions.html">introductory page on redox reactions</a> before you go on.</p>
</div>

<h3>Working Out Electron Half-equations and Using Them to Build Ionic Equations</h3>

<div class="text-block">
<p>In the example above, we've got at the electron half-equations by starting from the ionic equation and extracting the individual half-reactions from it. That's doing everything entirely the wrong way round!</p>
<p>In reality, you almost always start from the electron half-equations and use them to build the ionic equation.</p>
</div>

<h4>Example 1: The reaction between chlorine and iron(II) ions</h4>

<div class="text-block">
<p>Chlorine gas oxidises iron(II) ions to iron(III) ions. In the process, the chlorine is reduced to chloride ions.</p>
<p>You would have to know this, or be told it by an examiner. In building equations, there is quite a lot that you can work out as you go along, but you have to have somewhere to start from!</p>
<p>You start by writing down what you know for each of the half-reactions. In the chlorine case, you know that chlorine (as molecules) turns into chloride ions:</p>
</div>

<div class="block-formula" label="figure out the components">
\text{Cl}_2 \longrightarrow \color{#467abf}{\text{Cl}^-}
</div>

<div class="text-block">
<p>The first thing to do is to balance the atoms that you have got as far as you possibly can:</p>
</div>

<div class="block-formula" label="balance the atoms">
\text{Cl}_2 \longrightarrow \color{#467abf}{2}\text{Cl}^-
</div>

<div class="text-block">
<p>ALWAYS check that you have the existing atoms balanced before you do anything else. If you forget to do this, everything else that you do afterwards is a complete waste of time!</p>
<p>Now you have to add things to the half-equation in order to make it balance completely.</p>
<p>All you are allowed to add are:</p>
</div>

<ul>
<li>electrons</li>
<li>water</li>
<li>hydrogen ions (unless the reaction is being done under alkaline conditions – in which case, you can add hydroxide ions instead)</li>
</ul>

<div class="text-block">
<p>In the chlorine case, all that is wrong with the existing equation that we've produced so far is that the charges don't balance. The left-hand side of the equation has no charge, but the right-hand side carries 2 negative charges.</p>
<p>That's easily put right by adding two electrons to the left-hand side. The final version of the half-reaction is:</p>
</div>

<div class="block-formula" label="add electrons to balance charge">
\text{Cl}_2 + \color{#467abf}{2\text{e}^-} \longrightarrow 2\text{Cl}^-
</div>

<div class="text-block">
<p>Now you repeat this for the iron(II) ions. You know (or are told) that they are oxidised to iron(III) ions. Write this down:</p>
</div>

<div class="block-formula" label="recall the components">
\text{Fe}^{2+} \longrightarrow \color{#467abf}{\text{Fe}^{3+}}
</div>

<div class="text-block">
<p>The atoms balance, but the charges don't. There are 3 positive charges on the right-hand side, but only 2 on the left.</p>
<p>You need to reduce the number of positive charges on the right-hand side. That's easily done by adding an electron to that side:</p>
</div>

<div class="block-formula" label="add electrons to balance charge">
\text{Fe}^{2+} \longrightarrow \text{Fe}^{3+} + \color{#467abf}{\text{e}^-}
</div>

<div class="text-block">
<p>Combining the half-reactions to make the ionic equation for the reaction</p>
<p>What we've got at the moment is this:</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{Cl}_2 + \overbrace{\color{#467abf}{2\text{e}^-}}^{\clap{\color{#467abf}{\text{this reaction needs 2 electrons...}}{}}} &\longrightarrow 2\text{Cl}^- \\
\\
\text{Fe}^{2+} &\longrightarrow \text{Fe}^{3+} + \underbrace{\color{#00CC99}{\text{e}^-}}_{\clap{\color{#00CC99}{\text{...but this one only produces 1 electron}}}{}}
\end{aligned}
</div>

<div class="text-block">
<p>It is obvious that the iron reaction will have to happen twice for every chlorine molecule that reacts. Allow for that, and then add the two half-equations together.</p>
</div>

<div class="image-container"><img src="clfeiiionic.gif"></div>

<div class="text-block">
<p>But don't stop there!! Check that everything balances – atoms and charges. It is very easy to make small mistakes, especially if you are trying to multiply and add up more complicated equations.</p>
<p>You will notice that I haven't bothered to include the electrons in the added-up version. If you think about it, there are bound to be the same number on each side of the final equation, and so they will cancel out. If you aren't happy with this, write them down and then cross them out afterwards!</p>
</div>

<h4>Example 2: The reaction between hydrogen peroxide and manganate(VII) ions</h4>

<div class="text-block">
<p>The first example was a simple bit of chemistry which you may well have come across. The technique works just as well for more complicated (and perhaps unfamiliar) chemistry.</p>
<p> Manganate(VII) ions, MnO<sub>4</sub><sup>-</sup>, oxidise hydrogen peroxide, H<sub>2</sub>O<sub>2</sub>, to oxygen gas. The reaction is done with potassium manganate(VII) solution and hydrogen peroxide solution acidified with dilute sulfuric acid.</p>
<p>During the reaction, the manganate(VII) ions are reduced to manganese(II) ions.</p>
<p>Let's start with the hydrogen peroxide half-equation. What we know is:</p>
</div>

<div class="block-formula" label="figure out the product">
\text{H}_2\text{O}_2 \longrightarrow \color{#467abf}{\text{O}_2}
</div>

<div class="text-block">
<p>The oxygen is already balanced. What about the hydrogen?</p>
<p>All you are allowed to add to this equation are water, hydrogen ions and electrons. If you add water to supply the extra hydrogen atoms needed on the right-hand side, you will mess up the oxygens again – that's obviously wrong!</p>
<p>Add two hydrogen ions to the right-hand side.</p>
</div>

<div class="block-formula" label="add hydrogen ions to balance the hydrogen atoms">
\text{H}_2\text{O}_2 \longrightarrow \text{O}_2 + \color{#467abf}{2\text{H}^+}
</div>

<div class="text-block">
<p>Now all you need to do is balance the charges. You would have to add 2 electrons to the right-hand side to make the overall charge on both sides zero.</p>
</div>

<div class="block-formula" label="add electrons to balance charge">
\text{H}_2\text{O}_2 \longrightarrow \text{O}_2 + 2\text{H}^+ + \color{#467abf}{2\text{e}^-}
</div>

<h5>Now for the manganate(VII) half-equation:</h5>

<div class="text-block">
<p>You know (or are told) that the manganate(VII) ions turn into manganese(II) ions. Write that down.</p>
</div>

<div class="block-formula" label="recall the components">
{\text{MnO}_4}^- \longrightarrow \color{#467abf}{\text{Mn}^{2+}}
</div>

<div class="text-block">
<p>The manganese balances, but you need four oxygens on the right-hand side. These can only come from water – that's the only oxygen-containing thing you are allowed to write into one of these equations in acid conditions.</p>
</div>

<div class="block-formula" label="add water to balance the oxygen atoms">
{\text{MnO}_4}^- \longrightarrow \text{Mn}^{2+} + \color{#467abf}{4\text{H}_2\text{O}}
</div>

<div class="text-block">
<p>By doing this, we've introduced some hydrogens. To balance these, you will need 8 hydrogen ions on the left-hand side.</p>
</div>

<div class="block-formula" label="add hydrogen ions to balance the hydrogen atoms">
{\text{MnO}_4}^- + \color{#467abf}{8\text{H}^+} \longrightarrow \text{Mn}^{2+} + 4\text{H}_2\text{O}
</div>

<div class="text-block">
<p>Now that all the atoms are balanced, all you need to do is balance the charges. At the moment there are a net 7+ charges on the left-hand side (1- and 8+), but only 2+ on the right. Add 5 electrons to the left-hand side to reduce the 7+ to 2+.</p>
</div>

<div class="block-formula" label="add electrons to balance the charge">
{\text{MnO}_4}^- + 8\text{H}^+ + \color{#467abf}{5\text{e}^-} \longrightarrow \text{Mn}^{2+} + 4\text{H}_2\text{O}
</div>

<div class="text-block">
<p>This is the typical sort of half-equation which you will have to be able to work out.</p>
</div>

<div class="principle">
<ul>
<li>Balance the atoms apart from oxygen and hydrogen.</li>
<li>Balance the oxygens by adding water molecules.</li>
<li>Balance the hydrogens by adding hydrogen ions.</li>
<li>Balance the charges by adding electrons.</li>
</ul>
</div>

<div class="text-block">
<p>Combining the half-reactions to make the ionic equation for the reaction</p>
<p>The two half-equations we've produced are:</p>
</div>

<div class="block-formula full-width">
\begin{aligned}
{\text{MnO}_4}^- + 8\text{H}^+ + \overbrace{\color{#467abf}{5\text{e}^-}}^{\clap{\color{#467abf}{\text{this reaction needs 5 electrons...}}{}}} &\longrightarrow \text{Mn}^{2+} + 4\text{H}_2\text{O} \\
\\
\text{H}_2\text{O}_2 &\longrightarrow \text{O}_2 + 2\text{H}^+ + \underbrace{\color{#00CC99}{2\text{e}^-}}_{\clap{\color{#00CC99}{\text{...but this one only produces 2 electrons}}}{}}
\end{aligned}
</div>

<div class="text-block">
<p>You have to multiply the equations so that the same number of electrons are involved in both.In this case, everything would work out well if you transferred 10 electrons.</p>
</div>

<div class="image-container"><img src="ex2combine2.gif"></div>

<div class="text-block">
<p>But this time, you haven't quite finished. During the checking of the balancing, you should notice that there are hydrogen ions on both sides of the equation:</p>
</div>

<div class="image-container"><img src="ex2ionic1.gif"></div>

<div class="text-block">
<p>You can simplify this down by subtracting 10 hydrogen ions from both sides to leave the final version of the ionic equation – but don't forget to check the balancing of the atoms and charges!</p>
</div>

<div class="block-formula full-width" label="balanced redox equation for reaction of manganate(VII) ions and hydrogen peroxide">
2{\text{MnO}_4}^- + 6\text{H}^+ + 5\text{H}_2\text{O}_2 \longrightarrow 2\text{Mn}^{2+} + 8\text{H}_2\text{O} + 5\text{O}_2
</div>

<div class="text-block">
<p>You will often find that hydrogen ions or water molecules appear on both sides of the ionic equation in complicated cases built up in this way. Always check, and then simplify where possible.</p>
</div>

<h4>Example 3: The oxidation of ethanol by acidified potassium dichromate(VI)</h4>

<div class="text-block">
<p>This technique can be used just as well in examples involving organic chemicals. Potassium dichromate(VI) solution acidified with dilute sulfuric acid is used to oxidise ethanol, CH<sub>3</sub>CH<sub>2</sub>OH, to ethanoic acid, CH<sub>3</sub>COOH.</p>
<p>The oxidising agent is the dichromate(VI) ion, Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup>. This is reduced to chromium(III) ions, Cr<sup>3+</sup>.</p>
<p>We'll do the ethanol to ethanoic acid half-equation first. Using the same stages as before, start by writing down what you know:</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{OH} \longrightarrow \text{CH}_3\text{COOH}
</div>

<div class="text-block">
<p>Balance the oxygens by adding a water molecule to the left-hand side:</p>
</div>

<div class="block-formula" label="add water to balance the oxygen atoms">
\text{CH}_3\text{CH}_2\text{OH} + \color{#467abf}{\text{H}_2\text{O}} \longrightarrow \text{CH}_3\text{COOH}
</div>

<div class="text-block">
<p>Add hydrogen ions to the right-hand side to balance the hydrogens:</p>
</div>

<div class="block-formula" label="add hydrogen ions to balance the hydrogen atoms">
\text{CH}_3\text{CH}_2\text{OH} + \text{H}_2\text{O} \longrightarrow \text{CH}_3\text{COOH} + \color{#467abf}{4\text{H}^+}
</div>

<div class="text-block">
<p>And finally balance the charges by adding 4 electrons to the right-hand side to give an overall zero charge on each side:</p>
</div>

<div class="block-formula" label="add electrons to balance the charge">
\text{CH}_3\text{CH}_2\text{OH} + \text{H}_2\text{O} \longrightarrow \text{CH}_3\text{COOH} + 4\text{H}^+ + \color{#467abf}{4\text{e}^-}
</div>

<div class="text-block">
<p>The dichromate(VI) half-equation contains a trap which lots of people fall into!</p>
<p>Start by writing down what you know:</p>
</div>

<div class="block-formula" label="recall the components">
{\text{Cr}_2\text{O}_7}^{2-} \longrightarrow \color{#467abf}{\text{Cr}^{3+}}
</div>

<div class="text-block">
<p>What people often forget to do at this stage is to balance the chromiums. If you don't do that, you are doomed to getting the wrong answer at the end of the process! When you come to balance the charges you will have to write in the wrong number of electrons – which means that your multiplying factors will be wrong when you come to add the half-equations. A complete waste of time!</p>
</div>

<div class="block-formula" label="balance the atoms">
{\text{Cr}_2\text{O}_7}^{2-} \longrightarrow \color{#467abf}{2}\text{Cr}^{3+}
</div>

<div class="block-formula" label="add water to balance the oxygen atoms">
{\text{Cr}_2\text{O}_7}^{2-} \longrightarrow 2\text{Cr}^{3+} + \color{#467abf}{7\text{H}_2\text{O}}
</div>

<div class="block-formula" label="add hydrogen ions to balance the hydrogen atoms">
{\text{Cr}_2\text{O}_7}^{2-} + \color{#467abf}{14\text{H}^+} \longrightarrow 2\text{Cr}^{3+} + 7\text{H}_2\text{O}
</div>

<div class="text-block">
<p>Now all that needs balancing is the charges. Add 6 electrons to the left-hand side to give a net 6+ on each side.</p>
</div>

<div class="block-formula" label="add electrons to balance the charge">
{\text{Cr}_2\text{O}_7}^{2-} + 14\text{H}^+ + \color{#467abf}{6\text{e}^-} \longrightarrow 2\text{Cr}^{3+} + 7\text{H}_2\text{O}
</div>

<div class="text-block">
<p>Combining the half-reactions to make the ionic equation for the reaction</p>
<p>What we have so far is:</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{CH}_3\text{CH}_2\text{OH} + \text{H}_2\text{O} &\longrightarrow \text{CH}_3\text{COOH} + 4\text{H}^+ + 4\text{e}^- \\
\\
{\text{Cr}_2\text{O}_7}^{2-} + 14\text{H}^+ + 6\text{e}^- &\longrightarrow 2\text{Cr}^{3+} + 7\text{H}_2\text{O}
\end{aligned}
</div>

<div class="text-block">
<p>What are the multiplying factors for the equations this time? The simplest way of working this out is to find the smallest number of electrons which both 4 and 6 will divide into – in this case, 12. That means that you can multiply one equation by 3 and the other by 2.</p>
</div>

<div class="note">
<p>Note: Don't worry too much if you get this wrong and choose to transfer 24 electrons instead. All that will happen is that your final equation will end up with everything multiplied by 2. Your examiners might well allow that.</p>
</div>

<div class="text-block">
<p>The multiplication and addition looks like this:</p>
</div>

<div class="image-container"><img src="ex3combine2.gif"></div>

<div class="text-block">
<p>Now you will find that there are water molecules and hydrogen ions occurring on both sides of the ionic equation. You can simplify this to give the final equation:</p>
</div>

<div class="block-formula full-width" label="balanced redox equation for the reaction of ethanol with dichromate(VI) ions">
3\text{CH}_3\text{CH}_2\text{OH} + 2{\text{Cr}_2\text{O}_7}^{2-} + 16\text{H}^+ \longrightarrow 3\text{CH}_3\text{COOH} + 4\text{Cr}^{3+} + 11\text{H}_2\text{O}
</div>

<div class="note">
<p>Note: You have now seen a cross-section of the sort of equations which you could be asked to work out. Now you need to practice so that you can do this reasonably quickly and very accurately! Aim to get an averagely complicated example done in about 3 minutes.</p>
<p>If you want a few more examples, and the opportunity to practice with answers available, you might be interested in looking in chapter 1 of my book on <a href="../../book.html">Chemistry Calculations</a>.</p>
</div>

<h3>Reactions Done Under Alkaline Conditions</h3>

<div class="text-block">
<p>Working out half-equations for reactions in alkaline solution is decidedly more tricky than those above. You are less likely to be asked to do this at this level (UK A-level and its equivalents), and for that reason I've covered these on a separate page (link below). It would be worthwhile checking your syllabus and past papers before you start worrying about these! </p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-redoxequations.pdf" target="_blank">Questions on redox equations</a>
<a href="../questions/a-redoxequations.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="equations2.html#top">How to work out half-equations for reactions under alkaline conditions</a>
<a href="../redoxmenu.html#top">To the Redox menu</a>
<a href="../../inorgmenu.html#top">To the Inorganic Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>