<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Writing Ionic Equations for Redox Reactions Done Under Alkaline Conditions | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how you construct electron half-equations for redox reactions done under alkaline conditions and combine them to give the ionic equation for the reaction.">
<meta name="keywords" content="oxidation, reduction, redox, electron, half equation, half reaction, ionic equation, alkaline, alkali">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Writing Ionic Equations for Redox Reactions Done Under Alkaline Conditions</h1>

<div class="text-block">
<p>This page explains how to work out electron-half-reactions for oxidation and reduction processes which are carried out under alkaline conditions, and then how to combine them to give the overall ionic equation for the redox reaction. Combining them is easy; working them out may be more difficult than under acidic conditions.</p>
</div>

<div class="note">
<p>Warning! If you have come to this page directly via a search engine you should be aware that it follows-on from an <a href="equations.html">introductory page on this technique</a> . You would find it much easier to understand the current page if you read that page first.</p>
</div>

<h3>Why is it More Difficult to Write Electron Half-equations for These Reactions?</h3>

<h4>What you already know</h4>

<div class="text-block">
<p>When you are trying to balance electron half-equations, you are only allowed to add:</p>
</div>

<ul>
<li>electrons</li>
<li>water</li>
<li>hydrogen ions (unless the reaction is being done under alkaline conditions – in which case, you can add hydroxide ions instead)
</li>
</ul>

<div class="text-block">
<p>If you are working under acidic or neutral conditions, the sequence is usually:</p>
</div>

<ul>
<li>Balance the atoms apart from oxygen and hydrogen.</li>
<li>Balance the oxygens by adding water molecules.</li>
<li>Balance the hydrogens by adding hydrogen ions.</li>
<li>Balance the charges by adding electrons.</li>
</ul>

<div class="text-block">
<p>The whole process is fairly automatic, and provided you take care, there isn't much to go wrong.</p>
</div>

<h4>How is this different under alkaline conditions?</h4>

<div class="text-block">
<p>The problem is that the water and the hydroxide ions that you add to balance the equations under alkaline conditions contain both hydrogen and oxygen.</p>
<p>To balance the oxygens, you could in principle add either H<sub>2</sub>O or OH<sup>-</sup> to the equation. The same thing is true for balancing the hydrogens. How do you know what to start with?</p>
</div>

<h3>How to Tackle the Problem</h3>

<div class="text-block">
<p>In some cases, it is obvious how to build up the half-equation using hydroxide ions. Always check this before you get involved in anything more difficult. You will see what I mean shortly.</p>
<p>If it isn't immediately obvious, work out the electron-half equation as if it were being done under acidic conditions just as you have learnt to do on the previous page – in other words by writing in water molecules, hydrogen ions and electrons.</p>
<p>Once you have got a balanced half-equation, you then convert it to alkaline conditions. You will see how to do that in the following examples.</p>
</div>

<h3>Four Examples</h3>

<div class="text-block">
<p>Don't worry if the chemistry in these examples is unfamiliar to you. It doesn't matter in the slightest. All that matters is how you work out the equations.</p>
</div>

<h4>The oxidation of cobalt(II) to cobalt(III) by hydrogen peroxide</h4>

<div class="text-block">
<p>If you add an excess of ammonia solution to a solution containing cobalt(II) ions, you get a complex ion formed called the hexaamminecobalt(II) ion, Co(NH<sub>3</sub>)<sub>6</sub><sup>2+</sup>. This is oxidised rapidly by hydrogen peroxide solution to the hexaamminecobalt(III) ion, Co(NH<sub>3</sub>)<sub>6</sub><sup>3+</sup>.</p>
<p>Ammonia solution is, of course, alkaline.</p>
<p>The half-equation for the cobalt reaction is easy. Start by writing down what you know (or are told):</p>
</div>

<div class="block-formula" label="recall the components">
{\text{Co}(\text{NH}_3)_6}^{2+} \longrightarrow {\text{Co}(\text{NH}_3)_6}^{3+}
</div>

<div class="text-block">
<p>Everything balances apart from the charges. Add an electron to the right-hand side to give both sides an overall charge of 2+.</p>
</div>

<div class="block-formula" label="add electrons to balance the charge">
{\text{Co}(\text{NH}_3)_6}^{2+} \longrightarrow {\text{Co}(\text{NH}_3)_6}^{3+} + \color{#467abf}{\text{e}^-}
</div>

<div class="text-block">
<p>The hydrogen peroxide half-equation isn't very difficult either, except that you aren't told what is formed and so have to make a guess. It would balance very nicely if you ended up with 2 hydroxide ions on the right-hand side.</p>
<p>This is a good example of a case where it is fairly obvious where to put hydroxide ions.</p>
</div>

<div class="block-formula" label="figure out the components">
\text{H}_2\text{O}_2 \longrightarrow 2\text{OH}^-
</div>

<div class="text-block">
<p>You would then just have to add 2 electrons to the left-hand side to balance the charges.</p>
</div>

<div class="block-formula" label="add electrons to balance the charge">
\text{H}_2\text{O}_2 + \color{#467abf}{2\text{e}^-} \longrightarrow 2\text{OH}^-
</div>

<div class="text-block">
<p>Combining the half-reactions to make the ionic equation for the reaction</p>
<p>What we have so far is:</p>
</div>

<div class="block-formula">
\begin{aligned}
{\text{Co}(\text{NH}_3)_6}^{2+} &\longrightarrow {\text{Co}(\text{NH}_3)_6}^{3+} + \text{e}^- \\
\\
\text{H}_2\text{O}_2 + 2\text{e}^- &\longrightarrow 2\text{OH}^-
\end{aligned}
</div>

<div class="text-block">
<p>The multiplication and addition looks like this:</p>
</div>

<div class="image-container"><img src="ex4combine2.gif"></div>

<div class="text-block">
<p>And that's it – an easy example!</p>
</div>

<h4>The oxidation of iron(II) hydroxide by the air</h4>

<div class="text-block">
<p>If you add sodium hydroxide solution to a solution of an iron(II) compound you get a green precipitate of iron(II) hydroxide. This is quite quickly oxidised by oxygen in the air to an orange-brown precipitate of iron(III) hydroxide.</p>
<p>The half-equation for the iron(II) hydroxide is straightforward. Start with what you know:</p>
</div>

<div class="block-formula" label="recall the components">
\text{Fe}(\text{OH})_2 \longrightarrow \text{Fe}(\text{OH})_3
</div>

<div class="text-block">
<p>You obviously need another hydroxide ion on the left-hand side. This is even more straightforward than the previous example.</p>
</div>

<div class="block-formula" label="add hydroxide ions to balance oxygen and hydrogen atoms">
\text{Fe}(\text{OH})_2 + \color{#467abf}{{}^-OH} \longrightarrow \text{Fe}(\text{OH})_3
</div>

<div class="text-block">
<p>To balance the charges, add an electron to the right-hand side.</p>
</div>

<div class="block-formula" label="add electrons to balance the charge">
\text{Fe}(\text{OH})_2 + {}^-OH \longrightarrow \text{Fe}(\text{OH})_3 + \color{#467abf}{\text{e}^-}
</div>

<div class="text-block">
<p>The half-equation for the oxygen isn't so easy. You don't know what is being formed.</p>
</div>

<div class="block-formula">
\text{O}_2 \longrightarrow~\color{#467abf}{?}
</div>

<div class="text-block">
<p>It isn't at all obvious whether you need to balance the oxygens by adding water molecules or hydroxide ions to the right-hand side. OK – treat it as if it were being done under acidic conditions, and the problem disappears!</p>
<p>In this case, you can only balance the oxygens by adding water molecules to the right-hand side.</p>
</div>

<div class="block-formula" label="add water molecules to balance the oxygen atoms">
\text{O}_2 \longrightarrow \color{#467abf}{2\text{H}_2\text{O}}
</div>

<div class="text-block">
<p>Balance the hydrogens by adding hydrogen ions to the left-hand side.</p>
</div>

<div class="block-formula" label="add hydrogen ions to balance the hydrogen atoms">
\text{O}_2 + \color{#467abf}{4\text{H}^+} \longrightarrow 2\text{H}_2\text{O}
</div>

<div class="text-block">
<p>And then balance the charges by adding 4 electrons:</p>
</div>

<div class="block-formula" label="add electrons to balance the change">
\text{O}_2 + 4\text{H}^+ + \color{#467abf}{4\text{e}^-} \longrightarrow 2\text{H}_2\text{O}
</div>

<div class="text-block">
<p>Now you have got a perfectly balanced half-equation. The problem is, of course, that it only applies under acidic conditions. We should have alkaline conditions – with hydroxide ions present not hydrogen ions.</p>
<p>So get rid of the hydrogen ions! Add enough hydroxide ions to both sides of the equation so that you can neutralise all the hydrogen ions. Because it is now a balanced equation, you must add the same number to both sides to maintain the balance.</p>
</div>

<div class="block-formula" label="add hydroxide ions to both sides">
\text{O}_2 + 4\text{H}^+ + \color{#467abf}{4\text{OH}^-} + 4\text{e}^- \longrightarrow 2\text{H}_2\text{O} + \color{#467abf}{4\text{OH}^-}
</div>

<div class="text-block">
<p>The hydrogen ions and hydroxide ions on the left-hand side would turn into 4 water molecules:</p>
</div>

<div class="block-formula" label="convert hydrogen ions and hydroxide ions to water">
\text{O}_2 + \color{#467abf}{4\text{H}_2\text{O}} + 4\text{e}^- \longrightarrow 2\text{H}_2\text{O} + 4\text{OH}^-
</div>

<div class="text-block">
<p>Finally, there are water molecules on both sides of the equation. Cancel out any which aren't changed.</p>
</div>

<div class="block-formula" label="remove surplus water molecules">
\text{O}_2 + 2\text{H}_2\text{O} + 4\text{e}^- \longrightarrow 4\text{OH}^-
</div>

<div class="text-block">
<p>This has all been a bit tedious – although you haven't actually had to think very much! Don't forget to re-check that everything balances now that you have finished.</p>
<p>Combining the half-reactions to make the ionic equation for the reaction</p>
<p>From here on it's all back to the usual routine. We've worked out the two half-equations:</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{Fe}(\text{OH})_2 + {}^-OH &\longrightarrow \text{Fe}(\text{OH})_3 + \text{e}^- \\
\\
\text{O}_2 + 2\text{H}_2\text{O} + 4\text{e}^- &\longrightarrow 4\text{OH}^-
\end{aligned}
</div>

<div class="text-block">
<p>The iron equation will have to happen 4 times to supply enough electrons to the oxygen.</p>
</div>

<div class="image-container"><img src="ex5combine2.gif"></div>

<div class="text-block">
<p>Notice that the hydroxide ions on each side cancel out. (Perhaps to your surprise – certainly to mine when I worked this out!)</p>
</div>

<div class="block-formula" label="balanced redox equation for reaction of iron(II) hydroxide and oxygen">
4\text{Fe}(\text{OH})_2 + \text{O}_2 + 2\text{H}_2\text{O} \longrightarrow 4\text{Fe}(\text{OH})_3 + \text{e}^-
</div>

<h4>The reduction of manganate(VII) ions to manganate(VI) ions by hydroxide ions</h4>

<div class="text-block">
<p>This is a fairly obscure reaction but it is not too difficult to work out and balance. It is unusual in that hydroxide ions are acting as reducing agents.</p>
<p>Dark purple potassium manganate(VII) solution is slowly reduced to dark green potassium manganate(VI) solution by concentrated potassium hydroxide solution. Bubbles of oxygen gas are also given off.</p>
</div>

<div class="note">
<p>Note: Unless the potassium manganate(VII) solution is very dilute, its strong purple colour tends to mask the green of the potassium manganate(VI) in the short term.</p>
</div>

<div class="text-block">
<p>The half-equation for the conversion of manganate(VII) ions to manganate(VI) ions is easy (provided, of course, that you know their formulae!).</p>
</div>

<div class="block-formula">
{\text{MnO}_4}^- + \text{e}^- \longrightarrow {\text{MnO}_4}^{2-}
</div>

<div class="text-block">
<p>So what is happening to the hydroxide ions to turn them into oxygen gas?</p>
<p>It isn't actually very difficult to work out the half-equation directly, but suppose you want to avoid thinking and go through a standard routine:</p>
<p>Write down what you know, balancing the oxygens in the process:</p>
</div>

<div class="block-formula" label="recall the components">
2\text{OH}^- \longrightarrow \text{O}_2
</div>

<div class="text-block">
<p>Balance the hydrogens by adding hydrogen ions:</p>
</div>

<div class="block-formula" label="add hydrogen ions to balance the hydrogen atoms">
2\text{OH}^- \longrightarrow \color{#467abf}{2\text{H}^+} + \text{O}_2
</div>

<div class="text-block">
<p>and now balance the charges:</p>
</div>

<div class="block-formula" label="add electrons to balance the charge">
2\text{OH}^- \longrightarrow 2\text{H}^+ + \text{O}_2 + \color{#467abf}{4\text{e}^-}
</div>

<div class="text-block">
<p>Now get rid of the hydrogen ions by adding enough hydroxide ions to both sides of the equation:</p>
</div>

<div class="block-formula" label="add hydroxide ions to both sides">
2\text{OH}^- + \color{#467abf}{2\text{OH}^-} \longrightarrow 2\text{H}^+ + \color{#467abf}{2\text{OH}^-} + \text{O}_2 + 4\text{e}^-
</div>

<div class="text-block">
<p>and tidy it all up!</p>
</div>

<div class="block-formula" label="convert hydrogen ions and hydroxide ions to water">
4\text{OH}^- \longrightarrow \color{#467abf}{2\text{H}_2\text{O}} + \text{O}_2 + 4\text{e}^-
</div>

<div class="text-block">
<p>Combining the half-reactions to make the ionic equation for the reaction</p>
<p>What we have so far is:</p>
</div>

<div class="block-formula">
\begin{aligned}
{\text{MnO}_4}^- + \text{e}^- &\longrightarrow {\text{MnO}_4}^{2-} \\
\\
4\text{OH}^- &\longrightarrow 2\text{H}_2\text{O} + \text{O}_2 + 4\text{e}^-
\end{aligned}
</div>

<div class="text-block">
<p>The manganese reaction will have to happen four times in order to use up the four electrons produced by the hydroxide equation. Putting this together, you get:</p>
</div>

<div class="image-container"><img src="mn7plusoh.gif"></div>

<div class="text-block">
<p>The chemistry may be unfamiliar, but working out the equation isn't too hard!</p>
<h4>The oxidation of chromium(III) to chromium(VI)</h4>
<p>If you add an excess of sodium hydroxide solution to a solution containing chromium(III) ions, you get a dark green solution containing the complex ion hexahydroxochromate(III), Cr(OH)<sub>6</sub><sup>3-</sup>.</p>
<p>This can be oxidised to bright yellow chromate(VI) ions, CrO<sub>4</sub><sup>2-</sup> by warming it with hydrogen peroxide solution.</p>
<p>We've already worked out the hydrogen peroxide half-equation where it is acting as an oxidising agent under alkaline conditions:</p>
</div>

<div class="block-formula">
\text{H}_2\text{O}_2 + 2\text{e}^- \longrightarrow 2\text{OH}^-
</div>

<div class="text-block">
<p>So this time we just need to put together the half-equation for the chromium ions. What we know is:</p>
</div>

<div class="block-formula">
{\text{Cr}(\text{OH})_6}^{3-} \longrightarrow {\text{CrO}_4}^{2-}
</div>

<div class="text-block">
<p>It isn't the least bit obvious where to put hydroxide ions or water molecules, so treat it as if it were being done under acidic conditions. That way, you start by balancing the oxygens by adding water molecules.</p>
<p>To get 6 oxygens on each side, you need two waters on the right-hand side:</p>
</div>

<div class="block-formula" label="add water molecules to balance the oxygen atoms">
{\text{Cr}(\text{OH})_6}^{3-} \longrightarrow {\text{CrO}_4}^{2-} + \color{#467abf}{2\text{H}_2\text{O}}
</div>

<div class="text-block">
<p>Now balance the hydrogens by adding hydrogen ions:</p>
</div>

<div class="block-formula" label="add hydrogen ions to balance the hydrogen atoms">
{\text{Cr}(\text{OH})_6}^{3-} \longrightarrow {\text{CrO}_4}^{2-} + 2\text{H}_2\text{O} + \color{#467abf}{2\text{H}^+}
</div>

<div class="text-block">
<p>and then balance the charges by adding electrons:</p>
</div>

<div class="block-formula" label="add electrons to balance the charge">
{\text{Cr}(\text{OH})_6}^{3-} \longrightarrow {\text{CrO}_4}^{2-} + 2\text{H}_2\text{O} + 2\text{H}^+ + \color{#467abf}{3\text{e}^-}
</div>

<div class="text-block">
<p>Finally, convert from acidic to alkaline conditions by adding enough hydroxide ions to both sides to turn the hydrogen ions into water:</p>
</div>

<div class="block-formula" label="add hydroxide ions to both sides">
{\text{Cr}(\text{OH})_6}^{3-} + \color{#467abf}{2\text{OH}^-} \longrightarrow {\text{CrO}_4}^{2-} + 2\text{H}_2\text{O} + 2\text{H}^+ + \color{#467abf}{2\text{OH}^-} + 3\text{e}^-
</div>

<div class="text-block">
<p>and tidy it all up:</p>
</div>

<div class="block-formula">
{\text{Cr}(\text{OH})_6}^{3-} + 2\text{OH}^- \longrightarrow {\text{CrO}_4}^{2-} + 4\text{H}_2\text{O} + 3\text{e}^-
</div>

<div class="text-block">
<p>Combining the half-reactions to make the ionic equation for the reaction</p>
<p>The two half-equations are:</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{H}_2\text{O}_2 + 2\text{e}^- &\longrightarrow 2\text{OH}^- \\
\\
{\text{Cr}(\text{OH})_6}^{3-} + 2\text{OH}^- &\longrightarrow {\text{CrO}_4}^{2-} + 4\text{H}_2\text{O} + 3\text{e}^-
\end{aligned}
</div>

<div class="text-block">
<p>If you multiply one equation by 3 and the other by 2, that transfers a total of 6 electrons.</p>
</div>

<div class="image-container"><img src="ex6combine2.gif"></div>

<div class="text-block">
<p>Finally, tidy up the hydroxide ions that occur on both sides to leave the overall ionic equation:</p>
</div>

<div class="block-formula" label="balanced redox equation for the reaction of hydrogen peroxide and hexahydroxochromate(III)">
3\text{H}_2\text{O}_2 + 2{\text{Cr}(\text{OH})_6}^{3-} \longrightarrow 2\text{OH}^- + 2{\text{CrO}_4}^{2-} + 8\text{H}_2\text{O}
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-redoxequations2.pdf" target="_blank">Questions on redox equations under alkaline conditions</a>
<a href="../questions/a-redoxequations2.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="equations.html#top">Return to the first page on electron half-equations</a>
<a href="../redoxmenu.html#top">To the Redox menu</a>
<a href="../../inorgmenu.html#top">To the Inorganic Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>