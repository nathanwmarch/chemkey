<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Oxidation States (Oxidation Numbers) | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains what oxidation states (oxidation numbers) are, and how to calculate them and make use of them.">
<meta name="keywords" content="oxidation, reduction, redox, oxidation state, oxidation number, disproportionation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Oxidation States (Oxidation Numbers)</h1>

<div class="text-block">
<p>This page explains what oxidation states (oxidation numbers) are and how to calculate them and make use of them.</p>
<p>Oxidation states are straightforward to work out and to use, but it is quite difficult to define what they are in any quick way.</p>
</div>

<h3>Explaining What Oxidation States (Oxidation Numbers) Are</h3>

<div class="text-block">
<p>Oxidation states simplify the whole process of working out what is being oxidised and what is being reduced in redox reactions. However, for the purposes of this introduction, it would be helpful if you knew about:</p>
</div>

<ul>
<li>oxidation and reduction in terms of electron transfer</li>
<li>electron-half-equations</li>
</ul>

<div class="note">
<p>Note: If you aren't sure about either of these things, you might want to look at the pages on <a href="definitions.html#top">redox definitions</a> and <a href="equations.html#top">electron-half-equations</a>. It would probably be best to read on and come back to these links if you feel you need to.</p>
</div>

<div class="text-block">
<p>We are going to look at some examples from vanadium chemistry. If you don't know anything about vanadium, it doesn't matter in the slightest.</p>
<p>Vanadium forms a number of different ions – for example, V<sup>2+</sup> and V<sup>3+</sup>. If you think about how these might be produced from vanadium metal, the 2+ ion will be formed by oxidising the metal by removing two electrons:</p>
</div>

<div class="block-formula">
\text{V} \longrightarrow \text{V}^{2+} + 2{e}^-
</div>

<div class="text-block">
<p>The vanadium is now said to be in an oxidation state of +2.</p>
<p>Removal of another electron gives the V<sup>3+</sup> ion:</p>
</div>

<div class="block-formula">
\text{V}^{2+} \longrightarrow \text{V}^{3+} + \text{e}^-
</div>

<div class="text-block">
<p>The vanadium now has an oxidation state of +3.</p>
<p>Removal of another electron gives a more unusual looking ion, VO<sup>2+</sup>.</p>
</div>

<div class="block-formula">
\text{V}^{3+} + \text{H}_2\text{O} \longrightarrow \text{VO}^{2+} + 2\text{H}^+ + \text{e}^-
</div>

<div class="text-block">
<p>The vanadium is now in an oxidation state of +4. Notice that the oxidation state isn't simply counting the charge on the ion (that was true for the first two cases but not for this one).</p>
<p>The positive oxidation state is counting the total number of electrons which have had to be removed – starting from the element.</p>
<p>It is also possible to remove a fifth electron to give another ion (easily confused with the one before!). The oxidation state of the vanadium is now +5.</p>
</div>

<div class="block-formula">
\text{VO}^{2+} + \text{H}_2\text{O} \longrightarrow {\text{VO}_2}^+ + 2\text{H}^+ + \text{e}^-
</div>

<div class="text-block">
<p>Every time you oxidise the vanadium by removing another electron from it, its oxidation state increases by 1.</p>
<p>Fairly obviously, if you start adding electrons again the oxidation state will fall. You could eventually get back to the element vanadium which would have an oxidation state of zero.</p>
<p>What if you kept on adding electrons to the element? You can't actually do that with vanadium, but you can with an element like sulfur.</p>
</div>

<div class="block-formula">
\text{S} + 2\text{e}^- \longrightarrow \text{S}^{2-}
</div>

<div class="text-block">
<p>The sulfur has an oxidation state of -2.</p>
</div>

<div class="summary">
<p>Oxidation state shows the total number of electrons which have been removed from an element (a positive oxidation state) or added to an element (a negative oxidation state) to get to its present state.</p>
<ul>
<li>Oxidation involves an increase in oxidation state</li>
<li>Reduction involves a decrease in oxidation state</li>
</ul>
</div>

<div class="text-block">
<p>Recognising this simple pattern is the single most important thing about the concept of oxidation states. If you know how the oxidation state of an element changes during a reaction, you can instantly tell whether it is being oxidised or reduced without having to work in terms of electron-half-equations and electron transfers.</p>
</div>

<h3>Working Out Oxidation States</h3>

<div class="text-block">
<p>You don't work out oxidation states by counting the numbers of electrons transferred. It would take far too long. Instead you learn some simple rules, and do some very simple sums!</p>
</div>

<ul>
<li>The oxidation state of an uncombined element is zero. That's obviously so, because it hasn't been either oxidised or reduced yet! This applies whatever the structure of the element – whether it is, for example, Xe or Cl<sub>2</sub> or S<sub>8</sub>, or whether it has a giant structure like carbon or silicon.</li>
<li>The sum of the oxidation states of all the atoms or ions in a neutral compound is zero.</li>
<li>The sum of the oxidation states of all the atoms in an ion is equal to the charge on the ion.</li>
<li>The more electronegative element in a substance is given a negative oxidation state. The less electronegative one is given a positive oxidation state. Remember that fluorine is the most electronegative element with oxygen second.</li>
<li>Some elements almost always have the same oxidation states in their compounds:</li>
</ul>

<table class="data-table">
<thead>
<tr><th>element</th><th>usual oxidation state</th><th>exceptions</th></tr>
</thead>
<tbody>
<tr><td>Group 1 metals</td><td>always +1</td><td></td></tr>
<tr><td>Group 2 metals</td><td>always +2</td><td></td></tr>
<tr><td>Oxygen</td><td>usually -2</td><td>except in peroxides and F<sub>2</sub>O (see below)</td></tr>
<tr><td>Hydrogen</td><td>usually +1</td><td>except in metal hydrides where it is -1 (see below)</td></tr>
<tr><td>Fluorine</td><td>always -1</td><td></td></tr>
<tr><td>Chlorine</td><td>usually -1</td><td>except in compounds with O or F (see below)</td></tr>
</tbody></table>

<h4>The reasons for the exceptions</h4>

<h5>Hydrogen in the metal hydrides</h5>

<div class="text-block">
<p>Metal hydrides include compounds like sodium hydride, NaH. In this, the hydrogen is present as a hydride ion, H<sup>-</sup>. The oxidation state of a simple ion like hydride is equal to the charge on the ion – in this case, -1.</p>
<p>Alternatively, you can think of it that the sum of the oxidation states in a neutral compound is zero. Since Group 1 metals always have an oxidation state of +1 in their compounds, it follows that the hydrogen must have an oxidation state of -1 (+1 -1 = 0).</p>
</div>

<h5>Oxygen in peroxides</h5>

<div class="text-block">
<p>Peroxides include hydrogen peroxide, H<sub>2</sub>O<sub>2</sub>. This is an electrically neutral compound and so the sum of the oxidation states of the hydrogen and oxygen must be zero.</p>
<p>Since each hydrogen has an oxidation state of +1, each oxygen must have an oxidation state of -1 to balance it.</p>
</div>

<h5>Oxygen in F<sub>2</sub>O</h5>

<div class="text-block">
<p>The problem here is that oxygen isn't the most electronegative element. 
The fluorine is more electronegative and has an oxidation state of -1. In this case, the oxygen has an oxidation state of +2.</p>
</div>

<h5>Chlorine in compounds with fluorine or oxygen</h5>

<div class="text-block">
<p>There are so many different oxidation states that chlorine can have in these, that it is safer to simply remember that the chlorine doesn't have an oxidation state of -1 in them, and work out its actual oxidation state when you need it. You will find an example of this below.</p>
</div>

<h4>Warning! Examples of working out oxidation states</h4>

<h5>What is the oxidation state of chromium in Cr<sup>2+</sup>?</h5>

<div class="text-block">
<p>That's easy! For a simple ion like this, the oxidation state is the charge on the ion – in other words: +2 (Don't forget the + sign.)</p>
</div>

<h5>What is the oxidation state of chromium in CrCl<sub>3</sub>?</h5>

<div class="text-block">
<p>This is a neutral compound so the sum of the oxidation states is zero. Chlorine has an oxidation state of -1. If the oxidation state of chromium is n:</p>
</div>

<div class="block-formula" label="don't forget the + sign">
\begin{aligned}
\overset{n}{\text{Cr}}\text{Cl}_3\\
\\
n + 3 \times ({-}1) &= 0 \\
n &= {+}3
\end{aligned}
</div>

<h5>What is the oxidation state of chromium in Cr(H<sub>2</sub>O)<sub>6</sub><sup>3+</sup>?</h5>

<div class="text-block">
<p>This is an ion and so the sum of the oxidation states is equal to the charge on the ion. There is a short-cut for working out oxidation states in complex ions like this where the metal atom is surrounded by electrically neutral molecules like water or ammonia.</p>
<p>The sum of the oxidation states in the attached neutral molecule must be zero. That means that you can ignore them when you do the sum. This would be essentially the same as an unattached chromium ion, Cr<sup>3+</sup>. The oxidation state is +3.</p>
</div>

<h5>What is the oxidation state of chromium in the dichromate ion, Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup>?</h5>

<div class="text-block">
<p>The oxidation state of the oxygen is -2, and the sum of the oxidation states is equal to the charge on the ion. Don't forget that there are 2 chromium atoms present.</p>
</div>

<div class="block-formula">
\begin{aligned}
{\overset{n}{\text{Cr}}}_2{\text{O}_7}^{2-} \\
\\
2n + 7({-}2) &= {-}2 \\
n &= {+}6
\end{aligned}
</div>

<div class="note">
<p>Warning: Because these are simple sums it is tempting to try to do them in your head. If it matters (like in an exam) write them down using as many steps as you need so that there is no chance of making careless mistakes. Your examiners aren't going to be impressed by your mental arithmetic – all they want is the right answer!</p>
<p>If you want some more examples to practice on, you will find them in most text books, including my <a href="../../book.html#top">chemistry calculations book</a>.</p>
</div>

<h5>What is the oxidation state of copper in CuSO<sub>4</sub>?</h5>

<div class="text-block">
<p>Unfortunately, it isn't always possible to work out oxidation states by a simple use of the rules above. The problem in this case is that the compound contains two elements (the copper and the sulfur) whose oxidation states can both change.</p>
<p>The only way around this is to know some simple chemistry! There are two ways you might approach it. (There might be others as well, but I can't think of them at the moment!)</p>
</div>

<ul>
<li>You might recognise this as an ionic compound containing copper ions and sulfate ions, SO<sub>4</sub><sup>2-</sup>. To make an electrically neutral compound, the copper must be present as a 2+ ion. The oxidation state is therefore +2.</li> 
<li>You might recognise the formula as being copper(II) sulfate. The "(II)" in the name tells you that the oxidation state is 2 (see below).</p>
<p>You will know that it is +2 because you know that metals form positive ions, and the oxidation state will simply be the charge on the ion.</li>
</ul>

<h3>Using Oxidation States</h3>

<h4>In naming compounds</h4>

<div class="text-block">
<p>You will have come across names like iron(II) sulfate and iron(III) chloride. The (II) and (III) are the oxidation states of the iron in the two compounds: +2 and +3 respectively. That tells you that they contain Fe<sup>2+</sup> and Fe<sup>3+</sup> ions.</p>
<p>This can also be extended to the negative ion. Iron(II) sulfate is FeSO<sub>4</sub>. There is also a compound FeSO<sub>3</sub> with the old name of iron(II) sulfite. The modern names reflect the oxidation states of the sulfur in the two compounds.</p>
<p>The sulfate ion is SO<sub>4</sub><sup>2-</sup>. The oxidation state of the sulfur is +6 (work it out!). The ion is more properly called the sulfate(VI) ion.</p>
<p>The sulfite ion is SO<sub>3</sub><sup>2-</sup>. The oxidation state of the sulfur is +4 (work that out as well!). This ion is more properly called the sulfate(IV) ion. The ate ending simply shows that the sulfur is in a negative ion.</p>
<p>So FeSO<sub>4</sub> is properly called iron(II) sulfate(VI), and FeSO<sub>3</sub> is iron(II) sulfate(IV). In fact, because of the easy confusion between these names, the old names sulfate and sulfite are normally still used in introductory chemistry courses.</p>
</div>

<div class="note">
<p>Note: Even these aren't the full name! The oxygens in the negative ions should also be identified. FeSO<sub>4</sub> is properly called iron(II) tetraoxosulfate(VI). It all gets a bit out of hand for everyday use for common ions.</p>
</div>

<h4>Using oxidation states to identify what's been oxidised and what's been reduced</h4>

<div class="text-block">
<p>This is easily the most common use of oxidation states.</p>
<p>Remember:</p>
</div>

<ul>
<li>Oxidation involves an increase in oxidation state</li>
<li>Reduction involves a decrease in oxidation state</li>
</ul>

<div class="text-block">
<p>In each of the following examples, we have to decide whether the reaction involves redox, and if so what has been oxidised and what reduced.</p>
</div>

<h5>Example 1:</h5>

<div class="text-block">
<p>This is the reaction between magnesium and hydrochloric acid or hydrogen chloride gas:</p>
</div>

<div class="block-formula">
\text{Mg} + 2\text{HCl} \longrightarrow \text{MgCl}_2 + \text{H}_2
</div>

<div class="text-block">
<p>Have the oxidation states of anything changed? Yes they have – you have two elements which are in compounds on one side of the equation and as uncombined elements on the other. Check all the oxidation states to be sure:.</p>
</div>

<div class="image-container"><img src="mghcl2.gif"></div>

<div class="text-block">
<p>The magnesium's oxidation state has increased – it has been oxidised. The hydrogen's oxidation state has fallen – it has been reduced. The chlorine is in the same oxidation state on both sides of the equation – it hasn't been oxidised or reduced.</p>
</div>

<h5>Example 2:</h5>

<div class="text-block">
<p>The reaction between sodium hydroxide and hydrochloric acid is:</p>
</div>

<div class="block-formula">
\text{NaOH} + \text{HCl} \longrightarrow \text{NaCl} + \text{H}_2\text{O}
</div>

<div class="text-block">
<p>Checking all the oxidation states:</p>
</div>

<div class="image-container"><img src="naohhcl2.gif"></div>

<div class="text-block">
<p>Nothing has changed. This isn't a redox reaction.</p>
</div>

<h5>Example 3:</h5>

<div class="text-block">
<p>This is a sneaky one! The reaction between chlorine and cold dilute sodium hydroxide solution is:</p>
</div>

<div class="block-formula">
2\text{NaOH} + \text{Cl}_2 \longrightarrow \text{NaCl} + \text{NaClO} + \text{H}_2\text{O}
</div>

<div class="text-block">
<p>Obviously the chlorine has changed oxidation state because it has ended up in compounds starting from the original element. Checking all the oxidation states shows:</p>
</div>

<div class="image-container"><img src="cl2naoh2.gif"></div>

<div class="text-block">
<p>The chlorine is the only thing to have changed oxidation state. Has it been oxidised or reduced? Yes! Both! One atom has been reduced because its oxidation state has fallen. The other has been oxidised.</p>
<p>This is a good example of a disproportionation reaction. A disproportionation reaction is one in which a single substance is both oxidised and reduced.</p>
</div>

<h4>Using oxidation states to identify the oxidising and reducing agent</h4>

<div class="text-block">
<p>This is just a minor addition to the last section. If you know what has been oxidised and what has been reduced, then you can easily work out what the oxidising agent and reducing agent are.</p>
</div>

<h5>Example 1</h5>

<div class="text-block">
<p>This is the reaction between chromium(III) ions and zinc metal:</p>
</div>

<div class="block-formula">
2\text{Cr}^{3+} + \text{Zn} \longrightarrow 2\text{Cr}^{2+} + \text{Zn}^{2+}
</div>

<div class="text-block">
<p>The chromium has gone from the +3 to the +2 oxidation state, and so has been reduced. The zinc has gone from the zero oxidation state in the element to +2. It has been oxidised.</p>
<p>So what is doing the reducing? It is the zinc – the zinc is giving electrons to the chromium (III) ions. So zinc is the reducing agent.</p>
<p>Similarly, you can work out that the oxidising agent has to be the chromium(III) ions, because they are taking electrons from the zinc.</p>
</div>

<h5>Example 2</h5>

<div class="text-block">
<p>This is the equation for the reaction between manganate(VII) ions and iron(II) ions under acidic conditions. This is worked out further down the page.</p>
</div>

<div class="block-formula">
{\text{MnO}_4}^- + 8\text{H}^+ + 5\text{Fe}^{2+} \longrightarrow \text{Mn}^{2+} + 4\text{H}_2\text{O} + 5\text{Fe}^{3+}
</div>

<div class="text-block">
<p>Looking at it quickly, it is obvious that the iron(II) ions have been oxidised to iron(III) ions. They have each lost an electron, and their oxidation state has increased from +2 to +3.</p>
<p>The hydrogen is still in its +1 oxidation state before and after the reaction, but the manganate(VII) ions have clearly changed. If you work out the oxidation state of the manganese, it has fallen from +7 to +2 – a reduction.</p>
<p>So the iron(II) ions have been oxidised, and the manganate(VII) ions reduced.</p>
<p>What has reduced the manganate(VII) ions – clearly it is the iron(II) ions. Iron is the only other thing that has a changed oxidation state. So the iron(II) ions are the reducing agent.</p>
<p>Similarly, the manganate(VII) ions must be the oxidising agent.</p>
</div>

<h4>Using oxidation states to work out reacting proportions</h4>

<div class="text-block">
<p>This is sometimes useful where you have to work out reacting proportions for use in titration reactions where you don't have enough information to work out the complete ionic equation.</p>
<p>Remember that each time an oxidation state changes by one unit, one electron has been transferred. If one substance's oxidation state in a reaction falls by 2, that means that it has gained 2 electrons.</p>
<p>Something else in the reaction must be losing those electrons. Any oxidation state fall by one substance must be accompanied by an equal oxidation state increase by something else.</p>
<p>This example is based on information in an old AQA A-level question.</p>
<p>Ions containing cerium in the +4 oxidation state are oxidising agents. (They are more complicated than just Ce<sup>4+</sup>.) They can oxidise ions containing molybdenum from the +2 to the +6 oxidation state (from Mo<sup>2+</sup> to MoO<sub>4</sub><sup>2-</sup>). In the process the cerium is reduced to the +3 oxidation state (Ce<sup>3+</sup>). What are the reacting proportions?</p>
<p>The oxidation state of the molybdenum is increasing by 4. That means that the oxidation state of the cerium must fall by 4 to compensate.</p>
<p>But the oxidation state of the cerium in each of its ions only falls from +4 to +3 – a fall of 1. So there must obviously be 4 cerium ions involved for each molybdenum ion.</p>
<p>The reacting proportions are 4 cerium-containing ions to 1 molybdenum ion.</p>
<p>Or to take a more common example involving iron(II) ions and manganate(VII) ions</p>
<p>A solution of potassium manganate(VII), KMnO<sub>4</sub>, acidified with dilute sulfuric acid oxidises iron(II) ions to iron(III) ions. In the process, the manganate(VII) ions are reduced to manganese(II) ions. Use oxidation states to work out the equation for the reaction.</p>
<p>The oxidation state of the manganese in the manganate(VII) ion is +7. The name tells you that, but work it out again just for the practice!</p>
<p>In going to manganese(II) ions, the oxidation state of manganese has fallen by 5. Every iron(II) ion that reacts, increases its oxidation state by 1. That means that there must be five iron(II) ions reacting for every one manganate(VII) ion.</p>
<p>The left-hand side of the equation will therefore be: MnO<sub>4</sub><sup>-</sup> + 5Fe<sup>2+</sup> + ?</p>
<p>The right-hand side will be: Mn<sup>2+</sup> + 5Fe<sup>3+</sup> + ?</p>
<p>After that you will have to make guesses as to how to balance the remaining atoms and the charges. In this case, for example, it is quite likely that the oxygen will end up in water. That means that you need some hydrogen from somewhere.</p>
<p>That isn't a problem because you have the reaction in acid solution, so the hydrogens could well come from hydrogen ions.</p>
<p>Eventually, you will end up with this:</p>
</div>

<div class="block-formula">
{\text{MnO}_4}^- + 8\text{H}^+ + 5\text{Fe}^{2+} \longrightarrow \text{Mn}^{2+} + 4\text{H}_2\text{O} + 5\text{Fe}^{3+}
</div>

<div class="text-block">
<p>Personally, I would much rather work out these equations from electron-half-equations!</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-oxstates.pdf" target="_blank">Questions on oxidation states</a>
<a href="../questions/a-oxstates.pdf" target="_blank">Answers</a>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../redoxmenu.html#top">To the Redox menu</a>
<a href="../../inorgmenu.html#top">To the Inorganic Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>