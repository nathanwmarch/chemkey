<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Chromium | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes and explains some features of chromium chemistry">
<meta name="keywords" content="chromium, redox, chromate, dichromate, titrations">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Chromium</h1>

<div class="text-block">
<p>This page looks at some aspects of chromium chemistry required for UK A-level (and its equivalents). It includes: reactions of chromium(III) ions in solution (summarised from elsewhere on the site); the interconversion of the various oxidation states of chromium; the chromate(VI)-dichromate(VI) equilibrium; and the use of dichromate(VI) ions as an oxidising agent (including titrations).</p>
<p>The first part of this page is a summary of the reactions of chromium(III) ions in solution. You will find links to other pages where these reactions are discussed in more detail.</p>
<p>You are very unlikely to need everything on this page. Check your syllabus and past papers to find out exactly what you need to know.</p>
</div>

<h2>Reactions of Chromium(III) Ions in Solution</h2>

<div class="text-block">
<p>The simplest ion that chromium forms in solution is the hexaaquachromium(III) ion – [Cr(H<sub>2</sub>O)<sub>6</sub>]<sup>3+</sup>.</p>
</div>

<div class="note">
<p>Note: If you aren't happy about <a href="../complexmenu.html#top">complex ions</a> (including the way they are bonded and named), it would pay you to follow this link and explore the first couple of pages in the complex ions menu before you go on.</p>
</div>

<h3>The Acidity of the Hexaaqua Ions</h3>

<div class="text-block">
<p>In common with the other 3+ ions, the hexaaquachromium(III) ion is fairly acidic – with a pH for typical solutions in the 2 – 3 range.</p>
<p>The ion reacts with water molecules in the solution. A hydrogen ion is lost from one of the ligand water molecules:</p>
</div>

<div class="block-formula">
[\text{Cr}(\text{H}_2\text{O})_6]^{3+} + \color{#467abf}{\text{H}_2\text{O}} \xrightleftharpoons{} [\text{Cr}(\text{H}_2\text{O})(\text{OH})]^{2+} + \color{#467abf}{\text{H}_3\text{O}^+}
</div>

<div class="text-block">
<p>The complex ion is acting as an acid by donating a hydrogen ion to water molecules in the solution. The water is, of course, acting as a base by accepting the hydrogen ion.</p>
<p>Because of the confusing presence of water from two different sources (the ligands and the solution), it is easier to simplify this:</p>
</div>

<div class="block-formula">
[\text{Cr}(\text{H}_2\text{O})_6]^{3+}_{(aq)} \xrightleftharpoons{} [\text{Cr}(\text{H}_2\text{O})(\text{OH})]^{2+}_{(aq)} + \text{H}^+_{(aq)}
</div>

<div class="text-block">
<p>However, if you write it like this, remember that the hydrogen ion isn't just falling off the complex ion. It is being pulled off by a water molecule in the solution. Whenever you write "H<sup>+</sup><sub>(aq)</sub>" what you really mean is a hydroxonium ion, H<sub>3</sub>O<sup>+</sup>.</p>
</div>

<div class="note">
<p>Note: You will find the full reasons for the <a href="../complexions/acidity.html#top">acidity of hexaaqua ions</a> if you follow this link. You only need to read the beginning of that page which concentrates on explaining the acidity of the hexaaquairon(III) ion. What is said applies equally to the chromium-containing ion.</p>
</div>

<a name="exchange"></a>
<h3>Ligand Exchange Reactions Involving Chloride or Sulfate Ions</h3>

<div class="text-block">
<p>The hexaaquachromium(III) ion is a "difficult to describe" violet-blue-grey colour. However, when it is produced during a reaction in a test tube, it is often green.</p>
<p>We nearly always describe the green ion as being Cr<sup>3+</sup><sub>(aq)</sub> – implying the hexaaquachromium(III) ion. That's actually an over-simplification.</p>
<p>What happens is that one or more of the ligand water molecules get replaced by a negative ion in the solution – typically sulfate or chloride.</p>
</div>

<h4>Replacement of the water by sulfate ions</h4>

<div class="text-block">
<p>You can do this simply by warming some chromium(III) sulfate solution.</p>
</div>

<div class="image-container"><img src="crso4.gif"></div>

<div class="text-block">
<p>One of the water molecules is replaced by a sulfate ion. Notice the change in the charge on the ion. Two of the positive charges are cancelled by the presence of the two negative charges on the sulfate ion.</p>
</div>

<h4>Replacement of the water by chloride ions</h4>

<div class="text-block">
<p>In the presence of chloride ions (for example with chromium(III) chloride), the most commonly observed colour is green. This happens when two of the water molecules are replaced by chloride ions to give the tetraaquadichlorochromium(III) ion – [Cr(H<sub>2</sub>O)<sub>4</sub>Cl<sub>2</sub>]<sup>+</sup>.</p>
<p>Once again, notice that replacing water molecules by chloride ions changes the charge on the ion.</p>
</div>

<div class="note">
<p>Note: You will find an extensive discussion of <a href="../complexions/ligandexch.html#top">ligand exchange reactions</a> if you follow this link.</p>
</div>

<h3>Reactions of Hexaaquachromium(III) Ions With Hydroxide Ions</h3>

<div class="text-block">
<p>Hydroxide ions (from, say, sodium hydroxide solution) remove hydrogen ions from the water ligands attached to the chromium ion.</p>
<p>Once a hydrogen ion has been removed from three of the water molecules, you are left with a complex with no charge – a neutral complex. This is insoluble in water and a precipitate is formed.</p>
</div>

<div class="block-formula">
[\text{Cr}(\text{H}_2\text{O})_6]^{3+} + 3\text{OH}^- \longrightarrow [\text{Cr}(\text{H}_2\text{O})_3(\text{OH})_3] + 3\text{H}_2\text{O}
</div>

<div class="note">
Note: The colour coding is to show that this isn't a ligand exchange reaction. The oxygens which were originally attached to the chromium are still attached in the neutral complex.
</div>

<div class="text-block">
<p>But the process doesn't stop there. More hydrogen ions are removed to give ions like [Cr(H<sub>2</sub>O)<sub>2</sub>(OH)<sub>4</sub>]<sup>-</sup> and [Cr(OH)<sub>6</sub>]<sup>3-</sup>.</p>
</div>

<h5>For example:</h5>

<div class="block-formula">
[\text{Cr}(\text{H}_2\text{O})_3(\text{OH})_3] + \color{#467abf}{3\text{OH}^-} \longrightarrow [\text{Cr}(\text{OH})_6]^{3-} + \color{#467abf}{3\text{H}_2\text{O}}
</div>

<div class="text-block">
<p>The precipitate redissolves because these ions are soluble in water.</p>
<p>In the test-tube, the colour changes are:</p>
</div>

<div class="image-container"><img src="croh.gif"></div>

<div class="note">
<p>Note: You will find the <a href="../complexions/aquaoh.html#top">reactions between hexaaqua ions and hydroxide ions</a> discussed in detail if you follow this link.</p>
</div>

<h3>Reactions of Hexaaquachromium(III) Ions With Ammonia Solution</h3>

<div class="text-block">
<p>The ammonia acts as both a base and a ligand. With a small amount of ammonia, hydrogen ions are pulled off the hexaaqua ion exactly as in the hydroxide ion case to give the same neutral complex.</p>
</div>

<div class="block-formula">
[\text{Cr}(\text{H}_2\text{O})_6]^{3+} + 3\text{NH}_3 \longrightarrow [\text{Cr}(\text{H}_2\text{O})_3(\text{OH})_3] + 3{\text{NH}_4}^+
</div>

<div class="text-block">
<p>That precipitate dissolves to some extent if you add an excess of ammonia (especially if it is concentrated). The ammonia replaces water as a ligand to give hexaamminechromium(III) ions.</p>
</div>

<div class="block-formula">
[\text{Cr}(\text{H}_2\text{O})_6]^{3+} + 6\text{NH}_3 \xrightleftharpoons{} [\text{Cr}(\text{NH}_3)_6]^{3+} + 6\text{H}_2\text{O}
</div>

<div class="note">
<p>Note: You might wonder why this second equation is given starting from the original hexaaqua ion rather than the neutral complex. Explaining why the precipitate redissolves is quite complicated. You will find the explanation in full (although by reference to the corresponding copper case) on the page about the <a href="../complexions/aquanh3.html#top">reactions between hexaaqua ions and ammonia solution</a>.</p>
</div>

<h5>The colour changes are:</h5>

<div class="image-container"><img src="nh3crcols.gif"></div>

<h3>Reactions of Hexaaquachromium(III) Ions With Carbonate Ions</h3>

<div class="text-block">
<p>If you add sodium carbonate solution to a solution of hexaaquachromium(III) ions, you get exactly the same precipitate as if you added sodium hydroxide solution or ammonia solution.</p>
<p>This time, it is the carbonate ions which remove hydrogen ions from the hexaaqua ion and produce the neutral complex.</p>
<p>Depending on the proportions of carbonate ions to hexaaqua ions, you will get either hydrogencarbonate ions formed or carbon dioxide gas from the reaction between the hydrogen ions and carbonate ions. The more usually quoted equation shows the formation of carbon dioxide. </p>
</div>

<div class="block-formula">
    2[\text{Cr}(\text{H}_2\text{O})_6]^{3+} + 3{\text{CO}_3}^{2-} \longrightarrow 2[\text{Cr}(\text{H}_2\text{O})_3(\text{OH})_3] + 3\text{CO}_2 + 3\text{H}_2\text{O}
</div>

<div class="text-block">
<p>Apart from the carbon dioxide, there is nothing new in this reaction:</p>
</div>

<div class="image-container"><img src="crco3diag.gif"></div>

<div class="note">
Note: You will find the <a href="../complexions/aquaco3.html#top">reactions between hexaaqua ions and carbonate ions</a> discussed in detail if you follow this link.
</div>

<h3>The Oxidation of Chromium(III) to Chromium(VI)</h3>

<div class="text-block">
<p>An excess of sodium hydroxide solution is added to a solution of the hexaaquachromium(III) ions to produce a solution of green hexahydroxochromate(III) ions.</p>
</div>

<div class="image-container"><img src="croh.gif"></div>

<div class="text-block">
<p>This is then oxidised by warming it with hydrogen peroxide solution. You eventually get a bright yellow solution containing chromate(VI) ions.</p>
</div>

<div class="image-container"><img src="cr3to6.gif"></div>

<h5>The equation for the oxidation stage is:</h5>

<div class="block-formula">
    2[\text{Cr}(\text{OH})_6]^{3-} + 3\text{H}_2\text{O}_2 \longrightarrow 2{\text{CrO}_4}^{2-} + 2\text{OH}^- + 8\text{H}_2\text{O}
</div>

<div class="note">
<p>Note: Although it is still a complex ion, you don't write square brackets around the chromate(VI) ion – any more than you would around a sulfate or carbonate ion.</p>
<p>If you want to know <a href="../redox/equations2.html#top">how to work out this equation</a> , follow this link.</p>
</div>

<h2>Some Chromium(VI) Chemistry</h2>

<h3>The Chromate(VI)-dichromate(VI) Equilibrium</h3>

<div class="text-block">
<p>You are probably more familiar with the orange dichromate(VI) ion, Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup>, than the yellow chromate(VI) ion, CrO<sub>4</sub><sup>2-</sup>.</p>
</div>

<h5>Changing between them is easy:</h5>

<div class="text-block">
<p>If you add dilute sulfuric acid to the yellow solution it turns orange. If you add sodium hydroxide solution to the orange solution it turns yellow.</p>
</div>

<div class="image-container"><img src="dichrdiag.gif"></div>

<div class="note">
<p>Note: If you had just produced the yellow chromate(VI) ions by oxidising chromium(III) ions using hydrogen peroxide, you can't convert them into dichromate(VI) ions without taking a precaution first.</p>
<p>In the presence of acid, dichromate(VI) ions react with any hydrogen peroxide which is left in the solution from the original reaction. To prevent this, you heat the solution for some time to decompose the hydrogen peroxide into water and oxygen before adding the acid.</p>
</div>

<h5>The equilibrium reaction at the heart of the interconversion is:</h5>

<div class="block-formula">
    2{\text{CrO}_4}^{2-} + 2\text{H}^+ \xrightleftharpoons{} {\text{Cr}_2\text{O}_7}^{2-} + \text{H}_2\text{O}
</div>

<div class="text-block">
<p>If you add extra hydrogen ions to this, the equilibrium shifts to the right. This is consistent with Le Chatelier's Principle.</p>
</div>

<div class="block-formula">
    \begin{gathered}
    \color{#467abf}{\text{adding hydrogen ions forces the}}\\
    \underrightarrow{\color{#467abf}{\text{position of equilibrium to the right}}}\\
    2{\text{CrO}_4}^{2-} + 2\text{H}^+ \xrightleftharpoons{} {\text{Cr}_2\text{O}_7}^{2-} + \text{H}_2\text{O}
    \end{gathered}
</div>

<div class="note">
<p>Note: If you aren't familiar with <a href="../../physical/equilibria/lechatelier.html#top">Le Chatelier's Principle</a>, you should follow this link and read the first part of that page about the effect of concentration on position of equilibrium.</p>
</div>

<div class="text-block">
<p>If you add hydroxide ions, these react with the hydrogen ions. The equilibrium tips to the left to replace them.</p>
</div>

<div class="image-container"><img src="dichreqm3.gif"></div>

<h3>Making Potassium dichromate(VI) Crystals</h3>

<div class="text-block">
<p>Potassium dichromate crystals can be made by a combination of the reactions we've already looked at on this page.</p>
<p>Starting from a source of chromium(III) ions such as chromium(III) chloride solution:</p>
<p>You add potassium hydroxide solution to give first a grey-green precipitate and then the dark green solution containing [Cr(OH)<sub>6</sub>]<sup>3-</sup> ions. This is all described in detail further up the page. Notice that you have to use potassium hydroxide. If you used sodium hydroxide, you would end up eventually with sodium dichromate(VI).</p>
<p>Now you oxidise this solution by warming it with hydrogen peroxide solution. The solution turns yellow as potassium chromate(VI) is formed. This reaction is also described further up the page.</p>
<p>All that is left is to convert the yellow potassium chromate(VI) solution into orange potassium dichromate(VI) solution. You may remember that that is done by adding acid. This is described above if you have forgotten.</p>
<p>Unfortunately there is a problem here. Potassium dichromate will react with any excess hydrogen peroxide to give initially an unstable deep blue solution and it eventually gives the original chromium(III) ions again! To get around this, you first need to destroy any excess hydrogen peroxide.</p>
<p>This is done by boiling the solution. Hydrogen peroxide decomposes on heating to give water and oxygen. The solution is boiled until no more bubbles of oxygen are produced. The solution is heated further to concentrate it, and then concentrated ethanoic acid is added to acidify it. Orange crystals of potassium dichromate are formed on cooling.</p>
</div>

<h3>The Reduction of Dichromate(VI) Ions With Zinc and an Acid</h3>

<div class="text-block">
<p>Dichromate(VI) ions (for example, in potassium dichromate(VI) solution) can be reduced to chromium(III) ions and then to chromium(II) ions using zinc and either dilute sulfuric acid or hydrochloric acid.</p>
<p>Hydrogen is produced from a side reaction between the zinc and acid. This must be allowed to escape, but you need to keep air out of the reaction. Oxygen in the air rapidly re-oxidises chromium(II) to chromium(III).</p>
<p>An easy way of doing this is to put a bit of cotton wool in the top of the flask (or test-tube) that you are using. This allows the hydrogen to escape, but stops most of the air getting in against the flow of the hydrogen.</p>
</div>

<div class="image-container"><img src="dichrzndiag.gif"></div>

<div class="text-block">
<p>The reason for the inverted commas around the chromium(III) ion is that this is a simplification. The exact nature of the complex ion will depend on which acid you use in the reduction process. This has already been discussed towards the top of the page.</p>
</div>

<div class="note">
<p>Note: To <a href="#exchange">re-read this</a> use this link.</p>
</div>

<h5>The equations for the two stages of the reaction are:</h5>

<h5>For the reduction from +6 to +3:</h5>

<div class="block-formula">
    {\text{Cr}_2\text{O}_7}^{2-} + 14\text{H}^+ + 3\text{Zn} \longrightarrow 2\text{Cr}^{3+} + 7\text{H}_2\text{O} + 3\text{Zn}^{2+}
</div>

<h5>For the reduction from +3 to +2:</h5>

<div class="block-formula">
    2\text{Cr}^{3+} + \text{Zn} \longrightarrow 2\text{Cr}^{2+} + \text{Zn}^{2+}
</div>

<div class="note">
<p>Note: If you don't know how to work out equations like this, you can find out how to do it on the page about <a href="../redox/equations.html#top">writing ionic equations for redox reactions</a>.</p>
</div>

<h3>Using Potassium Dichromate(VI) as an Oxidising Agent in Organic Chemistry</h3>

<div class="text-block">
<p>Potassium dichromate(VI) solution acidified with dilute sulfuric acid is commonly used as an oxidising agent in organic chemistry. It is a reasonably strong oxidising agent without being so powerful that it takes the whole of the organic molecule to pieces! (Potassium manganate(VII) solution has some tendency to do that.)</p>
<p>It is used to:</p>
</div>

<ul>
<li>oxidise secondary alcohols to ketones;</li>
<li>oxidise primary alcohols to aldehydes;</li>
<li>oxidise primary alcohols to carboxylic acids.</li>
</ul>

<div class="text-block">
<p>For example, with ethanol (a primary alcohol), you can get either ethanal (an aldehyde) or ethanoic acid (a carboxylic acid) depending on the conditions.</p>
</div>

<ul>
<li>If the alcohol is in excess, and you distil off the aldehyde as soon as it is formed, you get ethanal as the main product.</p>

<div class="block-formula">
    {\text{Cr}_2\text{O}_7}^{2-} + 8\text{H}^+ + 3\text{CH}_2\text{CH}_2\text{OH} \longrightarrow 2\text{Cr}^{3+} + 7\text{H}_2\text{O} + 3\text{CH}_3\text{CHO}
</div>

<li>If the oxidising agent is in excess, and you don't allow the product to escape – for example, by heating the mixture under reflux (heating the flask with a condenser placed vertically in the neck) – you get ethanoic acid.</p>

<div class="block-formula">
    2{\text{Cr}_2\text{O}_7}^{2-} + 16\text{H}^+ + 3\text{CH}_2\text{CH}_2\text{OH} \longrightarrow 4\text{Cr}^{3+} + 11\text{H}_2\text{O} + 3\text{CH}_3\text{COOH}
</div>

</ul>

<div class="text-block">
<p>In organic chemistry, these equations are often simplified to concentrate on what is happening to the organic molecules. For example, the last two could be written:</p>
</div>

<div class="block-formula">
    \begin{gathered}
    \text{CH}_3\text{CH}_2\text{OH} + [\text{O}] \longrightarrow \text{CH}_3\text{CHO} + \text{H}_2\text{O} \\
    \\
    \text{CH}_3\text{CH}_2\text{OH} + 2[\text{O}] \longrightarrow \text{CH}_3\text{COOH} + \text{H}_2\text{O}
    \end{gathered}
</div>

<div class="text-block">
<p>The oxygen written in square brackets just means "oxygen from an oxidising agent".</p>
</div>

<div class="note">
<p>Note: These are not a proper substitute for real equations. Only use them if your examiners are happy with them. Check your syllabus and look at past papers and mark schemes.</p>
<p>If you are working towards a UK-based exam and don't have these things, you can find out how to get hold of them by going to the <a href="../../syllabuses.html#top">syllabuses</a> page.</p>
</div>

<h4>Using this same reaction to make chrome alum crystals</h4>

<div class="text-block">
<p>You will find chrome alum under all sorts of different names:</p>
</div>

<ul>
<li>chrome alum</li>
<li>potassium chromium(III) sulfate</li>
<li>chromium(III) potassium sulfate</li>
<li>chromium(III) potassium sulfate-12-water</li>
<li>chromium(III) potassium sulfate dodecahydrate</li>
</ul>

<div class="text-block">
<p> and various others!</p>
<p>You will also find variations on its formula. For example:</p>
</div>

<ul>
<li>CrK(SO<sub>4</sub>)<sub>2</sub>,12H<sub>2</sub>O</li>
<li>Cr<sub>2</sub>(SO<sub>4</sub>)<sub>3</sub>,K<sub>2</sub>SO<sub>4</sub>,24H<sub>2</sub>O</li>
<li>K<sub>2</sub>SO<sub>4</sub>,Cr<sub>2</sub>(SO<sub>4</sub>)<sub>3</sub>,24H<sub>2</sub>O</li>
</ul>

<div class="text-block">
<p>The first of these formulae is just the other ones divided by two and rearranged a bit. Personally, I prefer the second one because it is easier to understand what is going on.</p>
<p>Chrome alum is known as a double salt. If you mix solutions of potassium sulfate and chromium(III) sulfate so that their molar concentrations are the same, the solution behaves just like you would expect of such a mixture. It gives the reactions of chromium(III) ions, of potassium ions, and of sulfate ions.</p>
<p>However, if you crystallise it, instead of getting mixed crystals of potassium sulfate and chromium(III) sulfate, the solution crystallises as single deep purple crystals. These are "chrome alum".</p>
<p>Chrome alum crystals can be made by reducing acidified potassium dichromate(VI) solution using ethanol, and then crystallising the resulting solution.</p>
<p>Assuming you use an excess of ethanol, the main organic product will be ethanal – and we've already seen this equation above:</p>
</div>

<div class="block-formula">
    {\text{Cr}_2\text{O}_7}^{2-} + 8\text{H}^+ + 3\text{CH}_2\text{CH}_2\text{OH} \longrightarrow 2\text{Cr}^{3+} + 7\text{H}_2\text{O} + 3\text{CH}_3\text{CHO}
</div>

<div class="text-block">
<p>This ionic equation obviously doesn't contain the spectator ions, potassium and sulfate. Feeding those back in gives the full equation:</p>
</div>

<div class="block-formula full-width">
    \text{K}_2{\text{Cr}_2\text{O}_7} + 4\text{H}_2\text{SO}_4 + 3\text{CH}_2\text{CH}_2\text{OH} \longrightarrow \text{Cr}_2(\text{SO}_4)_3 + \text{K}_2\text{SO}_4 + 7\text{H}_2\text{O} + 3\text{CH}_3\text{CHO}
</div>

<div class="text-block">
<p>If you look at the top line on the right-hand side of the equation, you will see that the chromium(III) sulfate and potassium sulfate are produced in exactly the right proportions to make the double salt.</p>
<p>What you do, then, is this:</p>
</div>

<div class="note">
<p>Note: I am not giving quantities and exact conditions – there are practical and safety considerations which make me reluctant to do that. If you want precise details, they aren't difficult to find.</p>
</div>

<div class="text-block">
<p>You start with a solution of potassium dichromate(VI) to which has been added some concentrated sulfuric acid. The solution is then cooled by standing it in ice.</p>
<p>An excess of ethanol is added slowly with stirring so that the temperature doesn't rise too much.</p>
</div>

<div class="note">
<p>Note: If the solution gets too warm, you get a ligand exchange reaction between water molecules attached to the chromium(III) ions produced and sulfate ions in the solution. This leads to the green form of chromium(III) sulfate described higher up the page. To make chrome alum crystals, you have to stop this happening.</p>
</div>

<div class="text-block">
<p>When all the ethanol has been added, the solution is left over-night, preferably in a refrigerator, to crystallise. The crystals can be separated from the remaining solution, washed with a little pure water and then dried with filter paper.</p>
</div>

<h3>Using Potassium dichromate(VI) as an Oxidising Agent in Titrations</h3>

<div class="text-block">
<p>Potassium dichromate(VI) is often used to estimate the concentration of iron(II) ions in solution. It serves as an alternative to using potassium manganate(VII) solution.</p>
</div>

<div class="note">
<p>Note: Potassium manganate(VII) titrations are described fully on the page about <a href="manganese.html#top"> manganese chemistry</a>.</p>
</div>

<h4>In practice</h4>

<div class="text-block">
<p>There are advantages and disadvantages in using potassium dichromate(VI).</p>
</div>

<h5>Advantages</h5>

<ul>
<li>Potassium dichromate(VI) can be used as a primary standard. That means that it can be made up to give a stable solution of accurately known concentration. That isn't true of potassium manganate(VII).</li>
<li>Potassium dichromate(VI) can be used in the presence of chloride ions (as long as the chloride ions aren't present in very high concentration).<br>Potassium manganate(VII) oxidises chloride ions to chlorine; potassium dichromate(VI) isn't quite a strong enough oxidising agent to do this. That means that you don't get unwanted side reactions with the potassium dichromate(VI) soution.</li>
</ul>

<h5>Disadvantage</h5>

<ul>
<li>The main disadvantage lies in the colour change. Potassium manganate(VII) titrations are self-indicating. As you run the potassium manganate(VII) solution into the reaction, the solution becomes colourless. As soon as you add as much as one drop too much, the solution becomes pink – and you know you have reached the end point.<br>Unfortunately potassium dichromate(VI) solution turns green as you run it into the reaction, and there is no way you could possibly detect the colour change when you have one drop of excess orange solution in a strongly coloured green solution.<br>With potassium dichromate(VI) solution you have to use a separate indicator, known as a redox indicator. These change colour in the presence of an oxidising agent.<br>There are several such indicators – such as diphenylamine sulfonate. This gives a violet-blue colour in the presence of excess potassium dichromate(VI) solution. However, the colour is made difficult by the strong green also present. The end point of a potassium dichromate(VI) titration isn't as easy to see as the end point of a potassium manganate(VII) one.</li>
</ul>

<h4>The calculation</h4>

<h5>The half-equation for the dichromate(VI) ion is:</h5>

<div class="block-formula">
    {\text{Cr}_2\text{O}_7}^{2-} + 14\text{H}^+ + 6\text{e}^- \longrightarrow 2\text{Cr}^{3+} + 7\text{H}_2\text{O}
</div>

<h5>and for the iron(II) ions is:</h5>

<div class="block-formula">
    \text{Fe}^{2+} \longrightarrow \text{Fe}^{3+} + \text{e}^-
</div>

<h5>Combining these gives:</h5>

<div class="block-formula">
    {\text{Cr}_2\text{O}_7}^{2-} + 14\text{H}^+ + 6\text{Fe}^{2+} \longrightarrow 2\text{Cr}^{3+} + 7\text{H}_2\text{O} + 6\text{Fe}^{3+}
</div>

<div class="image-container"><img src="dichrfe2eqn.gif"></div>

<div class="text-block">
<p>You can see that the reacting proportions are 1 mole of dichromate(VI) ions to 6 moles of iron(II) ions.</p>
<p>Once you have established that, the titration calculation is going to be just like any other one.</p>
</div>

<div class="note">
<p>Note: If you aren't very good at doing titration calculations, you might be interested in my <a href="../../book.html#top">chemistry calculations book</a>.</p>
</div>

<h3>Testing for Chromate(VI) Ions in Solution</h3>

<div class="text-block">
<p>Typically, you would be looking at solutions containing sodium, potassium or ammonium chromate(VI). Most chromates are at best only slightly soluble; many we would count as insoluble.</p>
<p>The bright yellow colour of a solution suggests that it would be worth testing for chromate(VI) ions.</p>
</div>

<h4>Testing by adding an acid</h4>

<div class="text-block">
<p>If you add some dilute sulfuric acid to a solution containing chromate(VI) ions, the colour changes to the familiar orange of dichromate(VI) ions.</p>
</div>

<div class="image-container"><img src="testchr1.gif"></div>

<div class="text-block">
<p>You can't rely on this as a test for chromate(VI) ions, however. It might be that you have a solution containing an acid-base indicator which happens to have the same colour change!</p>
</div>

<h4>Testing by adding barium chloride (or nitrate) solution</h4>

<div class="text-block">
<p>Chromate(VI) ions will give a yellow precipitate of barium chromate(VI).</p>
</div>

<div class="block-formula">
    \text{Ba}^{2+}_{(aq)} + {\text{CrO}_4}^{2-}_{(aq)} \longrightarrow \text{BaCrO}_{4(s)}
</div>

<div class="image-container"><img src="testchr2.gif"></div>

<div class="sidebarright">
<div class="image-container" label="yellow precipitate of barium chromate(VI)">
<div class="image-credit" credit="Professor Stanley G. Smith"></div>
<a class="image-source" href="https://chemistry.illinois.edu/clc" target="_blank"></a>
<img src="bacro4.jpg">
</div>
</div>

<h4>Testing by adding lead(II) nitrate solution</h4>

<div class="text-block">
<p>Chromate(VI) ions will give a bright yellow precipitate of lead(II) chromate(VI). This is the original "chrome yellow" paint pigment.</p>
</div>

<div class="block-formula">
    \text{Pb}^{2+}_{(aq)} + {\text{CrO}_4}^{2-}_{(aq)} \longrightarrow \text{PbCrO}_{4(s)}
</div>

<div class="image-container"><img src="testchr3.gif"></div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-transchromium.pdf" target="_blank">Questions on chromium</a>
<a href="../questions/a-transchromium.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../transitionmenu.html#top">To the transition metal menu</a>
<a href="../../inorgmenu.html#top">To the Inorganic Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>

</div>
</body></html>