<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Atomic and Physical Properties of Periodic Table Group 7 (The Halogens) | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the trends in atomic radius, electronegativity , first electron affinity, melting and boiling points for the Group 7 elements in the Periodic Table. Also looks at the bond strengths of the X-X and H-X bonds.">
<meta name="keywords" content="fluorine, chlorine, bromine, iodine, group 7, periodic table, halogen, atomic radius, electron affinity, electronegativity, melting point, boiling point, trend, pattern">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Atomic and Physical Properties of Periodic Table Group 7 (The Halogens)</h1>

<div class="text-block">
<p>This page explores the trends in some atomic and physical properties of the Group 7 elements (the halogens) – fluorine, chlorine, bromine and iodine. You will find separate sections below covering the trends in atomic radius, electronegativity, electron affinity, melting and boiling points, and solubility. There is also a section on the bond enthalpies (strengths) of halogen-halogen bonds (for example, Cl-Cl) and of hydrogen-halogen bonds (e.g. H-Cl)</p>
<p>Even if you aren't currently interested in all these things, it would probably pay you to read the whole page. The same ideas tend to recur throughout the atomic properties, and you may find that earlier explanations help to you understand later ones.</p>
</div>

<h2>Trends in Atomic Radius</h2>

<div class="note">
<p>Note: You will find <a href="../../atoms/properties/atradius.html#top">atomic radius</a> covered in detail in another part of this site.</p>
</div>

<div class="image-container"><img src="atradii.gif"></div>	 

<div class="text-block">
<p>You can see that the atomic radius increases as you go down the Group.</p>
</div>

<h3>Explaining the Increase in Atomic Radius</h3>

<div class="text-block">
<p>The radius of an atom is governed by:</p>
</div>

<ul>
<li>the number of layers of electrons around the nucleus</li>
<li>the pull the outer electrons feel from the nucleus.</li>
</ul>

<div class="text-block">
<p>Compare fluorine and chlorine:</p>
</div>

<table class="list-table">
<tbody><tr><td>F</td><td></td><td>2,7</td></tr>
<tr><td>Cl</td><td></td><td>2,8,7</td></tr>
</tbody></table>

<div class="text-block">
<p>In each case, the outer electrons feel a net pull of 7+ from the nucleus. The positive charge on the nucleus is cut down by the negativeness of the inner electrons.</p>
</div>

<div class="image-container"><img src="fclsizes.gif"></div>	 

<div class="text-block">
<p>This is equally true for all the other atoms in Group 7. The outer electrons always feel a net pull of 7+ from the centre.</p>
<p>The only factor which is going to affect the size of the atom is therefore the number of layers of inner electrons which have to be fitted in around the atom. Obviously, the more layers of electrons you have, the more space they will take up – electrons repel each other. That means that the atoms are bound to get bigger as you go down the Group.</p>
</div>

<h2>Trends in Electronegativity</h2>

<div class="text-block">
<p>Electronegativity is a measure of the tendency of an atom to attract a bonding pair of electrons. It is usually measured on the Pauling scale, on which the most electronegative element (fluorine) is given an electronegativity of 4.0.</p>
</div>

<div class="note">
<p>Note: You will find <a href="../../atoms/bonding/electroneg.html#top">electronegativity</a> covered in detail in another part of this site.</p>
</div>

<div class="image-container"><img src="enegchart.gif"></div>	 

<div class="text-block">
<p>Notice that electronegativity falls as you go down the Group. The atoms become less good at attracting bonding pairs of electrons.</p>
</div>

<h3>Explaining the Decrease in Electronegativity</h3>

<div class="text-block">
<p>This is easily shown using simple dots-and-crosses diagrams for hydrogen fluoride and hydrogen chloride.</p>
</div>

<div class="image-container"><img src="hfandhcl.gif"></div>

<div class="text-block">
<p>The bonding pair of electrons between the hydrogen and the halogen feels the same net pull of 7+ from both the fluorine and the chlorine. (This is exactly the same sort of argument as you have seen in the atomic radius section above.) However, in the chlorine case, the nucleus is further away from that bonding pair. That means that it won't be as strongly attracted as in the fluorine case.</p>
<p>The larger pull from the closer fluorine nucleus is why fluorine is more electronegative than chlorine is.</p>
</div>

<div class="summary">
<p>As the halogen atoms get bigger, any bonding pair gets further and further away from the halogen nucleus, and so is less strongly attracted towards it. In other words, as you go down the Group, the elements become less electronegative.</p>
</div>	 

<h2>Trends in First Electron Affinity</h2>

<h3>Defining First Electron Affinity</h3>

<div class="text-block">
<p>The first electron affinity is the energy released when 1 mole of gaseous atoms each acquire an electron to form 1 mole of gaseous 1- ions.</p>
<p>This is more easily seen in symbol terms.</p>
</div>

<div class="block-formula">
\text{X}_{(g)} + \text{e}^- \longrightarrow \text{X}^-_{(g)}
</div>

<div class="text-block">
<p>It is the energy released (per mole of X) when this change happens.</p>
<p>First electron affinities have negative values. For example, the first electron affinity of chlorine is -349 kJ mol<sup>-1</sup>. By convention, the negative sign shows a release of energy.</p>
</div>

<h3>The First Electron Affinities of the Group 7 Elements</h3>

<div class="note">
<p>Note: You will find <a href="../../atoms/properties/eas.html#top">electron affinity</a> covered in detail in another part of this site. The current page duplicates much of that material, but you might like to read it again in different words.</p>
</div>

<div class="image-container"><img src="eachart.gif"></div>	 

<div class="text-block">
<p>Notice that the trend down the Group isn't tidy. The tendency is for the electron affinities to decrease (in the sense that less heat is given out), but the fluorine value is out of line.</p>
<p>The electron affinity is a measure of the attraction between the incoming electron and the nucleus. The higher the attraction, the higher the electron affinity.</p>
</div>

<div class="image-container"><img src="eadiag.gif"></div>	 

<div class="text-block">
<p>In the bigger atom, the attraction from the more positive nucleus is offset by the additional screening electrons, so each incoming electron feels the effect of a net 7+ charges from the centre – exactly as when you are thinking about atomic radius or electronegativity.</p>
<p>As the atom gets bigger, the incoming electron is further from the nucleus and so feels less attraction. The electron affinity therefore falls as you go down the Group.

</p>
<p>But what about fluorine? That is a very small atom, with the incoming electron quite close to the nucleus. Why isn't its electron affinity bigger than chlorine's?</p>
<p>There is another effect operating. When the new electron comes into the atom, it is entering a region of space already very negatively charged because of the existing electrons. There is bound to be some repulsion, offsetting some of the attraction from the nucleus.</p>
<p>In the case of fluorine, because the atom is very small, the existing electron density is very high. That means that the extra repulsion is particularly great and lessens the attraction from the nucleus enough to lower the electron affinity below that of chlorine.</p>
</div>

<h2>Trends in Melting Point and Boiling Point</h2>

<div class="image-container"><img src="mptbptgraph.gif"></div>

<div class="text-block">
<p>You will see that both melting points and boiling points rise as you go down the Group.</p>
<p>If you explore the graphs, you will find that fluorine and chlorine are gases at room temperature, bromine is a liquid and iodine a solid. Nothing very surprising there!</p>
</div>

<h3>Explaining the Trends in Melting Point and Boiling Point</h3>

<div class="text-block">
<p>All of the halogens exist as diatomic molecules – F<sub>2</sub>, Cl<sub>2</sub>, and so on. The intermolecular attractions between one molecule and its neighbours are van der Waals dispersion forces.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="../../atoms/bonding/vdw.html#top">van der Waals dispersion forces</a>, you will find them covered in detail in another part of this site. You won't understand the next bit unless you are happy about dispersion forces and how they vary with the size of the molecule.</p>
</div>

<div class="text-block">
<p>As the molecules get bigger there are obviously more electrons which can move around and set up the temporary dipoles which create these attractions.</p>
<p>The stronger intermolecular attractions as the molecules get bigger means that you have to supply more heat energy to turn them into either a liquid or a gas – and so their melting and boiling points rise.</p>
</div>

<h2>Solubilities</h2>

<h3>Solubility in Water</h3>

<div class="text-block">
<p>Fluorine reacts violently with water to give hydrogen fluoride gas (or a solution of hydrofluoric acid) and a mixture of oxygen and ozone. So thinking about its solubility is pointless.</p>
<p>Chlorine, bromine and iodine all dissolve in water to some extent, but there is no pattern in this. The following table shows the solubility of the three elements in water at 25°C.</p>
</div>

<table class="data-table">
<thead>
<tr><th> </th><th>solubility<br>/ mol dm<sup>-3</sup></th></tr>
</thead>
<tbody>
<tr><td>chlorine</td><td align="left">0.091</td></tr>
<tr><td>bromine</td><td align="left">0.21</td></tr>
<tr><td>iodine</td><td align="left">0.0013</td></tr>
</tbody></table>

<div class="note">
<p>Note: These figures come from page 476 of Advanced Inorganic Chemistry (third edition) by Cotton and Wilkinson.</p>
</div>

<div class="text-block">
<p>Chlorine solution in water is pale green. Bromine solution in water is anything from yellow to dark orange-red depending on how concentrated it is. Iodine solution in water is very pale brown.</p>
<p>Chlorine reacts with water to some extent to give a mixture of hydrochloric acid and chloric(I) acid – also known as hypochlorous acid. The reaction is reversible, and at any one time only about a third of the chlorine molecules have actually reacted.</p>
</div>

<div class="block-formula">
\text{Cl}_2 + \text{H}_2\text{O} \xrightleftharpoons{} + \text{HCl} + \text{HClO}
</div>

<div class="text-block">
<p>You will sometimes find the chloric(I) acid written as HOCl. That represents the way the atoms are actually joined together.</p>
<p>Bromine and iodine do something similar, but to a much lesser extent. In both cases, about 99.5% of the halogen remains as unreacted molecules.</p>
</div>

<h4>The solubility of iodine in potassium iodide solution</h4>

<div class="text-block">
<p>Although iodine is only faintly soluble in water, it does dissolve freely in potassium iodide solution to give a dark red-brown solution. There is a reversible reaction between iodine molecules and iodide ions to give I<sub>3</sub><sup>-</sup> ions. These are responsible for the colour.</p>
<p>In the lab, iodine is often produced by oxidation of a solution containing iodide ions, so this colour is actually quite familiar. As long as there are any excess iodide ions present, the iodine will react with them to make the I<sub>3</sub><sup>-</sup> ions. Once the iodide ions have all reacted, the iodine is precipitated as a dark grey solid, because there isn't anything left for it to react with to keep it in solution.</p>
</div>

<h3>Solubility in Hexane</h3>

<div class="text-block">
<p>The halogens are much more soluble in organic solvents like hexane than they are in water. Both hexane and the halogens are non-polar molecules attracted to each other by van der Waals dispersion forces.</p>
<p>That means that the attractions broken (between hexane molecules and between halogen molecules) are similar to the new attractions made when the two substances mix.</p>
<p>The colours of the solutions formed are much what you would expect. Solutions of iodine in organic solvents tend to be pinky-purple colour.</p>
</div>

<h2>Bond Enthalpies (Bond Energies or Bond Strengths)</h2>

<div class="text-block">
<p>Bond enthalpy is the heat needed to break one mole of a covalent bond to produce individual atoms, starting from the original substance in the gas state, and ending with gaseous atoms.</p>
<p>So for chlorine, Cl<sub>2(g)</sub>, it is the heat energy needed to carry out this change per mole of bond:</p>
</div>

<div class="block-formula">
\text{Cl}{-}\text{Cl}_{(g)} \longrightarrow 2\text{Cl}_{(g)}
</div>

<div class="text-block">
<p>For bromine, the reaction is still from gaseous bromine molecules to separate gaseous atoms.</p>
</div>

<div class="block-formula">
\text{Br}{-}\text{Br}_{\underbrace{(g)}_{{}\clap{\color{#467abf}{\text{note: gas, not a liquid}}}}} \longrightarrow 2\text{Br}_{(g)}
</div>

<h3>Bond Enthalpy in the Halogens, X<sub>2(g)</sub></h3>

<div class="text-block">
<p>A covalent bond works because the bonding pair is attracted to both the nuclei at either side of it. It is that attraction which holds the molecule together. The size of the attraction will depend, amongst other things, on the distance from the bonding pair to the two nuclei.</p>
</div>

<div class="image-container"><img src="x2small.gif"></div>

<div class="text-block">
<p>As with all halogens, the bonding pair will feel a net pull of 7+ from both ends of the bond – the charge on the nucleus offset by the inner electrons. That will still be the same whatever the size of the halogen atoms.</p>
<p>As the atoms get bigger, the bonding pair gets further from the nuclei and so you would expect the strength of the bond to fall.</p>
</div>

<div class="image-container"><img src="x2big.gif"></div>

<div class="text-block">
<p>So are the actual bond enthalpies in line with this prediction?</p>
</div>

<div class="image-container"><img src="bechartx2.gif"></div>

<div class="text-block">
<p>The bond enthalpies of the Cl-Cl, Br-Br and I-I bonds fall just as you would expect, but the F-F bond is way out of line!</p>
<p>Because fluorine atoms are so small, you might expect a very strong bond – in fact, it is remarkably weak. There must be another factor at work as well.</p>
<p>As well as the bonding pair of electrons between the two atoms, each atom has 3 non-bonding pairs of electrons in the outer level – lone pairs. Where the bond gets very short (as in F-F), the lone pairs on the two atoms get close enough together to set up a significant amount of repulsion.</p>
</div>

<div class="image-container"><img src="f2repel.gif"></div>

<div class="text-block">
<p>In the case of fluorine, this repulsion is great enough to counteract quite a lot of the attraction between the bonding pair and the two nuclei. This obviously weakens the bond.</p>
</div>

<h3>Bond Enthalpies in the Hydrogen Halides, HX<sub>(g)</sub></h3>

<div class="text-block">
<p>Where the halogen atom is attached to a hydrogen atom, this effect doesn't happen. There are no lone pairs on a hydrogen atom!</p>
</div>

<div class="image-container"><img src="becharthx.gif"></div>

<div class="text-block">
<p>As the halogen atom gets bigger, the bonding pair gets more and more distant from the nucleus. The attraction is less, and the bond gets weaker – exactly what is shown by the data. There is nothing complicated happening in this case.</p>
<p>This is important in the thermal stability of the hydrogen halides – how easily they are broken up into hydrogen and the halogen on heating.</p>
<p>Hydrogen fluoride and hydrogen chloride are very stable to heat. They don't split up into hydrogen and fluorine or chlorine again if heated to any normal lab temperature.</p>
<p>Hydrogen bromide splits slightly into hydrogen and bromine on heating, and hydrogen iodide splits to an even greater extent.</p>
<p>As the bonds get weaker, they are more easily broken.</p>
</div>

<div class="note">
<p>Note: Breaking the hydrogen-halogen bond is only one of the steps in the overall reaction, of course. You don't end up with hydrogen atoms and halogen atoms – you get diatomic molecules, H<sub>2</sub> and X<sub>2</sub>. If you have done some energetics calculations, it would be a useful exercise to use bond enthalpies and atomisation enthalpies to calculate the overall enthalpy changes for the decomposition of the hydrogen halides.</p>
<p>(Remember that bond enthalpies only apply to substances in the gas state, and bromine and iodine would end up as liquid and solid respectively. Using atomisation enthalpies for the halogens avoids this problem. If you don't understand what I am talking about, you don't yet have enough knowledge to be able to do this.)</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-gp7properties.pdf" target="_blank">Questions on the properties of Group 7 elements</a>
<a href="../questions/a-gp7properties.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../group7menu.html#top">To the Group 7 menu</a>
<a href="../../inorgmenu.html#top">To the Inorganic Chemistry menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>