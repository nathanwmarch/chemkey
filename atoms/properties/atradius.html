<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Atomic and Ionic Radii | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes and explains how atomic radii vary around the Periodic Table">
<meta name="keywords" content="atomic radius, atomic radii, ionic radius, ionic radii, metallic radius, metallic radii, covalent radius, covalent radii, van der waals radius, van der waals radii, electronic structure, atomic orbital, orbital, orbitals, electron, electrons, periodic table, periodicity">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Atomic and Ionic Radii</h1>

<div class="text-block">
<p>This page explains the various measures of atomic radius, and then looks at the way it varies around the Periodic Table – across periods and down groups. It assumes that you understand electronic structures for simple atoms written in s, p, d notation.</p>
</div>

<div class="note">
<p>Important! If you aren't reasonable happy about <a href="elstructs.html#top">electronic structures</a> you should follow this link before you go any further.</p>
</div>

<h2>Atomic Radius</h2>

<h3>Measures of Atomic Radius</h3>

<div class="text-block">
<p>Unlike a ball, an atom doesn't have a fixed radius. The radius of an atom can only be found by measuring the distance between the nuclei of two touching atoms, and then halving that distance.</p>
</div>

<div class="image-container">
<img src="squashed.GIF">
</div>

<div class="text-block">
<p>As you can see from the diagrams, the same atom could be found to have a different radius depending on what was around it.</p>
<p>The left hand diagram shows bonded atoms. The atoms are pulled closely together and so the measured radius is less than if they are just touching. This is what you would get if you had metal atoms in a metallic structure, or atoms covalently bonded to each other. The type of atomic radius being measured here is called the metallic radius or the covalent radius depending on the bonding.</p>
<p>The right hand diagram shows what happens if the atoms are just touching. The attractive forces are much less, and the atoms are essentially "unsquashed". This measure of atomic radius is called the van der Waals radius after the weak attractions present in this situation.</p>
</div>

<div class="note">
<p>Note: If you want to explore these various <a href="../bondingmenu.html#top">types of bonding</a> this link will take you to the bonding menu.</p>
</div>

<h3>Trends in Atomic Radius in the Periodic Table</h3>

<div class="text-block">
<p>The exact pattern you get depends on which measure of atomic radius you use – but the trends are still valid.</p>
<p>The following diagram uses metallic radii for metallic elements, covalent radii for elements that form covalent bonds, and van der Waals radii for those (like the noble gases) which don't form bonds.</p>
</div>

<h4>Trends in atomic radius in Periods 2 and 3</h4>

<div class="image-container">
<img src="p2and3radii.GIF">
</div>

<h4>Trends in atomic radius down a group</h4>

<div class="text-block">
<p>It is fairly obvious that the atoms get bigger as you go down groups. The reason is equally obvious – you are adding extra layers of electrons.</p>
</div>

<h4>Trends in atomic radius across periods</h4>

<div class="text-block">
<p>You have to ignore the noble gas at the end of each period. Because neon and argon don't form bonds, you can only measure their van der Waals radius – a case where the atom is pretty well "unsquashed". All the other atoms are being measured where their atomic radius is being lessened by strong attractions. You aren't comparing like with like if you include the noble gases.</p>
</div>

<div class="principle">
Leaving the noble gases out, atoms get smaller as you go across a period.
</div>

<div class="text-block">
<p>If you think about it, the metallic or covalent radius is going to be a measure of the distance from the nucleus to the electrons which make up the bond. (Look back to the left-hand side of the first diagram on this page if you aren't sure, and picture the bonding electrons as being half way between the two nuclei.)</p>
<p>From lithium to fluorine, those electrons are all in the 2-level, being screened by the 1s<sup>2</sup> electrons. The increasing number of protons in the nucleus as you go across the period pulls the electrons in more tightly. The amount of screening is constant for all of these elements.</p>
</div>

<div class="note">
<p>Note: You might possibly wonder why you don't get extra screening from the 2s<sup>2</sup> electrons in the cases of the elements from boron to fluorine where the bonding involves the p electrons.</p>
<p>In each of these cases, before bonding happens, the existing s and p orbitals are reorganised (hybridised) into new orbitals of equal energy. When these atoms are bonded, there aren't any 2s electrons as such.</p>
<p>If you don't know about hybridisation, just ignore this comment – you won't need it for UK A-level purposes anyway.</p>
</div>

<div class="text-block">
<p>In the period from sodium to chlorine, the same thing happens. The size of the atom is controlled by the 3-level bonding electrons being pulled closer to the nucleus by increasing numbers of protons – in each case, screened by the 1- and 2-level electrons.</p>
</div>

<h3>Trends in the Transition Elements</h3>

<div class="image-container">
<img src="dblockradii.GIF">
</div>

<div class="text-block">
<p>Although there is a slight contraction at the beginning of the series, the atoms are all much the same size.</p>
<p>The size is determined by the 4s electrons. The pull of the increasing number of protons in the nucleus is more or less offset by the extra screening due to the increasing number of 3d electrons.</p>
</div>

<div class="note">
<p>Note: The 4s orbital has a higher energy than the 3d in the transition elements. That means that it is a 4s electron which is lost from the atom when it forms an ion. It also means that the 3d orbitals are slightly closer to the nucleus than the 4s – and so offer some screening.</p>
<p>Confusingly, this is inconsistent with what we say when we use the Aufbau Principle to work out the electronic structures of atoms.</p>
<p>I have discussed this in detail in the page about <a href="3d4sproblem.html#top">the order of filling 3d and 4s orbitals</a>.</p>
<p>If you are a teacher or a very confident student then you might like to follow this link.</p>
<p>If you aren't so confident, or are coming at this for the first time, I suggest that you ignore it. Remember that the Aufbau Principle (which uses the assumption that the 3d orbitals fill after the 4s) is just a useful way of working out the structures of atoms, but that in real transition metal atoms the 4s is actually the outer, higher energy orbital.</p>
</div>

<h2>Ionic Radius</h2>

<h3>A warning!</h3>

<div class="text-block">
<p>Ionic radii are difficult to measure with any degree of certainty, and vary according to the environment of the ion. For example, it matters what the co-ordination of the ion is (how many oppositely charged ions are touching it), and what those ions are.</p>
<p>There are several different measures of ionic radii in use, and these all differ from each other by varying amounts. It means that if you are going to make reliable comparisons using ionic radii, they have to come from the same source.</p>
<p>What you have to remember is that there are quite big uncertainties in the use of ionic radii, and that trying to explain things in fine detail is made difficult by those uncertainties. What follows will be adequate for UK A-level (and its various equivalents), but detailed explanations are too complicated for this level.</p>
</div>

<h3>Trends in Ionic Radius in the Periodic Table</h3>

<h4>Trends in ionic radius down a group</h4>

<div class="text-block">
<p>This is the easy bit! As you add extra layers of electrons as you go down a group, the ions are bound to get bigger. The two tables below show this effect in Groups 1 and 7.</p>
</div>

<table class="data-table">
<thead>
<tr><th></th><th>electronic structure of ion</th><th>ionic radius<br>/ nm</th></tr>
</thead>
<tbody>
<tr><td>Li⁺</td><td>2</td><td>0.076</td></tr>
<tr><td>Na⁺</td><td>2, 8</td><td>0.102</td></tr>
<tr><td>K⁺</td><td>2, 8, 8</td><td>0.138</td></tr>
<tr><td>Rb⁺</td><td>2, 8, 18, 8</td><td>0.152</td></tr>
<tr><td>Cs⁺</td><td>2, 8, 18, 18, 8</td><td>0.167</td></tr>
</tbody>
</table>

<table class="data-table">
<thead>
<tr><th></th><th>electronic structure of ion</th><th>ionic radius<br>/ nm</th></tr>
</thead>
<tbody>
<tr><td>F⁻</td><td>2, 8</td><td>0.133</td></tr>

<tr><td>Cl⁻</td><td>2, 8, 8</td><td>0.181</td></tr>

<tr><td>Br⁻</td><td>2, 8, 18, 8</td><td>0.196</td></tr>

<tr><td>I⁻</td><td>2, 8, 18, 18, 8</td><td>0.220</td></tr>
</tbody>
</table>

<div class="note">
<p>Note: These figures all come from the <a href="http://abulafia.mt.ic.ac.uk/shannon/ptable.php">Database of Ionic Radii</a> from Imperial College London. I have converted them from Angstroms to nm (nanometres), which are more often used in the data tables that you are likely to come across.</p>
<p>If you are interested, 1 Angstrom is 10<sup>-10</sup> m; 1 nm = 10<sup>-9</sup> m. To convert from Angstroms to nm, you have to divide by 10, so that 1.02 Angstroms becomes 0.102 nm. You may also come across tables listing values in pm (picometres) which are 10<sup>-12</sup> m. A value in pm will look like, for example, for chlorine, 181 pm rather than 0.181 nm. Don't worry if you find this confusing. Just use the values you are given in whatever units you are given.</p><p>For comparison purposes, all the values relate to 6-co-ordinated ions (the same arrangement as in NaCl, for example). CsCl actually crystallises in an 8:8-co-ordinated structure – so you couldn't accurately use these values for CsCl. The 8-co-ordinated ionic radius for Cs is 0.174 nm rather than 0.167 for the 6-co-ordinated version.
</div>

<h4>Trends in ionic radius across a period</h4>

<div class="text-block">
<p>Let's look at the radii of the simple ions formed by elements as you go across Period 3 of the Periodic Table – the elements from Na to Cl.</p>
</div>

<table class="data-table">
<tbody>
<tr><th></th><th>Na⁺</th><th>Mg<sup>2+</sup></th><th>Al<sup>3+</sup></th><th>P<sup>3-</sup></th><th>S<sup>2-</sup></th><th>Cl⁻</th></tr>

<tr><td>no of protons</td><td>11</td><td>12</td><td>13</td><td>15</td><td>16</td><td>17</td></tr>

<tr><td>electronic structure of ion</td><td>2,8</td><td>2,8</td><td>2,8</td><td>2,8,8</td><td>2,8,8</td><td>2,8,8</td></tr>

<tr><td>ionic radius<br>/ nm</td><td>0.102</td><td>0.072</td><td>0.054</td><td>(0.212)</td><td>0.184</td><td>0.181</td></tr>
</tbody>
</table>

<div class="note">
<p>Note: The table misses out silicon which doesn't form a simple ion. The phosphide ion radius is in brackets because it comes from a different data source, and I am not sure whether it is safe to compare it. The values for the sulfide and chloride ions agree in the different source, so it is probably OK. The values are again for 6-co-ordination, although I can't guarantee that for the phosphide figure.</p>
</div>

<div class="text-block">
<p>First of all, notice the big jump in ionic radius as soon as you get into the negative ions. Is this surprising? Not at all – you have just added a whole extra layer of electrons.</p>
<p>Notice that, within the series of positive ions, and the series of negative ions, that the ionic radii fall as you go across the period. We need to look at the positive and negative ions separately.</p>
</div>

<h5>The positive ions</h5>

<div class="text-block">
<p>In each case, the ions have exactly the same electronic structure – they are said to be isoelectronic. However, the number of protons in the nucleus of the ions is increasing. That will tend to pull the electrons more and more towards the centre of the ion – causing the ionic radii to fall. That is pretty obvious!</p>
</div>

<h5>The negative ions</h5>

<div class="text-block">
<p>Exactly the same thing is happening here, except that you have an extra layer of electrons. What needs commenting on, though is how similar in size the sulfide ion and the chloride ion are. The additional proton here is making hardly any difference.</p>
<p>The difference between the size of similar pairs of ions actually gets even smaller as you go down Groups 6 and 7. For example, the Te<sup>2-</sup> ion is only 0.001 nm bigger than the I⁻ ion.</p>
<p>As far as I am aware there is no simple explanation for this – certainly not one which can be used at this level. This is a good illustration of what I said earlier – explaining things involving ionic radii in detail is sometimes very difficult.</p>
</div>

<h4>Trends in ionic radius for some more isoelectronic ions</h4>

<div class="text-block">
<p>This is only really a variation on what we have just been talking about, but fits negative and positive isoelectronic ions into the same series of results. Remember that isoelectronic ions all have exactly the same electron arrangement.</p>
</div>

<table class="data-table">
<tbody>
<tr><th></th><th>N<sup>3-</sup></th><th>O<sup>2-</sup></th><th>F⁻</th><th>Na⁺</th><th>Mg<sup>2+</sup></th><th>Al<sup>3+</sup></th></tr>

<tr><td>no of protons</td><td>7</td><td>8</td><td>9</td><td>11</td><td>12</td><td>13</td></tr>

<tr><td>electronic structure of ion</td><td>2, 8</td><td>2, 8</td><td>2, 8</td><td>2, 8</td><td>2, 8</td><td>2, 8</td></tr>

<tr><td>ionic radius<br>/ nm</td><td>(0.171)</td><td>0.140</td><td>0.133</td><td>0.102</td><td>0.072</td><td>0.054</td></tr>

</tbody>
</table>

<div class="note">
<p>Note: The nitride ion value is in brackets because it came from a different source, and I don't know for certain whether it relates to the same 6-co-ordination as the rest of the ions. This matters. My main source only gave a 4-co-ordinated value for the nitride ion, and that was 0.146 nm. You might also be curious as to how the neutral neon atom fits into this sequence. Its van der Waals radius is 0.154 or 0.160 nm (depending on which source you look the value up in) – bigger than the fluoride ion. You can't really sensibly compare a van der Waals radius with the radius of a bonded atom or ion.</p>
</div>

<div class="text-block">
<p>You can see that as the number of protons in the nucleus of the ion increases, the electrons get pulled in more closely to the nucleus. The radii of the isoelectronic ions therefore fall across this series.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-atradius.pdf" target="_blank">Questions on atomic and ionic radius</a>
<a href="../questions/a-atradius.pdf" target="_blank">Answers</a>
</div>

<h3>The Relative Sizes of Ions and Atoms</h3>

<div class="text-block">
<p>You probably won't have noticed, but nowhere in what you have read so far has there been any need to talk about the relative sizes of the ions and the atoms they have come from. Neither (as far as I can tell from the syllabuses) do any of the current UK-based exams for 16 – 18 year olds ask for this specifically in their syllabuses.</p>
<p>However, it is very common to find statements about the relative sizes of ions and atoms. I am fairly convinced that these statements are faulty, and I would like to attack the problem head-on rather than just ignoring it.</p>
</div>

<h3>Important!</h3>

<div class="text-block">
<p>For 10 years, until I rewrote this ionic radius section in August 2010, I included what is in the box below. You will find this same information and explanation in all sorts of books and on any number of websites aimed at this level. At least one non-UK A-level syllabus has a statement which specifically asks for this.</p>
</div>

<div class="principle">
<p>Ions aren't the same size as the atoms they come from. Compare the sizes of sodium and chloride ions with the sizes of sodium and chlorine atoms. Positive ions Positive ions are smaller than the atoms they come from. Sodium is 2,8,1; Na⁺ is 2,8. You've lost a whole layer of electrons, and the remaining 10 electrons are being pulled in by the full force of 11 protons. Negative ions Negative ions are bigger than the atoms they come from. Chlorine is 2,8,7; Cl⁻ is 2,8,8. Although the electrons are still all in the 3-level, the extra repulsion produced by the incoming electron causes the atom to expand. There are still only 17 protons, but they are now having to hold 18 electrons.</p>
</div>

<div class="text-block">
<p>However, I was challenged by an experienced teacher about the negative ion explanation, and that forced me to think about it carefully for the first time. I am now convinced that the facts and the explanation relating to negative ions are simply illogical.</p>
<p>As far as I can tell, no UK-based syllabus mentions the relative sizes of atoms and ions (as of August 2010), but you should check past papers and mark schemes to see whether questions have sneaked in.</p>
</div>

<h4>The rest of this page discusses the problems that I can see, and is really aimed at teachers and others, rather than at students.</h4>

<div class="text-block">
<p>If you are a student, look carefully at your syllabus, and past exam questions and mark schemes, to find out whether you need to know about this. If you don't need to know about it, stop reading now (unless, of course, you are interested in a bit of controversy!).</p>
<p>If you do need to know it, then you will have to learn what is in the box, even if, as I believe, it is wrong. If you like your chemistry to be simple, ignore the rest of the page, because you risk getting confused about what you need to know.</p>
<p>If you have expert knowledge of this topic, and can find any flaws in what I am saying, then please contact me via the address on the <a href="../../about.html">about this site</a> page.</p>
</div>

<h4>Choosing the right atomic radius to compare with</h4>

<div class="text-block">
<p>This is at the heart of the problem.</p>
<p>The diagrams in the box above, and similar ones that you will find elsewhere, use the metallic radius as the measure of atomic radius for metals, and the covalent radius for non-metals. I want to focus on the non-metals, because that is where the main problem lies.</p>
<p>You are, of course, perfectly free to compare the radius of an ion with whatever measure of atomic radius you choose. The problem comes in relating your choice of atomic radius to the "explanation" of the differences.</p>
<p>It is perfectly true that negative ions have radii which are significantly bigger than the covalent radius of the atom in question. And the argument then goes that the reason for this is that if you add one or more extra electrons to the atom, inter-electron repulsions cause the atom to expand. Therefore the negative ion is bigger than the atom.</p>
<p>This seems to me to be completely inconsistent. If you add one or more extra electrons to the atom, you aren't adding them to a covalently bound atom. You can't simply add electrons to a covalently-bound chlorine atom, for example – chlorine's existing electrons have reorganised themselves into new molecular orbitals which bind the atoms together.</p>
<p>In a covalently-bound atom, there is simply no room to add extra electrons.</p>
<p>So if you want to use the electron repulsion explanation, the implication is that you are adding the extra electrons to a raw atom with a simple uncombined electron arrangement.</p>
<p>In other words, if you were talking about, say, chlorine, you are adding an extra electron to chlorine with a configuration of 2,8,7 – not to covalently bound chlorine atoms in which the arrangement of the electrons has been altered by sharing.</p>
<p>That means that the comparison that you ought to be making isn't with the shortened covalent radius, but with the much larger van der Waals radius – the only available measure of the radius of an uncombined atom.</p>
<p>So what happens if you make that comparison?</p>
</div>

<h5>Group 7</h5>

<table class="convertable-table">
<thead>
<tr><th></th><th>vdW radius<br>/ nm</th><th>ionic radius of X⁻<br>/ nm</th></tr>
</thead>
<tbody>
<tr><td>F</td><td>0.147</td><td>0.133</td></tr>
<tr><td>Cl</td><td>0.175</td><td>0.181</td></tr>
<tr><td>Br</td><td>0.185</td><td>0.196</td></tr>
<tr><td>I</td><td>0.198</td><td>0.220</td></tr>
</tbody>
</table>

<h5>Group 6</h5>

<table class="convertable-table">
<thead>
<tr><th></th><th>vdW radius<br>/ nm</th><th>ionic radius of X²⁻<br>/ nm</th></tr>
</thead>
<tbody>
<tr><td>O</td><td>0.152</td><td>0.140</td></tr>
<tr><td>S</td><td>0.180</td><td>0.184</td></tr>
<tr><td>Se</td><td>0.190</td><td>0.198</td></tr>
<tr><td>Te</td><td>0.206</td><td>0.221</td></tr>
</tbody>
</table>

<h5>Group 5</h5>

<table class="convertable-table">
<thead>
<tr><th></th><th>vdW radius<br>/ nm</th><th>ionic radius of X³⁻<br>/ nm</th></tr>
</thead>
<tbody>
<tr><td>N</td><td>0.155</td><td>0.171</td></tr>
<tr><td>P</td><td>0.180</td><td>0.212</td></tr>
</tbody>
</table>

<div class="text-block">
<p>As we have already discussed above, measurements of ionic radii are full of uncertainties. That is also true of van der Waals radii. The table uses one particular set of values for comparison purposes. If you use data from different sources, you will find differences in the patterns – including which of the species (ion or atom) is bigger.</p>
<p>These ionic radius values are for 6-co-ordinated ions (with a slight question mark over the nitride and phosphide ion figures). But you may remember that I said that ionic radius changes with co-ordination. Nitrogen is a particularly good example of this.</p>
<p>4-co-ordinated nitride ions have a radius of 0.146 nm. In other words if you look at one of the co-ordinations, the nitride ion is bigger than the nitrogen atom; in the other case, it is smaller. Making a general statement that nitride ions are bigger or smaller than nitrogen atoms is impossible.</p>
</div>

<h4>So what is it safe to say about the facts?</h4>

<div class="text-block">
<p>For most, but not all, negative ions, the radius of the ion is bigger than that of the atom, but the difference is nothing like as great as is shown if you incorrectly compare ionic radii with covalent radii. There are also important exceptions.</p>
<p>I can't see how you can make any real generalisations about this, given the uncertainties in the data.</p>
</div>

<h4>And what is it safe to say about the explanation?</h4>

<div class="text-block">
<p>If there are any additional electron-electron repulsions on adding extra electrons, they must be fairly small. This is particularly shown if you consider some pairs of isoelectronic ions.</p>
<p>You would have thought that if repulsion was an important factor, then the radius of, say a sulfide ion, with two negative charges would be significantly larger than a chloride ion with only one. The difference should actually be even more marked, because the sulfide electrons are being held by only 16 protons rather than the 17 in the chlorine case.</p>
<p>On this repulsion theory, the sulfide ion shouldn't just be a little bit bigger than a chloride ion – it should be a lot bigger. The same effect is shown with selenide and bromide, and with telluride and iodide ions. In the last case, there is virtually no difference in the sizes of the 2- and 1- ions.</p>
<p>So if there is some repulsion playing a part in this, it certainly doesn't look as if it is playing a major part.</p>
</div>

<h4>What about positive ions?</h4>

<div class="text-block">
<p>Whether you choose to use van der Waals radii or metallic radii as a measure of the atomic radius, for metals the ionic radius is smaller than either, so the problem doesn't exist to the same extent. It is true that the ionic radius of a metal is less than its atomic radius (however vague you are about defining this).</p>
<p>The explanation (at least as long as you only consider positive ions from Groups 1, 2 and 3) in terms of losing a complete layer of electrons is also acceptable.</p>
</div>

<h4>Conclusion</h4>

<div class="text-block">
<p>It seems to me that, for negative ions, it is completely illogical to compare ionic radii with covalent radii if you want to use the electron repulsion explanation.</p>
<p>If you compare the ionic radii of negative ions with the van der Waals radii of the atoms they come from, the uncertainties in the data make it very difficult to make any reliable generalisations.</p>
<p>The similarity in sizes of pairs of isoelectronic ions from Groups 6 and 7 calls into question how important repulsion is in any explanation.</p>
<p>Having spent more than a week working on this, and discussing it with input from some very knowledgable people, I don't think there is any explanation which is simple enough to give to most students at this level. It would seem to me to be better that these ideas about relative sizes of atoms and ions are just dropped.</p>
<p>At this level, you can describe and explain simple periodic trends in atomic radii in the way I did further up this page, without even thinking about the relative sizes of the atoms and ions. Personally, I would be more than happy never to think about this again for the rest of my life!</p>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../propsmenu.html#top">To the atomic properties menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>