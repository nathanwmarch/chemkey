<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Successive Ionisation Energies | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A follow-on from the page on first ionisation energy. It explains the patterns shown by successive ionisation energies.">
<meta name="keywords" content="ionisation energy, ionisation energies, electronic structure, atomic orbital, orbital, orbitals, electron, electrons, periodic table">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Successive Ionisation Energies</h1>

<div class="text-block">
<p>This page explains what second, third, (etc) ionisation energy means, and then looks at patterns in successive ionisation energies for selected elements. It assumes that you understand about first ionisation energy.</p>
</div>

<div class="note">
<p>Important! If you have come straight to this page via a search engine, you should read the page on <a href="ies.html#top">first ionisation energy</a> before you go any further.</p>
</div>

<h3>Defining Second Ionisation Energy</h3>

<div class="text-block">
<p>Second ionisation energy is defined by the equation:</p>
</div>

<div class="block-formula">
X^+_{(g)} \longrightarrow X^{2+}_{(g)} + e^-
</div>

<div class="text-block">
<p>It is the energy needed to remove a second electron from each ion in 1 mole of gaseous 1+ ions to give gaseous 2+ ions.</p>
</div>

<h3>More ionisation energies</h3>

<div class="text-block">
<p>You can then have as many successive ionisation energies as there are electrons in the original atom.</p>
<p>The first four ionisation energies of aluminium, for example, are given by</p>
</div>

<div class="block-formula" label="first ionisation of aluminium">
Al_{(g)} \xrightarrow{{+}577~kJ~mol^{-1}} Al^+_{(g)} + e^-
</div>

<div class="block-formula" label="second ionisation of aluminium">
Al^+_{(g)} \xrightarrow{{+}1820~kJ~mol^{-1}} Al^{2+}_{(g)} + e^-
</div>

<div class="block-formula" label="third ionisation of aluminium">
Al^{2+}_{(g)} \xrightarrow{{+}2740~kJ~mol^{-1}} Al^{3+}_{(g)} + e^-
</div>

<div class="block-formula" label="third ionisation of aluminium">
Al^{3+}_{(g)} \xrightarrow{{+}11600~kJ~mol^{-1}} Al^{4+}_{(g)} + e^-
</div>

<div class="text-block">
<p>In order to form an Al<sup>3+</sup><sub>(g)</sub> ion from Al<sub>(g)</sub> you would have to supply:</p>
<p>577 + 1820 + 2740 = 5137 kJ mol<sup>-1</sup></p>
<p>That's a lot of energy. Why, then, does aluminium form Al<sup>3+</sup> ions?</p>
<p>It can only form them if it can get that energy back from somewhere, and whether that's feasible depends on what it is reacting with.</p>
<p>For example, if aluminium reacts with fluorine or oxygen, it can recover that energy in various changes involving the fluorine or oxygen – and so aluminium fluoride or aluminium oxide contain Al<sup>3+</sup> ions.</p>
<p>If it reacts with chlorine, it can't recover sufficient energy, and so solid anhydrous aluminium chloride isn't actually ionic – instead, it forms covalent bonds.</p>
<p>Why doesn't aluminium form an Al<sup>4+</sup> ion? The fourth ionisation energy is huge compared with the first three, and there is nothing that aluminium can react with which would enable it to recover that amount of extra energy.</p>
</div>

<h4>Why do successive ionisation energies get larger?</h4>

<div class="text-block">
<p>Once you have removed the first electron you are left with a positive ion. Trying to remove a negative electron from a positive ion is going to be more difficult than removing it from an atom. Removing an electron from a 2+ or 3+ (etc) ion is going to be progressively more difficult.</p>
</div>

<h4>Why is the fourth ionisation energy of aluminium so large?</h4>

<div class="text-block">
<p>The electronic structure of aluminium is 1s<sup>2</sup>2s<sup>2</sup>2p<sup>6</sup>3s<sup>2</sup>3p<sub>x</sub><sup>1</sup>. The first three electrons to be removed are the three electrons in the 3p and 3s orbitals. Once they've gone, the fourth electron is removed from the 2p level – much closer to the nucleus, and only screened by the 1s<sup>2</sup> (and to some extent the 2s<sup>2</sup>) electrons.</p>
</div>

<h3>Using Ionisation Energies to Work Out Which Group an Element is In</h3>

<div class="text-block">
<p>This big jump between two successive ionisation energies is typical of suddenly breaking in to an inner level. You can use this to work out which group of the Periodic Table an element is in from its successive ionisation energies.</p>
<p>Magnesium (1s<sup>2</sup>2s<sup>2</sup>2p<sup>6</sup>3s<sup>2</sup>) is in group 2 of the Periodic Table and has successive ionisation energies:</p>
</div>

<div class="image-container">
<img src="mgies.GIF">
</div>

<div class="text-block">
<p>Here the big jump occurs after the second ionisation energy. It means that there are 2 electrons which are relatively easy to remove (the 3s<sup>2</sup> electrons), while the third one is much more difficult (because it comes from an inner level – closer to the nucleus and with less screening).</p>
<p>Silicon (1s<sup>2</sup>2s<sup>2</sup>2p<sup>6</sup>3s<sup>2</sup>3p<sub>x</sub><sup>1</sup>3p<sub>y</sub><sup>1</sup>) is in group 4 of the Periodic Table and has successive ionisation energies:</p>
</div>

<div class="image-container">
<img src="siies.GIF">
</div>

<div class="text-block">
<p>Here the big jump comes after the fourth electron has been removed. The first 4 electrons are coming from the 3-level orbitals; the fifth from the 2-level.</p>
<h4>The lesson from all this:</h4>
<p>Count the easy electrons – those up to (but not including) the big jump. That is the same as the group number.</p>
<h4>Another example:</h4>
<p>Decide which group an atom is in if it has successive ionisation energies:</p>
</div>

<div class="image-container">
<img src="pies.GIF">
</div>

<div class="text-block">
<p>The ionisation energies are going up one or two thousand at a time for the first five. Then there is a huge jump of about 15000. There are 5 relatively easy electrons – so the element is in group 5.</p>
</div>

<h3>Exploring the Patterns in More Detail</h3>

<div class="text-block">
<p>If you plot graphs of successive ionisation energies for a particular element, you can see the fluctuations in it caused by the different electrons being removed.</p>
<p>Not only can you see the big jumps in ionisation energy when an electron comes from an inner level, but you can also see the minor fluctuations within a level depending on whether the electron is coming from an s or a p orbital, and even whether it is paired or unpaired in that orbital.</p>
<p>Chlorine has the electronic structure 1s<sup>2</sup>2s<sup>2</sup>2p<sup>6</sup>3s<sup>2</sup>3p<sub>x</sub><sup>2</sup>3p<sub>y</sub><sup>2</sup>3p<sub>z</sub><sup>1</sup>.</p>
<p>This graph plots the first eight ionisation energies of chlorine. The green labels show which electron is being removed for each of the ionisation energies.</p>
</div>

<div class="image-container">
<img src="clies1to8.GIF">
</div>

<div class="text-block">
<p>If you put a ruler on the first and second points to establish the trend, you'll find that the third, fourth and fifth points lie above the value you would expect. That is because the first two electrons are coming from pairs in the 3p levels and are therefore rather easier to remove than if they were unpaired.</p>
<p>Again, if you put a ruler on the 3rd, 4th and 5th points to establish their trend, you'll find that the 6th and 7th points lie well above the values you would expect from a continuation of the trend. That is because the 6th and 7th electrons are coming from the 3s level – slightly closer to the nucleus and slightly less well screened.</p>
<p>The massive jump as you break into the inner level at the 8th electron is fairly obvious!</p>
</div>

<div class="note">
<p>Warning! People sometimes get confused with these graphs because they forget that they are removing electrons from the atom. For example, the first point refers to the first electron being lost – from a 3p orbital. Basically, you start from the outside of the atom and work in towards the middle. If you start from the 1s orbital and work outwards, you are doomed to failure!</p>
</div>

<div class="text-block">
<p>To plot any more ionisation energies for chlorine needs a change of vertical scale. The seventeenth ionisation energy of chlorine is nearly 400,000 kJ mol<sup>-1</sup>, and the vertical scale has to be squashed to accommodate this.</p>
</div>

<div class="image-container">
<img src="clies1to17.GIF">
</div>

<div class="text-block">
<p>This is now a "log graph" – plotted by finding the logarithm of each ionisation energy (press the "log" button on your calculator). This doesn't simply squash the vertical scale. It distorts it as well, to such an extent that the only useful thing the graph now shows is the major jumps where the next electron to be removed comes from an inner level. The distortion is so great in the first 8 ionisation energies, for example, that the patterns shown by the previous graph are completely (and misleadingly) destroyed.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-moreies.pdf" target="_blank">Questions on successive ionisation energies</a>
<a href="../questions/a-moreies.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../propsmenu.html#top">To the atomic properties menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>