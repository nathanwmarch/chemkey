<!DOCTYPE html>
<html>
<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>First Ionisation Energy | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Describes and explains how first ionisation energies vary around the Periodic Table">
<meta name="keywords" content="ionisation energy, ionisation energies, electronic structure, atomic orbital, orbital, orbitals, electron, electrons, periodic table, periodicity">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">
<h1>First Ionisation Energy</h1>

<div class="text-block">
<p>This page explains what first ionisation energy is, and then looks at the way it varies around the Periodic Table – across periods and down groups. It assumes that you know about simple atomic orbitals, and can write electronic structures for simple atoms. You will find a link at the bottom of the page to a similar description of successive ionisation energies (second, third and so on).</p>
</div>

<div class="note">
<p>Important! If you aren't reasonable happy about <a href="atomorbs.html#top">atomic orbitals</a> and <a href="elstructs.html#top">electronic structures</a> you should follow these links before you go any further.</p>
</div>

<h2>Defining First Ionisation Energy</h2>

<div class="definition">
<p>The first ionisation energy is the energy required to remove one mole of the most loosely held electrons from one mole of gaseous atoms to produce 1 mole of gaseous ions each with a charge of 1+.</p>
</div>

<div class="text-block">
<p>This is more easily seen in symbol terms.</p>
</div>

<div class="block-formula">
\text{X}_{(g)} \longrightarrow \text{X}^+_{(g)} + \text{e}^-
</div>

<div class="text-block">
<p>It is the energy needed to carry out this change per mole of X.</p>
</div>

<div class="note">
<p>Worried about moles? Don't be! For now, just take it as a measure of a particular amount of a substance. It isn't worth worrying about at the moment.</p>
</div>

<h3>Things to Notice About the Equation</h3>

<div class="text-block">
<p>The state symbols – (g) – are essential. When you are talking about ionisation energies, everything must be present in the gas state.</p>
<p>Ionisation energies are measured in kJ mol<sup>-1</sup> (kilojoules per mole). They vary in size from 381 (which you would consider very low) up to 2370 (which is very high).</p>
<p>All elements have a first ionisation energy – even atoms which don't form positive ions in test tubes. The reason that helium (1st I.E. = 2370 kJ mol<sup>-1</sup>) doesn't normally form a positive ion is because of the huge amount of energy that would be needed to remove one of its electrons.</p>
</div>

<h2>Patterns of First Ionisation Energies in the Periodic Table</h2>

<h3>The First 20 Elements</h3>

<style>
table.one-center td:first-of-type {
text-align: center;
}

table.one-right td:first-of-type {
text-align: right;
}

table.one-left td:first-of-type {
text-align: left;
}

table.two-center td:nth-of-type(2) {
text-align: center;
}

table.two-right td:nth-of-type(2) {
text-align: right;
}

table.two-left td:nth-of-type(2) {
text-align: left;
}
</style>

<table class="convertable-table one-center two-right">
<thead>
<tr><th>element symbol</th><th>first ionisation energy<br>/ kJ mol<sup>-1</sup></th></tr>
</thead>
<tbody>
<tr>
<td>H</td>
<td>1312.0</td>
</tr>
<tr>
<td>He</td>
<td>2372.3</td>
</tr>
<tr>
<td>Li</td>
<td>520.2</td>
</tr>
<tr>
<td>Be</td>
<td>899.5</td>
</tr>
<tr>
<td>B</td>
<td>800.6</td>
</tr>
<tr>
<td>C</td>
<td>1086.5</td>
</tr>
<tr>
<td>N</td>
<td>1402.3</td>
</tr>
<tr>
<td>O</td>
<td>1313.9</td>
</tr>
<tr>
<td>F</td>
<td>1681.0</td>
</tr>
<tr>
<td>Ne</td>
<td>2080.7</td>
</tr>
<tr>
<td>Na</td>
<td>495.8</td>
</tr>
<tr>
<td>Mg</td>
<td>737.7</td>
</tr>
<tr>
<td>Al</td>
<td>577.5</td>
</tr>
<tr>
<td>Si</td>
<td>786.5</td>
</tr>
<tr>
<td>P</td>
<td>1011.8</td>
</tr>
<tr>
<td>S</td>
<td>999.6</td>
</tr>
<tr>
<td>Cl</td>
<td>1251.2</td>
</tr>
<tr>
<td>Ar</td>
<td>1520.6</td>
</tr>
<tr>
<td>K</td>
<td>418.8</td>
</tr>
<tr>
<td>Ca</td>
<td>589.8</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>First ionisation energy shows periodicity. That means that it varies in a repetitive way as you move through the Periodic Table. For example, look at the pattern from Li to Ne, and then compare it with the identical pattern from Na to Ar.</p>
<p>These variations in first ionisation energy can all be explained in terms of the structures of the atoms involved.</p>
</div>

<h3>Factors Affecting the Size of Ionisation Energy</h3>

<div class="text-block">
<p>Ionisation energy is a measure of the energy needed to pull a particular electron away from the attraction of the nucleus. A high value of ionisation energy shows a high attraction between the electron and the nucleus.</p>
<p>The size of that attraction will be governed by:</p>
</div>

<h4>The charge on the nucleus</h4>

<div class="text-block">
<p>The more protons there are in the nucleus, the more positively charged the nucleus is, and the more strongly electrons are attracted to it.</p>
</div>

<h4>The distance of the electron from the nucleus</h4>

<div class="text-block">
<p>Attraction falls off very rapidly with distance. An electron close to the nucleus will be much more strongly attracted than one further away.</p>
</div>

<h4>The number of electrons between the outer electrons and the nucleus</h4>

<div class="text-block">
<p>Consider a sodium atom, with the electronic structure 2,8,1. (There's no reason why you can't use this notation if it's useful!)</p>
<p>If the outer electron looks in towards the nucleus, it doesn't see the nucleus sharply. Between it and the nucleus there are the two layers of electrons in the first and second levels. The 11 protons in the sodium's nucleus have their effect cut down by the 10 inner electrons. The outer electron therefore only feels a net pull of approximately 1+ from the centre. This lessening of the pull of the nucleus by inner electrons is known as screening or shielding.</p>
</div>

<div class="note">
<p>Warning! Electrons don't, of course, "look in" towards the nucleus – and they don't "see" anything either! But there's no reason why you can't imagine it in these terms if it helps you to visualise what's happening. Just don't use these terms in an exam! You may get an examiner who is upset by this sort of loose language.</p>
</div>

<h4>Whether the electron is on its own in an orbital or paired with another electron</h4>

<div class="text-block">
<p>Two electrons in the same orbital experience a bit of repulsion from each other. This offsets the attraction of the nucleus, so that paired electrons are removed rather more easily than you might expect.</p>
</div>

<h3>Explaining the Pattern in the First Few Elements</h3>

<div class="text-block">
<p>Hydrogen has an electronic structure of 1s<sup>1</sup>. It is a very small atom, and the single electron is close to the nucleus and therefore strongly attracted. There are no electrons screening it from the nucleus and so the ionisation energy is high (1310 kJ mol<sup>-1</sup>).</p>
<p>Helium has a structure 1s<sup>2</sup>. The electron is being removed from the same orbital as in hydrogen's case. It is close to the nucleus and unscreened. The value of the ionisation energy (2370 kJ mol<sup>-1</sup>) is much higher than hydrogen, because the nucleus now has 2 protons attracting the electrons instead of 1.</p>
<p>Lithium is 1s<sup>2</sup>2s<sup>1</sup>. Its outer electron is in the second energy level, much more distant from the nucleus. You might argue that that would be offset by the additional proton in the nucleus, but the electron doesn't feel the full pull of the nucleus – it is screened by the 1s<sup>2</sup> electrons.</p>
</div>

<div class="image-container">
<img src="hevli.GIF">
</div>

<div class="text-block">
<p>You can think of the electron as feeling a net 1+ pull from the centre (3 protons offset by the two 1s<sup>2</sup> electrons).</p>
<p>If you compare lithium with hydrogen (instead of with helium), the hydrogen's electron also feels a 1+ pull from the nucleus, but the distance is much greater with lithium. Lithium's first ionisation energy drops to 519 kJ mol<sup>-1</sup> whereas hydrogen's is 1310 kJ mol<sup>-1</sup>.</p>
</div>

<h3>The Patterns in Periods 2 and 3</h3>

<div class="text-block">
<p>Talking through the next 17 atoms one at a time would take ages. We can do it much more neatly by explaining the main trends in these periods, and then accounting for the exceptions to these trends.</p>
<p>The first thing to realise is that the patterns in the two periods are identical – the difference being that the ionisation energies in period 3 are all lower than those in period 2.</p>
</div>

<div class="image-container">
<img src="p2vp3.GIF">
</div>

<h4>Explaining the general trend across periods 2 and 3</h4>

<div class="text-block">
<p>The general trend is for ionisation energies to increase across a period.</p>
<p>In the whole of period 2, the outer electrons are in 2-level orbitals – 2s or 2p. These are all the same sort of distances from the nucleus, and are screened by the same 1s<sup>2</sup> electrons.</p>
<p>The major difference is the increasing number of protons in the nucleus as you go from lithium to neon. That causes greater attraction between the nucleus and the electrons and so increases the ionisation energies. In fact the increasing nuclear charge also drags the outer electrons in closer to the nucleus. That increases ionisation energies still more as you go across the period.</p>
</div>

<div class="note">
<p>Note: Factors affecting <a href="atradius.html#top">atomic radius</a> are covered on a separate page.
</div>

<div class="text-block">
<p>In period 3, the trend is exactly the same. This time, all the electrons being removed are in the third level and are screened by the 1s<sup>2</sup>2s<sup>2</sup>2p<sup>6</sup> electrons. They all have the same sort of environment, but there is an increasing nuclear charge.</p>
</div>

<h4>Why the drop between groups 2 and 3 (Be-B and Mg-Al)?</h4>

<div class="text-block">
<p>The explanation lies with the structures of boron and aluminium. The outer electron is removed more easily from these atoms than the general trend in their period would suggest.</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>Be</td>
<td><span class="inline-formula">1s^22s^2</span></td>
<td><span class="inline-formula">\text{1}^\text{st}~\text{I.E.} = 900~kJ~mol^{-1}</span></td>
</tr>
<tr>
<td>B</td>
<td><span class="inline-formula">1s^22s^22p_x^1</span></td>
<td><span class="inline-formula">\text{1}^\text{st}~\text{I.E.} = 799~kJ~mol^{-1}</span></td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>You might expect the boron value to be more than the beryllium value because of the extra proton. Offsetting that is the fact that boron's outer electron is in a 2p orbital rather than a 2s. 2p orbitals have a slightly higher energy than the 2s orbital, and the electron is, on average, to be found further from the nucleus. This has two effects.</p>
</div>

<ul>
<li>The increased distance results in a reduced attraction and so a reduced ionisation energy.</li>
<li>The 2p orbital is screened not only by the 1s<sup>2</sup> electrons but, to some extent, by the 2s<sup>2</sup> electrons as well. That also reduces the pull from the nucleus and so lowers the ionisation energy.</li>
</ul>

<div class="text-block">
<p>The explanation for the drop between magnesium and aluminium is the same, except that everything is happening at the 3-level rather than the 2-level.</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>Mg</td>
<td><span class="inline-formula">1s^22s^22p^63s^2</span></td>
<td><span class="inline-formula">\text{1}^\text{st}~\text{I.E.} = 736~kJ~mol^{-1}</span></td>
</tr>
<tr>
<td>Al</td>
<td><span class="inline-formula">1s^22s^22p^63s^23p_x^1</span></td>
<td><span class="inline-formula">\text{1}^\text{st}~\text{I.E.} = 577~kJ~mol^{-1}</span></td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>The 3p electron in aluminium is slightly more distant from the nucleus than the 3s, and partially screened by the 3s<sup>2</sup> electrons as well as the inner electrons. Both of these factors offset the effect of the extra proton.</p>
</div>

<div class="note">
<p>Warning! You might possibly come across a text book which describes the drop between group 2 and group 3 by saying that a full s<sup>2</sup> orbital is in some way especially stable and that makes the electron more difficult to remove. In other words, that the fluctuation is because the group 2 value for ionisation energy is abnormally high. This is quite simply wrong! The reason for the fluctuation is because the group 3 value is lower than you might expect for the reasons we've looked at.</p>
</div>

<h4>Why the drop between groups 5 and 6 (N-O and P-S)?</h4>

<div class="text-block">
<p>Once again, you might expect the ionisation energy of the group 6 element to be higher than that of group 5 because of the extra proton. What is offsetting it this time?</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>N</td>
<td><span class="inline-formula">1s^22s^22p_x^12p_y^12p_z^1</span></td>
<td><span class="inline-formula">\text{1}^\text{st}~\text{I.E.} = 1400~kJ~mol^{-1}</span></td>
</tr>
<tr>
<td>O</td>
<td><span class="inline-formula">1s^22s^22p_x^22p_y^12p_z^1</span></td>
<td><span class="inline-formula">\text{1}^\text{st}~\text{I.E.} = 1310~kJ~mol^{-1}</span></td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>The screening is identical (from the 1s<sup>2</sup> and, to some extent, from the 2s<sup>2</sup> electrons), and the electron is being removed from an identical orbital.</p>
<p>The difference is that in the oxygen case the electron being removed is one of the 2p<sub>x</sub><sup>2</sup> pair. The repulsion between the two electrons in the same orbital means that the electron is easier to remove than it would otherwise be.</p>
<p>The drop in ionisation energy at sulfur is accounted for in the same way.</p>
</div>

<div class="note">
<p>Note: After oxygen or sulfur, the ionisation energies of the next two elements increase because of the additional protons. Everything else is the same – the type of orbital that the new electron is going into, the screening, and the fact that it is pairing up with an existing electron.</p>
<p>Students sometimes wonder why the next ionisation energies don't fall because of the repulsion caused by the electrons pairing up, in the same way it falls between, say, nitrogen and oxygen.</p>
<p>Between nitrogen and oxygen, the pairing up is a new factor, and the repulsion outweighs the effect of the extra proton. But between oxygen and fluorine the pairing up isn't a new factor, and the only difference in this case is the extra proton. So relative to oxygen, the ionisation energy of fluorine is greater. And, similarly, the ionisation energy of neon is greater still.</p>
</div>

<h3>Trends in Ionisation Energy Down a Group</h3>

<div class="text-block">
<p>As you go down a group in the Periodic Table ionisation energies generally fall. You have already seen evidence of this in the fact that the ionisation energies in period 3 are all less than those in period 2.</p>
<p>Taking Group 1 as a typical example:</p>
</div>

<div class="image-container">
<img src="iesgp1.GIF">
</div>

<div class="text-block">
<p>Why is the sodium value less than that of lithium?</p>
<p>There are 11 protons in a sodium atom but only 3 in a lithium atom, so the nuclear charge is much greater. You might have expected a much larger ionisation energy in sodium, but offsetting the nuclear charge is a greater distance from the nucleus and more screening.</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>Li</td>
<td><span class="inline-formula">1s^22s^1</span></td>
<td><span class="inline-formula">\text{1}^\text{st}~\text{I.E.} = 519~kJ~mol^{-1}</span></td>
</tr>
<tr>
<td>Na</td>
<td><span class="inline-formula">1s^22s^22p^63s^1</span></td>
<td><span class="inline-formula">\text{1}^\text{st}~\text{I.E.} = 494~kJ~mol^{-1}</span></td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>Lithium's outer electron is in the second level, and only has the 1s<sup>2</sup> electrons to screen it. The 2s<sup>1</sup> electron feels the pull of 3 protons screened by 2 electrons – a net pull from the centre of 1+.</p>
<p>The sodium's outer electron is in the third level, and is screened from the 11 protons in the nucleus by a total of 10 inner electrons. The 3s<sup>1</sup> electron also feels a net pull of 1+ from the centre of the atom. In other words, the effect of the extra protons is compensated for by the effect of the extra screening electrons. The only factor left is the extra distance between the outer electron and the nucleus in sodium's case. That lowers the ionisation energy.</p>
<p>Similar explanations hold as you go down the rest of this group – or, indeed, any other group.</p>
</div>

<h3>Trends in Ionisation Energy in a Transition Series</h3>

<div class="image-container">
<img src="iesdblock.GIF">
</div>

<div class="text-block">
<p>Apart from zinc at the end, the other ionisation energies are all much the same.</p>
<p>All of these elements have an electronic structure [Ar]3d<sup>n</sup>4s<sup>2</sup> (or 4s<sup>1</sup> in the cases of chromium and copper). The electron being lost always comes from the 4s orbital.</p>
</div>

<div class="note">
<p>Note: The 4s orbital has a higher energy than the 3d in the transition elements. That means that it is a 4s electron which is lost from the atom when it forms an ion. It also means that the 3d orbitals are slightly closer to the
nucleus than the 4s – and so offer some screening.</p>
<p>Confusingly, this is inconsistent with what we say when we use the Aufbau Principle to work out the electronic structures of atoms.</p>
<p>I have discussed this in detail in the page about <a href="3d4sproblem.html#top">the order of filling 3d and 4s orbitals</a>.</p>
<p>If you are a teacher or a very confident student then you might like to follow this link.</p>
<p>If you aren't so confident, or are coming at this for the first time, I suggest that you ignore it. Remember that the Aufbau Principle (which uses the assumption that the 3d orbitals fill after the 4s) is just a useful way of working out the structures of atoms, but that in real transition metal atoms the 4s is actually the outer, higher energy orbital.</p>
</div>

<div class="text-block">
<p>As you go from one atom to the next in the series, the number of protons in the nucleus increases, but so also does the number of 3d electrons. The 3d electrons have some screening effect, and the extra proton and the extra 3d electron more or less cancel each other out as far as attraction from the centre of the atom is concerned.</p>
<p>The rise at zinc is easy to explain.</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>Cu</td>
<td><span class="inline-formula">[Ar]3d^{10}4s^1</span></td>
<td><span class="inline-formula">\text{1}^\text{st}~\text{I.E.} = 745~kJ~mol^{-1}</span></td>
</tr>
<tr>
<td>Zn</td>
<td><span class="inline-formula">[Ar]3d^{10}4s^2</span></td>
<td><span class="inline-formula">\text{1}^\text{st}~\text{I.E.} = 908~kJ~mol^{-1}</span></td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>In each case, the electron is coming from the same orbital, with identical screening, but the zinc has one extra proton in the nucleus and so the attraction is greater. There will be a degree of repulsion between the paired up electrons in the 4s orbital, but in this case it obviously isn't enough to outweigh the effect of the extra proton.</p>
</div>

<div class="note">
<p>Note: This is actually very similar to the increase from, say, sodium to magnesium in the third period. In that case, the outer electronic structure is going from 3s<sup>1</sup> to 3s<sup>2</sup>. Despite the pairing-up of the electrons, the ionisation energy increases because of the extra proton in the nucleus. The repulsion between the 3s electrons obviously isn't enough to outweigh this either.</p>
<p>I don't know why the repulsion between the paired electrons matters less for electrons in s orbitals than in p orbitals (I don't even know whether you can make that generalisation!). I suspect that it has to do with orbital shape and possibly the greater penetration of s electrons towards the nucleus, but I haven't been able to find any reference to this anywhere. In fact, I haven't been able to find anyone who even mentions repulsion in the context of paired s electrons!</p>
<p>If you have any hard information on this, could you contact me via the address on the
<a href="../../about.html#top">about this site</a> page.</p>
</div>

<h2>Ionisation Energies and Reactivity</h2>

<div class="text-block">
<p>The lower the ionisation energy, the more easily this change happens:</p>
</div>

<div class="block-formula">
\text{X}_{(g)} \longrightarrow \text{X}^+_{(g)} + \text{e}^-
</div>

<div class="text-block">
<p>You can explain the increase in reactivity of the Group 1 metals (Li, Na, K, Rb, Cs) as you go down the group in terms of the fall in ionisation energy. Whatever these metals react with, they have to form positive ions in the process, and so the lower the ionisation energy, the more easily those ions will form.</p>
<p>The danger with this approach is that the formation of the positive ion is only one stage in a multi-step process.</p>
<p>For example, you wouldn't be starting with gaseous atoms; nor would you end up with gaseous positive ions – you would end up with ions in a solid or in solution. The energy changes in these processes also vary from element to element. Ideally you need to consider the whole picture and not just one small part of it.</p>
<p>However, the ionisation energies of the elements are going to be major contributing factors towards the activation energy of the reactions. Remember that activation energy is the minimum energy needed before a reaction will take place. The lower the activation energy, the faster the reaction will be – irrespective of what the overall energy changes in the reaction are.</p>
<p>The fall in ionisation energy as you go down a group will lead to lower activation energies and therefore faster reactions.</p>
</div>

<div class="note">
<p>Note: You will find a page discussing this in more detail in the inorganic section of this site dealing with the <a href="../../inorganic/group2/reacth2o.html#top">reactions of Group 2 metals with water</a>.
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-firsties.pdf" target="_blank">Questions on first ionisation energies</a>
<a href="../questions/a-firsties.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="moreies.html#top">To look at second (and successive) ionisation energies</a>
<a href="../propsmenu.html#top">To the atomic properties menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>