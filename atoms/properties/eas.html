<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Electron Affinity | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Defines electron affinity and explains the factors that govern its size">
<meta name="keywords" content="electron affinity, electron affinities, electronic structure, atomic orbital, orbitals, electron, electrons, periodic table, periodicity">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Electron Affinity</h1>

<div class="text-block">
<p>This page explains what electron affinity is, and then looks at the factors that affect its size. It assumes that you know about simple atomic orbitals, and can write electronic structures for simple atoms.</p>
</div>

<div class="note">
<p>Important! If you aren't reasonable happy about <a href="atomorbs.html#top">atomic orbitals</a> and <a href="elstructs.html#top">electronic structures</a> you should follow these links before you go any further.</p>
</div>

<h2>First Electron Affinity</h2>

<div class="text-block">
<p>Ionisation energies are always concerned with the formation of positive ions. Electron affinities are the negative ion equivalent, and their use is almost always confined to elements in groups 6 and 7 of the Periodic Table.</p>
</div>

<h3>Defining First Electron Affinity</h3>

<div class="definition">
<p>The first electron affinity is the energy released when 1 mole of gaseous atoms each acquire an electron to form 1 mole of gaseous 1- ions.</p>

</div>

<div class="text-block">
<p>This is more easily seen in symbol terms.</p>
</div>

<div class="block-formula">
\text{X}_{(g)} + \text{e}^- \longrightarrow \text{X}_{(g)}^-
</div>

<div class="text-block">
<p>It is the energy released (per mole of X) when this change happens.</p>
<p>First electron affinities have negative values. For example, the first electron affinity of chlorine is -349 kJ mol<sup>-1</sup>. By convention, the negative sign shows a release of energy.</p>
</div>

<h3>The First Electron Affinities of the Group 7 Elements</h3>

<table class="list-table">
<tbody>
<tr><td>F</td><td>-328 kJ mol<sup>-1</sup></td></tr>
<tr><td>Cl</td><td>-349 kJ mol<sup>-1</sup></td></tr>
<tr><td>Br</td><td>-324 kJ mol<sup>-1</sup></td></tr>
<tr><td>I</td><td>-295 kJ mol<sup>-1</sup></td></tr>
</tbody>
</table>

<div class="note">
<p>Note: These values are based on the most recent research. If you are using a different data source, you may have slightly different numbers. That doesn't matter – the pattern will still be the same.</p>
</div>

<h4>Is there a pattern?</h4>

<div class="text-block">
<p>Yes – as you go down the group, first electron affinities become less (in the sense that less energy is evolved when the negative ions are formed). Fluorine breaks that pattern, and will have to be accounted for separately.</p>
<p>The electron affinity is a measure of the attraction between the incoming electron and the nucleus – the stronger the attraction, the more energy is released.</p>
<p>The factors which affect this attraction are exactly the same as those relating to ionisation energies – nuclear charge, distance and screening.</p>
</div>

<div class="note">
<p>Note: If you haven't read about <a href="ies.html#top">ionisation energy</a> recently, it might be a good idea to follow this link before you go on. These factors are discussed in more detail on that page than they are on this one.</p>
</div>

<div class="text-block">
<p>The increased nuclear charge as you go down the group is offset by extra screening electrons. Each outer electron in effect feels a pull of 7+ from the centre of the atom, irrespective of which element you are talking about.</p>
<p>For example, a fluorine atom has an electronic structure of 1s<sup>2</sup>2s<sup>2</sup>2p<sub>x</sub><sup>2</sup>2p<sub>y</sub><sup>2</sup>2p<sub>z</sub><sup>1</sup>. It has 9 protons in the nucleus.</p>
<p>The incoming electron enters the 2-level, and is screened from the nucleus by the two 1s<sup>2</sup> electrons. It therefore feels a net attraction from the nucleus of 7+ (9 protons less the 2 screening electrons).</p>
<p>By contrast, chlorine has the electronic structure 1s<sup>2</sup>2s<sup>2</sup>2p<sup>6</sup>3s<sup>2</sup>3p<sub>x</sub><sup>2</sup>3p<sub>y</sub><sup>2</sup>3p<sub>z</sub><sup>1</sup>. It has 17 protons in the nucleus.</p>
<p>But again the incoming electron feels a net attraction from the nucleus of 7+ (17 protons less the 10 screening electrons in the first and second levels).</p>
</div>

<div class="note">
<p>Note: If you want to be fussy, there is also a small amount of screening by the 2s electrons in fluorine and by the 3s electrons in chlorine. This will be approximately the same in both these cases and so doesn't affect the argument in any way (apart from complicating it!).</p>
</div>

<div class="text-block">
<p>The over-riding factor is therefore the increased distance that the incoming electron finds itself from the nucleus as you go down the group. The greater the distance, the less the attraction and so the less energy is released as electron affinity.</p>
</div>

<div class="note">
<p>Note: Comparing fluorine and chlorine isn't ideal, because fluorine breaks the trend in the group. However, comparing chlorine and bromine, say, makes things seem more difficult because of the more complicated electronic structures involved. What we have said so far is perfectly true and applies to the fluorine-chlorine case as much as to anything else in the group, but there's another factor which operates as well which we haven't considered yet – and that over-rides the effect of distance in the case of fluorine.</p>
</div>

<h4>Why is fluorine out of line?</h4>

<div class="text-block">
<p>The incoming electron is going to be closer to the nucleus in fluorine than in any other of these elements, so you would expect a high value of electron affinity.</p>
<p>However, because fluorine is such a small atom, you are putting the new electron into a region of space already crowded with electrons and there is a significant amount of repulsion. This repulsion lessens the attraction the incoming electron feels and so lessens the electron affinity.</p>
<p>A similar reversal of the expected trend happens between oxygen and sulfur in Group 6. The first electron affinity of oxygen (-142 kJ mol<sup>-1</sup>) is smaller than that of sulfur (-200 kJ mol<sup>-1</sup>) for exactly the same reason that fluorine's is smaller than chlorine's.</p>
</div>

<h3>Comparing Group 6 and Group 7 Values</h3>

<div class="text-block">
<p>As you might have noticed, the first electron affinity of oxygen (-142 kJ mol<sup>-1</sup>) is less than that of fluorine (-328 kJ mol<sup>-1</sup>). Similarly sulfur's (-200 kJ mol<sup>-1</sup>) is less than chlorine's (--349 kJ mol<sup>-1</sup>). Why?</p>
<p>It's simply that the Group 6 element has 1 less proton in the nucleus than its next door neighbour in Group 7. The amount of screening is the same in both.</p>
<p>That means that the net pull from the nucleus is less in Group 6 than in Group 7, and so the electron affinities are less.</p>
</div>

<h3>First Electron Affinity and Reactivity</h3>

<div class="text-block">
<p>The reactivity of the elements in group 7 falls as you go down the group – fluorine is the most reactive and iodine the least.</p>
<p>Often in their reactions these elements form their negative ions. At GCSE the impression is sometimes given that the fall in reactivity is because the incoming electron is held less strongly as you go down the group and so the negative ion is less likely to form. That explanation looks reasonable until you include fluorine!</p>
<p>An overall reaction will be made up of lots of different steps all involving energy changes, and you cannot safely try to explain a trend in terms of just one of those steps. Fluorine is much more reactive than chlorine (despite the lower electron affinity) because the energy released in other steps in its reactions more than makes up for the lower amount of energy released as electron affinity.</p>
</div>

<h2>Second Electron Affinity</h2>

<div class="text-block">
<p>You are only ever likely to meet this with respect to the group 6 elements, oxygen and sulfur which both form 2- ions.</p>
</div>

<h3>Defining Second Electron Affinity</h3>

<div class="principle">
<p>The second electron affinity is the energy required to add an electron to each ion in 1 mole of gaseous 1- ions to produce 1 mole of gaseous 2- ions.</p>
</div>

<div class="text-block">
<p>This is more easily seen in symbol terms.</p>
</div>

<div class="block-formula">
\text{X}_{(g)}^- + \text{e}^- \longrightarrow \text{X}_{(g)}^{2-}
</div>

<div class="text-block">
<p>It is the energy needed to carry out this change per mole of X^-.</p>
</div>

<h4>Why is energy needed to do this?</h4>

<div class="text-block">
<p>You are forcing an electron into an already negative ion. It's not going to go in willingly!</p>
</div>

<div class="block-formula" label="first electron affinity of oxygen">
\text{O}_{(g)} + \text{e}^- \xrightarrow{{-}142~kJ~mol^{-1}} \text{O}_{(g)}^-
</div>

<div class="block-formula" label="second electron affinity of oxygen">
\text{O}_{(g)}^- + \text{e}^- \xrightarrow{{+}844~kJ~mol^{-1}} \text{O}_{(g)}^{2-}
</div>

<div class="text-block">
<p>The positive sign shows that you have to put in energy to perform this change. The second electron affinity of oxygen is particularly high because the electron is being forced into a small, very electron-dense space.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-eas.pdf" target="_blank">Questions on electron affinites</a>
<a href="../questions/a-eas.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../propsmenu.html#top">To the atomic properties menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>