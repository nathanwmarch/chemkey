<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Atomic Orbitals | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains what an atomic orbital is, and looks at the various kinds of atomic orbital – s, p, d and f">
<meta name="keywords" content="electronic structure, atomic orbital, orbital, orbitals, electron, electrons, s, p, d, f">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Atomic Orbitals</h1>

<div class="text-block">
<p>This page explains what atomic orbitals are in a way that makes them understandable for introductory courses such as UK A-level and its equivalents. It explores s and p orbitals in some detail, including their shapes and energies. d orbitals are described only in terms of their energy, and f orbitals only get a passing mention.</p>
</div>

<h2>What is an Atomic Orbital?</h2>

<h3>Orbitals and Orbits</h3>

<div class="text-block">
<p>When a planet moves around the sun, you can plot a definite path for it which is called an orbit. A simple view of the atom looks similar and you may have pictured the electrons as orbiting around the nucleus. The truth is different, and electrons in fact inhabit regions of space known as orbitals.</p>
<p>Orbits and orbitals sound similar, but they have quite different meanings. It is essential that you understand the difference between them.</p>
</div>

<h3>The impossibility of Drawing Orbits for Electrons</h3>

<div class="text-block">
<p>To plot a path for something you need to know exactly where the object is and be able to work out exactly where it's going to be an instant later. You can't do this for electrons.</p>
<p>The Heisenberg Uncertainty Principle says – loosely – that you can't know with certainty both where an electron is and where it's going next. (What it actually says is that it is impossible to define with absolute precision, at the same time, both the position and the momentum of an electron.)</p>
<p>That makes it impossible to plot an orbit for an electron around a nucleus. Is this a big problem? No. If something is impossible, you have to accept it and find a way around it.</p>
</div>

<div class="note">
<p>Note: Over the years I have had a steady drip of questions from students in which it is obvious that they still think of electrons as orbiting around a nucleus – which is completely wrong! I have added a page about why the idea of <a href="orbitsorbitals.html#top">orbits</a> is wrong to try to avoid having to say the same thing over and over again!</p>
</div>

<h3>Hydrogen's Electron – the 1s Orbital</h3>

<div class="note">
<p>Note: In this diagram (and the orbital diagrams that follow), the nucleus is shown very much larger than it really is. This is just for clarity.
</div>

<div class="image-container">
<img src="1sorbital.GIF"> 
</div>

<div class="text-block">
<p>Suppose you had a single hydrogen atom and at a particular instant plotted the position of the one electron. Soon afterwards, you do the same thing, and find that it is in a new position. You have no idea how it got from the first place to the second.</p>
<p>You keep on doing this over and over again, and gradually build up a sort of 3D map of the places that the electron is likely to be found.</p>
<p>In the hydrogen case, the electron can be found anywhere within a spherical space surrounding the nucleus. The diagram shows a cross-section through this spherical space.</p>
<p>95% of the time (or any other percentage you choose), the electron will be found within a fairly easily defined region of space quite close to the nucleus. Such a region of space is called an orbital. You can think of an orbital as being the region of space in which the electron lives.</p>
</div>

<div class="note">
<p>Note: If you wanted to be absolutely 100% sure of where the electron is, you would have to draw an orbital the size of the Universe!</p>
</div>

<div class="text-block">
<p>What is the electron doing in the orbital? We don't know, we can't know, and so we just ignore the problem! All you can say is that if an electron is in a particular orbital it will have a particular definable energy.</p>
<p>Each orbital has a name.</p>
<p>The orbital occupied by the hydrogen electron is called a 1s orbital. The "1" represents the fact that the orbital is in the energy level closest to the nucleus. The "s" tells you about the shape of the orbital. s orbitals are spherically symmetric around the nucleus – in each case, like a hollow ball made of rather chunky material with the nucleus at its centre.</p>
</div>

<div class="image-container">
<img src="2sorbital.GIF">
</div>

<div class="text-block">
<p>The orbital on the left is a 2s orbital. This is similar to a 1s orbital except that the region where there is the greatest chance of finding the electron is further from the nucleus – this is an orbital at the second energy level.</p>
<p>If you look carefully, you will notice that there is another region of slightly higher electron density (where the dots are thicker) nearer the nucleus. ("Electron density" is another way of talking about how likely you are to find an electron at a particular place.)</p>
<p>2s (and 3s, 4s, etc) electrons spend some of their time closer to the nucleus than you might expect. The effect of this is to slightly reduce the energy of electrons in s orbitals. The nearer the nucleus the electrons get, the lower their energy.</p>
<p>3s, 4s (etc) orbitals get progressively further from the nucleus.</p>
</div>

<h3>p Orbitals</h3>

<div class="image-container">
<img src="porbital.GIF">
</div>

<div class="text-block">
<p>Not all electrons inhabit s orbitals (in fact, very few electrons live in s orbitals). At the first energy level, the only orbital available to electrons is the 1s orbital, but at the second level, as well as a 2s orbital, there are also orbitals called 2p orbitals.</p>
<p>A p orbital is rather like 2 identical balloons tied together at the nucleus. The diagram on the left is a cross-section through that 3-dimensional region of space. Once again, the orbital shows where there is a 95% chance of finding a particular electron.</p>
</div>

<div class="note">
<p>Taking chemistry further: If you imagine a horizontal plane through the nucleus, with one lobe of the orbital above the plane and the other beneath it, there is a zero probability of finding the electron on that plane. So how does the electron get from one lobe to the other if it can never pass through the plane of the nucleus? At this introductory level you just have to accept that it does! If you want to find out more, read about the wave nature of electrons.</p>
</div>

<div class="text-block">
<p>Unlike an s orbital, a p orbital points in a particular direction – the one drawn points up and down the page.</p>
<p>At any one energy level it is possible to have three absolutely equivalent p orbitals pointing mutually at right angles to each other. These are arbitrarily given the symbols p<sub>x</sub>, p<sub>y</sub> and p<sub>z</sub>. This is simply for convenience – what you might think of as the x, y or z direction changes constantly as the atom tumbles in space.</p>
</div>

<div class="image-container">
<img src="pxyandz.GIF">
</div>

<div class="text-block">
<p>The p orbitals at the second energy level are called 2p<sub>x</sub>, 2p<sub>y</sub> and 2p<sub>z</sub>. There are similar orbitals at subsequent levels – 3p<sub>x</sub>, 3p<sub>y</sub>, 3p<sub>z</sub>, 4p<sub>x</sub>, 4p<sub>y</sub>, 4p<sub>z</sub>> and so on.</p>
<p>All levels except for the first level have p orbitals. At the higher levels the lobes get more elongated, with the most likely place to find the electron more distant from the nucleus.</p>
</div>

<h3>d and f Orbitals</h3>

<div class="text-block">
<p>In addition to s and p orbitals, there are two other sets of orbitals which become available for electrons to inhabit at higher energy levels. At the third level, there is a set of five d orbitals (with complicated shapes and names) as well as the 3s and 3p orbitals (3p<sub>x</sub>, 3p<sub>y</sub>, 3p<sub>z</sub>). At the third level there are a total of nine orbitals altogether.</p>
<p>At the fourth level, as well the 4s and 4p and 4d orbitals there are an additional seven f orbitals – 16 orbitals in all. s, p, d and f orbitals are then available at all higher energy levels as well.</p>
<p>For the moment, you need to be aware that there are sets of five d orbitals at levels from the third level upwards, but you probably won't be expected to draw them or name them. Apart from a passing reference, you won't come across f orbitals at all.</p>
</div>

<div class="note">
<p>Note: Some UK-based syllabuses will eventually want you to be able to draw, or at least recognise, the shapes of d orbitals. I am not including them now because I don't want to add confusion to what is already a difficult introductory topic. Check your <a href="../../syllabuses.html#top">syllabus and past papers</a> to find out what you need to know. If you are a studying a UK-based syllabus and haven't got these, follow this link to find out how to get hold of them.</p>
</div>

<h2>Fitting Electrons into Orbitals</h2>

<div class="text-block">
<p>You can think of an atom as a very bizarre house (like an inverted pyramid!) – with the nucleus living on the ground floor, and then various rooms (orbitals) on the higher floors occupied by the electrons. On the first floor there is only 1 room (the 1s orbital); on the second floor there are 4 rooms (the 2s, 2p<sub>x</sub>, 2p<sub>y</sub> and 2p<sub>z</sub> orbitals); on the third floor there are 9 rooms (one 3s orbital, three 3p orbitals and five 3d orbitals); and so on. But the rooms aren't very big. Each orbital can only hold 2 electrons.</p>
<p>A convenient way of showing the orbitals that the electrons live in is to draw "electrons-in-boxes".</p>
</div>

<h3>"Electrons-in-boxes"</h3>

<div class="text-block">
<p>Orbitals can be represented as boxes with the electrons in them shown as arrows. Often an up-arrow and a down-arrow are used to show that the electrons are in some way different.</p>
</div>

<div class="note">
<p>Taking chemistry further: The need to have all electrons in an atom different comes out of quantum theory. If they live in different orbitals, that's fine – but if they are both in the same orbital there has to be some subtle distinction between them. Quantum theory allocates them a property known as "spin" – which is what the arrows are intended to suggest.</p>
</div>

<div class="image-container">
<img src="1s2.GIF">
</div>

<div class="text-block">
<p>A 1s orbital holding 2 electrons would be drawn as shown on the right, but it can be written even more quickly as 1s<sup>2</sup>. This is read as "one s two" – not as "one s squared".</p>
<p>You mustn't confuse the two numbers in this notation:</p>
</div>

<div class="image-container">
<img src="explain1s2.GIF">
</div>

<h3>The Order of Filling Orbitals – The Aufbau Principle</h3>

<div class="text-block">
<p>Aufbau is a German word meaning building up or construction. We imagine that as you go from one atom to the next in the Periodic Table, you can work out the electronic structure of the next atom by fitting an extra electron into the next available orbital.</p>
<p>Electrons fill low energy orbitals (closer to the nucleus) before they fill higher energy ones. Where there is a choice between orbitals of equal energy, they fill the orbitals singly as far as possible.</p>
<p>This filling of orbitals singly where possible is known as Hund's rule. It only applies where the orbitals have exactly the same energies (as with p orbitals, for example), and helps to minimise the repulsions between electrons and so makes the atom more stable.</p>
<p>The diagram (not to scale) summarises the energies of the orbitals up to the 4p level that you will need to know when you are using the Aufbau Principle.</p>
</div>

<div class="image-container">
<img src="energies.GIF">
</div>

<div class="text-block">
<p>Notice that the s orbital always has a slightly lower energy than the p orbitals at the same energy level, so the s orbital always fills with electrons before the corresponding p orbitals.</p>
<p>The real oddity is the position of the 3d orbitals. They are at a slightly higher level than the 4s – and so it is the 4s orbital which you fill first, followed by all the 3d orbitals and then the 4p orbitals.</p>
<p>Similar confusion occurs at higher levels, with so much overlap between the energy levels that you don't fill the 4f orbitals until after the 6s, for example.</p>
<p>For UK-based exam purposes, you simply have to remember that when you are using the Aufbau Principle, you fill the 4s orbital before the 3d orbitals. The same thing happens at the next level as well – you fill the 5s orbital before the 4d orbitals. All the other complications are beyond the scope of this site.</p>
<p>Knowing the order of filling is central to understanding how to write electronic structures. Follow the link below to find out how to do this.</p>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="elstructs.html#top">To look at how to write electronic structures </a>
<a href="../propsmenu.html#top">To the atomic properties menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>