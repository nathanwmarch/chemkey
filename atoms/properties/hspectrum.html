<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Atomic Hydrogen Emission Spectrum | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An introduction to the atomic hydrogen emission spectrum, and how it can be used to find the ionisation energy of hydrogen">
<meta name="keywords" content="atomic, hydrogen, emission, spectrum, lyman, balmer, paschen, series, ionisation, energy, electron, electrons, rydberg, constant">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Atomic Hydrogen Emission Spectrum</h1>

<div class="text-block">
<p>This page introduces the atomic hydrogen emission spectrum, showing how it arises from electron movements between energy levels within the atom. It also looks at how the spectrum can be used to find the ionisation energy of hydrogen.</p>
</div>

<h2>What is an Emission Spectrum?</h2>

<h3>Observing Hydrogen's Emission Spectrum</h3>

<div class="text-block">
<p>A hydrogen discharge tube is a slim tube containing hydrogen gas at low pressure with an electrode at each end. If you put a high voltage across this (say, 5000 volts), the tube lights up with a bright pink glow.</p>
<p>If the light is passed through a prism or diffraction grating, it is split into its various colours. What you would see is a small part of the hydrogen emission spectrum. Most of the spectrum is invisible to the eye because it is either in the infra-red or the ultra-violet.</p>
<p>The photograph shows part of a hydrogen discharge tube on the left, and the three most easily seen lines in the visible part of the spectrum on the right. (Ignore the "smearing" – particularly to the left of the red line. This is caused by flaws in the way the photograph was taken. See note below.)</p>
</div>

<div class="image-container">
<img src="hydtube.jpg">
</div>

<div class="note">
<p>Note: This photograph is by courtesy of Dr Rod Nave of the Department of Physics and Astronomy at Georgia State University, Atlanta. The photograph comes from notes about the hydrogen spectrum in his <a href="http://hyperphysics.phy-astr.gsu.edu/HBASE/hyde.html">HyperPhysics</a> pages on the University site. If you are interested in more than an introductory look at the subject, that is a good place to go. Ideally the photo would show three clean spectral lines – dark blue, cyan and red. The red smearing which appears to the left of the red line, and other similar smearing (much more difficult to see) to the left of the other two lines probably comes, according to Dr Nave, from stray reflections in the set-up, or possibly from flaws in the diffraction grating. I have chosen to use this photograph anyway because a) I think it is a stunning image, and b) it is the only one I have ever come across which includes a hydrogen discharge tube and its spectrum in the same image.</p>
</div>

<h3>Extending Hydrogen's Emission Spectrum into the UV and IR</h3>

<div class="text-block">
<p>There is a lot more to the hydrogen spectrum than the three lines you can see with the naked eye. It is possible to detect patterns of lines in both the ultra-violet and infra-red regions of the spectrum as well.</p>
<p>These fall into a number of "series" of lines, each named after the person who discovered them. The diagram below shows three of these series, but there are others in the infra-red to the left of the Paschen series shown in the diagram.</p>
<p>The diagram is quite complicated, so we will look at it a bit at a time. Look first at the Lyman series on the right of the diagram – this is the most spread out one and easiest to see what is happening.</p>
</div>

<div class="image-container">
<img src="spectrumfreq.gif">
</div>

<div class="note">
<p>Note: The frequency scale is marked in PHz – that's petaHertz. You are familiar with prefixes like kilo (meaning a thousand or 10<sup>3</sup> times), and mega (meaning a million or 10<sup>6</sup> times). Peta means 10<sup>15</sup> times. So a value like 3 PHz means 3 &times; 10<sup>15</sup> Hz. If you are worried about "Hertz", it just means "cycles per second".
</div>

<div class="text-block">
<p>The Lyman series is a series of lines in the ultra-violet. Notice that the lines get closer and closer together as the frequency increases. Eventually, they get so close together that it becomes impossible to see them as anything other than a continuous spectrum. That's what the shaded bit on the right-hand end of the series suggests.</p>
<p>Then at one particular point, known as the series limit, the series stops.</p>
<p>If you now look at the Balmer series or the Paschen series, you will see that the pattern is just the same, but the series have become more compact. In the Balmer series, notice the position of the three visible lines from the photograph further up the page.</p>
</div>

<h3>Complicating Everything – Frequency and Wavelength</h3>

<div class="text-block">
<p>You will often find the hydrogen spectrum drawn using wavelengths of light rather than frequencies. Unfortunately, because of the mathematical relationship between the frequency of light and its wavelength, you get two completely different views of the spectrum if you plot it against frequency or against wavelength.</p>
</div>

<h4>The relationship between frequency and wavelength</h4>

<div class="text-block">
<p>The mathematical relationship is:</p>
</div>

<div class="block-formula">
c = \lambda\nu
</div>
<div class="block-formula">
\begin{aligned} c &- \text{speed of light} \\ \lambda &- \text{wavelength; the Green letter lambda} \\ \nu &- \text{frequency;
the Green letter nu} \end{aligned}
</div>

<div class="text-block">
<p>Rearranging this gives equations for either wavelength or frequency.</p>
</div>

<div class="block-formula">
\lambda = \frac{c}{\nu} \Biggr| \nu = \frac{c}{\lambda}
</div>

<div class="text-block">
<p>What this means is that there is an inverse relationship between the two – a high frequency means a low wavelength and vice versa.</p>
</div>

<div class="note">
<p>Note: You will sometimes find frequency given the much more obvious symbol f.</p>
</div>

<h4>Drawing the hydrogen spectrum in terms of wavelength</h4>

<div class="text-block">
<p>This is what the spectrum looks like if you plot it in terms of wavelength instead of frequency:</p>
</div>

<div class="image-container">
<img src="spectrumwav.gif">
</div>

<div class="text-block">
<p>and just to remind you what the spectrum in terms of frequency looks like:</p>
</div>

<div class="image-container">
<img src="spectrumfreq2.gif">
</div>

<div class="text-block">
<p>Is this confusing? Well, I find it extremely confusing! So what do you do about it?</p>
<p>For the rest of this page I shall only look at the spectrum plotted against frequency, because it is much easier to relate it to what is happening in the atom. Be aware that the spectrum looks different depending on how it is plotted, but, other than that, ignore the wavelength version unless it is obvious that your examiners want it. If you try to learn both versions, you are only going to get them muddled up!</p>
</div>

<div class="note">
<p>Note: Syllabuses probably won't be very helpful about this. You need to look at past papers and mark schemes.</p>
<p>If you are working towards a UK-based exam and don't have these things, you can find out how to get hold of them by going to the <a href="../../syllabuses.html#top">syllabuses</a> page.</p>
</div>

<h2>Explaining Hydrogen's Emission Spectrum</h2>

<h3>The Balmer and Rydberg Equations</h3>

<div class="text-block">
<p>By an amazing bit of mathematical insight, in 1885 Balmer came up with a simple formula for predicting the wavelength of any of the lines in what we now know as the Balmer series. Three years later, Rydberg generalised this so that it was possible to work out the wavelengths of any of the lines in the hydrogen emission spectrum.</p>
<p>What Rydberg came up with was:</p>
</div>

<div class="block-formula">
\frac{1}{\lambda} = R_H \left( \frac{1}{n_1^2} – \frac{1}{n_2^2} \right)
</div>

<div class="text-block">
<p>R<sub>H</sub> is a constant known as the Rydberg constant.</p>
<p>n<sub>1</sub> and n<sub>2</sub> are integers (whole numbers). n<sub>2</sub> has to be greater than n<sub>1</sub>. In other words, if n<sub>1</sub> is, say, 2 then n<sub>2</sub> can be any whole number between 3 and infinity.</p>
<p>The various combinations of numbers that you can slot into this formula let you calculate the wavelength of any of the lines in the hydrogen emission spectrum – and there is close agreement between the wavelengths that you get using this formula and those found by analysing a real spectrum.</p>
</div>

<div class="note">
<p>Note: If you come across a version of Balmer's original equation, it won't look like this. In Balmer's equation, n<sub>1</sub> is always 2 – because that gives the wavelengths of the lines in the visible part of the spectrum which is what he was interested in. His original equation was also organised differently. The modern version shows more clearly what is going on.</p>
</div>

<div class="text-block">
<p>You can also use a modified version of the Rydberg equation to calculate the frequency of each of the lines. You can work out this version from the previous equation and the formula relating wavelength and frequency further up the page.</p>
</div>

<div class="block-formula">
\begin{aligned} \nu &= \frac{c}{\lambda} \\ &= c \times \frac{1}{\lambda} \\ \text{If} \frac{1}{\lambda} &= R_H \left( \frac{1}{n_1^2}
- \frac{1}{n_2^2} \right) \\ \nu &= cR_H \left( \frac{1}{n_1^2} – \frac{1}{n_2^2} \right) \end{aligned}
</div>

<div class="note">
<p>Note: You may come across versions of the Rydberg equation where the n<sub>1</sub> and n<sub>2</sub> are the other way around, or they may even be swapped for letters like m and n. Whichever version you use, the bigger number must always be the one at the bottom of the right-hand term – the one you take away. If you get them the wrong way around, it is immediately obvious if you start to do a calculation, because you will end up with a negative answer!</p>
</div>

<h3>The Origin of the Hydrogen Emission Spectrum</h3>

<div class="text-block">
<p>The lines in the hydrogen emission spectrum form regular patterns and can be represented by a (relatively) simple equation. Each line can be calculated from a combination of simple whole numbers.</p>
<p>Why does hydrogen emit light when it is excited by being exposed to a high voltage and what is the significance of those whole numbers?</p>
<p>When nothing is exciting it, hydrogen's electron is in the first energy level – the level closest to the nucleus. But if you supply energy to the atom, the electron gets excited into a higher energy level – or even removed from the atom altogether.</p>
<p>The high voltage in a discharge tube provides that energy. Hydrogen molecules are first broken up into hydrogen atoms (hence the atomic hydrogen emission spectrum) and electrons are then promoted into higher energy levels.</p>
<p>Suppose a particular electron was excited into the third energy level. This would tend to lose energy again by falling back down to a lower level. It could do this in two different ways.</p>
<p>It could fall all the way back down to the first level again, or it could fall back to the second level – and then, in a second jump, down to the first level.</p>
</div>

<div class="image-container">
<img src="energylevels1.gif">
</div>

<div class="text-block">
<h4>Tying particular electron jumps to individual lines in the spectrum</h4>
<p>If an electron falls from the 3-level to the 2-level, it has to lose an amount of energy exactly the same as the energy gap between those two levels. That energy which the electron loses comes out as light (where "light" includes UV and IR as well as visible).</p>
<p>Each frequency of light is associated with a particular energy by the equation:</p>
</div>

<div class="block-formula">
E = h\nu
</div>
<div class="block-formula">
\begin{aligned} E &- \text{energy of the photon of light} \\ h &- \text{Planck's constant} \\ \nu &- \text{frequency of the
light} \end{aligned}
</div>

<div class="text-block">
<p>The higher the frequency, the higher the energy of the light.</p>
<p>If an electron falls from the 3-level to the 2-level, red light is seen. This is the origin of the red line in the hydrogen spectrum. By measuring the frequency of the red light, you can work out its energy. That energy must be exactly the same as the energy gap between the 3-level and the 2-level in the hydrogen atom.</p>
<p>The last equation can therefore be re-written as a measure of the energy gap between two electron levels.</p>
</div>

<div class="block-formula">
\Delta E = h\nu
</div>
<div class="block-formula">
\begin{aligned} \Delta E &- \text{energy gap between energy levels} \\ h &- \text{Planck's constant} \\ \nu &- \text{frequency
of the emitted light} \end{aligned}
</div>

<div class="text-block">
<p>The greatest possible fall in energy will therefore produce the highest frequency line in the spectrum. The greatest fall will be from the infinity level to the 1-level. (The significance of the infinity level will be made clear later.)</p>
<p>The next few diagrams are in two parts – with the energy levels at the top and the spectrum at the bottom.</p>
</div>

<div class="image-container">
<img src="tieup1.gif">
</div>

<div class="text-block">
<p>If an electron fell from the 6-level, the fall is a little bit less, and so the frequency will be a little bit lower. (Because of the scale of the diagram, it is impossible to draw in all the jumps involving all the levels between 7 and infinity!)</p>
</div>

<div class="image-container">
<img src="tieup2.gif">
</div>

<div class="text-block">
<p> and as you work your way through the other possible jumps to the 1-level, you have accounted for the whole of the Lyman series. The spacings between the lines in the spectrum reflect the way the spacings between the energy levels change.</p>
</div>

<div class="image-container">
<img src="tieup3.gif">
</div>

<div class="text-block">
<p>If you do the same thing for jumps down to the 2-level, you end up with the lines in the Balmer series. These energy gaps are all much smaller than in the Lyman series, and so the frequencies produced are also much lower.</p>
</div>

<div class="image-container">
<img src="tieup4.gif">
</div>

<div class="text-block">
<p>The Paschen series would be produced by jumps down to the 3-level, but the diagram is going to get very messy if I include those as well – not to mention all the other series with jumps down to the 4-level, the 5-level and so on.</p>
</div>

<h4>The significance of the numbers in the Rydberg equation</h4>

<div class="text-block">
<p>n<sub>1</sub> and n<sub>2</sub> in the Rydberg equation are simply the energy levels at either end of the jump producing a particular line in the spectrum.</p>
<p>For example, in the Lyman series, n<sub>1</sub> is always 1. Electrons are falling to the 1-level to produce lines in the Lyman series. For the Balmer series, n<sub>1</sub> is always 2, because electrons are falling to the 2-level.</p>
<p>n<sub>2</sub> is the level being jumped from. We have already mentioned that the red line is produced by electrons falling from the 3-level to the 2-level. In this case, then, n<sub>2</sub> is equal to 3.</p>
<h4>The significance of the infinity level</h4>
<p>The infinity level represents the highest possible energy an electron can have as a part of a hydrogen atom. So what happens if the electron exceeds that energy by even the tiniest bit?</p>
<p>The electron is no longer a part of the atom. The infinity level represents the point at which ionisation of the atom occurs to form a positively charged ion.</p>
</div>

<h2>Using the Spectrum to Find Hydrogen's Ionisation Energy</h2>

<div class="text-block">
<p>When there is no additional energy supplied to it, hydrogen's electron is found at the 1-level. This is known as its ground state. If you supply enough energy to move the electron up to the infinity level, you have ionised the hydrogen.</p>
<p>The ionisation energy per electron is therefore a measure of the distance between the 1-level and the infinity level. If you look back at the last few diagrams, you will find that that particular energy jump produces the series limit of the Lyman series.</p>
</div>

<div class="note">
<p>Note: Up to now we have been talking about the energy released when an electron falls from a higher to a lower level. Obviously if a certain amount of energy is released when an electron falls from the infinity level to the 1-level, that same amount will be needed to push the electron from the 1-level up to the infinity level.</p>
</div>

<div class="text-block">
<p>If you can determine the frequency of the Lyman series limit, you can use it to calculate the energy needed to move the electron in one atom from the 1-level to the point of ionisation. From that, you can calculate the ionisation energy per mole of atoms.</p>
<p>The problem is that the frequency of a series limit is quite difficult to find accurately from a spectrum because the lines are so close together in that region that the spectrum looks continuous.</p>
</div>

<h3>Finding the Frequency of the Series Limit Graphically</h3>

<div class="text-block">
<p>Here is a list of the frequencies of the seven most widely spaced lines in the Lyman series, together with the increase in frequency as you go from one to the next.</p>
</div>

<div class="image-container">
<img src="deltav.gif">
</div>

<div class="text-block">
<p>As the lines get closer together, obviously the increase in frequency gets less. At the series limit, the gap between the lines would be literally zero.</p>
<p>That means that if you were to plot the increases in frequency against the actual frequency, you could extrapolate (continue) the curve to the point at which the increase becomes zero. That would be the frequency of the series limit.</p>
<p>In fact you can actually plot two graphs from the data in the table above. The frequency difference is related to two frequencies. For example, the figure of 0.457 is found by taking 2.467 away from 2.924. So which of these two values should you plot the 0.457 against?</p>
<p>It doesn't matter, as long as you are always consistent – in other words, as long as you always plot the difference against either the higher or the lower figure. At the point you are interested in (where the difference becomes zero), the two frequency numbers are the same.</p>
<p>As you will see from the graph below, by plotting both of the possible curves on the same graph, it makes it easier to decide exactly how to extrapolate the curves. Because these are curves, they are much more difficult to extrapolate than if they were straight lines.</p>
</div>

<div class="image-container">
<img src="graph.gif">
</div>

<div class="text-block">
<p>Both lines point to a series limit at about 3.28 x 10<sup>15</sup> Hz.</p>
</div>

<div class="note">
<p>Note: Remember that 3.28 PHz is the same as 3.28 x 10<sup>15</sup> Hz. You can use the Rydberg equation to calculate the series limit of the Lyman series as a check on this figure: n<sub>1</sub> = 1 for the Lyman series, and n<sub>2</sub> = infinity for the series limit. 1/(infinity)<sup>2</sup> = zero. That gives a value for the frequency of 3.29 x 10<sup>15</sup> Hz – in other words the two values agree to within 0.3%.</p>
</div>

<div class="text-block">
<p>So now we can calculate the energy needed to remove a single electron from a hydrogen atom. Remember the equation from higher up the page:</p>
</div>

<div class="block-formula">
\Delta E = h\nu
</div>

<div class="block-formula">
\begin{aligned} \Delta E &- \text{energy gap between energy levels} \\ h &- \text{Planck's constant} \\ \nu &- {frequency
of the emitted light} \end{aligned}
</div>

<div class="text-block">
<p>We can work out the energy gap between the ground state and the point at which the electron leaves the atom by substituting the value we've got for frequency and looking up the value of Planck's constant from a data book.</p>
</div>

<div class="block-formula">
\begin{aligned}
\Delta E &= h\nu \\
&= 6.626 {\times} 10^{-34} \times 3.28 {\times} 10^{15} \\
{} &= 2.173 {\times} 10^{-18}~J
\end{aligned}
</div>

<div class="text-block">
<p>That gives you the ionisation energy for a single atom. To find the normally quoted ionisation energy, we need to multiply this by the number of atoms in a mole of hydrogen atoms (the Avogadro constant) and then divide by 1000 to convert it into kilojoules.</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{ionisation energy} &= 2.173{\times}10^{-18} \times 6.022{\times}10^{23} \times \frac{1}{1000} \\
{} &= 1310~kJ~mol^{-1}
\end{aligned}
</div>

<div class="note">
<p>Note: It would be wrong to quote this to more than 3 significant figures. The value for the frequency obtained from the graph is only to that accuracy.</p>
</div>

<div class="text-block">
<p>This compares well with the normally quoted value for hydrogen's ionisation energy of 1312 kJ mol<sup>-1</sup>.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-hspectrum.pdf" target="_blank">Questions on the hydrogen spectrum</a>
<a href="../questions/a-hspectrum.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../propsmenu.html#top">To the atomic properties menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>