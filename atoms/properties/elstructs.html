<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Electronic Structures of Atoms | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how to work out the electronic structures of atoms required for A-level chemistry">
<meta name="keywords" content="electronic structure, atomic orbital, orbital, orbitals, electron, electrons">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Electronic Structures of Atoms</h1>

<div class="text-block">
<p>This page explores how you write electronic structures for atoms using s, p, and d notation. It assumes that you know about simple atomic orbitals – at least as far as the way they are named, and their relative energies. If you want to look at the electronic structures of simple monatomic ions (such as Cl<sup>-</sup>, Ca<sup>2+</sup> and Cr<sup>3+</sup>), you will find a link at the bottom of the page.</p>
</div>

<div class="note">
<p>Important! If you haven't already read the page on <a href="atomorbs.html#top">atomic orbitals</a> you should follow this link before you go any further.</p>
</div>

<h2>The Electronic Structures of Atoms</h2>

<h3>Relating Orbital Filling to the Periodic Table</h3>

<div class="image-container">
<img src="pt.GIF">
</div>

<div class="note">
<p>Note: On some screens the V for vanadium (element 23) may look a bit like a Y. This isn't a mistake, but an effect of converting my original diagram into a lower quality gif image for efficient web use.</p>
</div>

<div class="text-block">
<p>UK syllabuses for 16 – 18 year olds tend to stop at krypton when it comes to writing electronic structures, but it is possible that you could be asked for structures for elements up as far as barium. After barium you have to worry about f orbitals as well as s, p and d orbitals – and that's a problem for chemistry at a higher level. It is important that you look through past exam papers as well as your syllabus so that you can judge how hard the questions are likely to get.</p>
<p>This page looks in detail at the elements in the shortened version of the Periodic Table above, and then shows how you could work out the structures of some bigger atoms.</p>
</div>

<div class="note">
<p>Important! You must have a copy of your <a href="../../syllabuses.html#top">syllabus</a> and copies of recent exam papers. If you are studying a UK-based syllabus and haven't got them, follow this link to find out how to get hold of them.</p>
</div>

<h4>The first period</h4>

<div class="text-block">
<p>Hydrogen has its only electron in the 1s orbital – <span class="attention">1s<sup>1</sup></span>, and at helium the first level is completely full – <span class="attention">1s<sup>2</sup></span>.</p>
</div>

<h4>The second period</h4>
<div class="text-block">
<p>Now we need to start filling the second level, and hence start the second period. Lithium's electron goes into the 2s orbital because that has a lower energy than the 2p orbitals. Lithium has an electronic structure of 1s<sup>2</sup><span class="attention">2s<sup>1</sup></span>. Beryllium adds a second electron to this same level – 1s<sup>2</sup><span class="attention">2s<sup>2</sup></span>.
<p>Now the 2p levels start to fill. These levels all have the same energy, and so the electrons go in singly at first.</p>
</div>
<table class="list-table">
<tbody>
<tr>
<td>
B
</td>
<td>
<span class="inline-formula">1s^22s^2\color{#467abf}{2p_x^1}</span>
</td>
</tr>
<tr>
<td>
C
</td>
<td>
<span class="inline-formula">1s^22s^22p_x^1\color{#467abf}{2p_y^1}</span>
</td>
</tr>
<tr>
<td>
N
</td>
<td>
<span class="inline-formula">1s^22s^22p_x^12p_y^1\color{#467abf}{2p_z^1}</span>
</td>
</tr>
</tbody>
</table>

<div class="note">
<p>Note: The orbitals where something new is happening are shown in blue. You wouldn't normally write them any differently
from the other orbitals.</p>
</div>

<div class="text-block">
<p>The next electrons to go in will have to pair up with those already there.</p>
</div>
<table class="list-table">
<tbody>
<tr>
<td>
O
</td>
<td>
<span class="inline-formula">1s^22s^2\color{#467abf}{2p_x^2}2p_y^12p_z^1</span>
</td>
</tr>
<tr>
<td>
F
</td>
<td>
<span class="inline-formula">1s^22s^22p_x^2\color{#467abf}{2p_y^2}2p_z^1</span>
</td>
</tr>
<tr>
<td>
Ne
</td>
<td>
<span class="inline-formula">1s^22s^22p_x^22p_y^2\color{#467abf}{2p_z^2}</span>
</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>You can see that it is going to get progressively tedious to write the full electronic structures of atoms as the number of electrons increases. There are two ways around this, and you must be familiar with both.</p>
<p>Shortcut 1: All the various p electrons can be lumped together. For example, fluorine could be written as 1s<sup>2</sup>2s<sup>2</sup><span class="attention">2p<sup>5</sup></span>, and neon as 1s<sup>2</sup>2s<sup>2</sup><span class="attention">2p<sup>6</sup></span>.</p>
<p>This is what is normally done if the electrons are in an inner layer. If the electrons are in the bonding level (those on the outside of the atom), they are sometimes written in shorthand, sometimes in full. Don't worry about this. Be prepared to meet either version, but if you are asked for the electronic structure of something in an exam, write it out in full showing all the p<sub>x</sub>, p<sub>y</sub> and p<sub>z</sub> orbitals in the outer level separately.</p>
<p>For example, although we haven't yet met the electronic structure of chlorine, you could write it as 1s<sup>2</sup>2s<sup>2</sup><span class="attention">2p<sup>6</sup></span>3s<sup>2</sup><span class="attention">3p<sub>x</sub><sup>2</sup>3p<sub>y</sub><sup>2</sup>3p<sub>z</sub><sup>1</sup></span>.</p>
<p>Notice that the 2p electrons are all lumped together whereas the 3p ones are shown in full. The logic is that the 3p electrons will be involved in bonding because they are on the outside of the atom, whereas the 2p electrons are buried deep in the atom and aren't really of any interest.</p>
<p>Shortcut 2: You can lump all the inner electrons together using, for example, the symbol [Ne]. In this context, [Ne] means the electronic structure of neon – in other words: 1s<sup>2</sup>2s<sup>2</sup>2p<sub>x</sub><sup>2</sup>2p<sub>y</sub><sup>2</sup>2p<sub>z</sub><sup>2</sup>. You wouldn't do this with helium because it takes longer to write [He] than it does to write 1s<sup>2</sup>.</p>
<p>On this basis the structure of chlorine would be written <span class="attention">[Ne]</span>3s<sup>2</sup>3p<sub>x</sub><sup>2</sup>3p<sub>y</sub><sup>2</sup>3p<sub>z</sub><sup>1</sup>.</p>
</div>

<h4>The third period</h4>

<div class="text-block">
<p>At neon, all the second level orbitals are full, and so after this we have to start the third period with sodium. The pattern of filling is now exactly the same as in the previous period, except that everything is now happening at the 3-level.</p>
<p>For example:</p>
</div>

<table>
<tbody>
<tr>
<td></td>
<td>electron configuration</td>
<td>
short version
</td>
</tr>
<tr>
<td>
Mg
</td>
<td>
<span class="inline-formula">1s^22s^22p^6\color{#467abf}{3s^2}</span>
</td>
<td>
<span class="inline-formula">[Ne]3s^2</span>
</td>
</tr>
<tr>
<td>
S
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^2\color{#467abf}{3p_x^23p_y^13p_z^1}</span>
</td>
<td>
<span class="inline-formula">[Ne]3s^23p_x^23p_y^13p_z^1</span>
</td>
</tr>
<tr>
<td>
Ar
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p_x^2\color{#467abf}{3p_y^23p_z^2}</span>
</td>
<td>
<span class="inline-formula">[Ne]3s^23p_x^23p_y^23p_z^2</span>
</td>
</tr>
</tbody>
</table>

<div class="note">
<p>Note: Check that you can do these. Cover the text and then work out these structures for yourself. Then do all the rest of this period. When you've finished, check your answers against the corresponding elements from the previous period. Your answers should be the same except a level further out.</p>
</div>

<h4>The beginning of the fourth period</h4>

<div class="text-block">
<p>At this point the 3-level orbitals aren't all full – the 3d levels haven't been used yet. But if you refer back to the energies of the orbitals, you will see that the next lowest energy orbital is the 4s – so that fills next.</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>
K
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{4s^1}</span>
</td>
</tr>
<tr>
<td>
Ca
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{4s^2}</span>
</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>There is strong evidence for this in the similarities in the chemistry of elements like sodium (<span class="inline-formula">1s^22s^22p^6\color{#467abf}{3s^1}</span>) and potassium (<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{4s^1}</span>)</p>
<p>The outer electron governs their properties and that electron is in the same sort of orbital in both of the elements. That wouldn't be true if the outer electron in potassium was <span class="inline-formula">3d^1</span>.</p>
</div>

<h4>s- and p-block elements</h4>

<div class="image-container">
<img src="pt2.GIF">
</div>

<div class="text-block">
<p>The elements in Group 1 of the Periodic Table all have an outer electronic structure of <span class="inline-formula">ns^1</span> (where n is a number between 2 and 7). All Group 2 elements have an outer electronic structure of <span class="inline-formula">ns^2</span>. Elements in Groups 1 and 2 are described as s-block elements.</p>
<p>Elements from Group 3 (the boron group) across to the noble gases all have their outer electrons in p orbitals. These are then described as p-block elements.</p>
</div>

<h4>d-block elements</h4>

<div class="text-block">
<p>We are working out the electronic structures of the atoms using the Aufbau ("building up") Principle. So far we have got to calcium with a structure of <span class="inline-formula">1s^22s^22p^63s^23p^64s^2</span>.</p>
<p>The 4s level is now full, and the structures of the next atoms show electrons gradually filling up the 3d level. These are known as d-block elements.</p>
</div>

<div class="image-container">
<img src="pt3.GIF">
</div>

<div class="text-block">
<p>Once the 3d orbitals have filled up, the next electrons go into the 4p orbitals as you would expect.</p>
<p>d-block elements are elements in which the last electron to be added to the atom using the Aufbau Principle is in a d orbital.</p>
<p>The first series of these contains the elements from scandium to zinc, which at GCSE you probably called transition elements or transition metals. The terms "transition element" and "d-block element" don't quite have the same meaning, but it doesn't matter in the present context.</p>
</div>

<div class="definition">
<p>A transition element is one which has partially filled d orbitals either in the element
or any of its compounds.</p>
<p>Zinc (at the right-hand end of the d-block) always has a completely full 3d level (<span class="inline-formula">3d^{10}</span>) and so doesn't count as a transition element.</p>
</div>

<div class="note">
<p>Some UK syllabuses use a more restrictive definition which defines a transition metal as one which has one or more stable ions with partly filled d orbitals. You don't need to worry about this until you do some transition metal chemistry.</p>
</div>

<div class="text-block">
<p>d electrons are almost always described as, for example, <span class="inline-formula">d^5</span> or <span class="inline-formula">d^8</span> – and not written as separate orbitals. Remember that there are five d orbitals, and that the electrons will inhabit them singly as far as possible. Up to 5 electrons will occupy orbitals on their own. After that they will have to pair up.</p>
</div>

<div class="image-container">
<span class="inline-formula">d^5</span> means
<img src="3d5.GIF">
</div>

<div class="image-container">
<span class="inline-formula">d^8</span> means
<img src="3d8.GIF">
</div>

<div class="text-block">
<p>Notice in what follows that all the 3-level orbitals are written together – with the 4s electrons written at the end of the electronic structure.</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>
Sc
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{3d^14s^2}</span>
</td>
</tr>
<tr>
<td>
Ti
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{3d^2}4s^2</span>
</td>
</tr>
<tr>
<td>
V
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{3d^3}4s^2</span>
</td>
</tr>
<tr>
<td>
Cr
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{3d^54s^1}</span>
</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>Whoops! Chromium breaks the sequence. In chromium, the electrons in the 3d and 4s orbitals rearrange so that there is one electron in each orbital. It would be convenient if the sequence was tidy – but it's not!</p>
</div>

<table class="list-table">
<tbody>
<tr>
<td>
Mn
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{3d^54s^2}</span>
</td>
<td>
(back to being tidy again)
</td>
</tr>
<tr>
<td>
Fe
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{3d^6}4s^2</span>
</td>
</tr>
<tr>
<td>
Co
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{3d^7}4s^2</span>
</td>
</tr>
<tr>
<td>
Ni
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{3d^8}4s^2</span>
</td>
</tr>
<tr>
<td>
Cu
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^6\color{#467abf}{3d^{10}4s^1}</span>
</td>
<td>
(another awkward one!)
</td>
</tr>
<tr>
<td>
Zn
</td>
<td>
<span class="inline-formula">1s^22s^22p^63s^23p^63d^{10}\color{#467abf}{4s^2}</span>
</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>And at zinc the process of filling the d orbitals is complete.</p>
</div>

<h4>Filling the rest of period 4</h4>

<div class="text-block">
<p>The next orbitals to be used are the 4p, and these fill in exactly the same way as the 2p or 3p. We are back now with the p-block elements from gallium to krypton. Bromine, for example, is <span class="inline-formula">1s^22s^22p^63s^23p^63d^34s^24p_x^24p_y^24p_z^1</span>.</p>
</div>

<div class="note">
<p>Useful exercise: Work out the electronic structures of all the elements from gallium to krypton. You can check your answers by comparing them with the elements directly above them in the Periodic Table. For example, gallium will have the same sort of arrangement of its outer level electrons as boron or aluminium – except that gallium's outer electrons will be in the 4-level.</p>
</div>

<h3>Summary</h3>

<h4>Writing the electronic structure of an element from hydrogen to krypton</h4>

<ul>
<li>Use the Periodic Table to find the atomic number, and hence number of electrons.</li>
<li>Fill up orbitals in the order 1s, 2s, 2p, 3s, 3p, 4s, 3d, 4p – until you run out of electrons. The 3d is the awkward one – remember that specially. Fill p and d orbitals singly as far as possible before pairing electrons up.</li>
<li>Remember that chromium and copper have electronic structures which break the pattern in the first row of the d-block.</li>
</ul>

<h4>Writing the electronic structure of big s- or p-block elements</h4>

<div class="note">
<p>Note: We are deliberately excluding the d-block elements apart from the first row that we've already looked at in detail. The pattern of awkward structures isn't the same in the other rows. This is a problem for degree level.</p>
</div>

<div class="text-block">
<p>First work out the number of outer electrons. This is quite likely all you will be asked to do anyway.</p>
<p>The number of outer electrons is the same as the group number. (The noble gases are a bit of a problem here, because they are normally called group 0 rather then group 8. Helium has 2 outer electrons; the rest have 8.) All elements in group 3, for example, have 3 electrons in their outer level. Fit these electrons into s and p orbitals as necessary. Which level orbitals? Count the periods in the Periodic Table (not forgetting the one with H and He in it).</p>
<p>Iodine is in group 7 and so has 7 outer electrons. It is in the fifth period and so its electrons will be in 5s and 5p orbitals. Iodine has the outer structure <span class="inline-formula">5s^25p_x^25p_y^25p_z^1</span>.</p>
<p>What about the inner electrons if you need to work them out as well? The 1, 2 and 3 levels will all be full, and so will the 4s, 4p and 4d. The 4f levels don't fill until after anything you will be asked about at A-level. Just forget about them! That gives the full structure: <span class="inline-formula">1s^22s^22p^63s^23p^63d^34s^24p^64d^{10}5s^25p_x^25p_y^25p_z^1</span>.</p>
<p>When you've finished, count all the electrons to make sure that they come to the same as the atomic number. Don't forget to make this check – it's easy to miss an orbital out when it gets this complicated.</p>
<p>Barium is in group 2 and so has 2 outer electrons. It is in the sixth period. Barium has the outer structure 6s<sup>2</sup>.</p>
<p>Including all the inner levels: <span class="inline-formula">1s^22s^22p^63s^23p^63d^34s^24p^64d^{10}5s^25p^66s^2</span>.</p>
<p>It would be easy to include <span class="inline-formula">5d^{10}</span> as well by mistake, but the d level always fills after the next s level – so 5d fills after 6s just as 3d fills after 4s. As long as you counted the number of electrons you could easily spot this mistake because you would have 10 too many.</p>
</div>

<div class="note">
<p>Note: Don't worry too much about these complicated structures. You need to know how to work them out in principle, but your examiners are much more likely to ask you for something simple like sulfur or iron.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-elstructs.pdf" target="_blank">Questions on electronic structures of atoms</a>
<a href="../questions/a-elstructs.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="ionstruct.html#top">To working out electronic structures for ions</a>
<a href="../propsmenu.html#top">To the atomic properties menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>