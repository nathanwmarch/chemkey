<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Intermolecular Bonding – van der Waals Forces | Chemkey | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the origin of van der Waals attractions between molecules">
<meta name="keywords" content="bond, bonding, intermolecular, van der waals, forces, attractions, dipole, dipoles, dispersion">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Intermolecular Bonding – van der Waals Forces</h1>

<div class="text-block">
<p>This page explains the origin of the two weaker forms of intermolecular attractions – van der Waals dispersion forces and dipole-dipole attractions. If you are also interested in hydrogen bonding there is a link at the bottom of the page.</p>
</div>

<h2>What are Intermolecular Attractions?</h2>

<h3>Intermolecular Versus Intramolecular Bonds</h3>

<div class="text-block">
<p>Intermolecular attractions are attractions between one molecule and a neighbouring molecule. The forces of attraction which hold an individual molecule together (for example, the covalent bonds) are known as intramolecular attractions. These two words are so confusingly similar that it is safer to abandon one of them and never use it. The term "intramolecular" won't be used again on this site.</p>
<p>All molecules experience intermolecular attractions, although in some cases those attractions are very weak. Even in a gas like hydrogen, H<sub>2</sub>, if you slow the molecules down by cooling the gas, the attractions are large enough for the molecules to stick together eventually to form a liquid and then a solid.</p>
<p>In hydrogen's case the attractions are so weak that the molecules have to be cooled to 21 K (-252°C) before the attractions are enough to condense the hydrogen as a liquid. Helium's intermolecular attractions are even weaker – the molecules won't stick together to form a liquid until the temperature drops to 4 K (-269°C).</p>
</div>

<h2>van der Waals Forces: Dispersion Forces</h2>

<div class="text-block">
<p>Dispersion forces (one of the two types of van der Waals force we are dealing with on this page) are also known as "London forces" (named after Fritz London who first suggested how they might arise).</p>
</div>

<h3>The Origin of van der Waals Dispersion Forces</h3>

<h4>Temporary fluctuating dipoles</h4>

<div class="text-block">
<p>Attractions are electrical in nature. In a symmetrical molecule like hydrogen, however, there doesn't seem to be any electrical distortion to produce positive or negative parts. But that's only true on average.</p>
</div>

<div class="image-container">
<img src="fluctuate0.GIF">
</div>

<div class="text-block">
<p>The lozenge-shaped diagram represents a small symmetrical molecule – H<sub>2</sub>, perhaps, or Br<sub>2</sub>. The even shading shows that on average there is no electrical distortion.</p>
<p>But the electrons are mobile, and at any one instant they might find themselves towards one end of the molecule, making that end &delta;-. The other end will be temporarily short of electrons and so becomes &delta;+.</p>
</div>

<div class="note">
<p>Note: &delta; (read as "delta") means "slightly" – so &delta;+ means "slightly positive".</p>
</div>

<div class="image-container">
<img src="fluctuate1.GIF">
</div>

<div class="text-block">
<p>An instant later the electrons may well have moved up to the other end, reversing the polarity of the molecule.</p>
</div>

<div class="image-container">
<img src="fluctuate2.GIF">
</div>

<div class="text-block">
<p>This constant "sloshing around" of the electrons in the molecule causes rapidly fluctuating dipoles even in the most symmetrical molecule. It even happens in monatomic molecules – molecules of noble gases, like helium, which consist of a single atom.</p>
<p>If both the helium electrons happen to be on one side of the atom at the same time, the nucleus is no longer properly covered by electrons for that instant.</p>
</div>

<div class="image-container">
<img src="atompolar.GIF">
</div>

<h4>How temporary dipoles give rise to intermolecular attractions</h4>

<div class="text-block">
<p>I'm going to use the same lozenge-shaped diagram now to represent any molecule which could, in fact, be a much more complicated shape. Shape does matter (see below), but keeping the shape simple makes it a lot easier to both draw the diagrams and understand what is going on.</p>
<p>Imagine a molecule which has a temporary polarity being approached by one which happens to be entirely non-polar just at that moment. (A pretty unlikely event, but it makes the diagrams much easier to draw! In reality, one of the molecules is likely to have a greater polarity than the other at that time – and so will be the dominant one.)</p>
</div>

<div class="image-container">
<img src="approach.GIF">
</div>

<div class="text-block">
<p>As the right hand molecule approaches, its electrons will tend to be attracted by the slightly positive end of the left hand one.</p>
<p>This sets up an induced dipole in the approaching molecule, which is orientated in such a way that the &delta;+ end of one is attracted to the &delta;- end of the other.</p>
</div>

<div class="image-container">
<img src="induced1.GIF">
</div>

<div class="text-block">
<p>An instant later the electrons in the left hand molecule may well have moved up the other end. In doing so, they will repel the electrons in the right hand one.</p>
</div>

<div class="image-container">
<img src="induced2.GIF">
</div>

<div class="text-block">
<p>The polarity of both molecules reverses, but you still have &delta;+ attracting &delta;-. As long as the molecules stay close to each other the polarities will continue to fluctuate in synchronisation so that the attraction is always maintained.</p>
<p>There is no reason why this has to be restricted to two molecules. As long as the molecules are close together this synchronised movement of the electrons can occur over huge numbers of molecules.</p>
</div>

<div class="image-container">
<img src="lattice.GIF">
</div>

<div class="text-block">
<p>This diagram shows how a whole lattice of molecules could be held together in a solid using van der Waals dispersion forces. An instant later, of course, you would have to draw a quite different arrangement of the distribution of the electrons as they shifted around – but always in synchronisation.</p>
</div>

<h3>The Strength of Dispersion Forces</h3>

<div class="text-block">
<p>Dispersion forces between molecules are much weaker than the covalent bonds within molecules. It isn't possible to give any exact value, because the size of the attraction varies considerably with the size of the molecule and its shape.</p>
</div>

<h4>How molecular size affects the strength of the dispersion forces</h4>

<div class="text-block">
<p>The boiling points of the noble gases are</p>
</div>

<table class="convertable-table two-right">
<thead>
<tr>
<th>gas</th>
<th>boiling point<br>/ °C</th>
</tr>
</thead>
<tbody>
<tr><td>helium</td><td>-269</td></tr>
<tr><td>neon</td><td>-246</td></tr>
<tr><td>argon</td><td>-186</td></tr>
<tr><td>krypton</td><td>-152</td></tr>
<tr><td>xenon</td><td>-108</td></tr>
<tr><td>radon</td><td>-62</td></tr>
</tbody>
</table>

<div class="text-block">
<p>All of these elements have monatomic molecules.</p>
<p>The reason that the boiling points increase as you go down the group is that the number of electrons increases, and so also does the radius of the atom. The more electrons you have, and the more distance over which they can move, the bigger the possible temporary dipoles and therefore the bigger the dispersion forces.</p>
</div>

<div class="image-container">
<img src="nexe.GIF">
</div>

<div class="text-block">
<p>Because of the greater temporary dipoles, xenon molecules are "stickier" than neon molecules. Neon molecules will break away from each other at much lower temperatures than xenon molecules – hence neon has the lower boiling point.</p>
<p>This is the reason that (all other things being equal) bigger molecules have higher boiling points than small ones. Bigger molecules have more electrons and more distance over which temporary dipoles can develop – and so the bigger molecules are "stickier".</p>
</div>

<h4>How molecular shape affects the strength of the dispersion forces</h4>

<div class="text-block">
<p>The shapes of the molecules also matter. Long thin molecules can develop bigger temporary dipoles due to electron movement than short fat ones containing the same numbers of electrons.</p>
<p>Long thin molecules can also lie closer together – these attractions are at their most effective if the molecules are really close.</p>
<p>For example, the hydrocarbon molecules butane and 2-methylpropane both have a molecular formula C<sub>4</sub>H<sub>10</sub>, but the atoms are arranged differently. In butane the carbon atoms are arranged in a single chain, but 2-methylpropane is a shorter chain with a branch.</p>
</div>

<div class="image-container">
<img src="butanes.GIF">
</div>

<div class="text-block">
<p>Butane has a higher boiling point because the dispersion forces are greater. The molecules are longer (and so set up bigger temporary dipoles) and can lie closer together than the shorter, fatter 2-methylpropane molecules.</p>
</div>

<h2>van der Waals Forces: Dipole-dipole Interactions </h2>

<div class="note">
<p>Warning! There's a bit of a problem here with modern syllabuses. The majority of the syllabuses talk as if dipole-dipole interactions were quite distinct from van der Waals forces. Such a syllabus will talk about van der Waals forces (meaning dispersion forces) and, separately, dipole-dipole interactions.</p>
<p>All intermolecular attractions are known collectively as van der Waals forces. The various different types were first explained by different people at different times. Dispersion forces, for example, were described by London in 1930, and dipole-dipole interactions by Keesom in 1912.</p>
<p>This oddity in the syllabuses doesn't matter in the least as far as understanding is concerned – but you obviously must know what your particular examiners mean by the terms they use in the questions. Check your syllabus.</p>
<p>If you are working to a UK-based syllabus for 16 – 18 year olds, but don't have a copy of it,
<a href="../../syllabuses.html#top">follow this link</a> to find out how to get one.</p>
</div>

<div class="text-block">
<p>A molecule like HCl has a permanent dipole because chlorine is more electronegative than hydrogen. These permanent, in-built dipoles will cause the molecules to attract each other rather more than they otherwise would if they had to rely only on dispersion forces.</p>
</div>

<div class="note">
<p>Note: If you aren't happy about
<a href="electroneg.html#top">electronegativity and polar molecules</a>, follow this link before you go on.</p>
</div>

<div class="text-block">
<p>It's important to realise that all molecules experience dispersion forces. Dipole-dipole interactions are not an alternative to dispersion forces – they occur in addition to them. Molecules which have permanent dipoles will therefore have boiling points rather higher than molecules which only have temporary fluctuating dipoles.</p>
<p>Surprisingly dipole-dipole attractions are fairly minor compared with dispersion forces, and their effect can only really be seen if you compare two molecules with the same number of electrons and the same size. For example, the boiling points of ethane, CH<sub>3</sub>CH<sub>3</sub>, and fluoromethane, CH<sub>3</sub>F, are</p>
</div>

<div class="image-container">
<img src="c2h6vch3f.GIF">
</div>

<div class="text-block">
<p>Why choose these two molecules to compare? Both have identical numbers of electrons, and if you made models you would find that the sizes were similar – as you can see in the diagrams. That means that the dispersion forces in both molecules should be much the same.</p>
<p>The higher boiling point of fluoromethane is due to the large permanent dipole on the molecule because of the high electronegativity of fluorine. However, even given the large permanent polarity of the molecule, the boiling point has only been increased by some 10°.</p>
</div>

<div class="image-container">
<img src="chcl3.GIF">
</div>

<div class="text-block">
<p>Here is another example showing the dominance of the dispersion forces. Trichloromethane, CHCl<sub>3</sub>, is a highly polar molecule because of the electronegativity of the three chlorines. There will be quite strong dipole-dipole attractions between one molecule and its neighbours.</p>
</div>

<div class="image-container">
<img src="ccl4.GIF">
</div>

<div class="text-block">
<p>On the other hand, tetrachloromethane, CCl<sub>4</sub>, is non-polar. The outside of the molecule is uniformly &delta;- in all directions. CCl<sub>4</sub> has to rely only on dispersion forces.</p>
<p>So which has the highest boiling point? CCl<sub>4</sub> does, because it is a bigger molecule with more electrons. The increase in the dispersion forces more than compensates for the loss of dipole-dipole interactions.</p>
<p>The boiling points are:</p>
</div>
<table class="list-table">
<tbody>
<tr><td>CHCl<sub>3</sub></td><td>61.2°C</td></tr>
<tr><td>CCl<sub>4</sub></td><td>76.8°C</td></tr>
</tbody>
</table>

<div class="note">
<p>Note: A student pointed out to me that many web and book sources and teachers describe dispersion forces as being the weakest of the intermolecular forces, quoting values of, perhaps, up to 4 kJ/mol. That conflicts with what I have said above that "dipole-dipole attractions are fairly minor compared with dispersion forces". I have discussed this question of the
<a href="vdwstrengths.html#top">strength of dispersion forces</a> on a separate page, where I have tried to show that those web and book sources
and teachers are wrong!</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-vdw.pdf" target="_blank">Questions on van der Waals forces</a>
<a href="../questions/a-vdw.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="hbond.html#top">To look at hydrogen bonding</a>
<a href="../bondingmenu.html#top">To the bonding menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>