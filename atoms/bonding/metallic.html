<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Metallic Bonding | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the bonding in metals – an array of positive ions in a sea of electrons">
<meta name="keywords" content="bond, bonding, metallic bonding, metal, metallic, delocalisation, sea of electrons, ion, ions">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">
<h1>Metallic Bonding</h1>

<div class="text-block">
<p>This page introduces the bonding in metals. It explains how the metallic bond arises and why its strength varies from metal to metal.</p>
</div>

<h2>What is a Metallic Bond?</h2>

<h3>Metallic Bonding in Sodium</h3>

<div class="text-block">
<p>Metals tend to have high melting points and boiling points suggesting strong bonds between the atoms. Even a metal like sodium (melting point 97.8°C) melts at a considerably higher temperature than the element (neon) which precedes it in the Periodic Table.</p>
<p>Sodium has the electronic structure 1s<sup>2</sup>2s<sup>2</sup>2p<sup>6</sup>3s<sup>1</sup>. When sodium atoms come together, the electron in the 3s atomic orbital of one sodium atom shares space with the corresponding electron on a neighbouring atom to form a molecular orbital – in much the same sort of way that a covalent bond is formed.</p>
<p>The difference, however, is that each sodium atom is being touched by eight other sodium atoms – and the sharing occurs between the central atom and the 3s orbitals on all of the eight other atoms. And each of these eight is in turn being touched by eight sodium atoms, which in turn are touched by eight atoms – and so on and so on, until you have taken in all the atoms in that lump of sodium.</p>
<p>All of the 3s orbitals on all of the atoms overlap to give a vast number of molecular orbitals which extend over the whole piece of metal. There have to be huge numbers of molecular orbitals, of course, because any orbital can only hold two electrons.</p>
<p>The electrons can move freely within these molecular orbitals, and so each electron becomes detached from its parent atom. The electrons are said to be delocalised. The metal is held together by the strong forces of attraction between the positive nuclei and the delocalised electrons.</p>
</div>

<div class="image-container">
<img src="metalbond.GIF">
</div>

<div class="text-block">
<p>This is sometimes described as "an array of positive ions in a sea of electrons".</p>
<p>If you are going to use this view, beware! Is a metal made up of atoms or ions? It is made of atoms.</p>
<p>Each positive centre in the diagram represents all the rest of the atom apart from the outer electron, but that electron hasn't been lost – it may no longer have an attachment to a particular atom, but it's still there in the structure. Sodium metal is therefore written as Na – not Na<sup>+</sup>.</p>
</div>

<h3>Metallic Bonding in Magnesium</h3>

<div class="text-block">
<p>If you work through the same argument with magnesium, you end up with stronger bonds and so a higher melting point.</p>
<p>Magnesium has the outer electronic structure 3s<sup>2</sup>. Both of these electrons become delocalised, so the "sea" has twice the electron density as it does in sodium. The remaining "ions" also have twice the charge (if you are going to use this particular view of the metal bond) and so there will be more attraction between "ions" and "sea".</p>
<p>More realistically, each magnesium atom has 12 protons in the nucleus compared with sodium's 11. In both cases, the nucleus is screened from the delocalised electrons by the same number of inner electrons – the 10 electrons in the 1s<sup>2</sup>2s<sup>2</sup>2p<sup>6</sup> orbitals.</p>
<p>That means that there will be a net pull from the magnesium nucleus of 2+, but only 1+ from the sodium nucleus.</p>
<p>So not only will there be a greater number of delocalised electrons in magnesium, but there will also be a greater attraction for them from the magnesium nuclei.</p>
<p>Magnesium atoms also have a slightly smaller radius than sodium atoms, and so the delocalised electrons are closer to the nuclei. Each magnesium atom also has twelve near neighbours rather than sodium's eight. Both of these factors increase the strength of the bond still further.</p>
</div>

<h3>Metallic Bonding in Transition Elements</h3>

<div class="text-block">
<p>Transition metals tend to have particularly high melting points and boiling points. The reason is that they can involve the 3d electrons in the delocalisation as well as the 4s. The more electrons you can involve, the stronger the attractions tend to be.</p>
</div>

<div class="note">
<p>Note: If you aren't happy about the <a href="../properties/elstructs.html#top">electronic structure</a> of transition metals, then you might like to follow this link to revise it.</p>
</div>

<h3>The Metallic Bond in Molten Metals</h3>

<div class="text-block">
<p>In a molten metal, the metallic bond is still present, although the ordered structure has been broken down. The metallic bond isn't fully broken until the metal boils. That means that boiling point is actually a better guide to the strength of the metallic bond than melting point is. On melting, the bond is loosened, not broken.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-metallicbond.pdf" target="_blank">Questions on metallic bonding</a>
<a href="../questions/a-metallicbond.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../structures/metals.html#top">To explore the structure of metals</a>
<a href="../bondingmenu.html#top">To the bonding menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>