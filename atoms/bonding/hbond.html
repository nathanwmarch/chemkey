<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Intermolecular Bonding – Hydrogen Bonds | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the origin of hydrogen bonding with a range of examples">
<meta name="keywords" content="bond, bonding, intermolecular, hydrogen bonds, hydrogen bond, hydrogen bonding, forces, attractions, dipole, dipoles, lone pair">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">
<h1>Intermolecular Bonding – Hydrogen Bonds</h1>

<div class="text-block">
<p>This page explains the origin of hydrogen bonding – a relatively strong form of intermolecular attraction. If you are also interested in the weaker intermolecular forces (van der Waals dispersion forces and dipole-dipole interactions), there is a link at the bottom of the page.</p>
</div>

<h2>The Evidence for Hydrogen Bonding</h2>

<div class="text-block">
<p>Many elements form compounds with hydrogen. If you plot the boiling points of the compounds of the Group 4 elements with hydrogen, you find that the boiling points increase as you go down the group.</p>
</div>

<div class="image-container">
<img src="bptgp4hyd.GIF">
</div>

<div class="text-block">
<p>The increase in boiling point happens because the molecules are getting larger with more electrons, and so van der Waals dispersion forces become greater.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about
<a href="vdw.html#top">van der Waals dispersion forces</a>, it would pay you to follow this link before you go on.</p>
</div>

<div class="text-block">
<p>If you repeat this exercise with the compounds of the elements in Groups 5, 6 and 7 with hydrogen, something odd happens.</p>
</div>

<div class="image-container">
<img src="bptgp567hyd.GIF">
</div>

<div class="text-block">
<p>Although for the most part the trend is exactly the same as in group 4 (for exactly the same reasons), the boiling point of the compound of hydrogen with the first element in each group is abnormally high.</p>
<p>In the cases of NH<sub>3</sub>, H<sub>2</sub>O and HF there must be some additional intermolecular forces of attraction, requiring significantly more heat energy to break. These relatively powerful intermolecular forces are described as hydrogen bonds.</p>
</div>

<h2>The Origin of Hydrogen Bonding</h2>

<div class="text-block">
<p>The molecules which have this extra bonding are:</p>
</div>

<div class="image-container">
<img src="nh3h2ohf.GIF">
</div>

<div class="note">
<p>Note: The solid line represents a bond in the plane of the screen or paper. Dotted bonds are going back into the screen
or paper away from you, and wedge-shaped ones are coming out towards you.</p>
</div>

<div class="text-block">
<p>Notice that in each of these molecules:</p>
</div>

<ul>
<li>The hydrogen is attached directly to one of the most electronegative elements, causing the hydrogen to acquire a significant amount of positive charge.</li>

<li>Each of the elements to which the hydrogen is attached is not only significantly negative, but also has at least one "active" lone pair.<br>Lone pairs at the 2-level have the electrons contained in a relatively small volume of space which therefore has a high density of negative charge. Lone pairs at higher levels are more diffuse and not so attractive to positive things.</li>
</ul>

<div class="note">
<p>Note: If you aren't happy about <a href="electroneg.html#top">electronegativity</a>, you should follow this link before you go on.</p>
</div>

<div class="text-block">
<p>Consider two water molecules coming close together.</p>
</div>

<div class="image-container">
<img src="h2ohbonds.GIF">
</div>

<div class="text-block">
<p>The &delta;<sup>+</sup> hydrogen is so strongly attracted to the lone pair that it is almost as if you were beginning to form a co-ordinate (dative covalent) bond. It doesn't go that far, but the attraction is significantly stronger than an ordinary dipole-dipole interaction.</p>
<p>Hydrogen bonds have about a tenth of the strength of an average covalent bond, and are being constantly broken and reformed in liquid water. If you liken the covalent bond between the oxygen and hydrogen to a stable marriage, the hydrogen bond has "just good friends" status.</p>
</div>

<h3>Water as a "Perfect" Example of Hydrogen Bonding</h3>

<div class="text-block">
<p>Notice that each water molecule can potentially form four hydrogen bonds with surrounding water molecules. There are exactly the right numbers of &delta;<sup>+</sup> hydrogens and lone pairs so that every one of them can be involved in hydrogen bonding.</p>
<p>This is why the boiling point of water is higher than that of ammonia or hydrogen fluoride.</p>
</div>

<div class="note">
<p>Note: You will find more discussion on the effect of hydrogen bonding on the properties of water in the page on <a href="../structures/molecular.html#top">molecular structures</a>.</p>
</div>

<div class="text-block">
<p>In the case of ammonia, the amount of hydrogen bonding is limited by the fact that each nitrogen only has one lone pair. In a group of ammonia molecules, there aren't enough lone pairs to go around to satisfy all the hydrogens.</p>
<p>That means that on average each ammonia molecule can form one hydrogen bond using its lone pair and one involving one of its &delta;<sup>+</sup> hydrogens. The other hydrogens are wasted.</p>
<p>In hydrogen fluoride, the problem is a shortage of hydrogens. On average, then, each molecule can only form one hydrogen bond using its &delta;<sup>+</sup> hydrogen and one involving one of its lone pairs. The other lone pairs are essentially wasted.</p>
<p>In water, there are exactly the right number of each. Water could be considered as the "perfect" hydrogen bonded system.</p>
</div>

<div class="note">
<p>Warning: It has been pointed out to me that some sources (including one of the UK A-level Exam Boards) count the number of hydrogen bonds formed by water, say, differently. They say that water forms 2 hydrogen bonds, not 4. That is often accompanied by a diagram of ice next to this statement clearly showing 4 hydrogen bonds!</p>
<p>Reading what they say, it appears that they only count a hydrogen bond as belonging to a particular molecule if it comes from a hydrogen atom on that molecule. That seems to me to be illogical. A hydrogen bond is made from two parts – a &delta;<sup>+</sup> hydrogen attached to a sufficiently electronegative element, and an active lone pair. These interact to make a hydrogen bond, and it is still a hydrogen bond irrespective of which end you look at it from.</p>
<p>The IUPAC definitions of a hydrogen bond make no reference at all to any of this, so there doesn't seem to be any "official" backing for this one way or the other.</p>
<p>However, it is essential that you find out what your examiners are expecting. They make the rules for the exam you will be sitting, and you have no choice other than to play by those rules.</p>
</div>

<h2>More Complex Examples of Hydrogen Bonding</h2>

<h3>The Hydration of Negative Ions</h3>

<div class="text-block">
<p>When an ionic substance dissolves in water, water molecules cluster around the separated ions. This process is called hydration.</p>
<p>Water frequently attaches to positive ions by co-ordinate (dative covalent) bonds. It bonds to negative ions using hydrogen bonds.</p>
</div>

<div class="note">
<p>Note: If you are interested in the bonding in hydrated positive ions, you could follow this link to <a href="dative.html#top">co-ordinate (dative covalent) bonding</a>.</p>
</div>

<div class="text-block">
<p>The diagram shows the potential hydrogen bonds formed to a chloride ion, Cl<sup>-</sup>. Although the lone pairs in the chloride ion are at the 3-level and wouldn't normally be active enough to form hydrogen bonds, in this case they are made more attractive by the full negative charge on the chlorine.</p>
</div>

<div class="image-container">
<img src="clhbonds.GIF">
</div>

<div class="text-block">
<p>However complicated the negative ion, there will always be lone pairs that the hydrogen atoms from the water molecules can hydrogen bond to.</p>
</div>

<h3>Hydrogen Bonding in Alcohols</h3>

<div class="text-block">
<p>An alcohol is an organic molecule containing an –O–H group.</p>
<p>Any molecule which has a hydrogen atom attached directly to an oxygen or a nitrogen is capable of hydrogen bonding. Such molecules will always have higher boiling points than similarly sized molecules which don't have an –O–H or an –N–H group. The hydrogen bonding makes the molecules "stickier", and more heat is necessary to separate them.</p>
<p>Ethanol, CH<sub>3</sub>CH<sub>2</sub>–O–H, and methoxymethane, CH<sub>3</sub>–O–CH<sub>3</sub>, both have the same molecular formula, C<sub>2</sub>H<sub>6</sub>O.</p>
</div>

<div class="image-container">
<img src="olvether.GIF">
</div>

<div class="note">
<p>Note: If you haven't done any organic chemistry yet, don't worry about the names.</p>
</div>

<div class="text-block">
<p>They have the same number of electrons, and a similar length to the molecule. The van der Waals attractions (both dispersion forces and dipole-dipole attractions) in each will be much the same.</p>
<p>However, ethanol has a hydrogen atom attached directly to an oxygen – and that oxygen still has exactly the same two lone pairs as in a water molecule. Hydrogen bonding can occur between ethanol molecules, although not as effectively as in water. The hydrogen bonding is limited by the fact that there is only one hydrogen in each ethanol molecule with sufficient &delta;<sup>+</sup> charge.</p>
<p>In methoxymethane, the lone pairs on the oxygen are still there, but the hydrogens aren't sufficiently &delta;<sup>+</sup> for hydrogen bonds to form. Except in some rather unusual cases, the hydrogen atom has to be attached directly to the very electronegative element for hydrogen bonding to occur.</p>
<p>The boiling points of ethanol and methoxymethane show the dramatic effect that the hydrogen bonding has on the stickiness of the ethanol molecules:</p>
</div>

<table class="data-table two-right">
<thead>
<tr>
<th></th>
<th>boiling point</th>
</tr>
</thead>
<tbody>
<tr><td>ethanol (with hydrogen bonding)</td><td>78.5°C</td></tr>
<tr><td>methoxymethane (without hydrogen bonding)</td><td>-24.8°C</td></tr>
</tbody>
</table>
<div class="text-block">
<p>The hydrogen bonding in the ethanol has lifted its boiling point about 100°C.</p>
<p>It is important to realise that hydrogen bonding exists in addition to van der Waals attractions. For example, all the following molecules contain the same number of electrons, and the first two are much the same length. The higher boiling point of the butan-1-ol is due to the additional hydrogen bonding.</p>
</div>

<div class="image-container">
<img src="olhbonds.GIF">
</div>

<div class="text-block">
<p>Comparing the two alcohols (containing –OH groups), both boiling points are high because of the additional hydrogen bonding due to the hydrogen attached directly to the oxygen – but they aren't the same.</p>
<p>The boiling point of the 2-methylpropan-1-ol isn't as high as the butan-1-ol because the branching in the molecule makes the van der Waals attractions less effective than in the longer butan-1-ol.</p>
</div>

<h3>Hydrogen Bonding in Organic Molecules Containing Nitrogen</h3>

<div class="text-block">
<p>Hydrogen bonding also occurs in organic molecules containing N–H groups – in the same sort of way that it occurs in ammonia. Examples range from simple molecules like CH<sub>3</sub>NH<sub>2</sub> (methylamine) to large molecules like proteins and DNA.</p>
<p>The two strands of the famous double helix in DNA are held together by hydrogen bonds between hydrogen atoms attached to nitrogen on one strand, and lone pairs on another nitrogen or an oxygen on the other one.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-hbond.pdf" target="_blank">Questions on hydrogen bonding</a>
<a href="../questions/a-hbond.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="vdw.html#top">To look at van der Waals forces</a>
<a href="../bondingmenu.html#top">To the bonding menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>