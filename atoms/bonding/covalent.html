<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Covalent Bonding – Single Bonds | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how single covalent bonds are formed, starting with a simple view and then extending it for A-level.">
<meta name="keywords" content="covalent bonding, covalent bond, covalency, covalent, bond, bonding, sigma bond">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Covalent Bonding – Single Bonds</h1>

<div class="text-block">
<p>This page explains what covalent bonding is. It starts with a simple picture of the single covalent bond, and then modifies it slightly for A-level purposes.</p>
<p>It also goes on to a more sophisticated view involving hybridisation. This isn't required by many UK-based syllabuses at this level. However, if you can follow it, it will make the bonding in organic compounds easier to understand. I shall make use of it throughout the rest of Chemguide.</p>
<p>You will find a link to a page on double covalent bonds at the bottom of the page.</p>
</div>

<h2>A Simple View of Covalent Bonding</h2>

<h3>The Importance of Noble Sas Structures</h3>

<div class="text-block">
<p>At a simple level (like GCSE) a lot of importance is attached to the electronic structures of noble gases like neon or argon which have eight electrons in their outer energy levels (or two in the case of helium). These noble gas structures are thought of as being in some way a "desirable" thing for an atom to have.</p>
<p>You may well have been left with the strong impression that when other atoms react, they try to achieve noble gas structures.</p>
<p>As well as achieving noble gas structures by transferring electrons from one atom to another as in ionic bonding, it is also possible for atoms to reach these stable structures by sharing electrons to give covalent bonds.</p>
</div>

<h3>Some Very Simple Covalent Molecules</h3>

<h4>Chlorine</h4>

<div class="text-block">
<p>For example, two chlorine atoms could both achieve stable structures by sharing their single unpaired electron as in the diagram.</p>
</div>

<div class="image-container">
<img src="cl2.GIF">
</div>

<div class="text-block">
<p>The fact that one chlorine has been drawn with electrons marked as crosses and the other as dots is simply to show where all the electrons come from. In reality there is no difference between them.</p>
<p>The two chlorine atoms are said to be joined by a covalent bond. The reason that the two chlorine atoms stick together is that the shared pair of electrons is attracted to the nucleus of both chlorine atoms.</p>
</div>

<h4>Hydrogen</h4>

<div class="image-container">
<img src="h2.GIF">
</div>

<div class="text-block">
<p>Hydrogen atoms only need two electrons in their outer level to reach the noble gas structure of helium. Once again, the covalent bond holds the two atoms together because the pair of electrons is attracted to both nuclei.</p>
</div>

<h4>Hydrogen chloride</h4>

<div class="image-container">
<img src="hcl.GIF">
</div>

<div class="text-block">
<p>The hydrogen has a helium structure, and the chlorine an argon structure.</p>
</div>

<h2>Covalent Bonding at A-level</h2>

<h3>Cases Where There Isn't Any Difference From the Simple View</h3>

<div class="text-block">
<p>If you stick closely to modern A-level syllabuses, there is little need to move far from the simple (GCSE) view. The only thing which must be changed is the over-reliance on the concept of noble gas structures. Most of the simple molecules you draw do in fact have all their atoms with noble gas structures.</p>
<p>For example:</p>
</div>

<div class="image-container">
<img src="ch4nh3h2o.GIF">
</div>

<div class="text-block">
<p>Even with a more complicated molecule like PCl<sub>3</sub>, there's no problem. In this case, only the outer electrons are shown for simplicity. Each atom in this structure has inner layers of electrons of 2,8. Again, everything present has a noble gas structure.</p>
</div>

<div class="image-container">
<img src="pcl3.GIF">
</div>

<h3>Cases Where the Simple View Throws Up Problems</h3>

<h4>Boron trifluoride, BF<sub>3</sub></h4>

<div class="image-container">
<img src="bf3.GIF">
</div>

<div class="text-block">
<p>A boron atom only has 3 electrons in its outer level, and there is no possibility of it reaching a noble gas structure by simple sharing of electrons. Is this a problem? No. The boron has formed the maximum number of bonds that it can in the circumstances, and this is a perfectly valid structure.</p>
<p>Energy is released whenever a covalent bond is formed. Because energy is being lost from the system, it becomes more stable after every covalent bond is made. It follows, therefore, that an atom will tend to make as many covalent bonds as possible. In the case of boron in BF<sub>3</sub>, three bonds is the maximum possible because boron only has 3 electrons to share.</p>
</div>

<div class="note">
<p>Note: You might perhaps wonder why boron doesn't form ionic bonds with fluorine instead. Boron doesn't form ions because the total energy needed to remove three electrons to form a B<sup>3+</sup> ion is simply too great to be recoverable when attractions are set up between the boron and fluoride ions.</p>
</div>

<h4>Phosphorus(V) chloride, PCl<sub>5</sub></h4>

<div class="text-block">
<p>In the case of phosphorus, 5 covalent bonds are possible – as in PCl<sub>5</sub>.</p>
<p>Phosphorus forms two chlorides – PCl<sub>3</sub> and PCl<sub>5</sub>. When phosphorus burns in chlorine both are formed – the majority product depending on how much chlorine is available. We've already looked at the structure of PCl<sub>3</sub>.</p>
<p>The diagram of PCl<sub>5</sub> (like the previous diagram of PCl<sub>3</sub>) shows only the outer electrons.</p>
</div>

<div class="image-container">
<img src="pcl5.GIF">
</div>

<div class="text-block">
<p>Notice that the phosphorus now has 5 pairs of electrons in the outer level – certainly not a noble gas structure. You would have been content to draw PCl<sub>3</sub> at GCSE, but PCl<sub>5</sub> would have looked very worrying.</p>
<p>Why does phosphorus sometimes break away from a noble gas structure and form five bonds? In order to answer that question, we need to explore territory beyond the limits of most current A-level syllabuses. Don't be put off by this! It isn't particularly difficult, and is extremely useful if you are going to understand the bonding in some important organic compounds.</p>
</div>

<h2>A More Sophisticated View of Covalent Bonding</h2>

<h3>The Bonding in Methane, CH<sub>4</sub></h3>

<div class="note">
<p>Warning! If you aren't happy with describing electron arrangements in s and p notation, and with the shapes of s and p orbitals, you need to read about <a href="../properties/atomorbs.html#top">orbitals</a> before you go on.</p>
</div>

<h4>What is wrong with the dots-and-crosses picture of bonding in methane?</h4>

<div class="text-block">
<p>We are starting with methane because it is the simplest case which illustrates the sort of processes involved. You will remember that the dots-and-crossed picture of methane looks like this.</p>
</div>

<div class="image-container">
<img src="ch4dandc.GIF">
</div>

<div class="text-block">
<p>There is a serious mis-match between this structure and the modern electronic structure of carbon, 1s<sup>2</sup>2s<sup>2</sup>2p<sub>x</sub><sup>1</sup>2p<sub>y</sub><sup>1</sup>. The modern structure shows that there are only 2 unpaired electrons to share with hydrogens, instead of the 4 which the simple view requires.</p>
</div>

<div class="image-container">
<img src="cground.GIF">
</div>

<div class="text-block">
<p>You can see this more readily using the electrons-in-boxes notation. Only the 2-level electrons are shown. The 1s<sup>2</sup> electrons are too deep inside the atom to be involved in bonding. The only electrons directly available for sharing are the 2p electrons. Why then isn't methane CH<sub>2</sub>?</p>
</div>

<h4>Promotion of an electron</h4>

<div class="image-container">
<img src="cpromote.GIF">
</div>

<div class="text-block">
<p>When bonds are formed, energy is released and the system becomes more stable. If carbon forms 4 bonds rather than 2, twice as much energy is released and so the resulting molecule becomes even more stable.</p>
<p>There is only a small energy gap between the 2s and 2p orbitals, and so it pays the carbon to provide a small amount of energy to promote an electron from the 2s to the empty 2p to give 4 unpaired electrons. The extra energy released when the bonds form more than compensates for the initial input.</p>
<p>The carbon atom is now said to be in an excited state.</p>
</div>

<div class="note">
<p>Note: People sometimes worry that the promoted electron is drawn as an up-arrow, whereas it started as a down-arrow. The reason for this is actually fairly complicated – well beyond the level we are working at. Just get in the habit of writing it like this because it makes the diagrams look tidy!</p>
</div>

<div class="text-block">
<p>Now that we've got 4 unpaired electrons ready for bonding, another problem arises. In methane all the carbon-hydrogen bonds are identical, but our electrons are in two different kinds of orbitals. You aren't going to get four identical bonds unless you start from four identical orbitals.</p>
</div>

<h4>Hybridisation</h4>

<div class="image-container">
<img src="sp3boxes.GIF">
</div>

<div class="text-block">
<p>The electrons rearrange themselves again in a process called hybridisation. This reorganises the electrons into four identical hybrid orbitals called sp<sup>3</sup> hybrids (because they are made from one s orbital and three p orbitals). You should read "sp<sup>3</sup>" as "s p three" – not as "s p cubed".</p>
</div>

<div class="image-container">
<img src="sp3orbitals.GIF">
</div>

<div class="text-block">
<p>sp<sup>3</sup> hybrid orbitals look a bit like half a p orbital, and they arrange themselves in space so that they are as far apart as possible. You can picture the nucleus as being at the centre of a tetrahedron (a triangularly based pyramid) with the orbitals pointing to the corners. For clarity, the nucleus is drawn far larger than it really is.</p>
</div>

<h4>What happens when the bonds are formed?</h4>

<div class="text-block">
<p>Remember that hydrogen's electron is in a 1s orbital – a spherically symmetric region of space surrounding the nucleus where there is some fixed chance (say 95%) of finding the electron. When a covalent bond is formed, the atomic orbitals (the orbitals in the individual atoms) merge to produce a new molecular orbital which contains the electron pair which creates the bond.</p>
</div>

<div class="image-container">
<img src="ch4orbitals.GIF">
</div>

<div class="text-block">
<p>Four molecular orbitals are formed, looking rather like the original sp<sup>3</sup> hybrids, but with a hydrogen nucleus embedded in each lobe. Each orbital holds the 2 electrons that we've previously drawn as a dot and a cross.</p>
<p>The principles involved – promotion of electrons if necessary, then hybridisation, followed by the formation of molecular orbitals – can be applied to any covalently-bound molecule.</p>
</div>
<div class="note">
<p>Note: You will find this bit on methane repeated in the organic section of this site. That article on <a href="../../basicorg/bonding/methane.html#top">methane</a> goes on to look at the formation of carbon-carbon single bonds in ethane.</p>
</div>

<h3>The Bonding in the Phosphorus Chlorides, PCl<sub>3</sub> and PCl<sub>5</sub></h3>

<h4>What's wrong with the simple view of PCl<sub>3</sub>?</h4>

<div class="text-block">
<p>This diagram only shows the outer (bonding) electrons.</p>
</div>

<div class="image-container">
<img src="pcl3.GIF">
</div>

<div class="text-block">
<p>Nothing is wrong with this! (Although it doesn't account for the shape of the molecule properly.) If you were going to take a more modern look at it, the argument would go like this:</p>
<p>Phosphorus has the electronic structure 1s<sup>2</sup>2s<sup>2</sup>2p<sup>6</sup>3s<sup>2</sup>3p<sub>x</sub><sup>1</sup>3p<sub>y</sub><sup>1</sup>3p_z<sup>1</sup>. If we look only at the outer electrons as "electrons-in-boxes":</p>
</div>

<div class="image-container">
<img src="pground.GIF">
</div>

<div class="text-block">
<p>There are 3 unpaired electrons that can be used to form bonds with 3 chlorine atoms. The four 3-level orbitals hybridise to produce 4 equivalent sp<sup>3</sup> hybrids just like in carbon – except that one of these hybrid orbitals contains a lone pair of electrons.</p>
</div>

<div class="image-container">
<img src="psp3hybrids.GIF">
</div>

<div class="text-block">
<p>Each of the 3 chlorines then forms a covalent bond by merging the atomic orbital containing its unpaired electron with one of the phosphorus's unpaired electrons to make 3 molecular orbitals.</p>
<p>You might wonder whether all this is worth the bother! Probably not! It is worth it with PCl<sub>5</sub>, though.</p>
</div>

<h4>What's wrong with the simple view of PCl<sub>5</sub>?</h4>

<div class="text-block">
<p>You will remember that the dots-and-crosses picture of PCl<sub>5</sub> looks awkward because the phosphorus doesn't end up with a noble gas structure. This diagram also shows only the outer electrons.</p>
</div>

<div class="image-container">
<img src="pcl5.GIF">
</div>

<div class="text-block">
<p>In this case, a more modern view makes things look better by abandoning any pretence of worrying about noble gas structures.</p>
<p>If the phosphorus is going to form PCl<sub>5</sub> it has first to generate 5 unpaired electrons. It does this by promoting one of the electrons in the 3s orbital to the next available higher energy orbital.</p>
<p>Which higher energy orbital? It uses one of the 3d orbitals. You might have expected it to use the 4s orbital because this is the orbital that fills before the 3d when atoms are being built from scratch. Not so! Apart from when you are building the atoms in the first place, the 3d always counts as the lower energy orbital.</p>
</div>

<div class="image-container">
<img src="ppromote.GIF">
</div>

<div class="text-block">
<p>This leaves the phosphorus with this arrangement of its electrons:</p>
</div>

<div class="image-container">
<img src="pexcited.GIF">
</div>

<div class="text-block">
<p>The 3-level electrons now rearrange (hybridise) themselves to give 5 hybrid orbitals, all of equal energy. They would be called sp<sup>3</sup>d hybrids because that's what they are made from.</p>
</div>

<div class="image-container">
<img src="psp3dhybrids.GIF">
</div>

<div class="text-block">
<p>The electrons in each of these orbitals would then share space with electrons from five chlorines to make five new molecular orbitals – and hence five covalent bonds.</p>
<p>Why does phosphorus form these extra two bonds? It puts in an amount of energy to promote an electron, which is more than paid back when the new bonds form. Put simply, it is energetically profitable for the phosphorus to form the extra bonds.</p>
<p>The advantage of thinking of it in this way is that it completely ignores the question of whether you've got a noble gas structure, and so you don't worry about it.</p>
</div>

<div class="note">
<p>If you are a teacher or if you are likely to do chemistry at university: A paper published in 2007 suggests that this explanation is seriously flawed. If you are likely to do chemistry at university level, you will probably have to discard it later in favour of a more accurate explanation. However, that explanation is way beyond 16 – 18 year old level.</p>
<p>If you get asked about this at the equivalent of UK A-level, you will have to give the explanation above – there is no alternative. Don't worry about it!</p>
<p>If you want a little bit more detail you will find it on <a href="sp3dhybrids.html">this page</a>.</p>
</div>

<h3>A Non-existent Compound – NCl<sub>5</sub></h3>

<div class="text-block">
<p>Nitrogen is in the same Group of the Periodic Table as phosphorus, and you might expect it to form a similar range of compounds. In fact, it doesn't. For example, the compound NCl<sub>3</sub> exists, but there is no such thing as NCl<sub>5</sub>.</p>
<p>Nitrogen is 1s<sup>2</sup>2s<sup>2</sup>2p<sub>x</sub><sup>1</sup>2p<sub>y</sub><sup>1</sup>2p_z<sup>1</sup>. The reason that NCl<sub>5</sub> doesn't exist is that in order to form five bonds, the nitrogen would have to promote one of its 2s electrons. The problem is that there aren't any 2d orbitals to promote an electron into – and the energy gap to the next level (the 3s) is far too great.</p>
<p>In this case, then, the energy released when the extra bonds are made isn't enough to compensate for the energy needed to promote an electron – and so that promotion doesn't happen.</p>
<p>Atoms will form as many bonds as possible provided it is energetically profitable.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-covsingle.pdf" target="_blank">Questions on covalent single bonding</a>
<a href="../questions/a-covsingle.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="doublebonds.html#top">To explore double covalent bonding</a>
<a href="../bondingmenu.html#top">To the bonding menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>