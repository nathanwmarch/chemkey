<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Ionic Structures | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Looks at the way the ions are arranged in sodium chloride and the way the structure affects the physical properties">
<meta name="keywords" content="sodium chloride, caesium chloride, cesium chloride, ionic, ion, ions, structure, giant, physical properties, melting point, boiling point, brittle, brittleness, solubility, electrolysis">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Ionic Structures</h1>

<div class="text-block">
<p>This page explains the relationship between the arrangement of the ions in a typical ionic solid like sodium chloride and its physical properties – melting point, boiling point, brittleness, solubility and electrical behaviour. It also explains why caesium chloride has a different structure from sodium chloride even though sodium and caesium are both in Group 1 of the Periodic Table.</p>
</div>

<div class="note">
<p>Note: If you need to revise how <a href="../bonding/ionic.html#top">ionic bonding</a> arises, then you might like to follow this link. It isn't important for understanding this page, however.</p>
</div>

<h2>The Structure of a Typical Ionic Solid – Sodium chloride</h2>

<h3>How the ions are arranged in sodium chloride</h3>

<div class="text-block">
<p>Sodium chloride is taken as a typical ionic compound. Compounds like this consist of a giant (endlessly repeating) lattice of ions. So sodium chloride (and any other ionic compound) is described as having a giant ionic structure.</p>
<p>You should be clear that giant in this context doesn't just mean very large. It means that you can't state exactly how many ions there are.</p>
<p>There could be billions of sodium ions and chloride ions packed together, or trillions, or whatever – it simply depends how big the crystal is. That is different from, say, a water molecule which always contains exactly 2 hydrogen atoms and one oxygen atom – never more and never less.</p>
<p>A small representative bit of a sodium chloride lattice looks like this:</p>
</div>

<div class="image-container">
<img src="naclspfil.GIF">
</div>

<div class="text-block">
<p>If you look at the diagram carefully, you will see that the sodium ions and chloride ions alternate with each other in each of the three dimensions.</p>
<p>This diagram is easy enough to draw with a computer, but extremely difficult to draw convincingly by hand. We normally draw an "exploded" version which looks like this:</p>
</div>

<div class="image-container">
<img src="naclexpl.GIF">
</div>

<div class="text-block">
<p>Only those ions joined by lines are actually touching each other. The sodium ion in the centre is being touched by 6 chloride ions. By chance we might just as well have centred the diagram around a chloride ion – that, of course, would be touched by 6 sodium ions. Sodium chloride is described as being 6:6-co-ordinated.</p>
<p>You must remember that this diagram represents only a tiny part of the whole sodium chloride crystal. The pattern repeats in this way over countless ions.</p>
</div>

<h3>How to Draw This Structure</h3>

<div class="text-block">
<p>Draw a perfect square:</p>
</div>

<div class="image-container">
<img src="draw1.GIF">
</div>

<div class="text-block">
<p>Now draw an identical square behind this one and offset a bit. You might have to practice a bit to get the placement of the two squares right. If you get it wrong, the ions get all tangled up with each other in your final diagram.</p>
</div>

<div class="image-container">
<img src="draw2.GIF">
</div>

<div class="text-block">
<p>Turn this into a perfect cube by joining the squares together:</p>
</div>

<div class="image-container">
<img src="draw3.GIF">
</div>

<div class="text-block">
<p>Now the tricky bit! Subdivide this big cube into 8 small cubes by joining the mid point of each edge to the mid point of the edge opposite it. To complete the process you will also have to join the mid point of each face (easily found once you've joined the edges) to the mid point of the opposite face.</p>
</div>

<div class="image-container">
<img src="draw3a.GIF">
</div>

<div class="text-block">
<p>Now all you have to do is put the ions in. Use different colours or different sizes for the two different ions, and don't forget a key. It doesn't matter whether you end up with a sodium ion or a chloride ion in the centre of the cube – all that matters is that they alternate in all three dimensions.</p>
</div>

<div class="image-container">
<img src="draw4.GIF">
</div>

<div class="text-block">
<p>You should be able to draw a perfectly adequate free-hand sketch of this in under two minutes – less than one minute if you're not too fussy!</p>
</div>

<h3>Why is Sodium chloride 6:6-co-ordinated?</h3>

<div class="text-block">
<p>The more attraction there is between the positive and negative ions, the more energy is released. The more energy that is released, the more energetically stable the structure becomes.</p>
<p>That means that to gain maximum stability, you need the maximum number of attractions. So why does each ion surround itself with 6 ions of the opposite charge?</p>
<p>That represents the maximum number of chloride ions that you can fit around a central sodium ion before the chloride ions start touching each other. If they start touching, you introduce repulsions into the crystal which makes it less stable.</p>
</div>

<h2>The Different Structure of Caesium chloride</h2>

<div class="text-block">
<p>We'll look first at the arrangement of the ions and then talk about why the structures of sodium chloride and caesium chloride are different afterwards.</p>
</div>

<div class="note">
<p>Warning: Before you go on with this section, make sure that you actually need it for your syllabus. If you don't, jump down the page to "The physical properties of sodium chloride".</p>
</div>

<h3>How the Ions are Arranged in Caesium chloride</h3>

<div class="text-block">
<p>Imagine a layer of chloride ions as shown below. The individual chloride ions aren't touching each other. That's really important – if they were touching, there would be repulsion.</p>
</div>

<div class="image-container">
<img src="cscl1.gif">
</div>

<div class="text-block">
<p>Now let's place a similarly arranged layer of caesium ions on top of these.</p>
</div>

<div class="image-container">
<img src="cscl2.gif">
</div>

<div class="text-block">
<p>Notice that the caesium ions aren't touching each other either, but that each caesium ion is resting on four chloride ions from the layer below.</p>
<p>Now let's put another layer of chloride ions on, exactly the same as the first layer. Again, the chloride ions in this layer are NOT touching those in the bottom layer – otherwise you are introducing repulsion. Since we are looking directly down on the structure, you can't see the bottom layer of chloride ions any more, of course.</p>
</div>

<div class="image-container">
<img src="cscl3.gif">
</div>

<div class="text-block">
<p>If you now think about a caesium ion sandwiched between the two layers of chloride ions, it is touching four chloride ions in the bottom layer, and another four in the top one. Each caesium ion is touched by eight chloride ions. We say that it is 8-co-ordinated.</p>
<p>If we added another layer of caesium ions, you could similarly work out that each chloride ion was touching eight caesium ions. The chloride ions are also 8-co-ordinated.</p>
<p>Overall, then, caesium chloride is 8:8-co-ordinated.</p>
<p>The final diagram in this sequence takes a slightly tilted view of the structure so that you can see how the layers build up. These diagrams are quite difficult to draw without it looking as if ions of the same charge are touching each other. They aren't!</p>
</div>

<div class="image-container">
<img src="cscl4.gif">
</div>

<div class="text-block">
<p>Diagrams of ionic crystals are usually simplified to show the most basic unit of the repeating pattern. For caesium chloride, you could, for example, draw a simple diagram showing the arrangement of the chloride ions around each caesium ion:</p>
</div>

<div class="image-container">
<img src="cscl5.gif">
</div>

<div class="text-block">
<p>By reversing the colours (green chloride ion in the centre, and orange caesium ions surrounding it), you would have an exactly equivalent diagram for the arrangement of caesium ions around each chloride ion.</p>
</div>

<div class="note">
<p>Note: These diagrams are difficult enough to draw convincingly on a computer. Trying to draw them freehand in an exam is seriously difficult. If you are doing a syllabus which wants you to know about the structure of caesium chloride, take a careful look at past exam papers and mark schemes to see exactly what sort of diagrams (if any) you need to use, and then practise them so that you can draw them quickly and well. If you haven't got any past papers and mark schemes, follow this link to the <a href="../../syllabuses.html">syllabuses page</a> to find out how to get them if you are doing a UK-based exam.</p>
</div>

<h3>Why are the Caesium chloride and Sodium chloride Structures Different?</h3>

<div class="text-block">
<p>When attractions are set up between two ions of opposite charges, energy is released. The more energy that can be released, the more stable the system becomes. That means that the more contact there is between negative and positive ions, the more stable the crystal should become.</p>
<p>If you can surround a positive ion like caesium with eight chloride ions rather than just six (and vice versa for the chloride ions), then you should have a more stable crystal. So why doesn't sodium chloride do the same thing?</p>
<p>Look again at the last diagram:</p>
</div>

<div class="image-container">
<img src="cscl5.gif">
</div>

<div class="text-block">
<p>Now imagine what would happen if you replaced the caesium ion with the smaller sodium ion. Sodium ions are, of course, smaller than caesium ions because they have fewer layers of electrons around them.</p>
<p>You still have to keep the chloride ions in contact with the sodium. The effect of this would be that the whole arrangement would shrink, bringing the chloride ions into contact with each other – and that introduces repulsion.</p>
<p>Any gain in attractions because you have eight chlorides around the sodium rather than six is more than countered by the new repulsions between the chloride ions themselves. When sodium chloride is 6:6-co-ordinated, there are no such repulsions – and so that is the best way for it to organise itself.</p>
<p>Which structure a simple 1:1 compound like NaCl or CsCl crystallises in depends on the radius ratio of the positive and the negative ions. If the radius of the positive ion is bigger than 73% of that of the negative ion, then 8:8-co-ordination is possible. Less than that (down to 41%) then you get 6:6-co-ordination.</p>
<p>In CsCl, the caesium ion is about 93% of the size of the chloride ion – so is easily within the range where 8:8-co-ordination is possible. But with NaCl, the sodium ion is only about 52% of the size of the chloride ion. That puts it in the range where you get 6:6-co-ordination.</p>
</div>

<div class="note">
<p>Note: What happens below 41%? At this point the negative ions will touch each other again even with 6:6-co-ordination. A new arrangement (known as 4:4-co-ordination) then becomes necessary. This is beyond any syllabus that I am currently tracking.</p>
</div>

<h2>The Physical Properties of Sodium chloride</h2>

<div class="text-block">
<p>Sodium chloride is taken as typical of ionic compounds, and is chosen rather than, say, caesium chloride, because it is found on every syllabus at this level.</p>
</div>

<h3>Sodium chloride has a High Melting and Boiling Point</h3>

<div class="text-block">
<p>There are strong electrostatic attractions between the positive and negative ions, and it takes a lot of heat energy to overcome them. Ionic substances all have high melting and boiling points. Differences between ionic substances will depend on things like:</p>
</div>

<ul>

<li>The number of charges on the ions<br>Magnesium oxide has exactly the same structure as sodium chloride, but a much higher melting and boiling point. The 2+ and 2- ions attract each other more strongly than 1+ attracts 1-.</li>

<li>The sizes of the ions<br>If the ions are smaller they get closer together and so the electrostatic attractions are greater. Rubidium iodide, for example, melts and boils at slightly lower temperatures than sodium chloride, because both rubidium and iodide ions are bigger than sodium and chloride ions. The attractions are less between the bigger ions and so less heat energy is needed to separate them.</li>
</ul>

<h3>Sodium chloride Crystals are Brittle</h3>

<div class="text-block">
<p>Brittleness is again typical of ionic substances. Imagine what happens to the crystal if a stress is applied which shifts the ion layers slightly.</p>
</div>

<div class="image-container">
<img src="brittle.GIF">
</div>

<div class="text-block">
<p>Ions of the same charge are brought side-by-side and so the crystal repels itself to pieces!</p>
</div>
<h3>Sodium chloride is Soluble in Water</h3>

<div class="text-block">
<p>Many ionic solids are soluble in water – although not all. It depends on whether there are big enough attractions between the water molecules and the ions to overcome the attractions between the ions themselves. Positive ions are attracted to the lone pairs on water molecules and co-ordinate (dative covalent) bonds may form. Water molecules form hydrogen bonds with negative ions.</p>
</div>

<div class="note">
<p>Note: The bonding in hydrated metal ions is covered in the page on <a href="../bonding/dative.html#top">co-ordinate bonding</a>. The bonding between negative ions like chloride ions and water molecules is covered in the page on <a href="../bonding/hbond.html#top">hydrogen bonding</a>.</p>
</div>

<h3>Sodium chloride is Insoluble in Organic Solvents</h3>

<div class="text-block">
<p>This is also typical of ionic solids. The attractions between the solvent molecules and the ions aren't big enough to overcome the attractions holding the crystal together.</p>
</div>

<h3>The Electrical Behaviour of Sodium chloride</h3>

<div class="text-block">
<p>Solid sodium chloride doesn't conduct electricity, because there are no electrons which are free to move. When it melts, sodium chloride undergoes electrolysis, which involves conduction of electricity because of the movement and discharge of the ions. In the process, sodium and chlorine are produced. This is a chemical change rather than a physical process.</p>
<p>The positive sodium ions move towards the negatively charged electrode (the cathode). When they get there, each sodium ion picks up an electron from the electrode to form a sodium atom. These float to the top of the melt as molten sodium metal. (And assuming you are doing this open to the air, this immediately catches fire and burns with an orange flame.)</p>
</div>

<div class="block-formula">
Na^+ + e^- \longrightarrow Na
</div>

<div class="text-block">
<p>The movement of electrons from the cathode onto the sodium ions leaves spaces on the cathode. The power source (the battery or whatever) moves electrons along the wire in the external circuit to fill those spaces. That flow of electrons would be seen as an electric current. (The external circuit is all the rest of the circuit apart from the molten sodium chloride.)</p>
<p>Meanwhile, chloride ions are attracted to the positive electrode (the anode). When they get there, each chloride ion loses an electron to the anode to form an atom. These then pair up to make chlorine molecules. Chlorine gas is produced. Overall, the change is</p>
</div>

<div class="block-formula">
2Cl^- \longrightarrow Cl_2 + 2e^-
</div>

<div class="text-block">
<p>The new electrons deposited on the anode are pumped off around the external circuit by the power source, eventually ending up on the cathode where they will be transferred to sodium ions.</p>
<p>Molten sodium chloride conducts electricity because of the movement of the ions in the melt, and the discharge of the ions at the electrodes. Both of these have to happen if you are to get electrons flowing in the external circuit.</p>
<p>In solid sodium chloride, of course, that ion movement can't happen and that stops any possibility of any current flow in the circuit.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-ionicstructs.pdf" target="_blank">Questions on ionic structures</a>
<a href="../questions/a-ionicstructs.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../structsmenu.html#top">To the structures menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>