<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Deciding Bond Type from Physical Properties | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how you can decide what sort of structure a substance has by looking at its physical properties">
<meta name="keywords" content="structure, physical properties, melting point, boiling point, solid, liquid, gas">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Deciding Bond Type from Physical Properties</h1>

<div class="text-block">
<p>This page explains how you can decide what sort of structure a substance has by looking at its physical properties.</p>
<p>The page originally had a brief kinetic theory description of solids, liquids and gases. That has now been transferred to a separate <a href="../../physical/kt/basic.html#top">introduction to kinetic theory</a> page in the physical chemistry section of Chemguide.</p>
</div>

<h2>Deducing the Type of Bonding from Physical Properties</h2>

<h3>Melting and Boiling Points</h3>

<div class="text-block">
<p>The best place to start is usually the physical state.</p>
<p>Melting point isn't always a good guide to the size of the attractions between particles, because the attractive forces have only been loosened on melting – not broken entirely. Boiling point is a much better guide, because enough heat has now been supplied to break the attractive forces completely. The stronger the attractions, the higher the boiling point.</p>
<p>That being said, melting points are often used to judge the size of attractive forces between particles in solids, but you will find the occasional oddity. Those oddities usually disappear if you consider boiling points instead.</p>
</div>

<div class="note">
<p>As an example: You would expect stronger metallic bonding in aluminium than in magnesium, because aluminium has 3 electrons to delocalise into the "sea of electrons" rather than magnesium's 2. The boiling points reflect this: Al 2470°C, Mg 1110°C. However, aluminium's melting point is only 10°C higher than magnesium's: Al 660°C, Mg 650°C. (I've never found a good explanation for this!) If you need some more background on <a href="../bonding/metallic.html#top">metallic bonding</a>, you could follow this link.</p>
</div>

<div class="text-block">
<p>So...</p>
<p>If it is a gas, liquid or low melting point solid, it will consist of covalently bound molecules (except the noble gases which have molecules consisting of single atoms). The size of the melting point or boiling point gives a guide to the strength of the intermolecular forces.</p>
<p>That is then the end of the problem. If it is a gas, liquid or low melting point solid then you are talking about a simple molecular substance. Full stop!</p>
<p>If it is a high melting point solid, it will be a giant structure – either ionic, metallic or giant covalent.</p>
<p>You now have to sort out which of these it is.</p>
</div>

<h3>Effect of Water</h3>

<div class="text-block">
<p>Solubility of a solid in water (without reaction) suggests it is ionic.</p>
<p>There are exceptions to this, of course. Sugar (sucrose) is soluble in water despite being a covalent molecule. It is capable of extensive hydrogen bonding with water molecules. And there are a lot of ionic compounds which are insoluble in water, of course.</p>
<p>Solubility of a low melting point solid or a liquid in water (without it reacting) suggests a small molecule capable of hydrogen bonding – or, at least, a small very polar molecule.</p>
</div>

<h3>Conduction of Electricity</h3>

<div class="text-block">
<p>Conduction of electricity in the solid state suggests delocalised electrons, and therefore either a metal or graphite (or graphene if your syllabus mentions it). The clue as to which you had would usually come from other data – appearance, malleability, etc (see below).</p>
</div>

<div class="note">
<p>Note: Semi-conductors like silicon – a giant covalent structure with the same arrangement of atoms as diamond – also conduct electricity. The theory of semi-conductors is beyond A level (or equivalent) chemistry syllabuses.</p>
</div>

<div class="text-block">
<p>If a substance doesn't conduct electricity as a solid, but undergoes electrolysis when it is molten, that would confirm that it was ionic.</p>
</div>

<div class="note">
<p>Note: Electrolysis is the splitting up of a compound using electricity. For example, molten sodium chloride conducts electricity and is split into sodium and chlorine in the process.</p>
</div>

<h3>Appearance etc.</h3>

<div class="text-block">
<p>Don't forget about obvious things like the shiny appearance of most metals, and their ease of working. Metals are malleable (can be bent or beaten into shape easily) and ductile (can be pulled out into wires). By contrast, giant ionic or giant covalent structures tend to be brittle – shattering rather than bending.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-whatsort.pdf" target="_blank">Questions on deciding types of structures</a>
<a href="../questions/a-whatsort.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../structsmenu.html#top">To the structures menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>