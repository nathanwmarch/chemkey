<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Molecular Substances | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of the physical properties of simple molecular substances including iodine, ice and polythene.">
<meta name="keywords" content="molecular, structure, physical properties, melting point, boiling point, hydrogen bond, van der waals, iodine, ice, polythene, poly(ethene), polymers">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Molecular Substances</h1>

<div class="text-block">
<p>This page describes how the physical properties of substances having molecular structures varies with the type of intermolecular attractions – hydrogen bonding or van der Waals forces.</p>
</div>

<div class="note">
<p>Important! There's not much point in reading this page unless you are reasonably happy about the origin of <a href="../bonding/hbond.html#top">hydrogen bonding</a> and <a href="../bonding/vdw.html#top">van der Waals forces</a>. Follow these links first if you aren't sure about these.</p>
</div>

<h2>The Physical Properties of Molecular Substances</h2>

<div class="text-block">
<p>Molecules are made of fixed numbers of atoms joined together by covalent bonds, and can range from the very small (even down to single atoms, as in the noble gases) to the very large (as in polymers, proteins or even DNA).</p>
<p>The covalent bonds holding the molecules together are very strong, but these are largely irrelevant to the physical properties of the substance. Physical properties are governed by the intermolecular forces – forces attracting one molecule to its neighbours – van der Waals attractions or hydrogen bonds.</p>
</div>

<h3>Melting and Boiling Points</h3>

<div class="text-block">
<p>Molecular substances tend to be gases, liquids or low melting point solids, because the intermolecular forces of attraction are comparatively weak. You don't have to break any covalent bonds in order to melt or boil a molecular substance.</p>
</div>

<div class="note">
<p>Note: This is really important! You can make yourself look extremely stupid if you imply in an exam that boiling water, for example, splits it into hydrogen and oxygen by breaking covalent bonds. Exactly the same water molecules are present in ice, water and steam.</p>
</div>

<div class="text-block">
<p>The size of the melting or boiling point will depend on the strength of the intermolecular forces. The presence of hydrogen bonding will lift the melting and boiling points. The larger the molecule the more van der Waals attractions are possible – and those will also need more energy to break.</p>
</div>

<h3>Solubility in Water</h3>

<div class="text-block">
<p>Most molecular substances are insoluble (or only very sparingly soluble) in water. Those which do dissolve often react with the water, or else are capable of forming hydrogen bonds with the water.</p>
</div>

<h4>Why doesn't methane, CH<sub>4</sub>, dissolve in water?</h4>

<div class="text-block">
<p>The methane itself isn't the problem. Methane is a gas, and so its molecules are already separate – the water doesn't need to pull them apart from one another.</p>
<p>The problem is the hydrogen bonds between the water molecules. If methane were to dissolve, it would have to force its way between water molecules and so break hydrogen bonds. That costs a reasonable amount of energy.</p>
<p>The only attractions possible between methane and water molecules are the much weaker van der Waals forces – and not much energy is released when these are set up. It simply isn't energetically profitable for the methane and water to mix.</p>
</div>

<h4>Why does ammonia, NH<sub>3</sub>, dissolve in water?</h4>

<div class="text-block">
<p>Ammonia has the ability to form hydrogen bonds. When the hydrogen bonds between water molecules are broken, they can be replaced by equivalent bonds between water and ammonia molecules.</p>
<p>Some of the ammonia also reacts with the water to produce ammonium ions and hydroxide ions.</p>
</div>

<div class="block-formula">
NH_3 + H_2O \xrightleftharpoons{} {NH_4}^+ + OH^-
</div>

<div class="text-block">
<p>The reversible arrows show that the reaction doesn't go to completion. At any one time only about 1% of the ammonia has actually reacted to form ammonium ions. The solubility of ammonia is mainly due to the hydrogen bonding and not the reaction.</p>
<p>Other common substances which are freely soluble in water because they can hydrogen bond with water molecules include ethanol (alcohol) and sucrose (sugar).</p>
</div>

<h3>Solubility in Organic Solvents</h3>

<div class="text-block">
<p>Molecular substances are often soluble in organic solvents – which are themselves molecular. Both the solute (the substance which is dissolving) and the solvent are likely to have molecules attracted to each other by van der Waals forces. Although these attractions will be disrupted when they mix, they are replaced by similar ones between the two different sorts of molecules.</p>
</div>

<h3>Electrical Conductivity</h3>

<div class="text-block">
<p>Molecular substances won't conduct electricity. Even in cases where electrons may be delocalised within a particular molecule, there isn't sufficient contact between the molecules to allow the electrons to move through the whole solid or liquid.</p>
</div>

<h2>Some Individual Examples</h2>

<h3>Iodine, I<sub>2</sub></h3>

<div class="text-block">
<p>Iodine is a dark grey crystalline solid with a purple vapour. Melting point: 114°C. Boiling point: 184°C. It is very, very slightly soluble in water, but dissolves freely in organic solvents.</p>
<p>Iodine is therefore a low melting point solid. The crystallinity suggests a regular packing of the molecules.</p>
</div>

<div class="image-container">
<img src="i2.GIF">
</div>

<div class="text-block">
<p>The structure is described as face centred cubic – it is a cube of iodine molecules with another molecule at the centre of each face.</p>
<p>The orientation of the iodine molecules within this structure is quite difficult to draw (let alone remember!). If your syllabus and past exam papers suggests that you need to remember it, look carefully at the next sequence of diagrams showing the layers.</p>
</div>

<div class="image-container">
<img src="i2layers.GIF">
</div>

<div class="text-block">
<p>Notice that as you look down on the cube, all the molecules on the left and right hand sides are aligned the same way. The ones in the middle are aligned in the opposite way.</p>
<p>All these diagrams show an "exploded" view of the crystal. The iodine molecules are, of course, touching each other. Measurements of the distances between the centres of the atoms in the crystal show two different values:</p>
</div>

<div class="image-container">
<img src="i2lengths.GIF">
</div>

<div class="text-block">
<p>The iodine atoms within each molecule are pulled closely together by the covalent bond. The van der Waals attraction between the molecules is much weaker, and you can think of the atoms in two separate molecules as just loosely touching each other.</p>
</div>

<h3>Ice</h3>

<div class="text-block">
<p>Ice is a good example of a hydrogen bonded solid.</p>
<p>There are lots of different ways that the water molecules can be arranged in ice. This is one of them, but NOT the common one – I can't draw that in any way that makes sense! The one below is known as "cubic ice", or "ice Ic". It is based on the water molecules arranged in a diamond structure.</p>
</div>

<div class="image-container">
<img src="ice.GIF">
</div>

<div class="text-block">
<p>This is just a small part of a structure which extends over huge numbers of molecules in three dimensions. In the diagram, the lines represent hydrogen bonds. The lone pairs that the hydrogen atoms are attracted to are left out for clarity.</p>
<p>Cubic ice is only stable at temperatures below -80°C. The ice you are familiar with has a different, hexagonal structure. It is called "ice Ih".</p>
</div>

<div class="note">
<p>Note: Don't worry about this problem. If asked to draw ice in an exam at this level (16 – 18 year olds), don't try to be too clever. It is probably best not to go beyond the top five molecules in the above diagram. This will show the essential features of the bonding in the structure without getting bogged down in stuff which is far beyond this level. If you are interested in following this up, try a Google search using the search term ice structure hexagonal cubic (or something similar). This will throw up lots of information together with an assortment of fairly dreadful diagrams which I for one don't have the visual imagination to unscramble!</p>
</div>

<h4>The unusual density behaviour of water</h4>

<div class="text-block">
<p>The hydrogen bonding forces a rather open structure on the ice – if you made a model of it, you would find a significant amount of wasted space. When ice melts, the structure breaks down and the molecules tend to fill up this wasted space.</p>
<p>This means that the water formed takes up less space than the original ice. Ice is a very unusual solid in this respect – most solids show an increase in volume on melting.</p>
<p>When water freezes, the opposite happens – there is an expansion as the hydrogen bonded structure establishes. Most liquids contract on freezing.</p>
<p>Remnants of the rigid hydrogen bonded structure are still present in very cold liquid water, and don't finally disappear until 4°C. From 0°C to 4°C, the density of water increases as the molecules free themselves from the open structure and take up less space. After 4°C, the thermal motion of the molecules causes them to move apart and the density falls. That's the normal behaviour with liquids on heating.</p>
</div>

<div class="note">
<p>Note: You can find more about water (particularly its abnormally high boiling point) in the page on <a href="../bonding/hbond.html#top">hydrogen bonding</a>.</p>
</div>

<h3>Polymers</h3>

<h4>Bonding in polymers</h4>

<div class="text-block">
<p>Polymers like poly(ethene) – commonly called polythene – consist of very long molecules. Poly(ethene) molecules are made by joining up lots of ethene molecules into chains of covalently bound carbon atoms with hydrogens attached. There may be short branches along the main chain, also consisting of carbon chains with attached hydrogens. The molecules are attracted to each other in the solid by van der Waals dispersion forces.</p>
<p>By controlling the conditions under which ethene is polymerised, it is possible to control the amount of branching to give two distinct types of polythene.</p>
</div>

<h4>High density polythene</h4>

<div class="text-block">
<p>High density polythene has chains which have very few branches. The lack of branching allows molecules to lie close together in a regular way which is almost crystalline.</p>
<p>Because the molecules lie close together, dispersion forces are more effective, and so the plastic is relatively strong and has a somewhat higher melting point than low density polythene.</p>
<p>High density polythene is used for containers for household chemicals like washing-up liquid, for example, or for bowls or buckets.</p>
</div>

<h4>Low density polythene</h4>

<div class="text-block">
<p>Low density polythene has lots of short branches along the chain. These branches prevent the chains from lying close together in a tidy arrangement. As a result dispersion forces are less and the plastic is weaker and has a lower melting point. Its density is lower, of course, because of the wasted space within the unevenly packed structure.</p>
<p>Low density polythene is used for things like plastic bags.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-molecular.pdf" target="_blank">Questions on molecular structures</a>
<a href="../questions/a-molecular.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../structsmenu.html#top">To the structures menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>