<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Metal Structures | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A simple view of metallic structures and how they affect the physical properties of metals">
<meta name="keywords" content="metal, metallic, structure, giant, physical properties, melting point, boiling point, hardness, conductivity, conduction, electricity, ductile, malleable, alloy, thermal">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Metallic Structures</h1>

<div class="text-block">
<p>This page decribes the structure of metals, and relates that structure to the physical properties of the metal.</p>
</div>

<h2>The Structure of Metals</h2>

<h3>The Arrangement of the Atoms</h3>

<div class="text-block">
<p>Metals are giant structures of atoms held together by metallic bonds. "Giant" implies that large but variable numbers of atoms are involved – depending on the size of the bit of metal.</p>
</div>

<div class="note">
<p>Note: Before you go on, it might be a good idea to read the page on <a href="../bonding/metallic.html#top">bonding in metals</a> unless you are reasonably happy about the idea of the delocalised electrons ("sea of electrons") in metals.</p>
</div>

<h4>12-co-ordination</h4>

<div class="text-block">
<p>Most metals are close packed – that is, they fit as many atoms as possible into the available volume. Each atom in the structure has 12 touching neighbours. Such a metal is described as 12-co-ordinated.</p>
<p>Each atom has 6 other atoms touching it in each layer.</p>
</div>

<div class="image-container">
<img src="12coord1.GIF">
</div>

<div class="text-block">
<p>There are also 3 atoms touching any particular atom in the layer above and another 3 in the layer underneath.</p>
</div>

<div class="image-container">
<img src="12coord2.GIF">
</div>

<div class="text-block">
<p>This second diagram shows the layer immediately above the first layer. There will be a corresponding layer underneath. (There are actually two different ways of placing the third layer in a close packed structure, but that goes beyond the requirements of current A-level syllabuses.)</p>
</div>

<h4>8-co-ordination</h4>

<div class="text-block">
<p>Some metals (notably those in Group 1 of the Periodic Table) are packed less efficiently, having only 8 touching neighbours. These are 8-co-ordinated.</p>
</div>

<div class="image-container">
<img src="8coord.GIF">
</div>

<div class="text-block">
<p>The left hand diagram shows that no atoms are touching each other within a particular layer . They are only touched by the atoms in the layers above and below. The right hand diagram shows the 8 atoms (4 above and 4 below) touching the darker coloured one.</p>
</div>

<h4>Crystal grains</h4>

<div class="text-block">
<p>It would be misleading to suppose that all the atoms in a piece of metal are arranged in a regular way. Any piece of metal is made up of a large number of "crystal grains", which are regions of regularity. At the grain boundaries atoms have become misaligned.</p>
</div>

<div class="image-container">
<img src="dislocate.GIF">
</div>

<div class="note">
<p>Note: Within a crystal grain you get rather subtle irregularities known as dislocations. It isn't important to know about these for UK A-level Chemistry (or equivalent) purposes, although they turn out to be essential in discussing the workability of metals at a higher level. I haven't included a description of them here because it is quite difficult to visualise how they work, and I don't want to add unnecessary complications.</p>
</div>

<h2>The Physical Properties of Metals</h2>

<h3>Melting Points and Boiling Points</h3>

<div class="text-block">
<p>Metals tend to have high melting and boiling points because of the strength of the metallic bond. The strength of the bond varies from metal to metal and depends on the number of electrons which each atom delocalises into the sea of electrons, and on the packing.</p>
<p>Group 1 metals like sodium and potassium have relatively low melting and boiling points mainly because each atom only has one electron to contribute to the bond – but there are other problems as well:</p>
</div>

<ul>
<li>Group 1 elements are also inefficiently packed (8-co-ordinated), so that they aren't forming as many bonds as most metals.</li>

<li>They have relatively large atoms (meaning that the nuclei are some distance from the delocalised electrons) which also weakens the bond.</li>
</ul>

<h3>Electrical Conductivity</h3>

<div class="text-block">
<p>Metals conduct electricity. The delocalised electrons are free to move throughout the structure in 3-dimensions. They can cross grain boundaries. Even though the pattern may be disrupted at the boundary, as long as atoms are touching each other, the metallic bond is still present.</p>
<p>Liquid metals also conduct electricity, showing that although the metal atoms may be free to move, the delocalisation remains in force until the metal boils.</p>
</div>

<h3>Thermal Conductivity</h3>

<div class="text-block">
<p>Metals are good conductors of heat. Heat energy is picked up by the electrons as additional kinetic energy (it makes them move faster). The energy is transferred throughout the rest of the metal by the moving electrons.</p>
</div>

<h3>Strength and Workability</h3>

<h4>Malleability and ductility</h4>

<div class="text-block">
<p>Metals are described as malleable (can be beaten into sheets) and ductile (can be pulled out into wires). This is because of the ability of the atoms to roll over each other into new positions without breaking the metallic bond.</p>
<p>If a small stress is put onto the metal, the layers of atoms will start to roll over each other. If the stress is released again, they will fall back to their original positions. Under these circumstances, the metal is said to be elastic.</p>
</div>

<div class="image-container">
<img src="smallstress.GIF">
</div>

<div class="text-block">
<p>If a larger stress is put on, the atoms roll over each other into a new position, and the metal is permanently changed.</p>
</div>

<div class="image-container">
<img src="bigstress.GIF">
</div>

<h4>The hardness of metals</h4>

<div class="text-block">
<p>This rolling of layers of atoms over each other is hindered by grain boundaries because the rows of atoms don't line up properly. It follows that the more grain boundaries there are (the smaller the individual crystal grains), the harder the metal becomes.</p>
<p>Offsetting this, because the grain boundaries are areas where the atoms aren't in such good contact with each other, metals tend to fracture at grain boundaries. Increasing the number of grain boundaries not only makes the metal harder, but also makes it more brittle.</p>
</div>

<h4>Controlling the size of the crystal grains</h4>

<div class="text-block">
<p>If you have a pure piece of metal, you can control the size of the grains by heat treatment or by working the metal.</p>
<p>Heating a metal tends to shake the atoms into a more regular arrangement – decreasing the number of grain boundaries, and so making the metal softer. Banging the metal around when it is cold tends to produce lots of small grains. Cold working therefore makes a metal harder. To restore its workability, you would need to reheat it.</p>
<p>You can also break up the regular arrangement of the atoms by inserting atoms of a slightly different size into the structure. Alloys such as brass (a mixture of copper and zinc) are harder than the original metals because the irregularity in the structure helps to stop rows of atoms from slipping over each other.</p>
</div>

<div class="image-container">
<img src="alloy.gif">
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-metalstruct.pdf" target="_blank">Questions on metal structures</a>
<a href="../questions/a-metalstruct.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../structsmenu.html#top">To the structures menu</a>
<a href="../../atommenu.html#top">To the atomic structure and bonding menu</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>