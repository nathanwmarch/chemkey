<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>What is Electrophilic Eubstitution? | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of the terms electrophile and electrophilic substitution, together with a general mechanism for this sort of reaction involving benzene.">
<meta name="keywords" content="benzene, arenes, electrophile, electrophilic substitution, substitution">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>What is Electrophilic Eubstitution?</h1>

<h3>Background</h3>

<div class="text-block">
<p>Electrophilic substitution happens in many of the reactions of compounds containing benzene rings – the arenes. For simplicity, we'll only look for now at benzene itself.</p>
</div>

<div class="note">
<p>Note: Before you start it would be a good idea if you had a clear idea about the structure of benzene. Check your syllabus now to find out what you need to know, and then read the page on the <a href="../../basicorg/bonding/benzene2.html#top">modern orbital view of benzene</a> in the organic bonding section of this site. Don't forget to look in the section(s) in your syllabus on bonding as well as organic chemistry.</p>
<p>Haven't got a <a href="../../syllabuses.html#top">syllabus</a>? If you are working towards a UK-based exam (A level or its equivalent), follow this link to find out how to get one.</p>
</div>

<div class="text-block">
<p>This is what you need to understand for the purposes of the electrophilic substitution mechanisms:</p>
</div>

<ul>
<li>Benzene, C<sub>6</sub>H<sub>6</sub>, is a planar molecule containing a ring of six carbon atoms each with a hydrogen atom attached.</li>
<li>There are delocalised electrons above and below the plane of the ring.</li>

<div class="image-container"><img src="benzenedeloc.GIF"></div>

<li>The presence of the delocalised electrons makes benzene particularly stable.</li>
<li>Benzene resists addition reactions because that would involve breaking the delocalisation and losing that stability.</li>

<div class="image-container">
<img src="benzene.GIF">
</div>

<li>Benzene is represented by this symbol, where the circle represents the delocalised electrons, and each corner of the hexagon has a carbon atom with a hydrogen attached.</li>
</ul>

<h2>Electrophilic Substitution Reactions Involving Positive Ions</h2>

<h3>Benzene and Electrophiles</h3>

<div class="text-block">
<p>Because of the delocalised electrons exposed above and below the plane of the rest of the molecule, benzene is obviously going to be highly attractive to electrophiles – species which seek after electron rich areas in other molecules.</p>
</div>

<div class="definition">
<p>Species: A useful word which can mean any particle you want it to mean – an atom, a molecule, an ion or a free radical.</p>
</div>

<div class="text-block">
<p>The electrophile will either be a positive ion, or the slightly positive end of a polar molecule.</p>
</div>

<div class="note">
<p>Help! If you aren't sure what a polar molecule is, read about <a href="../../basicorg/bonding/eneg.html#top">electronegativity and polar bonds</a> before you go on.</p>
</div>

<div class="text-block">
<p>The delocalised electrons above and below the plane of the benzene molecule are open to attack in the same way as those above and below the plane of an ethene molecule. However, the end result will be different.</p>
</div>
 
<div class="image-container"><img src="ethandben.GIF"></div>

<div class="text-block">
<p>If benzene underwent addition reactions in the same way as ethene, it would need to use some of the delocalised electrons to form bonds with the new atoms or groups. This would break the delocalisation – and this costs energy.</p>
</div>

<div class="note">
<p>Note: You can read about <a href="../eladd/whatis.html#top">electrophilic addition to ethene</a> if you are interested.</p>
</div>

<div class="text-block">
<p>Instead, it can maintain the delocalisation if it replaces a hydrogen atom by something else – a substitution reaction. The hydrogen atoms aren't involved in any way with the delocalised electrons.</p>
<p>In most of benzene's reactions, the electrophile is a positive ion, and these reactions all follow a general pattern.</p>
</div>

<h3>The General Mechanism</h3>

<h4>The first stage</h4>

<div class="text-block">
<p>Suppose the electrophile is a positive ion X<sup>+</sup>.</p>
<p>Two of the electrons in the delocalised system are attracted towards the X<sup>+</sup> and form a bond with it. This has the effect of breaking the delocalisation, although not completely.</p>
</div>

<div class="image-container"><img src="benzgenma.GIF"></div>

<div class="note">
<p>Note: If you aren't sure about the use of <a href="../../basicorg/conventions/curlies.html#top">curly arrows</a> in mechanisms, you must follow this link before you go on.</p>
</div>

<div class="text-block">
<p>The ion formed in this step isn't the final product. It immediately goes on to react with something else. It is just an intermediate.</p>
</div>

<div class="image-container">
<img src="benzinter.GIF">
</div>

<div class="text-block">
<p>There is still delocalisation in the intermediate formed, but it only covers part of the ion. When you write one of these mechanisms, draw the partial delocalisation to take in all the carbon atoms apart from the one that the X has become attached to.</p>
<p>The intermediate ion carries a positive charge because you are joining together a neutral molecule and a positive ion. This positive charge is spread over the delocalised part of the ring. Simply draw the "+" in the middle of the ring.</p>
<p>The hydrogen at the top isn't new – it's the hydrogen that was already attached to that carbon. We need to show that it is there for the next stage.</p>
</div>

<h4>The second stage</h4>

<div class="image-container"><img src="benzgenmb.GIF"></div>

<div class="text-block">
<p>Here we've introduced a new ion, Y<sup>-</sup>. Where did this come from? You have to remember that it is impossible to get a positive ion on its own in a chemical system – so Y<sup>-</sup> is simply the negative ion that was originally associated with X<sup>+</sup>. Don't worry about this at the moment – it's much easier to see when you've got a real example in front of you.</p>
</div>

<div class="image-container">
<img src="benzinterandy.GIF">
</div>

<div class="text-block">
<p>A lone pair of electrons on Y<sup>-</sup> forms a bond with the hydrogen atom at the top of the ring. That means that the pair of electrons joining the hydrogen onto the ring aren't needed any more. These then move down to plug the gap in the delocalised electrons, so restoring the delocalised ring of electrons which originally gave the benzene its special stability.</p>
</div>

<h4>The energetics of the reaction</h4>

<div class="text-block">
<p>The complete delocalisation is temporarily broken as X replaces H on the ring, and this costs energy. However, that energy is recovered when the delocalisation is re-established. This initial input of energy is simply the activation energy for the reaction. In this case, it is going to be high (something around 150 kJ mol<sup>-1</sup>), and this means that benzene's reactions tend to be slow.</p>
</div>

<h2>Electrophilic Substitution Reactions Not Involving Positive Ions</h2>

<h3>Halogenation and Sulfonation</h3>

<div class="text-block">
<p>In these reactions, the electrophiles are polar molecules rather than fully positive ions.</p>
<p>Because these mechanisms are different from what's gone before (and from each other), there isn't any point in dealing with them in a general way. You will find them explained in full if you follow the link to the electrophilic substitution menu below.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../elsubmenu.html#top">To menu of electrophilic substitution reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>