<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>What is Electrophilic Addition? | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of the terms electrophile and electrophilic addition, together with a general mechanism for this sort of reaction">
<meta name="keywords" content="ethene, alkenes, electrophile, electrophilic addition, carbonium ion, carbocation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>What is Electrophilic Addition?</h1>

<h3>Background</h3>

<div class="text-block">
<p>Electrophilic addition happens in many of the reactions of compounds containing carbon-carbon double bonds – the alkenes.</p>
</div>

<h2>The Structure of Ethene</h2>

<div class="text-block">
<p>We are going to start by looking at ethene, because it is the simplest molecule containing a carbon-carbon double bond. What is true of C=C in ethene will be equally true of C=C in more complicated alkenes.</p>
</div>

<div class="image-container">
<img src="ethenemodel.GIF">
</div>

<div class="text-block">
<p>Ethene, C<sub>2</sub>H<sub>4</sub>, is often modelled as shown on the right. The double bond between the carbon atoms is, of course, two pairs of shared electrons. What the diagram doesn't show is that the two pairs aren't the same as each other.</p>
<p>One of the pairs of electrons is held on the line between the two carbon nuclei as you would expect, but the other is held in a molecular orbital above and below the plane of the molecule. A molecular orbital is a region of space within the molecule where there is a high probability of finding a particular pair of electrons.</p>
</div>

<div class="image-container">
<img src="ethenebonds.GIF">
</div>

<div class="text-block">
<p>In this diagram, the line between the two carbon atoms represents a normal bond – the pair of shared electrons lies in a molecular orbital on the line between the two nuclei where you would expect them to be. This sort of bond is called a sigma bond.</p>
<p>The other pair of electrons is found somewhere in the shaded part above and below the plane of the molecule. This bond is called a &pi; bond. The electrons in the &pi; bond are free to move around anywhere in this shaded region and can move freely from one half to the other.</p>
</div>

<div class="note">
<p>Note: This diagram shows a side view of an ethene molecule. The dotted lines to two of the hydrogens show bonds going back into the screen or paper away from you. The wedge shapes show bonds coming out towards you.</p>
</div>

<div class="text-block">
<p>The &pi;electrons are not as fully under the control of the carbon nuclei as the electrons in the sigma bond and, because they lie exposed above and below the rest of the molecule, they are relatively open to attack by other things.</p>
</div>

<div class="note">
<p>Note: Check your <a href="../../syllabuses.html#top">syllabus</a> to see if you need to know how a &pi; bond is formed. Haven't got a syllabus? If you are working towards a UK-based exam, find out how to get one by following this link.</p>
<p>If you do need to know about the <a href="../../basicorg/bonding/ethene.html#top">bonding in ethene</a> in detail, follow this link as well.</p>
</div>

<h3>Electrophiles</h3>

<div class="text-block">
<p>An electrophile is something which is attracted to electron-rich regions in other molecules or ions. Because it is attracted to a negative region, an electrophile must be something which carries either a full positive charge, or has a slight positive charge on it somewhere.</p>
</div>

<div class="note">
<p>Note: The ending ~phile means a liking for. For example, a francophile is someone who likes the French; an anglophile is someone who likes the English.</p>
</div>

<div class="image-container">
<img src="hbrpolar.GIF">
</div>

<div class="text-block">
<p>Ethene and the other alkenes are attacked by electrophiles. The electrophile is normally the slightly positive (&delta;+) end of a molecule like hydrogen bromide, HBr.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about why some bonds are polar, read the page on <a href="../../basicorg/bonding/eneg.html#top">electronegativity</a>.</p>
</div>

<div class="text-block">
<p>Electrophiles are strongly attracted to the exposed electrons in the &pi; bond and reactions happen because of that initial attraction – as you will see shortly.</p>
<p>You might wonder why fully positive ions like sodium, Na<sup>+</sup>, don't react with ethene. Although these ions may well be attracted to the &pi; bond, there is no possibility of the process going any further to form bonds between sodium and carbon, because sodium forms ionic bonds, whereas carbon normally forms covalent ones.</p>
</div>

<h3>Addition Reactions</h3>

<div class="text-block">
<p>In a sense, the &pi; bond is an unnecessary bond. The structure would hold together perfectly well with a single bond rather than a double bond. The &pi; bond often breaks and the electrons in it are used to join other atoms (or groups of atoms) onto the ethene molecule. In other words, ethene undergoes addition reactions.</p>
<p>For example, using a general molecule X-Y</p>
</div>

<div class="image-container"><img src="eladdeq1.GIF"></div>

<div class="summary">
<p>An electrophilic addition reaction is a reaction in which two molecules join together to make a bigger one. Nothing is lost in the process. All the atoms in the original molecules are found in the bigger one.</p>
<p>An electrophilic addition reaction is an addition reaction which happens because what we think of as the "important" molecule is attacked by an electrophile. The "important" molecule has a region of high electron density which is attacked by something carrying some degree of positive charge.</p>
</div>

<div class="note">
<p>Note: When we talk about reactions of alkenes like ethene, we think of the ethene as being attacked by other molecules such as hydrogen bromide. Because ethene is the molecule we are focusing on, we quite arbitrarily think of it as the central molecule and hydrogen bromide as its attacker.</p>
<p>There's no real justification for this, of course, apart from the fact that it helps to put things in some sort of logical pattern. In reality, the molecules just collide and may react if they have enough energy and if they are lined up correctly.</p>
</div>

<h2>Understanding the Electrophilic Addition Mechanism</h2>

<h3>The Mechanism for the Reaction Between Ethene and a Molecule X-Y</h3>

<div class="text-block">
<p>It is very unlikely that any two different atoms joined together will have the same electronegativity. We are going to assume that Y is more electronegative than X, so that the pair of electrons is pulled slightly towards the Y end of the bond. That means that the X atom carries a slight positive charge.</p>
</div>

<div class="note">
<p>Note: Once again, if you aren't sure about <a href="../../basicorg/bonding/eneg.html#top">electronegativity and bond polarity</a> follow this link before you read on.</p>
</div>

<div class="text-block">
<p>The slightly positive X atom is an electrophile and is attracted to the exposed &pi; bond in the ethene. Now imagine what happens as they approach each other.</p>
</div>

<div class="image-container"><img src="ethenexy1.GIF"></div>

<div class="text-block">
<p>You are now much more likely to find the electrons in the half of the &pi; bond nearest the XY. As the process continues, the two electrons in the &pi; bond move even further towards the X until a covalent bond is made.</p>
<p>The electrons in the X-Y bond are pushed entirely onto the Y to give a negative Y<sup>-</sup> ion.</p>
</div>

<div class="image-container"><img src="ethenexy2.GIF"></div>

<div class="note">
<p>Help! Why does the carbon atom have a positive charge? The &pi; bond was originally made using an electron from each carbon atom, but both of these electrons have now been used to make a bond to the X atom. This leaves the right-hand carbon atom an electron short – hence positively charged.</p>
<p>Note also that we are only showing one of the pairs of electrons around the Y<sup>-</sup> ion. There will be other lone pairs as well, but we are only actually interested in the one we've drawn.</p>
</div>

<div class="definition">
<p>Carbocation (or a carbonium ion): an ion in which the positive charge is carried on a carbon atom.</p>
</div>

<div class="image-container">
<img src="ch2xch2y.GIF">
</div>

<div class="text-block">
<p>In the final stage of the reaction the electrons in the lone pair on the Y<sup>-</sup> ion are strongly attracted towards the positive carbon atom. They move towards it and form a co-ordinate (dative covalent) bond between the Y and the carbon.</p>
</div>

<div class="note">
<p>Help! A co-ordinate (dative covalent) bond is simply a covalent bond in which both shared electrons originate from the same atom. The bond formed between the X and the other carbon atom was also a co-ordinate bond. Once a co-ordinate bond has been formed there is no difference whatsoever between it and any other covalent bond.</p>
</div>

<h3>How to Write This Mechanism in an Exam</h3>

<div class="text-block">
<p>The movements of the various electron pairs are shown using curly arrows.</p>
</div>

<div class="note">
<p>Help! If you aren't sure about the use of <a href="../../basicorg/conventions/curlies.html#top">curly arrows</a> in mechanisms, you must follow this link before you go on.</p>
</div>

<div class="image-container"><img src="ethenexy3.GIF"></div>

<div class="text-block">
<p>Don't leave this page until you are sure that you understand how this relates to the electron pair movements drawn in the previous diagrams.</p>
</div> 

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../eladdmenu.html#top">To menu of electrophilic addition reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>