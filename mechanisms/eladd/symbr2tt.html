<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Explaining The Reaction Between Symmetrical Alkenes and Bromine | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A guide to the mechanism of the electrophilic addition reaction between bromine and symmetrical alkenes like ethene and cyclohexene.">
<meta name="keywords" content="electrophilic addition, electrophile, ethene, cyclohexene, alkenes, bromine">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Explaining The Reaction Between Symmetrical Alkenes and Bromine</h1>

<div class="text-block">
<p>This page guides you through the mechanism for the electrophilic addition of bromine to symmetrical alkenes like ethene or cyclohexene. Unsymmetrical alkenes are covered separately, and you will find a link at the bottom of the page.</p>
</div>

<h2>The Electrophilic Addition of Bromine to Ethene</h2>

<h3>The Structure of Ethene</h3>

<div class="image-container">
<img src="ethenebonds.GIF">
</div>

<div class="text-block">
<p>The structure of ethene is shown in the diagramabove. The &pi; bond is an orbital above and below the plane of the rest of the molecule, and relatively exposed to things around it.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about this, then you should read the page <a href="whatis.html#top">What is electrophilic addition?</a> before you go on.</p>
</div>

<h3>Bromine as an Electrophile</h3>

<div class="text-block">
<p>Since two identical bromine atoms are joined together in the bromine molecule there is no reason why one atom should pull the bonding pair of electrons towards itself – they must be equally electronegative and so there won't be any separation of charge, &delta;+ or &delta;-. How, then, can bromine be an electrophile?</p>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="../../basicorg/bonding/eneg.html#top">electronegativity and bond polarity</a> follow this link before you read on.</p>
<p>Equally, if you aren't sure about terms like electrophile, then it really would be a good idea to read the page <a href="whatis.html#top">What is electrophilic addition?</a> before you go on.</p>
</div>

<div class="text-block">
<p>In fact, bromine is a very polarisable molecule – in other words, the electrons in the bond are very easily pushed to one end or the other. As the bromine molecule approaches the ethene, the electrons in the &pi; bond tend to repel the electrons in the bromine-bromine bond, leaving the nearer bromine slightly positive and the further one slightly negative.</p>
</div>

<div class="image-container"><img src="inddipole.GIF"></div>

<div class="text-block">
<p>The bromine molecule therefore acquires an induced dipole which is automatically lined up the right way round for a successful attack on the ethene.</p>
</div>

<div class="note">
<p>Help! What is an "induced dipole"? A dipole is simply a separation of charge between &delta;+ at one end and &delta;- at the other. "Induced" means that it has been created by some external influence (in this case the approach of the &pi; bond), and didn't already exist.</p>
<p>Where it does already exist – as, for example, in HBr – it is called a permanent dipole.</p>
</div>

<h3>The Simplified Version of the Mechanism</h3>

<div class="note">
<p>Note: Use this version unless your examiners insist on the more accurate one.</p>
<p>If you've come into this web site from a search engine directly to this page, read the notes on the <a href="symbr2.html#top">introductory page</a> to this reaction before you go any further.</p>
</div>

<div class="text-block">
<p>The electrons from the &pi; bond move down towards the slightly positive bromine atom.</p>
</div>

<div class="image-container"><img src="ethbr2mech1a.GIF"></div>

<div class="text-block">
<p>In the process, the electrons in the Br-Br bond are repelled down until they are entirely on the bottom bromine atom, producing a bromide ion.</p>
</div>

<div class="note">
<p>Help! If you aren't sure about the use of <a href="../../basicorg/conventions/curlies.html#top">curly arrows</a> in mechanisms, you must follow this link before you go on.</p>
</div>

<div class="text-block">
<p>The ion with a positive charge on the carbon atom is called a carbocation or carbonium ion.</p>
<p>Why is there a positive charge on the carbon atom? The &pi; bond was originally made up of an electron from each of the carbon atoms. Both of those electrons have been used to make a new bond to the bromine. That leaves the right-hand carbon an electron short – hence positively charged.</p>
<p>In the second stage of the mechanism, the lone pair of electrons on the bromide ion is strongly attracted to the positive carbon and moves towards it until a bond is formed.</p>
</div>

<div class="image-container"><img src="ethbr2mech1b.GIF"></div>

<div class="text-block">
<p>The overall mechanism is therefore</p>
</div>

<div class="image-container"><img src="ethbr2mech1.GIF"></div>

<h3>The More Accurate Version of the Mechanism</h3>

<div class="note">
<p>Note: Don't learn this unless you have to. There is a real risk of getting confused. If your examiners are happy to accept the simple version, there's no point in making life difficult for yourself.</p>
</div>

<div class="text-block">
<p>The reaction starts off just the same as in the simplified version, with the &pi; bond electrons moving down towards the slightly positive bromine atom.</p>
<p>But this time, the top bromine atom becomes attached to both carbon atoms, with the positive charge being found on the bromine rather than on one of the carbons. A bromonium ion is formed.</p>
</div>

<div class="image-container"><img src="ethbr2mech2a.GIF"></div>

<div class="text-block">
<p>The bromonium ion is then attacked from the back by a bromide ion formed in a nearby reaction. It can't be attacked by its original bromide ion because the bromonium ion is completely cluttered up with a positive bromine on that side.</p>
</div>

<div class="image-container"><img src="ethbr2mech2b.GIF"></div>

<div class="text-block">
<p>It doesn't matter which of the carbon atoms the bromide ion attacks – the end result would be just the same.</p>
</div>

<div class="note">
<p>Note: You can't really draw this mechanism tidily in one line because the bromide ion has to be in a different place at the beginning of the second stage than it was at the end of the first stage.</p>
</div>

<h2>The Electrophilic Addition of Bromine to Cyclohexene</h2>

<h3>The Simplified Version of the Mechanism</h3>

<div class="note">
<p>Note: Use this version unless your examiners insist on the more accurate one.</p>
</div>

<div class="text-block">
<p>The electrons from the &pi; bond move towards the slightly positive bromine atom.</p>
</div>

<div class="image-container"><img src="cyclobr2m1a.GIF"></div>

<div class="text-block">
<p>In the process, the electrons in the bromine-bromine bond are repelled until they are entirely on the right-hand bromine atom, producing a bromide ion.</p>
<p>Exactly as with ethene, a carbocation is formed. The bottom carbon atom lost one of its electrons when the &pi;bond swung towards the bromine.</p>
<p>In the second stage of the mechanism, the lone pair of electrons on the bromide ion is strongly attracted to the positive carbon and moves towards it until a bond is formed.</p>
</div>

<div class="image-container"><img src="cyclobr2m1b.GIF"></div>

<div class="text-block">
<p>The overall mechanism is therefore</p>
</div>

<div class="image-container"><img src="cyclobr2m1.GIF"></div>

<h3>The Alternative Version of the Mechanism</h3>

<div class="note">
<p>Note: Don't learn this unless your examiners insist on it. Keep life simple!</p>
</div>

<div class="text-block">
<p>The reaction starts off just the same as in the simplified version, with the &pi;bond electrons moving towards the slightly positive bromine atom.</p>
<p>But this time, the left-hand bromine atom becomes attached to both carbon atoms, with the positive charge being found on the bromine rather than on one of the carbons. A bromonium ion is formed.</p>
</div>

<div class="image-container"><img src="cyclobr2m2a.GIF"></div>

<div class="text-block">
<p>The bromonium ion is then attacked from the back by a bromide ion formed in a nearby reaction. It can't be attacked by its original bromide ion because approach from that side is hindered by the positive bromine atom.</p>
</div>

<div class="image-container"><img src="cyclobr2m2b.GIF"></div>

<div class="text-block">
<p>It doesn't matter which of the carbon atoms on either end of the original double bond the bromide ion attacks – the end result would be just the same.</p>
</div>

<div class="note">
<p>Note: Once again, you can't really draw this mechanism tidily in one line because of the need to move the bromide ion.</p>
</div>

<h1 class="no-print">Interactive</h1>

<div class="interactive-note">
<p>Click on the structures to see them in 3D.</p>
<p>Click on the reaction arrows to see an animation of the reaction.</p>
</div>

<div class="image-container full-width no-print">
<img alt="elecaddn" src="elecaddnbr2.gif" width="479" height="134" usemap="#Map" />
<map name="Map" id="Map">
<area href="javascript:Jmol.script(jmolApplet0, 'load elecaddnpt1.sdf;select all;set labelOffset 0 0;font label 20 serif bold;color label orange; moveto 0.0 -996 62 -69 82.0; spin off; spacefill 20%; wireframe 0.1; connect 2.55 2.95 (atomno=1) (atomno=7) NONE; connect 2.25 2.55 (atomno=1) (atomno=7) PARTIAL; connect 1.90 2.25 (atomno=1) (atomno=7) SINGLE; connect 2.29 2.69 (atomno=7) (atomno=8) SINGLE; connect 2.69 2.99 (atomno=7) (atomno=8) PARTIAL; connect 2.99 3.29 (atomno=7) (atomno=8) NONE; connect 1.30 1.40 (atomno=2) (atomno=1) DOUBLE; connect 1.40 1.46 (atomno=2) (atomno=1) PARTIALDOUBLE; connect 1.46 1.53 (atomno=2) (atomno=1) SINGLE;select atomno=2 and model=1.15;label +;select atomno=2 and model=1.16;label +;select atomno=2 and model=1.17;label +;select atomno=2 and model=1.18;label +;select atomno=2 and model=1.19;label +;select atomno=2 and model=1.20;label +;select atomno=2 and model=1.21;label +;select atomno=8 and model=1.15;label -;select atomno=8 and model=1.16;label -;select atomno=8 and model=1.17;label -;select atomno=8 and model=1.18;label -;select atomno=8 and model=1.19;label -;select atomno=8 and model=1.20;label -;select atomno=8 and model=1.21;label -; anim mode once; frame 1; delay 0.5; anim on')" shape="rect" coords="97,33,180,60" />
<area href="javascript:Jmol.script(jmolApplet0, 'load elecaddnbr2pm3.sdf;select all;set labelOffset 0 0;font label 20 serif bold;color label orange; moveto 0.0 892 -237 -386 120.45; spin off; spacefill 20%; wireframe 0.1; connect 2.50 2.90 (atomno=8) (atomno=2) NONE; connect 2.20 2.50 (atomno=8) (atomno=2) PARTIAL; connect 1.95 2.20 (atomno=8) (atomno=2) SINGLE;select atomno=8 and model=1.1; label -;select atomno=8 and model=1.2; label -;select atomno=8 and model=1.3; label -;select atomno=8 and model=1.4; label -;select atomno=8 and model=1.5; label -;select atomno=8 and model=1.6; label -;select atomno=8 and model=1.7; label -;select atomno=8 and model=1.8; label -;select atomno=8 and model=1.9; label -;select atomno=2 and model=1.1; label +;select atomno=2 and model=1.2; label +;select atomno=2 and model=1.3; label +;select atomno=2 and model=1.4; label +;select atomno=2 and model=1.5; label +;select atomno=2 and model=1.6; label +;select atomno=2 and model=1.7; label +;select atomno=2 and model=1.8; label +;select atomno=2 and model=1.9; label +; anim mode once; frame 1; delay 0.5; anim on;')" shape="rect" coords="275,39,351,62" />
<area href="javascript:Jmol.script(jmolApplet0, 'load elecaddnpt1.sdf; moveto 0.0 -996 62 -69 82.0; select all;set labelOffset 0 0;font label 20 serif bold;color label orange;spin off; spacefill 20%; wireframe 0.1; select all;set labelOffset 0 0;font label 20 serif bold;color label orange;connect 2.55 2.95 (atomno=1) (atomno=7) NONE; connect 2.25 2.55 (atomno=1) (atomno=7) PARTIAL; connect 1.90 2.25 (atomno=1) (atomno=7) SINGLE; connect 2.29 2.69 (atomno=7) (atomno=8) SINGLE; connect 2.69 2.99 (atomno=7) (atomno=8) PARTIAL; connect 2.99 3.29 (atomno=7) (atomno=8) NONE; connect 1.30 1.40 (atomno=2) (atomno=1) DOUBLE; connect 1.40 1.46 (atomno=2) (atomno=1) PARTIALDOUBLE; connect 1.46 1.53 (atomno=2) (atomno=1) SINGLE;select atomno=2 and model=1.15;label +;select atomno=2 and model=1.16;label +;select atomno=2 and model=1.17;label +;select atomno=2 and model=1.18;label +;select atomno=2 and model=1.19;label +;select atomno=2 and model=1.20;label +;select atomno=2 and model=1.21;label +;select atomno=8 and model=1.15;label -;select atomno=8 and model=1.16;label -;select atomno=8 and model=1.17;label -;select atomno=8 and model=1.18;label -;select atomno=8 and model=1.19;label -;select atomno=8 and model=1.20;label -;select atomno=8 and model=1.21;label -; draw arrow1 ARROW {-0.0069561005 0.14430904 0.8087158} {0.10264778 0.5452852 -0.1060276} {0.1328373 0.2777114 -1.4135604}; draw arrow2 ARROW {0.16811275 0.07038498 -2.7513294} {0.956377 0.19355965 -3.3735123} {0.57818425 -0.008214951 -4.1912847}; draw fill noMesh noDots notFrontOnly fullylit;color draw opaque [xffa500]')" shape="rect" coords="0, 3, 90, 133" />
<area href="javascript:Jmol.script(jmolApplet0, 'load elecaddnbr2pm3.sdf; moveto 0.0 892 -237 -386 120.45; select all;set labelOffset 0 0;font label 20 serif bold;color label orange;spin off; spacefill 20%; wireframe 0.1; connect 2.50 2.90 (atomno=8) (atomno=2) NONE; connect 2.20 2.50 (atomno=8) (atomno=2) PARTIAL; connect 1.95 2.20 (atomno=8) (atomno=2) SINGLE;select atomno=8 and model=1.1; label -;select atomno=8 and model=1.2; label -;select atomno=8 and model=1.3; label -;select atomno=8 and model=1.4; label -;select atomno=8 and model=1.5; label -;select atomno=8 and model=1.6; label -;select atomno=8 and model=1.7; label -;select atomno=8 and model=1.8; label -;select atomno=8 and model=1.9; label -;select atomno=2 and model=1.1; label +;select atomno=2 and model=1.2; label +;select atomno=2 and model=1.3; label +;select atomno=2 and model=1.4; label +;select atomno=2 and model=1.5; label +;select atomno=2 and model=1.6; label +;select atomno=2 and model=1.7; label +;select atomno=2 and model=1.8; label +;select atomno=2 and model=1.9; label +; draw arrow1 ARROW {-0.02202797 0.23355484 -2.66743} {0.049075127 0.39088058 -1.6798196} {0.6582885 0.2103405 -0.19114685}; draw fill noMesh noDots notFrontOnly fullylit;color draw opaque [xffa500]')" shape="rect" coords="181,2,275,104" />
<area shape="rect" coords="361,6,477,100" href="javascript:Jmol.script(jmolApplet0, 'load elecaddnbr2pm3.sdf;frame 21;select all;set labelOffset 0 0;font label 20 serif bold;color label orange; moveto 0.0 892 -237 -386 120.45; spin off; spacefill 20%; wireframe 0.1; connect 2.50 2.90 (atomno=8) (atomno=2) NONE; connect 2.20 2.50 (atomno=8) (atomno=2) PARTIAL; connect 1.95 2.20 (atomno=8) (atomno=2) SINGLE;select atomno=8 and model=1.1; label -;select atomno=8 and model=1.2; label -;select atomno=8 and model=1.3; label -;select atomno=8 and model=1.4; label -;select atomno=8 and model=1.5; label -;select atomno=8 and model=1.6; label -;select atomno=8 and model=1.7; label -;select atomno=8 and model=1.8; label -;select atomno=8 and model=1.9; label -;select atomno=2 and model=1.1; label +;select atomno=2 and model=1.2; label +;select atomno=2 and model=1.3; label +;select atomno=2 and model=1.4; label +;select atomno=2 and model=1.5; label +;select atomno=2 and model=1.6; label +;select atomno=2 and model=1.7; label +;select atomno=2 and model=1.8; label +;select atomno=2 and model=1.9; label +')" /></map>
</div>

<div class="jsmol-container">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/jsmol/JSmol.min.js"></script>
<script>
var jmolApplet0;

var Info = {
	width: 350,
	height: 350,
	color: "white",
	serverURL: "https://shout.education/ChemKey/Libraries/jsmol/php/jsmol.php",
	addSelectionOptions: false,
	use: "HTML5 IMAGE",
j2sPath: "https://shout.education/ChemKey/Libraries/jsmol/j2s",
	jarPath: "https://shout.education/ChemKey/Libraries/jsmol/java",
	jarFile: "JmolAppletSigned.jar",
	isSigned: false,
	disableInitialConsole: true,
	script: "load elecaddnpt1.sdf; moveto 0.0 -996 62 -69 82.0;select all;set labelOffset 0 0;font label 20 serif bold;color label orange; spin off; spacefill 20%; wireframe 0.1; connect 2.55 2.95 (atomno=1) (atomno=7) NONE; connect 2.25 2.55 (atomno=1) (atomno=7) PARTIAL; connect 1.90 2.25 (atomno=1) (atomno=7) SINGLE; connect 2.29 2.69 (atomno=7) (atomno=8) SINGLE; connect 2.69 2.99 (atomno=7) (atomno=8) PARTIAL; connect 2.99 3.29 (atomno=7) (atomno=8) NONE; connect 1.30 1.40 (atomno=2) (atomno=1) DOUBLE; connect 1.40 1.46 (atomno=2) (atomno=1) PARTIALDOUBLE; connect 1.46 1.53 (atomno=2) (atomno=1) SINGLE;select atomno=2 and model=1.15;label +;select atomno=2 and model=1.16;label +;select atomno=2 and model=1.17;label +;select atomno=2 and model=1.18;label +;select atomno=2 and model=1.19;label +;select atomno=2 and model=1.20;label +;select atomno=2 and model=1.21;label +;select atomno=8 and model=1.15;label -;select atomno=8 and model=1.16;label -;select atomno=8 and model=1.17;label -;select atomno=8 and model=1.18;label -;select atomno=8 and model=1.19;label -;select atomno=8 and model=1.20;label -;select atomno=8 and model=1.21;label -; set antialiasDisplay true; set platformSpeed 8",
	allowJavaScript: true
}

jmolApplet0 = Jmol.getApplet("jmolApplet0", Info)
</script>
<p>Reproduced with permission from <a href="http://www.chemtube3d.com/A%20Level%20elecaddn.html" target="_blank">ChemTube3d.com</a></p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="unsymbr2.html#top">Look at the same reactions involving unsymmetrical alkenes </a>
<a href="../eladdmenu.html#top">To menu of electrophilic addition reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>