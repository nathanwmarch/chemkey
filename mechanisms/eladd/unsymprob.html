<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Electrophilic Addition to Unsymmetrical Alkenes | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the reasons behind Markovnikov's rule, and gives a general mechanism for electrophilic addition to alkenes like propene.">
<meta name="keywords" content="carbonium ion, carbonium ions, carbocation, carbocations, propene, alkenes, electrophile, electrophilic addition,">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Electrophilic Addition to Unsymmetrical Alkenes</h1>

<div class="note">
<p>Important! To make sense of this page, you will need to understand about the structure and stability of <a href="carbonium.html#top">carbocations</a> (previously called carbonium ions) and be confident about <a href="whatis.html#top">electrophilic addition to simple alkenes</a> like ethene.</p>
<p>If you aren't sure about either of these things, follow these links first. Trying to build on shaky foundations risks confusing you and undermining your confidence.</p>
</div>

<h2>The Problem</h2>

<h3>The Addition of H-X to an Unsymmetrical Alkene Like Propene</h3>

<div class="text-block">
<p>An unsymmetrical alkene is one like propene or but-1-ene in which the groups or atoms attached to either end of the carbon-carbon double bond are different.</p>
<p>For example, in propene there are a hydrogen and a methyl group at one end, but two hydrogen atoms at the other end of the double bond.</p>
</div>

<div class="inline">
<div class="chemical-structure" name="propene" type="2d"></div>
<div class="chemical-structure" name="but-1-ene" type="2d"></div>
</div>

<div class="text-block">
<p>With these unsymmetrical alkenes, it is possible to get two different products during some addition reactions.</p>
<p>During the addition of a molecule HX to propene, you could in principle get either this reaction:</p>
</div>

<div class="image-container"><img src="prophx1.GIF"></div>

<div class="text-block">
<p>or this one:</p>
</div>

<div class="image-container"><img src="prophx2.GIF"></div>

<div class="text-block">
<p>It depends on which way around you add the HX across the double bond.</p>
<p>In fact, in most cases, it's mainly the second reaction which happens. The hydrogen atom becomes attached to the right-hand carbon atom as we've drawn it.</p>
</div>

<h3>Markovnikov's Rule</h3>

<div class="text-block">
<p><span class="attention">When a compound HX is added to an unsymmetrical alkene, the hydrogen becomes attached to the carbon with the most hydrogens attached to it already.</span></p>
</div>

<div class="note">
<p>Note: Don't worry too much about the spelling of Markovnikov – there are nearly as many versions as there are text books. It's an English attempt at a Russian name and Markovnikov wouldn't actually have recognised any of them!</p>
</div>

<div class="text-block">
<p>Remember that the HX has to attach itself to the carbon atoms which were originally part of the double bond. So in this case, adding HX to CH<sub>3</sub>CH=CH<sub>2</sub>, the hydrogen is attached to the CH<sub>2</sub> group, because the CH<sub>2</sub> group has more hydrogens than the CH group.</p>
<p>Notice that only the hydrogens directly attached count. The ones in the CH<sub>3</sub> group are totally irrelevant.</p>
</div>

<div class="note">
<p>Warning! Markovnikov's Rule is a useful guide for you to work out which way round to add something across a double bond, but it isn't the reason why things add that way. Propene has never even heard of Markovnikov! When the question arises in an exam, you will need a much more fundamental explanation which is coming up next. As a general principle, don't quote Markovnikov's Rule in an exam unless you are specifically asked for it.</p>
</div>

<h2>The Mechanism</h2>

<h3>HX as an electrophile</h3>

<div class="image-container">
<img src="hxpolar.GIF">
</div>

<div class="text-block">
<p>In each of the cases we are interested in, X is more electronegative than hydrogen. That means that the bonding pair of electrons will be dragged towards the X end of the bond, and so the hydrogen becomes slightly positively charged.</p>
<p>The slightly positive hydrogen is the electrophile, and is attracted to the &pi; bond in the propene.</p>
<p>What happens next determines which way around the HX adds across the double bond.</p>
</div>

<h3>The Two Possible Mechanisms</h3>

<div class="text-block">
<p>In both possibilities, the &pi; bond breaks and the electron pair swings down to form a bond with the hydrogen atom. At the same time, the electrons in the H-X bond are repelled right down on to the X to give an X<sup>-</sup> ion.</p>
<p>The difference lies in which way the electron pair in the &pi; bond swings.</p>
</div>

<h4>First possibility</h4>

<div class="text-block">
<p>The electron pair moves to form a bond between the hydrogen and the left-hand carbon.</p>
</div>

<div class="image-container"><img src="hxmech1.GIF"></div> 

<div class="text-block">
<p>When the second stage of the mechanism happens, and the lone pair on X<sup>-</sup> forms a bond with the positive carbon atom, the product of this mechanism is not the one which Markovnikov's Rule predicts.</p>
</div>

<h4>Second possibility</h4>

<div class="text-block">
<p>The electron pair moves to form a bond between the hydrogen and the right-hand carbon.</p>
</div>

<div class="image-container"><img src="hxmech2.GIF"></div> 

<div class="text-block">
<p>This time, the overall mechanism leads to the correct product.</p>
</div>

<h3>Why Does One of These Work Better Than the Other?</h3>

<div class="text-block">
<p>That's the truth of the situation! One of these mechanisms works better than the other one. The second mechanism works much faster than the first, and so most of the product that you get is CH<sub>3</sub>-CHX-CH<sub>3</sub>.</p>
<p>There will be small amounts of CH<sub>3</sub>-CH<sub>2</sub>-CH<sub>2</sub>X despite what Markovnikov says!</p>
<p>The difference between the two mechanisms lies in the intermediates – the things formed at the half-way stage.</p>
<p>In the mechanism that works best, you get a secondary carbocation formed as one of the intermediate ions.</p>
</div>

<div class="image-container"><img src="secondary.GIF"></div> 

<div class="text-block">
<p>In the slow mechanism which produces hardly any product, you get a primary carbocation formed instead.</p>
</div>

<div class="image-container"><img src="primary.GIF"></div> 

<div class="text-block">
<p>It is much easier to form the secondary carbocation because it is more energetically stable. The activation energy for the reaction will be less, and so most of the reaction happens via that mechanism.</p>
</div>

<div class="definition">
<p>Activation energy: The minimum energy needed before a reaction will occur.</p>
</div>

<div class="text-block">
<p>In this case the activation energy is the energy needed to break the various bonds and make the carbocation and the X<sup>-</sup> ion.</p>
</div>

<h3>How to Attack This Sort of Question in an Exam</h3>

<div class="text-block">
<p>Suppose you were asked for the mechanism for the addition of HX to some alkene you hadn't come across before.</p>
<p>First, you need to decide whether the alkene is symmetrical or not. If it is symmetrical, there's no problem – it wouldn't matter which way around you added the HX. If it is unsymmetrical, you need to decide which way round the HX is going to add.</p>
<p>For example, supposed you were asked for the mechanism for the addition of HX to but-1-ene, CH<sub>3</sub>-CH<sub>2</sub>-CH=CH<sub>2</sub>.</p>
<p>First, use Markovnikov's Rule to decide which carbon to attach the hydrogen to. In this case, the hydrogen would get attached to the CH<sub>2</sub> end of the double bond, because that carbon has more hydrogens than the CH end.</p>
</div>

<div class="note">
<p>Warning! Markovnikov's Rule is only to help you decide. Don't give the examiners any hint that you are using it – unless they specifically ask.</p>
</div>

<div class="text-block">
<p>Now write the mechanism, taking care to draw the curly arrow showing the movement of the &pi; bond so that the hydrogen gets attached to that particular CH<sub>2</sub> carbon.</p>
</div>

<div class="image-container"><img src="buthxmech.GIF"></div> 

<div class="text-block">
<p>If you are asked why the HX adds this way round, look at the carbocation formed as an intermediate and decide whether it is secondary or tertiary. Here it is a secondary ion. Then think about what sort of ion would be formed if the HX added the other way around. In this case that would be a primary ion.</p>
</div>

<div class="image-container"><img src="butprim.GIF"></div> 

<div class="text-block">
<p>Then say something like:</p>
<p>"The secondary carbocation formed in this reaction is more energetically stable than the primary one which would be formed if the addition was the other way round, and so less activation energy is needed."</p>
</div>

<div class="note">
<p>Important! If there were bits of this that you found you couldn't understand, you are probably trying to do too much too quickly.</p>
<p>Follow the links to <a href="carbonium.html#top">carbocations (carbonium ions)</a> and <a href="whatis.html#top">electrophilic addition to simple alkenes</a>, get the basics sorted out properly, and then try again.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../eladdmenu.html#top">To menu of electrophilic addition reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>