<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Carbocations (or Carbonium Ions) | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of the term carbocation (or carbonium ion), including the relative stabilities of the different kinds.">
<meta name="keywords" content="carbonium ion, carbonium ions, carbocation, carbocations">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Carbocations<br>(or Carbonium Ions)</h1>

<div class="text-block">
<p>All carbocations (previously known as carbonium ions) carry a positive charge on a carbon atom. The name tells you that – a cation is a positive ion, and the "carbo" bit refers to a carbon atom. However there are important differences in the structures of various types of carbocations.</p>
</div>

<h2>The Different Kinds of Carbocations</h2>

<h3>Primary Carbocations</h3>

<div class="text-block">
<p>In a primary (1°) carbocation, the carbon which carries the positive charge is only attached to one other alkyl group.</p>
</div>

<div class="note">
<p>Help! An alkyl group is a group such as methyl, CH<sub>3</sub>, or ethyl, CH<sub>3</sub>CH<sub>2</sub>. These are groups containing chains of carbon atoms which may be branched. Alkyl groups are given the general symbol R.</p>
</div>

<div class="text-block">
<p>Some examples of primary carbocations include:</p> 
</div>

<div class="image-container"><img src="primcarbs.GIF"></div>

<p>Notice that it doesn't matter how complicated the attached alkyl group is. All you are doing is counting the number of bonds from the positive carbon to other carbon atoms. In all the above cases there is only one such link.</p>

<div class="image-container">
<img src="primgen.GIF">
</div>

<div class="text-block">
<p>Using the symbol R for an alkyl group, a primary carbocation would be written as in the box.</p>
</div>

<h3>Secondary Carbocations</h3>

<div class="text-block">
<p>In a secondary (2°) carbocation, the carbon with the positive charge is attached to two other alkyl groups, which may be the same or different.</p>
<p>Examples:</p>
</div>

<div class="image-container"><img src="seccarbs.GIF"></div>

<div class="image-container">
<img src="secgen.GIF">
</div>

<div class="text-block">
<p>A secondary carbocation has the general formula shown in the box. R and R' represent alkyl groups which may be the same or different.</p>
</div>

<h3>Tertiary Carbocations</h3>

<div class="text-block">
<p>In a tertiary (3°) carbocation, the positive carbon atom is attached to three alkyl groups, which may be any combination of same or different.</p>
</div>

<div class="image-container"><img src="tertcarbs.GIF"></div>

<div class="image-container">
<img src="tertgen.GIF">
</div>

<div class="text-block">
<p>A tertiary carbocation has the general formula shown in the box. R, R' and R" are alkyl groups and may be the same or different.</p>
</div>

<h2>The Stability of the Various Carbocations</h2>

<h3>The "Electron Pushing Effect" of Alkyl Groups</h3>

<div class="text-block">
<p>You are probably familiar with the idea that bromine is more electronegative than hydrogen, so that in a H-Br bond the electrons are held closer to the bromine than the hydrogen. A bromine atom attached to a carbon atom would have precisely the same effect – the electrons being pulled towards the bromine end of the bond. The bromine has a negative inductive effect.</p>
</div>

<div class="note">
<p>Help! If you aren't familiar with all of this, follow this link to read about <a href="../../basicorg/bonding/eneg.html#top">electronegativity and bond polarity</a> before you go any further.</p>
</div>

<div class="text-block">
<p>Alkyl groups do precisely the opposite and, rather than draw electrons towards themselves, tend to "push" electrons away.</p>
</div>

<div class="note">
<p>Note: The term "electron pushing" is only to help remember what happens. The alkyl group doesn't literally "push" the electrons away – the other end of the bond attracts them more strongly.</p>
</div>

<div class="text-block">
<p>This means that the alkyl group becomes slightly positive (&delta;+) and the carbon they are attached to becomes slightly negative (&delta;-). The alkyl group has a positive inductive effect.</p>
<p>This is sometimes shown as, for example:</p>
</div>

<div class="image-container"><img src="ch3induct.GIF"></div>

<div class="text-block">
<p>The arrow shows the electrons being "pushed" away from the CH<sub>3</sub> group. The plus sign on the left-hand end of it shows that the CH<sub>3</sub> group is becoming positive. The symbols &delta;+ and &delta;- simply reinforce that idea.</p>
</div>

<h3>The Importance of Spreading Charge Around in Making Ions Stable</h3>

<div class="text-block">
<p>The general rule-of-thumb is that if a charge is very localised (all concentrated on one atom) the ion is much less stable than if the charge is spread out over several atoms.</p>
<p>Applying that to carbocations of various sorts</p>
</div>

<div class="image-container"><img src="ionspolar.GIF"></div>

<div class="text-block">
<p>You will see that the electron pushing effect of the CH<sub>3</sub> group is placing more and more negative charge on the positive carbon as you go from primary to secondary to tertiary carbocations. The effect of this, of course, is to cut down that positive charge.</p>
<p>At the same time, the region around the various CH<sub>3</sub> groups is becoming somewhat positive. The net effect, then, is that the positive charge is being spread out over more and more atoms as you go from primary to secondary to tertiary ions.</p>
<p>The more you can spread the charge around, the more stable the ion becomes.</p>
</div>

<div class="note">
<p>Order of stability of carbocations:</p>
<p><span class="attention">primary &lt; secondary &lt; tertiary</span></p>
<p>The symbol "&lt;" means "is less than". So what this is saying is that primary ions are less stable than secondary ones which in turn are less stable than tertiary ones.</p>
</div>

<h3>The Stability of the Carbocations in Terms of Energetics</h3>

<div class="image-container">
<img src="ionsstab.GIF">
</div>

<div class="text-block">
<p>When we talk about secondary carbocations being more stable than primary ones, what exactly do we mean? We are actually talking about energetic stability – secondary carbocations are lower down an energy "ladder" than primary ones.</p>
<p>This means that it is going to take more energy to make a primary carbocation than a secondary one.</p>
<p>If there is a choice between making a secondary ion or a primary one, it will be much easier to make the secondary one.</p>
<p>Similarly, if there is a choice between making a tertiary ion or a secondary one, it will be easier to make the tertiary one.</p>
<p>This has important implications in the reactions of unsymmetrical alkenes. If you are interested in these, follow the link below to the electrophilic addition reactions menu.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../eladdmenu.html#top">To menu of electrophilic addition reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>