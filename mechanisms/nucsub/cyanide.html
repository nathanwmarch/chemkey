<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Nucleophilic Substitution Reactions of Halogenoalkanes and Cyanide Ions | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Facts and mechanisms for the reactions between halogenoalkanes and cyanide ions – nucleophilic substitution">
<meta name="keywords" content="halogenoalkanes, haloalkanes, alkyl halides, nucleophile, nucleophilic substitution, substitution, sn1, sn2, cyanide ions, cyanide, potassium cyanide">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Nucleophilic Substitution Reactions of Halogenoalkanes and Cyanide Ions</h1>

<div class="text-block">
<p>This page gives you the facts and simple, uncluttered mechanisms for the nucleophilic substitution reactions between halogenoalkanes and cyanide ions (from, for example, potassium cyanide). If you want the mechanisms explained to you in detail, there is a link at the bottom of the page.</p>
</div>

<h2>The Reaction of Primary Halogenoalkanes With Cyanide Ions</h2>

<div class="note">
<p>Important! If you aren't sure about the difference between <a href="types.html#top">primary, secondary and tertiary halogenoalkanes</a>, it is essential that you follow this link before you go on.</p>
</div>

<h3>The Facts</h3>

<div class="text-block">
<p>If a halogenoalkane is heated under reflux with a solution of sodium or potassium cyanide in ethanol, the halogen is replaced by a -CN group and a nitrile is produced. Heating under reflux means heating with a condenser placed vertically in the flask to prevent loss of volatile substances from the mixture.</p>
<p>The solvent is important. If water is present you tend to get substitution by -OH instead of -CN.</p>
</div>

<div class="note">
<p>Note: A solution of potassium cyanide in water is quite alkaline, and contains significant amounts of hydroxide ions. These react with the halogenoalkane.</p>
</div>

<div class="text-block">
<p>For example, using 1-bromopropane as a typical primary halogenoalkane:</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{CH}_2\text{Br} + \text{CN}^- \longrightarrow \text{CH}_3\text{CH}_2\text{CH}_2\text{CN} + \text{Br}^-
</div>

<div class="text-block">
<p>You could write the full equation rather than the ionic one, but it slightly obscures what's going on:</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}_2\text{CH}_2\text{Br} + \text{KCN} \longrightarrow \text{CH}_3\text{CH}_2\text{CH}_2\text{CN} + \text{KBr}
</div>

<div class="text-block">
<p>The bromine (or other halogen) in the halogenoalkane is simply replaced by a -CN group – hence a substitution reaction. In this example, butanenitrile is formed.</p>
</div>

<div class="note">
<p>Note: When you are naming nitriles, you have to remember to include the carbon in the -CN group when you count the longest chain. In this example, there are 4 carbons in the longest chain – hence butanenitrile.</p>
</div>

<h3>The Mechanism</h3>

<div class="text-block">
<p>Here is the mechanism for the reaction involving bromoethane:</p>
</div>

<div class="image-container"><img src="sn2cn.GIF"></div>

<div class="text-block">
<p>This is an example of nucleophilic substitution.</p>
<p>Because the mechanism involves collision between two species in the slow step (in this case, the only step) of the reaction, it is known as an S<sub>N</sub>2 reaction.</p>
</div>

<div class="note">
<p>Note: Unless your syllabus specifically mentions S<sub>N</sub>2 by name, you can just call it nucleophilic substitution.</p>
</div>

<div class="text-block">
<p>If your examiners want you to show the transition state, draw the mechanism like this:</p>
</div>

<div class="image-container"><img src="sn2cn2.gif"></div>

<h2>The Reaction of Tertiary Halogenoalkanes With Cyanide Ions</h2>

<h3>The Facts</h3>

<div class="text-block">
<p>The facts of the reaction are exactly the same as with primary halogenoalkanes. If the halogenoalkane is heated under reflux with a solution of sodium or potassium cyanide in ethanol, the halogen is replaced by -CN, and a nitrile is produced.</p>
<p>For example:</p>
</div>

<div class="block-formula">
(\text{CH}_3)_3\text{CBr} + \text{CN}^- \longrightarrow (\text{CH}_3)_3\text{CCN} + \text{Br}^-
</div>

<div class="text-block">
<p>Or if you want the full equation rather than the ionic one:</p>
</div>

<div class="block-formula">
(\text{CH}_3)_3\text{CBr} + \text{KCN} \longrightarrow (\text{CH}_3)_3\text{CCN} + \text{KBr}
</div>

<h3>The Mechanism</h3>

<div class="text-block">
<p>This mechanism involves an initial ionisation of the halogenoalkane:</p>
</div>

<div class="image-container"><img src="sn1slow.GIF"></div>

<div class="text-block">
<p>followed by a very rapid attack by the cyanide ion on the carbocation (carbonium ion) formed:</p>
</div>

<div class="image-container"><img src="sn1cnfast.GIF"></div>

<div class="text-block">
<p>This is again an example of nucleophilic substitution.</p>
<p>This time the slow step of the reaction only involves one species – the halogenoalkane. It is known as an S<sub>N</sub>1 reaction.</p>
</div>

<h2>The Reaction of Secondary Halogenoalkanes With Cyanide Ions</h2>

<h3>The Facts</h3>

<div class="text-block">
<p>The facts of the reaction are exactly the same as with primary or tertiary halogenoalkanes. The halogenoalkane is heated under reflux with a solution of sodium or potassium cyanide in ethanol.</p>
<p>For example:</p>
</div>

<div class="block-formula">
\text{CH}_3\text{CH}(\text{Br})\text{CH}_3 + \text{CN}^- \longrightarrow \text{CH}_3\text{CH}(\text{CN})\text{CH}_3 + \text{Br}^-
</div>

<h3>The Mechanism</h3>

<div class="text-block">
<p>Secondary halogenoalkanes use both S<sub>N</sub>2 and S<sub>N</sub>1 mechanisms. For example, the S<sub>N</sub>2 mechanism is:</p>
</div>

<div class="svg-container">
    <svg fill-opacity="1" xmlns:xlink="http://www.w3.org/1999/xlink" color-rendering="auto" color-interpolation="auto" text-rendering="auto" stroke="black" stroke-linecap="square" width="281" stroke-miterlimit="10" shape-rendering="auto" stroke-opacity="1" fill="black" stroke-dasharray="none" font-weight="normal" stroke-width="1" height="117" xmlns="http://www.w3.org/2000/svg" font-family="&apos;Dialog&apos;" font-style="normal" stroke-linejoin="miter" font-size="12" stroke-dashoffset="0" image-rendering="auto"><defs id="genericDefs"/><g><g text-rendering="geometricPrecision" transform="translate(-229,-238)" color-rendering="optimizeQuality" color-interpolation="linearRGB" stroke-linecap="butt" image-rendering="optimizeQuality"><path fill="none" d="M257.3333 269.3333 A11.9257 11.9257 0 0 0 269.2591 289.9892"/><path stroke-linecap="square" d="M269.259 289.9892 L260.2259 294.279 C260.2259 294.279 262.0325 293.421 261.6586 291.3845 C261.2848 289.348 259.2912 289.1877 259.2912 289.1877 Z" stroke="none"/><path stroke-linecap="square" fill="none" d="M269.259 289.9892 L260.2259 294.279 C260.2259 294.279 262.0325 293.421 261.6586 291.3845 C261.2848 289.348 259.2912 289.1877 259.2912 289.1877 Z"/><line y2="294.6667" fill="none" x1="343.3333" x2="383.375" y1="294.6667"/><path stroke-linecap="square" d="M391.375 294.6667 L381.7157 297.2549 C381.7157 297.2549 383.6476 296.7372 383.6476 294.6667 C383.6476 292.5961 381.7157 292.0785 381.7157 292.0785 Z" stroke="none"/><path stroke-linecap="square" fill="none" d="M391.375 294.6667 L381.7157 297.2549 C381.7157 297.2549 383.6476 296.7372 383.6476 294.6667 C383.6476 292.5961 381.7157 292.0785 381.7157 292.0785 Z"/><line y2="250.741" fill="none" stroke-width="1.2" x1="274" x2="274" y1="263.7434"/><line y2="250.741" fill="none" stroke-width="1.2" x1="279" x2="279" y1="263.7434"/><line y2="250.741" fill="none" stroke-width="1.2" x1="269" x2="269" y1="263.7434"/><line y2="316" fill="none" stroke-width="1.2" x1="267.4064" x2="253.4557" y1="316"/><line y2="316" fill="none" stroke-width="1.2" x1="278.3614" x2="292.3121" y1="316"/><line y2="334.9453" fill="none" stroke-width="1.2" x1="272.9285" x2="272.9771" y1="322.0525"/><circle stroke-width="1.2" r="1" cx="273.7988" cy="278.4557" stroke="none"/><circle stroke-width="1.2" r="1" cx="273.7037" cy="282.4546" stroke="none"/><circle stroke-width="1.2" r="1" cx="273.6086" cy="286.4535" stroke="none"/><circle stroke-width="1.2" r="1" cx="273.5135" cy="290.4523" stroke="none"/><circle stroke-width="1.2" r="1" cx="273.4183" cy="294.4512" stroke="none"/><circle stroke-width="1.2" r="1" cx="273.3232" cy="298.4501" stroke="none"/></g><g font-size="11" transform="translate(-229,-238)" fill="rgb(144,144,144)" text-rendering="geometricPrecision" image-rendering="optimizeQuality" color-rendering="optimizeQuality" font-family="&apos;Helvetica&apos;" stroke="rgb(144,144,144)" color-interpolation="linearRGB"><path d="M274.1799 265.7434 Q275.6785 265.7434 276.5056 266.533 Q277.3328 267.3225 277.4241 268.3269 L276.3821 268.3269 Q276.2048 267.5642 275.6758 267.1184 Q275.1467 266.6726 274.1907 266.6726 Q273.0251 266.6726 272.3081 267.4917 Q271.5911 268.3108 271.5911 270.0027 Q271.5911 271.3884 272.2383 272.2505 Q272.8855 273.1125 274.1692 273.1125 Q275.3508 273.1125 275.9685 272.2048 Q276.2961 271.7268 276.4573 270.948 L277.4993 270.948 Q277.3596 272.1941 276.5754 273.0374 Q275.6355 274.0525 274.0403 274.0525 Q272.6653 274.0525 271.7307 273.22 Q270.5007 272.1189 270.5007 269.8201 Q270.5007 268.0745 271.4246 266.9573 Q272.4236 265.7434 274.1799 265.7434 Z" stroke="none"/><path fill="black" d="M281.8438 269.7237 L286.1562 269.7237 L286.1562 270.2763 L281.8438 270.2763 Z" stroke="none"/><circle fill="black" r="1" cx="264" cy="268" stroke="none"/><circle fill="black" r="1" cx="264" cy="272" stroke="none"/><path fill="rgb(48,80,248)" d="M270.8687 240.8508 L272.1309 240.8508 L276.1162 247.2424 L276.1162 240.8508 L277.1313 240.8508 L277.1313 248.741 L275.9336 248.741 L271.8892 242.3547 L271.8892 248.741 L270.8687 248.741 Z" stroke="none"/><path d="M273.0856 311.7434 Q274.5841 311.7434 275.4113 312.533 Q276.2384 313.3225 276.3297 314.3269 L275.2878 314.3269 Q275.1105 313.5642 274.5815 313.1184 Q274.0524 312.6726 273.0963 312.6726 Q271.9308 312.6726 271.2138 313.4917 Q270.4967 314.3108 270.4967 316.0027 Q270.4967 317.3884 271.144 318.2505 Q271.7912 319.1125 273.0749 319.1125 Q274.2565 319.1125 274.8742 318.2048 Q275.2018 317.7268 275.3629 316.948 L276.4049 316.948 Q276.2653 318.1941 275.4811 319.0374 Q274.5412 320.0525 272.946 320.0525 Q271.571 320.0525 270.6364 319.22 Q269.4064 318.1189 269.4064 315.8201 Q269.4064 314.0745 270.3302 312.9573 Q271.3293 311.7434 273.0856 311.7434 Z" stroke="none"/><path d="M269.7743 302.8533 L270.8539 302.8533 L270.8539 306.1135 L274.9574 306.1135 L274.9574 302.8533 L276.037 302.8533 L276.037 310.7434 L274.9574 310.7434 L274.9574 307.0535 L270.8539 307.0535 L270.8539 310.7434 L269.7743 310.7434 Z" stroke="none"/><path fill="black" d="M278.2677 307.9082 L281.3018 307.9082 L281.3018 308.4646 L279.1437 308.4646 L279.1437 308.4796 L280.486 309.3707 Q281.2379 309.8783 281.5237 310.3746 Q281.7869 310.8746 281.7793 311.4198 Q281.7718 312.2206 281.2718 312.8071 Q280.7604 313.4049 279.7979 313.42 Q278.9144 313.4125 278.3918 312.8598 Q277.8654 312.3221 277.8579 311.4724 Q277.8767 309.6565 279.7077 309.5098 L279.7077 309.4948 L278.2677 308.4646 ZM279.8618 312.8673 Q280.4709 312.8598 280.7529 312.3898 Q281.0387 311.9424 281.0387 311.4123 Q281.0387 311.1115 280.9785 310.8896 Q280.9146 310.6716 280.8244 310.525 Q280.7304 310.3821 280.6401 310.2881 Q280.5461 310.1941 280.486 310.1565 Q280.3318 310.0174 280.0611 310.0174 Q278.6061 310.0174 278.5986 311.5063 Q278.5986 312.074 278.9294 312.4575 Q279.2603 312.8598 279.8618 312.8673 ZM282.5162 311.698 L282.5162 311.0664 L284.1555 311.0664 L284.1555 309.4158 L284.7984 309.4158 L284.7984 311.0664 L286.4377 311.0664 L286.4377 311.698 L284.7984 311.698 L284.7984 313.3448 L284.1555 313.3448 L284.1555 311.698 Z" stroke="none"/><path d="M248.1799 311.7434 Q249.6785 311.7434 250.5056 312.533 Q251.3328 313.3225 251.4241 314.3269 L250.3821 314.3269 Q250.2048 313.5642 249.6758 313.1184 Q249.1467 312.6726 248.1907 312.6726 Q247.0251 312.6726 246.3081 313.4917 Q245.5911 314.3108 245.5911 316.0027 Q245.5911 317.3884 246.2383 318.2505 Q246.8855 319.1125 248.1692 319.1125 Q249.3508 319.1125 249.9685 318.2048 Q250.2961 317.7268 250.4573 316.948 L251.4993 316.948 Q251.3596 318.1941 250.5754 319.0374 Q249.6355 320.0525 248.0403 320.0525 Q246.6653 320.0525 245.7307 319.22 Q244.5007 318.1189 244.5007 315.8201 Q244.5007 314.0745 245.4246 312.9573 Q246.4236 311.7434 248.1799 311.7434 Z" stroke="none"/><path d="M232.4745 311.9583 L233.5541 311.9583 L233.5541 315.2185 L237.6576 315.2185 L237.6576 311.9583 L238.7372 311.9583 L238.7372 319.8484 L237.6576 319.8484 L237.6576 316.1584 L233.5541 316.1584 L233.5541 319.8484 L232.4745 319.8484 Z" stroke="none"/><path d="M241.5532 321.8011 Q240.5982 321.8011 240.1677 321.2766 Q239.7372 320.7522 239.7372 320.0002 L240.444 320.0002 Q240.4892 320.5228 240.6396 320.7597 Q240.9027 321.1845 241.5908 321.1845 Q242.1247 321.1845 242.448 320.8988 Q242.7713 320.6131 242.7713 320.1619 Q242.7713 319.6054 242.4311 319.3836 Q242.0908 319.1618 241.4855 319.1618 Q241.4178 319.1618 241.3483 319.1637 Q241.2787 319.1656 241.2073 319.1693 L241.2073 318.5715 Q241.3125 318.5828 241.384 318.5865 Q241.4554 318.5903 241.5381 318.5903 Q241.9179 318.5903 242.1623 318.47 Q242.5909 318.2594 242.5909 317.718 Q242.5909 317.3157 242.3051 317.0977 Q242.0194 316.8796 241.6396 316.8796 Q240.9629 316.8796 240.7035 317.3308 Q240.5606 317.5789 240.5418 318.0376 L239.8726 318.0376 Q239.8726 317.4361 240.1132 317.015 Q240.5268 316.263 241.5682 316.263 Q242.3916 316.263 242.8428 316.6296 Q243.2939 316.9962 243.2939 317.6917 Q243.2939 318.188 243.027 318.4963 Q242.8616 318.688 242.5984 318.7971 Q243.0232 318.9136 243.262 319.2464 Q243.5007 319.5791 243.5007 320.0604 Q243.5007 320.8311 242.9932 321.3161 Q242.4856 321.8011 241.5532 321.8011 Z" stroke="none"/><path d="M297.9913 311.7434 Q299.4898 311.7434 300.317 312.533 Q301.1441 313.3225 301.2354 314.3269 L300.1934 314.3269 Q300.0162 313.5642 299.4871 313.1184 Q298.9581 312.6726 298.002 312.6726 Q296.8365 312.6726 296.1194 313.4917 Q295.4024 314.3108 295.4024 316.0027 Q295.4024 317.3884 296.0496 318.2505 Q296.6968 319.1125 297.9805 319.1125 Q299.1622 319.1125 299.7798 318.2048 Q300.1075 317.7268 300.2686 316.948 L301.3106 316.948 Q301.171 318.1941 300.3868 319.0374 Q299.4468 320.0525 297.8516 320.0525 Q296.4766 320.0525 295.5421 319.22 Q294.3121 318.1189 294.3121 315.8201 Q294.3121 314.0745 295.2359 312.9573 Q296.2349 311.7434 297.9913 311.7434 Z" stroke="none"/><path d="M302.3106 311.9583 L303.3902 311.9583 L303.3902 315.2185 L307.4937 315.2185 L307.4937 311.9583 L308.5733 311.9583 L308.5733 319.8484 L307.4937 319.8484 L307.4937 316.1584 L303.3902 316.1584 L303.3902 319.8484 L302.3106 319.8484 Z" stroke="none"/><path d="M311.3893 321.8011 Q310.4343 321.8011 310.0038 321.2766 Q309.5733 320.7522 309.5733 320.0002 L310.2801 320.0002 Q310.3253 320.5228 310.4756 320.7597 Q310.7388 321.1845 311.4268 321.1845 Q311.9608 321.1845 312.2841 320.8988 Q312.6074 320.6131 312.6074 320.1619 Q312.6074 319.6054 312.2672 319.3836 Q311.9269 319.1618 311.3216 319.1618 Q311.2539 319.1618 311.1844 319.1637 Q311.1148 319.1656 311.0434 319.1693 L311.0434 318.5715 Q311.1487 318.5828 311.2201 318.5865 Q311.2915 318.5903 311.3742 318.5903 Q311.754 318.5903 311.9984 318.47 Q312.427 318.2594 312.427 317.718 Q312.427 317.3157 312.1412 317.0977 Q311.8555 316.8796 311.4757 316.8796 Q310.799 316.8796 310.5396 317.3308 Q310.3967 317.5789 310.3779 318.0376 L309.7086 318.0376 Q309.7086 317.4361 309.9493 317.015 Q310.3629 316.263 311.4043 316.263 Q312.2277 316.263 312.6789 316.6296 Q313.13 316.9962 313.13 317.6917 Q313.13 318.188 312.8631 318.4963 Q312.6977 318.688 312.4345 318.7971 Q312.8593 318.9136 313.0981 319.2464 Q313.3368 319.5791 313.3368 320.0604 Q313.3368 320.8311 312.8293 321.3161 Q312.3217 321.8011 311.3893 321.8011 Z" stroke="none"/><path fill="rgb(166,41,41)" d="M270.9617 340.1863 Q271.6384 340.1863 272.0144 339.9983 Q272.6052 339.7029 272.6052 338.9348 Q272.6052 338.1614 271.9768 337.8928 Q271.6223 337.7424 270.9241 337.7424 L269.0173 337.7424 L269.0173 340.1863 ZM271.3215 343.8279 Q272.3044 343.8279 272.7234 343.2585 Q272.9866 342.8987 272.9866 342.3884 Q272.9866 341.5291 272.2185 341.2175 Q271.8103 341.051 271.1389 341.051 L269.0173 341.051 L269.0173 343.8279 ZM267.97 336.8508 L271.3591 336.8508 Q272.7449 336.8508 273.3303 337.678 Q273.6741 338.1667 273.6741 338.8059 Q273.6741 339.5525 273.2498 340.0305 Q273.0295 340.283 272.616 340.4924 Q273.2229 340.7234 273.5237 341.0134 Q274.0554 341.5291 274.0554 342.4368 Q274.0554 343.1995 273.5774 343.8171 Q272.863 344.741 271.3054 344.741 L267.97 344.741 ZM275.2317 338.9885 L276.1501 338.9885 L276.1501 339.9822 Q276.2629 339.6921 276.7034 339.2759 Q277.1438 338.8596 277.7185 338.8596 Q277.7454 338.8596 277.8098 338.865 Q277.8743 338.8704 278.03 338.8865 L278.03 339.907 Q277.9441 339.8909 277.8716 339.8855 Q277.7991 339.8801 277.7131 339.8801 Q276.9827 339.8801 276.5906 340.3501 Q276.1985 340.8201 276.1985 341.4324 L276.1985 344.741 L275.2317 344.741 Z" stroke="none"/><path fill="black" d="M277.767 333.2441 L280.8011 333.2441 L280.8011 333.8005 L278.643 333.8005 L278.643 333.8156 L279.9853 334.7067 Q280.7372 335.2142 281.023 335.7105 Q281.2862 336.2106 281.2786 336.7557 Q281.2711 337.5565 280.7711 338.1431 Q280.2597 338.7409 279.2972 338.7559 Q278.4137 338.7484 277.8911 338.1957 Q277.3647 337.6581 277.3572 336.8083 Q277.376 334.9924 279.207 334.8458 L279.207 334.8307 L277.767 333.8005 ZM279.3611 338.2032 Q279.9702 338.1957 280.2522 337.7257 Q280.538 337.2783 280.538 336.7482 Q280.538 336.4474 280.4778 336.2256 Q280.4139 336.0075 280.3237 335.8609 Q280.2297 335.718 280.1394 335.624 Q280.0454 335.53 279.9853 335.4924 Q279.8311 335.3533 279.5604 335.3533 Q278.1054 335.3533 278.0979 336.8422 Q278.0979 337.4099 278.4287 337.7934 Q278.7596 338.1957 279.3611 338.2032 ZM281.6508 336.2745 L285.9633 336.2745 L285.9633 336.8271 L281.6508 336.8271 Z" stroke="none"/></g><g stroke-linecap="butt" font-size="7.7" transform="translate(-229,-238)" text-rendering="geometricPrecision" image-rendering="optimizeQuality" color-rendering="optimizeQuality" font-family="&apos;Helvetica&apos;" color-interpolation="linearRGB" stroke-width="1.2"><line y2="316.1896" fill="none" x1="461.1674" x2="447.2167" y1="316.1896"/><line y2="316.1896" fill="none" x1="472.1224" x2="486.0731" y1="316.1896"/><line y2="297.2421" fill="none" x1="466.6667" x2="466.6667" y1="309.933"/><line y2="271.9306" fill="none" x1="466.6667" x2="466.6667" y1="284.933"/><line y2="271.9306" fill="none" x1="471.6667" x2="471.6667" y1="284.933"/><line y2="271.9306" fill="none" x1="461.6667" x2="461.6667" y1="284.933"/></g><g font-size="11" transform="translate(-229,-238)" fill="rgb(144,144,144)" text-rendering="geometricPrecision" image-rendering="optimizeQuality" color-rendering="optimizeQuality" font-family="&apos;Helvetica&apos;" stroke="rgb(144,144,144)" color-interpolation="linearRGB"><path d="M466.8466 311.933 Q468.3451 311.933 469.1723 312.7226 Q469.9994 313.5121 470.0907 314.5165 L469.0487 314.5165 Q468.8715 313.7538 468.3424 313.308 Q467.8134 312.8622 466.8573 312.8622 Q465.6918 312.8622 464.9748 313.6813 Q464.2577 314.5004 464.2577 316.1923 Q464.2577 317.5781 464.9049 318.4401 Q465.5522 319.3022 466.8358 319.3022 Q468.0175 319.3022 468.6352 318.3945 Q468.9628 317.9164 469.1239 317.1376 L470.1659 317.1376 Q470.0263 318.3837 469.2421 319.227 Q468.3022 320.2421 466.7069 320.2421 Q465.3319 320.2421 464.3974 319.4096 Q463.1674 318.3085 463.1674 316.0097 Q463.1674 314.2641 464.0912 313.1469 Q465.0902 311.933 466.8466 311.933 Z" stroke="none"/><path d="M463.5353 321.2421 L464.6149 321.2421 L464.6149 324.5024 L468.7184 324.5024 L468.7184 321.2421 L469.798 321.2421 L469.798 329.1323 L468.7184 329.1323 L468.7184 325.4423 L464.6149 325.4423 L464.6149 329.1323 L463.5353 329.1323 Z" stroke="none"/><path d="M441.9409 311.933 Q443.4395 311.933 444.2666 312.7226 Q445.0938 313.5121 445.1851 314.5165 L444.1431 314.5165 Q443.9658 313.7538 443.4368 313.308 Q442.9077 312.8622 441.9517 312.8622 Q440.7861 312.8622 440.0691 313.6813 Q439.3521 314.5004 439.3521 316.1923 Q439.3521 317.5781 439.9993 318.4401 Q440.6465 319.3022 441.9302 319.3022 Q443.1118 319.3022 443.7295 318.3945 Q444.0571 317.9164 444.2183 317.1376 L445.2603 317.1376 Q445.1206 318.3837 444.3364 319.227 Q443.3965 320.2421 441.8013 320.2421 Q440.4263 320.2421 439.4917 319.4096 Q438.2617 318.3085 438.2617 316.0097 Q438.2617 314.2641 439.1855 313.1469 Q440.1846 311.933 441.9409 311.933 Z" stroke="none"/><path d="M426.2355 312.1479 L427.3151 312.1479 L427.3151 315.4081 L431.4186 315.4081 L431.4186 312.1479 L432.4982 312.1479 L432.4982 320.038 L431.4186 320.038 L431.4186 316.3481 L427.3151 316.3481 L427.3151 320.038 L426.2355 320.038 Z" stroke="none"/><path d="M435.3141 321.9908 Q434.3592 321.9908 433.9287 321.4663 Q433.4982 320.9418 433.4982 320.1898 L434.205 320.1898 Q434.2502 320.7125 434.4005 320.9493 Q434.6637 321.3742 435.3517 321.3742 Q435.8857 321.3742 436.209 321.0884 Q436.5323 320.8027 436.5323 320.3515 Q436.5323 319.7951 436.1921 319.5732 Q435.8518 319.3514 435.2465 319.3514 Q435.1788 319.3514 435.1093 319.3533 Q435.0397 319.3552 434.9683 319.3589 L434.9683 318.7611 Q435.0735 318.7724 435.145 318.7762 Q435.2164 318.7799 435.2991 318.7799 Q435.6789 318.7799 435.9232 318.6596 Q436.3519 318.4491 436.3519 317.9077 Q436.3519 317.5054 436.0661 317.2873 Q435.7804 317.0692 435.4006 317.0692 Q434.7239 317.0692 434.4644 317.5204 Q434.3216 317.7686 434.3028 318.2273 L433.6335 318.2273 Q433.6335 317.6257 433.8742 317.2046 Q434.2878 316.4526 435.3292 316.4526 Q436.1526 316.4526 436.6038 316.8192 Q437.0549 317.1858 437.0549 317.8813 Q437.0549 318.3777 436.788 318.6859 Q436.6226 318.8777 436.3594 318.9867 Q436.7842 319.1033 437.023 319.436 Q437.2617 319.7688 437.2617 320.25 Q437.2617 321.0208 436.7542 321.5058 Q436.2466 321.9908 435.3141 321.9908 Z" stroke="none"/><path d="M491.7523 311.933 Q493.2508 311.933 494.0779 312.7226 Q494.9051 313.5121 494.9964 314.5165 L493.9544 314.5165 Q493.7772 313.7538 493.2481 313.308 Q492.7191 312.8622 491.763 312.8622 Q490.5975 312.8622 489.8804 313.6813 Q489.1634 314.5004 489.1634 316.1923 Q489.1634 317.5781 489.8106 318.4401 Q490.4578 319.3022 491.7415 319.3022 Q492.9232 319.3022 493.5408 318.3945 Q493.8685 317.9164 494.0296 317.1376 L495.0716 317.1376 Q494.9319 318.3837 494.1478 319.227 Q493.2078 320.2421 491.6126 320.2421 Q490.2376 320.2421 489.303 319.4096 Q488.0731 318.3085 488.0731 316.0097 Q488.0731 314.2641 488.9969 313.1469 Q489.9959 311.933 491.7523 311.933 Z" stroke="none"/><path d="M496.0716 312.1479 L497.1512 312.1479 L497.1512 315.4081 L501.2547 315.4081 L501.2547 312.1479 L502.3343 312.1479 L502.3343 320.038 L501.2547 320.038 L501.2547 316.3481 L497.1512 316.3481 L497.1512 320.038 L496.0716 320.038 Z" stroke="none"/><path d="M505.1502 321.9908 Q504.1953 321.9908 503.7648 321.4663 Q503.3343 320.9418 503.3343 320.1898 L504.0411 320.1898 Q504.0862 320.7125 504.2366 320.9493 Q504.4998 321.3742 505.1878 321.3742 Q505.7217 321.3742 506.0451 321.0884 Q506.3684 320.8027 506.3684 320.3515 Q506.3684 319.7951 506.0282 319.5732 Q505.6879 319.3514 505.0826 319.3514 Q505.0149 319.3514 504.9453 319.3533 Q504.8758 319.3552 504.8044 319.3589 L504.8044 318.7611 Q504.9096 318.7724 504.981 318.7762 Q505.0525 318.7799 505.1352 318.7799 Q505.515 318.7799 505.7593 318.6596 Q506.188 318.4491 506.188 317.9077 Q506.188 317.5054 505.9022 317.2873 Q505.6165 317.0692 505.2367 317.0692 Q504.56 317.0692 504.3005 317.5204 Q504.1577 317.7686 504.1389 318.2273 L503.4696 318.2273 Q503.4696 317.6257 503.7103 317.2046 Q504.1238 316.4526 505.1653 316.4526 Q505.9887 316.4526 506.4398 316.8192 Q506.891 317.1858 506.891 317.8813 Q506.891 318.3777 506.6241 318.6859 Q506.4586 318.8777 506.1955 318.9867 Q506.6203 319.1033 506.8591 319.436 Q507.0978 319.7688 507.0978 320.25 Q507.0978 321.0208 506.5902 321.5058 Q506.0827 321.9908 505.1502 321.9908 Z" stroke="none"/><path d="M466.8466 286.933 Q468.3451 286.933 469.1723 287.7226 Q469.9994 288.5121 470.0907 289.5165 L469.0487 289.5165 Q468.8715 288.7538 468.3424 288.308 Q467.8134 287.8622 466.8573 287.8622 Q465.6918 287.8622 464.9748 288.6813 Q464.2577 289.5004 464.2577 291.1923 Q464.2577 292.5781 464.9049 293.4401 Q465.5522 294.3022 466.8358 294.3022 Q468.0175 294.3022 468.6352 293.3945 Q468.9628 292.9164 469.1239 292.1376 L470.1659 292.1376 Q470.0263 293.3837 469.2421 294.227 Q468.3022 295.2421 466.7069 295.2421 Q465.3319 295.2421 464.3974 294.4096 Q463.1674 293.3085 463.1674 291.0097 Q463.1674 289.2641 464.0912 288.1469 Q465.0902 286.933 466.8466 286.933 Z" stroke="none"/><path fill="rgb(48,80,248)" d="M463.5353 262.0405 L464.7975 262.0405 L468.7829 268.4321 L468.7829 262.0405 L469.798 262.0405 L469.798 269.9306 L468.6003 269.9306 L464.5558 263.5444 L464.5558 269.9306 L463.5353 269.9306 Z" stroke="none"/><path fill="rgb(166,41,41)" d="M465.295 347.1863 Q465.9717 347.1863 466.3477 346.9983 Q466.9385 346.7029 466.9385 345.9348 Q466.9385 345.1614 466.3101 344.8928 Q465.9556 344.7424 465.2574 344.7424 L463.3506 344.7424 L463.3506 347.1863 ZM465.6548 350.8279 Q466.6378 350.8279 467.0567 350.2585 Q467.3199 349.8987 467.3199 349.3884 Q467.3199 348.5291 466.5518 348.2175 Q466.1436 348.051 465.4722 348.051 L463.3506 348.051 L463.3506 350.8279 ZM462.3033 343.8508 L465.6924 343.8508 Q467.0782 343.8508 467.6636 344.678 Q468.0074 345.1667 468.0074 345.8059 Q468.0074 346.5525 467.5831 347.0305 Q467.3629 347.283 466.9493 347.4924 Q467.5562 347.7234 467.857 348.0134 Q468.3887 348.5291 468.3887 349.4368 Q468.3887 350.1995 467.9107 350.8171 Q467.1964 351.741 465.6387 351.741 L462.3033 351.741 ZM469.565 345.9885 L470.4835 345.9885 L470.4835 346.9822 Q470.5963 346.6921 471.0367 346.2759 Q471.4771 345.8596 472.0518 345.8596 Q472.0787 345.8596 472.1431 345.865 Q472.2076 345.8704 472.3633 345.8865 L472.3633 346.907 Q472.2774 346.8909 472.2049 346.8855 Q472.1324 346.8801 472.0464 346.8801 Q471.316 346.8801 470.9239 347.3501 Q470.5318 347.8201 470.5318 348.4324 L470.5318 351.741 L469.565 351.741 Z" stroke="none"/><path fill="black" d="M473.8373 342.7237 L478.1498 342.7237 L478.1498 343.2763 L473.8373 343.2763 Z" stroke="none"/><circle fill="black" r="1" cx="457.3333" cy="346" stroke="none"/><circle fill="black" r="1" cx="457.3333" cy="350" stroke="none"/></g></g></svg>
</div>

<div class="text-block">
<p>Should you need it, the two stages of the S<sub>N</sub>1 mechanism are:</p>
</div>

<div class="image-container"><img src="sn1secohslow.GIF"></div>

<div class="image-container"><img src="sn1seccnfast.GIF"></div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="cyanidett.html#top">Help! Talk me through these mechanisms</a>
<a href="../nucsubmenu.html#top">To menu of nucleophilic substitution reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>