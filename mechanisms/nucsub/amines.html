<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Multiple Nucleophilic Substitution Reactions of Halogenoalkanes and Ammonia | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Multiple nucleophilic substitution in the reaction between halogenoalkanes and ammonia">
<meta name="keywords" content="halogenoalkanes, haloalkanes, alkyl halides, nucleophile, nucleophilic substitution, substitution, sn1, sn2, ammonia, amines, ethylamine">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Multiple Nucleophilic Substitution Reactions of Halogenoalkanes and Ammonia</h1>

<div class="text-block">
<p>This page looks at further substitution in the nucleophilic substitution reaction between halogenoalkanes and ammonia following the formation of the primary amine. You could also think of this as a description of how primary, secondary and tertiary amines act as nucleophiles in a sequence of reactions with a halogenoalkane.</p>
<p>Only primary halogenoalkanes are considered on this page. Only one current A-level syllabus (AQA) is likely to ask about these reactions, and that only asks about primary halogenoalkanes in this context.</p>
</div>

<div class="note">
<p>Warning! Don't even think about reading this page unless you are confident about the nucleophilic substitution reaction involving <a href="ammoniatt.html#top">a primary halogenoalkane and ammonia giving a primary amine</a>. You need only worry about the S<sub>N</sub>2 reaction.</p>
<p>The reactions on the current page follow on from those described on that page. They are no harder, but they do look more difficult. They are certainly much more tedious! It is important that you understand what is explained on this page. Life is too valuable to waste it learning each of the reactions described here. If you understand what's happening, you can work out the equations if you need to.</p>
</div>

<h2>Why Do You Get Multiple Substitution?</h2>

<h3>The Initial Substitution – A Reminder</h3>

<div class="text-block">
<p>The lone pair on the nitrogen atom in an ammonia molecule is attracted towards the &delta;+ carbon in the halogenoalkane – in this example, bromoethane. It forms a bond with it – in the process expelling the bromine as a bromide ion.</p>
</div>

<div class="image-container"><img src="sn2nh3m1.GIF"></div>

<div class="text-block">
<p>A salt is formed – ethylammonium bromide.</p>
<p>There is then the possibility of another ammonia molecule removing a hydrogen from the positive ion to give a primary amine – ethylamine. A primary amine has the general formula RNH<sub>2</sub>. This reaction is reversible, and you will only get significant amounts of the free amine if you use a large excess of ammonia.</p>
</div>

<div class="image-container"><img src="sn2nh3m2.GIF"></div>

<h3>The Product as a Nucleophile</h3>

<div class="text-block">
<p>At the end of the initial substitution there will be a certain amount of free primary amine formed – the CH<sub>3</sub>CH<sub>2</sub>NH<sub>2</sub> in the example above. There may not be very much of it, but there will be some. If that small amount reacts with something else, the reverse reaction can't happen any longer. The free amine will continue to be produced, and what was originally a reversible reaction becomes a one-way reaction – provided the free amine is removed by some other reaction as soon as it is formed.</p>
<p>Now compare the structures of ammonia and ethylamine:</p>
</div>

<div class="image-container"><img src="nh3rnh2.GIF"></div>

<div class="text-block">
<p>You can think of the primary amine as a slightly modified ammonia molecule. It has a lone pair on the nitrogen atom and an even bigger &delta;- charge than in ammonia.</p>
<p>That means that the primary amine is going to be a better nucleophile than ammonia is. You can therefore get a reaction between it and a molecule of the halogenoalkane.</p>
</div>

<div class="note">
<p>Note: If you are interested in why the nitrogen in ethylamine has a greater degree of negative charge than the one in ammonia, you could follow this link and read about <a href="../../basicorg/acidbase/bases.html#top">organic bases</a>.</p>
</div>

<h2>Making a Secondary Amine From a Primary Amine</h2>

<h3>What is a Secondary Amine?</h3>

<div class="text-block">
<p>A secondary amine has the general formula R<sub>2</sub>NH. It is like an ammonia molecule (NH<sub>3</sub>) in which two of the hydrogens have been replaced by alkyl groups.</p>
</div>

<h3>The Mechanism</h3>

<div class="image-container"><img src="sn2rnh2m1.GIF"></div>

<div class="text-block">
<p>The lone pair on the nitrogen in the primary amine attacks the &delta;+ carbon exactly the same as the ammonia did. Bromine is lost as a bromide ion, and the immediate product is a salt called diethylammonium bromide – (CH<sub>3</sub>CH<sub>2</sub>)<sub>2</sub>NH<sub>2</sub><sup>+</sup> Br<sup>-</sup>. This is essentially ammonium bromide in which two of the hydrogens attached to the nitrogen have been replaced by ethyl groups.</p>
<p>This then reacts with ammonia in a reversible reaction, exactly as we've seen before:</p>
</div>

<div class="image-container"><img src="sn2rnh2m2.GIF"></div>

<div class="text-block">
<p>The organic product is a secondary amine – diethylamine. It is secondary because there are two alkyl groups attached to the nitrogen atom.</p>
</div>

<h2>Making a Tertiary Amine From the Secondary Amine</h2>

<h3>What is a Tertiary Amine?</h3>

<div class="text-block">
<p>A tertiary amine has the general formula R<sub>3</sub>N. It is like an ammonia molecule in which all three of the hydrogens have been replaced by alkyl groups.</p>
</div>

<h3>The Mechanism</h3>

<div class="text-block">
<p>The secondary amine still has an active lone pair of electrons on the nitrogen atom. That, in turn, can attack bromoethane if it happens to collide with it.
The two step sequence is exactly as before:</p>
</div>

<div class="image-container"><img src="sn2r2nhm1.GIF"></div>

<div class="text-block">
<p>In this first step, the bromine is again displaced as a bromide ion and you get a salt formed called triethylammonium bromide.</p>
</div>

<div class="note">
<p>Note: It has been necessary to rearrange the formula of the secondary amine in order to keep the appearance of the mechanism the same as the previous ones. Look carefully at it to be sure there isn't anything nasty going on!</p>
</div>

<div class="text-block">
<p>An ammonia molecule can then remove the hydrogen from the nitrogen in the reversible reaction:</p>
</div>

<div class="image-container"><img src="sn2r2nhm2.GIF"></div>

<div class="text-block">
<p>The organic product of this reaction is the tertiary amine, triethylamine. It is tertiary because of the three alkyl groups attached to the nitrogen.</p>
</div>

<h2>The Final Stage – Making a Quaternary Ammonium Salt</h2>

<h3>What is a Quaternary Ammonium Salt?</h3>

<div class="text-block">
<p>A quaternary ammonium salt is an ammonium salt (for example, NH<sub>4</sub><sup>+</sup> Br<sup>-</sup>) in which all the hydrogens have been replaced by an alkyl group – for example, (CH<sub>3</sub>CH<sub>2</sub>)<sub>4</sub>N<sup>+</sup> Br<sup>-</sup>. </p>
</div>

<h3>The Mechanism</h3>

<div class="text-block">
<p>The tertiary amine still has an active lone pair on the nitrogen and, once again, that can attack the &delta;+ carbon in the bromoethane.</p>
</div>

<div class="image-container"><img src="sn2r3n.GIF"></div>

<div class="text-block">
<p>But this time there is nowhere else for the reaction to go. There is no longer a hydrogen atom on the nitrogen that an ammonia molecule could remove, and so the reaction finally comes to an end.</p>
<p>The product is a salt called tetraethylammonium bromide, (CH<sub>3</sub>CH<sub>2</sub>)<sub>4</sub>N<sup>+</sup> Br<sup>-</sup>.</p>
</div>

<h2>If You React Bromoethane and Ammonia, What Do You Actually Get?</h2>

<div class="text-block">
<p>You will always get a mixture of all the above products unless you use a very large excess of bromoethane so that there is enough of it to carry the sequence all the way through to the quaternary ammonium salt.</p>
<p>You could favour formation of the primary amine (or its salt) by using a very large excess of ammonia. That way there is always a greater chance of a bromoethane molecule being hit by an ammonia molecule than any other possible nucleophile. Even so, there will always, by chance, be some collisions leading to the follow-on products.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../nucsubmenu.html#top">To menu of nucleophilic substitution reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>