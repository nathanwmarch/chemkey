<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Nucleophilic Substitution Reactions of Halogenoalkanes and Hydroxide Ions | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Facts and mechanisms for the reactions between halogenoalkanes and hydroxide ions – nucleophilic substitution">
<meta name="keywords" content="halogenoalkanes, haloalkanes, alkyl halides, nucleophile, nucleophilic substitution, substitution, sn1, sn2, hydroxide ions, sodium hydroxide">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Nucleophilic Substitution Reactions of Halogenoalkanes and Hydroxide Ions</h1>

<div class="text-block">
<p>This page gives you the facts and simple, uncluttered mechanisms for the nucleophilic substitution reactions between halogenoalkanes and hydroxide ions (from, for example, sodium hydroxide). If you want the mechanisms explained to you in detail, there is a link at the bottom of the page.</p>
</div>

<h2>The Reaction of Primary Halogenoalkanes With Hydroxide Ions</h2>

<div class="note">
<p>Important! If you aren't sure about the difference between <a href="types.html#top">primary, secondary and tertiary halogenoalkanes</a>, it is essential that you follow this link before you go on.</p>
</div>

<h3>The Facts</h3>

<div class="text-block">
<p>If a halogenoalkane is heated under reflux with a solution of sodium or potassium hydroxide, the halogen is replaced by -OH and an alcohol is produced. Heating under reflux means heating with a condenser placed vertically in the flask to prevent loss of volatile substances from the mixture.</p>
<p>The solvent is usually a 50/50 mixture of ethanol and water, because everything will dissolve in that. The halogenoalkane is insoluble in water. If you used water alone as the solvent, the halogenoalkane and the sodium hydroxide solution wouldn't mix and the reaction could only happen where the two layers met.</p>
<p>For example, using 1-bromopropane as a typical primary halogenoalkane:</p>
</div>

<div class="block-formula">
CH_3CH_2CH_2\text{Br} + {}^-OH \longrightarrow CH_3CH_2CH_2OH + \text{Br}^-
</div>

<div class="text-block">
<p>You could write the full equation rather than the ionic one, but it slightly obscures what's going on:</p>
</div>

<div class="block-formula">
CH_3CH_2CH_2\text{Br} + \text{Na}OH \longrightarrow CH_3CH_2CH_2OH + \text{NaBr}
</div>

<div class="text-block">
<p>The bromine (or other halogen) in the halogenoalkane is simply replaced by an -OH group – hence a substitution reaction. In this example, propan-1-ol is formed.</p>
</div>

<h3>The Mechanism</h3>

<div class="text-block">
<p>Here is the mechanism for the reaction involving bromoethane:</p>
</div>

<div class="image-container"><img src="sn2oh.GIF"></div>

<div class="text-block">
<p>This is an example of nucleophilic substitution.</p>
<p>Because the mechanism involves collision between two species in the slow step (in this case, the only step) of the reaction, it is known as an S<sub>N</sub>2 reaction.</p>
</div>

<div class="note">
<p>Note: Unless your syllabus specifically mentions S<sub>N</sub>2 by name, you can just call it nucleophilic substitution.</p>
</div>

<div class="text-block">
<p>If your examiners want you to show the transition state, draw the mechanism like this:</p>
</div>

<div class="image-container"><img src="sn2oh2.gif"></div>

<h2>The Reaction of Tertiary Halogenoalkanes With Hydroxide Ions</h2>

<h3>The Facts</h3>

<div class="text-block">
<p>The facts of the reaction are exactly the same as with primary halogenoalkanes. If the halogenoalkane is heated under reflux with a solution of sodium or potassium hydroxide in a mixture of ethanol and water, the halogen is replaced by -OH, and an alcohol is produced.</p>
<p>For example:</p>
</div>

<div class="block-formula">
(CH_3)_3C\text{Br} + {}^-OH \longrightarrow (CH_3)_3COH + \text{Br}^-
</div>

<div class="text-block">
<p>Or if you want the full equation rather than the ionic one:</p>
</div>

<div class="block-formula">
(CH_3)_3C\text{Br} + \text{Na}OH \longrightarrow (CH_3)_3COH + \text{NaBr}
</div>

<h3>The Mechanism</h3>

<div class="text-block">
<p>This mechanism involves an initial ionisation of the halogenoalkane:</p>
</div>

<div class="image-container"><img src="sn1slow.GIF"></div>

<div class="text-block">
<p>followed by a very rapid attack by the hydroxide ion on the carbocation (carbonium ion) formed:</p>
</div>

<div class="image-container"><img src="sn1ohfast.GIF"></div>

<div class="text-block">
<p>This is again an example ofnucleophilic substitution.

</p>
<p>This time the slow step of the reaction only involves one species – the halogenoalkane. It is known as an S<sub>N</sub>1 reaction.</p>
</div>

<h2>The Reaction of Secondary Halogenoalkanes With Hydroxide Ions</h2>

<h3>The Facts</h3>

<div class="text-block">
<p>The facts of the reaction are exactly the same as with primary or tertiary halogenoalkanes. The halogenoalkane is heated under reflux with a solution of sodium or potassium hydroxide in a mixture of ethanol and water.</p>
<p>For example:</p>
</div>

<div class="block-formula">
CH_3CH(\text{Br})CH_3 + {}^-OH \longrightarrow CH_3CH(OH)CH_3 + \text{Br}^-
</div>

<h3>The Mechanism</h3>

<div class="text-block">
<p>Secondary halogenoalkanes use both S<sub>N</sub>2 and S<sub>N</sub>1 mechanisms. For example, the S<sub>N</sub>2 mechanism is:</p>
</div>

<div class="image-container"><img src="sn2secoh.GIF"></div>

<div class="text-block">
<p>Should you need it, the two stages of the S<sub>N</sub>1 mechanism are:</p>
</div>

<div class="image-container"><img src="sn1secohslow.GIF"></div>

<div class="image-container"><img src="sn1secohfast.GIF"></div>

<div class="note">
<p>Note: There is another reaction between halogenoalkanes and hydroxide ions involving <a href="../elim/elim.html#top">an elimination reaction</a>. If you want to explore that as an alternative, follow this link.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="hydroxidett.html#top">Help! Talk me through these mechanisms</a>
<a href="../nucsubmenu.html#top">To menu of nucleophilic substitution reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>