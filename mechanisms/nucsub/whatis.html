<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>What is Nucleophilic Substitution? | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of the terms nucleophile and nucleophilic substitution, together with the general mechanisms for these reactions involving halogenoalkanes.">
<meta name="keywords" content="halogenoalkanes, haloalkanes, alkyl halides, nucleophile, nucleophilic substitution, substitution, sn1, sn2">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>What is Nucleophilic Substitution?</h1>

<h2>Background</h2>

<h3>Bonding in the Halogenoalkanes</h3>

<div class="text-block">
<p>Halogenoalkanes (also known as haloalkanes or alkyl halides) are compounds containing a halogen atom (fluorine, chlorine, bromine or iodine) joined to one or more carbon atoms in a chain.</p>
<p>The interesting thing about these compounds is the carbon-halogen bond, and all the nucleophilic substitution reactions of the halogenoalkanes involve breaking that bond.</p>
</div>

<h4>The polarity of the carbon-halogen bonds</h4>

<div class="text-block">
<p>With the exception of iodine, all of the halogens are more electronegative than carbon.</p>
</div>

<h4>Electronegativity values (Pauling scale)</h4>

<div class="inline">
<table class="list-table">
<tbody>
<tr><td>C</td><td>2.5</td></tr>
</tbody>
</table>

<table class="list-table">
<tbody>
<tr><td>F</td><td>4.0</td></tr>
<tr><td>Cl</td><td>3.0</td></tr>
<tr><td>Br</td><td>2.8</td></tr>
<tr><td>I</td><td>2.5</td></tr>
</tbody>
</table>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="../../basicorg/bonding/eneg.html#top">electronegativity and bond polarity</a> follow this link before you read on.</p>
</div>

<div class="text-block">
<p>That means that the electron pair in the carbon-halogen bond will be dragged towards the halogen end, leaving the halogen slightly negative (&delta;-) and the carbon slightly positive (&delta;+) – except in the carbon-iodine case.</p>
<p>Although the carbon-iodine bond doesn't have a permanent dipole, the bond is very easily polarised by anything approaching it. Imagine a negative ion approaching the bond from the far side of the carbon atom:</p>
</div>

<div class="image-container"><img src="cidipole.GIF"></div>

<div class="text-block">
<p>The fairly small polarity of the carbon-bromine bond will be increased by the same effect.</p>
</div>

<h4>The strengths of the carbon-halogen bonds</h4>

<div class="note">
<p>Note: If you haven't done any work on bond strengths, or are a bit rusty, it doesn't matter. Just realise that the bigger the number, the stronger the bond. And don't worry if you have found slightly different numbers in a different data source – there is a lot of variability in the quoted values, but the overall pattern is still the same.</p>
</div>

<div class="text-block">
<p>Look at the strengths of various bonds (all values in kJ mol<sup>-1</sup>).</p>
</div>

<div class="inline">
<table class="list-table">
<tbody>
<tr><td>C–H</td><td>413</td></tr>
</tbody>
</table>

<table class="list-table">
<tbody>
<tr><td>C–F</td><td>467</td></tr>
<tr><td>C–Cl</td><td>346</td></tr>
<tr><td>C–Br</td><td>290</td></tr>
<tr><td>C–I</td><td>228</td></tr>
</tbody>
</table>
</div>

<div class="text-block">
<p>In all of these nucleophilic substitution reactions, the carbon-halogen bond has to be broken at some point during the reaction. The harder it is to break, the slower the reaction will be.</p>
<p>The carbon-fluorine bond is very strong (stronger than C-H) and isn't easily broken. It doesn't matter that the carbon-fluorine bond has the greatest polarity – the strength of the bond is much more important in determining its reactivity. You might therefore expect fluoroalkanes to be very unreactive – and they are! We shall simply ignore them from now on.</p>
<p>In the other halogenoalkanes, the bonds get weaker as you go from chlorine to bromine to iodine.</p>
<p>That means that chloroalkanes react most slowly, bromoalkanes react faster, and iodoalkanes react faster still.</p>
</div>

<h3>Rates of Reaction: R–<span class="attention">Cl</span> &lt; R—<span class="attention">Br</span> &lt; R–<span class="attention">I</span></h3>

<div class="text-block">
<p>Where "&lt;" is read as "is less than" – or, in this instance, "is slower than", and R represents any alkyl group.</p>
</div>

<div class="note">
<p>Warning! Before you read on it is essential that you know exactly what your syllabus says about these reactions so that you can extract the right amount of detail from what follows. The problem is that there are two different mechanisms depending on the type of halogenoalkane you are using (whether primary, secondary or tertiary). Some syllabuses try to make things simpler by restricting you to just one of the two mechanisms. Where the syllabus is vague, look at recent exam papers and mark schemes, or any support material published by your examiners.</p>
<p>Haven't got a <a href="../../syllabuses.html#top">syllabus or recent exam papers?</a> If you are working to one of the UK-based syllabuses for 16 – 18 year olds, follow this link to find out how to get them. You must know what your examiners expect in this topic.</p>
</div>

<h2>Nucleophilic Substitution in Primary Halogenoalkanes</h2>

<div class="text-block">
<p>You will need to know about this if your syllabus talks about "primary halogenoalkanes" or about S<sub>N</sub>2 reactions. If the syllabus is vague, check recent exam papers and mark schemes, and compare them against what follows.</p>
</div>

<h3>Nucleophiles</h3>

<div class="text-block">
<p>A nucleophile is a species (an ion or a molecule) which is strongly attracted to a region of positive charge in something else.</p>
<p>Nucleophiles are either fully negative ions, or else have a strongly &delta;- charge somewhere on a molecule. Common nucleophiles are hydroxide ions, cyanide ions, water and ammonia.</p>
</div>

<div class="image-container"><img src="nucleophiles.GIF"></div>

<div class="text-block">
<p>Notice that each of these contains at least one lone pair of electrons, either on an atom carrying a full negative charge, or on a very electronegative atom carrying a substantial &delta;- charge.</p>
</div>

<h3>The Nucleophilic Substitution Reaction – An S<sub>N</sub>2 Reaction</h3>

<div class="text-block">
<p>We'll talk this mechanism through using an ion as a nucleophile, because it's slightly easier. The water and ammonia mechanisms involve an extra step which you can read about on the pages describing those particular mechanisms.</p>
</div>

<div class="image-container">
<img src="c2h5br.GIF">
</div>

<div class="text-block">
<p>We'll take bromoethane as a typical primary halogenoalkane. The bromoethane has a polar bond between the carbon and the bromine.</p>
<p>We'll look at its reaction with a general purpose nucleophilic ion which we'll call Nu<sup>-</sup>. This will have at least one lone pair of electrons. Nu<sup>-</sup> could, for example, be OH<sup>-</sup> or CN<sup>-</sup>.</p>
</div>

<div class="image-container"><img src="sn2gen.GIF"></div>

<div class="text-block">
<p>The lone pair on the Nu<sup>-</sup> ion will be strongly attracted to the &delta;+ carbon, and will move towards it, beginning to make a co-ordinate (dative covalent) bond. In the process the electrons in the C-Br bond will be pushed even closer towards the bromine, making it increasingly negative.</p>
</div>

<div class="note">
<p>Note: A co-ordinate bond is a covalent bond in which both electrons come from one of the atoms.</p>
</div>

<div class="text-block">
<p>The movement goes on until the -Nu is firmly attached to the carbon, and the bromine has been expelled as a Br<sup>-</sup> ion.</p>
</div>

<div class="note">
<p>Note: We haven't shown all the lone pairs on the bromine. These other lone pairs aren't involved in the reaction, and including them simply clutters the diagram to no purpose.</p>
</div>

<h4>Things to notice</h4>

<div class="text-block">
<p>The Nu<sup>-</sup> ion approaches the &delta;+ carbon from the side away from the bromine atom. The large bromine atom hinders attack from its side and, being &delta;-, would repel the incoming Nu<sup>-</sup> anyway. This attack from the back is important if you need to understand why tertiary halogenoalkanes have a different mechanism. We'll discuss this later on this page.</p>
<p>There is obviously a point in which the Nu<sup>-</sup> is half attached to the carbon, and the C-Br bond is half way to being broken. This is called a transition state. It isn't an intermediate. You can't isolate it – even for a very short time. It's just the mid-point of a smooth attack by one group and the departure of another.</p>
</div>

<h4>How to write the mechanism</h4>

<div class="text-block">
<p>The simplest way is:</p>
</div>

<div class="image-container"><img src="sn2geneq.GIF"></div>

<div class="note">
<p>Note: In exam you must show the lone pair of electrons on the nucleophile (in this case, the Nu<sup>-</sup> ion). It probably doesn't matter whether you show them on the departing Br<sup>-</sup> ion or not.</p>
<p>If you aren't happy about the use of <a href="../../basicorg/conventions/curlies.html#top">curly arrows</a> in mechanisms, follow this link before you go on.</p>
</div>

<div class="text-block">
<p>Technically, this is known as an S<sub>N</sub>2 reaction. S stands for substitution, N for nucleophilic, and the 2 is because the initial stage of the reaction involves two species – the bromoethane and the Nu<sup>-</sup> ion. If your syllabus doesn't refer to S<sub>N</sub>2 reactions by name, you can just call it nucleophilic substitution.</p>
<p>Some examiners like you to show the transition state in the mechanism, in which case you need to write it in a bit more detail – showing how everything is arranged in space.</p>
</div>

<div class="image-container"><img src="sn2geneq2.gif"></div>

<div class="text-block">
<p>Be very careful when you draw the transition state to make a clear difference between the dotted lines showing the half-made and half-broken bonds, and those showing the bonds going back into the paper.</p>
<p>Notice that the molecule has been inverted during the reaction – rather like an umbrella being blown inside-out.</p>
</div>

<div class="note">
<p>Note: If you aren't happy about the various ways of <a href="../../basicorg/conventions/draw.html#top">drawing bonds</a>, it is important to follow this link to find out exactly what the various symbols mean.</p>
<p>It is also important to know which of these ways of drawing the mechanism your particular examiners want you to use. If you haven't already checked your <a href="../../syllabuses.html#top">syllabus, recent exam papers and mark schemes</a>, you must do so! At the time of writing, Edexcel, for example, wanted the transition state included, and that isn't obvious from their syllabus. You have to check mark schemes and examiners reports.</p>
</div>

<h2>Nucleophilic Substitution in Tertiary Halogenoalkanes</h2>

<div class="note">
<p>Warning! Check your syllabus, past papers and any support material published by your examiners to find out whether you need this. If there's no mention of tertiary halogenoalkanes or S<sub>N</sub>1 reactions, then you probably don't need it.</p>
</div>

<div class="chemical-structure" name="2-bromo-2-methylpropane" type="2d" label="2-bromo-2-methylpropane – a tertiary halogenoalkane"></div>

<div class="text-block">
<p>Remember that a tertiary halogenoalkane has three alkyl groups attached to the carbon with the halogen on it. These alkyl groups can be the same or different, but in this section, we shall just consider a simple one, (CH<sub>3</sub>)<sub>3</sub>CBr -
2-bromo-2-methylpropane.</p>
</div>

<h3>The Nucleophilic Substitution Reaction – An S<sub>N</sub>1 Reaction</h3>

<div class="text-block">
<p>Once again, we'll talk this mechanism through using an ion as a nucleophile, because it's slightly easier, and again we'll look at the reaction of a general purpose nucleophilic ion which we'll call Nu<sup>-</sup>. This will have at least one lone pair of electrons.</p>
</div>

<h4>Why is a different mechanism necessary?</h4>

<div class="text-block">
<p>You will remember that when a nucleophile attacks a primary halogenoalkane, it approaches the &delta;+ carbon atom from the side away from the halogen atom.</p>
<p>With a tertiary halogenoalkane, this is impossible. The back of the molecule is completely cluttered with CH<sub>3</sub> groups.</p>
</div>

<div class="image-container"><img src="clutter.GIF"></div>

<div class="text-block">
<p>Since any other approach is prevented by the bromine atom, the reaction has to go by an alternative mechanism.</p>
</div>

<h4>The alternative mechanism</h4>

<div class="note">
<p>Important! To understand this section, you need to know what a <a href="../eladd/carbonium.html#top">carbocation (carbonium ion)</a> is, and about the relative stabilities of primary, secondary and tertiary carbocations.</p>
</div>

<div class="text-block">
<p>The reaction happens in two stages. In the first, a small proportion of the halogenoalkane ionises to give a carbocation and a bromide ion.</p>
</div>

<div class="image-container"><img src="sn1slow.GIF"></div>

<div class="text-block">
<p>This reaction is possible because tertiary carbocations are relatively stable compared with secondary or primary ones. Even so, the reaction is slow.</p>
<p>Once the carbocation is formed, however, it would react immediately it came into contact with a nucleophile like Nu<sup>-</sup>. The lone pair on the nucleophile is strongly attracted towards the positive carbon, and moves towards it to create a new bond.</p>
</div>

<div class="image-container"><img src="sn1fast.GIF"></div>

<div class="text-block">
<p>How fast the reaction happens is going to be governed by how fast the halogenoalkane ionises. Because this initial slow step only involves one species, the mechanism is described as S<sub>N</sub>1 – substitution, nucleophilic, one species taking part in the initial slow step.</p>
</div>

<h4>Why don't primary halogenoalkanes use the S<sub>N</sub>1 mechanism?</h4>

<div class="text-block">
<p>If a primary halogenoalkane did use this mechanism, the first step would be, for example:</p>
</div>

<div class="image-container"><img src="sn1primslow.GIF"></div>

<div class="text-block">
<p>A primary carbocation would be formed, and this is much more energetically unstable than the tertiary one formed from tertiary halogenoalkanes – and therefore much more difficult to produce.</p>
<p>This instability means that there will be a very high activation energy for the reaction involving a primary halogenoalkane. The activation energy is much less if it undergoes an S<sub>N</sub>2 reaction – and so that's what it does instead.</p>
</div>

<h2>Nucleophilic Substitution in Secondary Halogenoalkanes</h2>

<div class="chemical-structure" name="2-bromopropane" type="2d" label="2-bromopropane – a secondary halogenoalkane"></div>

<div class="text-block">
<p>There isn't anything new in this. Secondary halogenoalkanes will use both mechanisms – some molecules will react using the S<sub>N</sub>2 mechanism and others the S<sub>N</sub>1.</p>
<p>The S<sub>N</sub>2 mechanism is possible because the back of the molecule isn't completely cluttered by alkyl groups and so the approaching nucleophile can still get at the &delta;+ carbon atom.</p>
<p>The S<sub>N</sub>1 mechanism is possible because the secondary carbocation formed in the slow step is more stable than a primary one. It isn't as stable as a tertiary one though, and so the S<sub>N</sub>1 route isn't as effective as it is with tertiary halogenoalkanes.</p>
</div>

<h1 class="no-print">Interactive</h1>

<div class="image-container full-width no-print"><img src="Aexample.png" width="607" height="138" border="0" usemap="#Map" />
<map name="Map" id="Map">
<area shape="rect" coords="154,79,264,131" href="javascript:Jmol.script(jmolApplet0, 'load br+mecl.xyz;select all;set labelOffset 0 0;font label 20 serif bold;color label orange;wireframe 0.15;spacefill 30%;moveto 0.0 -829 428 361 106.04;connect 2.1 3 (atomno=2) (atomno=5) PARTIAL;connect 2.4 3.2 (atomno=2) (atomno=6) PARTIAL;frame 1;select atomno=6 and model=1.1;label -;select atomno=6 and model=1.2;label -;select atomno=6 and model=1.3;label -;select atomno=6 and model=1.4;label -;select atomno=5 and model=1.16;label -;select atomno=5 and model=1.17;label -;select atomno=5 and model=1.18;label -;select atomno=5 and model=1.19;label -;select atomno=5 and model=1.20;label -;select atomno=5 and model=1.21;label -;draw arrow1 ARROW {-1.7221336 2.6127853 -0.0725956} {-0.6569023 1.4039688 0.30386162} {-0.14348412 0.23309803 -0.012130737};draw fill noMesh noDots notFrontOnly fullylit;color draw opaque [xffa500];frame 1;draw arrow2 ARROW {0.7572918 -1.1469498 0.041915894} {0.28889275 -1.4835367 1.0095739} {0.8438797 -1.8804264 0.5594969};draw fill noMesh noDots notFrontOnly fullylit;color draw opaque [xffa500];')" />
<area shape="rect" coords="272,100,332,119" href="javascript:Jmol.script(jmolApplet0, 'load br+mecl.xyz;select all;set labelOffset 0 0;font label 20 serif bold;color label orange;wireframe 0.15;spacefill 30%;moveto 0.0 -829 428 361 106.04;connect 2.1 3 (atomno=2) (atomno=5) PARTIAL;connect 2.4 3.2 (atomno=2) (atomno=6) PARTIAL;select atomno=6 and model=1.1;label -;select atomno=6 and model=1.2;label -;select atomno=6 and model=1.3;label -;select atomno=6 and model=1.4;label -;select atomno=5 and model=1.16;label -;select atomno=5 and model=1.17;label -;select atomno=5 and model=1.18;label -;select atomno=5 and model=1.19;label -;select atomno=5 and model=1.20;label -;select atomno=5 and model=1.21;label -;anim mode once;frame 1; delay 0.5; anim on;set echo bottom center;font echo 16 sansserif bold;echo Plays once through, then stops;')" />
<area shape="rect" coords="341,78,451,132" href="javascript:Jmol.script(jmolApplet0, 'load br+mecl.xyz;frame 21;select all;set labelOffset 0 0;font label 20 serif bold;color label orange;wireframe 0.15;spacefill 30%;moveto 0.0 -829 428 361 106.04;connect 2.1 3 (atomno=2) (atomno=5) PARTIAL;connect 2.4 3.2 (atomno=2) (atomno=6) PARTIAL;select atomno=6 and model=1.1;label -;select atomno=6 and model=1.2;label -;select atomno=6 and model=1.3;label -;select atomno=6 and model=1.4;label -;select atomno=5 and model=1.16;label -;select atomno=5 and model=1.17;label -;select atomno=5 and model=1.18;label -;select atomno=5 and model=1.19;label -;select atomno=5 and model=1.20;label -;select atomno=5 and model=1.21;label -;')" />
</map>
</div>
<div class="jsmol-container">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/jsmol/JSmol.min.js"></script>
<script>
var jmolApplet0;

var Info = {
	width: 350,
	height: 350,
	color: "white",
	serverURL: "https://shout.education/ChemKey/Libraries/jsmol/php/jsmol.php",
	addSelectionOptions: false,
	use: "HTML5 IMAGE",
j2sPath: "https://shout.education/ChemKey/Libraries/jsmol/j2s",
	jarPath: "https://shout.education/ChemKey/Libraries/jsmol/java",
	jarFile: "JmolAppletSigned.jar",
	isSigned: false,
	disableInitialConsole: true,
	script: "load br+mecl.xyz;select all;set labelOffset 0 0;font label 20 serif bold;color label orange;wireframe 0.15;spacefill 30%;moveto 0.0 -829 428 361 106.04;connect 2.1 3 (atomno=2) (atomno=5) PARTIAL;connect 2.4 3.2 (atomno=2) (atomno=6) PARTIAL;select atomno=6 and model=1.1;label -;select atomno=6 and model=1.2;label -;select atomno=6 and model=1.3;label -;select atomno=6 and model=1.4;label -;select atomno=5 and model=1.16;label -;select atomno=5 and model=1.17;label -;select atomno=5 and model=1.18;label -;select atomno=5 and model=1.19;label -;select atomno=5 and model=1.20;label -;select atomno=5 and model=1.21;label -;set antialiasDisplay true; set platformSpeed 8",
	allowJavaScript: true
}

jmolApplet0 = Jmol.getApplet("jmolApplet0", Info)
</script>
<p>Reproduced with permission from <a href="http://www.chemtube3d.com/ALevel.html" target="_blank">ChemTube3d.com</a></p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../nucsubmenu.html#top">To menu of nucleophilic substitution reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>

</div>
</body></html>