<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Free Radical Substitution in the Reaction of Methane and Chlorine | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Facts and mechanism for the free radical substitution reaction between methane and chlorine.">
<meta name="keywords" content="chlorine, methane, free radical substitution, free radical, free radicals, radical, radicals, substitution, chain reaction, mechanism, ultraviolet, uv, UV, photocatalysis">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Free Radical Substitution in the Reaction of Methane and Chlorine</h1>

<h2> A Free Radical Substitution Reaction</h2>

<div class="text-block">
<p>This page gives you the facts and a simple, uncluttered mechanism for the free radical substitution reaction between methane and chlorine. If you want the mechanism explained to you in detail, there is a link at the bottom of the page.</p>
</div>

<h3>The facts</h3>
<div class="text-block">
<p>If a mixture of methane and chlorine is exposed to a flame, it explodes – producing carbon and hydrogen chloride. This isn't a very useful reaction!</p>
<p>The reaction we are going to explore is a more gentle one between methane and chlorine in the presence of ultraviolet light – typically sunlight. This is a good example of a photochemical reaction – a reaction brought about by light.</p>
</div>

<div class="note">
<p>Note: These reactions are sometimes described as examples of photocatalysis – reactions catalysed by light. It is better to use the term "photochemical" and keep the word "catalysis" for reactions speeded up by actual substances rather than light.</p>
</div>

<div class="block-formula">
CH_4 \text{Cl}_2 \longrightarrow CH_3\text{Cl} + H\text{Cl}
</div>

<div class="text-block">
<p>The organic product is chloromethane.</p>
<p>One of the hydrogen atoms in the methane has been replaced by a chlorine atom, so this is a substitution reaction. However, the reaction doesn't stop there, and all the hydrogens in the methane can in turn be replaced by chlorine atoms. Multiple substitution is dealt with on a separate page, and you will find a link to that at the bottom of this page.</p>
</div>

<h3>The Mechanism</h3>

<div class="text-block">
<p>The mechanism involves a chain reaction. During a chain reaction, for every reactive species you start off with, a new one is generated at the end – and this keeps the process going.</p>
</div>

<div class="definition">
<p>Species: a useful word which is used in chemistry to mean any sort of particle you want it to mean. It covers molecules, ions, atoms, or (in this case) free radicals.</p>
</div>

<div class="text-block">
<p>The over-all process is known as free radical substitution, or as a free radical chain reaction.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about the words free radical or substitution, read the page <a href="whatis.html#top">What is free radical substitution?</a>.</p>
</div>

<h4>Chain initiation</h4>

<div class="text-block">
<p>The chain is initiated (started) by UV light breaking a chlorine molecule into free radicals.</p>
</div>

<div class="block-formula">
\text{Cl}_2 \longrightarrow 2\text{Cl}{\bullet}
</div>

<h4>Chain propagation reactions</h4>

<div class="text-block">
<p>These are the reactions which keep the chain going.</p>
</div>

<div class="block-formula">
\begin{aligned}
CH_4 + \text{Cl}{\bullet} &\longrightarrow {\bullet}CH_3 + H\text{Cl} \\
\\
{\bullet}CH_3 + \text{Cl}_2 &\longrightarrow CH_3\text{Cl} + \text{Cl}{\bullet}
\end{aligned}
</div>

<h4>Chain termination reactions</h4>

<div class="text-block">
<p>These are reactions which remove free radicals from the system without replacing them by new ones.</p>
</div>

<div class="block-formula">
\begin{aligned}
2{Cl}{\bullet} &\longrightarrow \text{Cl}_2 \\
\\
{\bullet}CH_3 + \text{Cl}{\bullet} &\longrightarrow CH_3\text{Cl} \\
\\
{\bullet}CH_3 + {\bullet}CH_3 &\longrightarrow CH_3CH_3
\end{aligned}
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="ch4andcl2tt.html#top">Help! Talk me through this mechanism</a>
<a href="multisubcl.html#top">Look at multiple substitution in this reaction </a>
<a href="sidereactcl.html#top">Look at why side reactions happen in this reaction </a>
<a href="../frmenu.html#top">To menu of free radical reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>