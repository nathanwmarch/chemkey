<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Multiple Free Radical Substitutions in the Reaction of Methane and Chlorine | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A guide to the mechanism of the free radical substitution reactions between methane and chlorine where more than one hydrogen atom is replaced.">
<meta name="keywords" content="chlorine, methane, free radical substitution, free radical, free radicals, radical, radicals, substitution, chain reaction, mechanism, photocatalysis">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Multiple Free Radical Substitutions in the Reaction of Methane and Chlorine</h1>

<div class="note">
<p>Warning! We are just about to muddy the water quite considerably! Don't go on until you are sure that you understand the mechanism for the production of chloromethane – and are confident that you could write it in an exam. If you aren't sure about it, <a href="ch4andcl2.html#top">go back to that reaction</a> and look at it again.</p>
<p>It would be worth checking your syllabus and past exam papers to see if you need to know about these further substitution reactions.</p>
</div>

<h2>The Facts</h2>

<div class="text-block">
<p>When a mixture of methane and chlorine is exposed to ultraviolet light – typically sunlight – a substitution reaction occurs and the organic product is chloromethane.</p>
</div>

<div class="block-formula">
CH_4 + \text{Cl}_2 \longrightarrow CH_3\text{Cl} + H\text{Cl}
</div>

<div class="text-block">
<p>However, the reaction doesn't stop there, and all the hydrogens in the methane can in turn be replaced by chlorine atoms. That means that you could get any of chloromethane, dichloromethane, trichloromethane or tetrachloromethane.</p>
</div>

<div class="block-formula">
\begin{aligned}
CH_4 + \text{Cl}_2 &\longrightarrow CH_3\text{Cl} + H\text{Cl} \\
\\
CH_3\text{Cl} + \text{Cl}_2 &\longrightarrow CH_2\text{Cl}_2 + H\text{Cl} \\
\\
CH_2\text{Cl}_2 + \text{Cl}_2 &\longrightarrow CH\text{Cl}_3 + H\text{Cl} \\
\\
CH\text{Cl}_3 + \text{Cl}_2 &\longrightarrow C\text{Cl}_4 + H\text{Cl} \\
\end{aligned}
</div>

<div class="text-block">
<p>You might think that you could control which product you got by the proportions of methane and chlorine you used, but it isn't as simple as that. If you use enough chlorine you will eventually get CCl<sub>4</sub>, but any other proportions will always lead to a mixture of products.</p>
</div>

<h2>The Mechanisms</h2>

<div class="text-block">
<p>The formation of multiple substitution products like di-, tri- and tetrachloromethane can be explained in just the same sort of way as the formation of the original chloromethane. You just have to look at the likely collisions as the reaction progresses.</p>
</div>

<h3>Making Dichloromethane</h3>

<div class="text-block">
<p>You will remember that the over-all equation for the first stage of the reaction is</p>
</div>

<div class="block-formula">
CH_4 + \text{Cl}_2 \longrightarrow CH_3\text{Cl} + H\text{Cl}
</div>

<div class="text-block">
<p>As the reaction proceeds, the methane is getting used up and chloromethane is taking its place. That means that the argument about what a chlorine radical is likely to hit changes during the course of the reaction. As time goes by there is an increasing chance of it hitting a chloromethane molecule rather than a methane molecule.</p>
<p>When that happens, the chlorine radical can take a hydrogen from the chloromethane just as well as it could from a methane. In this new case:</p>
</div>

<div class="block-formula">
CH_3\text{Cl} + \text{Cl}{\bullet} \longrightarrow {\bullet}CH_2\text{Cl} + H\text{Cl}
</div>

<div class="note">
Notice: The dot representing the electron has been moved against the carbon which is the atom with the unpaired electron. It would be potentially confusing to leave it next to the chlorine.
</div>

<div class="text-block">
<p>The chloromethyl radical formed can then interact with a chlorine molecule in a new propagation step</p>
</div>

<div class="block-formula">
{\bullet}CH_2\text{Cl} + \text{Cl}_2 \longrightarrow CH_2\text{Cl}_2 + \text{Cl}{\bullet}
</div>

<div class="text-block">
<p> and so dichloromethane is formed and a chlorine radical regenerated.</p>
<p>These propagation steps continue until the chain is terminated by any two radicals colliding and combining together.</p>
</div>

<h3>Making Tri- and Tetrachloromethane</h3>

<div class="text-block">
<p>Obviously, as time goes on, there is an increasing chance of the dichloromethane being hit by a chlorine radical – producing these propagation steps giving trichloromethane:</p>
</div>

<div class="block-formula">
\begin{aligned}
CH_2\text{Cl}_2 + \text{Cl}{\bullet} &\longrightarrow {\bullet}CH\text{Cl}_2 + H\text{Cl} \\
\\
{\bullet}CH\text{Cl}_2 + \text{Cl}_2 &\longrightarrow CH\text{Cl}_3 + \text{Cl}{\bullet}
\end{aligned}
</div>

<div class="note">
<p>Care! Don't just skip lightly over these equations. Look carefully at each one so that you understand what is happening, and can relate it to what has gone before. Talk through the equations with yourself.</p>
<p> For example: "A chlorine radical hits the dichloromethane molecule and steals a hydrogen. That leaves a new radical (I don't know what it's called, but that doesn't really matter, as long as I can work out its formula if I have to!), which then bumps into a chlorine molecule – etc, etc."</p>
<p>Doing this helps you to focus properly on the equations. If you just read them quickly, you'll have forgotten all about them again in 15 seconds!</p>
</div>

<div class="text-block">
<p>As the amount of trichloromethane builds up, then you will get these steps giving tetrachloromethane:</p>
</div>

<div class="block-formula">
\begin{aligned}
CH\text{Cl}_3 + \text{Cl}{\bullet} &\longrightarrow {\bullet}C\text{Cl}_3 + H\text{Cl} \\
\\
{\bullet}C\text{Cl}_3 + \text{Cl}_2 &\longrightarrow C\text{Cl}_4 + \text{Cl}{\bullet}
\end{aligned}
</div>

<div class="text-block">
<p>This is why you will always get a mixture of products whatever the reaction proportions of methane and chlorine you use. The whole process is simply governed by chance. Having produced some chloromethane there is no way that you can prevent it from being hit by chlorine radicals, and similarly for dichloromethane and trichloromethane.</p>
</div>

<h3>Trying to Produce Mainly One Product</h3>

<div class="text-block">
<p>If you wanted tetrachloromethane, you could of course get it by using a large excess of chlorine, so that eventually all the hydrogens would be replaced.</p>
<p>If you wanted mainly chloromethane, you could favour this by using a huge excess of methane so that the chances were always greater of a chlorine radical hitting a methane rather than anything else – but even so, you would still get some mixture of products.</p>
<p>There is no obvious way of getting mainly dichloromethane or trichloromethane.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="ch4andcl2.html#top">Look at single substitution again</a>
<a href="sidereactcl.html#top">Look at why side reactions happen in this reaction</a>
<a href="../frmenu.html#top">To menu of free radical reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>