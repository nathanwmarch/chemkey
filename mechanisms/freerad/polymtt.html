<!DOCTYPE html>
<html><head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Explaining Free Radical Addition in the Polymerisation of Ethene | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="A step-by-step guide to the mechanism for the polymerisation of ethene by a free radical addition reaction">
<meta name="keywords" content="ethene, poly(ethene), polythene, free radical addition, free radical, free radicals, radical, radicals, addition, chain reaction, mechanism, polymerisation">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-109203606-1');</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Explaining Free Radical Addition in the Polymerisation of Ethene</h1>

<div class="text-block">
<p>This page guides you through the mechanism for the polymerisation of ethene by a free radical addition reaction.</p>
<p>We are going to talk through this mechanism in a very detailed way so that you get a feel for what is going on. You couldn't possibly do the same thing in an exam. At the bottom of the page, you will find the condensed down version corresponding to the sort of answer you would produce in an exam.</p>
<p>You will remember that during the polymerisation of ethene, thousands of ethene molecules join together to make poly(ethene) – commonly called polythene. The reaction is done at high pressures in the presence of a trace of oxygen as an initiator.</p>
</div>

<div class="block-formula">
n~CH_2{=}CH_2 \longrightarrow {{[\hspace{1.5mu}}\mathllap{-}CH_2{-}CH_2{-}\mathllap{]\hspace{2.5mu}}}_n
</div>

<h3>The Function of the Oxygen – Chain Initiation</h3>

<div class="text-block">
<p>The oxygen reacts with some of the ethene to give an organic peroxide. Organic peroxides are very reactive molecules containing oxygen-oxygen single bonds which are quite weak and which break easily to give free radicals.</p>
<p>You can short-cut the process by adding other organic peroxides directly to the ethene instead of using oxygen if you want to.</p>
<p>You don't need to worry about the exact formulae of the free radicals which start the reaction off – they vary depending on their source. For simplicity we give them a general formula: Ra•</div>

<h3>Chain Propagation</h3>

<div class="image-container">
<img src="ethenebonds.GIF">
</div>

<div class="text-block">
<p>In an ethene molecule, CH<sub>2</sub>=CH<sub>2</sub>, the two pairs of electrons which make up the double bond aren't the same. One pair is held securely on the line between the two carbon nuclei in a bond called a sigma bond. The other pair is more loosely held in an orbital above and below the plane of the molecule known as a &pi; bond.</p>
</div>

<div class="note">
<p>Note: It would be helpful – but not essential – if you read about the <a href="../../basicorg/bonding/ethene.html#top">structure of ethene</a> before you went on. If the diagram above is unfamiliar to you, then you certainly ought to read this background material.</p>
</div>

<div class="text-block">
<p>Imagine what happens if a free radical approaches the &pi; bond in ethene.</p>
</div>

<div class="image-container"><img src="ethenera.GIF"></div>

<div class="note">
<p>Note: Don't worry that we've gone back to a simpler diagram. As long as you realise that the pair of electrons shown between the two carbon atoms is in a &pi; bond – and therefore vulnerable – that's all that really matters for this mechanism.</p>
</div>

<div class="text-block">
<p>The sigma bond between the carbon atoms isn't affected by any of this.</p>
<p>The free radical, Ra•, uses one of the electrons in the &pi; bond to help to form a new bond between itself and the left hand carbon atom. The other electron returns to the right hand carbon.</div>

<div class="text-block">
<p>You can show this using "curly arrow" notation if you want to:</p>
</div>

<div class="image-container"><img src="polymfh.GIF"></div>

<div class="note">
<p>Note: If you aren't sure about about <a href="../../basicorg/conventions/curlies.html#top">curly arrow notation</a> you can follow this link.</p>
</div>

<div class="text-block">
<p>This is energetically worth doing because the new bond between the radical and the carbon is stronger than the &pi; bond which is broken. You would get more energy out when the new bond is made than was used to break the old one. The more energy that is given out, the more stable the system becomes.</p>
<p>What we've now got is a bigger free radical – lengthened by CH<sub>2</sub>CH<sub>2</sub>. That can react with another ethene molecule in the same way:</p>
</div>

<div class="block-formula">
\text{Ra}CH_2CH_2{\bullet} + CH_2{=}CH_2 \longrightarrow \text{Ra}CH_2CH_2CH_2CH_2{\bullet}
</div>

<div class="text-block">
<p>So now the radical is even bigger. That can react with another ethene – and so on and so on. The polymer chain gets longer and longer.</p>
</div>

<h3>Chain Termination</h3>

<div class="text-block">
<p>The chain doesn't, however, grow indefinitely. Sooner or later two free radicals will collide together.</p>
</div>

<div class="block-formula">
\text{Ra}(CH_2)_m{\bullet} + {\bullet}(CH_2)_n\text{Ra} \longrightarrow \text{Ra}(CH_2)_{m+n}\text{Ra}
</div>

<div class="text-block">
<p>That immediately stops the growth of two chains and produces one of the final molecules in the poly(ethene). It is important to realise that the poly(ethene) is going to be a mixture of molecules of different sizes, made in this sort of random way.</p>
</div>

<h3>Simplifying All This For Exam Purposes</h3>

<div class="text-block">
<p>The over-all process is known as free radical addition.</p>
</div>

<h4>Chain initiation</h4>

<div class="text-block">
<p>The chain is initiated by free radicals, Ra•, produced by reaction between some of the ethene and the oxygen initiator.</p>
</div>

<h4>Chain propagation</h4>

<div class="text-block">
<p>Each time a free radical hits an ethene molecule a new longer free radical is formed.</p>
</div>

<div class="block-formula">
\begin{aligned}
\text{Ra}{\bullet} + CH_2{=}CH_2 &\longrightarrow \text{Ra}CH_2CH_2{\bullet} \\
\\
\text{Ra}CH_2CH_2{\bullet} + CH_2{=}CH_2 &\longrightarrow \text{Ra}CH_2CH_2CH_2CH_2{\bullet} \\
\\
{}&\text{etc.}
\end{aligned}
</div>

<h4>Chain termination</h4>

<div class="text-block">
<p>Eventually two free radicals hit each other producing a final molecule. The process stops here because no new free radicals are formed.</p>
</div>

<div class="block-formula">
\text{Ra}(CH_2)_m{\bullet} + {\bullet}(CH_2)_n\text{Ra} \longrightarrow \text{Ra}(CH_2)_{m+n}\text{Ra}
</div>

<div class="text-block">
<p>Because chain termination is a random process, poly(ethene) will be made up of chains of all sorts of different lengths.</p>
</div>

<div class="link-list"> 
<p>Where would you like to go now?</p>
<a href="../frmenu.html#top">To menu of free radical reactions</a>
<a href="../../mechmenu.html#top">To menu of other types of mechanism</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body></html>