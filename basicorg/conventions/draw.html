<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>How to Draw Organic Molecules | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the various ways in which organic molecules can be represented on paper or screen">
<meta name="keywords" content="organic, structural formula, molecular formula, skeletal formula, displayed formula, formula">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>How to Draw Organic Molecules</h1>

<div class="text-block">
<p>This page explains the various ways that organic molecules can be represented on paper or on screen – including molecular formulae, and various forms of structural formulae.</p>
</div>

<h2>Molecular Formulae</h2>

<div class="note">
<p>Note: "formulae" is just a different spelling of "formulas". "Formulae" is more traditional, but "formulas" is just as valid.</p>
</div>

<div class="text-block">
<p>A molecular formula simply counts the numbers of each sort of atom present in the molecule, but tells you nothing about the way they are joined together.</p>
<p>For example, the molecular formula of butane is C<sub>4</sub>H<sub>10</sub>, and the molecular formula of ethanol is C<sub>2</sub>H<sub>6</sub>O.</p>
<p>Molecular formulae are very rarely used in organic chemistry, because they don't give any useful information about the bonding in the molecule. About the only place where you might come across them is in equations for the combustion of simple hydrocarbons, for example:</p>
</div>

<div class="block-formula">
C_5H_{12} + 8O_2 \longrightarrow 5CO_2 + 6H_2O
</div>

<div class="text-block">
<p>In cases like this, the bonding in the organic molecule isn't important.</p>
</div>

<h2>Structural Formulae</h2>

<div class="text-block">
<p>A structural formula shows how the various atoms are bonded. There are various ways of drawing this and you will need to be familiar with all of them.</p>
</div>

<h3>Displayed Formulae</h3>

<div class="text-block">
<p>A displayed formula shows all the bonds in the molecule as individual lines. You need to remember that each line represents a pair of shared electrons.</p>
<p>For example, this is a model of methane together with its displayed formula:</p>
</div>

<div class="image-container">
<img src="methane.GIF">
</div>

<div class="text-block">
<p>Notice that the way the methane is drawn bears no resemblance to the actual shape of the molecule. Methane isn't flat with 90° bond angles. This mismatch between what you draw and what the molecule actually looks like can lead to problems if you aren't careful.</p>
<p>For example, consider the simple molecule with the molecular formula CH<sub>2</sub>Cl<sub>2</sub>. You might think that there were two different ways of arranging these atoms if you drew a displayed formula.</p>
</div>

<div class="image-container">
<img src="ch2cl2displ.GIF">
</div>

<div class="text-block">
<p>The chlorines could be opposite each other or at right angles to each other. But these two structures are actually exactly the same. Look at how they appear as models.</p>
</div>

<div class="image-container">
<img src="ch2cl2model.GIF">
</div>

<div class="text-block">
<p>One structure is in reality a simple rotation of the other one.</p>
</div>

<div class="text-block">
<p>Consider a slightly more complicated molecule, C<sub>2</sub>H<sub>5</sub>Cl. The displayed formula could be written as either of these:</p>
</div>

<div class="image-container">
<img src="c2h5cldispl.GIF">
</div>

<div class="text-block">
<p>But, again these are exactly the same. Look at the models.</p>
</div>

<div class="image-container">
<img src="c2h5clmodel.GIF">
</div>

<h3>The Commonest Way to Draw Structural Formulae</h3>

<div class="text-block">
<p>For anything other than the most simple molecules, drawing a fully displayed formula is a bit of a bother – especially all the carbon-hydrogen bonds. You can simplify the formula by writing, for example, CH<sub>3</sub> or CH<sub>2</sub> instead of showing all these bonds.</p>
<p>So for example, ethanoic acid would be shown in a fully displayed form and a simplified form as:</p>
</div>

<div class="image-container">
<img src="ethanoicacid.GIF">
</div>

<div class="text-block">
<p>You could even condense it further to CH<sub>3</sub>COOH, and would probably do this if you had to write a simple chemical equation involving ethanoic acid. You do, however, lose something by condensing the acid group in this way, because you can't immediately see how the bonding works.</p>
<p>You still have to be careful in drawing structures in this way. Remember from above that these two structures both represent the same molecule:</p>
</div>

<div class="image-container">
<img src="c2h5cldispl.GIF">
</div>

<div class="text-block">
<p>The next three structures all represent butane.</p>
</div>

<div class="image-container">
<img src="butanedispl.GIF">
</div>

<div class="text-block">
<p>All of these are just versions of four carbon atoms joined up in a line. The only difference is that there has been some rotation about some of the carbon-carbon bonds. You can see this in a couple of models.</p>
</div>

<div class="image-container">
<img src="butanemodel.GIF">
</div>

<div class="text-block">
<p>Not one of the structural formulae accurately represents the shape of butane. The convention is that we draw it with all the carbon atoms in a straight line – as in the first of the structures above.</p>
<p>This is even more important when you start to have branched chains of carbon atoms. The following structures again all represent the same molecule – 2-methylbutane.</p>
</div>

<div class="image-container">
<img src="methylbutane.GIF">
</div>

<div class="text-block">
<p>The two structures on the left are fairly obviously the same – all we've done is flip the molecule over. The other one isn't so obvious until you look at the structure in detail. There are four carbons joined up in a row, with a CH<sub>3</sub> group attached to the next-to-end one. That's exactly the same as the other two structures. If you had a model, the only difference between these three diagrams is that you have rotated some of the bonds and turned the model around a bit.</p>
<p>To overcome this possible confusion, the convention is that you always look for the longest possible chain of carbon atoms, and then draw it horizontally. Anything else is simply hung off that chain.</p>
<p>It doesn't matter in the least whether you draw any side groups pointing up or down. All of the following represent exactly the same molecule.</p>
</div>

<div class="image-container">
<img src="complex1.GIF">
</div>

<div class="text-block">
<p>If you made a model of one of them, you could turn it into any other one simply by rotating one or more of the carbon-carbon bonds.</p>
</div>

<h3>How to Draw Structural Formulae in 3 Dimensions</h3>

<div class="text-block">
<p>There are occasions when it is important to be able to show the precise 3D arrangement in parts of some molecules. To do this, the bonds are shown using conventional symbols:</p>
</div>

<div class="image-container">
<img src="bondtypes.GIF">
</div>

<div class="text-block">
<p>For example, you might want to show the 3D arrangement of the groups around the carbon which has the -OH group in butan-2-ol.</p>
<p>Butan-2-ol has the structural formula:</p>
</div>

<div class="image-container">
<img src="butan2ol.GIF">
</div>

<div class="text-block">
<p>Using conventional bond notation, you could draw it as, for example:</p>
</div>

<div class="image-container">
<img src="butan2ol3d.GIF">
</div>

<div class="text-block">
<p>The only difference between these is a slight rotation of the bond between the centre two carbon atoms. This is shown in the two models below. Look carefully at them – particularly at what has happened to the lone hydrogen atom. In the left-hand model, it is tucked behind the carbon atom. In the right-hand model, it is in the same plane. The change is very slight.</p>
</div>

<div class="image-container">
<img src="butan2olmod.GIF">
</div>

<div class="text-block">
<p>It doesn't matter in the least which of the two arrangements you draw. You could easily invent other ones as well. Choose one of them and get into the habit of drawing 3-dimensional structures that way. My own habit (used elsewhere on this site) is to draw two bonds going back into the paper and one coming out – as in the left-hand diagram above.</p>
<p>Notice that no attempt was made to show the whole molecule in 3-dimensions in the structural formula diagrams. The CH<sub>2</sub>CH<sub>3</sub> group was left in a simple form. Keep diagrams simple – trying to show too much detail makes the whole thing amazingly difficult to understand!</p>
</div>

<h3>Skeletal Formulae</h3>

<div class="text-block">
<p>In a skeletal formula, all the hydrogen atoms are removed from carbon chains, leaving just a carbon skeleton with functional groups attached to it.</p>
<p>For example, we've just been talking about butan-2-ol. The normal structural formula and the skeletal formula look like this:</p>
</div>

<div class="image-container">
<img src="butan2olskel.GIF">
</div>

<div class="text-block">
<p>In a skeletal diagram of this sort</p>
</div>

<ul>
<li>there is a carbon atom at each junction between bonds in a chain and at the end of each bond (unless there is something else there already – like the -OH group in the example);</li>
<li>there are enough hydrogen atoms attached to each carbon to make the total number of bonds on that carbon up to 4.</li>
</ul>

<div class="text-block">
<p>Beware! Diagrams of this sort take practice to interpret correctly – and may well not be acceptable to your examiners (see below).</p>
<p>There are, however, some very common cases where they are frequently used. These cases involve rings of carbon atoms which are surprisingly awkward to draw tidily in a normal structural formula.</p>
<p>Cyclohexane, C<sub>6</sub>H<sub>12</sub>, is a ring of carbon atoms each with two hydrogens attached. This is what it looks like in both a structural formula and a skeletal formula.</p>
</div>

<div class="image-container">
<img src="cyclohexane.GIF">
</div>

<div class="text-block">
<p>And this is cyclohexene, which is similar but contains a double bond:</p>
</div>

<div class="image-container">
<img src="cyclohexene.GIF">
</div>

<div class="text-block">
<p>But the commonest of all is the benzene ring, C<sub>6</sub>H<sub>6</sub>, which has a special symbol of its own.</p>
</div>

<div class="image-container">
<img src="benzene.GIF">
</div>

<div class="note">
<p>Note: Explaining exactly what this structure means needs more space than is available here. It is explained in full in two pages on <a href="../bonding/benzene1.html#top">the structure of benzene</a> elsewhere in this site. It would probably be better not to follow this link unless you are actively interested in benzene chemistry at the moment – it will lead you off into quite deep water!</p>
</div>

<h2>Deciding Which Sort of Formula to Use</h2>

<div class="text-block">
<p>There's no easy, all-embracing answer to this problem. It depends more than anything else on experience – a feeling that a particular way of writing a formula is best for the situation you are dealing with.</p>
<p>Don't worry about this – as you do more and more organic chemistry, you will probably find it will come naturally. You'll get so used to writing formulae in reaction mechanisms, or for the structures for isomers, or in simple chemical equations, that you won't even think about it.</p>
<p>There are, however, a few guidelines that you should follow.</p>
</div>

<h3>What Does Your Syllabus Say?</h3>

<div class="text-block">
<p>Different examiners will have different preferences. Check first with your syllabus. If you've down-loaded a copy of your syllabus from your examiners' web site, it is easy to check what they say they want. Use the "find" function on your Adobe Acrobat Reader to search the organic section(s) of the syllabus for the word "formula".</p>
<p>You should also check recent exam papers and (particulary) mark schemes to find out what sort of formula the examiners really prefer in given situations. You could also look at any support material published by your examiners.</p>
</div>

<div class="note">
<p>Note: If you are working to a UK-based syllabus and haven't got a copy of that <a href="../../syllabuses.html#top">syllabus</a> and recent exam papers, follow this link to find out how to get them.</p>
</div>

<h3>What If You Still Aren't Sure?</h3>

<div class="text-block">
<p>Draw the most detailed formula that you can fit into the space available. If in doubt, draw a fully displayed formula. You would never lose marks for giving too much detail.</p>
<p>Apart from the most trivial cases (for example, burning hydrocarbons), never use a molecular formula. Always show the detail around the important part(s) of a molecule. For example, the important part of an ethene molecule is the carbon-carbon double bond – so write (at the very least) CH<sub>2</sub>=CH<sub>2</sub> and not C<sub>2</sub>H<sub>4</sub>.</p>
<p>Where a particular way of drawing a structure is important, this will always be pointed out where it arises elsewhere on this site.</p>
</div>

<div class="note">
<p>Note: This is all much easier to understand if you have actually got some models to play with. If your school or college hasn't given you the opportunity to play around with molecular models in the early stages of your organic chemistry course, you might consider getting hold of a cheap set. The models made by Molymod are both cheap and easy to use. An introductory organic set is more than adequate. Google <i>molymod</i> to find a supplier and more about them, or click on the picture or text link below to see a typical example from Amazon.</p>
<p>Share the cost with some friends, keep it in good condition and don't lose any bits, and resell it via eBay or Amazon at the end of your course.</p>
<p>Alternatively, get hold of some coloured Plasticene (or other children's modelling clay) and some used matches and make your own. It's cheaper, but more difficult to get the bond angles right.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-drawing.pdf" target="_blank">Questions on ways of drawing organic molecules</a>
<a href="../questions/a-drawing.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../convmenu.html#top">To the organic conventions menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>