<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>The Use of Curly Arrows | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how to show the movement of electron pairs or single electrons during organic reaction mechanisms">
<meta name="keywords" content="curly arrow, curly arrows, electron, electrons, organic, reaction mechanism, reaction mechanisms">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>The Use of Curly Arrows in Reaction Mechanisms</h1>

<div class="text-block">
<p>This page explains the use of curly arrows to show the movement both of electron pairs and of single electrons during organic reaction mechanisms.</p>
</div>

<h2>Using Curly Arrows to Show the Movement of electron pairs</h2>

<div class="text-block">
<p>Curly arrows (and that's exactly what they are called!) are used in mechanisms to show the various electron pairs moving around. You mustn't use them for any other purpose.</p>
</div>

<ul>
<li>The arrow tail is where the electron pair starts from. That's always fairly obvious, but you must show the electron pair either as a bond or, if it is a lone pair, as a pair of dots. Remember that a lone pair is a pair of electrons at the bonding level which isn't currently being used to join on to anything else.</li>
<li>The arrow head is where you want the electron pair to end up.</li>
</ul>

<div class="image-container">
<img src="ethenehbr1.GIF">
</div>

<div class="text-block">
<p>For example, in the reaction between ethene and hydrogen bromide, one of the two bonds between the two carbon atoms breaks. That bond is simply a pair of electrons.</p>
<p>Those electrons move to form a new bond with the hydrogen from the HBr. At the same time the pair of electrons in the hydrogen-bromine bond moves down on to the bromine atom.</p>
</div>

<div class="image-container">
<img src="ethenehbr1a.GIF">
</div>

<div class="text-block">
<p>There's no need to draw the pairs of electrons in the bonds as two dots. Drawing the bond as a line is enough, but you could put two dots in as well if you wanted to.</p>
<p>Notice that the arrow head points between the C and H because that's where the electron pair ends up. Notice also that the electron movement between the H and Br is shown as a curly arrow even though the electron pair moves straight down. You have to show electron pair movements as curly arrows – not as straight ones.</p>
<p>The second stage of this reaction nicely illustrates how you use a curly arrow if a lone pair of electrons is involved.</p>
</div>

<div class="image-container">
<img src="ethenehbr2.GIF">
</div>

<div class="text-block">
<p>The first stage leaves you with a positive charge on the right hand carbon atom and a negative bromide ion. You can think of the electrons shown on the bromide ion as being the ones which originally made up the hydrogen-bromine bond.</p>
</div>

<div class="note">
<p>Note: There are another three lone pairs around the outside of the bromide ion – making four in all. These aren't normally shown because they don't actually do anything new and interesting! However, it is essential that you show the lone pair you are interested in as a pair of dots. If you don't, you risk losing marks in an exam.</p>
</div>

<div class="text-block">
<p>The lone pair on the bromide ion moves to form a new bond between the bromine and the right hand carbon atom. That movement is again shown by a curly arrow. Notice again, that the curly arrow points between the carbon and the bromine because that's where the electron pair ends up.</p>
<p>That leaves you with the product of this reaction, bromoethane:</p>
</div>

<div class="image-container">
<img src="ethenehbr3.GIF">
</div>

<div class="note">
<p>Note: You can read a full description of this mechanism together with other similar <a href="../../mechanisms/eladdmenu.html#top">reactions of ethene and the other alkenes</a> by following this link.</p>
</div>

<h2>Using Curly Arrows to Show the Movement of Single Slectrons</h2>

<div class="text-block">
<p>The most common use of "curly arrows" is to show the movement of pairs of electrons. You can also use similar arrows to show the movement of single electrons – except that the heads of these arrows only have a single line rather than two lines.</p>
</div>

<div class="image-container">
<img src="curly.GIF">
</div>

<div class="text-block">
<p>shows the movement of an electron pair</p>
</div>

<div class="image-container">
<img src="fishhook.GIF">
</div>

<div class="text-block">
<p>shows the movement of a single electron</p>
<p>The first stage of the polymerisation of ethene, for example, could be shown as:</p>
</div>

<div class="image-container">
<img src="polymfh.GIF">
</div>

<div class="text-block">
<p>You should draw the dots showing the interesting electrons. The half arrows show where they go. This is very much a "belt-and-braces" job, and the arrows don't add much.</p>
<p>Whether you choose to use these half arrows to show the movement of a single electron should be governed by what your syllabus says. If your syllabus encourages the use of these arrows, then it makes sense to use them. If not – if the syllabus says that they "may" be used, or just ignores them altogether – then they are as well avoided.</p>
<p>There is some danger of confusing them with the arrows showing electron pair movements, which you will use all the time. If, by mistake, you use an ordinary full arrow to show the movement of a single electron you run the risk of losing marks.</p>
</div>

<div class="note">
<p>Note: If you are a working to one of the UK-based syllabuses for 16 – 18 year olds and haven't got a copy, find out <a href="../../syllabuses.html#top">how to get a syllabus</a> by following this link.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-curlyarrows.pdf" target="_blank">Questions on curly arrows</a>
<a href="../questions/a-curlyarrows.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../convmenu.html#top">To the organic conventions menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>