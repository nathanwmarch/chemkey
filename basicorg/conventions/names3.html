<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Understanding the Names of Organic Compounds Part 3 – Naming Aromatic Compounds | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the names of the simple aromatic compounds (containing benzene rings) required at A-level">
<meta name="keywords" content="organic, names, naming, nomenclature, iupac, chemistry, formula, compounds, aromatic, benzene, methylbenzene, phenylethene, chlorobenzene, bromobenzene, nitrobenzene, phenol, benzoic acid, phenylamine">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Understanding the Names of Organic Compounds Part 3<br> – Naming Aromatic Compounds</h1>

<div class="text-block">
<p>This page looks at the names of some simple aromatic compounds. An aromatic compound is one which contains a benzene ring. It assumes that you are reasonably confident about naming compounds containing chains of carbon atoms (aliphatic compounds).</p>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="names.html#top">naming aliphatic compounds</a>, follow this link before you go on.</p>
</div>

<div class="text-block">
<p>Naming aromatic compounds isn't quite so straightforward as naming chain compounds. Often, more than one name is acceptable and it's not uncommon to find the old names still in use as well.</p>
</div>

<h2>Background</h2>

<h3>The Benzene Ring</h3>

<div class="text-block">
<p>All aromatic compounds are based on benzene, C<sub>6</sub>H<sub>6</sub>, which has a ring of six carbon atoms and has the symbol:</p>
</div>

<div class="image-container">
<img src="benzene.GIF">
</div>

<div class="text-block">
<p>Each corner of the hexagon has a carbon atom with a hydrogen attached.</p>
</div>

<div class="note">
<p>Note: If you don't understand this structure, it is explained in full in two pages on <a href="../bonding/benzene1.html#top">the structure of benzene</a> elsewhere in this site.</p>
</div>

<h3>The Phenyl Group</h3>

<div class="text-block">
<p>Remember that you get a methyl group, CH<sub>3</sub>, by removing a hydrogen from methane, CH<sub>4</sub>.</p>
<p>You get a phenyl group, C<sub>6</sub>H<sub>5</sub>, by removing a hydrogen from a benzene ring, C<sub>6</sub>H<sub>6</sub>. Like a methyl or an ethyl group, a phenyl group is always attached to something else.</p>
</div>

<h2>Aromatic Compounds With Only One Group Attached to the Benzene Ring</h2>

<h3>Cases Where the Name is Based on Benzene</h3>

<h4>chlorobenzene</h4>

<div class="text-block">
<p>This is a simple example of a halogen attached to the benzene ring. The name is self-obvious.</p>
</div>

<div class="image-container">
<img src="chlorobenz.GIF">
</div>

<div class="text-block">
<p>The simplified formula for this is C<sub>6</sub>H<sub>5</sub>Cl. You could therefore (although you never do!) call it phenyl chloride. Whenever you draw a benzene ring with one other thing attached to it, you are in fact drawing a phenyl group. In order to attach something else, you have to remove one of the existing hydrogen atoms, and so automatically make a phenyl group.</p>
</div>

<h4>nitrobenzene</h4>

<div class="text-block">
<p>The nitro group, NO<sub>2</sub>, is attached to a benzene ring.</p>
</div>

<div class="image-container">
<img src="nitrobenz.GIF">
</div>

<div class="text-block">
<p>The simplified formula for this is C<sub>6</sub>H<sub>5</sub>NO<sub>2</sub>.</p>
</div>

<h4>methylbenzene</h4>

<div class="text-block">
<p>Another obvious name – the benzene ring has a methyl group attached. Other alkyl side-chains would be named similarly – for example, ethylbenzene. The traditional name for methylbenzene is toluene, and you may still meet that.</p>
</div>

<div class="image-container">
<img src="methylbenz.GIF">
</div>

<div class="text-block">
<p>The simplified formula for this is C<sub>6</sub>H<sub>5</sub>CH<sub>3</sub>.</p>
</div>

<h4>(chloromethyl)benzene</h4>

<div class="text-block">
<p>A variant on this which you may need to know about is where one of the hydrogens on the CH<sub>3</sub> group is replaced by a chlorine atom. Notice the brackets around the (chloromethyl) in the name. This is so that you are sure that the chlorine is part of the methyl group and not somewhere else on the ring.</p>
</div>

<div class="image-container">
<img src="clmethbenz.GIF">
</div>

<div class="text-block">
<p>If more than one of the hydrogens had been replaced by chlorine, the names would be (dichloromethyl)benzene or (trichloromethyl)benzene. Again, notice the importance of the brackets in showing that the chlorines are part of the side group and not directly attached to the ring.</p>
</div>

<h4>benzoic acid (benzenecarboxylic acid)</h4>

<div class="text-block">
<p>Benzoic acid is the older name, but is still in common use – it's a lot easier to say and write than the modern alternative! Whatever you call it, it has a carboxylic acid group, -COOH, attached to the benzene ring.</p>
</div>

<div class="image-container">
<img src="benzacid.GIF">
</div>

<h3>Cases Where the Name is Based on Phenyl</h3>

<div class="text-block">
<p>Remember that the phenyl group is a benzene ring minus a hydrogen atom – C<sub>6</sub>H<sub>5</sub>. If you draw a benzene ring with one group attached, you have drawn a phenyl group.</p>
</div>

<h4>phenylamine</h4>

<div class="text-block">
<p>Phenylamine is a primary amine and contains the -NH<sub>2</sub> group attached to a benzene ring.</p>
</div>

<div class="image-container">
<img src="aniline.GIF">
</div>

<div class="text-block">
<p>The old name for phenylamine is aniline, and you could also reasonably call it aminobenzene. Phenylamine is what it is most commonly for UK-based exam purposes.</p>
</div>

<div class="note">
<p>Note: In all cases where there is some possibility of alternative names, you need to know what your examiners are likely to call a particular compound. Refer to your <a href="../../syllabuses.html#top">syllabus and recent exam papers</a>. If you are working to a UK-based syllabus for 16 – 8 year olds, and haven't got these, follow this link to find out how to get hold of them.</p>
</div>

<h4>phenylethene</h4>

<div class="text-block">
<p>This is an ethene molecule with a phenyl group attached. Ethene is a two carbon chain with a carbon-carbon double bond. Phenylethene is therefore:</p>
</div>

<div class="image-container">
<img src="phenethene.GIF">
</div>

<div class="text-block">
<p>The traditional name for phenylethene is styrene – the monomer from which polystyrene is made.</p>
</div>

<h4>phenylethanone</h4>

<div class="text-block">
<p>This is a slightly awkward name – take it to pieces. It consists of a two carbon chain with no carbon-carbon double bond. The one ending shows that it is a ketone, and so has a C=O group somewhere in the middle. Attached to the carbon chain is a phenyl group. Putting that together gives:</p>
</div>

<div class="image-container">
<img src="phenketone.GIF">
</div>

<h4>phenyl ethanoate</h4>

<div class="text-block">
<p>This is an ester based on ethanoic acid. The hydrogen atom in the -COOH group has been replaced by a phenyl group.</p>
</div>

<div class="image-container">
<img src="phenester.GIF">
</div>

<div class="note">
<p>Note: If you aren't happy about <a href="names2.html#top">naming esters</a>, follow this link before you go on.</p>
</div>

<h4>phenol</h4>

<div class="text-block">
<p>Phenol has an -OH group attached to a benzene ring and so has a formula C<sub>6</sub>H<sub>5</sub>OH.</p>
</div>

<div class="image-container">
<img src="phenol.GIF">
</div>

<h2>Aromatic Compounds With More Than One Group Attached to the Benzene Ring</h2>

<h3>Numbering the Ring</h3>

<div class="text-block">
<p>Any group already attached to the ring is given the number 1 position. Where you draw it on the ring (at the top or in any other position) doesn't matter – that's just a question of rotating the molecule a bit. It's much easier, though, to get in the habit of drawing your main group at the top.</p>
<p>The other ring positions are then numbered from 2 to 6. You can number them either clockwise or anti-clockwise. As with chain compounds, you number the ring so that the name you end up with has the smallest possible numbers in it. Examples will make this clear.</p>
</div>

<h3>Some Simple Examples</h3>

<h4>Substituting chlorine atoms on the ring</h4>

<div class="text-block">
<p>Look at these compounds:</p>
</div>

<div class="image-container">
<img src="ringposits.GIF">
</div>

<div class="text-block">
<p>All of these are based on methylbenzene and so the methyl group is given the number 1 position on the ring.</p>
<p>Why is it 2-chloromethylbenzene rather than 6-chloromethylbenzene? The ring is numbered clockwise in this case because that produces a 2- in the name rather than a 6-. 2 is smaller than 6.</p>
</div>

<div class="note">
<p>Warning! You will find all sorts of variations on this depending on the age of the book you look it up in, and where it was published. What I have described above isn't in strict accordance with the most modern interpretation of the IUPAC recommendations for naming organic compounds.</p>
<p>The names should actually be 1-chloro-2-methylbenzene, 1-chloro-3-methylbenzene, and so on. The substituted groups are named in alphabetical order, and the "1" position is assigned to the first of these – rather than to the more logical methyl group.</p>
<p>This produces some silly inconsistencies. For example, if you had the exactly equivalent compounds containing nitro groups in place of the chlorines, the names would change completely, to 1-methyl-2-nitrobenzene, 1-methyl-3-nitrobenzene, etc. In this case, the normal practice of naming the hydrocarbon first, and then attaching other things to it has been completely wrecked.</p>
<p>Do you need to worry about this? NO! It is extremely unlikely that you would ever be asked to name these in an exam, and it is always easy to write a structure from one of these names – however illogical it may be! There is a simple rule for exam purposes. Unless you are specifically asked for the name of anything remotely complicated, don't give it. As long as you have got the structure right, that's all that matters.</p>
</div>

<h4>2-hydroxybenzoic acid</h4>

<div class="text-block">
<p>This might also be called 2-hydroxybenzenecarboxylic acid. There is a -COOH group attached to the ring and, because the name is based on benzoic acid, that group is assigned the number 1 position. Next door to it in the 2 position is a hydroxy group, -OH.</p>
</div>

<div class="image-container">
<img src="salycil.GIF">
</div>

<h4>benzene-1,4-dicarboxylic acid</h4>

<div class="text-block">
<p>The di shows that there are two carboxylic acid groups, -COOH, one of them in the 1 position and the other opposite it in the 4 position.</p>
</div>

<div class="image-container">
<img src="benzdioic.GIF">
</div>

<h4>2,4,6-trichlorophenol</h4>

<div class="text-block">
<p>This is based on phenol – with an -OH group attached in the number 1 position on the ring. There are 3 chlorine atoms substituted onto the ring in the 2, 4 and 6 positions.</p>
</div>

<div class="image-container">
<img src="tcp.GIF">
</div>

<h4>methyl 3-nitrobenzoate</h4>

<div class="text-block">
<p>This is a name you might come across as a part of a practical exercise in nitrating benzene rings. It's included partly for that reason, and partly because it is a relatively complicated name to finish with!</p>
<p>The structure of the name shows that it is an ester. You can tell that from the oate ending, and the methyl group floating separately from the rest of the name at the beginning.</p>
<p>The ester is based on the acid, 3-nitrobenzoic acid – so start with that.</p>
<p>There will be a benzene ring with a -COOH group in the number 1 position and a nitro group, NO<sub>2</sub>, in the 3 position. The -COOH group is modified to make an ester by replacing the hydrogen of the -COOH group by a methyl group.</p>
<p>Methyl 3-nitrobenzoate is therefore:</p>
</div>

<div class="image-container">
<img src="nitroester.GIF">
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-names3.pdf" target="_blank">Questions on naming aromatic molecules</a>
<a href="../questions/a-names3.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../convmenu.html#top">To the organic conventions menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>