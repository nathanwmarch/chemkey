<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Understanding the Names of Organic Compounds | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how to write the formula for an organic compound from its name, and vice versa.">
<meta name="keywords" content="organic, names, naming, nomenclature, iupac, chemistry, formula, compounds, alkanes, cycloalkanes, alkenes, halogenoalkanes, haloalkanes, alkyl halides, alcohols, aldehydes, ketones">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Understanding the Names of Organic Compounds</h1>

<div class="text-block">
<p>This page explains how to write the formula for an organic compound given its name – and vice versa. It covers alkanes, cycloalkanes, alkenes, simple compounds containing halogens, alcohols, aldehydes and ketones. At the bottom of the page, you will find links to other types of compound.</p>
</div>

<h2>Background</h2>

<h3>How This Page is Going to Tackle the Problem</h3>

<div class="text-block">
<p>There are two skills you have to develop in this area:</p>
</div>

<ul>
<li>You need to be able to translate the name of an organic compound into its structural formula.</li>
<li>You need to be able to name a compound from its given formula.</li>
</ul>

<div class="text-block">
<p>The first of these is more important (and also easier!) than the second. In an exam, if you can't write a formula for a given compound, you aren't going to know what the examiner is talking about and could lose lots of marks. However, you might only be asked to write a name for a given formula once in a whole exam – in which case you only risk 1 mark.</p>
<p>So, we're going to look mainly at how you decode names and turn them into formulae. In the process you will also pick up tips about how to produce names yourself.</p>
<p>In the early stages of an organic chemistry course people frequently get confused and daunted by the names because they try to do too much at once. Don't try to read all these pages in one go. Just go as far as the compounds you are interested in at the moment and ignore the rest. Come back to them as they arise during the natural flow of your course.</p>
</div>

<h3>Cracking the Code</h3>

<div class="text-block">
<p>A modern organic name is simply a code. Each part of the name gives you some useful information about the compound.</p>
<p>For example, to understand the name 2-methylpropan-1-ol you need to take the name to pieces.</p>
<p>The <i>prop</i> in the middle tells you how many carbon atoms there are in the longest chain (in this case, 3). The <i>an</i> which follows the "prop" tells you that there aren't any carbon-carbon double bonds.</p>
<p>The other two parts of the name tell you about interesting things which are happening on the first and second carbon atom in the chain. Any name you are likely to come across can be broken up in this same way.</p>
</div>

<h3>Counting the Carbon Atoms</h3>

<div class="text-block">
<p>You will need to remember the codes for the number of carbon atoms in a chain up to 6 carbons. There is no easy way around this – you have got to learn them. If you don't do this properly, you won't be able to name anything!</p>
</div>

<table class="list-table">
<tbody>
<tr><td>code</td><td>no of carbons</td></tr>
<tr><td>meth</td><td>1</td><td>C</td></tr>
<tr><td>eth</td><td>2</td><td>C-C</td></tr>
<tr><td>prop</td><td>3</td><td>C-C-C</td></tr>
<tr><td>but</td><td>4</td><td>C-C-C-C</td></tr>
<tr><td>pent</td><td>5</td><td>C-C-C-C-C</td></tr>
<tr><td>hex</td><td>6</td><td>C-C-C-C-C-C</td></tr>
</tbody>
</table>

<h3>Types of Carbon-carbon Bonds</h3>

<div class="text-block">
<p>Whether or not the compound contains a carbon-carbon double bond is shown by the two letters immediately after the code for the chain length.</p>
</div>

<table class="list-table">
<tbody>
<tr><td>code</td><td>means</td></tr>
<tr><td>an</td><td>only carbon-carbon single bonds</td></tr>
<tr><td>en</td><td>contains a carbon-carbon double bond</td></tr>
</tbody>
</table>

<div class="text-block">
<p>For example, butane means four carbons in a chain with no double bond.</p>
<p>Propene means three carbons in a chain with a double bond between two of the carbons.</p>
</div>

<h3>Alkyl Groups</h3>

<div class="text-block">
<p>Compounds like methane, CH<sub>4</sub>, and ethane, CH<sub>3</sub>CH<sub>3</sub>, are members of a family of compounds called alkanes. If you remove a hydrogen atom from one of these you get an alkyl group.</p>
<p>For example:</p>
</div>

<ul>
<li>A methyl group is CH<sub>3</sub></li>
<li>An ethyl group is CH<sub>3</sub>CH<sub>2</sub></li>
</ul>

<div class="text-block">
<p>These groups must, of course, always be attached to something else.</p>
</div>

<h2>Types of Compounds</h2>

<h3>The Alkanes</h3>

<div class="text-block">
<p>Example 1: Write the structural formula for...</p>
</div>

<div class="block-formula">
\Large \text{2-methylpentane}
</div>

<div class="text-block">
<p>Start decoding the name, starting with the part that indicates the number of carbon atoms in the longest chain</p>
</div>

<div class="block-formula" label="longest carbon chain has 5 carbon atoms">
\Large \text{2-methyl}\underbrace{\color{#467abf}{\text{pent}}}_{}ane
</div>

<div class="text-block">
<p>Are there any carbon-carbon double bonds? No...</p>
</div>

<div class="block-formula" label="tells you there are no carbon-carbon double bonds">
\Large \text{2-methylpent}\underbrace{\color{#467abf}{\text{an}}}_{}\text{e}
</div>

<div class="text-block">
<p>Now draw this carbon skeleton:</p>
</div>

<div class="block-formula">C{-}C{-}C{-}C{-}C</div>

<div class="block-formula" label="tells you there is a methyl group on carbon number 2">
\Large \underbrace{\color{#467abf}{\text{2-methyl}}}_{}\text{pentane}
</div>

<div class="text-block">
<p>Each atom in the longest carbon chain is given a number, starting from 1.</p>
<p>Put a methyl group on the number 2 carbon atom:</p>
</div>

<div class="image-container">
<img src="alkane1b.GIF">
</div>

<div class="text-block">
<p>Does it matter which end you start counting from?</p>
<p>No. Let's see why.</p>
<p>If you counted from the other end, you would draw the structure below. If you flip it over, it is exactly the same as the first one.</p>
</div>

<div class="image-container">
<img src="alkane1bb.GIF">
</div>

<div class="note">
<p>When drawing out chemical structures, it does not matter whether the diagram is rotated, flipped, enlarged or shrunk. It doesn't matter if the bonds are long, short, straight or angled...</p>
<p>What matters is how the atoms are connected. As you go onto higher levels, you will see ways of indicating the 3D shape of molecules in your diagrams, but we won't worry about that here.</p>
</div>

<div class="text-block">
<p>Finally, all you have to do is to put in the correct number of hydrogen atoms on each carbon so that each carbon is forming four bonds.</p>
</div>

<div class="image-container">
<img src="alkane1c.GIF">
</div>

<h4>If you had to name this yourself:</h4>

<ol>
<li>Find the longest chain of carbons atoms and count those atoms. The longest chain may not be straight – you may need to try a few options to find the longest one.<br>
Here, the longest chain has five carbon atoms...<br>
<div class="block-formula">
\Large \color{#467abf}{\text{pent}}
</div></li>

<li>Are there any carbon-carbon double bonds?<br>No – therefore.<br>
<div class="block-formula">
\Large \text{pent\color{#467abf}{an}e}</div><br>The <i>e</i> is added only when there is nothing else to finish the word off with.</li>

<li>There's a methyl group on the number 2 carbon, therefore...<br>
<div class="block-formula">
\Large \text{\color{#467abf}{2-methyl}pentane}
</div><br>The number 2 carbon could have easily been the number 4 carbon. Why must it be 2?<br>The convention is that you start numbering from the end which produces the lowest numbers in the name overall, hence <i>2-</i> rather than <i>4-methyl</i>.</li>
</ol>

<div class="text-block">
<p>Example 2: Write the structural formula for 2,3-dimethylbutane.</p>
<p>Start with the carbon backbone. There are 4 carbons in the longest chain (but) with no carbon-carbon double bonds (an).</p>
</div>

<div class="image-container">
<img src="alkane2a.GIF">
</div>

<div class="text-block">
<p>This time there are two methyl groups (di) on the number 2 and number 3 carbon atoms.</p>
</div>

<div class="image-container">
<img src="alkane3b.GIF">
</div>

<div class="text-block">
<p>Completing the formula by filling in the hydrogen atoms gives:</p>
</div>

<div class="image-container">
<img src="alkane3c.GIF">
</div>

<div class="note">
<p>Note: Does it matter whether you draw the two methyl groups one up and one down, or both up, or both down? Not in the least!
If you aren't sure about
<a href="draw.html#top">drawing organic molecules</a>, follow this link before you go on.</p>
</div>

<div class="text-block">
<p>Example 3: Write the structural formula for 2,2-dimethylbutane.</p>
<p>This is exactly like the last example, except that both methyl groups are on the same carbon atom. Notice that the name shows this by using 2,2- as well as di. The structure is worked out as before:</p>
</div>

<div class="image-container">
<img src="alkane2a.GIF">
</div>

<div class="image-container">
<img src="alkane2b.GIF">
</div>

<div class="image-container">
<img src="alkane2c.GIF">
</div>

<div class="text-block">
<p>Example 4: Write the structural formula for 3-ethyl-2-methylhexane.</p>
<p>hexan shows a 6 carbon chain with no carbon-carbon double bonds.</p>
</div>

<div class="image-container">
<img src="alkane4a.GIF">
</div>

<div class="text-block">
<p>This time there are two different alkyl groups attached – an ethyl group on the number 3 carbon atom and a methyl group on number 2.</p>
</div>

<div class="image-container">
<img src="alkane4b.GIF">
</div>

<div class="text-block">
<p>Filling in the hydrogen atoms gives:</p>
</div>

<div class="image-container">
<img src="alkane4c.GIF">
</div>

<div class="note">
<p>Note: Once again it doesn't matter whether the ethyl and methyl groups point up or down. You might also have chosen to start numbering from the right-hand end of the chain. These would all be perfectly valid structures. All you would have done is to rotate the whole molecule in space, or rotate it around particular bonds. If you aren't sure about this, then you must read about <a href="draw.html#top">drawing organic molecules</a> before you go on.</p>
</div>

<h4>If you had to name this yourself:</h4>

<div class="text-block">
<p>How do you know what order to write the different alkyl groups at the beginning of the name? The convention is that you write them in alphabetical order – hence ethyl comes before methyl which in turn comes before propyl.</p>
</div>

<h3>The Cycloalkanes</h3>

<div class="text-block">
<p>In a cycloalkane the carbon atoms are joined up in a ring – hence <i>cyclo</i>.</p>
<p>Example: Write the structural formula for cyclohexane.</p>
<p><i>hexan</i> means the longest chain of carbon atoms in the molecule is 6 atoms long, with no carbon-carbon double bonds anywhere. <i>cyclo</i> shows that they are in a ring. Drawing the ring and putting in the correct number of hydrogens to satisfy the bonding requirements of the carbons gives:</p>
</div>

<div class="image-container">
<img src="cyclohexane2.GIF">
</div>

<h3>The alkenes</h3>

<div class="text-block">
<p>Example 1: Write the structural formula for propene.</p>
<p><i>prop</i> means that there are 3 carbon atoms in the longest chain. <i>en</i> tells you that there is a carbon-carbon double bond. That means that the carbon skeleton looks like this:</p>
</div>

<div class="image-container">
<img src="alkene1a.GIF">
</div>

<div class="text-block">
<p>Putting in the hydrogens gives you:</p>
</div>

<div class="image-container">
<img src="alkene1b.GIF">
</div>

<div class="text-block">
<p>Example 2: Write the structural formula for but-1-ene.</p>
<p><i>but</i> means there are 4 carbon atoms in the longest chain and <i>en</i> tells you that there is a carbon-carbon double bond. The number in the name tells you where the double bond starts.</p>
<p>No number was necessary in the propene example above because the double bond has to start on one of the end carbon atoms. In the case of butene, though, the double bond could either be at the end of the chain or in the middle – and so the name has to code for the its position.</p>
<p>The carbon skeleton is:</p>
</div>

<div class="image-container">
<img src="alkene2a.GIF">
</div>

<div class="text-block">
<p>And the full structure is:</p>
</div>

<div class="image-container">
<img src="alkene2c.GIF">
</div>

<div class="text-block">
<p>Incidentally, you might equally well have decided that the right-hand carbon was the number 1 carbon, and drawn the structure as:</p>
</div>

<div class="image-container">
<img src="alkene2b.GIF">
</div>

<div class="text-block">
<p>Example 3: Write the structural formula for 3-methylhex-2-ene.</p>
<p>The longest chain has got 6 carbon atoms (hex) with a double bond starting on the second one (-2-en).</p>
<p>But this time there is a methyl group attached to the chain on the number 3 carbon atom, giving you the underlying structure:</p>
</div>

<div class="image-container">
<img src="alkene3a.GIF">
</div>

<div class="text-block">
<p>Adding the hydrogens gives the final structure:</p>
</div>

<div class="image-container">
<img src="alkene3b.GIF">
</div>

<div class="text-block">
<p>Be very careful to count the bonds around each carbon atom when you put the hydrogens in. It would be very easy this time to make the mistake of writing an H after the third carbon – but that would give that carbon a total of 5 bonds.</p>
</div>

<h3>Compounds Containing Halogens</h3>

<div class="text-block">
<p>Example 1: Write the structural formula for 1,1,1-trichloroethane.</p>
<p>This is a two-carbon chain (eth) with no double bonds (an). There are three chlorine atoms all on the first carbon atom.</p>
</div>

<div class="image-container">
<img src="halo1.GIF">
</div>

<div class="text-block">
<p>Example 2: Write the structural formula for 2-bromo-2-methylpropane.</p>
<p>First sort out the carbon skeleton. It's a three carbon chain with no double bonds and a methyl group on the second carbon atom.</p>
</div>

<div class="image-container">
<img src="halo2a.GIF">
</div>

<div class="text-block">
<p>Draw the bromine atom which is also on the second carbon.</p>
</div>

<div class="image-container">
<img src="halo2b.GIF">
</div>

<div class="text-block">
<p>And finally put the hydrogen atoms in.</p>
</div>

<div class="image-container">
<img src="halo2c.GIF">
</div>

<h4>If you had to name this yourself:</h4>

<div class="text-block">
<p>Notice that the whole of the hydrocarbon part of the name is written together – as methylpropane – before you start adding anything else on to the name.</p>
<p>Example 2: Write the structural formula for 1-iodo-3-methylpent-2-ene.</p>
<p>This time the longest chain has 5 carbons (pent), but has a double bond starting on the number 2 carbon. There is also a methyl group on the number 3 carbon.</p>
</div>

<div class="image-container">
<img src="halo3a.GIF">
</div>

<div class="text-block">
<p>Now draw the iodine on the number 1 carbon.</p>
</div>

<div class="image-container">
<img src="halo3b.GIF">
</div>

<div class="text-block">
<p>Giving a final structure:</p>
</div>

<div class="image-container">
<img src="halo3c.GIF">
</div>

<div class="note">
<p>Note: You could equally well draw this molecule the other way round, but normally where you have, say, 1-bromo-something, you tend to write the bromine (or other halogen) on the right-hand end of the structure.</p>
</div>

<h3>Alcohols</h3>

<div class="text-block">
<p>All alcohols contain an -OH group. This is shown in a name by the ending ol.</p>
<p>Example 1: Write the structural formula for methanol.</p>
<p>This is a one carbon chain with no carbon-carbon double bond (obviously!). The ol ending shows it's an alcohol and so contains an -OH group.</p>
</div>

<div class="image-container">
<img src="alcohol1.GIF">
</div>

<div class="note">
<p>Note: <i>hydroxy</i> is sometimes used rather than <i>ol</i> to indicate an -OH group is present. For instance, when a molecule contains another functional group like a carboxylic acid, the <i>oic acid</i> suffix is used, leaving no space for <i>ol</i>. 2-hydroxypropanoic acid is one such example.</p>
</div>

<div class="text-block">
<p>Example 2: Write the structural formula for 2-methylpropan-1-ol.</p>
<p>The carbon skeleton is a 3 carbon chain with no carbon-carbon double bonds, but a methyl group on the number 2 carbon.</p>
</div>

<div class="image-container">
<img src="halo2a.GIF">
</div>

<div class="text-block">
<p>The -OH group is attached to the number 1 carbon.</p>
</div>

<div class="image-container">
<img src="alcohol2a.GIF">
</div>

<div class="text-block">
<p>The structure is therefore:</p>
</div>

<div class="image-container">
<img src="alcohol2b.GIF">
</div>

<div class="text-block">
<p>Example 3: Write the structural formula for ethane-1,2-diol.</p>
<p>This is a two carbon chain with no double bond. The diol shows 2 -OH groups, one on each carbon atom.</p>
</div>

<div class="image-container">
<img src="alcohol3.GIF">
</div>

<div class="note">
<p>Note: There's no particular significance in the fact that this formula has the carbon chain drawn vertically. If you draw it horizontally, unless you stretch the carbon-carbon bond a lot, the -OH groups look very squashed together. Drawing it vertically makes it look tidier!</p>
</div>

<h3>Aldehydes</h3>

<div class="text-block">
<p>All aldehydes contain the group:</p>
</div>

<div class="image-container">
<img src="aldehyde.GIF">
</div>

<div class="text-block">
<p>If you are going to write this in a condensed form, you write it as -CHO – never as -COH, because that looks like an alcohol.</p>
<p>The names of aldehydes end in al.</p>
<p>Example 1: Write the structural formula for propanal.</p>
<p>This is a 3 carbon chain with no carbon-carbon double bonds. The al ending shows the presence of the -CHO group. The carbon in that group counts as one of the chain.</p>
</div>

<div class="image-container">
<img src="aldehyde1.GIF">
</div>

<div class="text-block">
<p>Example 2: Write the structural formula for 2-methylpentanal.</p>
<p>This time there are 5 carbons in the longest chain, including the one in the -CHO group. There aren't any carbon-carbon double bonds. A methyl group is attached to the number 2 carbon. Notice that in aldehydes, the carbon in the -CHO group is always counted as the number 1 carbon.</p>
</div>

<div class="image-container">
<img src="aldehyde2.GIF">
</div>

<h3>Ketones</h3>

<div class="text-block">
<p>Ketones contain a carbon-oxygen double bond just like aldehydes, but this time it's in the middle of a carbon chain. There isn't a hydrogen atom attached to the group as there is in aldehydes.</p>
<p>Ketones are written with the ending <i>one</i> (pronounced like <i>own</i>.</p>
<p>Example 1: Write the structural formula for propanone.</p>
<p>This is a 3 carbon chain with no carbon-carbon double bond. The carbon-oxygen double bond has to be in the middle of the chain and so must be on the number 2 carbon.</p>
</div>

<div class="image-container">
<img src="ketone1.GIF">
</div>

<div class="text-block">
<p>Ketones are often written in this way to emphasise the carbon-oxygen double bond.</p>
<p>Example 2: Write the structural formula for pentan-3-one.</p>
<p>This time the position of the carbon-oxygen double bond has to be stated because there is more than one possibility. It's on the third carbon of a 5 carbon chain with no carbon-carbon double bonds. If it was on the second carbon, it would be pentan-2-one.</p>
</div>

<div class="image-container">
<img src="ketone2a.GIF">
</div>

<div class="text-block">
<p>This could equally well be written:</p>
</div>

<div class="image-container">
<img src="ketone2b.GIF">
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-names1.pdf" target="_blank">Questions on naming organic molecules, page 1</a>
<a href="../questions/a-names1.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="names2.html#top">Continue naming more chain compounds</a>
<a href="names3.html#top">Naming aromatic compounds</a>
<a href="../convmenu.html#top">To the organic conventions menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>