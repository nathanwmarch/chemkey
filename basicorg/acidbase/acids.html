<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Explaining the Acidity of Organic Acids | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains why organic acids are acidic and what affects their relative strengths.">
<meta name="keywords" content="organic, acids, acidity, strength, carboxylic acids, phenol, alcohols, pka">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Explaining the Acidity of Organic Acids</h1>

<div class="text-block">
<p>This page explains the acidity of simple organic acids and looks at the factors which affect their relative strengths.</p>
</div>

<h2>Why are Organic Acids Acidic?</h2>

<h3>Organic Acids as Weak Acids</h3>

<div class="text-block">
<p>For the purposes of this topic, we are going to take the definition of an acid as "a substance which donates hydrogen ions (protons) to other things". We are going to get a measure of this by looking at how easily the acids release hydrogen ions to water molecules when they are in solution in water.</p>
<p>An acid in solution sets up this equilibrium:</p>
</div>

<div class="block-formula">
\text{AH} + \text{H}_2\text{O} \xrightleftharpoons{} \text{A}^- + \text{H}_3\text{O}^+
</div>

<div class="note">
<p>Note: We are writing the acid as AH rather than HA, because, in all the cases we shall be looking at, the hydrogen we are interested in is at the right-hand end of a molecule.</p>
</div>

<div class="text-block">
<p>A hydroxonium ion is formed together with the anion (negative ion) from the acid.</p>
<p>This equilibrium is sometimes simplified by leaving out the water to emphasise the ionisation of the acid.</p>
</div>

<div class="block-formula">
\text{AH}_{(aq)} \xrightleftharpoons{} \text{A}^-_{(aq)} + \text{H}^+_{(aq)}
</div>

<div class="text-block">
<p>If you write it like this, you must include the state symbols – "(aq)". Writing H<sup>+</sup><sub>(aq)</sub> implies that the hydrogen ion is attached to a water molecule as H<sub>3</sub>O<sup>+</sup>. Hydrogen ions are always attached to something during chemical reactions.</p>
<p>The organic acids are weak in the sense that this ionisation is very incomplete. At any one time, most of the acid will be present in the solution as un-ionised molecules. For example, in the case of dilute ethanoic acid, the solution contains about 99% of ethanoic acid molecules – at any instant, only about 1% have actually ionised. The position of equilibrium therefore lies well to the left.</p>
</div>

<h3>Comparing the Strengths of Weak Acids</h3>

<div class="text-block">
<p>The strengths of weak acids are measured on the pK<sub>a</sub> scale. The smaller the number on this scale, the stronger the acid is.</p>
<p>Three of the compounds we shall be looking at, together with their pK<sub>a</sub> values are:</p>
</div>

<table class="data-table">
<thead>
<tr>
<th></th>
<th>
pK
<sub>a</sub>
</th>
</tr>

</thead>
<tbody>
<tr>
<td>

<div class="chemical-structure" name="ethanoic acid" type="2d"></div>

</td>
<td>
4.76
</td>
</tr>
<tr>
<td>

<div class="chemical-structure" name="phenol" type="2d"></div>

</td>
<td>
10.00
</td>
</tr>
<tr>
<td>

<div class="chemical-structure" name="ethanol" type="2d"></div>

</td>
<td>
~16
</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>Remember – the smaller the number the stronger the acid. Comparing the other two to ethanoic acid, you will see that phenol is very much weaker with a pK<sub>a</sub> of 10.00, and ethanol is so weak with a pK<sub>a</sub> of about 16 that it hardly counts as acidic at all!</p>
</div>

<h3>Why are These Acids Acidic?</h3>

<div class="text-block">
<p>In each case, the same bond gets broken – the bond between the hydrogen and oxygen in an -OH group. Writing the rest of the molecule as "X ":</p>
</div>

<div class="image-container">
<img src="acideq3.GIF ">
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="../..//atoms/bonding/dative.html#top ">coordinate covalent (dative covalent) bonding</a>, you might like to follow this link. It isn't, however, particularly important to the rest of the current page.</p>
</div>

<div class="text-block">
<p>So if the same bond is being broken in each case, why do these three compounds have such widely different acid strengths?</p>
</div>

<h2>Differences in Acid Strengths Between Carboxylic acids, Phenols and Alcohols</h2>

<h3>The Factors to Consider</h3>

<div class="text-block">
<p>Two of the factors which influence the ionisation of an acid are:</p>
</div>

<ul>
<li>the strength of the bond being broken.</li>
<li>the stability of the ions being formed.</li>
</ul>

<div class="text-block">
<p>In these cases, you seem to be breaking the same oxygen-hydrogen bond each time, and so you might expect the strengths to be similar.</p>
</div>

<div class="note ">
<p>Note: You've got to be a bit careful about this. The bonds won't be identically strong, because what's around them in the molecule isn't the same in each case.</p>
</div>

<div class="text-block">
<p>The most important factor in determining the relative acid strengths of these molecules is the nature of the ions formed. You always get a hydroxonium ion – so that's constant – but the nature of the anion (the negative ion) varies markedly from case to case.</p>
</div>

<h3>Ethanoic acid</h3>

<div class="text-block">
<p>Ethanoic acid has the structure:</p>
</div>

<div class="chemical-structure" name="ethanoic acid" type="2d"></div>

<div class="text-block">
<p>The acidic hydrogen is the one attached to the oxygen. When ethanoic acid ionises it forms the ethanoate ion, CH<sub>3</sub>COO<sup>-</sup>.</p>
<p>You might reasonably suppose that the structure of the ethanoate ion was as below, but measurements of bond lengths show that the two carbon-oxygen bonds are identical and somewhere in length between a single and a double bond.</p>
</div>

<div class="image-container ">
<img src="ethanoate1.GIF ">
</div>

<div class="text-block">
<p>To understand why this is, you have to look in some detail at the bonding in the ethanoate ion.</p>
</div>

<div class="note ">
<p>Warning! If you don't already understand about the <a href="../bonding/carbonyl.html#top ">bonding in the carbon-oxygen double bond</a>, you would be well advised to skip this next bit – all the way down to the simplified structure of the ethanoate ion towards the end of it. It goes beyond anything that you are likely to want for UK A-level purposes. If you do choose to follow this link, it will probably take you to several other pages before you are ready to come back here again.</p>
</div>

<div class="text-block">
<p>Like any other double bond, a carbon-oxygen double bond is made up of two different parts. One electron pair is found on the line between the two nuclei – this is known as a sigma bond. The other electron pair is found above and below the plane of the molecule in a &pi; bond.</p>
<p>&pi; bonds are made by sideways overlap between p orbitals on the carbon and the oxygen.</p>
<p>In an ethanoate ion, one of the lone pairs on the negative oxygen ends up almost parallel to these p orbitals, and overlaps with them.</p>
</div>

<div class="image-container ">
<img src="ethanoate2.GIF ">
</div>

<div class="text-block">
<p>This leads to a delocalised &pi;system over the whole of the -COO<sup>-</sup> group, rather like that in benzene.</p>
</div>

<div class="image-container ">
<img src="ethanoate3.GIF ">
</div>

<div class="text-block">
<p>All the oxygen lone pairs have been left out of this diagram to avoid confusion.</p>
<p>Because the oxygens are more electronegative than the carbon, the delocalised system is heavily distorted so that the electrons spend much more time in the region of the oxygen atoms.</p>
<p>So where is the negative charge in all this? It has been spread around over the whole of the -COO<sup>-</sup> group, but with the greatest chance of finding it in the region of the two oxygen atoms.</p>
<p>Ethanoate ions can be drawn simply as:</p>
</div>

<div class="image-container ">
<img src="ethanoate4.GIF ">
</div>

<div class="text-block">
<p>The dotted line represents the delocalisation. The negative charge is written centrally on that end of the molecule to show that it isn't localised on one of the oxygen atoms.</p>
<p>The more you can spread charge around, the more stable an ion becomes. In this case, if you delocalise the negative charge over several atoms, it is going to be much less attractive to hydrogen ions – and so you are less likely to re-form the ethanoic acid.</p>
</div>

<h3>Phenol</h3>

<div class="text-block">
<p>Phenols have an -OH group attached directly to a benzene ring. Phenol itself is the simplest of these with nothing else attached to the ring apart from the -OH group.</p>
</div>

<div class="chemical-structure" name="phenol" type="2d"></div>

<div class="text-block">
<p>When the hydrogen-oxygen bond in phenol breaks, you get a phenoxide ion, C<sub>6</sub>H<sub>5</sub>O<sup>-</sup>.</p>
</div>

<div class="note ">
<p>Warning! You need to understand about the <a href="../bonding/benzene2.html#top ">bonding in benzene</a> in order to make sense of this next bit.</p>
<p>If your syllabus says that you need to know about the acidity of phenol, then you will have to understand the next few paragraphs – which in turn means that you will have to understand about benzene. If it doesn't mention phenol, skip it!</p>
<p>If you follow this link, you may have to explore several other pages before you are ready to come back here again.</p>
</div>

<div class="text-block">
<p>Delocalisation also occurs in this ion. This time, one of the lone pairs on the oxygen atom overlaps with the delocalised electrons on the benzene ring.</p>
</div>

<div class="image-container ">
<img src="phenate1.GIF ">
</div>

<div class="text-block">
<p>This overlap leads to a delocalisation which extends from the ring out over the oxygen atom. As a result, the negative charge is no longer entirely localised on the oxygen, but is spread out around the whole ion.</p>
</div>

<div class="image-container ">
<img src="phenate2.GIF ">
</div>

<h4>Why then is phenol a much weaker acid than ethanoic acid?</h4>

<div class="text-block">
<p>Think about the ethanoate ion again. If there wasn't any delocalisation, the charge would all be on one of the oxygen atoms, like this:</p>
</div>

<div class="image-container ">
<img src="ethanoate1a.GIF ">
</div>

<div class="text-block">
<p>But the delocalisation spreads this charge over the whole of the COO group. Because oxygen is more electronegative than carbon, you can think of most of the charge being shared between the two oxygens (shown by the heavy red shading in this diagram).</p>
</div>

<div class="image-container ">
<img src="ethanoate3.GIF ">
</div>

<div class="text-block">
<p>If there wasn't any delocalisation, one of the oxygens would have a full charge which would be very attractive towards hydrogen ions. With delocalisation, that charge is spread over two oxygen atoms, and neither will be as attractive to a hydrogen ion as if one of the oxygens carried the whole charge.</p>
<p>That means that the ethanoate ion won't take up a hydrogen ion as easily as it would if there wasn't any delocalisation. Because some of it stays ionised, the formation of the hydrogen ions means that it is acidic.</p>
<p>In the phenoxide ion, the single oxygen atom is still the most electronegative thing present, and the delocalised system will be heavily distorted towards it. That still leaves the oxygen atom with most of its negative charge.</p>
<p>What delocalisation there is makes the phenoxide ion more stable than it would otherwise be, and so phenol is acidic to an extent.</p>
<p>However, the delocalisation hasn't shared the charge around very effectively. There is still lots of negative charge around the oxygen to which hydrogen ions will be attracted – and so the phenol will readily re-form. Phenol is therefore only very weakly acidic.</p>
</div>

<h3>Ethanol</h3>

<div class="text-block">
<p>Ethanol, CH<sub>3</sub>CH<sub>2</sub>OH, is so weakly acidic that you would hardly count it as acidic at all. If the hydrogen-oxygen bond breaks to release a hydrogen ion, an ethoxide ion is formed:</p>
</div>

<div class="image-container ">
<img src="ethoxide.GIF ">
</div>

<div class="text-block">
<p>This has nothing at all going for it. There is no way of delocalising the negative charge, which remains firmly on the oxygen atom. That intense negative charge will be highly attractive towards hydrogen ions, and so the ethanol will instantly re-form.</p>
<p>Since ethanol is very poor at losing hydrogen ions, it is hardly acidic at all.</p>
</div>

<h2>Variations in Acid Strengths Between Different Carboxylic acids</h2>

<div class="text-block">
<p>You might think that all carboxylic acids would have the same strength because each depends on the delocalisation of the negative charge around the -COO<sup>-</sup> group to make the anion more stable, and so more reluctant to re-combine with a hydrogen ion.</p>
<p>In fact, the carboxylic acids have widely different acidities. One obvious difference is between methanoic acid, HCOOH, and the other simple carboxylic acids:</p>
</div>


<table class="data-table">
<tbody>
<tr>
<th>
</th>
<th>
pK<sub>a</sub>
</th>
</tr>
<tr>
<td>
HCOOH
</td>
<td>
3.75
</td>
</tr>
<tr>
<td>
CH<sub>3</sub>CHOOH
</td>
<td>
4.76
</td>
</tr>
<tr>
<td>
CH<sub>3</sub>CH<sub>2</sub>COOH
</td>
<td>
4.87
</td>
</tr>
<tr>
<td>
CH<sub>3</sub>CH<sub>2</sub>CH<sub>2</sub>COOH
</td>
<td>
4.82
</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>Remember that the higher the value for pK<sub>a</sub>, the weaker the acid is.</p>
<p>Why is ethanoic acid weaker than methanoic acid? It again depends on the stability of the anions formed – on how much it is possible to delocalise the negative charge. The less the charge is delocalised, the less stable the ion, and the weaker the acid.</p>
<p>The methanoate ion (from methanoic acid) is:</p>
</div>

<div class="image-container ">
<img src="methanoate.GIF ">
</div>

<div class="text-block">
<p>The only difference between this and the ethanoate ion is the presence of the CH<sub>3</sub> group in the ethanoate.</p>
<p>But that's important! Alkyl groups have a tendency to "push " electrons away from themselves. That means that there will be a small amount of extra negative charge built up on the -COO<sup>-</sup> group. Any build-up of charge will make the ion less stable, and more attractive to hydrogen ions.</p>
<p>Ethanoic acid is therefore weaker than methanoic acid, because it will re-form more easily from its ions.</p>
</div>

<div class="image-container ">
<img src="ethanoate5.GIF ">
</div>

<div class="text-block">
<p>The other alkyl groups have "electron-pushing " effects very similar to the methyl group, and so the strengths of propanoic acid and butanoic acid are very similar to ethanoic acid.</p>
</div>

<div class="note ">
<p>Note: If you want more information about the inductive effect of alkyl groups, you could read about <a href="../../mechanisms/eladd/carbonium.html#top ">carbocations (carbonium ions)</a> in the mechanism section of this site.</p>
</div>

<div class="text-block">
<p>The acids can be strengthened by pulling charge away from the -COO<sup>-</sup> end. You can do this by attaching electronegative atoms like chlorine to the chain.</p>
</div>

<div class="image-container ">
<img src="clethanoate.GIF ">
</div>

<div class="text-block">
<p>As the next table shows, the more chlorines you can attach the better:</p>
</div>
<table class="data-table">
<tbody>
<tr>
<th>
</th>
<th>
pK<sub>a</sub>
</th>
</tr>
<tr>
<td>
CH<sub>3</sub>COOH
</td>
<td>
4.76
</td>
</tr>
<tr>
<td>
CH<sub>2</sub>ClCOOH
</td>
<td>
2.86
</td>
</tr>
<tr>
<td>
CHCl<sub>2</sub>COOH
</td>
<td>
1.29
</td>
</tr>
<tr>
<td>
CCl<sub>3</sub>COOH
</td>
<td>
0.65
</td>
</tr>
</tbody>
</table>


<div class="text-block">
<p>Trichloroethanoic acid is quite a strong acid.</p>
<p>Attaching different halogens also makes a difference. Fluorine is the most electronegative and so you would expect it to be most successful at pulling charge away from the -COO<sup>-</sup> end and so strengthening the acid.</p>
</div>

<table class="data-table">
<tbody>
<tr>
<th>
</th>
<th>
pK<sub>a</sub>
</th>
</tr>
<tr>
<td>
CH<sub>2</sub>FCOOH
</td>
<td>
2.66
</td>
</tr>
<tr>
<td>
CH<sub>2</sub>ClCOOH
</td>
<td>
2.86
</td>
</tr>
<tr>
<td>
CH<sub>2</sub>BrCOOH
</td>
<td>
2.90
</td>
</tr>
<tr>
<td>
CH<sub>2</sub>ICOOH
</td>
<td>
3.17
</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>The effect is there, but isn't as great as you might expect.</p>
<p>Finally, notice that the effect falls off quite quickly as the attached halogen gets further away from the -COO<sup>-</sup> end. Here is what happens if you move a chlorine atom along the chain in butanoic acid.</p>
</div>

<table class="data-table">
<tbody>
<tr>
<th>
</th>
<th>
pK<sub>a</sub>
</th>
</tr>
<tr>
<td>
CH<sub>3</sub>CH<sub>2</sub>CH<sub>2</sub>COOH
</td>
<td>
4.82
</td>
</tr>
<tr>
<td>
CH<sub>3</sub>CH<sub>2</sub>CHClCOOH
</td>
<td>
2.84
</td>
</tr>
<tr>
<td>
CH<sub>3</sub>CHClCH<sub>2</sub>COOH
</td>
<td>
4.06
</td>
</tr>
<tr>
<td>
CH<sub>3</sub>CHClCH<sub>2</sub>COOH
</td>
<td>
4.52
</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>The chlorine is effective at withdrawing charge when it is next-door to the -COO<sup>-</sup> group, and much less so as it gets even one carbon further away.</p>
</div>

<div class="questions-note ">
<p>Questions to test your understanding</p>
<a href="../questions/q-organicacids.pdf ">Questions on organic acids</a>
<a href="../questions/a-organicacids.pdf ">Answers</a>
</div>

<div class="link-list ">
<p>Where would you like to go now?</p>
<a href="../acidmenu.html#top ">To the acids and bases menu</a>
<a href="../../orgmenu.html#top ">To menu of basic organic chemistry</a>
<a href="../../index.html#top ">To Main Menu</a>
</div>

<div class="link-list">

<p>You might also be interested in the properties and reactions of:</p>
<a href="../../organicprops/acidmenu.html#top ">carboxylic acids</a>
<a href="../../organicprops/aminoacidmenu.html#top ">amino acids</a>
<a href="../../organicprops/phenolmenu.html#top ">phenol</a>
<a href="../../organicprops/alcoholmenu.html#top ">alcohols</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>
</html>