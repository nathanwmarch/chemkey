<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Explaining the Strength of Organic Bases | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains why organic bases are basic and what affects their relative strengths.">
<meta name="keywords" content="organic, bases, basicity, strength, ammonia, amines, primary amines, phenylamine, aniline, pkb">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Explaining the Strength of Organic Bases</h1>

<div class="text-block">
<p>This page explains why simple organic bases are basic and looks at the factors which affect their relative strengths. For A-level purposes, all the bases we are concerned with are primary amines – compounds in which one of the hydrogens in an ammonia molecule, NH<sub>3</sub>, is replaced either by an alkyl group or a benzene ring.</p>
</div>

<h2>Why are Primary Amines Basic?</h2>

<h3>Ammonia as a Weak Base</h3>

<div class="text-block">
<p>All of the compounds we are concerned with are derived from ammonia and so we'll start by looking at the reason for its basic properties.</p>
<p>For the purposes of this topic, we are going to take the definition of a base as "a substance which combines with hydrogen ions (protons)". We are going to get a measure of this by looking at how easily the bases take hydrogen ions from water molecules when they are in solution in water.</p>
<p>Ammonia in solution sets up this equilibrium:</p>
</div>

<div class="block-formula">
\text{NH}_3 + \text{H}_2\text{O} \xrightleftharpoons{} {\text{NH}_4}^+ + \text{OH}^-
</div>

<div class="text-block">
<p>An ammonium ion is formed together with hydroxide ions. Because the ammonia is only a weak base, it doesn't hang on to the extra hydrogen ion very effectively and so the reaction is reversible. At any one time, about 99% of the ammonia is present as unreacted molecules. The position of equilibrium lies well to the left.</p>
<p>The ammonia reacts as a base because of the active lone pair on the nitrogen. Nitrogen is more electronegative than hydrogen and so attracts the bonding electrons in the ammonia molecule towards itself. That means that in addition to the lone pair, there is a build-up of negative charge around the nitrogen atom. That combination of extra negativity and active lone pair attracts the new hydrogen from the water.</p>
</div>

<div class="image-container">
<img src="ammoniaeq2.GIF">
</div>

<h3>Comparing the Strengths of Weak Bases</h3>

<div class="text-block">
<p>The strengths of weak bases are measured on the pK<sub>b</sub> scale. The smaller the number on this scale, the stronger the base is.</p>
<p>Three of the compounds we shall be looking at, together with their pK<sub>b</sub> values are:</p>
</div>

<table class="data-table">
<thead>
<tr>
<th></th>
<th>pK<sub>b</sub></th>
</tr>
</thead>
<tbody>
<tr>
<td>

<div class="chemical-structure" name="ammonia" type="2d"></div>
 
</td>
<td>

4.75
 
</td>
</tr>
<tr>
<td>

<div class="chemical-structure" name="methylamine" type="2d"></div>
 
</td>
<td>

3.36
 
</td>
</tr>
<tr>
<td>

<div class="chemical-structure" name="phenylamine" type="2d"></div>
 
</td>
<td>

9.38
 
</td>
</tr>
</tbody>
</table>

<div class="text-block">
<p>Remember – the smaller the number the stronger the base. Comparing the other two to ammonia, you will see that methylamine is a stronger base, whereas phenylamine is very much weaker.</p>
<p>Methylamine is typical of aliphatic primary amines – where the -NH<sub>2</sub> group is attached to a carbon chain. All aliphatic primary amines are stronger bases than ammonia.</p>
<p>Phenylamine is typical of aromatic primary amines – where the -NH<sub>2</sub> group is attached directly to a benzene ring. These are very much weaker bases than ammonia.</p>
</div>

<h2>Explaining the Differences in Base Strengths</h2>

<h3>The Factors to Consider</h3>

<div class="text-block">
<p>Two of the factors which influence the strength of a base are:</p>
</div>

<ul>
<li>the ease with which the lone pair picks up a hydrogen ion.</li>

<li>the stability of the ions being formed.</li>
</ul>

<h3>Why are Aliphatic Primary Amines Stronger Bases Than Ammonia?</h3>

<h4>Methylamine</h4>

<div class="text-block">
<p>Methylamine has the structure:</p>
</div>

<div class="chemical-structure" name="methylamine" type="2d"></div>

<div class="text-block">
<p>The only difference between this and ammonia is the presence of the CH<sub>3</sub> group in the methylamine. But that's important! Alkyl groups have a tendency to "push" electrons away from themselves. That means that there will be a small amount of extra negative charge built up on the nitrogen atom. That extra negativity around the nitrogen makes the lone pair even more attractive towards hydrogen ions.</p>
</div>

<div class="image-container">
<img src="ch3nh2b.GIF">
</div>

<div class="text-block">
<p>Making the nitrogen more negative helps the lone pair to pick up a hydrogen ion.</p>
<p>What about the effect on the positive methylammonium ion formed? Is this more stable than a simple ammonium ion?</p>
<p>Compare the methylammonium ion with an ammonium ion:</p>
</div>

<div class="image-container">
<img src="ch3nh2c.GIF">
</div>

<div class="text-block">
<p>In the methylammonium ion, the positive charge is spread around the ion by the "electron-pushing" effect of the methyl group. The more you can spread charge around, the more stable an ion becomes. In the ammonium ion there isn't any way of spreading the charge.</p>
<p>To summarise:</p>
</div>

<ul>
<li>The nitrogen is more negative in methylamine than in ammonia, and so it picks up a hydrogen ion more readily.</li>
<li>The ion formed from methylamine is more stable than the one formed from ammonia, and so is less likely to shed the hydrogen ion again.</li>
</ul>

<div class="text-block">
<p>Taken together, these mean that methylamine is a stronger base than ammonia.</p>
</div>

<div class="note">
<p>Note: This is a bit of a simplification for A-level purposes. As bases get more complex, another factor concerning the stability of the ions formed becomes important. That concerns the way they interact with water molecules in the solution. You don't need to worry about that at this level.</p>
</div>

<h4>The other aliphatic primary amines</h4>

<div class="text-block">
<p>The other alkyl groups have "electron-pushing" effects very similar to the methyl group, and so the strengths of the other aliphatic primary amines are very similar to methylamine.</p>
</div>

<div class="note">
<p>Note: If you want more information about the inductive effect of alkyl groups, you could read about <a href="../../mechanisms/eladd/carbonium.html#top">carbocations (carbonium ions)</a> in the mechanism section of this site.</p>
</div>

<div class="text-block">
<p>For example:</p>
</div>

<table class="data-table">
<tbody>
<tr>
<th>
</th>
<th>
pK<sub>b</sub>
</th>
</tr>
<tr>
<td>
CH<sub>3</sub>NH<sub>2</sub>
</td>
<td>
3.36
</td>
</tr>
<tr>
<td>
CH<sub>3</sub>CH<sub>2</sub>NH<sub>2</sub>
</td>
<td>
3.27
</td>
</tr>
<tr>
<td>
CH<sub>3</sub>CH<sub>2</sub>CH<sub>2</sub>NH<sub>2</sub>
</td>
<td>
3.16
</td>
</tr>
<tr>
<td>
CH<sub>3</sub>CH<sub>2</sub>CH<sub>2</sub>CH<sub>2</sub>NH<sub>2</sub>
</td>
<td>
3.39
</td>
</tr>
</tbody>
</table>

<h3>Why are Aromatic Primary Amines Much Weaker Bases Than Ammonia?</h3>

<div class="text-block">
<p>An aromatic primary amine is one in which the -NH<sub>2</sub> group is attached directly to a benzene ring. The only one you are likely to come across is phenylamine.</p>
<p>Phenylamine has the structure:</p>
</div>

<div class="image-container">
<img src="aniline1.GIF">
</div>

<div class="text-block">
<p>The lone pair on the nitrogen touches the delocalised ring electrons</p>
</div>

<div class="image-container">
<img src="aniline2.GIF">
</div>

<div class="text-block">
<p> and becomes delocalised with them:</p>
</div>

<div class="image-container">
<img src="aniline3.GIF">
</div>

<div class="text-block">
<p>That means that the lone pair is no longer fully available to combine with hydrogen ions. The nitrogen is still the most electronegative atom in the molecule, and so the delocalised electrons will be attracted towards it, but the intensity of charge around the nitrogen is nothing like what it is in, say, an ammonia molecule.</p>
<p>The other problem is that if the lone pair is used to join to a hydrogen ion, it is no longer available to contribute to the delocalisation. That means that the delocalisation would have to be disrupted if the phenylamine acts as a base. Delocalisation makes molecules more stable, and so disrupting the delocalisation costs energy and won't happen easily.</p>
<p>Taken together – the lack of intense charge around the nitrogen, and the need to break some delocalisation – this means that phenylamine is a very weak base indeed.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-organicbases.pdf" target="_blank">Questions on organic bases</a>
<a href="../questions/a-organicbases.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../acidmenu.html#top">To the acids and bases menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="link-list">
<p>You might also be interested in the properties and reactions of:</p>
<a href="../../organicprops/aminemenu.html#top">aliphatic amines</a>
<a href="../../organicprops/anilinemenu.html#top">phenylamine (aniline)</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>