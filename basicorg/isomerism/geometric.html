<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Stereoisomerism – Geometric Isomerism | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains what geometric (cis / trans) isomerism is and how you recognise the possibility of it in a molecule.">
<meta name="keywords" content="organic, chemistry, isomers, isomerism, geometric isomers, geometric isomerism, cis, trans, stereoisomers, stereoisomerism">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js">
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Stereoisomerism – Geometric Isomerism</h1>

<div class="text-block">
<p>Geometric isomerism (also known as cis-trans isomerism or E-Z isomerism) is a form of stereoisomerism. This page explains what stereoisomers are and how you recognise the possibility of geometric isomers in a molecule.</p>
<p>Further down the page, you will find a link to a second page which describes the E-Z notation for naming geometric isomers. You shouldn't move on to that page (even if the E-Z notation is what your syllabus is asking for) until you are really confident about how geometric isomers arise and how they are named on the cis-trans system.</p>
<p>The E-Z system is better for naming more complicated structures but is more difficult to understand than cis-trans. The cis-trans system of naming is still widely used – especially for the sort of simple molecules you will meet at this level. That means that irrespective of what your syllabus might say, you will have to be familiar with both systems. Get the easier one sorted out before you go on to the more sophisticated one!</p>
</div>

<h2>What is Stereoisomerism?</h2>

<h3>What are Isomers?</h3>

<div class="definition">
<p>Isomers are molecules that have the same molecular formula, but have a different arrangement of the atoms in space.</p>
</div>

<div class="text-block">
<p>That definition excludes any different arrangements which are simply due to the molecule rotating as a whole, or rotating about particular bonds.</p>
<p>Where the atoms making up the various isomers are joined up in a different order, this is known as structural isomerism. Structural isomerism is not a form of stereoisomerism, and is dealt with on a separate page.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="structural.html#top">structural isomerism</a>, it might be worth reading about it before you go on with this page.</p>
</div>

<h3>What are Stereoisomers?</h3>

<div class="definition">
<p>Stereoisomers have the same atoms, connected together with the same configuration, but with different spatial arrangements.</p>
</div>

<div class="text-block">
<p>Geometric isomerism is one form of stereoisomerism.</p>
</div>

<h2>Geometric (cis / trans) Isomerism </h2>

<h3>How Geometric Isomers Arise</h3>

<div class="text-block">
<p>These isomers occur where you have restricted rotation somewhere in a molecule. At an introductory level in organic chemistry, examples usually just involve the carbon-carbon double bond – and that's what this page will concentrate on.</p>
<p>Think about what happens in molecules where there is unrestricted rotation about carbon bonds – in other words where the carbon-carbon bonds are all single. The next diagram shows two possible configurations of 1,2-dichloroethane.</p>
</div>

<div class="image-container">
<img src="freerot.GIF">
</div>

<div class="text-block">
<p>These two models represent exactly the same molecule. You can get from one to the other just by twisting around the carbon-carbon single bond. These molecules are not isomers.</p>
<p>If you draw a structural formula instead of using models, you have to bear in mind the possibility of this free rotation about single bonds. You must accept that these two structures represent the same molecule:</p>
</div>

<div class="image-container">
<img src="diclethane.GIF">
</div>

<div class="text-block">
<p>But what happens if you have a carbon-carbon double bond – as in 1,2-dichloroeth<span class="attention">ene</span>?</p>
</div>

<div class="image-container">
<img src="norot.GIF">
</div>

<div class="text-block">
<p>These two molecules aren't the same. The carbon-carbon double bond won't rotate and so you would have to take the models to pieces in order to convert one structure into the other one. That is a simple test for isomers. If you have to take a model to pieces to convert it into another one, then you've got isomers. If you merely have to twist it a bit, then you haven't!</p>
</div>

<div class="note">
<p>Note: In the model, the reason that you can't rotate a carbon-carbon double bond is that there are two links joining the carbons together. In reality, the reason is that you would have to break the &pi; bond. &pi; bonds are formed by the sideways overlap between p orbitals. If you tried to rotate the carbon-carbon bond, the p orbitals won't line up any more and so the &pi; bond is disrupted. This costs energy and only happens if the compound is heated strongly. If you are interested in the <a href="../bonding/ethene.html#top">bonding in carbon-carbon double bonds</a>, follow this link. Be warned, though, that you might have to read several pages of background material and it could all take a long time. It isn't necessary for understanding the rest of this page.</p>
</div>

<div class="text-block">
<p>Drawing structural formulae for the last pair of models gives two possible isomers.</p>
<p>In one, the two chlorine atoms are locked on opposite sides of the double bond. This is known as the trans isomer. (trans : from latin meaning "across" – as in transatlantic).</p>
<p>In the other, the two chlorine atoms are locked on the same side of the double bond. This is know as the cis isomer. (cis : from latin meaning "on this side")</p>
</div>

<div class="image-container">
<img src="diclethene.GIF">
</div>

<div class="text-block">
<p>The most likely example of geometric isomerism you will meet at an introductory level is but-2-ene. In one case, the CH<sub>3</sub> groups are on opposite sides of the double bond, and in the other case they are on the same side.</p>
</div>

<div class="image-container">
<img src="butene.GIF">
</div>

<h3>The Importance of Drawing Geometric Isomers pProperly</h3>

<div class="text-block">
<p>It's very easy to miss geometric isomers in exams if you take short-cuts in drawing the structural formulae. For example, it is very tempting to draw but-2-ene as</p>
<p align="center">CH
<sub>3</sub>CH=CHCH
<sub>3</sub>
</p>
<p>If you write it like this, you will almost certainly miss the fact that there are geometric isomers. If there is even the slightest hint in a question that isomers might be involved, always draw compounds containing carbon-carbon double bonds showing the correct bond angles (120°) around the carbon atoms at the ends of the bond. In other words, use the format shown in the last diagrams above.</p>
</div>

<h3>How to Recognise the Possibility of Geometric Isomerism</h3>

<div class="text-block">
<p>You obviously need to have restricted rotation somewhere in the molecule. Compounds containing a carbon-carbon double bond have this restricted rotation. (Other sorts of compounds may have restricted rotation as well, but we are concentrating on the case you are most likely to meet when you first come across geometric isomers.) If you have a carbon-carbon double bond, you need to think carefully about the possibility of geometric isomers.</p>
<p>What needs to be attached to the carbon-carbon double bond?</p>
</div>
<div class="note">
<p>Note: This is much easier to understand if you have actually got some models to play with. If your school or college hasn't given you the opportunity to play around with molecular models in the early stages of your organic chemistry course, you might consider getting hold of a cheap set. The models made by Molymod are both cheap and easy to use. An introductory organic set is more than adequate. Google molymod to find a supplier and more about them.</p>
<p>Share the cost with some friends, keep it in good condition and don't lose any bits, and resell it via eBay or Amazon at the end of your course.</p>
<p>Alternatively, get hold of some coloured Plasticene (or other children's modelling clay) and some used matches and make your own. It's cheaper, but more difficult to get the bond angles right.</p>
</div>

<div class="text-block">
<p>Think about this case:</p>
</div>

<div class="image-container">
<img src="geometric1.GIF">
</div>

<div class="text-block">
<p>Although we've swapped the right-hand groups around, these are still the same molecule. To get from one to the other, all you would have to do is to turn the whole model over.</p>
<p>You won't have geometric isomers if there are two groups the same on one end of the bond – in this case, the two pink groups on the left-hand end.</p>
<p>So there must be two different groups on the left-hand carbon and two different groups on the right-hand one. The cases we've been exploring earlier are like this:</p>
</div>

<div class="image-container">
<img src="geometric2.GIF">
</div>

<div class="text-block">
<p>But you could make things even more different and still have geometric isomers:</p>
</div>

<div class="image-container">
<img src="geometric3.GIF">
</div>

<div class="text-block">
<p>Here, the blue and green groups are either on the same side of the bond or the opposite side.</p>
<p>Or you could go the whole hog and make everything different. You still get geometric isomers, but by now the words cis and trans are meaningless. This is where the more sophisticated E-Z notation comes in.</p>
</div>

<div class="image-container">
<img src="geometric4.GIF">
</div>

<div class="summary">
<p>To get geometric isomers you must have:</p>
<ul>
<li>restricted rotation (often involving a carbon-carbon double bond for introductory purposes);</li>
<li>two different groups on the left-hand end of the bond and two different groups on the right-hand end. It doesn't matter whether the left-hand groups are the same as the right-hand ones or not.</li>
</ul>
</div>

<div class="note">
<p>Note: The rest of this page looks at how geometric isomerism affects the melting and boiling points of compounds. If you are meeting geometric isomerism for the first time, you may not need this at the moment.</p>
<p>If you need to know about <a href="ez.html#top">E-Z notation</a>, you could follow this link at once to the next page. (But be sure that you understand what you have already read on this page first!)</p>
<p>Alternatively, read to the bottom of this page where you will find this link repeated.</p>
</div>

<h3>The Effect of Geometric Isomerism on Physical Properties</h3>

<div class="text-block">
<p>The table shows the melting point and boiling point of the cis and trans isomers of 1,2-dichloroethene.</p>
</div>

<table class="data-table">

<tbody>
<tr><th>isomer</th><th>melting point<br>/ °C</th><th>boiling point<br>/ °C</th></tr>
<tr><td>cis</td><td>-80</td><td>60</td></tr>
<tr><td>trans</td><td>-50</td><td>48</td></tr>
</tbody>
</table>
<div class="text-block">
<p>In each case, the higher melting or boiling point is shown in red.</p>
<p>You will notice that:</p>
</div>

<ul>
<li>the trans isomer has the higher melting point;</li>
<li>the cis isomer has the higher boiling point.</li>
</ul>

<div class="text-block">
<p>This is common. You can see the same effect with the cis and trans isomers of but-2-ene:</p>
</div>

<table class="data-table">

<tbody>
<tr><th>isomer</th><th>melting point<br>/ °C</th><th>boiling point<br>/ °C</th>

</tr>
<tr><td>cis-but-2-ene</td><td>-139</td><td>4</td></tr>

<tr><td>trans-but-2-ene</td><td>-106</td><td>1</td></tr>

</tbody>
</table>

<h4>Why is the boiling point of the cis isomers higher?</h4>

<div class="text-block">
<p>There must be stronger intermolecular forces between the molecules of the cis isomers than between trans isomers.</p>
<p>Taking 1,2-dichloroethene as an example:</p>
<p>Both of the isomers have exactly the same atoms joined up in exactly the same order. That means that the van der Waals dispersion forces between the molecules will be identical in both cases.</p>
<p>The difference between the two is that the cis isomer is a polar molecule whereas the trans isomer is non-polar.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="../../atoms/bonding/vdw.html#top">intermolecular forces</a> (and also about bond polarity), it is essential that you follow this link before you go on. You need to know about van der Waals dispersion forces and dipole-dipole interactions, and to follow the link on that page to another about bond polarity if you need to.</p>
</div>

<div class="text-block">
<p>Both molecules contain polar chlorine-carbon bonds, but in the cis isomer they are both on the same side of the molecule. That means that one side of the molecule will have a slight negative charge while the other is slightly positive. The molecule is therefore polar.</p>
</div>

<div class="image-container">
<img src="cisdichlor.gif">
</div>

<div class="text-block">
<p>Because of this, there will be dipole-dipole interactions as well as dispersion forces – needing extra energy to break. That will raise the boiling point.</p>
<p>A similar thing happens where there are CH<sub>3</sub> groups attached to the carbon-carbon double bond, as in cis-but-2-ene.</p>
<p>Alkyl groups like methyl groups tend to "push" electrons away from themselves. You again get a polar molecule, although with a reversed polarity from the first example.</p>
</div>

<div class="image-container">
<img src="cisdich3.gif">
</div>

<div class="note">
<p>Note: The term "electron pushing" is only to help remember what happens. The alkyl group doesn't literally "push" the electrons away – the other end of the bond attracts them more strongly. The arrows with the cross on (representing the more positive end of the bond) are a conventional way of showing this electron pushing effect.</p>
</div>

<div class="text-block">
<p>By contrast, although there will still be polar bonds in the trans isomers, overall the molecules are non-polar.</p>
</div>

<div class="image-container">
<img src="trans.gif">
</div>

<div class="text-block">
<p>The slight charge on the top of the molecule (as drawn) is exactly balanced by an equivalent charge on the bottom. The slight charge on the left of the molecule is exactly balanced by the same charge on the right.</p>
<p>This lack of overall polarity means that the only intermolecular attractions these molecules experience are van der Waals dispersion forces. Less energy is needed to separate them, and so their boiling points are lower.</p>
</div>

<h4>Why is the melting point of the cis isomers lower?</h4>

<div class="text-block">
<p>You might have thought that the same argument would lead to a higher melting point for cis isomers as well, but there is another important factor operating.</p>
<p>In order for the intermolecular forces to work well, the molecules must be able to pack together efficiently in the solid.</p>
<p>Trans isomers pack better than cis isomers. The "U" shape of the cis isomer doesn't pack as well as the straighter shape of the trans isomer.</p>
<p>The poorer packing in the cis isomers means that the intermolecular forces aren't as effective as they should be and so less energy is needed to melt the molecule – a lower melting point.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-geomisomerism.pdf" target="_blank">Questions on geometric isomerism</a>
<a href="../questions/a-geomisomerism.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="ez.html#top">To learn about E-Z notation</a>
<a href="../isomermenu.html#top">To the isomerism menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>