<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Structural Isomerism | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains what structural isomerism is and the various ways that structural isomers can arise">
<meta name="keywords" content="organic, chemistry, structural isomers, structural isomerism, chain isomers, chain isomerism, position isomers, position isomerism, series isomers, series isomerism, functional group isomers, functional group isomerism">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Structural Isomerism</h1>

<div class="text-block">
<p>This page explains what structural isomerism is, and looks at some of the various ways that structural isomers can arise.</p>
</div>

<h2>What is Structural Isomerism?</h2>

<h3>What are Isomers?</h3>

<div class="definition">
<p>Isomers are molecules that have the same molecular formula, but have a different arrangement of the atoms in space.</p>
</div>

<div class="text-block">
<p>That definition excludes any different arrangements which are simply due to the molecule rotating as a whole, or rotating about particular bonds.</p>
<p>For example, both of the following are the same molecule. They are not isomers. Both are butane.</p>
</div>

<div class="image-container">
<img src="bentbutane.GIF">
</div>

<div class="text-block">
<p>There are also endless other possible ways that this molecule could twist itself. There is completely free rotation around all the carbon-carbon single bonds.</p>
</div>

<div class="image-container">
<p>Note: Isomerism is much easier to understand if you have actually got some models to play with. If your school or college hasn't given you the opportunity to play around with molecular models in the early stages of your organic chemistry course, you might consider getting hold of a cheap set. The models made by Molymod are both cheap and easy to use. An introductory organic set is more than adequate. Google molymod to find a supplier and more about them.</p>
<p>Share the cost with some friends, keep it in good condition and don't lose any bits, and resell it via eBay or Amazon at the end of your course.</p>
<p>Alternatively, get hold of some coloured Plasticene (or other children's modelling clay) and some used matches and make your own. It's cheaper, but more difficult to get the bond angles right.</p>
</div>

<div class="text-block">
<p>If you had a model of a molecule in front of you, you would have to take it to pieces and rebuild it if you wanted to make an isomer of that molecule. If you can make an apparently different molecule just by rotating single bonds, it's not different – it's still the same molecule.</p>
</div>

<div class="note">
<p>Note: It's really important that you understand this. If you aren't sure, then you must get hold of (or make) some models.</p>
</div>

<h3>What are Structural Isomers?</h3>

<div class="definition">
<p>Structural isomers contain the same number of atoms of each element, but the atoms are bonded in a different configuration.</p>
</div>
<div class="text-block">
<p>This is easier to see with specific examples.</p>
<p>What follows looks at some of the ways that structural isomers can arise. The names of the various forms of structural isomerism probably don't matter all that much, but you must be aware of the different possibilities when you come to draw isomers.</p>
</div>

<h2>Types of Structural Isomerism </h2>

<h3>Chain Isomerism</h3>

<div class="text-block">
<p>These isomers arise because of the possibility of branching in carbon chains. For example, there are two isomers of butane, C<sub>4</sub>H<sub>10</sub>. In one of them, the carbon atoms lie in a "straight chain" whereas in the other the chain is branched.</p>
</div>

<div class="image-container">
<img src="butane.GIF">
</div>

<div class="note">
<p>Note: Although the chain is drawn as straight, in reality it's anything but straight. If you aren't happy about the ways of <a href="../conventions/draw.html#top">drawing organic molecules</a>, follow this link.</p>
</div>

<div class="image-container">
<img src="stillbutane.GIF">
</div>

<div class="text-block">
<p>Be careful not to draw "false" isomers which are just twisted versions of the original molecule. For example, this structure is just the straight chain version of butane rotated about the central carbon-carbon bond.</p>
<p>You could easily see this with a model. This is the example we've already used at the top of this page.</p>
</div>

<div class="image-container">
<img src="bentbutane.GIF">
</div>

<div class="text-block">
<p>Pentane, C<sub>5</sub>H<sub>12</sub>, has three chain isomers. If you think you can find any others, they are simply twisted versions of the ones below. If in doubt make some models.</p>
</div>

<div class="image-container">
<img src="pentane.GIF">
</div>

<h3>Position isomerism</h3>

<div class="text-block">
<p>In position isomerism, the basic carbon skeleton remains unchanged, but important groups are moved around on that skeleton.</p>
<p>For example, there are two structural isomers with the molecular formula C<sub>3</sub>H<sub>7</sub>Br. In one of them the bromine atom is on the end of the chain, whereas in the other it's attached in the middle.</p>
</div>

<div class="image-container">
<img src="bromoprop.GIF">
</div>

<div class="text-block">
<p>If you made a model, there is no way that you could twist one molecule to turn it into the other one. You would have to break the bromine off the end and re-attach it in the middle. At the same time, you would have to move a hydrogen from the middle to the end.</p>
<p>Another similar example occurs in alcohols such as C<sub>4</sub>H<sub>9</sub>OH</p>
</div>

<div class="image-container">
<img src="butanola.GIF">
</div>

<div class="text-block">
<p>These are the only two possibilities provided you keep to a four carbon chain, but there is no reason why you should do that. You can easily have a mixture of chain isomerism and position isomerism – you aren't restricted to one or the other.</p>
<p>So two other isomers of butanol are:</p>
</div>

<div class="image-container">
<img src="butanolb.GIF">
</div>

<div class="note">
<p>Note: It's essential if you are asked to draw isomers in an exam not to restrict yourself to chain isomers or position isomers. You must be aware of all the possibilities.</p>
</div>

<div class="text-block">
<p>You can also get position isomers on benzene rings. Consider the molecular formula C<sub>7</sub>H<sub>7</sub>Cl. There are four different isomers you could make depending on the position of the chlorine atom. In one case it is attached to the side-group carbon atom, and then there are three other possible positions it could have around the ring – next to the CH<sub>3</sub> group, next-but-one to the CH<sub>3</sub> group, or opposite the CH<sub>3</sub> group.</p>
</div>

<div class="image-container">
<img src="chlorotol.GIF">
</div>

<h3>Functional Group Isomerism</h3>

<div class="text-block">
<p>In this variety of structural isomerism, the isomers contain different functional groups – that is, they belong to different families of compounds (different homologous series).</p>
<p>For example, a molecular formula C<sub>3</sub>H<sub>6</sub>O could be either propanal (an aldehyde) or propanone (a ketone).</p>
</div>

<div class="image-container">
<img src="aldket.GIF">
</div>

<div class="text-block">
<p>There are other possibilities as well for this same molecular formula – for example, you could have a carbon-carbon double bond (an alkene) and an -OH group (an alcohol) in the same molecule.</p>
</div>

<div class="image-container">
<img src="enol.GIF">
</div>

<div class="text-block">
<p>Another common example is illustrated by the molecular formula C<sub>3</sub>H<sub>6</sub>O<sub>2</sub>. Amongst the several structural isomers of this are propanoic acid (a carboxylic acid) and methyl ethanoate (an ester).</p>
</div>

<div class="image-container">
<img src="acidester.GIF">
</div>

<div class="note">
<p>Note: To repeat the warning given earlier: If you are asked to draw the structural isomers from a given molecular formula, don't forget to think about all the possibilities. Can you branch the carbon chain? Can you move a group around on that chain? Is it possible to make more than one type of compound? Be careful though! If you are asked to draw the structures of esters with the molecular formula C<sub>3</sub>H<sub>6</sub>O<sub>2</sub>, you aren't going to get a lot of credit for drawing propanoic acid, even if it is a valid isomer.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-structisomerism.pdf" target="_blank">Questions on structural isomerism</a>
<a href="../questions/a-structisomerism.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../isomermenu.html#top">To the isomerism menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>