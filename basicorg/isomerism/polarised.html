<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Plane-polarised Light | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Gives a simple explanation of plane-polarised light and the effect optical isomers have on it.">
<meta name="keywords" content="plane-polarised light, polarised light, polarized light, polarimeter">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Plane-polarised Light</h1>

<div class="text-block">
<p>This page gives a simple explanation of what plane-polarised light is and the way it is affected by optically active compounds.</p>
</div>

<h3>A Simple Analogy – "Plane-polarised String"</h3>

<div class="text-block">
<p>Imagine tying a piece of thick string to a hook in a wall, and then shaking the string vigorously. The string will be vibrating in all possible directions – up-and-down, side-to-side, and all the directions in-between – giving it a really complex overall motion.</p>
</div>

<div class="image-container">
<img src="string1.GIF">
</div>

<div class="text-block">
<p>Now, suppose you passed the string through a vertical slit. The string is a really snug fit in the slit. The only vibrations still happening the other side of the slit will be vertical ones. All the others will have been prevented by the slit.</p>
</div>

<div class="image-container">
<img src="string2.GIF">
</div>

<div class="text-block">
<p>What emerges from the slit could be described as "plane-polarised string", because the vibrations are only in a single (vertical) plane.</p>
<p>Now look at the possibility of putting a second slit on the string. If it is aligned the same way as the first one, the vibrations will still get through.</p>
</div>

<div class="image-container">
<img src="string3.GIF">
</div>

<div class="text-block">
<p>But if the second slit is at 90° to the first one, the string will stop vibrating entirely to the right of the second slit. The second slit will only let through horizontal vibrations – and there aren't any.</p>
</div>

<div class="image-container">
<img src="string4.GIF">
</div>

<h3>The Real Thing – Plane-polarised Light</h3>

<div class="text-block">
<p>Light is also made up of vibrations – this time, electromagnetic ones. Some materials have the ability to screen out all the vibrations apart from those in one plane and so produce plane-polarised light.</p>
<p>The most familiar example of this is the material that Polaroid sunglasses are made of. If you wear one pair of Polaroid sunglasses and hold another pair up in front of them so that the glasses are held vertically rather than horizontally, you'll find that no light gets through – you will just see darkness. This is equivalent to the two slits at right angles in the string analogy. The polaroids are described as being "crossed".</p>
</div>

<div class="note">
<p>Take care! It is important not to take the analogy too far. The polaroid material doesn't consist of "slits" in any sense of the word. The way it actually polarises the light is quite different (and irrelevant to us here!).</p>
</div>

<h3>Optically Active Substances</h3>

<div class="text-block">
<p>An optically active substance is one which can rotate the plane of polarisation of plane-polarised light. if you shine a beam of polarised monochromatic light (light of only a single frequency – in other words a single colour) through a solution of an optically active substance, when the light emerges, its plane of polarisation is found to have rotated.</p>
<p>The rotation may be either clockwise or anti-clockwise. Assuming the original plane of polarisation was vertical, you might get either of these results.</p>
</div>

<div class="image-container">
<img src="rotateplane.GIF">
</div>

<h3>How Can You Tell That the Plane of Polarisation has Been Rotated?</h3>

<div class="text-block">
<p>You use a polarimeter.</p>
</div>

<div class="image-container">
<img src="polarimeter.GIF">
</div>

<div class="text-block">
<p>The polariser and analyser are both made of polaroid material.</p>
<p>The polarimeter is originally set up with water in the tube. Water isn't optically active – it has no effect on the plane of polarisation. The analyser is rotated until you can't see any light coming through the instrument. The polaroids are then "crossed".</p>
</div>

<div class="image-container">
<img src="polarimeter1.GIF">
</div>

<div class="text-block">
<p>Now you put a solution of an optically active substance into the tube. It rotates the plane of polarisation of the light, and so the analyser won't be at right-angles to it any longer and some light will get through. You would have to rotate the analyser in order to cut the light off again.</p>
</div>

<div class="image-container">
<img src="polarimeter2.GIF">
</div>

<div class="text-block">
<p>You can easily tell whether the plane of polarisation has been rotated clockwise or anti-clockwise, and by how much.</p>
</div>

<div class="note">
<p>Note: It is very unlikely that you will need to learn much of this. You won't be able to tell from your syllabus whether any of this is required. Look instead at past exam papers. If you are working to a UK-based syllabus for 16 – 18 year olds, and haven't already got these, you can get them from your examiners by following the appropriate link on the <a href="../../syllabuses.html#top">syllabuses</a> page. Probably all you need to be able to do is to understand the expression "rotates the plane of polarisation of plane-polarised light" so that you can use it sensibly.</p>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="optical.html#top">Back to optical isomerism</a>
<a href="../isomermenu.html#top">To the isomerism menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>