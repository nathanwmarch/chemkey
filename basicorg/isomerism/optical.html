<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Stereoisomerism – Optical Isomerism | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains what optical isomerism is and how you recognise the possibility of it in a molecule.">
<meta name="keywords" content="organic, chemistry, isomers, isomerism, optical isomers, optical isomerism, stereoisomers, stereoisomerism, chiral, chirality, achiral, enantiomer, racemate, racemic, racemic mixture">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js">
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Stereoisomerism – Optical Isomerism</h1>

<div class="text-block">
<p>Optical isomerism is a form of stereoisomerism. This page explains what stereoisomers are and how you recognise the possibility of optical isomers in a molecule.</p>
</div>

<h2>What is Stereoisomerism?</h2>

<h3>What are Isomers?</h3>

<div class="text-block">
<p>Isomers are molecules that have the same molecular formula, but have a different arrangement of the atoms in space. That excludes any different arrangements which are simply due to the molecule rotating as a whole, or rotating about particular bonds.</p>
<p>Where the atoms making up the various isomers are joined up in a different order, this is known as structural isomerism. Structural isomerism is not a form of stereoisomerism, and is dealt with on a separate page.</p>
</div>

<div class="note">
<p>Note: If you aren't sure about <a href="structural.html#top">structural isomerism</a>, it might be worth reading about it before you go on with this page.</p>
</div>

<h3>What are Stereoisomers?</h3>

<div class="text-block">
<p>In stereoisomerism, the atoms making up the isomers are joined up in the same order, but still manage to have a different spatial arrangement. Optical isomerism is one form of stereoisomerism.</p>
</div>

<h2>Optical Isomerism</h2>

<h3>Why Optical Isomers?</h3>

<div class="text-block">
<p>Optical isomers are named like this because of their effect on plane polarised light. </p>
</div>

<div class="note">
<p>Help! If you don't understand about <a href="polarised.html#top">plane polarised light</a>, follow this link before you go on with this page.</p>
</div>

<div class="text-block">
<p>Simple substances which show optical isomerism exist as two isomers known as enantiomers.</p>
</div>

<ul>
<li>A solution of one enantiomer rotates the plane of polarisation in a clockwise direction. This enantiomer is known as the (+) form.<br>For example, one of the optical isomers (enantiomers) of the amino acid alanine is known as (+)alanine.</li>
<li>A solution of the other enantiomer rotates the plane of polarisation in an anti-clockwise direction. This enantiomer is known as the (-) form. So the other enantiomer of alanine is known as or (-)alanine.</li>
<li>If the solutions are equally concentrated the amount of rotation caused by the two isomers is exactly the same – but in opposite directions.</li>
<li>When optically active substances are made in the lab, they often occur as a 50/50 mixture of the two enantiomers. This is known as a racemic mixture or racemate. It has no effect on plane polarised light.</li>
</ul>

<div class="note">
<p>Note: One of the worrying things about optical isomerism is the number of obscure words that suddenly get thrown at you. Bear with it – things are soon going to get more visual! There is an alternative way of describing the (+) and (-) forms which is potentially very confusing. This involves the use of the lowercase letters d- and l-, standing for dextrorotatory and laevorotatory respectively. Unfortunately, there is another different use of the capital letters D- and L- in this topic. This is totally confusing! Stick with (+) and (-).</p>
</div>

<h3>How Optical Isomers Arise</h3>

<div class="text-block">
<p>The examples of organic optical isomers required at A-level all contain a carbon atom joined to four different groups. These two models each have the same groups joined to the central carbon atom, but still manage to be different:</p>
</div>

<div class="image-container">
<img src="models1.GIF">
</div>

<div class="text-block">
<p>Obviously as they are drawn, the orange and blue groups aren't aligned the same way. Could you get them to align by rotating one of the molecules? The next diagram shows what happens if you rotate molecule B.</p>
</div>

<div class="image-container">
<img src="models1a.GIF">
</div>

<div class="text-block">
<p>They still aren't the same – and there is no way that you can rotate them so that they look exactly the same. These are isomers of each other.</p>
<p>They are described as being non-superimposable in the sense that (if you imagine molecule B being turned into a ghostly version of itself) you couldn't slide one molecule exactly over the other one. Something would always be pointing in the wrong direction.</p>
</div>

<div class="note">
<p>Note: Unless your visual imagination is reasonably good, this is much easier to understand if you have actually got some models to play with. If your school or college hasn't given you the opportunity to play around with molecular models in the early stages of your organic chemistry course, you might consider getting hold of a cheap set. The models made by Molymod are both cheap and easy to use. An introductory organic set is more than adequate. Google molymod to find a supplier and more about them</p>
<p>Share the cost with some friends, keep it in good condition and don't lose any bits, and resell it via eBay or Amazon at the end of your course.</p>
<p>Alternatively, get hold of some coloured Plasticene (or other children's modelling clay) and some used matches and make your own. It's cheaper, but more difficult to get the bond angles right.</p>
</div>

<div class="text-block">
<p>What happens if two of the groups attached to the central carbon atom are the same? The next diagram shows this possibility.</p>
</div>

<div class="image-container">
<img src="models2.GIF">
</div>

<div class="text-block">
<p>The two models are aligned exactly as before, but the orange group has been replaced by another pink one.</p>
<p>Rotating molecule B this time shows that it is exactly the same as molecule A. You only get optical isomers if all four groups attached to the central carbon are different.</p>
</div>

<div class="image-container">
<img src="models2a.GIF">
</div>

<h3>Chiral and Achiral Molecules</h3>

<div class="text-block">
<p>The essential difference between the two examples we've looked at lies in the symmetry of the molecules.</p>
<p>If there are two groups the same attached to the central carbon atom, the molecule has a plane of symmetry. If you imagine slicing through the molecule, the left-hand side is an exact reflection of the right-hand side.</p>
<p>Where there are four groups attached, there is no symmetry anywhere in the molecule.</p>
</div>

<div class="image-container">
<img src="symmetry.GIF">
</div>

<div class="text-block">
<p>A molecule which has no plane of symmetry is described as chiral. The carbon atom with the four different groups attached which causes this lack of symmetry is described as a chiral centre or as an asymmetric carbon atom.</p>
<p>The molecule on the left above (with a plane of symmetry) is described as achiral.</p>
<p>Only chiral molecules have optical isomers.</p>
</div>

<h3>The Relationship Between the Enantiomers</h3>

<div class="text-block">
<p>One of the enantiomers is simply a non-superimposable mirror image of the other one.</p>
<p>In other words, if one isomer looked in a mirror, what it would see is the other one. The two isomers (the original one and its mirror image) have a different spatial arrangement, and so can't be superimposed on each other.</p>
</div>
Stereoisomerism – Optical Isomerism
<div class="image-container">
<img src="modmirror.GIF">
</div>

<div class="text-block">
<p>If an achiral molecule (one with a plane of symmetry) looked in a mirror, you would always find that by rotating the image in space, you could make the two look identical. It would be possible to superimpose the original molecule and its mirror image.</p>
</div>

<h3>Some Real Examples of Optical Isomers</h3>

<h4>Butan-2-ol</h4>

<div class="text-block">
<p>The asymmetric carbon atom in a compound (the one with four different groups attached) is often shown by a star.</p>
</div>

<div class="image-container">
<img src="butanolchir.GIF">
</div>

<div class="text-block">
<p>It's extremely important to draw the isomers correctly. Draw one of them using standard bond notation to show the 3-dimensional arrangement around the asymmetric carbon atom. Then draw the mirror to show the examiner that you know what you are doing, and then the mirror image.</p>
</div>

<div class="image-container">
<img src="butanolisom.GIF">
</div>

<div class="note">
<p>Help! If you don't understand this bond notation, follow this link to <a href="../conventions/draw.html#top">drawing organic molecules</a> before you go on with this page.</p>
</div>

<div class="text-block">
<p>Notice that you don't literally draw the mirror images of all the letters and numbers! It is, however, quite useful to reverse large groups – look, for example, at the ethyl group at the top of the diagram.</p>
<p>It doesn't matter in the least in what order you draw the four groups around the central carbon. As long as your mirror image is drawn accurately, you will automatically have drawn the two isomers.</p>
<p>So which of these two isomers is (+)butan-2-ol and which is (-)butan-2-ol? There is no simple way of telling that. For A-level purposes, you can just ignore that problem – all you need to be able to do is to draw the two isomers correctly.</p>
<h4>2-hydroxypropanoic acid (lactic acid)</h4>
<p>Once again the chiral centre is shown by a star.</p>
</div>

<div class="image-container">
<img src="lacticchir.GIF">
</div>

<div class="text-block">
<p>The two enantiomers are:</p>
</div>

<div class="image-container">
<img src="lacticisom.GIF">
</div>

<div class="text-block">
<p>It is important this time to draw the COOH group backwards in the mirror image. If you don't there is a good chance of you joining it on to the central carbon wrongly.</p>
</div>

<div class="image-container">
<img src="lacticwrong.GIF">
</div>

<div class="text-block">
<p>If you draw it like this in an exam, you won't get the mark for that isomer even if you have drawn everything else perfectly.</p>
<h4>2-aminopropanoic acid (alanine)</h4>
<p>This is typical of naturally-occurring amino acids. Structurally, it is just like the last example, except that the -OH group is replaced by -NH<sub>2</sub></p>
</div>

<div class="image-container">
<img src="alaninechir.GIF">
</div>

<div class="text-block">
<p>The two enantiomers are:</p>
</div>

<div class="image-container">
<img src="alanineisom.GIF">
</div>

<div class="text-block">
<p>Only one of these isomers occurs naturally: the (+) form. You can't tell just by looking at the structures which this is.</p>
<p>It has, however, been possible to work out which of these structures is which. Naturally occurring alanine is the right-hand structure, and the way the groups are arranged around the central carbon atom is known as an L- configuration. Notice the use of the capital L. The other configuration is known as D-.</p>
<p>So you may well find alanine described as L-(+)alanine.</p>
<p>That means that it has this particular structure and rotates the plane of polarisation clockwise.</p>
<p>Even if you know that a different compound has an arrangement of groups similar to alanine, you still can't say which way it will rotate the plane of polarisation.</p>
<p>The other amino acids, for example, have the same arrangement of groups as alanine does (all that changes is the CH<sub>3</sub> group), but some are (+) forms and others are (-) forms.</p>
<p>It's quite common for natural systems to only work with one of the enantiomers of an optically active substance. It isn't too difficult to see why that might be. Because the molecules have different spatial arrangements of their various groups, only one of them is likely to fit properly into the active sites on the enzymes they work with.</p>
<p>In the lab, it is quite common to produce equal amounts of both forms of a compound when it is synthesised. This happens just by chance, and you tend to get racemic mixtures.</p>
</div>

<div class="note">
<p>Note: For a detailed discussion of this, you could have a look at the page on the <a href="../../mechanisms/nucadd/hcntt.html#top">addition of HCN to aldehydes</a>.</p>
</div>

<h3>Identifying Chiral Centres in Skeletal Formulae</h3>

<div class="text-block">
<p>A skeletal formula is the most stripped-down formula possible. Look at the structural formula and skeletal formula for butan-2-ol.</p>
</div>

<div class="image-container">
<img src="butan2olskel.gif">
</div>

<div class="text-block">
<p>Notice that in the skeletal formula all of the carbon atoms have been left out, as well as all of the hydrogen atoms attached to carbons.</p>
<p>In a skeletal diagram of this sort:</p>
</div>

<ul>
<li>there is a carbon atom at each junction between bonds in a chain and at the end of each bond (unless there is something else there already – like the -OH group in the example);</li>
<li>there are enough hydrogen atoms attached to each carbon to make the total number of bonds on that carbon up to 4.</li>
</ul>

<div class="text-block">
<p>We have already discussed the butan-2-ol case further up the page, and you know that it has optical isomers. The second carbon atom (the one with the -OH attached) has four different groups around it, and so is a chiral centre.</p>
<p>Is this obvious from the skeletal formula?</p>
<p>Well, it is, provided you remember that each carbon atom has to have 4 bonds going away from it. Since the second carbon here only seems to have 3, there must also be a hydrogen attached to that carbon. So it has a hydrogen, an -OH group, and two different hydrocarbon groups (methyl and ethyl).</p>
<p>Four different groups around a carbon atom means that it is a chiral centre.</p>
</div>

<h4>A slightly more complicated case: 2,3-dimethylpentane</h4>

<div class="text-block">
<p>The diagrams show an uncluttered skeletal formula, and a repeat of it with two of the carbons labelled.</p>
</div>

<div class="image-container">
<img src="23dmpskel.gif">
</div>

<div class="text-block">
<p>Look first at the carbon atom labelled 2. Is this a chiral centre?</p>
<p>No, it isn't. Two bonds (one vertical and one to the left) are both attached to methyl groups. In addition, of course, there is a hydrogen atom and the more complicated hydrocarbon group to the right. It doesn't have 4 different groups attached, and so isn't a chiral centre.</p>
<p>What about the number 3 carbon atom?</p>
<p>This has a methyl group below it, an ethyl group to the right, and a more complicated hydrocarbon group to the left. Plus, of course, a hydrogen atom to make up the 4 bonds that have to be formed by the carbon. That means that it is attached to 4 different things, and so is a chiral centre.</p>
</div>

<h4>Introducing rings – further complications</h4>

<div class="text-block">
<p>At the time of writing, one of the UK-based exam boards (Cambridge International – CIE) commonly asked about the number of chiral centres in some very complicated molecules involving rings of carbon atoms. The rest of this page is to teach you how to cope with these.</p>
<p>We will start with a fairly simple ring compound:</p>
</div>

<div class="image-container">
<img src="ringskel1.gif">
</div>

<div class="text-block">
<p>When you are looking at rings like this, as far as optical isomerism is concerned, you don't need to look at any carbon in a double bond. You also don't need to look at any junction which only has two bonds going away from it. In that case, there must be 2 hydrogens attached, and so there can't possibly be 4 different groups attached.</p>
<p>In this case, that means that you only need to look at the carbon with the -OH group attached.</p>
<p>It has an -OH group, a hydrogen (to make up the total number of bonds to four), and links to two carbon atoms. How does the fact that these carbon atoms are part of a ring affect things?</p>
<p>You just need to trace back around the ring from both sides of the carbon you are looking at. Is the arrangement in both directions exactly the same? In this case, it isn't. Going in one direction, you come immediately to a carbon with a double bond. In the other direction, you meet two singly bonded carbon atoms, and then one with a double bond.</p>
<p>That means that you haven't got two identical hydrocarbon groups attached to the carbon you are interested in, and so it has 4 different groups in total around it. It is asymmetric – a chiral centre.</p>
<p>What about this near-relative of the last molecule?</p>
</div>

<div class="image-container">
<img src="ringskel2.gif">
</div>

<div class="text-block">
<p>In this case, everything is as before, except that if you trace around the ring clockwise and anticlockwise from the carbon at the bottom of the ring, there is an identical pattern in both directions. You can think of the bottom carbon being attached to a hydrogen, an -OH group, and two identical hydrocarbon groups.</p>
<p>It therefore isn't a chiral centre.</p>
<p>The other thing which is very noticeable about this molecule is that there is a plane of symmetry through the carbon atom we are interested in. If you chopped it in half through this carbon, one side of the molecule would be an exact reflection of the other. In the first ring molecule above, that isn't the case.</p>
<p>If you can see a plane of symmetry through the carbon atom it won't be a chiral centre. If there isn't a plane of symmetry, it will be a chiral centre.</p>
</div>

<h4>A seriously complicated example – cholesterol</h4>

<div class="text-block">
<p>The skeletal diagram shows the structure of cholesterol. Some of the carbon atoms have been numbered for discussion purposes below. These are not part of the normal system for numbering the carbon atoms in cholesterol.</p>
</div>

<div class="image-container">
<img src="cholesterol.gif">
</div>

<div class="text-block">
<p>Before you read on, look carefully at each of the numbered carbon atoms, and decide which of them are chiral centres. The other carbon atoms in the structure can't be chiral centres, because they are either parts of double bonds, or are joined to either two or three hydrogen atoms.</p>
</div>

<div class="note">
<p>Note: I am being deliberately unkind here! Normally when a molecule like cholesterol is discussed in this context, extra detail is often added to the skeletal structure. For example, important hydrogen atoms or methyl groups are drawn in. It is good for you to have to do it the hard way!</p>
</div>

<div class="text-block">
<p>So how many chiral centres did you find? In fact, there are 8 chiral centres out of the total of 9 carbons marked. If you didn't find all eight, go back and have another look before you read any further. It might help to sketch the structure on a piece of paper and draw in any missing hydrogens attached to the numbered carbons, and write in the methyl groups at the end of the branches as well.</p>
<p>This is done for you below, but it would be a lot better if you did it yourself and then checked your sketch afterwards.</p>
</div>

<div class="image-container">
<img src="cholesterol2.gif">
</div>

<div class="text-block">
<p>Starting with the easy one – it is obvious that carbon 9 has two methyl groups attached. It doesn't have 4 different groups, and so can't be chiral.</p>
<p>If you take a general look at the rest, it is fairly clear that none of them has a plane of symmetry through the numbered carbons. Therefore they are all likely to be chiral centres. But it's worth checking to see what is attached to each of them.</p>
<p>Carbon 1 has a hydrogen, an -OH and two different hydrocarbon chains (actually bits of rings) attached. Check clockwise and anticlockwise, and you will see that the arrangement isn't identical in each direction. Four different groups means a chiral centre.</p>
<p>Carbon 2 has a methyl and three other different hydrocarbon groups. If you check along all three bits of rings , they are all different – another chiral centre. This is also true of carbon 6.</p>
<p>Carbons 3, 4, 5 and 7 are all basically the same. Each is attached to a hydrogen and three different bits of rings. All of these are chiral centres.</p>
<p>Finally, carbon 8 has a hydrogen, a methyl group, and two different hydrocarbon groups attached. Again, this is a chiral centre.</p>
<p>This all looks difficult at first glance, but it isn't. You do, however, have to take a great deal of care in working through it – it is amazingly easy to miss one out.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-optisomerism.pdf" target="_blank">Questions on optical isomerism</a>
<a href="../questions/a-optisomerism.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../isomermenu.html#top">To the isomerism menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>