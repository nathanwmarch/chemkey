<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>E-Z Notation for Geometric Isomerism | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains the E-Z notation for naming geometric isomers.">
<meta name="keywords" content="organic, chemistry, isomers, isomerism, geometric isomers, geometric isomerism, E, Z, stereoisomers, stereoisomerism">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">
<h1>E-Z Notation for Geometric Isomerism</h1>

<div class="text-block">
<p>This page explains the E-Z system for naming geometric isomers.</p>
</div>

<div class="note">
<p>Important! If you have come straight here via a search engine, you should be aware that this page follows on from an <a href="geometric.html#top">introductory page</a> about geometric isomerism. Unless you are already confident about how geometric isomers arise, and the cis-trans system for naming them, you should follow this link first. You will find links back to this current page at suitable points on that page.</p>
</div>

<h2>The E-Z System</h2>

<h3>The Problem With the Cis-trans System for Naming Geometric Isomers</h3>

<div class="text-block">
<p>Consider a simple case of geometric isomerism which we've already discussed on the previous page.</p>
</div>

<div class="image-container">
<img src="diclethene.GIF">
</div>

<div class="text-block">
<p>You can tell which is the cis and which the trans form just by looking at them. All you really have to remember is that trans means "across" (as in transatlantic or transcontinental) and that cis is the opposite. It is a simple and visual way of telling the two isomers apart. So why do we need another system?</p>
<p>There are problems as compounds get more complicated. For example, could you name these two isomers using cis and trans?</p>
</div>

<div class="image-container">
<img src="ez1.gif">
</div>

<div class="text-block">
<p>Because everything attached to the carbon-carbon double bond is different, there aren't any obvious things which you can think of as being "cis" or "trans" to each other. The E-Z system gets around this problem completely – but unfortunately makes things slightly more difficult for the simple examples you usually meet in introductory courses.</p>
</div>

<h3>How the E-Z system works</h3>

<div class="text-block">
<p>We'll use the last two compounds as an example to explain how the system works.</p>
<p>You look at what is attached to each end of the double bond in turn, and give the two groups a "priority" according to a set of rules which we'll explore in a minute.</p>
<p>In the example above, at the left-hand end of the bond, it turns out that bromine has a higher priority than fluorine. And on the right-hand end, it turns out that chlorine has a higher priority than hydrogen.</p>
</div>

<div class="image-container">
<img src="ez2.gif">
</div>

<div class="text-block">
<p>If the two groups with the higher priorities are on the same side of the double bond, that is described as the (Z)- isomer. So you would write it as (Z)-name of compound. The symbol Z comes from a German word <i>zusammen</i> which means <i>together</i>.</p>
</div>

<div class="note">
Note: I'm not getting bogged down in the names of these more complex compounds. As soon as I put the proper full names in,
the whole thing suddenly looks much more complicated than it really is, and you will start to focus on where
the whole name comes from rather than on if it is a (Z)- or (E)- isomer.
</div>

<div class="text-block">
<p>If the two groups with the higher priorities are on opposite sides of the double bond, then this is the (E)- isomer. E comes from the German <i>entgegen</i> which means <i>opposite</i>.</p>
<p>So the two isomers are:</p>
</div>

<div class="image-container">
<img src="ez3.gif">
</div>

<h4>Summary</h4>

<ul>
<li>(E)- : the higher priority groups are on opposite sides of the double bond.</li>

<li>(Z)- : the higher priority groups are on the same side of the double bond.</li>
</ul>

<div class="note">
<p>Note: Three possible suggestions for remembering this:</p>
<ul>
<li>E is for "Enemies", which are on opposite sides.<br>You don't, of course, need a way of remembering the Z as well – it's just the other way around from E.</li>

<li>In Z isomers, the higher priority groups are on zee zame zide.That works best if you imagine you are an American speaking with a stage German accent!<br></li>
<li>This is the way I remembered it when I first came across E-Z notation. It is more visual than the other methods.<li>It relies on the fact that the shapes of E and Z isomers are the opposite of the shapes of the letters E and Z.<br>In the letter E, the horizontal strokes are all on the same side; in the E isomer, the higher priority groups are on opposite sides. In the letter Z, the horizontal strokes are on opposite sides; in the Z isomer, the groups are on the same side.</li>
</ul>
<p>Choose whichever of these methods make most sense to you.</p>
</div>

<h3>Rules for determining priorities</h3>

<div class="text-block">
<p>These are known as Cahn-Ingold-Prelog (CIP) rules after the people who developed the system.</p>
</div>

<h4>The first rule for very simple cases</h4>

<div class="text-block">
<p>You look first at the atoms attached directly to the carbon atoms at each end of the double bond – thinking about the two ends separately.</p>
</div>

<ul>
<li>The atom which has the higher atomic number is given the higher priority.</li>
</ul>

<div class="text-block">
<p>Let's look at the example we've been talking about.</p>
</div>

<div class="image-container">
<img src="ez1.gif">
</div>

<div class="text-block">
<p>Just consider the first isomer – and look separately at the left-hand and then the right-hand carbon atom. Compare the atomic numbers of the attached atoms to work out the various priorities.</p>
</div>

<div class="image-container">
<img src="ez4.gif">
</div>

<div class="text-block">
<p>Notice that the atoms with the higher priorities are both on the same side of the double bond. That counts as the (Z)- isomer.</p>
<p>The second isomer obviously still has the same atoms at each end, but this time the higher priority atoms are on opposite sides of the double bond. That's the (E)- isomer.</p>
<p>What about the more familiar examples like 1,2-dichloroethene or but-2-ene? Here's 1,2-dichloroethene.</p>
</div>

<div class="image-container">
<img src="ez5.gif">
</div>

<div class="text-block">
<p>Think about the priority of the two groups on the first carbon of the left-hand isomer.</p>
<p>Chlorine has a higher atomic number than hydrogen, and so has the higher priority. That, of course, is equally true of all the other carbon atoms in these two isomers.</p>
<p>In the first isomer, the higher priority groups are on opposite sides of the bond. That must be the (E)- isomer. The other one, with the higher priority groups on the same side, is the (Z)- isomer.</p>
</div>

<div class="image-container">
<img src="ez6.gif">
</div>

<div class="text-block">
<p>And now but-2-ene</p>
<p>This adds the slight complication that you haven't got a single atom attached to the double bond, but a group of atoms.</p>
<p>That isn't a problem. Concentrate on the atom directly attached to the double bond – in this case the carbon in the CH<sub>3</sub> group. For this simple case, you can ignore the hydrogen atoms in the CH<sub>3</sub> group entirely. However, with more complicated groups you may have to worry about atoms not directly attached to the double bond. We'll look at that problem in a moment.</p>
<p>Here is one of the isomers of but-2-ene:</p>
</div>

<div class="image-container">
<img src="ez7.gif">
</div>

<div class="text-block">
<p>The CH<sub>3</sub> group has the higher priority because its carbon atom has an atomic number of 6 compared with an atomic number of 1 for the hydrogen also attached to the carbon-carbon double bond.</p>
<p>The isomer drawn above has the two higher priority groups on opposite sides of the double bond. The compound is (E)-but-2-ene.</p>
<h4>A minor addition to the rule to allow for isotopes of, for example, hydrogen</h4>
<p>Deuterium is an isotope of hydrogen having a relative atomic mass of 2. It still has only 1 proton, and so still has an atomic number of 1. However, it isn't the same as an atom of "ordinary" hydrogen, and so these two compounds are geometric isomers:</p>
</div>

<div class="image-container">
<img src="ez8.gif">
</div>

<div class="text-block">
<p>The hydrogen and deuterium have the same atomic number – so on that basis, they would have the same priority. In a case like that, the one with the higher relative atomic mass has the higher priority. So in these isomers, the deuterium and chlorine are the higher priority groups on each end of the double bond.</p>
<p>That means that the left-hand isomer in the last diagram is the (E)- form, and the right-hand one the (Z)-.</p>
</div>

<h4>Extending the rules to more complicated molecules</h4>

<div class="text-block">
<p>If you are reading this because you are doing a course for 16 – 18 year olds such as UK A-level, you may well not need to know much about this section, but it really isn't very difficult!</p>
<p>Let's illustrate this by taking a fairly scary-looking molecule, and seeing how easy it is to find out whether it is a (Z)- or (E)- isomer by applying an extra rule.</p>
</div>

<div class="image-container">
<img src="ez9.gif">
</div>

<div class="text-block">
<p>Focus on the left-hand end of the molecule. What is attached directly to the carbon-carbon double bond?</p>
<p>In both of the attached groups, a carbon atom is attached directly to the bond. Those two atoms obviously have the same atomic number and therefore the same priority. So that doesn't help.</p>
<p>In this sort of case, you now look at what is attached directly to those two carbons (but without counting the carbon of the double bond) and compare the priorities of these next lot of atoms.</p>
<p>You can do this in your head in simple cases, but it is sometimes useful to write the attached atoms down, listing them with the highest priority atom first. It makes them easier to compare. Like this</p>
<p>In the CH<sub>3</sub> group:</p>
<p>The atoms attached to the carbon are H H H.</p>
<p>In the CH<sub>3</sub>CH<sub>2</sub> group:</p>
<p>The atoms attached directly to the carbon of the CH<sub>2</sub> group are C H H.</p>
<p>In the second list, the C is written first because it has the highest atomic number.</p>
<p>Now compare the two lists atom by atom. The first atom in each list is an H in the CH<sub>3</sub> group and a C in the CH<sub>3</sub>CH<sub>2</sub> group. The carbon has the higher priority because it has the higher atomic number. So that gives the CH<sub>3</sub>CH<sub>2</sub> group a higher priority than the CH<sub>3</sub> group.</p>
<p>Now look at the other end of the double bond. The extra thing that this illustrates is that if you have a double bond, you count the attached atom twice. Here is the structure again.</p>
</div>

<div class="image-container">
<img src="ez9.gif">
</div>

<div class="text-block">
<p>So, again, the atoms attached directly to the carbon-carbon double bond are both carbons. We therefore need to look at what is attached to those carbons.</p>
<p>In the CH<sub>2</sub>OH group:</p>
<p>The atoms attached directly to the carbon are O H H.</p>
<p>In the CHO group:</p>
<p>The atoms attached directly to the carbon are O O H. Remember that the oxygen is counted twice because of the carbon-oxygen double bond.</p>
<p>In both lists, the oxygens are written first because they have a higher atomic number than hydrogen.</p>
<p>So, what is the priority of the two groups? The first atom in both lists is an oxygen – that doesn't help. Look at the next atom in both lists. In the CH<sub>2</sub>OH group, that's a hydrogen; in the CHO list, it's an oxygen.</p>
<p>The oxygen has the higher priority – and that gives the CHO group a higher priority than the CH<sub>2</sub>OH group.</p>
<p>The isomer is therefore a (Z)- form, because the two higher priority groups (the CH<sub>3</sub>CH<sub>2</sub> group and the CHO group) are both on the same side of the bond.</p>
<p>That's been a fairly long-winded explanation just to make clear how it works. With a bit of practice, it takes a few seconds to work out in any but the most complex cases.</p>
<p>One more example to make a couple of additional minor points </p>
<p>Here's an even more complicated molecule!</p>
</div>

<div class="image-container">
<img src="ez10.gif">
</div>

<div class="text-block">
<p>Before you read on, have a go at working out the relative priorities of the two groups on the left-hand end of the double bond, and the two on the right-hand end. There's another bit of rule that I haven't specifically told you yet, but it isn't hard to guess what it might be when you start to look at the problem. If you can work this out, then you won't have any difficulty with any problem you are likely to come across at this level.</p>
<p>Look first at the left-hand groups.</p>
<p>In both the top and bottom groups, you have a CH<sub>2</sub> group attached directly to the carbon-carbon double bond, and the carbon in that CH<sub>2</sub> group is also attached to another carbon atom. In each case, the list will read C H H.</p>
<p>There is no difference between the priorities of those groups, so what are you going to do about it? The answer is to move out along the chain to the next group. And if necessary, continue to do this until you have found a difference.</p>
<p>Next along the chain at the top left of the molecule is another CH<sub>2</sub> group attached to a further carbon atom. The list for this group is again C H H.</p>
<p>But the next group along the chain at the bottom left is a CH group attached to two more carbon atoms. Its list is therefore C C H.</p>
<p>Comparing these lists atom by atom, leads you to the fact that the bottom group has the higher priority.</p>
<p>Now look at the right-hand groups. Here is the molecule again:</p>
</div>

<div class="image-container">
<img src="ez10.gif">
</div>

<div class="text-block">
<p>The top right group has C H H attached to the first carbon in the chain.</p>
<p>The bottom right one has Cl H H.</p>
<p>The chlorine has a higher atomic number than carbon, and so the bottom right group has the higher priority of these two groups.</p>
<p>The extra point I am trying to make with this bit of the example is that you must just focus on one bit of a chain at a time. We never get around to considering the bromine at the extreme top right of the molecule. We don't need to go out that far along the chain – you work out one link at a time until you find a difference. Anything beyond that is irrelevant.</p>
<p>For the record, this molecule is a (Z)- isomer because the higher priority groups at each end are on the same side of the double bond.</p>
</div>

<h3>Can You Easily Translate Cis- and Trans- Into (Z)- and (E)-?</h3>

<div class="text-block">
<p>You might think that for simple cases, cis- will just convert into (Z)- and trans- into (E)-.</p>
<p>Look for example at the 1,2-dichloroethene and but-2-ene cases.</p>
</div>

<div class="image-container">
<img src="ez11.gif">
</div>

<div class="text-block">
<p>But it doesn't always work! Think about this relatively uncomplicated molecule</p>
</div>

<div class="image-container">
<img src="ez12.gif">
</div>

<div class="text-block">
<p>This is clearly a cis- isomer. It has two CH<sub>3</sub> groups on the same side of the double bond. But work out the priorities on the right-hand end of the double bond.</p>
<p>The two directly attached atoms are carbon and bromine. Bromine has the higher atomic number and so has the higher priority on that end. At the other end, the CH<sub>3</sub> group has the higher priority.</p>
<p>That means that the two higher priority groups are on opposite sides of the double bond, and so this is an (E)- isomer – NOT a (Z)-.</p>
<p>Never assume that you can convert directly from one of these systems into the other. The only safe thing to do is to start from scratch in each case.</p>
<p>Does it matter that the two systems will sometimes give different results? No! The purpose of both systems is to enable you to decode a name and write a correct formula. Properly used, both systems will do this for you – although the cis-trans system will only work for very straightforward molecules.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-eznotation.pdf" target="_blank">Questions on E/Z notation</a>
<a href="../questions/a-eznotation.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../isomermenu.html#top">To the isomerism menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>