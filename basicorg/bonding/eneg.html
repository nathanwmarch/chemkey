<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Electronegativity – Polar Bonds in Organic Compounds | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of how electronegativity arises, and the way it produces polar bonds in organic compounds">
<meta name="keywords" content=" electronegativity, electronegative, polarity, polar bond, inductive effect, organic">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Electronegativity – Polar Bonds in Organic Compounds</h1>

<div class="text-block">
<p>This page deals with electronegativity in an organic chemistry context. If you want a wider view of electronegativity, there is a link at the bottom of the page.</p>
</div>

<h2>What is Electronegativity?</h2>

<div class="text-block">
<p>Electronegativity is a measure of the tendency of an atom to attract a bonding pair of electrons. The Pauling scale is the most commonly used. Fluorine (the most electronegative element) is given a value of 4.0, and values range down to caesium and francium which are the least electronegative at 0.7.</p>
</div>

<h3>What Happens If Two Atoms of Equal Electronegativity Bond Together?</h3>

<div class="text-block">
<p>The most obvious example of this is the bond between two carbon atoms. Both atoms will attract the bonding pair to exactly the same extent. That means that on average the electron pair will be found half way between the two nuclei, and you could draw a picture of the bond like this:</p>
</div>

<div class="image-container">
<img src="cc.GIF">
</div>

<div class="text-block">
<p>It is important to realise that this is an average picture. The electrons are actually in a sigma orbital, and are moving constantly within that orbital.</p>
</div>

<div class="note">
<p>Help! A sigma orbital is a molecular orbital formed by end-to-end overlap between two atomic orbitals. If you aren't happy about this, read the articles on <a href="orbitals.html#top">orbitals</a> and the <a href="methane.html#top">bonding in methane and ethane</a>.</p>
</div>

<h3>The Carbon-fluorine Bond</h3>

<div class="text-block">
<p>Fluorine is much more electronegative than carbon. The actual values on the Pauling scale are</p>
</div>
<table class="list-table">
<thead>
<tr><th></th><th>electronegativity</th></tr>
</thead>
<tbody>
<tr><td>carbon</td><td>2.5</td></tr>
<tr><td>fluorine</td><td>4.0</td></tr>
</tbody>
</table>

<div class="text-block">
<p>That means that fluorine attracts the bonding pair much more strongly than carbon does. The bond – on average – will look like this:</p>
</div>

<div class="image-container">
<img src="cf.GIF">
</div>

<div class="text-block">
<h4>Why is fluorine more electronegative than carbon?</h4>
<p>A simple dots-and-crosses diagram of a C-F bond is perfectly adequate to explain it.</p>
</div>

<div class="image-container">
<img src="cfdandc.GIF">
</div>

<div class="text-block">
<p>The bonding pair is in the second energy level of both carbon and fluorine, so in the absence of any other effect, the distance of the pair from both nuclei would be the same.</p>
<p>The electron pair is shielded from the full force of both nuclei by the 1s electrons – again there is nothing to pull it closer to one atom than the other.</p>
<p><strong>But</strong>, the fluorine nucleus has 9 protons whereas the carbon nucleus has only 6.</p>
<p>Allowing for the shielding effect of the 1s electrons, the bonding pair feels a net pull of about 4+ from the carbon, but about 7+ from the fluorine. It is this extra nuclear charge which pulls the bonding pair (on average) closer to the fluorine than the carbon.</p>
</div>

<div class="note">
<p>Help! You have to imagine what the bonding pair "sees" if it looks in towards the nucleus. In the carbon case, it sees 6 positive protons, and 2 negative electrons. That means that there will be a net pull from the carbon of about 4+. The shielding wouldn't actually be quite as high as 2-, because the 1s electrons spend some of their time on the far side of the carbon nucleus – and so aren't always between the bonding pair and the nucleus. Incidentally, thinking about electrons looking towards the nucleus may be helpful in picturing what is going on, but avoid using terms like this in exams.</p>
</div>

<h3>The Carbon-chlorine Bond</h3>

<div class="text-block">
<p>The electronegativities are:</p>
</div>

<table class="list-table">
<thead>
<tr><th></th><th>electronegativity</th></tr>
</thead>
<tbody>
<tr><td>carbon</td><td>2.5</td></tr>
<tr><td>chlorine</td><td>3.0</td></tr>
</tbody>
</table>

<div class="text-block">
<p>The bonding pair of electrons will be dragged towards the chlorine but not as much as in the fluorine case. Chlorine isn't as electronegative as fluorine.</p>
</div>

<h4>Why isn't chlorine as electronegative as fluorine?</h4>

<div class="text-block">
<p>Chlorine is a bigger atom than fluorine.</p>
</div>

<table class="list-table">
<tr><td>fluorine</td><td><span class="inline-formula">1s^22s^22p_x^22p_y^22p_z^1</span></td></tr>
<tr><td>chlorine</td><td><span class="inline-formula">1s^22s^22p_x^22p_y^22p_z^23s^23p_x^23p_y^23p_z^1</span></td></tr>
</table>

<div class="note">
<p>Help! If you aren't happy about this, read the article on <a href="orbitals.html#top">orbitals</a>.</p>
</div>

<div class="text-block">
<p>In the chlorine case, the bonding pair will be shielded by all the 1-level and 2-level electrons. The 17 protons on the nucleus will be shielded by a total of 10 electrons, giving a net pull from the chlorine of about 7+.</p>
<p>That is the same as the pull from the fluorine, but with chlorine the bonding pair starts off further away from the nucleus because it is in the 3-level. Since it is further away, it feels the pull from the nucleus less strongly.</p>
</div>

<h2>Bond Polarity and Inductive Effects</h2>

<h3>Polar Bonds</h3>

<div class="text-block">
<p>Think about the carbon-fluorine bond again. Because the bonding pair is pulled towards the fluorine end of the bond, that end is left rather more negative than it would otherwise be. The carbon end is left rather short of electrons and so becomes slightly positive.</p>
</div>

<div class="image-container">
<img src="cfpolar.GIF">
</div>

<div class="text-block">
<p>The symbols &delta;+ and &delta;- mean "slightly positive" and "slightly negative". You read &delta;+ as "delta plus" or "delta positive".</p>
<p>We describe a bond having one end slightly positive and the other end slightly negative as being polar.</p>
</div>

<h3>Inductive effects</h3>

<div class="text-block">
<p>An atom like fluorine which can pull the bonding pair away from the atom it is attached to is said to have a negative inductive effect.</p>
<p>Most atoms that you will come across have a negative inductive effect when they are attached to a carbon atom, because they are mostly more electronegative than carbon.</p>
<p>You will come across some groups of atoms which have a slight positive inductive effect – they "push" electrons towards the carbon they are attached to, making it slightly negative.</p>
<p>Inductive effects are sometimes given symbols: -I (a negative inductive effect) and +I (a positive inductive effect).</p>
</div>

<div class="note">
<p>Note: You should be aware of terms like "negative inductive effect", but don't get bogged down in them. Provided that you understand what happens when electronegative atoms like fluorine or chlorine are attached to carbon atoms in terms of the polarity of the bonds, that's really all you need for most purposes.</p>
</div>

<h2>Some Important Examples of Polar Bonds</h2>

<h3>Hydrogen bromide (and Other Hydrogen halide)</h3>

<div class="image-container">
<img src="hbrpolar.GIF">
</div>

<div class="text-block">
<p>Bromine (and the other halogens) are all more electronegative than hydrogen, and so all the hydrogen halides have polar bonds with the hydrogen end slightly positive and the halogen end slightly negative.</p>
</div>

<div class="definition">
<p>Halogen: a member of group VII of the Periodic Table – fluorine, chlorine, bromine and iodine.</p>
<p>Halide: a compound of one of these – e.g. hydrogen chloride, hydrogen bromide, etc.</p>
</div>

<div class="text-block">
<p>The polarity of these molecules is important in their reactions with alkenes.</p>
</div>

<div class="note">
<p>Note: These reactions are explored in the section dealing with <a href="../../mechanisms/eladd/symhbr.html#top">the addition of hydrogen halides to alkenes</a>.</p>
</div>

<h3>The Carbon-bromine Bond in Halogenoalkanes</h3>

<div class="note">
<p>Note: You may come across halogenoalkanes under the names "haloalkanes" or "alkyl halides".</p>
</div>

<div class="text-block">
<p>Bromine is more electronegative than carbon and so the bond is polarised in the way that we have already described with C-F and C-Cl.</p>
</div>

<div class="image-container">
<img src="cbrpolar.GIF">
</div>

<div class="text-block">
<p>The polarity of the carbon-halogen bonds is important in the reactions of the halogenoalkanes.</p>
</div>

<div class="note">
<p>Note: This link will take you to the <a href="../../mechanisms/nucsubmenu.html#top">nucleophilic substitution reactions of the halogenoalkanes</a> in which this polarity is important.</p>
</div>

<h3>The Carbon-oxygen Double Bond</h3>

<div class="text-block">
<p>An orbital model of the C=O bond in methanal, HCHO, looks like this:</p>
</div>

<div class="image-container">
<img src="methanalpi2.GIF">
</div>

<div class="note">
<p>Note: If you aren't sure about this, read the article on <a href="carbonyl.html#top">bonding in the carbonyl group (C=O)</a>.</p>
</div>

<div class="text-block">
<p>The very electronegative oxygen atom pulls both bonding pairs towards itself – in the sigma bond and the &pi; bond. That leaves the oxygen fairly negative and the carbon fairly positive.</p>
</div>

<div class="image-container">
<img src="copolar.GIF">
</div>

<div class="note">
<p>Note: You can read about
<a href="../../mechanisms/nucaddmenu.html#top">addition reactions</a> or <a href="../../mechanisms/addelimmenu.html#top">addition-elimination reactions</a> of compounds containing carbon-oxygen double bonds elswhere on this site.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-electronegorg.pdf" target="_blank">Questions on electronegativity in organic chemistry</a>
<a href="../questions/a-electronegorg.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../../atoms/bonding/electroneg.html#top">To look at electronegativity in a wider context</a>
<a href="../bondmenu.html#top">To the organic bonding menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>