<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Bonding in Methane – sp3 hybridisation | ChemKey</title>
<meta charset="UTF-8">

<meta name="description" content="An explanation of the bonding in methane and ethane, including a simple view of hybridisation">
<meta name="keywords" content="methane, ethane, bonding, sp3 hybrids, sp3 hybridisation, sigma bond">
<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Bonding in Methane and Ethane</h1>

<div class="text-block">
<p>Warning! If you aren't happy with describing electron arrangements in s and p notation, and with the shapes of s and p orbitals, you really should read about <a href="orbitals.html#top">orbitals</a>.</p>
</div>

<h2>Methane, CH<sub>4</sub></h2>

<h3>The Simple View of the Bonding in Methane</h3>

<div class="image-container">
<img src="ch4gcse.GIF" align="right" hspace="20">
</div>

<div class="text-block">
<p>You will be familiar with drawing methane using dots and crosses diagrams, but it is worth looking at its structure a bit more closely.</p>
<p>There is a serious mis-match between this structure and the modern electronic structure of carbon, 1s<sup>2</sup>2s<sup>2</sup>2p<sub>x</sub><sup>1</sup>2p<sub>y</sub><sup>1</sup>. The modern structure shows that there are only 2 unpaired electrons to share with hydrogens, instead of the 4 which the simple view requires.</p>
</div>

<div class="image-container">
<img src="cground.GIF" align="left" hspace="20" vspace="12">
</div>

<div class="text-block">
<p>You can see this more readily using the electrons-in-boxes notation. Only the 2-level electrons are shown. The 1s<sup>2</sup> electrons are too deep inside the atom to be involved in bonding. The only electrons directly available for sharing are the 2p electrons. Why then isn't methane CH<sub>2</sub>?</p>
</div>

<h3>Promotion of an Electron</h3>

<div class="image-container">
<img src="cpromote.GIF" align="left" hspace="20" vspace="12">
</div>

<div class="text-block">
<p>When bonds are formed, energy is released and the system becomes more stable. If carbon forms 4 bonds rather than 2, twice as much energy is released and so the resulting molecule becomes even more stable.</p>

<p>There is only a small energy gap between the 2s and 2p orbitals, and so it pays the carbon to provide a small amount of energy to promote an electron from the 2s to the empty 2p to give 4 unpaired electrons. The extra energy released when the bonds form more than compensates for the initial input.</p>

<p>The carbon atom is now said to be in an excited state.</p>
</div>

<div class="note">
<p>Note: People sometimes worry that the promoted electron is drawn as an up-arrow, whereas it started as a down-arrow. The reason for this is actually fairly complicated – well beyond the level we are working at. Just get in the habit of writing it like this because it makes the diagrams look tidy!</p>
</div>

<div class="text-block">
<p>Now that we've got 4 unpaired electrons ready for bonding, another problem arises. In methane all the carbon-hydrogen bonds are identical, but our electrons are in two different kinds of orbitals. You aren't going to get four identical bonds unless you start from four identical orbitals.</p>
</div>

<h3>Hybridisation</h3>

<div class="image-container">
<img src="sp3boxes.GIF" align="left" hspace="20" vspace="25">
</div>

<div class="text-block">
<p>The electrons rearrange themselves again in a process called hybridisation. This reorganises the electrons into four identical hybrid orbitals called sp<sup>3</sup> hybrids (because they are made from one s orbital and three p orbitals). You should read "sp<sup>3</sup>" as "s p three" – not as "s p cubed".</p>
</div>

<div class="image-container">
<img src="sp3orbitals.GIF" align="right" hspace="20">
</div>

<div class="text-block">
<p>sp<sup>3</sup> hybrid orbitals look a bit like half a p orbital, and they arrange themselves in space so that they are as far apart as possible. You can picture the nucleus as being at the centre of a tetrahedron (a triangularly based pyramid) with the orbitals pointing to the corners. For clarity, the nucleus is drawn far larger than it really is.</p>
</div>

<h4>What happens when the bonds are formed?</h4>

<div class="text-block">
<p>Remember that hydrogen's electron is in a 1s orbital – a spherically symmetric region of space surrounding the nucleus where there is some fixed chance (say 95%) of finding the electron. When a covalent bond is formed, the atomic orbitals (the orbitals in the individual atoms) merge to produce a new molecular orbital which contains the electron pair which creates the bond.</p>
</div>

<div class="image-container">
<img src="ch4orbitals.GIF">
</div>

<div class="text-block">
<p>Four molecular orbitals are formed, looking rather like the original sp<sup>3</sup> hybrids, but with a hydrogen nucleus embedded in each lobe. Each orbital holds the 2 electrons that we've previously drawn as a dot and a cross.</p>

<p>The principles involved – promotion of electrons if necessary, then hybridisation, followed by the formation of molecular orbitals – can be applied to any covalently-bound molecule.</p>
</div>

<h3>The Shape of Methane</h3>

<div class="text-block">
<p>When sp<sup>3</sup> orbitals are formed, they arrange themselves so that they are as far apart as possible. That is a tetrahedral arrangement, with an angle of 109.5°.</p>

<p>Nothing changes in terms of the shape when the hydrogen atoms combine with the carbon, and so the methane molecule is also tetrahedral with 109.5° bond angles.</p>
</div>

<h2>Ethane, C<sub>2</sub>H<sub>6</sub></h2>

<h3>The Formation of Molecular Orbitals in Ethane</h3>

<div class="text-block">
<p>Ethane isn't particularly important in its own right, but is included because it is a simple example of how a carbon-carbon single bond is formed.</p>
<p>Each carbon atom in the ethane promotes an electron and then forms sp<sup>3 </sup>hybrids exactly as we've described in methane. So just before bonding, the atoms look like this:</p>
</div>

<div class="image-container">
<img src="ethanebits.GIF">
</div>

<div class="text-block">
<p>The hydrogens bond with the two carbons to produce molecular orbitals just as they did with methane. The two carbon atoms bond by merging their remaining sp<sup>3</sup> hybrid orbitals end-to-end to make a new molecular orbital. The bond formed by this end-to-end overlap is called a sigma bond. The bonds between the carbons and hydrogens are also sigma bonds.</p>
</div>

<div class="image-container">
<img src="ethaneorbits.GIF">
</div>

<div class="text-block">
<p>In any sigma bond, the most likely place to find the pair of electrons is on a line between the two nuclei.</p>
</div>

<h3>The Shape of Ethane Around Each Carbon Atom</h3>

<div class="text-block">
<p>The shape is again determined by the way the sp<sup>3</sup> orbitals are arranged around each carbon atom. That is a tetrahedral arrangement, with an angle of 109.5°.</p>

<p>When the ethane molecule is put together, the arrangement around each carbon atom is again tetrahedral with approximately 109.5° bond angles. Why only "approximately"? This time, each carbon atoms doesn't have four identical things attached. There will be a small amount of distortion because of the attachment of 3 hydrogens and 1 carbon, rather than 4 hydrogens.</p>
</div>

<h3>Free Rotation About the Carbon-carbon Single Bond</h3>

<div class="text-block">
<p>The two ends of this molecule can spin quite freely about the sigma bond so that there are, in a sense, an infinite number of possibilities for the shape of an ethane molecule. Some possible shapes are:</p>
</div>

<div class="image-container">
<img src="ethanemodels.GIF">
</div>

<div class="text-block">
<p>In each case, the left hand CH<sub>3</sub> group has been kept in a constant position so that you can see the effect of spinning the right hand one.</p>
</div>

<h3>Other Alkanes</h3>

<div class="text-block">
<p>All other alkanes will be bonded in the same way:</p>
</div>

<ul>
<li>The carbon atoms will each promote an electron and then hybridise to give sp<sup>3</sup> hybrid orbitals.</li>
<li>The carbon atoms will join to each other by forming sigma bonds by the end-to-end overlap of their sp<sup>3</sup> hybrid orbitals.</li>
<li>Hydrogen atoms will join on wherever they are needed by overlapping their 1s<sup>1</sup> orbitals with sp<sup>3</sup> hybrid orbitals on the carbon atoms.</li>
</ul>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-methane.pdf" target="_blank">Questions on bonding in methane and ethane</a>
<a href="../questions/a-methane.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../bondmenu.html#top">To the organic bonding menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>