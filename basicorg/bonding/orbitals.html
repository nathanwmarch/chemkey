<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Electronic Structure and Atomic Orbitals | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="Explains how electrons are arranged in atomic orbitals, leading to the modern electronic structures of carbon and hydrogen">
<meta name="keywords" content="electronic structure, atomic orbital, orbital, orbitals, electron, electrons, 1s, 2s, 2p">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">

<h1>Electronic Structure and Atomic Orbitals</h1>

<h2>A Simple View</h2>

<div class="text-block">
<p>In any introductory chemistry course you will have come across the electronic structures of hydrogen and carbon drawn as:</p>
</div>

<div class="image-container">
<img src="handc.GIF">
</div>

<div class="note">
<p>Note: There are many places where you could still make use of this model of the atom at A- level. It is, however, a simplification and can be misleading. It gives the impression that the electrons are circling the nucleus in orbits like planets around the sun. As you will see in a moment, it is impossible to know exactly how they are actually moving.</p>
</div>

<div class="text-block">
<p>The circles show energy levels – representing increasing distances from the nucleus. You could straighten the circles out and draw the electronic structure as a simple energy diagram.</p>
</div>

<div class="image-container">
<img src="cenlev.GIF">
</div>

<h2>Atomic Orbitals</h2>

<div class="text-block">
<p>Orbits and orbitals sound similar, but they have quite different meanings. It is essential that you understand the difference between them.</p>
</div>

<h3>The Impossibility of Drawing Orbits for Electrons</h3>

<div class="text-block">
<p>To plot a path for something you need to know exactly where the object is and be able to work out exactly where it's going to be an instant later. You can't do this for electrons.</p>
</div>

<div class="note">
<p>Note: In order to plot a plane's course, it is no use knowing its exact location in mid-Atlantic if you don't know its direction or speed. Equally it's no use knowing that it is travelling at 500 mph due west if you have no idea whether it is near Iceland or the Azores at that particular moment.</p>
</div>

<div class="text-block">
<p>The Heisenberg Uncertainty Principle (not required at A-level) says – loosely – that you can't know with certainty both where an electron is and where it's going next. That makes it impossible to plot an orbit for an electron around a nucleus. Is this a big problem? No. If something is impossible, you have to accept it and find a way around it.</p>
</div>

<h3>Hydrogen's Electron – the 1s Orbital</h3>

<div class="note">
<p>Note: In this diagram (and the orbital diagrams that follow), the nucleus is shown very much larger than it really is. This is just for clarity.</p>
</div>

<div class="image-container">
<img src="1sorbital.GIF">
</div>

<div class="text-block">
<p>Suppose you had a single hydrogen atom and at a particular instant plotted the position of the one electron. Soon afterwards, you do the same thing, and find that it is in a new position. You have no idea how it got from the first place to the second.</p>
<p>You keep on doing this over and over again, and gradually build up a sort of 3D map of the places that the electron is likely to be found.</p>
<p>In the hydrogen case, the electron can be found anywhere within a spherical space surrounding the nucleus. The diagram shows a cross-section through this spherical space.</p>
<p>95% of the time (or any other percentage you choose), the electron will be found within a fairly easily defined region of space quite close to the nucleus. Such a region of space is called an orbital. You can think of an orbital as being the region of space in which the electron lives.</p>
</div>

<div class="note">
<p>Note: If you wanted to be absolutely 100% sure of where the electron is, you would have to draw an orbital the size of the Universe!</p>
</div>

<div class="text-block">
<p>What is the electron doing in the orbital? We don't know, we can't know, and so we just ignore the problem! All you can say is that if an electron is in a particular orbital it will have a particular definable energy.</p>
<p>Each orbital has a name.</p>
<p>The orbital occupied by the hydrogen electron is called a 1s orbital. The "1" represents the fact that the orbital is in the energy level closest to the nucleus. The "s" tells you about the shape of the orbital. s orbitals are spherically symmetric around the nucleus – in each case, like a hollow ball made of rather chunky material with the nucleus at its centre.</p>
</div>

<div class="image-container">
<img src="2sorbital.GIF">
</div>

<div class="text-block">
<p>The orbital on the left is a 2s orbital. This is similar to a 1s orbital except that the region where there
is the greatest chance of finding the electron is further from the nucleus – this is an orbital at the second
energy level.</p>
<p>If you look carefully, you will notice that there is another region of slightly higher electron density (where the dots are thicker) nearer the nucleus. ("Electron density" is another way of talking about how likely you are to find an electron at a particular place.)</p>
<p>2s (and 3s, 4s, etc) electrons spend some of their time closer to the nucleus than you might expect. The effect of this is to slightly reduce the energy of electrons in s orbitals. The nearer the nucleus the electrons get, the lower their energy.</p>
<p>3s, 4s (etc) orbitals get progressively further from the nucleus.</p>
</div>

<h3>p Orbitals</h3>

<div class="image-container">
<img src="porbital.GIF">
</div>

<div class="text-block">
<p>Not all electrons inhabit s orbitals (in fact, very few electrons live in s orbitals). At the first energy level, the only orbital available to electrons is the 1s orbital, but at the second level, as well as a 2s orbital, there are also orbitals called 2p orbitals.</p>
<p>A p orbital is rather like 2 identical balloons tied together at the nucleus. The diagram on the right is a cross-section through that 3-dimensional region of space. Once again, the orbital shows where there is a 95% chance of finding a particular electron.</p>
</div>

<div class="note">
<p>Beyond A-level: If you imagine a horizontal plane through the nucleus, with one lobe of the orbital above the plane and the other beneath it, there is a zero probability of finding the electron on that plane. So how does the electron get from one lobe to the other if it can never pass through the plane of the nucleus? For A-level chemistry you just have to accept that it does! If you want to find out more, read about the wave nature of electrons.</p>
</div>

<div class="text-block">
<p>Unlike an s orbital, a p orbital points in a particular direction – the one drawn points up and down the page.</p>
<p>At any one energy level it is possible to have three absolutely equivalent p orbitals pointing mutually at right angles to each other. These are arbitrarily given the symbols p<sub>x</sub>, p<sub>y</sub> and p<sub>z</sub>. This is simply for convenience – what you might think of as the x, y or z direction changes constantly as the atom tumbles in space.</p>
</div>

<div class="image-container">
<img src="pxyandz.GIF">
</div>

<div class="text-block">
<p>The p orbitals at the second energy level are called 2p<sub>x</sub>, 2p<sub>y</sub> and 2p<sub>z</sub>. There are similar orbitals at subsequent levels – 3p<sub>x</sub>, 3p<sub>y</sub>, 3p<sub>z</sub>, 4p<sub>x</sub>, 4p<sub>y</sub>, 4p<sub>z</sub> and so on.</p>
<p>All levels except for the first level have p orbitals. At the higher levels the lobes get more elongated, with the most likely place to find the electron more distant from the nucleus.</p>
</div>

<h2>Fitting Electrons into Orbitals</h2>

<div class="text-block">
<p>Because for the moment we are only interested in the electronic structures of hydrogen and carbon, we don't need to concern ourselves with what happens beyond the second energy level.</p>
<p>Remember:
<p>At the first level there is only one orbital – the 1s orbital.</p>
<p>At the second level there are four orbitals – the 2s, 2p<sub>x</sub>, 2p<sub>y</sub> and 2p<sub>z</sub> orbitals.</p>
<p>Each orbital can hold either 1 or 2 electrons, but no more.</p>
</div>

<h3>"Electrons-in-boxes"</h3>

<div class="text-block">
<p>Orbitals can be represented as boxes with the electrons in them shown as arrows. Often an up-arrow and a down-arrow are used to show that the electrons are in some way different.</p>
</div>

<div class="note">
<p>Beyond A-level: The need to have all electrons in an atom different comes out of quantum theory. If they live in different orbitals, that's fine – but if they are both in the same orbital there has to be some subtle distinction between them. Quantum theory allocates them a property known as "spin" – which is what the arrows are intended to suggest.</p>
</div>

<div class="image-container">
<img src="1s2.GIF">
</div>

<div class="text-block">
<p>A 1s orbital holding 2 electrons would be drawn as shown on the right, but it can be written even more quickly as 1s<sup>2</sup>. This is read as "one s two" – not as "one s squared".</p>
<p>You mustn't confuse the two numbers in this notation:</p>
</div>

<div class="image-container">
<img src="explain1s2.GIF">
</div>

<h3>The order of filling orbitals</h3>

<div class="text-block">
<p>Electrons fill low energy orbitals (closer to the nucleus) before they fill higher energy ones. Where there is a choice between orbitals of equal energy, they fill the orbitals singly as far as possible.</p>
<p>The diagram (not to scale) summarises the energies of the various orbitals in the first and second levels.</p>
</div>

<div class="image-container">
<img src="orbenergy.GIF">
</div>

<div class="text-block">
<p>Notice that the 2s orbital has a slightly lower energy than the 2p orbitals. That means that the 2s orbital will fill with electrons before the 2p orbitals. All the 2p orbitals have exactly the same energy.</p>
</div>

<h2>The Electronic Structure of Hydrogen</h2>

<div class="text-block">
<p>Hydrogen only has one electron and that will go into the orbital with the lowest energy – the 1s orbital.</p>
<p>Hydrogen has an electronic structure of 1s<sup>1</sup>. We have already described this orbital earlier.</p>
</div>

<h2>The Electronic Structure of Carbon</h2>

<div class="text-block">
<p>Carbon has six electrons. Two of them will be found in the 1s orbital close to the nucleus. The next two will go into the 2s orbital. The remaining ones will be in two separate 2p orbitals. This is because the p orbitals all have the same energy and the electrons prefer to be on their own if that's the case.</p>
</div>

<div class="image-container">
<img src="corbenergy.GIF">
</div>

<div class="note">
<p>Note: People sometimes wonder why the electrons choose to go into the 2p<sub>x</sub> and 2p<sub>y</sub> orbitals rather than the 2p<sub>z</sub>. They don't! All of the 2p orbitals are exactly equivalent, and the names we give them are entirely arbitrary.
It just looks tidier if we call the orbitals the electrons occupy the 2p<sub>x</sub> and 2p<sub>y</sub>.</p>
</div>

<div class="text-block">
<p>The electronic structure of carbon is normally written 1s<sup>2</sup>2s<sup>2</sup>2p<sub>x</sub><sup>1</sup>2p<sub>y</sub><sup>1</sup>.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-orbitals.pdf" target="_blank">Questions on orbitals</a>
<a href="../questions/a-orbitals.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../bondmenu.html#top">To the organic bonding menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>