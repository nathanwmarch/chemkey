<!DOCTYPE html>
<html>

<head>
<meta name=viewport content="width=device-width, initial-scale=0.7">
<title>Bonding in Carbonyl Compounds – The Carbon-oxygen Double Bond | ChemKey</title>
<meta charset="UTF-8">
<meta name="description" content="An explanation of the bonding in carbonyl compounds (containing carbon-oxygen double bonds), including a simple view of hybridisation.">
<meta name="keywords" content=" carbonyl, aldehydes, ketones, bonding, sp2 hybrids, sp2 hybridisation, &pi; bond">

<link rel="stylesheet" type="text/css" href="https://shout.education/ChemKey/chemguide.css">
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/AccessibilityFeatures/FontSizeController/fontSizeController.js"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/ChemDoodleWeb.js">
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/ChemDoodleWebComponents/chemDoodleWebComponents.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.css" integrity="sha384-8QOKbPtTFvh/lMY0qPVbXj9hDh+v8US0pD//FcoYFst2lCIf0BmT58+Heqj0IGyx"
crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ"
crossorigin="anonymous"></script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/KaTeX/KaTeXer.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109203606-1"></script>
<script>window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-109203606-1');
</script>
<script type="text/javascript" src="https://shout.education/ChemKey/Libraries/QuickNavigation/navigation.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://shout.education/ChemKey/Libraries/GoogleCharts/googleTable.js"></script>
<script src="https://shout.education/ChemKey/Libraries/Sequences/sequences.js" type="text/javascript"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a84906d4b5cb20013b18487&product=sticky-share-buttons"></script>
</head>

<body>
<a name="top"></a>
<div id="top-bar">
<div class="help">Need help? Email <a href="mailto:help@shout.education?subject=Help with ChemKey">help@shout.education</a></div>
<div id="translate">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-109203606-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div id="search">
<script>(function () { var cx = '004986493609168734102:owk3bhie1ts'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();</script>
<gcse:search></gcse:search>
</div>
</div>

<div class="content">
<h1>Bonding in Carbonyl Compounds – The Carbon-oxygen Double Bond</h1>

<div class="note">
<p>Important! Don't try to read this unless you are sure that you fully understand the orbital view of <a href="methane.html#top">the bonding in methane</a> and <a href="ethene.html#top">the bonding in ethene</a>. You may also find it useful to read the article on <a href="orbitals.html#top">orbitals</a> if you aren't sure about simple orbital theory.</p>
</div>

<h2>The Carbonyl Group</h2>

<h3>The Simple View of the Bonding in Carbon-oxygen Double Bonds</h3>

<div class="chemical-structure" name="methanal" type="2d"></div>

<div class="text-block">
<p>Where the carbon-oxygen double bond, C=O, occurs in organic compounds it is called a carbonyl group. The simplest compound containing this group is methanal.</p>
<p>We are going to look at the bonding in methanal, but it would equally apply to any other compound containing C=O. The interesting thing is the nature of the carbon-oxygen double bond – not what it's attached to.</p>
</div>

<div class="note">
<p>Note: Methanal is normally written as HCHO. If you wrote it as HCOH, it looks as if it contains an -O-H group – and it doesn't. Methanal is an aldehyde. All aldehydes contain the CHO group. Naming: methanal: meth counts 1 carbon atom, an means no C=C, al says that it is an aldehyde and so contains CHO.</p>
</div>

<h3>An Orbital View of the Bonding in Carbon-oxygen Double Bonds</h3>

<h4>The carbon atom</h4>

<div class="text-block">
<p>Just as in ethene or benzene, the carbon atom is joined to three other atoms. The carbon's electrons rearrange themselves, and promotion and hybridisation give sp<sup>2</sup> hybrid orbitals.</p>

<p>Promotion gives:</p>
</div>

<div class="image-container">
<img src="promoteline.GIF">
</div>
<div class="text-block">
<p>Hybridisation of the 2s orbital and two of the 2p orbitals means that the carbon atom now looks like the diagram on the right.</p>
</div>

<div class="image-container">
<img src="sp2.GIF">
</div>

<div class="text-block">
<p>Three sp<sup>2</sup> hybrid orbitals are formed and these arrange themselves as far apart in space as they can – at 120° to each other. The remaining p orbital is at right angles to them.</p>
<p>This is exactly the same as in ethene or in benzene.</p>
</div>

<div class="note">
<p>Important! If this isn't really clear to you, you must go and read the article about <a href="ethene.html#top">the bonding in ethene</a>.</p>
</div>

<h4>The oxygen atom</h4>

<div class="text-block">
<p>Oxygen's electronic structure is 1s<sup>2</sup>2s<sup>2</sup>2p<sub>x</sub><sup>2</sup>2p<sub>y</sub><sup>1</sup>2p<sub>z</sub><sup>1</sup>.</p>
<p>The 1s electrons are too deep inside the atom to be concerned with the bonding and so we'll ignore them from now on. Hybridisation occurs in the oxygen as well. It is easier to see this using "electrons-in-boxes".</p>
</div>

<div class="image-container">
<img src="oxboxes.GIF">
</div>

<div class="text-block">
<p>This time two of the sp<sup>2</sup> hybrid orbitals contain lone pairs of electrons.</p>
</div>

<div class="note">
<p>Help! A "lone pair" of electrons is a pair of electrons at the bonding level which isn't being used to bond on to another atom.</p>
</div>

<div class="image-container">
<img src="oxorbs.GIF">
</div>

<div class="text-block">
<p>The carbon atom and oxygen atom then bond in much the same way as the two carbons do in ethene. In the next diagram, we are assuming that the carbon will also bond to two hydrogens to make methanal – but it could equally well bond to anything else.</p>
</div>

<div class="image-container">
<img src="methanalbits.GIF">
</div>

<div class="text-block">
<p>End-to-end overlap between the atomic orbitals that are pointing towards each other produce sigma bonds.</p>
<p>Notice that the p orbitals are overlapping sideways.</p>
</div>

<div class="image-container">
<img src="methanalorbits.GIF">
</div>

<div class="text-block">
<p>This sideways overlap produces a &pi; bond. So just like C=C, C=O is made up of a sigma bond and a &pi; bond.</p>
<p>Does that mean that the bonding is exactly the same as in ethene? No! The distribution of electrons in the &pi; bond is heavily distorted towards the oxygen end of the bond, because oxygen is much more electronegative than carbon.</p>
</div>

<div class="note">
<p>Help! You can read about the origins of <a href="eneg.html#top">electronegativity</a> and its effects in organic compounds in a separate article.</p>
</div>

<div class="image-container">
<img src="methanalpi.GIF">
</div>

<div class="text-block">
<p>This distortion in the &pi; bond causes major differences in the reactions of compounds containing carbon-oxygen double bonds like methanal compared with compounds containing carbon-carbon double bonds like ethene.</p>
</div>

<div class="note">
<p>Note: You can read about <a href="../../mechanisms/nucaddmenu.html#top">addition reactions</a> or <a href="../../mechanisms/addelimmenu.html#top">addition-elimination reactions</a> of carbonyl compounds elswhere on this site.</p>
</div>

<div class="questions-note">
<p>Questions to test your understanding</p>
<a href="../questions/q-carbonyl.pdf" target="_blank">Questions on the bonding in the carbon-oxygen double bond</a>
<a href="../questions/a-carbonyl.pdf" target="_blank">Answers</a>
</div>

<div class="link-list">
<p>Where would you like to go now?</p>
<a href="../bondmenu.html#top">To the organic bonding menu</a>
<a href="../../orgmenu.html#top">To menu of basic organic chemistry</a>
<a href="../../index.html#top">To Main Menu</a>
</div>

<div class="copyright"><a href="../../about.html">&copy;</a></div>
</div>
</body>

</html>